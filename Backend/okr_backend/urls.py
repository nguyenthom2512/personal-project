"""okr_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

from api.m_okr import views as okr_views
from api.m_account import views as user_views
from api.m_department import views as department_views
from api.m_todo_list import views as todo_views
from api.m_todo_list.views import group_todo
from api.m_upload.views import FileUploadView

from django.conf import settings
from django.conf.urls.static import static
from django.contrib.admin.sites import AdminSite

from api.m_quiz import views as quiz_views
from api.m_okr.views import dashboard_view, list_okr_relate, detail_okr_tree,data_for_create,recursive_update_root_result
from api.m_report import views as report_views
from api.m_report.views import report_feeback
from api.m_feedback import views as feedback_views
from api.m_feedback.views import rank_department, rank_month, statistics_feedback
from api.m_notification import views as noti_views
# from api. import views as auth_views
from api.authmail_template import views as custom_auth
from authemail import views as auth_views
from api.m_payment import views as payment_views
from api.m_authadmin import views as custom_admin
#SWAGGER DOCUMENT    
schema_view = get_schema_view(
   openapi.Info(
      title="OKRS API",
      default_version='v1',
      description="List API OF OKRS APP AND WEB",
      terms_of_service="https://www.google.com/policies/terms/",
      contact=openapi.Contact(email="contact@snippets.local"),
      license=openapi.License(name="BSD License"),
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)

AdminSite.login_template = 'login.html'

urlpatterns = [
    #path('swagger(?P<format>\.json|\.yaml)', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('jet/', include('jet.urls', 'jet')),  # Django JET URLS
    path('admin/', admin.site.urls),
    path('admin/login/', include('allauth.urls')),
    path('api/accounts/', include('authemail.urls')),
    # path('api/signup/verify/', auth_views.SignupVerify.as_view(),), 
    
    # AUTHMAIL
    path('api/signup/verify/', custom_auth.SignupVerifyFrontEnd.as_view()),
    path('api/signup/verified/', custom_auth.SignupVerifiedFrontEnd.as_view(),
         name='signup_verified_page'),
    path('api/signup/not_verified/', custom_auth.SignupNotVerifiedFrontEnd.as_view(),
         name='signup_not_verified_page'),
    
    path('api/password/reset/verify/', custom_auth.PasswordResetVerifyFrontEnd.as_view()),
    path('api/password/reset/verified/',
         custom_auth.PasswordResetVerifiedFrontEnd.as_view(),
         name='password_reset_verified_page'),
    path('api/password/reset/not_verified/',
         custom_auth.PasswordResetNotVerifiedFrontEnd.as_view(),
         name='password_reset_not_verified_page'),
    path('api/password/reset/success/', custom_auth.PasswordResetSuccessFrontEnd.as_view(),
         name='password_reset_success_page'),

    path('api/password/change/', custom_auth.PasswordChangeFrontEnd.as_view(),
         name='password_change_page'),
    path('api/password/change/success/', custom_auth.PasswordChangeSuccessFrontEnd.as_view(),
         name='password_change_success_page'),
    path('password/reset/email_sent/',custom_auth.PasswordResetEmailSentFrontEnd.as_view(),
         name='password_reset_email_sent_page'),
    
    
    # USER
    path('api/user/', user_views.UserList.as_view()),
    path('api/signup/', user_views.Signup.as_view()),
    path('api/package/', user_views.PackageList.as_view()),
    path('api/user/<str:pk>', user_views.UserEdit.as_view()),
    path('api/login/', user_views.Login.as_view()),
    path('api/login/social/', user_views.login_social),
    path('api/transform_full_name/', user_views.transform_data_fullname),
    path('api/transfer_diamond/', user_views.transfer_rate_to_diamond),
    # PRESENT
    path('api/present/', user_views.PresentList.as_view()),
    path('api/present/change/', user_views.ChangePresentList.as_view()),
    path('api/present/change/<str:pk>', user_views.ChangePresentDetail.as_view()),

    #DEPARTMENT AND COMPANY
    path('api/company/', department_views.CompanyList.as_view()),
    path('api/department/', department_views.DepartmentList.as_view()),
    #path('snippets/<int:pk>', views.snippet_detail),
   
    # UNIT
    path('api/unit/', okr_views.UnitList.as_view()),
    
    # OKR
    path('api/okr/', okr_views.OKRList.as_view()),
    path('api/okr/<str:pk>', okr_views.OKRDetail.as_view()),
    path('api/okr/relate/<str:pk>', list_okr_relate),
    path('api/okr/all/', okr_views.OKRListAll.as_view()),
    path('api/okr/challenge/', okr_views.ChallengeList.as_view()),
    path('api/okr/challenge/<str:pk>', okr_views.ChallengeDetaiL.as_view()),
    path('api/okr/inspiration/', okr_views.InspirationList.as_view()),
    path('api/okr/inspiration/<str:pk>', okr_views.InspirationDetail.as_view()),
    path('api/okr/dashboard/', dashboard_view),
    path('api/okr/mute-notification/', okr_views.muted_okr),
    path('api/data-for-creating-okr/', data_for_create),
    path('api/check-in/', okr_views.CheckinList.as_view()),
    path('api/check-in/<str:pk>', okr_views.CheckinDetail.as_view()),
    path('api/okr-tree/<str:pk>', detail_okr_tree),
    path('api/comment/', okr_views.CommentList.as_view()),
    path('api/comment/<str:pk>', okr_views.CommentDetail.as_view()),
    path('api/recursive/', recursive_update_root_result),
    # ROOM
    path('api/room/', okr_views.RoomCreate.as_view()),
    path('api/room/<str:pk>', okr_views.RoomDetail.as_view()),
#     path('api/room-detail/', get_room_info),

    # REPORT
    path('api/report/okr/', report_views.OKRReport.as_view()),
    path('api/report/checkin/', report_views.CheckinReport.as_view()),
    path('api/report/feedback/', report_feeback),
    
    
    # RESULT
    path('api/result/', okr_views.ResultList.as_view()),
    path('api/result/<str:pk>', okr_views.ResultDetail.as_view()),
    # TODO
    path('api/todo/', todo_views.TodoList.as_view()),
    path('api/todo/dashboard/', todo_views.dashboard_todo),
    path('api/todo-group/', group_todo),
    path('api/todo/', todo_views.TodoList.as_view()),
    path('api/todo/<str:pk>', todo_views.TodoDetail.as_view()),
    path('api/priority/', todo_views.PriorityList.as_view()),
    path('api/update-priority-list/', todo_views.update_list_priority),
    path('api/priority/<str:pk>', todo_views.PriorityDetail.as_view()),
    path('api/tag/', todo_views.TagList.as_view()),
    path('api/tag/<str:pk>', todo_views.TagDetail.as_view()),
    
    # UPLOAD
    path('api/upload/', FileUploadView.as_view()),
    
    # FEEDBACK
    path('api/feedback/', feedback_views.FeedbackList.as_view()),
    path('api/feedback/<str:pk>', feedback_views.FeedbackDetail.as_view()),
    path('api/feedback/statistics/', statistics_feedback),
    path('api/feedback/rank-rate/month/', rank_month),
    path('api/feedback/rank-rate/department/', rank_department),
    path('api/criteria/', feedback_views.CriteriaList.as_view()),
    
    # QUIZ SYSTEM
    path('api/quiz/', quiz_views.QuizList.as_view()),
    path('api/quiz/<str:pk>', quiz_views.QuizDetail.as_view()),
    path('api/question/', quiz_views.QuestionList.as_view()),
    path('api/user-quiz/', quiz_views.UserQuizCreate.as_view()),
    path('api/user-submit-quiz/<str:pk>', quiz_views.UserQuizSubmit.as_view()),

    # NOTIFICATION
    path('api/notification/', noti_views.NotiList.as_view()),
    path('api/message/', noti_views.send_mesage),
    path('api/notification/<str:pk>', noti_views.NotiDetail.as_view()),
    
    # PAYMENT
    path('api/payment/', payment_views.payment),
    path('api/payment_return/', payment_views.payment_return),
    path('api/payment_ipn/', payment_views.payment_ipn),
    
     # Other App
     
    path('api/quiz-app/quiz/', quiz_views.QuizListApp.as_view()),
    path('api/quiz-app/quiz/<str:pk>', quiz_views.QuizDetailApp.as_view()),
    path('api/quiz-app/user-quiz/', quiz_views.UserQuizCreateApp.as_view()),
    path('api/quiz-app/user-submit-quiz/<str:pk>', quiz_views.UserQuizSubmitApp.as_view()),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)