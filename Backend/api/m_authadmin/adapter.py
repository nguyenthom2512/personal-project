from django.conf import settings
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.account.adapter import DefaultAccountAdapter
from django.http import HttpResponseRedirect
from django.urls import reverse
from api.models import AppUser
from allauth.account.utils import user_email, user_field, user_username


class UserAccountAdapter(DefaultAccountAdapter):
    
    def is_open_for_signup(self, request):
        return False
    def save_user(self, request, user, form, commit=True):
        """
        Saves a new `User` instance using information provided in the
        signup form.
        """
        from allauth.utils import user_email, user_field, user_username

        data = form.cleaned_data
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        email = data.get("email")
        username = data.get("username")
        user_email(user, email)
        user_username(user, username)
        if first_name:
            user_field(user, "first_name", first_name)
        if last_name:
            user_field(user, "last_name", last_name)
        if "password1" in data:
            user.set_password(data["password1"])
        else:
            user.set_unusable_password()
        self.populate_username(request, user)
        if commit:
            # Ability not to commit makes it easier to derive from
            # this adapter by adding
            user.save()
        return user
class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def get_signup_form_initial_data(self, sociallogin):
        # user = sociallogin.user
        extra_data = sociallogin.account.extra_data
        uid = extra_data['id']
        if sociallogin.account.provider == 'google':
            email = 'gg' + uid
        if sociallogin.account.provider == 'facebook':
            email = 'fb' + uid
        user = AppUser.objects.get(email = email)
        initial = {
            "email": email or "",
            "full_name": user_field(user, "first_name") + user_field(user, "last_name") or "",
            "first_name": user_field(user, "first_name") or "",
            "last_name": user_field(user, "last_name") or "",
        }
        return initial
    def populate_user(self, request, sociallogin, data):
        extra_data = sociallogin.account.extra_data
        uid = extra_data['id']
        if sociallogin.account.provider == 'google':
            email = 'gg' + uid
        if sociallogin.account.provider == 'facebook':
            email = 'fb' + uid
        
        """
        Hook that can be used to further populate the user instance.

        For convenience, we populate several common fields.

        Note that the user instance being populated represents a
        suggested User instance that represents the social user that is
        in the process of being logged in.

        The User instance need not be completely valid and conflict
        free. For example, verifying whether or not the username
        already exists, is not a responsibility.
        """
        username = data.get("username")
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        try:
            user_qs = AppUser.objects.get(email = email)
            return user_qs
        except AppUser.DoesNotExist:
            user = AppUser()
            name = data.get("name")
            user.email = email
            user.full_name = last_name + " " + first_name
            name_parts = (name or "").partition(" ")
            user_field(user, "first_name", first_name or name_parts[0])
            user_field(user, "last_name", last_name or name_parts[2])
            user.save()
            return user
    def is_auto_signup_allowed(self, request, sociallogin):
        return True
    def save_user(self, request, sociallogin, form=None):
        print('asdfasdf')
    
    # def pre_social_login(self, request, sociallogin):
    #     extra_data = sociallogin.account.extra_data
    #     uid = extra_data['id']
    #     if sociallogin.account.provider == 'google':
    #         try:
    #             user_qs = AppUser.objects.get(email = f'gg{uid}')
    #             return HttpResponseRedirect(reverse('admin:index'))
    #         except AppUser.DoesNotExist:
    #             return HttpResponseRedirect(reverse('admin:login'))
    #     if sociallogin.account.provider == 'facebook':
    #         try:
    #             user_qs = AppUser.objects.get(email = f'fb{uid}')
    #             return HttpResponseRedirect(reverse('admin:index'))
    #         except AppUser.DoesNotExist:
    #             pass
    #             return HttpResponseRedirect(reverse('admin:login'))
        # user = sociallogin.user
        # if user.id:  
        #     return          
        # try:
        #     customer = Customer.objects.get(email=user.email)  # if user exists, connect the account to the existing account and login
        #     sociallogin.state['process'] = 'connect'                
        #     perform_login(request, customer, 'none')
        # except Customer.DoesNotExist:
        #     pass
        # return super().pre_social_login(request, sociallogin)
    # def get_connect_redirect_url(self, request, socialaccount):
    #     path = "/accounts/{username}/"
    #     print(path)
    #     return path.format(username=request.user.username)