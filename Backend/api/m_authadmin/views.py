from django.contrib.admin.sites import AdminSite
from django.views.generic.base import TemplateView
from django.conf import settings
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter


# class CustomAdminLoginForm(AdminSite):
    
#     def __init__(self, name: str = ...) -> None:
#         super().__init__(name=name)
    
#     def login(self, request, extra_context):
#         print(extra_context)
#         return super().login(request, extra_context=extra_context)


class SocialAccountAdapter(DefaultSocialAccountAdapter):

    def get_connect_redirect_url(self, request, socialaccount):
        path = "/accounts/{username}/"
        print(path)
        return path.format(username=request.user.username)
class AdminLoginFrontEnd(TemplateView):
    template_name = 'login.html'