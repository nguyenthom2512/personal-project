import json
from django.db.models.aggregates import Count, Sum
from api.models import OKR, AppUser, Checkin, Feedback, Todo
from rest_framework import generics,status
from api.m_okr.serializers import CheckinSerializer, OKRListSerializer
from django_filters.rest_framework.backends import DjangoFilterBackend
from api.filters  import FilterCreatedTimeByDateRange
from rest_framework.decorators import api_view, permission_classes
from django.http import JsonResponse

from api.permissions import IsUserHasPermissionsOKR

 
#### REPORT ####
class OKRReport(generics.ListAPIView):
    pagination_class = None
    # queryset = OKR.objects.all();
    serializer_class = OKRListSerializer
    filter_backends = [DjangoFilterBackend, FilterCreatedTimeByDateRange]
    filter_fields = {'department':['exact'],'is_done': ['exact'], 'user':['exact']}
    def get_queryset(self):
        query = self.request.query_params
        if len(query) != 0:
            return OKR.objects.all()
        else:
            return OKR.objects.none()
    permission_classes = [IsUserHasPermissionsOKR]

class CheckinReport(generics.ListAPIView):
    pagination_class = None
    serializer_class = CheckinSerializer
    filter_backends = [DjangoFilterBackend,FilterCreatedTimeByDateRange]
    filter_fields = {'user':['exact']}
    def get_queryset(self):
        query = self.request.query_params
        if len(query) != 0:
            return Checkin.objects.all()
        else:
            return Checkin.objects.none()
    permission_classes = [IsUserHasPermissionsOKR]
    
        
@api_view(['GET'])
def report_feeback(request, format=None):
    '''
    param
    --
    criteria__isnull = true,
    department = int,
    ?criteria = int,
    '''
    department= request.query_params.get('department', None)
    criteria = request.query_params.get('criteria', None)   
    filter_set = {
        'criteria__isnull': False,
        'okr__department': department,
    }
    if criteria is not None:
        filter_set['criteria'] = criteria
    receive_feedback = Feedback.objects.filter(**filter_set ).values('user_received','user_received__last_name', 'user_received__first_name','user_received__img_url','user_received__role__role_name').annotate(total = Sum('criteria__rate')).order_by('-total')
    sent_feedback = Feedback.objects.filter(**filter_set ).values('user','user__last_name','user__first_name','user__img_url', 'user__role__role_name').annotate(total = Sum('criteria__rate')).order_by('-total')
    response = {
        'received_feedback': list(receive_feedback),
        'sent_feedback': list(sent_feedback)
        }
    return JsonResponse(response)

    