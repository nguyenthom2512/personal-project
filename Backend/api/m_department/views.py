from rest_framework import generics
from django_filters.rest_framework.backends import DjangoFilterBackend
from api.models import Company, Department
from .serializers import CompanySerializer, DepartmentSerializer
from api.filters import FilterDepartmentInCompanyByUserRequest
from api.permissions import IsUserHasPermissionsOKR
from django_filters.rest_framework import DjangoFilterBackend
from api.filters import FilterCompanyByUser

class CompanyList(generics.ListAPIView):
    pagination_class = None
    queryset = Company.objects.all()
    filter_backends = [DjangoFilterBackend, FilterCompanyByUser]
    serializer_class = CompanySerializer
    
class DepartmentList(generics.ListAPIView):
    pagination_class = None
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    filter_backends = [DjangoFilterBackend,FilterDepartmentInCompanyByUserRequest]
    filterset_fields = ['company',]
    permission_classes = [IsUserHasPermissionsOKR,]
    