from django.db import models
from rest_flex_fields.serializers import FlexFieldsModelSerializer
from rest_framework import fields
from api.models import Company, Department, Master


class CompanySerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'
        
class DepartmentSerializer(FlexFieldsModelSerializer):
    company = CompanySerializer()
    class Meta:
        model = Department
        fields = '__all__'

class MasterCompanySerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Master
        fields = '__all__'
        