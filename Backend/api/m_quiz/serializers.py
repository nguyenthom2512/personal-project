from typing import ItemsView
from django.contrib.auth.models import User
from django.db import models, transaction
from django.db.models.base import Model
from rest_framework import fields, serializers
from rest_flex_fields import FlexFieldsModelSerializer
from api.m_okr.serializers import CheckinDetailSerializer
from api.models import AppUser, Criteria, Feedback, Inspiration, Notification, Question, Answer, QuestionAttachment, QuestionQuiz, Quiz, UserQuestion, UserQuiz
import datetime
from api.utils import NotificationBody
from api.notification import send_noti_by_token

class AnswerSerializer(FlexFieldsModelSerializer):
    weight = serializers.IntegerField(write_only = True)
    # id = serializers.IntegerField(read_only = True)
    user_id = serializers.IntegerField(write_only = True)
    class Meta:
        model = Answer
        fields = ('id','content', 'weight','user_id')
        
class QuestionAttachmentSerializer(FlexFieldsModelSerializer):
     class Meta:
        model = QuestionAttachment
        fields = ('file_url','file_type')

class QuestionSerializer(FlexFieldsModelSerializer):
    attachment_question = QuestionAttachmentSerializer(many = True, allow_null = False)
    answers = AnswerSerializer(many=True, allow_null = True)
    class Meta:
        model = Question
        fields = '__all__'
    # def create(self, validated_data):
    #     answer_date = validated_data.pop('answers',None)
    #     attachment_data = validated_data.pop('attachment_question', None)
    #     request_user = validated_data.get('user_created', None)
    #     answers = []
    #     attachments = []
    #     with transaction.atomic():
    #         question = super(QuestionSerializer, self).create(validated_data)
    #         for answer in answer_date:
    #             item = Answer(**answer, question_id = question.id)
    #             answers.append(item)
    #         Answer.objects.bulk_create(answers)
    #         for attachment in attachment_data:
    #             item = QuestionAttachment(**attachment, question_id = question)
    #             attachments.append(item)
    #         QuestionAttachment.objects.bulk_create(attachments)
    #         return question
        
        # return super().create(validated_data)

class QuizSerializer(FlexFieldsModelSerializer):
    questions = QuestionSerializer(many = True, allow_null = True)
    class Meta:
        model = Quiz
        fields = '__all__'
        
class UserQuestionSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = UserQuestion
        fields = ('question','answer')
class UserQuizSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = UserQuiz
        fields = ('id', 'user', 'quiz', 'created_time','inspiration')

class UserSubmitSerializer(FlexFieldsModelSerializer):
    question_quiz = UserQuestionSerializer(many = True, allow_null = True)
    # total_point =serializers.IntegerField(read_only = True)
    class Meta:
        model = UserQuiz
        fields = '__all__'
    def update(self, instance, validated_data):
        question_quiz_data = validated_data.pop('question_quiz', None)
        inspiration = validated_data.pop('inspiration', None)
        start_time = instance.created_time.replace(tzinfo=None,microsecond=0)
        time_end = datetime.datetime.now().replace(tzinfo=None,microsecond=0)
        self_user = self.context['request'].user
        duration = time_end - start_time
        user_questions = []
        user_total_point = 0
        if  duration.total_seconds() <= instance.quiz.duration:
            with transaction.atomic():
                if question_quiz_data:
                    for item in question_quiz_data:
                        quiz_point = QuestionQuiz.objects.filter(quiz = instance.quiz, question = item['question']).values()
                        answers = item['answer'].split(',')
                        user_weight = 0
                        total_weight = 0
                        for obj in Answer.objects.filter(question = item['question']):
                            total_weight += obj.weight
                            if str(obj.id) in answers:
                                user_weight += obj.weight
                        item['point'] = user_weight/total_weight
                        question = UserQuestion(**item,user_quiz_id = instance.id)
                        user_questions.append(question)
                        user_total_point += (user_weight/total_weight) * quiz_point[0]['point']
                    UserQuestion.objects.bulk_create(user_questions)
            instance.total_point = user_total_point
            instance.is_end = True
            instance.save()
            if inspiration:
                user = inspiration.user
                body_message = NotificationBody.noti_body(data_id= instance.id, content=f"đã làm bài cảm hứng của okr {inspiration.okr.object_name} được {instance.total_point} điểm",type_notification=6, okr_id=inspiration.okr.id)
                noti = Notification.objects.create(**body_message, user = user, user_sent = self_user)
                send_noti_by_token(to_user=[user], send_message=body_message, title=f'{self_user.full_name} đã làm bài cảm hứng của okr {inspiration.okr.object_name} được {instance.total_point} điểm')
            return instance
        raise serializers.ValidationError({"error":"Time is over!"})
 