from django.core.exceptions import ImproperlyConfigured
from django.db.models.query import QuerySet
from django.http.response import Http404
from rest_framework import generics
from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework import response

from api.permissions import IsUserHasPermissionsOKR
from .serializers import QuestionSerializer, QuizSerializer, UserQuizSerializer, UserSubmitSerializer
from api.models import Question, Quiz, UserQuiz, QuestionQuiz, Answer, UserQuestion
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework import status
import datetime
from api.pagination import StandardResultsSetPagination
from api.filters import FilterByCompany
from api.pagination import StandardResultsSetPagination
class QuestionList(generics.ListCreateAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = QuestionSerializer
    queryset = Question.objects.all()
    permission_classes = [IsUserHasPermissionsOKR]
    filter_backend  = [FilterByCompany]
    
    
class QuizList(generics.ListAPIView):
    pagination_class = None
    serializer_class = QuizSerializer
    queryset = Quiz.objects.all()
    filter_backend  = [FilterByCompany]
    permission_classes = [IsUserHasPermissionsOKR]

class QuizDetail(generics.RetrieveAPIView):
    pagination_class = None
    serializer_class = QuizSerializer
    queryset = Quiz.objects.all()
    
class UserQuizCreate(generics.ListCreateAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = UserQuizSerializer
    filter_backends = [DjangoFilterBackend,FilterByCompany]
    filterset_fields = ['inspiration']
    queryset = UserQuiz.objects.all()
    permission_classes  = [IsUserHasPermissionsOKR]
class UserQuizSubmit(generics.UpdateAPIView):
    pagination_class = None
    serializer_class = UserSubmitSerializer
    queryset = UserQuiz.objects.all()
    permission_classes = [IsUserHasPermissionsOKR]
 
 
 #### OTHER APP ####
 
class QuizListApp(generics.ListAPIView):
    pagination_class = None
    serializer_class = QuizSerializer
    queryset = Quiz.objects.all()
class QuizDetailApp(generics.RetrieveAPIView):
    pagination_class = None
    serializer_class = QuizSerializer
    queryset = Quiz.objects.all()
    
class UserQuizCreateApp(generics.ListCreateAPIView):
    pagination_class = None
    serializer_class = UserQuizSerializer
    queryset = UserQuiz.objects.all()
    
class UserQuizSubmitApp(generics.UpdateAPIView):
    pagination_class = None
    serializer_class = UserSubmitSerializer
    queryset = UserQuiz.objects.all()
    permission_classes = []
    