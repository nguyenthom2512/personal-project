from rest_flex_fields.serializers import FlexFieldsModelSerializer
from api.models import OKR, Challenge, Inspiration, Notification
from rest_framework import serializers

class NotiSerializer(FlexFieldsModelSerializer):
    user_sent_data = serializers.SerializerMethodField('get_user_sent_data')
    class Meta:
        model = Notification
        fields = '__all__'
    def get_user_sent_data(self, obj):
        return{'full_name': obj.user_sent.full_name, 'img_url': obj.user_sent.img_url}