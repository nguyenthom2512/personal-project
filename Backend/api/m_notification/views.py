
from rest_framework.response import Response
from api.models import Notification
from rest_framework import generics, filters, status
from .serializers import NotiSerializer
from django_filters.rest_framework.backends import DjangoFilterBackend
from api.filters import FilterByUserRequest
from rest_framework.decorators import api_view
from api.notification import send_noti_by_topic
from api.pagination import StandardResultsSetPagination

class NotiList(generics.ListCreateAPIView):
    """
    type_notification=6 -> user_do_quiz
    type_notification=5 -> feedback
    type_notification=4 -> detail_checkin
    type_notification=3 -> call
    type_notification=2 -> detail_okr
    type_notification=1 -> inspiration
    type_notification=0 -> challenge
    """
    pagination_class = StandardResultsSetPagination
    queryset = Notification.objects.all()
    serializer_class = NotiSerializer
    
    filter_backends = [DjangoFilterBackend,FilterByUserRequest]
    filterset_fields = []

class NotiDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    type_notification=6 -> user_do_quiz
    type_notification=5 -> feedback
    type_notification=3 -> call
    type_notification=2 -> detail_okr
    type_notification=1 -> inspiration
    type_notification=0 -> challenge
    """
    pagination_class = None
    queryset = Notification.objects.all()
    serializer_class = NotiSerializer
    
@api_view(['GET'])
def send_mesage(request):
    send_noti_by_topic()
    return Response(data={'message':'send message successfully'}, status=status.HTTP_200_OK)