class TypePatternFormat:
    date_format_pattern = '%Y-%m-%d'
    
class NotificationBody:
    def noti_body(data_id, content, type_notification, okr_id) -> dict:
        return {
            'title' : 'OKRs - Bạn có thông báo',
            'content' : content,
            'data_id' : data_id,
            'type_notification': type_notification,
            'is_read': False,
            'okr_id':okr_id
        }
        
password_parttern = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$"
