from django.db.models import query
from rest_framework import serializers
from rest_flex_fields.serializers import FlexFieldsModelSerializer
from rest_framework.relations import ManyRelatedField
from api.models import OKR, Comment, LinkUser, Priority, Result, Todo, Tag, TodoCheckList
from api.m_okr.serializers import CommentSerializer, LinkUserSerializer
from django.db import transaction
from api.utils import TypePatternFormat as parttern

class TagSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'
class PrioritySerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Priority
        fields = '__all__'
        
class ChecklistSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = TodoCheckList
        fields = ('id', 'content', 'is_done')
class TodoSerializer(FlexFieldsModelSerializer):
    raic_user = LinkUserSerializer(many=True, source='raic_user_todo')
    check_list = ChecklistSerializer(many = True, allow_null = True)
    todo_comment = CommentSerializer(many = True, allow_null = True)
    class Meta:
        model = Todo
        fields = '__all__'
    
    def validate(self, data):
        todo = self.instance
        dt_start_time = data.get('start_time', None)
        dt_end_time = data.get('end_time', None)
        self_user = self.context['request'].user
        if dt_end_time and dt_start_time:
            if todo: 
                todo_qs = Todo.objects.filter(user = self_user, end_time__date = dt_start_time.date(), is_done = False).exclude(id = todo.id)
            else:
                todo_qs = Todo.objects.filter(user = self_user, end_time__date = dt_start_time.date(), is_done = False)
            for todo in todo_qs:
                start_in = (todo.start_time <= dt_start_time and dt_start_time <= todo.end_time) 
                end_in = todo.start_time <= dt_end_time and dt_end_time <= todo.end_time 
                at_time = dt_end_time <= todo.start_time and todo.end_time <= dt_end_time
                if start_in or end_in or at_time:
                    raise serializers.ValidationError({'message': 'cannot start two todo in the same time'})                    
        return super().validate(data)
    
    def create(self, validated_data):
        raic_user = validated_data.pop('raic_user_todo')
        check_list_data = validated_data.pop('check_list')
        request_user = validated_data.get('user')
        comment_list_data = validated_data.pop('todo_comment', [])
        self_user = self.context['request'].user
        users = []
        check_lists = []
        comments = []
        if self_user.id == request_user.id: 
            with transaction.atomic():
                todo = super(TodoSerializer, self).create(validated_data)
                for user in raic_user:
                    item = LinkUser(**user, todo_id = todo.id)
                    users.append(item)
                LinkUser.objects.bulk_create(users)
                for check_list in check_list_data:
                    item = TodoCheckList(**check_list, todo_id = todo.id)
                    check_lists.append(item)
                TodoCheckList.objects.bulk_create(check_lists)
                for comment in comment_list_data:
                    item = Comment(**comment, todo_id = todo.id)
                    comments.append(item)
                Comment.objects.bulk_create(comments)
            return todo
        else:
            raise serializers.ValidationError({"message": "[create todo] user is different token"})
        
    def update(self, instance, validated_data):
        raic_user = validated_data.pop('raic_user_todo',None)
        check_list_data = validated_data.pop('check_list',None)
        comment_list_data = validated_data.pop('todo_comment', [])
        LinkUser.objects.filter(todo = instance.id).delete()
        TodoCheckList.objects.filter(todo= instance.id).delete()
        users = []
        check_lists = []
        comments = []
        with transaction.atomic():
            for attr, value in validated_data.items():
                if attr == 'tag':
                    instance.tag.set(value)
                    continue
                setattr(instance, attr, value)
            instance.save()
            if check_list_data is not None:
                for check_list in check_list_data:
                    item = TodoCheckList(**check_list, todo_id = instance.id)
                    check_lists.append(item)
                TodoCheckList.objects.bulk_create(check_lists)    
            if raic_user is not None:
                for user in raic_user:
                    item = LinkUser(**user, todo_id = instance.id)
                    users.append(item)
                LinkUser.objects.bulk_create(users)
            for comment in comment_list_data:
                    item = Comment(**comment, todo_id = instance.id)
                    comments.append(item)
            Comment.objects.bulk_create(comments)
            return instance
        
class PriorityListSerializer(FlexFieldsModelSerializer):
    priority_todo = serializers.SerializerMethodField('get_priority_todo')
    class Meta:
        model = Priority
        fields = '__all__'
    def get_priority_todo(self, obj):
        user = self.context['request'].user
        todo_name = self.context['request'].query_params.get('todo_name', None)
        is_done = self.context['request'].query_params.get('is_done', None)
        result__isnull = self.context['request'].query_params.get('result__isnull', None)
        fillter_query ={'priority_id': obj.id,'user':user}
        if todo_name: 
            fillter_query['todo_name__contains'] = todo_name
        if is_done is not None:
            fillter_query['is_done'] = True if is_done == 'true' else False
        if result__isnull is not None:
            fillter_query['result__isnull'] = True if result__isnull == 'true' else False
        todo_qs = Todo.objects.filter(**fillter_query)
        return TodoSerializer(todo_qs, many = True).data
    
class TodoListSerializer(FlexFieldsModelSerializer):
    priority = PrioritySerializer()
    tag = TagSerializer(many=True,allow_null=True)
    raic_user = LinkUserSerializer(many=True, source='raic_user_todo')
    
    result_name = serializers.SerializerMethodField('get_result_name')
    okr_name = serializers.SerializerMethodField('get_okr_name')
    check_list = ChecklistSerializer(many = True, allow_null = True)
    comment_count = serializers.SerializerMethodField('get_comment_count')
    okr_id = serializers.SerializerMethodField('get_okr_id')
    department = serializers.SerializerMethodField('get_user_department')
    priority_color = serializers.SerializerMethodField('get_priority_color')
    

    class Meta:
        model = Todo
        fields = '__all__'
        
    def get_priority_color(self, obj):
        return obj.priority.color
    
    def get_result_name(self,obj):
        if obj.result:
            return obj.result.key_result
        return None
    
    def get_okr_name(self,obj):
        if obj.result:
            return obj.result.okr.object_name
        return None
    
    def get_comment_count(self,obj):
        return Comment.objects.filter(todo_id = obj.id).count()
    
    def get_okr_id(self, obj):
        if obj.result:
            return Result.objects.get(id = obj.result_id).okr_id
        return None
    def get_user_department(self, obj):
        if obj.user.department:
            return obj.user.department.department_name
        return None