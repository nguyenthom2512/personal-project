from django.http import response
from django.http.response import JsonResponse
from rest_framework.response import Response  
from rest_framework import serializers,status
from api.models import Company, Master, Notification, Todo,Tag, Priority
from .serializers import PrioritySerializer, TagSerializer, TodoSerializer, PriorityListSerializer, TodoListSerializer
from rest_framework import generics
from django_filters.rest_framework.backends import DjangoFilterBackend
from datetime import date, datetime, timedelta
from rest_framework.decorators import api_view
from django.db.models import Q
from rest_framework.pagination import PageNumberPagination
from api.utils import TypePatternFormat as pattern
from api.filters import FilterByUserRequest

class StandardResultsSetPagination(PageNumberPagination):
    page_size = 20
    page_size_query_param = 'page_size'
    max_page_size = 1000
    ordering = 'id'

@api_view(['PUT','PATCH'])
def update_list_priority(request,format=None, *args, **kwargs):
    # serializer = PrioritySerializer(post, data=data)
    data = request.data
    serializer = PrioritySerializer(data=data,many = True)
    instances = []
    if serializer.is_valid():
        for item in data:
            obj = Priority.objects.get(id = item['id'])
            obj.priority_level = item['priority_level']
            obj.priority_name = item['priority_name']
            obj.color = item['color']
            obj.save()
            instances.append(obj)
        serializer = PrioritySerializer(instances, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class PriorityList(generics.ListCreateAPIView):
    pagination_class = None
    serializer_class = PriorityListSerializer
    filter_backends = [FilterByUserRequest]
    queryset = Priority.objects.all().order_by('priority_level')
    def get_serializer_class(self):
        if self.request.method == 'POST':
           return PrioritySerializer
        return PriorityListSerializer 

class PriorityDetail(generics.RetrieveDestroyAPIView):
    pagination_class = None
    serializer_class = PrioritySerializer
    queryset = Priority.objects.all()

# check filter
@api_view(['GET'])
def group_todo(request,format=None):
    grouped = dict()
    start_time = request.query_params.get('start_time',None)
    end_time = request.query_params.get('end_time',None) 
    is_done = request.query_params.get('is_done',False)
    if start_time != None and end_time != None:
        dt_start_time = datetime.strptime(start_time, pattern.date_format_pattern)
        dt_end_time = datetime.strptime(end_time, pattern.date_format_pattern)
        date_range = [dt_start_time + timedelta(days=i) for i in range((dt_end_time + timedelta(days=1) - dt_start_time).days)]
        for date in date_range:
            obj = Todo.objects.filter(start_time__date__lte=date, end_time__date__gte=date, is_done=is_done, user = request.user)
            if obj.count() != 0:
                grouped[date.strftime("%Y-%m-%d")] = TodoListSerializer(obj,many=True).data
        return JsonResponse(grouped, safe=False)
    else:
        return JsonResponse({}, safe=False)


class TodoList(generics.ListCreateAPIView):
    pagination_class = StandardResultsSetPagination
    filter_backends =[DjangoFilterBackend, FilterByUserRequest]
    filter_fields = {'start_time':['date','isnull'], 'end_time':['date'], 'is_done':['exact'], 'result':['isnull']}
    queryset = Todo.objects.all()
    def get_serializer_class(self):
        if self.request.method == 'POST':
           return TodoSerializer
        return TodoListSerializer
class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    confident=0 -> bad
    confident=1 -> normal
    confident=2 -> good
    confident=3 -> very good
    """
    pagination_class = None
    serializer_class = TodoListSerializer
    queryset = Todo.objects.all()
    def get_serializer_class(self):
        if self.request.method == 'GET':
           return TodoListSerializer
        return TodoSerializer
    
class TagList(generics.ListCreateAPIView):
    pagination_class = None
    serializer_class = TagSerializer
    filter_backends =[DjangoFilterBackend, FilterByUserRequest]
    queryset = Tag.objects.all()
class TagDetail(generics.RetrieveUpdateDestroyAPIView):
    pagination_class = None
    serializer_class = TagSerializer
    queryset = Tag.objects.all()

@api_view(['GET'])
def dashboard_todo(request):
    user  = request.user
    if user.pk:
        if user.department:
            master_qs = Master.objects.filter(company = user.department.company).first() 
        response = {
            'total_rate': user.total_rate,
            'todo_done': Todo.objects.filter(user = user, is_done = True).count(),
            'todo_doing': Todo.objects.filter(user = user, is_done = False).count(),
            'diamond' : int(user.total_rate * master_qs.ratio_convert_diamond) if user.department else 0, # wait third part
            'unread_notification': Notification.objects.filter(user = user, is_read = False).count()
        }
        return Response(data=response, status= status.HTTP_200_OK)
    return Response(data={"message":'user invalid'}, status= status.HTTP_400_BAD_REQUEST)