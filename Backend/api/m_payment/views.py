from rest_framework.decorators import api_view, authentication_classes, \
    permission_classes
from datetime import datetime
import requests
from django.conf import settings
from rest_framework.response import Response
from django.http.response import JsonResponse
from rest_framework import status
from api.models import AppUser, Company, Department, Notification, Package, Payment, Transaction
from api.notification import send_noti_by_token
from django.db.models import Q
import os
from api.vnpay import vnpay
from django.contrib.auth.models import Group
from dateutil.relativedelta import relativedelta
from django.shortcuts import render



import uuid
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

# def send_noti(id, message):
#     appuser = AppUser.objects.filter(id=id).first()
#     if appuser is not None:
#         notification = {
#             "registration_ids":[appuser.notificationId],
#             "collapse_key": "message",
#             "notification": {
#                 "body": message['Message'],
#                 "title":"Nạp tiền vào tài khoản"
#                 },
#             "data":  {
#                 "message":message['Message'],
#                 "type": "topup",
#                 "rspcode": message['RspCode']
#             }
#         }
#         headers = {
#             "Content-type": "application/json",
#             "Authorization": "key="+ NOTIFICATION_FCM.SERVER_KEY
#         }
#         response = requests.post(NOTIFICATION_FCM.URL,data=json.dumps(notification),headers=headers)
# 

noti_title = 'Nạp tiền vào tài khoản'

@api_view(['POST'])
def payment(request):
    package_id =  request.data['package_id']
    package = Package.objects.get(pk=package_id)
    if package:
        result = package.price * 100
        orderId = uuid.uuid4()
        createDate = datetime.now().strftime('%Y%m%d%H%M%S') # 20150410063022
        orderInfo = request.user.id
        ipAddr = str(get_client_ip(request))
        currCode = "VND"
        locale = "vn"
        # Build URL Payment
        payment = Payment()
        payment.vnp_version = '2.1.0'
        payment.vnp_command = 'pay'
        payment.vnp_tmncode = settings.VNPAY_TMN_CODE
        payment.vnp_locale= locale
        payment.vnp_currcode= currCode
        payment.vnp_txnref = orderId
        payment.vnp_orderinfo = orderInfo
        payment.vnp_amount = str(result)
        payment.vnp_returnurl = settings.VNPAY_RETURN_URL
        payment.vnp_ipaddr = ipAddr
        payment.vnp_createdate = createDate
        payment.vnp_ordertype = "topup"
        payment.vpn_package = package_id
        # payment.vnp_bankcode = ""
        payment.save()
        vnp = vnpay()
        vnp.requestData['vnp_Version'] = payment.vnp_version
        vnp.requestData['vnp_Command'] = payment.vnp_command
        vnp.requestData['vnp_TmnCode'] = payment.vnp_tmncode 
        vnp.requestData['vnp_Amount'] = payment.vnp_amount
        # vnp.requestData['vnp_BankCode'] = payment.vnp_bankcode
        vnp.requestData['vnp_CreateDate'] = payment.vnp_createdate
        vnp.requestData['vnp_CurrCode'] =  payment.vnp_currcode
        vnp.requestData['vnp_IpAddr'] = payment.vnp_ipaddr
        vnp.requestData['vnp_Locale'] = payment.vnp_locale
        vnp.requestData['vnp_OrderInfo'] = payment.vnp_orderinfo
        vnp.requestData['vnp_OrderType'] = payment.vnp_ordertype
        vnp.requestData['vnp_ReturnUrl'] = payment.vnp_returnurl
        vnp.requestData['vnp_TxnRef'] = payment.vnp_txnref

        vnpay_payment_url = vnp.get_payment_url(settings.VNPAY_PAYMENT_URL, settings.VNPAY_HASH_SECRET_KEY)
        order = {}
        order["url"] = vnpay_payment_url
        return Response(order,status=status.HTTP_200_OK)
    
    return Response({},status= status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def payment_ipn(request):
    inputData = request.GET
    if inputData:
        vnp = vnpay()
        vnp.responseData = inputData.dict()
        order_id = inputData['vnp_TxnRef']
        amount = int(inputData['vnp_Amount']) / 100
        order_desc = inputData['vnp_OrderInfo']
        id = int(order_desc)
        vnp_TransactionNo = inputData['vnp_TransactionNo']
        vnp_ResponseCode = inputData['vnp_ResponseCode']
        vnp_TmnCode = inputData['vnp_TmnCode']
        vnp_PayDate = inputData['vnp_PayDate']
        vnp_BankCode = inputData['vnp_BankCode']
        vnp_CardType = inputData['vnp_CardType']
        
        app_user = AppUser.objects.filter(id=id).first()

        
        if vnp.validate_response(settings.VNPAY_HASH_SECRET_KEY):

            checkOrderID = True;    # check vnp_TxnRef VNPAY trả về tồn tại trong CSDL
            firstTimeUpdate = False; # paymentstatus = pending (tức chưa được update trạng thái thanh toán trước đó)
            checkAmount = True; #check vnp_Amount VNPAY so sánh với số tiền của đơn hàng trong DB là đúng.
            transaction = Transaction.objects.filter(Q(order_id=order_id) | Q(vnp_transactionno=vnp_TransactionNo)).first()
            payment = Payment.objects.filter(vnp_txnref=order_id).first()
            if payment is None:
                checkOrderID = False
            else:
                if int(payment.vnp_amount)/100 != amount:
                    checkAmount = False;
            if  checkOrderID:
                print(checkOrderID)
                if  transaction is None: #(paymentstatus = pending)
                    if  checkAmount:
                        if vnp_ResponseCode == '00':
                                    
                            transaction_user = Transaction(user=app_user)
                            transaction_user.type = 0
                            transaction_user.channels = 1
                            transaction_user.order_id = order_id
                            transaction_user.amount = amount
                            transaction_user.order_desc = order_desc
                            transaction_user.vnp_transactionno = vnp_TransactionNo
                            transaction_user.vnp_tmncode = vnp_TmnCode
                            transaction_user.vnp_paydate = vnp_PayDate
                            transaction_user.vnp_bankcode = vnp_BankCode
                            transaction_user.vnp_cardtype = vnp_CardType
                            # Ở đây xử lý code update trạng thái thanh toán thành công (paymentstatus = success) cho đơn hàng vào database
                            transaction_user.vnp_responsecode = "Thành công " + vnp_ResponseCode
                            # transaction_user.current_amount = app_user.amount + amount
                            transaction_user.save()
                            # app_user.amount = transaction_user.current_amount
                            # app_user.save()
                            
                            # update user
                            try:
                                payment_qs = Payment.objects.filter(vnp_txnref = order_id).first()
                                package_qs = Package.objects.get(id = payment_qs.vpn_package)
                                group_qs = Group.objects.filter(name = 'premium')
                                app_user.is_staff = True
                                app_user.groups.set(group_qs)
                                app_user.expired = datetime.now() + relativedelta(months=package_qs.duration)
                                
                                # create company 
                                company = Company.objects.create(company_name = f"Công ty của {app_user.full_name}")
                                department = Department.objects.create(company = company, department_name = "Tổng công ty")
                                app_user.department = department
                                app_user.save()
                                respone = 'Thành công'
                            except Exception as e:
                                respone = e

                            send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '00', 'Message': 'Confirm Success'}, title=noti_title)
                            result = JsonResponse({'RspCode': '00', 'Message': 'Confirm Success','response':respone})
                        else:                            
                            print("đã vào  00")
                            send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '00', 'Message': 'Confirm Success'},title=noti_title)
                            result = JsonResponse({'RspCode': '00', 'Message': 'Confirm Success'})
                    else:
#                         print("đã vào  04")
#                         transaction_user.vnp_responsecode = "Không thành công mã " + vnp_ResponseCode
#                         transaction_user.current_amount = app_user.amount
#                         
                        send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '04', 'Message': 'Invalid Amount'},title=noti_title)
                        result = JsonResponse({'RspCode': '04', 'Message': 'Invalid Amount'})
                else:
#                     print("đã vào  02")
#                     transaction_user.vnp_responsecode = "Không thành công mã " + vnp_ResponseCode
#                     transaction_user.current_amount = app_user.amount
                    send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '02', 'Message': 'Order Already Update'},title=noti_title)
                    result = JsonResponse({'RspCode': '02', 'Message': 'Order Already Update'})
            else:
#                 print("đã vào  01")
#                 transaction_user.vnp_responsecode = "Không thành công mã " + vnp_ResponseCode
#                 transaction_user.current_amount = app_user.amount
                result = JsonResponse({'RspCode': '01', 'Message': 'Order not Found'})
                send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '01', 'Message': 'Order not Found'},title=noti_title)
        else:
#             print("đã vào  97")
#             transaction_user.vnp_responsecode = "Không thành công mã " + vnp_ResponseCode
#             transaction_user.current_amount = app_user.amount
            result = JsonResponse({'RspCode': '97', 'Message': 'Invalid Signature'})
            send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '97', 'Message': 'Invalid Signature'},title=noti_title)
    else:
        print("đã vào  99")
#         transaction_user.vnp_responsecode = "Không thành công"
#         transaction_user.current_amount = app_user.amount
        result = JsonResponse({'RspCode': '99', 'Message': 'Invalid request'})
        # send_noti_by_token(AppUser.objects.filter(id = id).first(),send_message= {'RspCode': '99', 'Message': 'Invalid request'},title=noti_title)
    
    transaction_user.save()
    return result
          



@api_view(['GET'])
@authentication_classes([])
@permission_classes([])
def payment_return(request):
    inputData = request.GET
    if inputData:
        vnp = vnpay()
        vnp.responseData = inputData.dict()
        order_id = inputData['vnp_TxnRef']
        amount = int(inputData['vnp_Amount']) / 100
        order_desc = inputData['vnp_OrderInfo']
        id = int(order_desc)
        vnp_TransactionNo = inputData['vnp_TransactionNo']
        vnp_ResponseCode = inputData['vnp_ResponseCode']
        vnp_TmnCode = inputData['vnp_TmnCode']
        vnp_PayDate = inputData['vnp_PayDate']
        vnp_BankCode = inputData['vnp_BankCode']
        vnp_CardType = inputData['vnp_CardType']
        app_user = AppUser.objects.filter(id=id).first()
        
        if vnp.validate_response(settings.VNPAY_HASH_SECRET_KEY):
            # Check & Update Order Status in your Database
            # Your code here
            firstTimeUpdate = False
            if firstTimeUpdate == False:
                if vnp_ResponseCode == '00':
                    print("Payment Success. Your code implement here")
                    try:
                        payment_qs = Payment.objects.filter(vnp_txnref = order_id).first()
                        package_qs = Package.objects.get(id = payment_qs.vpn_package)
                        group_qs = Group.objects.filter(name = 'premium')
                        app_user.is_staff = True
                        app_user.groups.set(group_qs)
                        app_user.expired = datetime.now() + relativedelta(months=package_qs.duration)
                        
                        # create company 
                        company = Company.objects.create(company_name = f"Công ty của {app_user.full_name}")
                        department = Department.objects.create(company = company, department_name = "Tổng công ty")
                        app_user.department = department
                        app_user.save()
                        respone = 'Thành công'
                    except Exception as e:
                        respone = e
                    # noti = Notification.objects.create()
                    send_noti_by_token([AppUser.objects.filter(id = id).first()],send_message= {'content': 'Nạp tiền vào tài khoản thành công' ,'data_id':None, 'type_notification': None}, title=noti_title)
                    return render(request, 'payment_success.html')

                
                else:
                    print('Payment Error. Your code implement here')
                    return Response(data={'detail':'Payment Error'}, status= status.HTTP_503_SERVICE_UNAVAILABLE)
                    
 
                # Return VNPAY: Merchant update success
                firstTimeUpdate = True
                result = JsonResponse({'RspCode': '00', 'Message': 'Confirm Success'})
            else:
                # Already Update
                result = JsonResponse({'RspCode': '02', 'Message': 'Order Already Update'})
 
        else:
            # Invalid Signature
            result = JsonResponse({'RspCode': '97', 'Message': 'Invalid Signature'})
    else:
        result = JsonResponse({'RspCode': '99', 'Message': 'Invalid request'})

    return result
