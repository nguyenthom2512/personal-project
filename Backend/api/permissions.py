from django.contrib.auth.models import Group, Permission
from rest_framework import permissions
from datetime import date, datetime

def is_has_permission(user, code_name) -> bool:
    try:
        user_group = user.groups.get(name = 'premium')
        permissions = Permission.objects.get(codename = code_name.lower(), group = user_group.id)
        return True
    except Permission.DoesNotExist:
        return False
    except Exception as e:
        print('Except in permission.py', e)

class IsUserHasPermissionsOKR(permissions.BasePermission):
    
    def has_permission(self, request, view):
        user = request.user
        if is_has_permission(user = user ,code_name = 'add_okr'):
            if not user.expired:
                return False
            if user.expired >= date.today():
                return True
            else:
                return False
        return False
    
# class LoginPermission(permissions.BasePermission):
    
#     def has_permission(self, request, view):
        
#         return super().has_permission(request, view)
 
    