from authemail.models import EmailAbstractUser, EmailUserManager
from django.db import models
from django.db.models import Q
import datetime



############# ACCOUNT MODEL ################
class Role(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey("Company", on_delete=models.CASCADE, related_name='role_in_company')
    role_name = models.CharField(max_length=150)
    description = models.CharField(max_length=250, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = "Vị trí công việc"
        verbose_name = "Vị trí công việc"
        ordering = ('-id',)
    def __str__(self):
        return self.role_name
    
class Company(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    company_name = models.CharField(max_length=150)   
    
    class Meta:
        verbose_name_plural = "Công ty"
        verbose_name = "Công ty"
        ordering = ('-id',)
    def __str__(self):
        return self.company_name
    
class Department(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    department_name = models.CharField(max_length=150)
    company = models.ForeignKey(Company,on_delete=models.CASCADE, verbose_name = "Company",related_name='company_department')
    description = models.CharField(max_length=250, blank=True, null=True)
    
    class Meta:
        verbose_name_plural = "Phòng ban"
        verbose_name = "Phòng ban"
        ordering = ('-id',)
    def __str__(self):
        return self.department_name

class AppUser(EmailAbstractUser):
    # Gender
    GENDER_DEFINE = (
        (0, 'female'),
        (1, 'male'),
        (2, 'other')
    )
    LOGIN_TYPE = ((0, 'normal'), (1, 'fb'), (2,'gg'), (3,'apple'))
    # Custom fields
    date_of_birth = models.DateField('Date of birth', null=True, blank=True)
    fb_link = models.CharField(max_length=150,null=True, blank=True)
    phone_number = models.CharField(max_length=20,default="",blank=True, null=True)
    img_url= models.CharField(max_length=150,null=True, blank=True)
    role = models.ForeignKey(Role,on_delete=models.CASCADE, verbose_name = "Role",related_name='appuser_role',null=True, blank=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, related_name='user_department', blank=True, null=True)
    rate_sent = models.IntegerField(default=0)
    rate_received = models.IntegerField(default=0)
    device_id = models.CharField(max_length=250, blank=True,null=True)
    topic = models.CharField(max_length=250, blank=True, null=True)
    gender = models.IntegerField(default=0, choices=GENDER_DEFINE)
    full_name = models.CharField(max_length=250)
    total_rate = models.PositiveIntegerField(default=0)
    rate_month = models.PositiveIntegerField(default=0)
    diamond = models.PositiveIntegerField(default=0)
    login_type = models.PositiveSmallIntegerField(default=0, choices=LOGIN_TYPE)
    expired = models.DateField(blank=True,null=True)
    # Required
    objects = EmailUserManager()
    
    class Meta:
        verbose_name_plural = "Quản lý nhân viên"
        verbose_name = "Quản lý nhân viên"
        ordering = ('-id',)
        
 
############# OKR MODEL ################

class OKR(models.Model):
    user = models.ForeignKey(AppUser,on_delete=models.CASCADE, verbose_name = "AppUser",related_name='okr_user')       
    created_time = models.DateTimeField(auto_now_add=True)
    department = models.ForeignKey(Department,on_delete=models.CASCADE, verbose_name = "Department",related_name='okr_department')   
    okr_parent = models.ForeignKey("self",on_delete=models.CASCADE,null=True, blank=True)
    object_name = models.CharField(max_length=250)
    content = models.TextField(max_length=250, blank=True, null=True)
    is_done = models.BooleanField(default=False)
    confident = models.IntegerField(default=-1)
    okr_relate = models.ManyToManyField("self", blank=True, null=True, default=None, related_name='okr_related')
    challenge = models.BooleanField(default=False) 
    percent_changed = models.FloatField(default=0.0, blank=True, null=True)
    raic_user = models.ManyToManyField(AppUser, through= "LinkUser")
    result_parent = models.ForeignKey("Result", on_delete=models.CASCADE, related_name='result_okr_parent', blank=True, null=True)
    inspiration = models.BooleanField(default=False)
    user_muted = models.ManyToManyField(AppUser, blank=True, null=True, related_name="user_muted_okr")
    class Meta:
        verbose_name_plural = "OKR"
        verbose_name = "OKR"
        ordering = ('-id',)
    def __str__(self):
        return self.object_name    

    
class Unit(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    unit_name = models.CharField(max_length=50)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='unit_company')
    class Meta:
        verbose_name_plural = "Đơn vị tính"
        verbose_name = "Đơn vị tính"
        ordering = ('-id',)
    def __str__(self):
        return self.unit_name        
    
class Result(models.Model):
    user = models.ForeignKey(AppUser,on_delete=models.CASCADE, verbose_name = "AppUser",related_name='result_user')
    created_time = models.DateTimeField(auto_now_add=True)
    okr = models.ForeignKey(OKR,on_delete=models.CASCADE,verbose_name = "OKR",related_name='okr_result')
    key_result =  models.CharField(max_length=250)
    target = models.BigIntegerField()
    current_done = models.IntegerField()
    unit = models.ForeignKey(Unit,on_delete=models.CASCADE, verbose_name = "Unit",related_name='unit')   
    plan_url = models.CharField(max_length=250,null=True, blank=True)
    result_url =  models.CharField(max_length=250,null=True, blank=True)
    deadline = models.DateTimeField()
    percent_changed = models.FloatField(default=0.0, blank=True, null=True)
    root_result = models.ForeignKey('Result', on_delete=models.CASCADE, blank=True, null=True)
    is_done = models.BooleanField(default=False)
    class Meta:
        verbose_name_plural = "Result"
        verbose_name = "Result"
        ordering = ('-id',)
    def __str__(self):
        return self.key_result
    
class Checkin(models.Model):
    user = models.ForeignKey(AppUser,on_delete=models.CASCADE, verbose_name = "AppUser",related_name='checkin_user')
    created_time = models.DateTimeField(auto_now_add=True)
    okr = models.ForeignKey(OKR,on_delete=models.CASCADE,verbose_name = "OKR",related_name='ok_checkin')
    checkin_date = models.DateTimeField(blank=True, null=True)
    checkin_status = models.IntegerField(default=-1)
    checkin_result = models.ManyToManyField(Result, through='CheckinDetail')
    is_done = models.BooleanField(default=False)
   
class Meta:
    verbose_name_plural = "Checkin"
    verbose_name = "Checkin"
    ordering = ('-id',)

class CheckinDetail(models.Model):
    checkin = models.ForeignKey(Checkin, on_delete=models.CASCADE,related_name='checkindetail_id')
    result = models.ForeignKey(Result, on_delete=models.CASCADE,related_name='checkindetail_result')
    total_done =  models.IntegerField()
    confident = models.IntegerField(default=-1)
    process_note = models.CharField(max_length=250)
    overdue_note = models.CharField(max_length=250)
    trouble_note = models.CharField(max_length=250)
    solution_note = models.CharField(max_length=250)
    is_done = models.BooleanField(default=False)
    
#**********************************************************************************************    
#TODO LIST 
    
class Tag(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete= models.CASCADE, related_name="user_tag")
    tag_name = models.CharField(max_length=250)
    tag_code = models.CharField(max_length=250)
    class Meta:
        verbose_name_plural = "Nhãn công việc"
        verbose_name = "Nhãn công việc"
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.tag_name
    
class Priority(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name="user_priority")
    priority_level = models.IntegerField(blank=True, null=True)
    priority_name = models.CharField(max_length=250)
    color = models.CharField(max_length=20)
    class Meta:
        verbose_name_plural = "Ưu tiên công việc"
        verbose_name = "Ưu tiên công việc"
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.priority_name
class Todo(models.Model):
    CONFIDENT_CHOICES =((0,'bad'),(1,'normal'), (2,'good'), (3,"very good"))
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name="user_todo")
    result = models.ForeignKey(Result, on_delete=models.CASCADE, related_name="result_todo", blank=True, null=True)
    todo_name = models.CharField(max_length=250)
    description = models.CharField(max_length=250, blank=True, null=True)
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    priority = models.ForeignKey(Priority, on_delete=models.CASCADE, related_name="priority_todo")
    confident = models.IntegerField(default=-1, choices=CONFIDENT_CHOICES)
    is_done = models.BooleanField(default=False)
    tag = models.ManyToManyField(Tag, blank=True, null=True, related_name="tag_list")
    raic_user = models.ManyToManyField(AppUser, through= "LinkUser")
    img_url = models.CharField(max_length=250, blank=True, null=True)
    date = models.DateField(blank=True,null=True)
    
    class Meta: 
        verbose_name_plural = "Todo"
        verbose_name = "Todo"
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.todo_name        

class TodoCheckList(models.Model):
    todo = models.ForeignKey(Todo, on_delete= models.CASCADE, related_name="check_list")
    content = models.CharField(max_length=250)
    is_done = models.BooleanField(default=False)
class Comment(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name="user_comment")
    title = models.TextField(blank=True, null=True)
    todo = models.ForeignKey(Todo, on_delete=models.CASCADE, related_name="todo_comment", blank=True, null=True)
    challenge = models.ForeignKey("Challenge", on_delete=models.CASCADE,blank=True, null=True, related_name='challenge_comment')
    
    class Meta:
        verbose_name_plural = "Comment"
        verbose_name = "Comment"
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.title
    
class LinkUser(models.Model):
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name="user_linkuser")
    okr = models.ForeignKey(OKR, on_delete= models.CASCADE, related_name="raic_user_okr", blank=True, null=True)
    todo = models.ForeignKey(Todo, on_delete=models.CASCADE, related_name="raic_user_todo", blank=True, null=True)
    raic_type = models.IntegerField(default=0)
    class Meta:
        verbose_name_plural = "RAIC User"
        verbose_name = "RAIC User"
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.user.email

##### CHALLENGE & INSPIRATION #####
class Challenge(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name='user_challenge')
    okr = models.ForeignKey(OKR, on_delete=models.CASCADE,related_name='challenge_okr')
    challenged_user = models.ManyToManyField(AppUser, blank=True, null=True)
    challenged_department = models.ManyToManyField(Department, blank=True, null=True)
    challenged_company = models.ForeignKey(Company,on_delete=models.CASCADE, blank=True, null=True)
    public = models.BooleanField(default=False)
    content = models.CharField(max_length=250, blank=True, null=True)
    class Meta:
        verbose_name_plural = "Thách thức"
        verbose_name = "Thách thức"
        ordering = ('-id',)
    def __str__(self) -> str:
        return f"(Challenge {self.okr.object_name})"

class Inspiration(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    okr = models.ForeignKey(OKR, on_delete=models.CASCADE, related_name="inspiration_okr")
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name="user_create_inspiration")
    user_do = models.ManyToManyField(AppUser, blank=True, null=True)
    quiz = models.ForeignKey("Quiz", on_delete=models.CASCADE, related_name='quiz_inspiration')
    class Meta:
        verbose_name_plural = "Cảm hứng"
        verbose_name = "Cảm hứng"
        ordering = ('-id',)
    def __str__(self) -> str:
        return "Inspiration of " + self.okr.object_name

#### FEEDBACK ####
class Criteria(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='creteria_company')
    title = models.CharField(max_length=250)
    rate = models.PositiveIntegerField(default=0)
    class Meta:
        verbose_name_plural = "Tiêu chí đánh giá"
        verbose_name = "Tiêu chí đánh giá" 
        ordering = ('-id',)
    def __str__(self) -> str:
        return f"{self.title} ({self.rate})"       
class Feedback(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name='user_feedback')
    user_received = models.ForeignKey(AppUser, on_delete=models.CASCADE,related_name='user_receive_feedback')
    okr = models.ForeignKey(OKR, on_delete=models.CASCADE, related_name="feedback_okr", blank=True, null=True)
    criteria = models.ForeignKey(Criteria, on_delete= models.CASCADE, related_name="feedback_criteria", blank=True, null=True)
    content = models.CharField(max_length=250)
    quantity = models.PositiveSmallIntegerField(default=0, blank=True, null=True)
    class Meta:
        verbose_name_plural = "Phản hồi"
        verbose_name = "Phản hồi"
        ordering = ('-id',)
    def __str__(self) -> str:
        return f"{self.user.last_name} phan hoi toi {self.user_received.last_name}"

#? QUIZ SYSTEM
class Question(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    content = models.CharField(max_length=250)
    question_type = models.IntegerField(default=0)  
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="company_question")
    duration = models.IntegerField(default=0)
    class Meta:
        verbose_name_plural = 'Quản lý câu hỏi'
        verbose_name = 'Quản lý câu hỏi'
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.content

class QuestionAttachment(models.Model):
    file_url = models.CharField(max_length=250)
    file_type = models.IntegerField(default=0)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='attachment_question')
    def __str__(self) -> str:
        return self.file_url

class Answer(models.Model):
    content = models.CharField(max_length=250)
    weight = models.IntegerField(default=1)
    user = models.ForeignKey(AppUser, on_delete= models.CASCADE, related_name="user_create_answer")
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name="answers")

class Quiz(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=250)
    description = models.CharField(max_length=250, blank=True, null=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name="company_quiz")
    duration = models.IntegerField(default=0)
    total_point = models.IntegerField(default=0)
    questions = models.ManyToManyField(Question, through='QuestionQuiz')
    class Meta:
        verbose_name_plural = 'Quản lý bộ câu hỏi'
        verbose_name = 'Bộ câu hỏi'
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.title

class QuestionQuiz(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='quiz_for_question')
    question = models.ForeignKey(Question, on_delete= models.CASCADE, related_name='question_of_quiz')
    point = models.IntegerField(default=0)
    
class UserQuiz(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='user_quiz')
    user = models.ForeignKey(AppUser, on_delete= models.CASCADE, related_name='user_do_quiz')
    inspiration = models.ForeignKey(Inspiration, on_delete=models.CASCADE, related_name='quiz_inspiration',blank=True, null=True)
    total_point = models.IntegerField(default=0)
    is_end = models.BooleanField(default=False)
    
    # trigger
    class Meta:
        verbose_name_plural = 'Người trả lời'
        verbose_name = 'Người trả lời'
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.user.last_name + ' ' + self.user.first_name

class UserQuestion(models.Model):
    user_quiz = models.ForeignKey(UserQuiz, on_delete= models.CASCADE,related_name='user_quiz_question')
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='user_question')
    answer = models.CharField(max_length=250)
    point = models.IntegerField(default=0)
    
    # trigger
    def __str__(self) -> str:
        return self.question.content
    
    
####### FILE #######
class File(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser,blank=True,null=True,on_delete=models.CASCADE)
    file = models.FileField(upload_to='media', blank=True)

####### MEETING ######
class Room(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    checkin = models.OneToOneField(Checkin, on_delete=models.CASCADE, related_name='okr_room', blank=True, null=True)
    room_url = models.CharField(max_length=250)
    room_id = models.CharField(max_length=250)
    room_name = models.CharField(max_length=250)
    user_relate = models.ManyToManyField(AppUser, blank=True, null=True)
    password = models.CharField(max_length=250, blank=True, null=True)
    start_time = models.DateTimeField()
    okr_id = models.CharField(max_length=250, blank=True, null=True)
    class Meta:
        verbose_name_plural = "Phòng họp"
        verbose_name = "Phòng họp"
        ordering = ('-id',)
    def  __str__(self) -> str:
        return self.room_name 
class Notification(models.Model):
    
    NOTIFICATION_FOR =(
        (0,'challenge'),
        (1,'inspiration'),
        (2,'detail_okr'),
        (3,'call') ,
        (4,'detail_checkin'),
        (5,'feedback'),
        (6,'user_do_quiz'),
    )
    
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name='notification_to_user')
    user_sent =  models.ForeignKey(AppUser, on_delete= models.CASCADE, related_name= 'user_sent_noti', blank=True, null=True)
    title = models.CharField(max_length=250)
    content = models.CharField(max_length=250)
    data_id = models.PositiveIntegerField(default=0)
    is_read = models.BooleanField(default=False)
    type_notification = models.IntegerField(choices= NOTIFICATION_FOR, default=-1)
    okr_id = models.CharField(max_length=250, blank=True, null=True)
    # objects = NotificationMange()
    class Meta:
        verbose_name_plural = 'Thông báo'
        verbose_name = 'Thông báo'
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.title
    
##### PRESENT #####

class Present(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    present_name = models.CharField(max_length=250)
    img_url = models.CharField(max_length=250)
    price = models.PositiveIntegerField(default=0)
    title = models.CharField(max_length=250)
    subscription = models.TextField(blank=True, null=True)
    material = models.CharField(max_length=250)
    class Meta:
        verbose_name_plural = 'Quà tặng'
        verbose_name  = 'Quà tặng'
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.present_name

class ChangePresent(models.Model):
    created_time = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(AppUser, on_delete=models.CASCADE, related_name='user_change_present')
    present = models.ForeignKey(Present, on_delete=models.CASCADE, related_name='present_change')
    quantity = models.PositiveSmallIntegerField()
    is_receive = models.BooleanField(default=False)
    
    class Meta:
        verbose_name_plural = 'Đổi quà'
        verbose_name = 'Đổi quả'
        ordering = ('-id',)
    def __str__(self) -> str:
        return self.user.full_name

class Master(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='master_company')
    ratio_convert_diamond = models.FloatField(default=0, blank=True, null=True)
    
####### LICENSE ######

class Package(models.Model):
    MONTH_CHOICES = ((1, '1 months'),(3, '3 months'), (6, '6 months'))
    
    created_time = models.DateTimeField(auto_now_add=True)
    package_name = models.CharField(max_length=250)
    duration = models.IntegerField(default=0, choices=MONTH_CHOICES)
    price = models.PositiveIntegerField(default=0,)
    description = models.CharField(max_length=250, blank=True, null=True)
    is_recommend = models.BooleanField(default=False)
    class Meta:
        verbose_name_plural = 'Gói'
        verbose_name = 'Gói'
    
    def __str__(self) -> str:
        return self.package_name
    

class Transaction(models.Model):
    TYPE = (
    (0, 'Nộp tiền VNP (+)'),
    (1, 'Trừ cước cuộc gọi (-)'),
    (2, 'Cộng cước cuộc gọi (+)'),
    (3, 'Nộp tiền mặt(+)'),
    (4, 'Rút tiền mặt(-)')
    )
    CHANNELS = (
    (0, 'Số dư ví '),
    (1, 'VNPAY'),
    (2, 'Giao dịch ngoài')
    )

    channels = models.IntegerField(choices=CHANNELS, verbose_name='Kênh thanh toán', default=0)
    user = models.ForeignKey(AppUser,blank=True,null=True,on_delete=models.CASCADE)
    amount= models.IntegerField(verbose_name='Số tiền phát sinh',default=0 )
    title = models.CharField(max_length=200, blank=True,null=True)
    order_id = models.CharField(max_length=200,blank=True,null=True)
    order_desc = models.CharField(max_length=200, blank=True,null=True)
    vnp_transactionno = models.CharField(max_length=200, blank=True,null=True)
    vnp_responsecode = models.CharField(max_length=200, blank=True,null=True, verbose_name='Trạng thái giao dịch' )
    vnp_tmncode = models.CharField(max_length=200, blank=True,null=True)
    vnp_paydate = models.CharField(max_length=200, blank=True,null=True)
    vnp_bankcode = models.CharField(max_length=200, blank=True,null=True)
    vnp_cardtype = models.CharField(max_length=200, blank=True,null=True)
    # total_income = models.FloatField(default=0,blank=True,null=True, verbose_name = "Cước thu nhập")
    #Type = 0 - Nap tien, Type = 1 : Call
#     total_income = models.FloatField(default=0,blank=True,null=True, verbose_name = "")
    current_total_income = models.FloatField(default=0,blank=True,null=True, verbose_name = "Số tiền lũy kế")
    type = models.IntegerField(choices=TYPE, verbose_name='Loại cước phí')
    current_amount = models.IntegerField(default=0,  verbose_name = "Số tiền ví")
    description = models.TextField(blank=True,null=True)
    created_time = models.DateTimeField(auto_now_add=True,verbose_name='Ngày phát sinh giao dịch' )
    check_call = models.IntegerField(default=0, blank=False, null=False)
    class Meta:
        verbose_name_plural = "Lịch sử giao dịch tài khoản ví"
        ordering = ['-created_time']
    def __str__(self):
        return str(self.user)
    
    def STT(self):
        # Return the index position of this instance with regards to the sorted queryset
        return list(Transaction.objects.filter(Q(type=0) | Q(type=1) | Q(type=3), user_id__isnull=False).values_list('pk', flat=True)).index(self.pk) + 1
    
    def STTINCOME(self):
        # Return the index position of this instance with regards to the sorted queryset
        return list(Transaction.objects.filter((Q(type=2) | Q(type=4)) & Q(user__type=1)).values_list('pk', flat=True)).index(self.pk) + 1
    def STTREPORT(self):
        # Return the index position of this instance with regards to the sorted queryset
        return list(Transaction.objects.values_list('pk', flat=True)).index(self.pk) + 1
    STTINCOME.short_description = "STT"
    STTREPORT.short_description = 'STT'
    
class Payment(models.Model):
    vnp_version= models.CharField(max_length=200, blank=True,null=True)
    vnp_command = models.CharField(max_length=200, blank=True,null=True)
    vnp_tmncode= models.CharField(max_length=200, blank=True,null=True)
    vnp_locale = models.CharField(max_length=200, blank=True,null=True)
    vnp_currcode = models.CharField(max_length=200, blank=True,null=True)
    vnp_txnref = models.CharField(max_length=200, blank=True,null=True)
    vnp_orderinfo = models.CharField(max_length=200, blank=True,null=True)
    vnp_amount =models.CharField(max_length=200, blank=True,null=True)
    vnp_returnurl = models.CharField(max_length=200, blank=True,null=True)
    vnp_ipaddr = models.CharField(max_length=200, blank=True,null=True)
    vnp_createdate = models.CharField(max_length=200, blank=True,null=True)
    vnp_orderType = models.CharField(max_length=200, blank=True,null=True)
    vnp_locale = models.CharField(max_length=200, blank=True,null=True)
    vnp_bankcode = models.CharField(max_length=200, blank=True,null=True)
    vpn_package = models.CharField(max_length=200, blank=True,null=True)

