from rest_framework import serializers
from api.models import File

class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = "__all__"
    
    def create(self, validated_data):
        file = File.objects.create(file=self.request.data.get('file'))
        return file