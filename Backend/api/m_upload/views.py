from rest_framework.response import Response
from rest_framework import views, status, parsers, decorators
from api.models import File
from .serializers import FileSerializer
class FileUploadView(views.APIView):
    parser_class = (parsers.FileUploadParser,)

    def post(self, request):
        file_obj = request.data.get('file', None)
       
        if file_obj:
            file_upload = File.objects.create(file=file_obj)
            serializer = FileSerializer(file_upload)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response({"message": "File is fail"}, status=status.HTTP_400_BAD_REQUEST)