from rest_framework import generics, filters
from api.models import Checkin, Department, Feedback, Inspiration, UserQuiz
import datetime
from api.utils import TypePatternFormat as parttern
from django.db.models import Q

class CheckinDepartmentFilter(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        department = request.query_params.get('department',None)
        if department:
            queryset = Checkin.objects.filter(okr__department=department)
        return queryset

class FilterByUserRequest(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        if request.user.pk:
            return queryset.filter(user_id = request.user.id)
        return {}

class FilterCreatedTimeByDateRange(filters.BaseFilterBackend):
    """
    Filter DateTimeField with date
    """
    def filter_queryset(self, request, queryset, view):
        created_time__range = request.query_params.get('created_time__range', None)
        if created_time__range:
            created_time__range = created_time__range.split(',')
            created_time__range = [datetime.datetime.strptime(item, parttern.date_format_pattern) for item in created_time__range]
            return queryset.filter(created_time__date__range = created_time__range ) 
        return queryset

class FilterDepartmentInCompanyByUserRequest(filters.BaseFilterBackend):
    """
    Filter department in company by user request
    """
    def filter_queryset(self, request, queryset, view):
        if request.user.pk and request.user.department:
                company = request.user.department.company
                queryset = queryset.filter(company = company)
                return queryset
        return {}

class ExcludeUserInDepartment(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        exclude_department = request.query_params.get('exclude_department', None)
        if exclude_department:
            exclude_department = exclude_department.split(',')
            exclude_filter = [int(department) for department in exclude_department]
            queryset = queryset.exclude(Q(department_id__in = exclude_filter)).filter(department__isnull = False)
        return queryset
    
class FilterUserInCompanyByUserRequest(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.pk and request.user.department:
            queryset = queryset.filter(department__company = request.user.department.company)
            return queryset
        return {}
    
class FilterByCompany(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.pk and request.user.department:
            queryset = queryset.filter(okr__department__company = request.user.department.company)
            return queryset
        return {}

class FilterCompanyByUser(filters.BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        if request.user.pk and request.user.department:
            queryset = queryset.filter(id = request.user.department.company.id)
            return queryset
        return {}

class FilterFeedbackListByUser(filters.BaseFilterBackend):
    def  filter_queryset(self, request, queryset, view):
        if request.user.pk:
            queryset = Feedback.objects.filter(user = request.user) | Feedback.objects.filter(user_received = request.user)
            return queryset.order_by('-created_time')
        return {}