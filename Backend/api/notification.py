import json
import requests
from firebase_admin import messaging

def send_noti_by_token(to_user, send_message, title ,*args):
    registration_tokens = [str(user.device_id) for user in to_user if user.device_id]
    # See documentation on defining a message payload.
    if len(registration_tokens) != 0:
        message = messaging.MulticastMessage(
            notification=messaging.Notification(title= title),
            data={   
                'title' : title,
                'content' : send_message['content'],
                'data_id' : str(send_message['data_id']),
                'type_notification': str(send_message['type_notification']),
            },
            tokens = registration_tokens
        )

        # Send a message to the device corresponding to the provided
        # registration token.
        try:
            response = messaging.send_multicast(message)
            # Response is a message ID string.
        except Exception as e:
            print('\nEXCEPTION... EX \n')
            print(e)
        # response = messaging.send(message)
        # # Response is a message ID string.
        # print('Successfully sent message:', response)

def send_noti_by_topic(topic, send_message,title, *args):
    if topic:
        message = messaging.Message(
            notification=messaging.Notification(title= title),
            data={   
                'title' : title,
                'content' : send_message['content'],
                'data_id' : str(send_message['data_id']),
                'type_notification': str(send_message['type_notification']),
            },
            topic = topic
        )

        # Send a message to the device corresponding to the provided
        # registration token.
        try:
            response = messaging.send(message)
        except Exception as e:
            print('EXCEPTION: ',e)