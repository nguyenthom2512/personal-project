from django.db import models
from django.db.models.aggregates import Count
from rest_flex_fields.serializers import FlexFieldsModelSerializer
from rest_framework import fields, serializers
from rest_framework.exceptions import ValidationError
from rest_framework.utils.serializer_helpers import ReturnDict
from api.models import OKR, AppUser, Challenge, Checkin, CheckinDetail, Comment, Inspiration, LinkUser, Notification, Result, Room, Unit,Company, Department
from django.db.models import Sum 
from django.db import transaction
from api.m_account.serializers import UserListSerializer, UserSerializers
from api.utils import NotificationBody as message
from api.notification import send_noti_by_token, send_noti_by_topic


class LinkUserSerializer(FlexFieldsModelSerializer):
    user_data = serializers.SerializerMethodField('get_user_data')
    class Meta:
        model = LinkUser
        fields = '__all__'
    def get_user_data(self, obj):
        user = AppUser.objects.get(id = obj.user.id)
        data ={
            "id": user.pk,
            "full_name": user.last_name + " " + user.first_name,
            "img_url": user.img_url,
        }
        return data
class UnitSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Unit
        fields = '__all__'
class ResultSerializer(FlexFieldsModelSerializer):  
    # unit = UnitSerializers()
    unit_name = serializers.SerializerMethodField('get_unit_name')
    processed = serializers.SerializerMethodField('get_processed')
    
    class Meta:
        model = Result
        fields = '__all__'
  
    def validate(self, data):
        """
        Check that if user request is differences with user.
        """
        request_user = data.get('user')
        self_user = self.context['request'].user
        if request_user.id != self_user.id:
            raise serializers.ValidationError({"message": "user is different token"})
        return super().validate(data)
    
    def get_unit_name(self,obj):
        unit = Unit.objects.get(id = obj.unit_id)
        if unit is not None:
            return unit.unit_name
        return None
    def get_processed(self, obj):
        if obj.target == 0:
            return obj.current_done
        return obj.current_done/obj.target
    def create(self, validated_data):
        okr_qs = validated_data.get('okr', None)
        with transaction.atomic():
            result = super().create(validated_data)
            if okr_qs.result_parent:
                result.root_result = okr_qs.result_parent.root_result
            result.root_result = result
            result.save()
            return result
    def update(self, instance, validated_data): 
        unit = validated_data.get('unit',None)
        okr = validated_data.get('okr', None)
        target = validated_data.get('target',None)
        current_done = validated_data.get('current_done',0)
        with transaction.atomic():
            if target:
                instance.percent_changed = current_done/target
            result = super().update(instance, validated_data)
            if instance.unit != unit:
                Result.objects.filter(root_result = instance.id).update(unit = unit)
            result_qs = Result.objects.filter(okr = okr)
            total_target = 0
            total_done = 0
            for result in result_qs:
                total_target += result.target
                total_done += result.current_done
            okr.percent_changed = total_done/total_target
            okr.save()
        return result

class ResultSpecialSerializer(FlexFieldsModelSerializer): #used for creating okr without okr field in result model
    processed = serializers.SerializerMethodField("get_processed")
    confident = serializers.SerializerMethodField("get_confident")
    unit_name = serializers.SerializerMethodField('get_unit_name')
    target_remain = serializers.SerializerMethodField('get_target_remain')
    class Meta:
        model = Result
        fields = ("id","user", "key_result","target","current_done","unit_name","unit", "plan_url", "result_url", "deadline","processed","percent_changed","confident",'target_remain')
    def get_confident(self, obj):
        checkin_detail = CheckinDetail.objects.filter(result_id = obj.id)
        if checkin_detail.count() != 0:
            return checkin_detail[::-1][0].confident
        return None
    def get_processed(self, obj):
        return obj.current_done/obj.target if obj.target != 0 else 0
    def get_unit_name(self,obj):
        return Unit.objects.get(id = obj.unit_id).unit_name
    def get_target_remain(self, obj):
        return obj.target - obj.current_done

class CheckinDetailSerializer(FlexFieldsModelSerializer):
    result_data = serializers.SerializerMethodField('get_result_data')
    class Meta:
        model = CheckinDetail
        fields = ("id","total_done","confident","process_note","overdue_note","trouble_note","solution_note","is_done","result","result_data")
    def get_result_data(self, obj):
        result_qs = Result.objects.filter(id = obj.result.id)
        return ResultSpecialSerializer(result_qs, many = True).data
class CheckinSerializer(FlexFieldsModelSerializer):
    checkin_result = CheckinDetailSerializer(many = True, allow_null = True, source ="checkindetail_id")
    confident = serializers.CharField(write_only = True)
    
    class Meta:
        model = Checkin
        fields = '__all__'
        odering = ('-id',)
    def __init__(self, *args, **kwargs):
        # Instantiate the superclass normally
        super(CheckinSerializer, self).__init__(*args, **kwargs)

        user_request = self.context.get('request',None)
        if user_request is not None:
            fields = str(user_request._request.path).split('/')
            if 'report' in fields:
                self.fields.pop('checkin_result')
    def create(self, validated_data):
        checkin_result = validated_data.pop('checkindetail_id', None)
        confident = validated_data.pop('confident', -1)
        request_user = validated_data.get('user')
        self_user = self.context['request'].user
        target = 0
        done = 0
        details = []
        results = []
        notis = []
        users = []
        prev_user = [self_user.id]
        if request_user.id == self_user.id:
            with transaction.atomic():
                checkin = super(CheckinSerializer, self).create(validated_data)
                for detail in checkin_result:
                    item = CheckinDetail(**detail, checkin_id = checkin.id)
                    details.append(item)
                    if item.result:
                        result = Result.objects.get(id = item.result_id)
                    if not item.result.is_done:
                        result.is_done = item.is_done
                        result.current_done =  result.current_done + detail['total_done']
                        done += result.current_done
                    result.percent_changed = round(result.current_done/result.target - result.percent_changed,2)
                    results.append(result)
                    target += result.target
                CheckinDetail.objects.bulk_create(details)
                Result.objects.bulk_update(results,['current_done','percent_changed','is_done'])
                
                okr = OKR.objects.get(id = checkin.okr_id)
                if checkin.is_done == True:
                    okr.is_done = True
                okr.confident = int(confident)
                okr.percent_changed = round(done/target - okr.percent_changed, 2) 
                okr.save()

                #create notification and push
                body_message = message.noti_body(data_id=checkin.id, content=f'đã check in cho okr {okr.object_name}', type_notification=4, okr_id= okr.id)
                raic_user = LinkUser.objects.filter(okr = okr)
                for user_obj in raic_user:
                    if user_obj.user not in list(okr.user_muted.all()):
                        if not user_obj.user.id in prev_user:
                            item = Notification(**body_message, user = user_obj.user, user_sent = self_user)
                            users.append(user_obj.user)
                            notis.append(item)
                            prev_user.append(user_obj.user.id) 
                Notification.objects.bulk_create(notis)
                send_noti_by_token(to_user= users,send_message= body_message, title=f"{self_user.full_name} đã check in cho okr {okr.object_name}")
                return checkin
        else:
            raise serializers.ValidationError({"message": "[create checkin] user is different token"})
            
class OKRListSerializer(FlexFieldsModelSerializer):
    
    okr_result = ResultSerializer(many=True,allow_null=True )
    count_result = serializers.SerializerMethodField('get_count_result')
    percent_completed = serializers.SerializerMethodField('get_percent_completed')
    user = UserListSerializer()
    owner_okr = serializers.SerializerMethodField('get_owner_okr')
    count_child = serializers.SerializerMethodField('get_count_child')
    last_checkin = serializers.SerializerMethodField('get_last_checkin')
    is_match = serializers.SerializerMethodField('get_is_match')
    
    class Meta:
        model = OKR
        fields = ('id','created_time','object_name','owner_okr','count_result','percent_changed','confident','percent_completed','count_child','okr_result','user','last_checkin','is_match','is_done','result_parent')
    def get_count_result(self, obj):
        return obj.okr_result.count()    
    def get_percent_completed(self, obj):
        sum_obj = obj.okr_result.all().aggregate(Sum('current_done'),Sum('target'))
        if sum_obj['target__sum'] is None or sum_obj['target__sum'] == 0:
            return 0
        return round(sum_obj['current_done__sum']/sum_obj['target__sum'],2)
    def get_owner_okr(self, obj):
        return f"[{obj.user.last_name} {obj.user.first_name}] {obj.object_name}"
    def get_count_child(self, obj):
        return OKR.objects.filter(okr_parent = obj.id).count()
    def get_last_checkin(self, obj):
        check_in = Checkin.objects.filter(okr_id = obj.id)
        if check_in.count()!=0:
            last_checkin = check_in.latest('created_time')
            return {'checkin_date' : last_checkin.checkin_date, 'checkin_status' : last_checkin.checkin_status }
        return None
    def get_is_match(self, obj):
        total_parent_target_qs = obj.okr_result.all().aggregate(Sum('target'))
        total_parent_target_num = total_parent_target_qs['target__sum'] if total_parent_target_qs['target__sum'] else 0
        total_child_target = 0
        okr_child = OKR.objects.filter(okr_parent_id = obj.id)
        if okr_child.count() == 0:
            return True
        for obj in okr_child:
            sum_result_qs = Result.objects.filter(okr_id = obj.id).aggregate(Sum('target'))
            total_child_target += sum_result_qs['target__sum'] if sum_result_qs['target__sum'] else 0
        return False if total_parent_target_num != total_child_target else True
                
class OKRRelateSerializer(FlexFieldsModelSerializer):
    count_result = serializers.SerializerMethodField('get_count_result')
    okr_result = ResultSpecialSerializer(many = True, allow_null = True)
    last_checkin = serializers.SerializerMethodField('get_last_checkin')
    percent_completed = serializers.SerializerMethodField('get_percent_completed')
    user = UserListSerializer()

    class Meta:
        model = OKR
        fields = ('id','created_time','object_name','count_result','percent_changed','confident','percent_completed','okr_result','user','last_checkin')
    def get_count_result(self, obj):
        okr_result = Result.objects.filter(okr_id = obj.id)
        return okr_result.count()  
    def get_percent_completed(self, obj):
        sum_obj = obj.okr_result.all().aggregate(Sum('current_done'),Sum('target'))
        if sum_obj['target__sum'] is None or sum_obj['target__sum'] == 0:
            return 0
        return round(sum_obj['current_done__sum']/sum_obj['target__sum'],2)
    def get_last_checkin(self, obj):
        check_in = Checkin.objects.filter(okr_id = obj.id)
        if check_in.count()!=0:
            last_checkin = check_in.latest('created_time')
            return {'checkin_date' : last_checkin.checkin_date, 'checkin_status' : last_checkin.checkin_status }
        return None

class OKRDetailSerializer(FlexFieldsModelSerializer):
    okr_result = ResultSpecialSerializer(many=True,allow_null=True)
    ok_checkin = serializers.SerializerMethodField('get_ok_checkin')
    raic_user  = LinkUserSerializer(many=True,allow_null=True,source="raic_user_okr")
    percent_completed = serializers.SerializerMethodField('get_percent_completed')
    user_data = serializers.SerializerMethodField('get_user_data')
    class Meta: 
        model = OKR
        fields = '__all__'
    def get_ok_checkin(self, obj):
        checkin_qs = Checkin.objects.filter(okr_id = obj.id).order_by('-id',)
        return CheckinSerializer(checkin_qs, many = True).data
    def get_percent_completed(self, obj):
        sum_obj = obj.okr_result.all().aggregate(Sum('current_done'),Sum('target'))
        if sum_obj['target__sum'] is None or sum_obj['target__sum'] == 0:
            return 0
        return round(sum_obj['current_done__sum']/sum_obj['target__sum'],2)
    def get_user_data(self, obj):
        return {'full_name': obj.user.full_name, 'img_url': obj.user.img_url}


    def validate(self, data):
        self_user = self.context['request'].user
        user = data.get('user')
        if self_user != user:
            raise serializers.ValidationError({'detail':'user token is different user create'})
        return super().validate(data)
    
    def create(self, validated_data):
        okr_result = validated_data.pop('okr_result')
        raic_user = validated_data.pop('raic_user_okr')
        request_user = validated_data.get('user')
        self_user = self.context['request'].user
        results = []
        users_raic = []
        users_noti = []
        list_user = [self_user]
        with transaction.atomic():
            okr = super(OKRDetailSerializer, self).create(validated_data)
            # okr.save()
            body_message = message.noti_body(data_id = okr.id,content= f'đã thêm vai trò cho bạn vào 1 OKR',type_notification=2, okr_id=okr.id)
            for result in okr_result:
                # item = Result(**result,okr=okr)
                item = Result(**result, okr_id = okr.id)
                results.append(item)
            Result.objects.bulk_create(results)
            for user in raic_user:
                item = LinkUser(**user, okr_id = okr.id)
                users_raic.append(item)
                if not user['user'].id in list_user:
                    item_noti = Notification(**body_message, user = user['user'], user_sent = self_user)
                    users_noti.append(item_noti)
                    list_user.append(user['user'])
            LinkUser.objects.bulk_create(users_raic)
            Notification.objects.bulk_create(users_noti)
            send_noti_by_token(to_user= list_user,send_message= body_message, title= f'{self_user.full_name}đã thêm vai trò cho bạn vào 1 OKR')
        return okr
        
    def update(self, instance, validated_data):
        self_user = self.context['request'].user
        okr_result = validated_data.pop('okr_result',None)
        raic_user = validated_data.pop('raic_user_okr',None)
        Result.objects.filter(okr = instance.id).delete()
        LinkUser.objects.filter(okr = instance.id).delete()
        results = []
        users_raic =[]
        users_noti = []
        list_user = [self_user]
        with transaction.atomic():
            for attr, value in validated_data.items():
                if attr == 'okr_result':
                    continue
                if attr == 'okr_relate':
                    instance.okr_relate.set(value)
                    continue
                if attr == 'user_muted':
                    continue
                setattr(instance, attr, value)
            instance.save()
            body_message = message.noti_body(data_id = instance.id,content= f'đã chỉnh sửa {instance.object_name}',type_notification=2, okr_id=instance.id)
            
            if okr_result is not None:
                for result in okr_result:
                    item = Result(**result, okr_id = instance.id)
                    results.append(item)
                Result.objects.bulk_create(results)
            if raic_user is not None:
                for user in raic_user:
                    item = LinkUser(**user, okr_id = instance.id)
                    users_raic.append(item)
                    if user['user'] not in list(instance.user_muted.all()):
                        if user['user'] not in list_user: 
                            item_noti = Notification(**body_message, user = user['user'], user_sent = self_user )
                            users_noti.append(item_noti)
                            list_user.append(user['user'])
                LinkUser.objects.bulk_create(users_raic)
                Notification.objects.bulk_create(users_noti)
                send_noti_by_token(to_user= list_user,send_message= body_message, title= f'{self_user.full_name}đã chỉnh sửa {instance.object_name}')
            return instance

class RoomSerializers(FlexFieldsModelSerializer):
    okr_id = serializers.IntegerField(allow_null = True)
    class Meta: 
        model = Room
        fields = '__all__' 
    def create(self, validated_data):
        self_user = self.context['request'].user
        okr_id = validated_data.get('okr_id',None)
        okr = OKR.objects.get(id = okr_id)
        prev_user = []
        users = [self_user]
        if self_user.id == okr.user.id:        
            raic_user = LinkUser.objects.filter(okr_id = okr_id)
            for obj in raic_user:
                if obj.user not in list(okr.user_muted.all()):
                    if obj.user.id not in prev_user and obj.user.id != self_user.id:
                        users.append(obj.user)
                        prev_user.append(obj.user.id)
            validated_data['user_relate'] = users
            room = super(RoomSerializers, self).create(validated_data)
            try:
                title = f'{self_user.full_name} đã tạo phòng họp cho {okr.object_name} vào lúc {room.start_time.strftime("%m/%d/%Y, %H:%M:%S")}'
                body_masage = message.noti_body(data_id=room.id, content=f'đã tạo 1 phòng họp cho okr {okr.object_name} vào lúc {room.start_time.strftime("%m/%d/%Y, %H:%M:%S")}', type_notification=3, okr_id = okr_id)
            except Exception as e:
                title = f'{self_user.full_name} đã tạo phòng họp cho {okr.object_name}'
                body_masage = message.noti_body(data_id=room.id, content=f'đã tạo 1 phòng họp cho okr {okr.object_name}', type_notification=3, okr_id = okr_id)
            notis =[]
            for user in users:
                item = Notification(**body_masage, user = user, user_sent = self_user)
                notis.append(item)
            Notification.objects.bulk_create(notis)
            send_noti_by_token(to_user= users,send_message= body_masage, title=title)
            return room
        raise serializers.ValidationError({'message':"Don't have permission to create room "})
    
##### CHALLENGE & INSPIRATION #####
class OKRInspirationSerializer(FlexFieldsModelSerializer):
    okr_name = serializers.SerializerMethodField('get_okr_name')
    class Meta:
        model = Inspiration
        fields = '__all__'
        
    def get_okr_name(self, obj):
        return obj.okr.object_name
        
    def create(self, validated_data):
        self_user = self.context['request'].user
        okr = validated_data.get('okr', None)
        content = ''
        if okr:
            content = 'đã yêu cầu bạn làm 1 bài cảm hứng'
        user_do = validated_data.get('user_do',[])
        noti_list=[]
        if self_user != okr.user:
            raise serializers.ValidationError({'detail':'user token is different user create okr'})
        with transaction.atomic():
            inspiration = super(OKRInspirationSerializer, self).create(validated_data)
            body_message =  message.noti_body(inspiration.id,content=content,type_notification=1, okr_id=inspiration.okr.id)
            for user in user_do:
                notification = Notification(user = user, user_sent = self_user,**body_message)
                noti_list.append(notification)
            Notification.objects.bulk_create(noti_list)
            send_noti_by_token(to_user= user_do,send_message= body_message, title=f"{self_user.full_name} đã tạo cho bạn 1 bài cảm hứng") # notification to users
            okr.inspiration = True
            okr.save()
            return inspiration

class CommentSerializer(FlexFieldsModelSerializer):
    user_data = serializers.SerializerMethodField('get_user_data')
    class Meta:
        model = Comment
        fields = '__all__'
    def get_user_data(self, obj):
        return {'full_name': obj.user.full_name ,'img_url':obj.user.img_url,}
    def create(self, validated_data):
        self_user = self.context['request'].user
        challenge = validated_data.get('challenge',Challenge())
        prev_user = [self_user.id]
        with transaction.atomic():
            comment = super(CommentSerializer, self).create(validated_data)
            if comment.challenge:
                # challenge = comment.challenge
                users = []
                noti_list =[]
                content = f'đã bình luận về {challenge.okr.object_name}'
                body_message = message.noti_body(challenge.id, content, type_notification=0, okr_id=challenge.okr.id)
                title=f'{self_user.full_name} đã bình luận về {challenge.okr.object_name}'
                if challenge.challenged_company:
                    noti_to_users = AppUser.objects.filter(department__company = challenge.challenged_company)
                    send_noti_by_topic(topic = challenge.okr.user.topic, send_message= body_message,title= title )
                else:
                    challenged_user = challenge.challenged_user.all()
                    challenged_department = AppUser.objects.filter(department__in = challenge.challenged_department.all())
                    
                    noti_to_users = list(challenged_department) + list(challenged_user)
                    send_noti_by_token(to_user= noti_to_users,send_message= body_message,title = title ) # notification to users
                for user in noti_to_users:
                    if user not in list(challenge.okr.user_muted.all()):
                        if user.id not in prev_user:
                            notification = Notification(user = user, user_sent = self_user,**body_message)
                            noti_list.append(notification)
                            prev_user.append(user.id)
                Notification.objects.bulk_create(noti_list)
        return comment
    
class OKRChallengeSerializer(FlexFieldsModelSerializer):
    okr_name = serializers.SerializerMethodField('get_okr_name')
    challenge_comment = CommentSerializer(read_only = True, many = True)
    user_data = serializers.SerializerMethodField('get_user_data')
    class Meta:
        model = Challenge
        fields = '__all__'
    def get_okr_name(self, obj):
        return obj.okr.object_name
    def get_user_data(self, obj):
        return {'full_name': obj.user.full_name, 'img_url':obj.user.img_url}
    def create(self, validated_data):
        self_user = self.context['request'].user
        okr = validated_data.get('okr', None)
        challenged_user = validated_data.get('challenged_user', [])
        challenged_department = validated_data.get('challenged_department',None)
        challenged_company = validated_data.get('challenged_company',None)
        noti_to_users= []
        noti_list=[]
        
        with transaction.atomic():
            challenge = super(OKRChallengeSerializer, self).create(validated_data)
            title=f"{self_user.full_name} đã gửi cho bạn 1 thách thức"
            body_message =  message.noti_body(challenge.id,content='đã gửi cho bạn 1 thách thức', type_notification=0, okr_id=challenge.okr.id)
            if challenged_company:
                noti_to_users = AppUser.objects.filter(department__company = challenged_company)
                send_noti_by_topic(topic = challenge.okr.user.topic,send_message= body_message, title=title)
            else:
                noti_to_users = list(AppUser.objects.filter(department__in = challenged_department)) + challenged_user
                send_noti_by_token(to_user= noti_to_users,send_message= body_message, title=title) # notification to users
            for user in noti_to_users:
                notification = Notification(user = user, user_sent = self_user,**body_message)
                noti_list.append(notification)
            Notification.objects.bulk_create(noti_list)
            okr.challenge = True
            okr.save()
            return challenge

