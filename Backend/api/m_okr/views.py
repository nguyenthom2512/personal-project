from django.db.models.aggregates import Sum
from django.http import response
from django.utils.translation import deactivate
from drf_yasg.openapi import Items
from api.models import OKR, AppUser, Challenge, Checkin, Comment, Feedback, Inspiration, LinkUser, Result, Room, Unit, Department
from rest_framework import generics, filters, status
from .serializers import CheckinSerializer, CommentSerializer, OKRChallengeSerializer, OKRInspirationSerializer, OKRListSerializer, OKRRelateSerializer, ResultSerializer, ResultSpecialSerializer, RoomSerializers, UnitSerializer, OKRDetailSerializer
from django_filters.rest_framework.backends import DjangoFilterBackend
from drf_multiple_model.views import ObjectMultipleModelAPIView
from api.m_account.serializers import  UserListSerializer
from api.m_department.serializers import DepartmentSerializer
from api.filters  import CheckinDepartmentFilter, FilterCreatedTimeByDateRange, FilterByUserRequest, FilterUserInCompanyByUserRequest
from api.pagination import StandardResultsSetPagination
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from django.http.response import JsonResponse
import datetime
from api.utils import TypePatternFormat as parttern
from dry_rest_permissions.generics import DRYGlobalPermissions, DRYPermissions
from django.contrib.auth.models import Group, Permission
from api.permissions import IsUserHasPermissionsOKR
from datetime import date
class UnitList(generics.ListCreateAPIView):
    pagination_class = None
    queryset = Unit.objects.all()
    serializer_class = UnitSerializer
class OKRList(generics.ListCreateAPIView):
    """
    param 
    --
    ?department=2
    ?okr_parent__isnull = True
    ?okr_parent = 2
    """ 
    queryset = OKR.objects.all()
    pagination_class = StandardResultsSetPagination
    permission_classes = [IsUserHasPermissionsOKR,]
    
    filter_backends  = (filters.SearchFilter,DjangoFilterBackend, FilterCreatedTimeByDateRange)
    filter_fields = {'okr_parent': ['exact', 'isnull'],'department':['exact'],'is_done': ['exact']}
    search_fields  = ['object_name']
    def get_serializer_class(self):
        if self.request.method == 'POST':
            return OKRDetailSerializer
        return OKRListSerializer
    
class OKRListAll(generics.ListAPIView):
    pagination_class = None
    queryset = OKR.objects.all()
    serializer_class = OKRListSerializer
    filter_backends = [DjangoFilterBackend,FilterUserInCompanyByUserRequest]
    filterset_fields = ['department']
    permission_classes = [IsUserHasPermissionsOKR,]
    # search_fields = ['object_name']

@api_view(['GET'])
def data_for_create(request):
    company_id = None
    response = {}
    if request.user.pk and request.user.department:
        company = request.user.department.company
        user_qs = AppUser.objects.filter(department__company=company).order_by('department')
        unit_qs = Unit.objects.all()
        department_qs = Department.objects.filter(company = company)
        response = {
            'Company': company.id,
            'AppUser': UserListSerializer(user_qs, many = True).data,
            'Unit': UnitSerializer(unit_qs, many = True).data,
            'Department' : DepartmentSerializer(department_qs, many =True).data
        }
    return Response(data=response, status= status.HTTP_200_OK)
    
class OKRDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = OKR.objects.all()
    serializer_class = OKRDetailSerializer
    filter_backends = [DjangoFilterBackend]
    permission_classes = [IsUserHasPermissionsOKR,]

@api_view(['GET'])
def list_okr_relate(request, pk):
    okr_realate_values = OKR.objects.filter(id = pk).values('okr_relate')
    data = []
    if okr_realate_values.count() != 0 and okr_realate_values[0]['okr_relate']:
        for item in okr_realate_values:
            relate_qs = OKR.objects.get(id = item['okr_relate'])
            data.append(OKRRelateSerializer(relate_qs).data)
    return Response(data={'okr_relate': data}, status=status.HTTP_200_OK)
    
class ResultList(generics.CreateAPIView):
    pagination_class = None
    queryset =  Result.objects.all()
    serializer_class = ResultSerializer
    
class ResultDetail(generics.RetrieveUpdateDestroyAPIView):
    pagination_class = None
    queryset =  Result.objects.all()
    serializer_class = ResultSerializer
    

@api_view(['GET'])
def detail_okr_tree(request,pk, format= None):
    result_qs = Result.objects.filter(okr_id = pk)
    okr_child_qs = OKR.objects.filter(okr_parent = pk)
    response = {
        'okr_result' : ResultSpecialSerializer(result_qs, many = True).data,
        'okr_child': OKRListSerializer(okr_child_qs, many = True).data
    }
    return Response(data= response, status = status.HTTP_200_OK)

# * FLOW CHECKIN
class CheckinList(generics.CreateAPIView):
    pagination_class = None
    queryset = Checkin.objects.all()
    serializer_class = CheckinSerializer
class CheckinDetail(generics.RetrieveAPIView):
    pagination_class = None
    queryset = Checkin.objects.all()
    serializer_class = CheckinSerializer

class RoomCreate(generics.CreateAPIView):
    pagination_class = None
    queryset = Room.objects.all()
    serializer_class = RoomSerializers
    permission_classes = [IsUserHasPermissionsOKR]

class RoomDetail(generics.RetrieveAPIView):
    pagination_class = None
    queryset = Room.objects.all()
    serializer_class = RoomSerializers
    permission_classes = [IsUserHasPermissionsOKR]
    
# @api_view(['GET'])
# def get_room_info(request):
#     """
#     room_id = "string"
#     """
#     room_id_qs = request.query_params.get('room_id', None)
#     room_qs = Room.objects.filter(room_id = room_id_qs)
#     return Response(data= RoomSerializers(room_qs, many = True).data, status= status.HTTP_200_OK)

 
##### CHALLENGE & INSPIRATION #####

class ChallengeList(generics.ListCreateAPIView):
    pagination_class = None
    queryset = Challenge.objects.all()
    serializer_class = OKRChallengeSerializer
    filter_backends = [DjangoFilterBackend, FilterByUserRequest]
    # filterset_fields = ['challenged_user','challenged_department','challenged_company']
    permission_classes = [IsUserHasPermissionsOKR]

class ChallengeDetaiL(generics.RetrieveAPIView):
    pagination_class = None
    queryset = Challenge.objects.all()
    serializer_class = OKRChallengeSerializer
    

class InspirationList(generics.ListCreateAPIView):
    pagination_class = None
    queryset = Inspiration.objects.all()
    serializer_class = OKRInspirationSerializer
    filter_backends = [DjangoFilterBackend, FilterByUserRequest]

class InspirationDetail(generics.RetrieveAPIView):
    pagination_class = None
    queryset = Inspiration.objects.all()
    serializer_class = OKRInspirationSerializer
    filter_backends = [DjangoFilterBackend]
class CommentList(generics.ListCreateAPIView):
    pagination_class = StandardResultsSetPagination
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['todo', 'challenge']

class CommentDetail(generics.RetrieveUpdateDestroyAPIView):
    pagination_class = None
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

def recursive_result(list_result, root_result):

    for result in list_result:
        okr_qs = OKR.objects.filter(result_parent = result)
        if okr_qs.count() == 0:
            if result.okr.result_parent is None:
                result.root_result = result
            else: result.root_result = root_result
            result.save()
        else:
            for okr in okr_qs:
                result_qs = Result.objects.filter(okr = okr)
                Result.objects.filter(okr = okr).update(root_result = root_result)
                recursive_result(result_qs, root_result)
                

@api_view(['GET'])
def recursive_update_root_result(request):
    department_qs = Department.objects.all()
    response = dict()
    for department in department_qs:
        okr_qs = OKR.objects.filter(department = department, okr_parent__isnull = True)
        for okr in okr_qs:
            result_qs = Result.objects.filter(okr = okr)
            for result in result_qs:
                result.root_result = result
                result.save()
                recursive_result([result], result)
        okr_qs_all = OKR.objects.filter(department = department)
        response[department.department_name] = OKRListSerializer(okr_qs_all, many = True).data
    return Response(data=response, status=status.HTTP_200_OK)

@api_view(['GET'])
def dashboard_view(request, format= None):
    """
    PARAM: 
    --
        start_time: "YYYY-MM-DD"
        end_time: "YYYY-MM-DD"
        department: int
    """
    user = request.user
    if not user.expired:
        return Response(data={'detail':'You do not have permission to perform this action.'}, status=status.HTTP_403_FORBIDDEN)
    else:
        groups = user.groups.all().first()
        # if user.groups.all().count() and 
    start_time = request.query_params.get('start_time', None)
    end_time = request.query_params.get('end_time', None)
    department = request.query_params.get('department', None)
    try:
        dt_start_time = datetime.datetime.strptime(start_time, parttern.date_format_pattern)
        dt_end_time = datetime.datetime.strptime(end_time, parttern.date_format_pattern)
        if (start_time and end_time) and department:
            filterset ={
                'department' : department, 
                'created_time__date__range': [dt_start_time, dt_end_time]
            }
            total_okr = OKR.objects.filter(**filterset).count()
            total_okr_by_challenge = OKR.objects.filter(**filterset, challenge = True).count()
            total_okr_by_inspiration = OKR.objects.filter(**filterset, inspiration = True).count()
            return Response(data={'total_okr':total_okr, 'total_okr_by_challenge':total_okr_by_challenge, 'total_okr_by_inspiration': total_okr_by_inspiration },status=status.HTTP_200_OK)
        return Response(data={}, status= status.HTTP_200_OK)
    except:
        return Response(data={'message':'Invalid date'}, status=status.HTTP_400_BAD_REQUEST)


# @api_view(['GET'])
# def get_user_in_okr(request):
#     """
#     okr = id
#     """
#     okr = request.query_params.get('okr',None)
#     if okr:
#         try:
#             okr = OKR.objects.get(id = okr)
#             linkuser_qs = LinkUser.objects.filter(okr=okr)
#             user_qs = [obj.user for obj in linkuser_qs]
#             user_qs.append(okr.user)
#             return Response(UserListSerializer(user_qs, many=True).data, status=status.HTTP_200_OK)
#         except OKR.DoesNotExist:
#             return Response(data={'detail':'not found'}, status=status.HTTP_400_BAD_REQUEST)
#     return Response(data={}, status=status.HTTP_200_OK)

@api_view(['PATCH'])
def muted_okr(request):
    """
    okr = int
    """
    user_request = request.user
    okr = request.data.get('okr', None)
    if okr:
        try:
            okr_qs = OKR.objects.get(id = okr)
            okr_qs.user_muted.add(user_request)
            okr_qs.save()
            return Response(data={'detail':'Mute notification completely'}, status=status.HTTP_200_OK)
        except OKR.DoesNotExist:
            return Response(data={'detail':'can not find OKR'}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print("\nException in OKR view: ", e)
            return Response(data={'detail':'something went wrong', 'message': e}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
    return Response(data={'detail':'Invalid body'}, status=status.HTTP_400_BAD_REQUEST)
