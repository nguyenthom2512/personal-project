from authemail.admin import EmailUserAdmin
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.db import models
from django import forms
from django.utils.translation import override
from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from api.models import OKR, Answer, AppUser, Challenge, Checkin, CheckinDetail, Criteria, Department, Company, Feedback, Inspiration, LinkUser, Master, Notification, Package, Present, Priority, Question, QuestionAttachment, QuestionQuiz, Quiz, Result, Role, Tag, Todo, Unit, UserQuestion, UserQuiz, File, TodoCheckList, Room
from api.proxy import StaffUser

# Register your models here.

class AppUserAdmin(EmailUserAdmin):
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'full_name','password1', 'password2', )}
        ),
    )
    fieldsets = [
        (None, {'fields': ()}),
        ('Personal Info', {'fields': ('email','full_name')}),
        ['Permissions', {'fields': ['is_active', 'is_staff', 
                                       'is_superuser', 'is_verified', 
                                       'groups', 'user_permissions']}],
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Custom info', {'fields': ('date_of_birth','fb_link',\
            'phone_number','img_url','role','gender','total_rate', 'diamond', 'expired')}),
        ]
    # def render_change_form(self, request, context, *args, **kwargs):
    #     if request.user.is_superuser:
    #         return super(AppUserAdmin, self).render_change_form(request, context, *args, **kwargs)
    #     context['adminform'].form.fields['department'].queryset = Department.objects.filter(company = request.user.department.company)
    #     return super(AppUserAdmin, self).render_change_form(request, context, *args, **kwargs)
    
# Tabular inline
class UserRAICInline(admin.TabularInline):
    model = LinkUser

class AnswerInline(admin.TabularInline):
    model = Answer

    
class QuestionAttaccmentInline(admin.TabularInline):
    model = QuestionAttachment

class QuestionQuizInline(admin.TabularInline):
    model = QuestionQuiz


class ChecklistInline(admin.TabularInline):
    model = TodoCheckList
    
class FileInline(admin.TabularInline):
    model = File

# Model admin
class RaicAdmin(admin.ModelAdmin):
   inlines = [UserRAICInline,]

class LinkUserAdmin(admin.ModelAdmin):
    list_display =['priority_name','priority_level','color']    

class QuestionAdmin(admin.ModelAdmin):
    list_display =['content', 'duration', 'created_time']
    inlines =[AnswerInline,QuestionAttaccmentInline]
    def render_change_form(self, request, context, *args, **kwargs):
        if request.user.is_superuser:
            return super(QuestionAdmin, self).render_change_form(request, context, *args, **kwargs)
        if request.user.department:
            context['adminform'].form.fields['company'].queryset = Company.objects.filter(id=request.user.department.company.id)
        return super(QuestionAdmin, self).render_change_form(request, context, *args, **kwargs)

class QuizAdmin(admin.ModelAdmin):
    list_display =['title','description','total_point','duration', 'created_time']
    exclude = ('company',)
    inlines = [QuestionQuizInline, ]

class AppUserAdmin(AppUserAdmin):
    model = AppUser
    list_display =['id','full_name','email','phone_number', 'department','role','is_active']
    def get_queryset(self, request):
        qs = super(AppUserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(department__company=request.user.department.company) 
    # def render_change_form(self, request, context, *args, **kwargs):
    #     if request.user.is_superuser:
    #         return super(AppUserAdmin, self).render_change_form(request, context, *args, **kwargs)
    #     context['adminform'].form.fields['department'].queryset = Department.objects.filter(department__company=request.user.department.company)
    #     return super(AppUserAdmin, self).render_change_form(request, context, *args, **kwargs)
    

class TodoAdmin(admin.ModelAdmin):
    list_display = ['todo_name','user','result','priority','start_time','end_time']
    inlines = [ChecklistInline,UserRAICInline]

class OKRAdmin(admin.ModelAdmin):
    model = OKR
    list_display = ['object_name', 'user','created_time', 'challenge', 'inspiration','is_done']
    inlines = [UserRAICInline,]
class CriteriaAdmin(admin.ModelAdmin):
    list_display =['title', 'rate', 'created_time']
    def get_queryset(self, request):
        qs = super(CriteriaAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.department.company) 
    def render_change_form(self, request, context, *args, **kwargs):
        if request.user.is_superuser:
            return super(CriteriaAdmin, self).render_change_form(request, context, *args, **kwargs)
        context['adminform'].form.fields['company'].queryset = Company.objects.filter(id=request.user.department.company.id)
        return super(CriteriaAdmin, self).render_change_form(request, context, *args, **kwargs)

class RoleAdmin(admin.ModelAdmin):
    list_display = ['role_name','company','description','created_time']
    def get_queryset(self, request):
        qs = super(RoleAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.department.company) 
    def render_change_form(self, request, context, *args, **kwargs):
        if request.user.is_superuser:
            return super(RoleAdmin, self).render_change_form(request, context, *args, **kwargs)
        context['adminform'].form.fields['company'].queryset = Company.objects.filter(id=request.user.department.company.id)
        return super(RoleAdmin, self).render_change_form(request, context, *args, **kwargs)

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ['department_name','company', 'description', 'created_time']
    def get_queryset(self, request):
        qs = super(DepartmentAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.department.company) 
    def render_change_form(self, request, context, *args, **kwargs):
        if request.user.is_superuser:
            return super(DepartmentAdmin, self).render_change_form(request, context, *args, **kwargs)
        context['adminform'].form.fields['company'].queryset = Company.objects.filter(id=request.user.department.company.id)
        return super(DepartmentAdmin, self).render_change_form(request, context, *args, **kwargs)
    
class UnitAdmin(admin.ModelAdmin):
    list_display = ['id', 'unit_name']
    def get_queryset(self, request):
        qs = super(UnitAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.department.company)
    def render_change_form(self, request, context, *args, **kwargs):
        if request.user.is_superuser:
            return super(UnitAdmin, self).render_change_form(request, context, *args, **kwargs)
        context['adminform'].form.fields['company'].queryset = Company.objects.filter(id=request.user.department.company.id)
        return super(UnitAdmin, self).render_change_form(request, context, *args, **kwargs)
class MasterAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(MasterAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(company=request.user.department.company)
    def render_change_form(self, request, context, *args, **kwargs):
        if request.user.is_superuser:
            return super(MasterAdmin, self).render_change_form(request, context, *args, **kwargs)
        context['adminform'].form.fields['company'].queryset = Company.objects.filter(id=request.user.department.company.id)
        return super(MasterAdmin, self).render_change_form(request, context, *args, **kwargs)

class CompanyAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(CompanyAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(id=request.user.department.company.id)
    def has_add_permission(self, request):
        if request.user.is_superuser:
            return True
        return False

class PackageAdmin(admin.ModelAdmin):
    def has_view_permission(self, request):
        if request.user.is_superuser:
            return True
        return False 
class StaffUserAdmin(admin.ModelAdmin):
    #model = AppUser
    list_display =['full_name','email','phone_number', 'department','role','is_active']
    
    fieldsets = [
        (None, {'fields': ()}),
        ('Personal Info', {'fields': ('email','full_name','password')}),
        ('Custom info', {'fields': ('date_of_birth','fb_link',\
            'phone_number','img_url','role','gender','rate_month','department')}),
        ]
    def get_form(self, request, obj=None, **kwargs):
        form = super(StaffUserAdmin, self).get_form(request, obj, **kwargs)
        form.base_fields['department'].initial = Department.objects.filter(company = request.user.department.company).first().id
        return form
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "department":
            kwargs["queryset"] = Department.objects.filter(company=request.user.department.company)
        return super(StaffUserAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
    def get_queryset(self, request):
        qs = super(StaffUserAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return qs
        department = Department.objects.filter(company = request.user.department.company)
        return qs.filter(department__in=department) 
    
    
admin.site.unregister(get_user_model())
admin.site.register(get_user_model(), AppUserAdmin)
admin.site.register(StaffUser, StaffUserAdmin)

admin.site.register(Company,CompanyAdmin)
admin.site.register(Department,DepartmentAdmin)
admin.site.register(Role,RoleAdmin)
admin.site.register(Unit,UnitAdmin)
admin.site.register(Quiz,QuizAdmin)
admin.site.register(Question,QuestionAdmin)
admin.site.register(Criteria,CriteriaAdmin)
admin.site.register(Master,MasterAdmin)
admin.site.register(Present) 
admin.site.register(Package)
