from itertools import count
from typing import cast
from rest_framework import generics, filters, status
from api.filters import FilterByCompany, FilterByUserRequest
from api.models import Criteria, Department, Feedback
from api.permissions import IsUserHasPermissionsOKR
from .serializers import CriteriaSerializer, FeedbackSerializer
from django_filters.rest_framework.backends import DjangoFilterBackend
from api.pagination import StandardResultsSetPagination
from rest_framework.decorators import api_view, permission_classes
from django.db.models.aggregates import Count, Sum
from rest_framework.response import Response
from api.utils import TypePatternFormat as parttern
import datetime
import calendar
from api.filters import FilterFeedbackListByUser


    
#### FEEDBACK ####

class CriteriaList(generics.ListAPIView):
    pagination_class = None
    serializer_class = CriteriaSerializer
    queryset = Criteria.objects.all()
    filter_backend = [FilterByCompany]
class FeedbackList(generics.ListCreateAPIView):
    pagination_class = StandardResultsSetPagination
    serializer_class = FeedbackSerializer
    queryset = Feedback.objects.all()
    filter_backends = [DjangoFilterBackend,FilterFeedbackListByUser]
    filter_fields = {'criteria': ['isnull']}
    permission_classes = [IsUserHasPermissionsOKR]

class FeedbackDetail(generics.RetrieveAPIView): 
    pagination_class = None
    queryset = Feedback.objects.all()
    serializer_class = FeedbackSerializer
    permission_classes = [IsUserHasPermissionsOKR]

    
@api_view(['GET'])
def statistics_feedback(request):
    user = request.user
    current_year = datetime.datetime.now().year
    current_month = datetime.datetime.now().month
    start_date_current_month = datetime.date(current_year,current_month,1)
    end_date_current_month = datetime.date(current_year,current_month,calendar.monthrange(current_year, current_month)[1])
    
    if user.pk:
        if not user.department:
            return Response({'message':"Cant't not find department"}, status=status.HTTP_400_BAD_REQUEST)
    # statistic sent
        total_sent_rate = Feedback.objects.filter(user = user, criteria__isnull = False).values('user').aggregate(total = Sum('criteria__rate'))
        rank_sent_month = Feedback.objects.filter(okr__department__company = user.department.company, created_time__date__range = [start_date_current_month,end_date_current_month]).values('user').annotate(total = Sum('criteria__rate'))
        rank_sent_department = Feedback.objects.filter(okr__department__company = user.department.company, okr__department = user.department).values('user').annotate(total = Sum('criteria__rate')).values_list('user',flat=True)

        # statistics received
        total_received_rate = Feedback.objects.filter(user_received = user).values('user_received').aggregate(total = Sum('criteria__rate'))
        rank_received_month = Feedback.objects.filter(okr__department__company = user.department.company, created_time__date__range = [start_date_current_month,end_date_current_month]).values('user_received').annotate(total = Sum('criteria__rate')).values_list('user_received',flat=True)
        rank_received_department = Feedback.objects.filter(okr__department__company = user.department.company, okr__department = user.department).values('user_received').annotate(total = Sum('criteria__rate')).values_list('user',flat=True)

        response = {
            'sent':{
                'total' : total_sent_rate['total'],
                'rank_month':list(rank_sent_month),
                'rank_department' : list(rank_sent_department)
            },
            'received':{
                'total' : total_received_rate['total'],  
                'rank_month':list(rank_received_month),
                'rank_department':list(rank_received_department)
            }
        }
        return Response(data=response, status= status.HTTP_200_OK)
    return Response(data={'mesage':'token is invalid'}, status=status.HTTP_401_UNAUTHORIZED)

@api_view(['GET'])
def rank_month(request):
    """
    start_time: "YYYY-MM-DD"
    end_time : "YYYY-MM-DD"
    type_rank: "sent"/"received"
    """
    user = request.user
    start_time = request.query_params.get('start_time', None)
    end_time = request.query_params.get('end_time', None)
    type_rank = request.query_params.get('type_rank', None)
    try:
        dt_start_time = datetime.datetime.strptime(start_time, parttern.date_format_pattern)
        dt_end_time = datetime.datetime.strptime(end_time, parttern.date_format_pattern)
        retrieve_value = ""
        if type_rank == 'sent':
            retrieve_value = 'user'
        else:
            retrieve_value = 'user_received'
        if start_time and end_time:
            rank_month = Feedback.objects.filter(okr__department__company = user.department.company, created_time__date__range = [dt_start_time,dt_end_time]).values(retrieve_value,'total','user__role_role_name').annotate(total = Sum('criteria__rate'))
            return Response(data= {'rank_month':list(rank_month)}, status=status.HTTP_200_OK)
        return Response(data={}, status= status.HTTP_200_OK)
    except:
        return Response(data={'message':'Invalid date'}, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET'])
def rank_department(request):
    """
    department = int
    type_rank = "sent/"received"
    """
    user = request.user
    department = request.query_params.get('department', None)
    type_rank = request.query_params.get('type_rank', None)
    retrieve_value = ""
    if type_rank == 'sent':
        retrieve_value = 'user'
    else:
        retrieve_value = 'user_received'
    if department:
        rank_department = Feedback.objects.filter(okr__department__company = user.department.company, okr__department = department).values(retrieve_value,'total','user__role_role_name').annotate(total = Sum('criteria__rate'))
        return Response(data= {'rank_department':list(rank_department)}, status=status.HTTP_200_OK)
    return Response(data={}, status= status.HTTP_200_OK) 
