from django.db import transaction
from rest_flex_fields.serializers import FlexFieldsModelSerializer
from rest_framework import fields, serializers
from rest_framework.generics import RetrieveUpdateAPIView
from api.utils import NotificationBody
from api.models import AppUser, Criteria, Feedback, LinkUser, Notification
from api.notification import send_noti_by_token
   
    
#### FEEDBACK ####

class CriteriaSerializer(FlexFieldsModelSerializer):
    title_rate = serializers.SerializerMethodField('get_title_rate')
    class Meta:
        model = Criteria
        fields = '__all__'
    def get_title_rate(self, obj):
        return f"{obj.title} ({obj.rate})"

class FeedbackSerializer(FlexFieldsModelSerializer):
    rate = serializers.SerializerMethodField('get_rate')
    criteria_title = serializers.SerializerMethodField('get_criteria_title')
    okr_name = serializers.SerializerMethodField('get_okr_name')
    user_data = serializers.SerializerMethodField('get_user_data')
    user_received_data = serializers.SerializerMethodField('get_user_received_data')
    class Meta:
        model = Feedback
        fields = '__all__'
    def get_rate(self, obj):
        return  obj.criteria.rate if obj.criteria is not None else None
    def get_criteria_title(self, obj):
        return  obj.criteria.title if obj.criteria is not None else None
    def get_okr_name(self, obj):
        if obj.okr:
            return obj.okr.object_name
        return None
    def get_user_data(self, obj):
        return{'full_name':obj.user.full_name, 'img_url':obj.user.img_url,}
    def get_user_received_data(self, obj):
        return{'full_name':obj.user_received.full_name, 'img_url':obj.user_received.img_url,}

    def validate(self, data):
        okr = data.get('okr')
        self_user = self.context['request'].user
        request_user = data.get('user')
        user_received = data.get('user_received')
        if request_user.id != self_user.id:
             raise serializers.ValidationError({"message": "[create feedback] user is different token"})
        if user_received.id == self_user.id:
             raise serializers.ValidationError({"message": "can not send feedback to yourself"})
        criteria = data.get('criteria', None)
        quantity = data.get('quantity', None)
        self_user = self.context['request'].user
        user_received = data.get('user_received')
        if not criteria and (quantity and quantity > self_user.rate_month):            
            raise serializers.ValidationError({'message':"Your rate is not enough to send"})
        if criteria and criteria.rate > self_user.rate_month:
            raise serializers.ValidationError({'message':"Your rate is not enough to send"})
        return super().validate(data)
    
    def create(self, validated_data):
        request_user = validated_data.get('user')
        quantity = validated_data.get('quantity', 0)
        if validated_data.get('user') == validated_data.get('user_received'):
            validated_data.pop('criteria')
        criteria = validated_data.get('criteria', None)
        self_user = self.context['request'].user
        with transaction.atomic():
            rate = criteria.rate if criteria is not None else quantity
            feedback =  super(FeedbackSerializer,self).create(validated_data)
            body_massage = NotificationBody.noti_body(data_id=feedback.id,content=f"đã tặng bạn {rate} sao", type_notification=5, okr_id=None)
            user_sent = feedback.user
            user_received = feedback.user_received
            user_sent.rate_sent = user_sent.rate_sent + rate
            user_sent.rate_month = user_sent.rate_month - abs(rate) 
            user_sent.save()
            
            user_received.rate_received = user_received.rate_received + rate
            user_received.total_rate = user_received.total_rate + rate 
            user_received.save()
            
            noti = Notification.objects.create(**body_massage, user = user_received, user_sent = self_user)
            send_noti_by_token(to_user=[user_received], send_message=body_massage, title=f'{user_sent.full_name} đã tặng bạn {rate} sao')
            return feedback
