from django.contrib import admin
from django.urls import include, path
from . import views

admin.autodiscover()

urlpatterns = [
    path('admin/', admin.site.urls),

    path('api/accounts/', include('accounts.urls')),
    path('api/accounts/', include('authemail.urls')),
    
    path('api/signup/verify/', views.SignupVerifyFrontEnd.as_view()),
    path('api/signup/verified/', views.SignupVerifiedFrontEnd.as_view(),
         name='signup_verified_page'),
    path('signup/not_verified/', views.SignupNotVerifiedFrontEnd.as_view(),
         name='signup_not_verified_page'),

    path('password/reset/verified/',
         views.PasswordResetVerifiedFrontEnd.as_view(),
         name='password_reset_verified_page'),
    path('password/reset/not_verified/',
         views.PasswordResetNotVerifiedFrontEnd.as_view(),
         name='password_reset_not_verified_page'),
    path('password/reset/success/', views.PasswordResetSuccessFrontEnd.as_view(),
         name='password_reset_success_page'),

    path('password/change/', views.PasswordChangeFrontEnd.as_view(),
         name='password_change_page'),
    path('password/change/success/', views.PasswordChangeSuccessFrontEnd.as_view(),
         name='password_change_success_page'),

]