from django.urls import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from django.views.generic.base import View
from django.views.generic.edit import FormView

from authemail import wrapper

from .forms import PasswordResetVerifiedForm
from .forms import PasswordChangeForm

class SignupVerifyFrontEnd(View):
    def get(self, request, format=None):
        code = request.GET.get('code', '')

        account = wrapper.Authemail()
        response = account.signup_verify(code=code)

        # Handle other error responses from API
        if 'detail' in response:
            return HttpResponseRedirect(reverse('signup_not_verified_page'))

        return HttpResponseRedirect(reverse('signup_verified_page'))


class SignupVerifiedFrontEnd(TemplateView):
    template_name = 'signup_verified.html'


class SignupNotVerifiedFrontEnd(TemplateView):
    template_name = 'signup_not_verified.html'


class PasswordResetEmailSentFrontEnd(TemplateView):
    template_name = 'password_reset_email_sent.html'


class PasswordResetVerifyFrontEnd(View):
    def get(self, request, format=None):
        code = request.GET.get('code', '')

        account = wrapper.Authemail()
        response = account.password_reset_verify(code=code)

        # Handle other error responses from API
        if 'detail' in response:
            return HttpResponseRedirect(
                reverse('password_reset_not_verified_page'))

        request.session['password_reset_code'] = code

        return HttpResponseRedirect(reverse('password_reset_verified_page'))


class PasswordResetVerifiedFrontEnd(FormView):
    template_name = 'password_reset_verified.html'
    form_class = PasswordResetVerifiedForm
    success_url = reverse_lazy('password_reset_success_page')

    def form_valid(self, form):
        code = self.request.session['password_reset_code']
        password = form.data['password']

        account = wrapper.Authemail()
        response = account.password_reset_verified(code=code, password=password)

        # Handle other error responses from API
        if 'detail' in response:
            form.add_error(None, response['detail'])
            return self.form_invalid(form)

        return super(PasswordResetVerifiedFrontEnd, self).form_valid(form)


class PasswordResetNotVerifiedFrontEnd(TemplateView):
    template_name = 'password_reset_not_verified.html'


class PasswordResetSuccessFrontEnd(TemplateView):
    template_name = 'password_reset_success.html'


class PasswordChangeFrontEnd(FormView):
    template_name = 'password_change.html'
    form_class = PasswordChangeForm
    success_url = reverse_lazy('password_change_success_page')

    def form_valid(self, form):
        token = self.request.session['auth_token']
        password = form.cleaned_data['password']

        account = wrapper.Authemail()
        response = account.password_change(token=token, password=password)

        # Handle other error responses from API
        if 'detail' in response:
            form.add_error(None, response['detail'])
            return self.form_invalid(form)

        return super(PasswordChangeFrontEnd, self).form_valid(form)


class PasswordChangeSuccessFrontEnd(TemplateView):
    template_name = 'password_change_success.html'
    

# SENT MAIL

class PasswordResetEmailSentFrontEnd(TemplateView):
    template_name = 'password_reset_email_sent.html'

