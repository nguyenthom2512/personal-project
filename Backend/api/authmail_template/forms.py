from django import forms
from django.forms.forms import NON_FIELD_ERRORS
from django.utils.translation import ugettext_lazy as _
import re
from api.utils import password_parttern


class AddErrorMixin(object):
    def add_error(self, field, msg):
        field = field or NON_FIELD_ERRORS
        if field in self._errors:
            self._errors[field].append(msg)
        else:
            self._errors[field] = self.error_class([msg])


class PasswordConfirmForm(AddErrorMixin, forms.Form):
    password = forms.CharField(max_length=128,error_messages={'required':'Không được để trống'})
    password2 = forms.CharField(max_length=128,error_messages={'required':'Không được để trống'})

    def clean_password(self):
        password = self.cleaned_data.get('password', '')
        if not re.fullmatch(password_parttern, password):
            raise forms.ValidationError(
                _("Mật khẩu tối thiểu 8 ký tự và chứa ít nhất 1 chữ hoa, chữ thường, số và 1 ký tự đặc biệt"))
        return password
    
    def clean_password2(self):
        password = self.cleaned_data.get('password', '')
        password2 = self.cleaned_data['password2']
        if password != password2:
            raise forms.ValidationError(
                _("Không khớp với mật khẩu"))
        return password2


class PasswordResetVerifiedForm(PasswordConfirmForm):
    pass


class PasswordChangeForm(PasswordConfirmForm):
    pass
