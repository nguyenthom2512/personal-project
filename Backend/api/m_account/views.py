from django import http
from django.db.models import query
from api.models import OKR, AppUser, LinkUser, Master, Package, Present, ChangePresent
from rest_framework import generics
from .serializers import ChangePresentSerializer, PackageSerializer, PresentSerializer, UserListSerializer, UserSerializers, SignupSerializer
from django_filters.rest_framework.backends import DjangoFilterBackend
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from django.contrib.auth import authenticate, get_user_model
from authemail.serializers import LoginSerializer
from django.utils.translation import gettext as _
from rest_framework.decorators import api_view, authentication_classes, permission_classes
import requests
from django.contrib.auth.models import User
from api.filters import ExcludeUserInDepartment, FilterByUserRequest, FilterUserInCompanyByUserRequest
from django.conf import settings
from authemail.models import send_multi_format_email
from firebase_admin import messaging
from authemail.models import SignupCode
from django.contrib.auth.models import Group
import re
from api.permissions import IsUserHasPermissionsOKR
from django_filters.rest_framework.backends import DjangoFilterBackend

def is_browser(request):
    """Return True if the request comes from a mobile device."""

    MOBILE_AGENT_RE=re.compile(r".*(Mozilla)",re.IGNORECASE)

    if MOBILE_AGENT_RE.match(request.META['HTTP_USER_AGENT']):
        return True
    else:
        return False

class UserList(generics.ListAPIView):
    pagination_class = None
    serializer_class = UserListSerializer
    queryset = AppUser.objects.all()
    filter_backends = [DjangoFilterBackend,ExcludeUserInDepartment,FilterUserInCompanyByUserRequest]
    filter_fields = {'role': ['exact', 'isnull'], 'department':['exact']}
    permission_classes = [IsUserHasPermissionsOKR]
    def get_queryset(self):
        queryset = AppUser.objects.all()
        okr = self.request.query_params.get('okr',None)
        if okr:
            try:
                okr = OKR.objects.get(id = okr)
                user_qs = AppUser.objects.filter(id = okr.user.id)
                linkuser_qs = LinkUser.objects.filter(okr = okr)
                for obj in linkuser_qs:
                   user_qs = user_qs | AppUser.objects.filter(id = obj.user.id)
                return user_qs
            except OKR.DoesNotExist:
                return queryset
        return queryset
    
class Login(APIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        device_id = self.request.data.get('device_id',None)
        

        if serializer.is_valid():
            email = serializer.data['email']
            password = serializer.data['password']
            user = authenticate(email=email, password=password)

            if user:
                if user.is_verified:
                    if user.is_active:
                        token, created = Token.objects.get_or_create(user=user)
                        user = AppUser.objects.get(id = token.user.id )
                        if is_browser(request):
                            try:
                                group_qs = user.groups.get(name = 'premium')
                            except Group.DoesNotExist:
                                return Response({'detail':"you don't have permission"}, status=status.HTTP_403_FORBIDDEN)
                        if device_id:
                            try:
                                user.device_id = device_id
                                user.topic = 'change_topic' # change topic
                                user.save()
                                response = messaging.subscribe_to_topic(user.device_id, 'change_topic') # change topic when license
                                # print(response.success_count, 'firebase')
                            except Exception as e:
                                print('except: ', e)
                                
                        return Response({'token': token.key,
                                         'user':UserSerializers(user).data},
                                        status=status.HTTP_200_OK)
                    else:
                        content = {'detail': _('User account not active.')}
                        return Response(content,
                                        status=status.HTTP_401_UNAUTHORIZED)
                else:
                    content = {'detail':
                               _('User account not verified.')}
                    return Response(content, status=status.HTTP_401_UNAUTHORIZED)
            else:
                content = {'detail':
                           _('Unable to login with provided credentials.')}
                return Response(content, status=status.HTTP_401_UNAUTHORIZED)

        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
            
class Logout(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        """
        Remove all auth tokens owned by request.user.
        """
        user = request.user
        tokens = Token.objects.filter(user=request.user)
        for token in tokens:
            token.delete()
            if user.device_id:
                user.device_id = None
                user.save()
                response =  messaging.unsubscribe_from_topic(user.device_id, user.topic)
                print(response.success_count, 'tokens were subscribed successfully')
        content = {'success': _('User logged out.')}
        return Response(content, status=status.HTTP_200_OK)

class Signup(APIView):
    permission_classes = (AllowAny,)
    serializer_class = SignupSerializer

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)

        if serializer.is_valid():
            email = serializer.data['email']
            password = serializer.data['password']
            full_name  = serializer.data['full_name']

            must_validate_email = getattr(settings, "AUTH_EMAIL_VERIFICATION", True)

            try:
                user = get_user_model().objects.get(email=email)
                if user.is_verified:
                    content = {'detail': _('Email address already taken.')}
                    return Response(content, status=status.HTTP_400_BAD_REQUEST)

                try:
                    # Delete old signup codes
                    signup_code = SignupCode.objects.get(user=user)
                    signup_code.delete()
                except SignupCode.DoesNotExist:
                    pass

            except get_user_model().DoesNotExist:
                user = get_user_model().objects.create_user(email=email)

            # Set user fields provided
            user.set_password(password)
            user.full_name = full_name
            user.groups.set(Group.objects.filter(name='normal'))
            if not must_validate_email:
                user.is_verified = True
                send_multi_format_email('welcome_email',
                                        {'email': user.email, },
                                        target_email=user.email)
            user.save()

            if must_validate_email:
                # Create and associate signup code
                ipaddr = self.request.META.get('REMOTE_ADDR', '0.0.0.0')
                signup_code = SignupCode.objects.create_signup_code(user, ipaddr)
                signup_code.send_signup_email()

            content = {'email': email, 'full_name': full_name,}
            return Response(content, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def login_social(request):
    """
    access_token : "string",
    login_type : "int" , type=0: normal, type=1: fb, type=2: gg, type=3: apple,
    """
    access_token = request.data['access_token']
    # access_token_sceret = request.data.get('access_token_secret', None)
    login_type = request.data.get('login_type', None)
    device_id =  request.data.get('device_id', None)
    url_fb = 'https://graph.facebook.com/me?fields=id,name,email&access_token='
    url_gg='https://www.googleapis.com/oauth2/v1/userinfo?access_token='
    url_apple = 'https://appleid.apple.com/auth/token='
    email = request.data.get('email', None)

#    login TW
    if login_type is None:
        return Response("Chua truyen login_type", status=status.HTTP_401_UNAUTHORIZED)
    
    if login_type == 1 or login_type == '1':
        url = url_fb
        prefix =  "fb"
    elif login_type == 2 or login_type == '2':
        url = url_gg
        prefix = "gg"
    elif login_type == 3 or login_type == '3':
        url = url_apple
        prefix = "apple"
        
    response = requests.get(url + access_token)
    if response.status_code == 400:
        return Response(data={'message': 'Invalid OAuth access token.'}, status=status.HTTP_400_BAD_REQUEST)
    if response.status_code == 401:
        return Response(data={'message': 'Expected OAuth 2 access token, login cookie or other valid authentication credential.'}, status=status.HTTP_401_UNAUTHORIZED)
    
    geodata = response.json()
    name = geodata['name']
    id = geodata['id']
    social_id = prefix + id
    user = AppUser.objects.filter(email=social_id).first()
    if login_type == 1 or login_type == '1':
        img_url = geodata['']
    elif login_type == 2 or login_type == '2':
        img_url = geodata['picture']
    elif login_type == 3 or login_type == '3':
        url = url_apple
    
    if user is None:
        user = AppUser.objects.create(email=social_id, full_name= name, login_type=login_type, device_id = device_id, is_active = True, img_url = img_url)
        user.groups.set(Group.objects.filter(name = 'normal'))
        user.save()
    else:    
        user.login_type = login_type
        user.device_id = device_id
        user.save()
        
    try:
        response = messaging.subscribe_to_topic(user.device_id, 'change_topic') # change topic when license
        # print(response.success_count, 'firebase')
    except Exception as e:
        print('except: ', e)
        
    token, created = Token.objects.get_or_create(user=user)
    return Response({
            'token': token.key,
            'user': UserSerializers(user).data
    },status=status.HTTP_202_ACCEPTED)

class UserEdit(generics.RetrieveUpdateAPIView):
    pagination_class = None
    queryset = AppUser.objects.all()
    serializer_class = UserSerializers
    
@api_view(['GET'])
def transform_data_fullname(request):
    user_qs = AppUser.objects.all()
    user_list = []
    for user in user_qs:
        user.full_name = f"{user.last_name} {user.first_name}"
        user.save()
        user_list.append(user)
    return Response(data= UserSerializers(user_list, many = True).data, status= status.HTTP_200_OK)

class PresentList(generics.ListAPIView):
    pagination_class = None
    serializer_class = PresentSerializer
    queryset = Present.objects.all()
    
class ChangePresentList(generics.ListCreateAPIView):
    pagination_class = None
    serializer_class = ChangePresentSerializer
    filter_backends  = [DjangoFilterBackend, FilterByUserRequest]
    queryset = ChangePresent.objects.all()
    permission_classes = [IsUserHasPermissionsOKR]
    
class ChangePresentDetail(generics.UpdateAPIView):
    pagination_class = None
    serializer_class = ChangePresentSerializer
    filter_backends  = [DjangoFilterBackend, FilterByUserRequest]
    queryset = ChangePresent.objects.all()
    
@api_view(['POST'])
def transfer_rate_to_diamond(request):
    
    """
    rate_quantity = int
    """
    
    user = request.user
    rate_quantity = request.data.get('rate_quantity', 0)
    rate_quantity = int(rate_quantity)
    if user.pk and user.department:
        master = Master.objects.filter(company = user.department.company).first()
        if rate_quantity <= user.total_rate:
            total_diamond =  rate_quantity * master.ratio_convert_diamond
            user.diamond += int(total_diamond)
            user.total_rate = user.total_rate - total_diamond/master.ratio_convert_diamond
            user.save()
            return Response(data= UserSerializers(user).data, status=status.HTTP_200_OK)
        return Response(data={'message': 'Not enough rate to transfer'}, status=status.HTTP_400_BAD_REQUEST)
    return Response(data={'message': 'User invalid'},status=status.HTTP_400_BAD_REQUEST)
        

class PackageList(generics.ListAPIView):
    pagination_class = None
    serializer_class = PackageSerializer
    queryset = Package.objects.all()
    