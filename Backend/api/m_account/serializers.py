from django.db import models
from rest_framework import fields, serializers, validators
from rest_flex_fields.serializers import FlexFieldsModelSerializer
from api.models import AppUser, ChangePresent, Package, Present, Role
from django.db import transaction
from django.contrib.auth.models import Group

class RoleSerializers(FlexFieldsModelSerializer):
    class Meta:
        model = Role
        fields = ('role_name','role_level')

class SignupSerializer(serializers.Serializer):
    """
    Don't require email to be unique so visitor can signup multiple times,
    if misplace verification email.  Handle in view.
    """
    email = serializers.EmailField(max_length=255)
    password = serializers.CharField(max_length=128)
    full_name = serializers.CharField(max_length=250)
class UserSerializers(FlexFieldsModelSerializer):
    company = serializers.SerializerMethodField('get_company')
    account_type = serializers.SerializerMethodField('get_account_type')
    class Meta:
        model = AppUser
        # fields = "__all__"
        fields = ('id','email','account_type','fb_link','phone_number','img_url','rate_sent','rate_received','full_name',\
            'department','company','device_id','gender', 'date_of_birth', 'diamond','rate_month','total_rate', 'expired')
    def get_company(self, obj):
        if obj.department:
            return obj.department.company.id
        return None
    def get_account_type(self, obj):
        if obj.groups.count != 0:
            try:
                group_qs = obj.groups.get(name = 'premium')
                return 'premium'
            except Group.DoesNotExist:
                return 'normal'
            except Exception as e:
                print('\nEXCEPTION....: ', e)
          
        return None

class UserListSerializer(FlexFieldsModelSerializer):
    name_with_department = serializers.SerializerMethodField('get_name_with_department')

    class Meta:
        model = AppUser
        fields =('id', 'full_name', 'name_with_department', 'img_url')
    def get_name_with_department(self,obj):
        if obj.department:
            return f"[{obj.department.department_name}] {obj.full_name}"
        return None
    

class PresentSerializer(FlexFieldsModelSerializer):
    
    class Meta:
        model = Present
        fields = '__all__'

class ChangePresentSerializer(FlexFieldsModelSerializer):
    user = serializers.PrimaryKeyRelatedField(read_only = True)
    present_data  = serializers.SerializerMethodField('get_pressent_data')
    class Meta:
        model = ChangePresent
        fields = '__all__'
    def get_pressent_data(self, obj):
        return {'img_url':obj.present.img_url, 'present_name':obj.present.present_name, 'price':obj.present.price}
    def create(self, validated_data):
        present = validated_data.get('present', None)
        quantity = validated_data.get('quantity', 0)
        self_user = self.context['request'].user
        validated_data['user'] = self_user
        with transaction.atomic():
            change_present = super().create(validated_data)
            total_price = present.price * quantity
            if total_price <= self_user.total_rate:
                self_user.total_rate = self_user.total_rate - total_price
                self_user.save()
                return change_present
            raise serializers.ValidationError({'message': 'user does not enough rate'})

class PackageSerializer(FlexFieldsModelSerializer):
    class Meta:
        model = Package
        fields = '__all__'