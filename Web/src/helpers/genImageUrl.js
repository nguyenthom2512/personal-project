import { BASE_URL_IMAGE } from 'constants/api';

const genImageUrl = (url) => `${BASE_URL_IMAGE}${url}`;

export default genImageUrl;
