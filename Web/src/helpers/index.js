import { cloneDeep, isEqual, isObject, isArray } from 'lodash';
import moment from 'moment';

export const momentInstance = moment;

export const convertToDateCommon = (date, format = 'DD-MM-YYYY') => {
  return moment(date).format(format);
};

export const copyToClipboard = (text = '') => {
  const el = document.createElement('textarea');
  el.value = text;
  document.body.appendChild(el);
  el.select();
  document.execCommand('copy');
  document.body.removeChild(el);
};
export const raicType = {
  R: 0,
  A: 1,
  I: 2,
  C: 3,
};

export const CONFIDENT = ['Không ổn lắm', 'Ổn', 'Rất tốt'];
export const CHECKINSTATUS = (status, date, isDone) => {
  if (isDone) return 'Đã hoàn thành';
  const today = moment();
  let statusText = '';
  switch (status) {
    case 1:
      statusText = 'Check in đúng hạn';
      break;
    case 0:
      statusText = 'Check in muộn';
      break;
    case -1:
      statusText = 'Chưa check in';
      break;
    default:
      statusText = 'Chưa check in';
      break;
  }
  // const statusText = ['Chưa check in', 'Đã check in', 'Hoàn thành', 'Chưa hoàn thành'];
  return statusText;
};

export const filterQueryInHeaderField = (headerField = [], query = {}) => {
  const mapValHeaderField = headerField?.map((el) => el.value);
  const itemInHeaderField = {};
  const itemNotInHeaderField = {};
  Object.keys(query).forEach((key) => {
    if (mapValHeaderField.includes(query[key])) {
      itemInHeaderField[key] = query[key];
    } else {
      itemNotInHeaderField[key] = query[key];
    }
  });

  return { itemInHeaderField, itemNotInHeaderField };
};

export const findValueInOptions = (valueField = '', options = [], nameFieldOfOptions = 'value') => {
  return (
    options?.find((el) =>
      isEqual(
        (valueField || '').toString().toLocaleLowerCase(),
        (el[nameFieldOfOptions] || '').toString().toLocaleLowerCase(),
      ),
    ) || {
      label: '',
      value: '',
    }
  );
};

export const convertToFormSelect = (list, fieldForLabel = undefined, fieldForValue = undefined, noneOption = false) => {
  if (!fieldForLabel || !fieldForValue) {
    return [
      ...list.reduce((arr, el) => {
        return [...arr, { label: el, value: el }];
      }, []),
    ];
  }
  if (typeof list === 'object' && list) {
    const listReturn = [
      ...list.reduce((arr, el) => {
        return [...arr, { ...el, label: el[fieldForLabel] || 'None', value: el[fieldForValue] || '' }];
      }, []),
    ];

    if (noneOption) {
      return [{ label: 'None', value: '' }, ...listReturn];
    }
    return listReturn;
  }
  return [{ label: 'None', value: '' }, ...list];
};

export const correctBodyToRequest = (params = {}, removeNull = false) => {
  const values = cloneDeep(params || {});
  // convert object -> value when from selection
  return Object.keys(values).reduce((obj, key) => {
    obj = {
      ...obj,
      [key]: (values[key] || '').trim(),
    };

    if (isObject(values[key]) && values[key].hasOwnProperty('value')) {
      obj = {
        ...obj,
        [key]: values?.[key]?.value,
      };
    }

    if (isArray(values[key])) {
      obj = {
        ...obj,
        [key]: values[key]?.map((el) => el['value']),
      };
    }

    if (removeNull && values[key] === null) {
      delete obj[key];
    }

    return obj;
  }, {});
};

export const toggleDialog = async (open, setOpen, close, setClose) => {
  setOpen(!open);
  setTimeout(() => {
    setClose(!close);
    /**
     * Fix freeze bug - user cannot scroll
     */
    // Check if modal exist, remove class modal-open
    const noModalExist = document.querySelectorAll("body > div[tabindex='-1']").length === 0;
    if (noModalExist) {
      document.body.classList.remove('modal-open');
    }
  }, 500);
};

export const isResponseSuccess = (status = 0) => {
  return status >= 200 && status <= 299;
};

export const createRoomCall = (id) => `checkin${id}_${moment().unix()}`;

export const chooseField = (arrayData = [], label, id = 'id') => {
  return arrayData.map((elm) => {
    return {
      label: elm[label],
      value: elm[id],
    };
  });
};
export function convertParamsToQuery(param) {
  return (
    '?' +
    Object.keys(param)
      .map(function (key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(param[key]);
      })
      .join('&')
  );
}
