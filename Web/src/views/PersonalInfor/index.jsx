import React from 'react';
import AvatarPersonal from './Component/AvatarPersonal';
import FormPersonal from './Component/FormPersonal';
import authServices from 'services/authServices';
import * as Yup from 'yup';
import { Formik, Form } from 'formik';
import userServices from 'services/userServices';
import UserModel from 'models/user.model';
import { useToasts } from 'react-toast-notifications';
import { useHistory } from 'react-router';
import { RouteBase } from 'constants/routeUrl';
import { Card } from 'reactstrap';

const propTypes = {};
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const validationUserSchema = Yup.object().shape({
  date_of_birth: Yup.date().max(new Date(), 'Vui lòng chọn ngày trước hiện tại').typeError('Đây là trường bắt buộc'),
  gender: Yup.string().required('Đây là trường bắt buộc'),
  phone_number: Yup.string()
    .required('Đây là trường bắt buộc')
    .matches(phoneRegExp, 'Số điện thoại không hợp lệ')
    .min(10, 'Số điện thoại không hợp lệ')
    .max(10, 'Số điện thoại không hợp lệ'),
});
const PersonalInforPage = () => {
  //! State
  const userData = authServices.getUserLocalStorage();
  const { addToast } = useToasts();
  const history = useHistory();

  //! Function
  const onSubmit = async (values, helperFormik) => {
    const err = [];

    const newUser = UserModel.parseRequestToEdit(values);

    const responseUpdate = await userServices.updateUser(newUser);
    authServices.updateNewLocalStorage(responseUpdate.data);
    if (err.length > 0) {
      addToast(err, { appearance: 'error' });
    } else {
      addToast('Thành công!', { appearance: 'success' });
      history.push(RouteBase.Dashboard);
    }
  };

  const onChangeImage = ({ values = {}, setValues }) => async (e, ref) => {
    const imageUploaded = e?.target?.files?.[0] || '';
    ref.current.value = '';

    if (imageUploaded) {
      try {
        const response = await userServices.uploadAvatar(imageUploaded);

        //* If upload file successfully
        const linkImgUploaded = response?.data?.file;
        if (linkImgUploaded) {
          const nextData = {
            ...values,
            img_url: linkImgUploaded,
          };

          const newUser = UserModel.parseRequestToEdit(nextData);
          setValues(newUser);

          const responseUpdate = await userServices.updateUser(newUser);
          authServices.updateNewLocalStorage(responseUpdate.data);
        }
      } catch (error) {}
    }
  };

  //! Render
  return (
    <Formik
      initialValues={new UserModel(userData.user)}
      onSubmit={onSubmit}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={validationUserSchema}
    >
      {({ initialValues, values, setValues, isSubmitting, errors }) => {
        return (
          <Form>
            <div className="personal-infor-container">
              <div className="personal-infor-container-header">
                <span>Thông tin tài khoản</span>
              </div>
              {/* <Card> */}
              <div className="personal-infor-container-content">
                <AvatarPersonal
                  fullName={initialValues?.full_name}
                  email={initialValues?.email}
                  avatar={values?.img_url}
                  onChangeImage={onChangeImage({ values, setValues })}
                />

                <FormPersonal isSubmitting={isSubmitting} />
              </div>
              {/* </Card> */}
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

PersonalInforPage.propTypes = propTypes;
export default PersonalInforPage;
