import React, { useRef } from 'react';
import { ReactComponent as ImageIcon } from '../../../assets/svg/image.svg';
import { BASE_URL_IMAGE } from 'constants/api';

import PropTypes from 'prop-types';

const propTypes = {
  namePersonal: PropTypes.string,
  email: PropTypes.string,
  userData: PropTypes.object,
  onChangeImage: PropTypes.func,
};

const AvatarPersonal = ({ fullName = '', email = '', avatar = '', onChangeImage = () => {} }) => {
  //! State
  const refFileSelected = useRef(null);

  //! Function
  const handleClick = () => {
    refFileSelected.current.click();
  };

  //! Render
  return (
    <div className="align-items-center d-flex avatar-personal-container">
      <div className="avatar-personal-container_image">
        <img
          src={
            `${BASE_URL_IMAGE}${avatar}` ||
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTCoDaZN5puCmtGotL9KwkRqLG9OW1Mgb0Hog&usqp=CAU'
          }
          alt="Girl in a jacket"
          width="88"
          height="88"
        />
        <div
          className="align-items-center avatar-personal-container_image-input d-flex justify-content-center is-hover"
          onClick={handleClick}
        >
          <ImageIcon />
          <input
            ref={refFileSelected}
            type="file"
            style={{ display: 'none' }}
            onChange={(e) => onChangeImage(e, refFileSelected)}
            accept="image/png, image/gif, image/jpeg"
          />
        </div>
      </div>
      <div className="avatar-personal-container_content">
        <div className="avatar-personal-container_content-title">
          <span>{fullName}</span>{' '}
        </div>
        <div className="avatar-personal-container_content-subtitle">
          <span>{email}</span>{' '}
        </div>
      </div>
    </div>
  );
};

AvatarPersonal.propTypes = propTypes;
export default AvatarPersonal;
