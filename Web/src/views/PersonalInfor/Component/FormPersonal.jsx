import ButtonCommon from 'components/ButtonCommon';
import DateFieldFormik from 'components/CustomField/DateFieldFormik';
import InputField from 'components/CustomField/InputField';
import SelectField from 'components/CustomField/SelectField';
import { optionsGender } from 'constants/options';
import { RouteBase } from 'constants/routeUrl';
import { FastField, Field } from 'formik';
import { chooseField } from 'helpers';
import useGetCompany from 'hooks/useGetCompany';
import useGetDepartment from 'hooks/useGetDepartment';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Col, Label, Row } from 'reactstrap';

const propTypes = {};

const FormPersonal = ({ isSubmitting }) => {
  //! State
  const [dataCompany, loadingCompany, errorCompany, refetchCompany] = useGetCompany();
  const [dataDepartment, loadingDepartment, errorDepartment, refetchDepartment] = useGetDepartment();
  const history = useHistory();

  //! Function

  //! Render
  return (
    <Row xs={12} md={12} className="form-personal__container">
      <Row xs={12} sm={12}>
        <Col xs={12} sm={12}>
          <Field name="full_name" component={InputField} label={'Họ tên'} />
        </Col>
        <Col xs={12} sm={12} className="mt-1">
          <Field name="email" component={InputField} label={'Địa chỉ Email'} disabled={true} />
        </Col>
      </Row>
      <Row xs={12} sm={12}>
        <Col xs={12} sm={12}>
          <Field data={optionsGender} name="gender" component={SelectField} label={'Giới tính'} />
        </Col>
        <Col xs={12} sm={12} className="mt-1">
          <Label style={{ paddingBottom: '5px' }}>Ngày sinh</Label>
          <FastField name="date_of_birth" component={DateFieldFormik} disableFuture={true} />{' '}
        </Col>
      </Row>
      <Row xs="12" md="6">
        <Col xs={12} md={12}>
          <Field name="phone_number" component={InputField} label={'Số điện thoại'} />
        </Col>
        <Col xs="12" md="6"></Col>
      </Row>
      <Row xs={12} sm={12}>
        <Col xs={12} md={''} sm={12}>
          <Field
            name="company"
            data={chooseField(dataCompany, 'company_name')}
            component={SelectField}
            label={'Công ty'}
          />
        </Col>
        <Col xs={12} md={''} sm={12} className="department-col">
          <Field
            name="department"
            data={chooseField(dataDepartment, 'department_name')}
            component={SelectField}
            label={'Phòng ban'}
            defaultOption={'Lựa chọn phòng ban'}
          />
        </Col>
      </Row>
      <Row xs="12" md="6">
        <Col xs={12} md={3} sm={12} className="col-3 button-cancel">
          <ButtonCommon
            style={{ minWidth: '140px', height: '40px' }}
            type="button"
            name="cancel"
            label="Hủy bỏ"
            outline
            onClick={() => {
              history.push(RouteBase.Dashboard);
            }}
          />
        </Col>
        <Col xs={12} md={3} sm={12} className="col-3">
          <ButtonCommon
            isLoading={isSubmitting}
            style={{ minWidth: '140px', height: '40px' }}
            type="submit"
            name="save"
            label="Lưu lại"
          />
        </Col>
      </Row>
    </Row>
  );
};

FormPersonal.propTypes = propTypes;
export default FormPersonal;
