import ButtonCommon from 'components/ButtonCommon';
import DateTimeFieldFormik from 'components/CustomField/DateTimeFieldFormik';
import InputField from 'components/CustomField/InputField';
import { DATE_TIME_SERVER } from 'constants/date';
import { RouteBase } from 'constants/routeUrl';
import { Field, Form, Formik } from 'formik';
import { createRoomCall } from 'helpers';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { useToasts } from 'react-toast-notifications';
import { Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import roomServices from 'services/roomServices';
import * as Yup from 'yup';

const DialogCall = ({ toggle = () => {}, initData = {}, isOpen = false }) => {
  const { t } = useTranslation();
  const [initialValue, setInitValue] = useState({});

  useEffect(() => {
    const roomId = createRoomCall(initData.id);
    setInitValue({
      room_url: `${RouteBase.CallCheckinOkr}/${roomId}`,
      room_id: roomId,
      room_name: initData.object_name,
      start_time: '',
      okr_id: initData.id,
      password: '',
      checkin: '',
      user_relate: [],
    });
  }, [initData]);

  const { addToast } = useToasts();
  const history = useHistory();
  const checkInforRoom = Yup.object().shape({
    room_name: Yup.string().required(t('common:required')),
    start_time: Yup.string().required(t('common:required')),
  });
  const onSubmit = (values, { setSubmitting = () => {} }) => {
    setSubmitting(true);
    const body = {
      ...values,
      start_time: moment(values.start_time).format(DATE_TIME_SERVER),
    };
    roomServices
      .createRoom(body)
      .then((res) => {
        if (res.status === 201) {
          addToast(t('checkin:createRoomSuccess'), { appearance: 'success' });
          toggle();
          // history.push(`${RouteBase.CallCheckinOkr}/${res.data.room_id}`);
        } else {
          addToast(t('checkin:createRoomFailed'), { appearance: 'error' });
        }
      })
      .catch((err) => {
        addToast(t('checkin:createRoomFailed'), { appearance: 'error' });
      })
      .finally(() => {
        setSubmitting(false);
      });
  };

  return (
    <Modal isOpen={isOpen}>
      <Formik initialValues={initialValue} enableReinitialize onSubmit={onSubmit} validationSchema={checkInforRoom}>
        {(props) => {
          return (
            <Form>
              <ModalHeader>{t('checkin:createMeet')}</ModalHeader>
              <ModalBody>
                <Label className="w-100">{t('checkin:meetName')}</Label>
                <Field component={InputField} className="form-control" name="room_name" />
                {/* <Label className="w-100">{t('checkin:meetPassword')}</Label>
                <Field component={InputField} className="form-control" name="password" /> */}
                <Label style={{ paddingBottom: '12px' }} className="w-100 pt-1">
                  {t('checkin:meetTime')}
                </Label>
                <Field
                  component={DateTimeFieldFormik}
                  className="form-control"
                  placeholder="start_time"
                  name="start_time"
                />
              </ModalBody>
              <ModalFooter>
                <ButtonCommon onClick={props.handleSubmit} label={t('checkin:createMeet')} />
                <ButtonCommon onClick={toggle} label={t('common:cancel')} outline />
              </ModalFooter>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
};

export default DialogCall;
