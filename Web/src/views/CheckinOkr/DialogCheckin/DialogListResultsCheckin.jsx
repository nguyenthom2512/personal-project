import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import useGetOkrDetail from 'hooks/okr/useGetOkrDetail';
import { useToasts } from 'react-toast-notifications';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import LoadingIndicator from 'components/LoadingIndicator';
import { ReactComponent as GroupIcon } from '../../../assets/svg/Group.svg';
import { CONFIDENT } from 'helpers';

const propTypes = {
  idOkr: PropTypes.string,
  isOpen: PropTypes.string,
  toggle: PropTypes.func,
};

const DialogListResultsCheckin = ({ idOkr = '', isOpen, toggle }) => {
  const [okrDetail, loading, error, refetchDetail] = useGetOkrDetail({ id: idOkr, shouldTrigger: !!idOkr });
  const { addToast } = useToasts();

  //! Function

  //! Render
  return (
    <Modal isOpen={isOpen} toggle={toggle} style={{ maxWidth: '1000px', display: 'flex' }}>
      <ModalHeader>
        <div className="okr__objectname text-ellipsis">{okrDetail?.object_name}</div>
        <div className="okrDetail-header"></div>
      </ModalHeader>
      <ModalBody className="overflow-auto">
        {loading ? (
          <LoadingIndicator />
        ) : (
          <Fragment>
            <table className="table table-valign-middle">
              <thead>
                <tr>
                  <th className="text-center text-uppercase ">{'Kết quả chính'}</th>
                  <th className="text-center text-uppercase ">{'Mục tiêu'}</th>
                  <th className="text-center text-uppercase ">{'Đơn vị'}</th>
                  <th className="text-center text-uppercase ">{'Đạt được'}</th>
                  <th className="text-center text-uppercase ">{'Tiến độ'}</th>
                  <th className="text-center text-uppercase ">{'Thay đổi'}</th>
                  <th className="text-center text-uppercase ">{'Mức độ tự tin'}</th>
                  <th className="text-center text-uppercase ">{'Kế hoạch'}</th>
                  <th className="text-center text-uppercase ">{'Kết quả'}</th>
                </tr>
              </thead>
              <tbody>
                {(okrDetail?.okr_result || []).map((elm, index) => {
                  return (
                    <tr key={elm.key}>
                      <td className="text-center text-ellipsis">{elm?.key_result}</td>
                      <td className="text-center">{elm?.target}</td>
                      <td className="text-center">{elm?.unit_name}</td>
                      <td className="text-center">{elm?.current_done}</td>
                      <td className="text-center">
                        <div className="d-flex justify-content-center">
                          <div className="percent-completed">{(elm?.processed * 100)?.toFixed(2)}%</div>
                        </div>
                      </td>
                      <td className="text-center">{(elm?.percent_changed * 100)?.toFixed(2)}%</td>
                      <td className="text-center">{CONFIDENT?.[elm?.confident]}</td>
                      <td className="text-center">
                        <GroupIcon
                          type="button"
                          onClick={() => {
                            if (!elm.plan_url) return addToast('Không có link kế hoạch');
                            window.open(elm?.plan_url);
                          }}
                        />
                      </td>
                      <td className="text-center">
                        <GroupIcon
                          type="button"
                          onClick={() => {
                            if (!elm.result_url) return addToast('Không có link kết quả');
                            window.open(elm?.result_url);
                          }}
                        />
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </Fragment>
        )}
      </ModalBody>
    </Modal>
  );
};

DialogListResultsCheckin.propTypes = propTypes;
export default DialogListResultsCheckin;
