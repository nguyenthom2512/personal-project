import ButtonCommon from 'components/ButtonCommon';
import SelectField from 'components/CustomField/SelectField';
import Paging from 'components/Paging';
import { QUARTER } from 'constants/date';
import { RouteBase } from 'constants/routeUrl';
import { Field, Form, Formik } from 'formik';
import { CHECKINSTATUS, chooseField, CONFIDENT } from 'helpers';
import useGetOkrList from 'hooks/okr/useGetOkrList';
import useGetDepartment from 'hooks/useGetDepartment';
import useToggleDialog from 'hooks/useToggleDialog';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Button, Col, Row } from 'reactstrap';
import DialogCall from './Component/DialogCall';
import DialogListResultsCheckin from './DialogCheckin/DialogListResultsCheckin';

let okrDetail = {};

const CheckinOkrPage = () => {
  //!State
  const history = useHistory();
  const [param, setParam] = useState({
    page: 1,
    page_size: 5,
    department: '',
    created_time__range: '',
  });
  const { t } = useTranslation();
  const [dataDepartment] = useGetDepartment();
  const { data: okrList, loading, error, refetch } = useGetOkrList(param);
  const [okrSelected, setOkrSelected] = useState({});
  const [openListResults, toggleListResults, shouldRenderListResults] = useToggleDialog();

  const [openCall, toggleCall, shouldRenderCall] = useToggleDialog();

  //!Function
  const onSubmit = (values) => {
    setParam(values);
  };
  const onClickResult = (okr = {}) => () => {
    toggleListResults();
    setOkrSelected(okr);
  };

  const theader = () => {
    const header = [
      'Mục tiêu OKRs',
      'Kết quả chính',
      'Tiến độ',
      'Thay đổi',
      'Nhân viên',
      'Mức độ tự tin',
      'Trạng thái',
    ];
    return (
      <tr>
        {header.map((elm, index) => {
          return (
            <th key={index} className={`align-baseline${index !== 0 ? ' text-center' : ''}`}>
              {elm}
            </th>
          );
        })}
      </tr>
    );
  };

  //!Render
  return (
    <Formik initialValues={param} onSubmit={onSubmit} validateOnBlur={false} validateOnChange={false}>
      {({ values, isSubmitting }) => (
        <Form className="form-body">
          <h4 className="mb-4">Check-in</h4>
          <div className="listokr-container">
            <div className="okr-header d-flex justify-content-between">
              <Row className="field-header d-flex row" xs="12" md="3">
                <Col className="field-header__col" xs="12" sm="12">
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    component={SelectField}
                    // data={dataDepartment}
                    data={chooseField(dataDepartment, 'department_name')}
                    defaultOption="Lựa chọn"
                    onFieldChange={(elm) => onSubmit({ ...values, department: elm })}
                  />
                </Col>
                <Col xs="12" sm="12" className="field-header__col">
                  <Field
                    className="filter-time ms-2"
                    name="created_time__range"
                    component={SelectField}
                    data={QUARTER()}
                    defaultOption={t('treeOkr:selectQuarter')}
                    onFieldChange={(elm) => {
                      onSubmit({ ...values, created_time__range: elm });
                    }}
                  />
                </Col>
                {/* <Field className="form-control" name="text" type="text" placeholder="Tìm kiếm" /> */}
              </Row>
            </div>
          </div>
          <div className="listokr-container__table">
            <div className="text-okr p-lg-2 p-2">OKRs</div>
            <table style={{ width: '100%' }} className="table table-valign-middle">
              <thead>{theader()}</thead>
              <tbody>
                {(okrList?.results || []).map((elm) => {
                  return (
                    <tr key={elm.id}>
                      <td>
                        <div className="okr-result_text">{elm?.object_name}</div>
                      </td>
                      <td className="text-center">
                        <Button className="button-result" onClick={onClickResult(elm)}>
                          {(elm?.okr_result || []).length} kết quả
                        </Button>
                      </td>
                      <td className="text-center">
                        <div className="d-flex justify-content-center">
                          <div className="percent-completed">{(elm?.percent_completed * 100)?.toFixed(2)}%</div>
                        </div>
                      </td>
                      <td className="text-center">{(elm?.percent_changed * 100)?.toFixed(2)}%</td>
                      <td className="text-center">{elm?.user?.full_name}</td>
                      <td className="text-center">{CONFIDENT?.[elm?.confident]}</td>
                      <td className="text-center">
                        {!elm?.is_done && (
                          <ButtonCommon
                            className="btn-status mb-1"
                            label="Tạo phòng họp"
                            onClick={() => {
                              okrDetail = elm;
                              toggleCall();
                            }}
                            outline
                          />
                        )}
                        <Button
                          type="button"
                          className="btn-status"
                          onClick={() => history.push(RouteBase.CheckinDetailWithID(elm.id))}
                        >
                          {CHECKINSTATUS(
                            elm?.last_checkin?.checkin_status,
                            elm?.last_checkin?.checkin_date,
                            elm?.is_done,
                          )}
                        </Button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          <Paging
            count={okrList.count}
            onSelectPage={(page) => setParam({ ...param, page })}
            pageSize={param.page_size}
          />
          {shouldRenderListResults && (
            <DialogListResultsCheckin isOpen={openListResults} toggle={toggleListResults} idOkr={okrSelected?.id} />
          )}
          {shouldRenderCall && <DialogCall isOpen={openCall} toggle={toggleCall} initData={okrDetail} />}
        </Form>
      )}
    </Formik>
  );
};
export default CheckinOkrPage;
