import { Link } from '@material-ui/core';
import ButtonCommon from 'components/ButtonCommon';
import SelectField from 'components/CustomField/SelectField';
import NoteChart from 'components/NoteChart';
import {
  backgroundColor,
  backgroundColorChartBar,
  borderColor,
  borderColorChartBar,
  labelChartItem,
} from 'constants/chart';
import { FORMAT_QUARTER } from 'constants/date';
import { Field, Form, Formik } from 'formik';
import { chooseField } from 'helpers';
import useDataCreateOKR from 'hooks/useGetDataCreateOKR';
import useGetDataReportCheckin from 'hooks/useGetDataReportCheckin';
import useGetDataUserReportCheckin from 'hooks/useGetDataUserReportCheckin';
import useGetDataUserReport from 'hooks/useGetDataUserReportOkr';
import useGetDataReport from 'hooks/useGetReportOkr';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { Bar, Pie } from 'react-chartjs-2';
import { MdRefresh } from 'react-icons/md';
import { Row } from 'reactstrap';
import authServices from 'services/authServices';
import DateFieldFormik from '../../components/CustomField/DateFieldFormik';

const propTypes = {};
const statuses = [-1, 0, 1, 2];
const quickFilter = [
  {
    label: 'Ngày',
    value: `${moment().startOf('day').format(FORMAT_QUARTER)},${moment().endOf('day').format(FORMAT_QUARTER)}`,
  },
  {
    label: 'Tuần',
    value: `${moment().startOf('week').format(FORMAT_QUARTER)},${moment().endOf('week').format(FORMAT_QUARTER)}`,
  },
  {
    label: 'Tháng',
    value: `${moment().startOf('month').format(FORMAT_QUARTER)},${moment().endOf('month').format(FORMAT_QUARTER)}`,
  },
];
const ReportPage = (props) => {
  //! State
  const labels = ['OKRs'];
  const userData = authServices.getUserLocalStorage();
  const [param, setParam] = useState({
    department: 1,
    is_done: true,
    created_time__range: '',
    user: userData?.user?.id,
  });
  const [dataReportOkr, loadingReportOkr, errorReportOkr, refetchReportOkr] = useGetDataReport(param);
  const [dataUserReportOkr, , , refetchUserReportOkr] = useGetDataUserReport(param);
  const [dataReportCheckin, , , refetchReportCheckin] = useGetDataReportCheckin(param);
  const [, , , refetchUserReportCheckin] = useGetDataUserReportCheckin(param);
  const [dataOKR, , ,] = useDataCreateOKR();
  const [isOpen, setIsOpen] = useState(false);

  //! Function
  const convertDataReportOkr = (arrOkr, type) => {
    const dataReport = [];
    statuses.forEach((element) => {
      const statusArray = arrOkr.filter((el) => {
        const valueCheck = type === 'confident' ? el?.confident : el?.last_checkin?.checkin_status;
        if (element === -1) {
          return !element || valueCheck === element;
        }
        return element === valueCheck;
      });
      dataReport.push(Math.round((statusArray.length / arrOkr.length) * 100));
    });
    return dataReport;
  };
  useEffect(() => {
    if (dataReportOkr.length > 0) {
      convertData(dataReportOkr);
    }
  }, [dataReportOkr]);
  const convertData = (array) => {
    const dataConverted = [];
    statuses.forEach((element) => {
      const statusArray = array.filter((el) => el.checkin_status === element);
      dataConverted.push(Math.round((statusArray.length / array.length) * 100));
    });
    return dataConverted;
  };
  useEffect(() => {
    if (dataReportCheckin.length > 0) {
      convertData(dataReportCheckin);
    }
  }, [dataReportCheckin]);
  const onSubmit = (value) => {};
  const processData = (values) => {
    if ((values.startTime && values.endTime) || values.department) {
      const startTime = moment(values.startTime).format(FORMAT_QUARTER);
      const endTime = moment(values.endTime).format(FORMAT_QUARTER);
      const created_time__range = `${startTime},${endTime}`;
      const paramsApi = {
        is_done: true,
      };
      if (!!values?.department) {
        paramsApi.department = values.department;
      }
      if (values.startTime && values.endTime) {
        paramsApi.created_time__range = created_time__range;
      }
      return paramsApi;
    }
  };
  const onClickOpen = (tab) => {
    setIsOpen(tab);
  };
  //! Render
  if (loadingReportOkr) {
    return <span>Loading...</span>;
  }
  if (errorReportOkr) {
    return <span style={{ color: 'red' }}>{errorReportOkr.toString()}</span>;
  }
  return (
    <div className="chartOkr-container">
      <Formik
        initialValues={{ startTime: '', endTime: '', department: '' }}
        onSubmit={onSubmit}
        validateOnBlur={false}
        enableReinitialize
        validateOnChange={false}
      >
        {({ values, setFieldValue, errors, setValues, isSubmitting, handleSubmit, touched }) => {
          return (
            <Form>
              <Row className="chartOkr-container-header">
                <div className="align-items-center d-flex p-0">
                  <div className="chartOkr-container-header_title">Báo cáo</div>
                  <ButtonCommon
                    onClick={() => onClickOpen(false)}
                    className="rounded-0 chartOkr-container-header_btnOkr"
                    label="OKRs"
                    outline={isOpen}
                  />{' '}
                  <ButtonCommon
                    onClick={() => onClickOpen(true)}
                    className="rounded-0 chartOkr-container-header_btnCfrs"
                    label="CFRs"
                    outline={!isOpen}
                  />
                </div>
              </Row>
              {!isOpen && (
                <div>
                  <div className="chartOkr-container-content">
                    <Row className="chartOkr-container-content_title">
                      <div className="align-items-center d-flex p-0">
                        <div className="subtitle">Tiến độ OKRs</div>
                        <div className="form-group">
                          <Field
                            defaultOption="Chọn phòng ban"
                            className="form-control"
                            component={SelectField}
                            name="department"
                            data={chooseField(dataOKR.Department, 'department_name')}
                            onFieldChange={(value) => {
                              refetchReportOkr(processData({ ...values, department: value }));
                              const newValue = { ...values };
                              delete newValue.department;
                              refetchUserReportOkr({
                                ...processData({ ...newValue }),
                                ...{ user: userData?.user?.id },
                              });
                            }}
                          />
                        </div>
                        <div className="align-items-center d-flex form-group ms-3">
                          <span className="me-2 time">Chọn thời gian bắt đầu</span>
                          <Field
                            defaultOption="Chọn thời gian"
                            className="form-control rounded-0"
                            component={DateFieldFormik}
                            maxDate={new Date()}
                            name="startTime"
                            onChange={(value) => {
                              if (!moment(value).isValid()) {
                                return;
                              }
                              const newValue = { ...values };
                              delete newValue.department;
                              if (!!values.endTime && moment(values.endTime).isValid()) {
                                refetchReportOkr(processData({ ...newValue, startTime: value }));
                                refetchUserReportOkr({
                                  ...processData({ ...newValue }),
                                  ...{ user: userData?.user?.id },
                                });
                              }
                            }}
                          />
                        </div>
                        <div className="align-items-center d-flex form-group ms-3">
                          <span className="me-2 time">Chọn thời gian kết thúc</span>
                          <Field
                            defaultOption="Chọn thời gian"
                            className="form-control rounded-0"
                            component={DateFieldFormik}
                            minDate={new Date()}
                            name="endTime"
                            onChange={(value) => {
                              if (!moment(value).isValid()) {
                                return;
                              }
                              const newValue = { ...values };
                              delete newValue.department;
                              if (!!values.startTime && moment(values.startTime).isValid()) {
                                refetchReportOkr(processData({ ...newValue, endTime: value }));
                                refetchUserReportOkr({
                                  ...processData({ ...newValue }),
                                  ...{ user: userData?.user?.id },
                                });
                              }
                            }}
                          />
                        </div>
                        <div className="ms-3 me-4">
                          <MdRefresh
                            onClick={() => {
                              if (values.department) {
                                refetchReportOkr({ department: values.department });
                              }
                              refetchUserReportOkr({
                                user: userData?.user?.id,
                              });
                              setValues({ ...values, ...{ startTime: '', endTime: '' } });
                            }}
                            size="20"
                          />{' '}
                        </div>
                      </div>
                      <div className="d-flex justify-content-lg-end list-time ps-0 pt-3">
                        {quickFilter.map((el, index) => {
                          return (
                            <>
                              {index > 0 && <Link className="list pe-2">|</Link>}
                              <Link
                                onClick={() => {
                                  const paramsObject = {
                                    created_time__range: el.value,
                                  };
                                  if (values.department) {
                                    paramsObject.department = values.department;
                                  }
                                  refetchReportOkr(paramsObject);
                                  refetchUserReportOkr({
                                    user: userData?.user?.id,
                                    created_time__range: el.value,
                                  });
                                }}
                                className="list pe-2"
                              >
                                {el.label}
                              </Link>
                            </>
                          );
                        })}
                      </div>
                      <div className="align-items-center chart d-flex p-0">
                        <div className="chartOkr-container-content_title-chartBar col-4">
                          <div className="border-bottom">Theo phòng ban</div>
                          <Bar
                            data={{
                              labels: ['', '', '', '', '', '', '', '', ''],
                              datasets: [
                                {
                                  label: [],
                                  data: [
                                    ...convertDataReportOkr(dataReportOkr, 'percent'),
                                    convertDataReportOkr(dataReportOkr)[0],
                                    ...convertDataReportOkr(dataReportOkr, 'confident'),
                                  ],
                                  backgroundColor: backgroundColorChartBar,
                                  borderColor: borderColorChartBar,
                                  borderWidth: 1,
                                },
                              ],
                            }}
                            width="160"
                            height="160"
                            options={{
                              maintainAspectRatio: true,
                              plugins: {
                                legend: {
                                  display: false,
                                },
                              },
                              tooltips: {
                                callbacks: {
                                  label: function (tooltipItem) {
                                    return tooltipItem.yLabel;
                                  },
                                },
                              },
                            }}
                          />

                          <div className="d-flex align-items-center mb-3 ms-3">
                            <div className="text-center w-50">
                              <p className="ps-3">Tiến độ</p>
                            </div>
                            <div className="text-center w-50">
                              <p>Tự tin</p>
                            </div>
                          </div>

                          <div className="d-flex align-items-center mb-3 ">
                            <div className="ms-4">
                              <NoteChart label={'OKRs tiến độ 0%'} type={0} />
                              <NoteChart label={'OKRs tiến độ 1-40%'} type={1} />
                            </div>
                            <div className="ms-4">
                              <NoteChart label={'OKRs tiến độ 41-70%'} type={2} />
                              <NoteChart type={3} label={'OKRs tiến độ trên 70%'} />
                            </div>
                          </div>
                        </div>
                        <div className="chartOkr-container-content_title-chartBar col-4">
                          <div className="border-bottom">Theo cá nhân</div>{' '}
                          <Pie
                            data={{
                              labels: labelChartItem,
                              datasets: [
                                {
                                  label: '# of Votes',
                                  data: convertDataReportOkr(dataUserReportOkr, 'percent'),
                                  backgroundColor: backgroundColor,
                                  borderColor: borderColor,
                                  borderWidth: 1,
                                },
                              ],
                            }}
                            width="180"
                            height="180"
                            options={{
                              maintainAspectRatio: true,
                              plugins: {
                                legend: {
                                  display: false,
                                },
                              },
                              tooltips: {
                                callbacks: {
                                  label: function (tooltipItem) {
                                    return tooltipItem.yLabel;
                                  },
                                },
                              },
                            }}
                          />
                          <div className="d-flex align-items-center mb-3 ms-3">
                            <div className="text-center w-50">
                              <p className="ps-3 text-white">Tiến độ</p>
                            </div>
                            <div className="text-center w-50">
                              <p className="ps-3 text-white">Tự tin</p>
                            </div>
                          </div>
                          <div className="d-flex align-items-center mb-3 ">
                            <div className="ms-4">
                              <NoteChart label={'OKRs tiến độ 0%'} type={0} />
                              <NoteChart label={'OKRs tiến độ 1-40%'} type={1} />
                            </div>
                            <div className="ms-4">
                              <NoteChart label={'OKRs tiến độ 41-70%'} type={2} />
                              <NoteChart type={3} label={'OKRs tiến độ trên 70%'} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </Row>
                  </div>
                  <Row className="chartOkr-container-header_text">
                    <div className="chartOkr-container-header_text-span">Kỷ luật checkin</div>
                  </Row>
                  <div className="chartOkr-container-content">
                    <Row className="chartOkr-container-content_title">
                      <div className="align-items-center d-flex p-0">
                        <div className="subtitle">Thống kê checkin</div>
                        <div className="align-items-center d-flex form-group ms-3">
                          <span className="me-2 time">Chọn thời gian bắt đầu</span>
                          <Field
                            defaultOption="Chọn thời gian"
                            className="form-control rounded-0"
                            component={DateFieldFormik}
                            maxDate={new Date()}
                            name="startTime"
                            onChange={(value) => {
                              if (!!values.endTime) {
                                if (!moment(value).isValid()) {
                                  return;
                                }
                                const newValue = { ...values };
                                delete newValue.department;
                                refetchReportCheckin(processData({ ...newValue, startTime: value }));

                                refetchUserReportCheckin({
                                  ...processData({ ...newValue }),
                                  ...{ user: userData?.user?.id },
                                });
                              }
                            }}
                          />
                        </div>
                        <div className="align-items-center d-flex form-group ms-3">
                          <span className="me-2 time">Chọn thời gian kết thúc</span>
                          <Field
                            defaultOption="Chọn thời gian"
                            className="form-control rounded-0"
                            component={DateFieldFormik}
                            minDate={new Date()}
                            name="endTime"
                            onChange={(value) => {
                              if (!!values.startTime) {
                                if (!moment(value).isValid()) {
                                  return;
                                }
                                const newValue = { ...values };
                                delete newValue.department;
                                refetchReportCheckin(processData({ ...newValue, endTime: value }));
                                refetchUserReportCheckin({
                                  ...processData({ ...newValue }),
                                  ...{ user: userData?.user?.id },
                                });
                              }
                            }}
                          />
                        </div>
                        <div className="clear-time me-4">
                          <MdRefresh
                            size="20"
                            onClick={() => {
                              refetchReportCheckin();
                              refetchUserReportCheckin({
                                user: userData?.user?.id,
                              });
                              setValues({ ...values, ...{ startTime: '', endTime: '' } });
                            }}
                          />{' '}
                        </div>
                      </div>
                      <div className="d-flex justify-content-lg-end list-time ps-0 pt-3">
                        {quickFilter.map((el, index) => {
                          return (
                            <>
                              {index > 0 && <Link className="list pe-2">|</Link>}
                              <Link
                                onClick={() => {
                                  const paramsObject = {
                                    created_time__range: el.value,
                                  };
                                  if (values.department) {
                                    paramsObject.department = values.department;
                                  }
                                  refetchReportCheckin(paramsObject);
                                  refetchUserReportCheckin({
                                    user: userData?.user?.id,
                                    created_time__range: el.value,
                                  });
                                }}
                                className="list pe-2"
                              >
                                {el.label}
                              </Link>
                            </>
                          );
                        })}
                      </div>
                      <div className="align-items-center chart d-flex p-0">
                        <div className="chartOkr-container-content_title-chartBar col-4">
                          <div className="border-bottom">Theo phòng ban</div>
                          <Bar
                            data={{
                              labels: labels,
                              datasets: [
                                {
                                  label: 'Chưa thực hiện Check-in',
                                  data: [convertData(dataReportCheckin)[0]],
                                  backgroundColor: ['rgba(255, 99, 132, 0.2)'],
                                },
                                {
                                  label: 'Quá hạn Check-in',
                                  data: [convertData(dataReportCheckin)[1]],
                                  backgroundColor: ['rgba(54, 162, 235, 0.2)'],
                                },
                                {
                                  label: 'Check-in muộn',
                                  data: [convertData(dataReportCheckin)[2]],
                                  backgroundColor: ['rgba(255, 206, 86, 0.2)'],
                                },
                                {
                                  label: 'Check-in đúng hạn',
                                  data: [convertData(dataReportCheckin)[3]],
                                  backgroundColor: ['rgba(153, 102, 255, 0.2)'],
                                },
                              ],
                            }}
                            options={{
                              plugins: {
                                title: {
                                  display: false,
                                },
                                legend: {
                                  display: false,
                                },
                              },
                              responsive: true,
                              scales: {
                                x: {
                                  stacked: true,
                                },
                                y: {
                                  stacked: true,
                                },
                              },
                            }}
                            width="180"
                            height="180"
                          />
                          <div className="d-flex align-items-center mb-3 ">
                            <div className="ms-4">
                              <NoteChart label={'Chưa thực hiện Check-in'} type={0} />
                              <NoteChart label={'Quá hạn Check-in'} type={1} />
                            </div>
                            <div className="ms-4">
                              <NoteChart label={'Check-in muộn'} type={2} />
                              <NoteChart type={3} label={'Check-in đúng hạn'} />
                            </div>
                          </div>
                        </div>
                        <div className="chartOkr-container-content_title-chartBar col-4">
                          <div className="border-bottom">Theo cá nhân</div>
                          <Bar
                            data={{
                              labels: labels,
                              datasets: [
                                {
                                  label: 'Chưa thực hiện Check-in',
                                  data: [convertData(dataReportCheckin)[0]],
                                  backgroundColor: ['rgba(255, 99, 132, 0.2)'],
                                },
                                {
                                  label: 'Quá hạn Check-in',
                                  data: [convertData(dataReportCheckin)[1]],
                                  backgroundColor: ['rgba(54, 162, 235, 0.2)'],
                                },
                                {
                                  label: 'Check-in muộn',
                                  data: [convertData(dataReportCheckin)[2]],
                                  backgroundColor: ['rgba(255, 206, 86, 0.2)'],
                                },
                                {
                                  label: 'Check-in đúng hạn',
                                  data: [convertData(dataReportCheckin)[3]],
                                  backgroundColor: ['rgba(153, 102, 255, 0.2)'],
                                },
                              ],
                            }}
                            options={{
                              plugins: {
                                title: {
                                  display: false,
                                },
                                legend: {
                                  display: false,
                                },
                              },
                              responsive: true,
                              scales: {
                                x: {
                                  stacked: true,
                                },
                                y: {
                                  stacked: true,
                                },
                              },
                            }}
                            width="200"
                            height="200"
                          />
                          <div className="d-flex align-items-center mb-3 ">
                            <div className="ms-4">
                              <NoteChart label={'Chưa thực hiện Check-in'} type={0} />
                              <NoteChart label={'Quá hạn Check-in'} type={1} />
                            </div>
                            <div className="ms-4">
                              <NoteChart label={'Check-in muộn'} type={2} />
                              <NoteChart type={3} label={'Check-in đúng hạn'} />
                            </div>
                          </div>
                        </div>
                      </div>
                    </Row>
                  </div>
                </div>
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

ReportPage.propTypes = propTypes;
export default ReportPage;
