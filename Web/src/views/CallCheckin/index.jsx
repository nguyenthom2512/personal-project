import { RouteBase } from 'constants/routeUrl';
import { CONFIG_JITSI, DOMAIN_JITSI, INTERFACE_CONFIG } from 'helpers/jitsiConfig';
import useGetRoomDetail from 'hooks/room/useGetRoomDetail';
import useToggleDialog from 'hooks/useToggleDialog';
import { isEmpty } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useHistory, useParams } from 'react-router';
import authServices from 'services/authServices';
import CommandBar from './components/CommandBar';

let api = null;

const CallCheckin = (props) => {
  const params = useParams();
  const room_id = params?.id;
  const history = useHistory();
  const [data, , , refetch] = useGetRoomDetail(room_id);
  const [, toggle, shouldRender] = useToggleDialog();
  const user = authServices.getUserLocalStorage();
  const [isAudioMuted, setAudioMute] = useState(true);
  const [isVideoMuted, setVideoMute] = useState(true);
  useEffect(() => {
    if (isEmpty(data)) {
      refetch();
    } else {
      if (window.JitsiMeetExternalAPI) {
        startMeet();
      } else {
        alert('Lỗi khi khởi tạo cuộc gọi');
      }
    }
  }, [data]);

  const startMeet = () => {
    const options = {
      roomName: data?.room_id,
      width: '100%',
      height: 500,
      configOverwrite: CONFIG_JITSI,
      interfaceConfigOverwrite: INTERFACE_CONFIG,
      parentNode: document.querySelector('#jitsi-iframe'),
      userInfo: {
        displayName: user?.user?.full_name,
      },
    };
    api = new window.JitsiMeetExternalAPI(DOMAIN_JITSI, options);

    api.addEventListeners({
      readyToClose: handleClose,
      participantLeft: handleParticipantLeft,
      participantJoined: handleParticipantJoined,
      videoConferenceJoined: handleVideoConferenceJoined,
      videoConferenceLeft: handleVideoConferenceLeft,
      audioMuteStatusChanged: handleMuteStatus,
      videoMuteStatusChanged: handleVideoStatus,
    });
    toggle();
  };

  const handleClose = () => {};

  const handleParticipantLeft = async (participant) => {};

  const handleParticipantJoined = async (participant) => {
    // const data = await getParticipants();
  };

  const handleVideoConferenceJoined = async (participant) => {
    // const data = await getParticipants();
  };

  const handleVideoConferenceLeft = () => {
    // return props.history.push('/thank-you');
  };

  const handleMuteStatus = (audio) => {
    setAudioMute(audio.muted);
  };

  const handleVideoStatus = (video) => {
    setVideoMute(video.muted);
  };

  const onEndCall = () => {
    history.push(RouteBase.CheckinDetailWithID(data?.okr_id));
  };

  return (
    <div>
      <div id="jitsi-iframe" />
      {shouldRender && (
        <CommandBar jisiApi={api} onEndCall={onEndCall} isAudioMuted={isAudioMuted} isVideoMuted={isVideoMuted} />
      )}
    </div>
  );
};

export default CallCheckin;
