import React, { useState } from 'react';
import { BiChat, BiMicrophone, BiMicrophoneOff } from 'react-icons/bi';
import { FiCamera, FiCameraOff, FiPhoneOff } from 'react-icons/fi';

const COMMAND = {
  TOOGLE_AUDIO: 'toggleAudio',
  TOOGLE_VIDEO: 'toggleVideo',
  TOOGLE_CHAT: 'toggleChat',
  HANGUP: 'hangup',
};

const defaultSize = 24;

const CommandBar = ({ jisiApi = null, onEndCall = () => {}, isAudioMuted, isVideoMuted }) =>
  // setAudioMute = () => {},
  // setVideoMute = () => {},
  {
    // const [isAudioMuted, setAudioMute] = useState(false);
    // const [isVideoMuted, setVideoMute] = useState(false);

    const executeCommand = (command) => {
      jisiApi.executeCommand(command);
      switch (command) {
        // case COMMAND.TOOGLE_AUDIO:
        //   setAudioMute(isAudioMuted);
        //   break;
        // case COMMAND.TOOGLE_VIDEO:
        //   setVideoMute(isVideoMuted);
        //   break;
        case COMMAND.HANGUP:
          onEndCall();
          break;
        default:
          break;
      }
    };

    const WrapCommand = ({ children }) => <div className="p-2 cursor-pointer">{children}</div>;
    const micCommand = () => {
      return isAudioMuted ? (
        <BiMicrophoneOff size={defaultSize} onClick={() => executeCommand(COMMAND.TOOGLE_AUDIO)} />
      ) : (
        <BiMicrophone size={defaultSize} onClick={() => executeCommand(COMMAND.TOOGLE_AUDIO)} />
      );
    };
    const camCommand = () => {
      return isVideoMuted ? (
        <FiCameraOff size={defaultSize} onClick={() => executeCommand(COMMAND.TOOGLE_VIDEO)} />
      ) : (
        <FiCamera size={defaultSize} onClick={() => executeCommand(COMMAND.TOOGLE_VIDEO)} />
      );
    };
    return (
      <div className="bg-body d-flex justify-content-evenly rounded-2 shadow-sm">
        <WrapCommand>{micCommand()}</WrapCommand>
        <WrapCommand>{camCommand()}</WrapCommand>
        <WrapCommand>
          <BiChat size={defaultSize} onClick={() => executeCommand(COMMAND.TOOGLE_CHAT)} />
        </WrapCommand>
        <WrapCommand>
          <FiPhoneOff size={defaultSize} onClick={() => executeCommand(COMMAND.HANGUP)} />
        </WrapCommand>
      </div>
    );
  };

export default CommandBar;
