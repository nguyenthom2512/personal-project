import ButtonCommon from 'components/ButtonCommon';
import ErrorMessages from 'components/CustomField/ErrorMessage';
import RadioField from 'components/CustomField/RadioField';
import LoadingIndicator from 'components/LoadingIndicator';
import { CONFIDENT } from 'constants/confident';
import { Field, Formik } from 'formik';
import { isEmpty, isObject } from 'lodash-es';
import React, { Fragment, useEffect, useState } from 'react';
import { Input, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row } from 'reactstrap';
import checkinDetailService from 'services/checkinDetailService';
const DiaLogDetail = ({ isOpen = false, setIsOpen, id, CheckinDetail = [], values }) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    if (!id || isEmpty(id + '') || !isOpen) return;
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await checkinDetailService.getCheckinDetail(id);
        //* api
        if (isObject(response?.data)) {
          setData(response?.data);
        }
        setLoading(false);
      } catch (error) {
        setLoading(false);
      }
    };

    fetchData();
  }, [isOpen]);

  return (
    <Modal isOpen={isOpen} size={'xl'} scrollable style={{ overflow: 'hidden', zIndex: 100 }}>
      <ModalHeader>Lịch sử checkin</ModalHeader>
      <ModalBody>
        {loading ? (
          <LoadingIndicator />
        ) : (
          <Fragment>
            <Formik initialValues={{}}>
              {() => (
                <>
                  <Row className="checkin-container-table">
                    <table className="table table-borderless">
                      <thead>
                        <tr className="border-bottom">
                          {CheckinDetail.map((el) => {
                            return (
                              <th key={el} className="th-table">
                                {el}
                              </th>
                            );
                          })}
                        </tr>
                      </thead>
                      <tbody>
                        {(data?.checkin_result || []).map((el, index) => {
                          return (
                            <tr key={el.id} className="border-bottom tr-table">
                              <td className="td-list">
                                <Input type="checkbox" disabled={true} checked={true} className="position-relative" />
                                <Label className="text-ellipsis" style={{ marginLeft: 5 }}>
                                  {el?.result_data[0]?.key_result || ''}
                                </Label>
                              </td>
                              <td className="td-list">
                                <Input
                                  value={el?.result_data[0]?.target || ''}
                                  disabled
                                  className="position-relative"
                                  style={{ height: '37.99px' }}
                                />
                              </td>
                              <td className="td-list">
                                <Input value={el?.total_done || ''} disabled style={{ height: '37.99px' }} />
                              </td>
                              <td className="text-center processed">
                                <div className="processed-completed">
                                  <span className="processed-completed-span">
                                    {' '}
                                    {(el?.result_data[0].processed * 100)?.toFixed(2)}%
                                  </span>
                                </div>
                              </td>

                              <td className="td-list">
                                <Field
                                  disabled={true}
                                  component={RadioField}
                                  checked={el?.confident + '' === CONFIDENT.good}
                                  label="Rất tốt"
                                  isValidate={true}
                                  name="confident"
                                />
                                <Field
                                  disabled={true}
                                  component={RadioField}
                                  checked={el.confident + '' === CONFIDENT.normal}
                                  label="Ổn"
                                  isValidate={true}
                                  name="confident"
                                />
                                <Field
                                  disabled={true}
                                  component={RadioField}
                                  checked={el?.confident + '' === CONFIDENT.bad}
                                  label="Không ổn lắm"
                                  value={CONFIDENT.bad}
                                  isValidate={true}
                                  name="confident"
                                />
                                <ErrorMessages name={`okr_result[${index}].confident`} />
                              </td>
                              <td>
                                <textarea value={el?.process_note} className={`form-control textarea-field`} disabled />
                              </td>
                              <td>
                                <textarea value={el?.overdue_note} className={`form-control textarea-field`} disabled />
                              </td>
                              <td>
                                <textarea value={el?.trouble_note} className={`form-control textarea-field`} disabled />
                              </td>
                              <td>
                                <textarea
                                  value={el?.solution_note}
                                  className={`form-control textarea-field`}
                                  disabled
                                />
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </Row>
                </>
              )}
            </Formik>
          </Fragment>
        )}
      </ModalBody>
      <ModalFooter>
        <ButtonCommon
          onClick={() => {
            setIsOpen(!isOpen);
          }}
          label="Đóng"
          outline
        />
      </ModalFooter>
    </Modal>
  );
};

export default DiaLogDetail;
