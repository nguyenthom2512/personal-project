import _ from 'lodash';
import moment from 'moment';
import React, { useState } from 'react';
import { Button, Row } from 'reactstrap';
import DiaLogDetail from './DialogDetail';
const HistoryCheckin = ({ dataHistory, values, CheckinDetail }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [id, setId] = useState();
  return (
    <Row className="checkin-container-table">
      <table className="table table-borderless">
        <thead>
          <tr>
            {dataHistory.map((el) => {
              return (
                <th key={el} className="th-table ">
                  {el}
                </th>
              );
            })}
            <th className="th-table">Hành động</th>
          </tr>
        </thead>
        {_.isEmpty(values?.ok_checkin) ? (
          <tbody className="body-history">
            <span>Chưa có thông tin Check-in</span>
          </tbody>
        ) : (
          <tbody>
            {values?.ok_checkin?.map((el, index) => {
              return (
                <tr key={el.id} className="border-bottom tr-table">
                  <td className="t-lists">{moment(el?.created_time).format('DD/MM/YYYY')}</td>
                  <td className="t-lists">Check-in {moment(el?.checkin_date).format('DD/MM/YYYY')}</td>
                  <td className="t-lists">
                    <Button type="button" className="is-done" disabled>
                      Đã duyệt
                    </Button>
                  </td>
                  <td className="t-lists">{el?.is_done ? 'Hoàn thành' : 'Chưa hoàn thành'}</td>
                  <td className="t-lists">
                    <Button
                      type="button"
                      className="is-done"
                      onClick={() => {
                        setIsOpen(true);
                        setId(el.id);
                      }}
                    >
                      Xem chi tiết
                    </Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        )}
      </table>
      <DiaLogDetail id={id} isOpen={isOpen} setIsOpen={setIsOpen} values={values} CheckinDetail={CheckinDetail} />
    </Row>
  );
};
export default HistoryCheckin;
