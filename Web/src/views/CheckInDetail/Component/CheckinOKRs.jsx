import ButtonCommon from 'components/ButtonCommon';
import CheckBoxFormikField from 'components/CustomField/CheckBoxFormikField';
import DateFieldFormik from 'components/CustomField/DateFieldFormik';
import ErrorMessages from 'components/CustomField/ErrorMessage';
import InputField from 'components/CustomField/InputField';
import RadioField from 'components/CustomField/RadioField';
import TextAreaField from 'components/CustomField/TextAreaField';
import { CONFIDENT } from 'constants/confident';
import { Field } from 'formik';
import { toString } from 'lodash-es';
import React from 'react';
import { Col, Row } from 'reactstrap';
const CheckinOKRs = ({ dataDetail, values, handleSubmit, CheckinDetail, setFieldValue }) => {
  return (
    <>
      <Row className="checkin-container-table">
        <table className="table table-borderless">
          <thead>
            <tr className="border-bottom">
              {CheckinDetail.map((el) => {
                return (
                  <th key={el} className="th-table">
                    {el}
                  </th>
                );
              })}
            </tr>
          </thead>
          <tbody>
            {(values?.okr_result || []).map((el, index) => {
              const matchedIndex = dataDetail?.ok_checkin[0];
              const isDisable = matchedIndex?.checkin_result?.find((elm) => elm.result === el.id)?.is_done;
              return (
                <tr key={el.id} className="border-bottom tr-table">
                  <td className="td-list">
                    <Field
                      disabled={isDisable}
                      defaultChecked={values.okr_result[index].is_done}
                      component={CheckBoxFormikField}
                      name={`okr_result[${index}].is_done`}
                      label={el?.key_result}
                    />
                  </td>
                  <td className="td-list">
                    <Field disabled component={InputField} className="btn-list" name={`okr_result[${index}].target`} />
                  </td>
                  <td className="td-list">
                    <Field
                      disabled={isDisable}
                      component={InputField}
                      className="btn-list"
                      name={`okr_result[${index}].total_done`}
                      onChangeCustomize={(e) => {
                        setFieldValue(`okr_result[${index}].total_done`, e.target.value);
                        setFieldValue(
                          `okr_result[${index}].processed`,
                          Number(e.target.value) / Number(values.okr_result[index].target),
                        );
                      }}
                    />
                  </td>
                  <td className="text-center processed">
                    <div className="processed-completed">
                      <span className="processed-completed-span"> {(el?.processed * 100)?.toFixed(2)}%</span>
                    </div>
                  </td>

                  <td className="td-list">
                    <Field
                      disabled={isDisable}
                      component={RadioField}
                      name={`okr_result[${index}].confident`}
                      checked={values.okr_result[index]?.confident === CONFIDENT.good}
                      label="Rất tốt"
                      value={CONFIDENT.good}
                      isValidate={true}
                      id={`good[${index}].0`}
                    />
                    <Field
                      disabled={isDisable}
                      component={RadioField}
                      name={`okr_result[${index}].confident`}
                      checked={values.okr_result[index]?.confident === CONFIDENT.normal}
                      label="Ổn"
                      value={CONFIDENT.normal}
                      isValidate={true}
                      id={`normal[${index}].1`}
                    />
                    <Field
                      disabled={isDisable}
                      component={RadioField}
                      name={`okr_result[${index}].confident`}
                      checked={values.okr_result[index]?.confident === CONFIDENT.bad}
                      label="Không ổn lắm"
                      value={CONFIDENT.bad}
                      isValidate={true}
                      id={`bad[${index}].2`}
                    />
                    <ErrorMessages name={`okr_result[${index}].confident`} />
                  </td>
                  <td>
                    <Field
                      disabled={isDisable}
                      className="textarea-field"
                      component={TextAreaField}
                      name={`okr_result[${index}].process_note`}
                    />
                  </td>
                  <td>
                    <Field
                      disabled={isDisable}
                      className="textarea-field"
                      component={TextAreaField}
                      name={`okr_result[${index}].overdue_note`}
                    />
                  </td>
                  <td>
                    <Field
                      disabled={isDisable}
                      className="textarea-field"
                      component={TextAreaField}
                      name={`okr_result[${index}].trouble_note`}
                    />
                  </td>
                  <td>
                    <Field
                      disabled={isDisable}
                      className="textarea-field"
                      component={TextAreaField}
                      name={`okr_result[${index}].solution_note`}
                    />
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </Row>
      <Row className="checkin-container-confident">
        <div className="d-flex confident-checkin">
          <div className="confident-checkin-text">Chọn mức độ tự tin hoàn thành của mục tiêu:</div>
          <div className="confident-checkin-radio d-flex">
            <Field
              className="confident-okr"
              component={RadioField}
              name="confident"
              label="Rất tốt"
              checked={toString(values?.confident) === toString(CONFIDENT.good)}
              value={CONFIDENT.good}
              isValidate={true}
              id="good"
            />
            <Field
              className="confident-okr"
              component={RadioField}
              name="confident"
              label="Ổn"
              checked={toString(values?.confident) === toString(CONFIDENT.normal)}
              value={CONFIDENT.normal}
              isValidate={true}
              id="normal"
            />
            <Field
              className="confident-okr"
              component={RadioField}
              name="confident"
              label="Không ổn lắm"
              checked={toString(values?.confident) === toString(CONFIDENT.bad)}
              value={CONFIDENT.bad}
              isValidate={true}
              id="bad"
            />
            <ErrorMessages name="confident" />
          </div>
        </div>
      </Row>
      <Row className="checkin-container-footer">
        <div className="align-items-center d-flex footer-checkin">
          <Col xs="12" md="8" className="align-items-center d-flex">
            <div className="footer-checkin-text">
              <Field
                name="is_done"
                disabled={dataDetail?.is_done}
                defaultChecked={values?.is_done}
                component={CheckBoxFormikField}
                label="Hoàn thành OKRs"
              />
            </div>
            <div className="align-items-center d-flex footer-checkin-next-time">
              <div className="align-items-center d-flex footer-checkin-next-time-right">
                <span> Ngày Check-in tiếp theo</span>
                <Field
                  disabled={values?.is_done}
                  component={DateFieldFormik}
                  name="checkin_date"
                  className="w-100 form-control btn-time"
                  // minDate={new Date()}
                />
              </div>
            </div>
          </Col>
          <Col xs="12" md="4" className="d-flex justify-content-end">
            <ButtonCommon
              type="button"
              onClick={() => {
                handleSubmit();
              }}
              className="btn-done"
              name="done"
              label="Check-in Xong"
              disabled={dataDetail?.is_done}
            />
          </Col>
        </div>
      </Row>
    </>
  );
};
export default CheckinOKRs;
