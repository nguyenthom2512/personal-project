/* eslint-disable prettier/prettier */
import * as Yup from 'yup';
export const CheckInDetailSchema = Yup.object().shape({
  confident: Yup.string().required('Đây là trường bắt buộc'),
  user: Yup.string().required('Đây là trường bắt buộc'),
  checkin_date: Yup.date().when('is_done', {
    is: false,
    // eslint-disable-next-line prettier/prettier
    then: Yup.date().typeError('Thời gian sai định dạng ').required('Đây là trường bắt buộc'),
  }),
  okr_result: Yup.array().of(
    Yup.object().shape({
      result: Yup.string().required('Đây là trường bắt buộc'),
      // eslint-disable-next-line prettier/prettier
      confident: Yup.string().nullable().required('Đây là trường bắt buộc'),
      overdue_note: Yup.string().required('Đây là trường bắt buộc'),
      process_note: Yup.string().required('Đây là trường bắt buộc'),
      solution_note: Yup.string().required('Đây là trường bắt buộc'),
      total_done: Yup.number().typeError('Chỉ nhập số').required('Đây là trường bắt buộc'),
      target: Yup.string().required('Đây là trường bắt buộc'),
      trouble_note: Yup.string().required('Đây là trường bắt buộc'),
      processed: Yup.string().required('Đây là trường bắt buộc'),
    }),
  ),
});
