import ButtonCommon from 'components/ButtonCommon';
import { DATE_FORMAT } from 'constants/date';
import { el } from 'date-fns/locale';
import { Form, Formik } from 'formik';
import useGetDetailListOkr from 'hooks/useGetDetailListOkr';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { useToasts } from 'react-toast-notifications';
import { Row } from 'reactstrap';
import { createCheckin } from 'redux/modules/createCheckin';
import authServices from 'services/authServices';
import * as Yup from 'yup';
import CheckinOKRs from './Component/CheckinOKRs';
import DiaLogDetail from './Component/DialogDetail';
import HistoryCheckin from './Component/HistoryCheckin';
import { CheckInDetailSchema } from './validation';

const propTypes = {};
const CheckinDetail = (props) => {
  const { addToast } = useToasts();
  const params = useParams();
  const id = params?.id || '';
  const [dataDetail, loadingDetail, errorDetail, refetchDetail] = useGetDetailListOkr(id);
  const userData = authServices.getUserLocalStorage();
  const user = userData?.user?.id;
  const dispatch = useDispatch();
  const [isHistory, setHistory] = useState(false);
  const CheckinDetail = [
    'Kết quả chính',
    'Mục tiêu',
    'Số đạt được',
    'Tiến độ',
    'Mức độ tự tin',
    'Tiến độ, kết quả công việc?',
    'Công việc nào đang & sẽ chậm tiến độ?',
    'Trở ngại, khó khăn là gì?',
    'Cần làm gì để vượt qua trở ngại?',
  ];
  const dataHistory = ['Ngày Check-in', 'Ngày Check-in kế tiếp', 'Đã duyệt', 'Trạng thái'];

  //! Function

  const onSubmit = (values) => {
    const dataCheckin = (values?.okr_result).map((el) => {
      return {
        confident: el?.confident,
        is_done: el?.is_done,
        overdue_note: el?.overdue_note,
        process_note: el?.process_note,
        result: el?.result,
        solution_note: el?.solution_note,
        total_done: el?.total_done,
        trouble_note: el?.trouble_note,
      };
    });
    const data = {
      checkin_result: dataCheckin,
      checkin_date: values.checkin_date,
      confident: values.confident,
      checkin_status: el.checkin_date ? 0 : 1,
      is_done: values.is_done,
      user: values.user,
      okr: values.id,
    };
    if (values.is_done) {
      delete data.checkin_date;
    }

    dispatch(
      createCheckin(data, {
        onFailed: (err) => {
          addToast(err, { appearance: 'error' });
        },
        onSuccess: (data) => {
          addToast('Thành công!', { appearance: 'success' });
          refetchDetail();
        },
      }),
    );
  };
  const processInitialValue = () => {
    if (isEmpty(dataDetail)) {
      const newData = (dataDetail?.okr_result || []).map((elm) => {
        return {
          ...elm,
          overdue_note: '',
          process_note: '',
          solution_note: '',
          trouble_note: '',
          total_done: '',
          result: elm.id,
          is_done: false,
          confident: elm.confident.toString(),
        };
      });
      const oldData = { ...dataDetail, okr_result: newData, user, checkin_date: '' };

      return oldData;
    } else {
      const matchedIndex = dataDetail?.ok_checkin[0];
      const newData = (dataDetail?.okr_result || []).map((elm) => {
        const foundData = matchedIndex?.checkin_result?.find((el) => el.result === elm.id);
        if (!isEmpty(foundData) && foundData.is_done) {
          return {
            ...elm,
            overdue_note: foundData.overdue_note,
            process_note: foundData.process_note,
            solution_note: foundData.solution_note,
            trouble_note: foundData.trouble_note,
            total_done: foundData.total_done,
            is_done: foundData.is_done,
            confident: foundData.confident.toString(),
            result: elm.id,
          };
        }

        return {
          ...elm,
          overdue_note: '',
          process_note: '',
          solution_note: '',
          trouble_note: '',
          total_done: '',
          is_done: false,
          result: elm.id,
        };
      });

      const oldData = { ...dataDetail, okr_result: newData, user, checkin_date: '' };
      return oldData;
    }
  };
  const onClickHistory = (tab) => {
    setHistory(tab);
  };

  //! Render

  if (loadingDetail) {
    return <span>Loading...</span>;
  }
  if (errorDetail) {
    return <span style={{ color: 'red' }}>{errorDetail.toString()}</span>;
  }

  const checkinTime = () => {
    if (dataDetail.is_done) return 'Đã hoàn thành';
    if (isEmpty(dataDetail.ok_checkin)) return 'Vui lòng check-in';
    const dataLastCheckin = dataDetail.ok_checkin[0];
    return moment(dataLastCheckin?.checkin_date).format(DATE_FORMAT);
  };

  return (
    <div className="checkin-container">
      <Formik
        initialValues={processInitialValue(dataDetail)}
        onSubmit={onSubmit}
        validationSchema={CheckInDetailSchema}
        validateOnBlur={false}
        enableReinitialize
        validateOnChange={false}
      >
        {({ values, setFieldValue, errors, touched, setFieldTouched, handleSubmit, setTouched, handleBlur }) => {
          // eslint-disable-next-line react-hooks/rules-of-hooks
          // useEffect(() => {
          //   setFieldTouched('checkin_date', true);
          // }, [values]);
          return (
            <Form>
              <Row className="checkin-container-title">
                <div className="border-bottom align-items-center d-flex header-checkin justify-content-between">
                  <div className="align-items-center d-flex">
                    <div className="header-checkin-text">Check-in OKRs hàng tuần</div>
                    <ButtonCommon
                      onClick={() => onClickHistory(false)}
                      className="rounded-0"
                      label="Check-in OKRs"
                      outline={isHistory}
                    />
                    <ButtonCommon
                      onClick={() => onClickHistory(true)}
                      className="rounded-0"
                      label="Lịch sử Check-in"
                      outline={!isHistory}
                    />
                  </div>
                </div>
              </Row>
              <Row className="checkin-container-subtitle">
                <div className="align-items-center d-flex subtitle-checkin">
                  <div
                    className="subtitle-checkin-span"
                    style={{ maxWidth: 300, overflow: 'hidden', textOverflow: 'ellipsis' }}
                  >
                    {values?.object_name}
                  </div>
                  <div className="subtitle-checkin-time">
                    <span>Ngày cần Check-in: {checkinTime(values)} </span>
                  </div>
                  <div className="align-items-center d-flex">
                    <div className="subtitle-checkin-process">Tiến độ thực hiện: </div>
                    <div className="percent-completed text-center">
                      <span className="percent-completed-span">{(values?.percent_completed * 100)?.toFixed(2)}%</span>
                    </div>
                  </div>
                </div>
              </Row>
              {isHistory && <HistoryCheckin dataHistory={dataHistory} values={values} CheckinDetail={CheckinDetail} />}
              {!isHistory && (
                <CheckinOKRs
                  dataDetail={dataDetail}
                  values={values}
                  handleSubmit={handleSubmit}
                  CheckinDetail={CheckinDetail}
                  setFieldValue={setFieldValue}
                />
              )}
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

CheckinDetail.propTypes = propTypes;
export default CheckinDetail;
