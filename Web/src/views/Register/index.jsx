import { ReactComponent as Lock } from 'assets/svg/lock.svg';
import { ReactComponent as Mail } from 'assets/svg/mail.svg';
import { ReactComponent as UserIcon } from 'assets/svg/UserIcon.svg';
import ButtonCommon from 'components/ButtonCommon';
import InputField from 'components/CustomField/InputField';
import { RouteBase } from 'constants/routeUrl';
import { FastField, Formik } from 'formik';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import { Col, Form, Label, Row } from 'reactstrap';
import registerService from 'services/registerService';
import * as Yup from 'yup';
import { ReactComponent as LoginImage } from '../../assets/svg/login-image.svg';

const propTypes = {};

const validateRegister = Yup.object({
  full_name: Yup.string().required('Đây là trường bắt buộc'),
  email: Yup.string().email('Email không hợp lệ').required('Đây là trường bắt buộc'),
  password: Yup.string()
    .required('Đây là trường bắt buộc')
    .matches(
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,128}$/,
      'Mật khẩu cần ít nhất chứa 8 ký tự, một chữ hoa, một chữ thường, một số và một ký tự đặc biệt',
    ),
  passwordConfirmation: Yup.string()
    .required('Đây là trường bắt buộc')
    .oneOf([Yup.ref('password'), null], 'Mật khẩu đã nhập không khớp. Hãy thử lại. '),
});

const RegisterPage = () => {
  //! State
  const { t } = useTranslation();
  const { addToast } = useToasts();
  const history = useHistory();

  //! Function
  const onSubmit = async (body) => {
    try {
      const response = await registerService.postRegister(body);
      addToast('Tạo tài khoản thành công! Vui lòng kiểm tra email của bạn!', { appearance: 'success' });
      history.push(RouteBase.Login);
    } catch (error) {
      addToast('Tạo mới không thành công vui lòng thử lại');
    }
  };

  //! Render
  return (
    <Row className="custom-form g-0">
      <Col xs={12} md={6} className="pos-relative">
        <Formik
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={{
            email: '',
            password: '',
            full_name: '',
            passwordConfirmation: '',
          }}
          onSubmit={onSubmit}
          validationSchema={validateRegister}
        >
          {({ isSubmitting, handleSubmit }) => (
            <Form className="center-div">
              {/* <ErrorFocus /> */}
              <Row className="g-0">
                <div className="custom-form--header">
                  <h2 className="custom-form--title">{t('register:name')}</h2>
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('register:labelEmail')}</Label>
                  <FastField
                    component={InputField}
                    icon={<Mail />}
                    name="email"
                    className="form-control"
                    placeholder={t('register:placeHolderEmail')}
                  />
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('register:userName')}</Label>
                  <FastField
                    component={InputField}
                    icon={<UserIcon />}
                    name="full_name"
                    className="form-control"
                    placeholder={t('register:placeHolderUserName')}
                  />
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('register:labelPassword')}</Label>
                  <FastField
                    icon={<Lock />}
                    component={InputField}
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder={t('register:placeHolderPassword')}
                  />
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('register:labelConfirmPassword')}</Label>
                  <FastField
                    icon={<Lock />}
                    component={InputField}
                    name="passwordConfirmation"
                    type="password"
                    className="form-control"
                    placeholder={t('register:placeHolderConfirmPassword')}
                  />
                </div>
                <div className="d-flex justify-content-center mt-4">
                  <ButtonCommon
                    // isLoading={propsFormik.isSubmitting}
                    label={'Tạo tài khoản'}
                    isLoading={isSubmitting}
                    type="submit"
                    className="w-100 button-primary"
                    onClick={handleSubmit}
                  >
                    {t('register:register')}
                  </ButtonCommon>
                </div>
                <div className="custom-form--forgot-pass custom-form--forgot-pass__gray mt-4">
                  {t('register:alreadyAccount')}{' '}
                  <Link to={RouteBase.Login} className="custom-form--forgot-pass">
                    {t('register:haveAccount')}
                  </Link>
                </div>
              </Row>
            </Form>
          )}
        </Formik>
      </Col>
      <Col xs={0} md={6} className="image-wrap">
        <div className="custom-form--image-wrap">
          <LoginImage className="image-login" />
        </div>
      </Col>
    </Row>
  );
};

RegisterPage.propTypes = propTypes;
export default RegisterPage;
