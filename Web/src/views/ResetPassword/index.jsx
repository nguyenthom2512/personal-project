import { ReactComponent as Mail } from 'assets/svg/mail.svg';
import ButtonCommon from 'components/ButtonCommon';
import InputField from 'components/CustomField/InputField';
import ErrorFocus from 'components/ErrorFocus';
import { RouteBase } from 'constants/routeUrl';
import { FastField, Form, Formik } from 'formik';
import React, { useCallback, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { Redirect } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import { Col, Label, Row } from 'reactstrap';
import { GetAuthSelector } from 'redux/selectors/auth';
import resetPasswordService from 'services/resetPasswordService';
import * as Yup from 'yup';
import { ReactComponent as LoginImage } from '../../assets/svg/login-image.svg';

const ResetPasswordPage = (props) => {
  //!State
  const { t } = useTranslation();
  const auth = GetAuthSelector();
  const { isLogin } = auth;
  const { addToast } = useToasts();

  //!Function
  const loginschema = useCallback(() => {
    return Yup.object({
      email: Yup.string().required(t('common:required')),
    });
  }, [t]);

  const onSubmit = async (body) => {
    try {
      const response = await resetPasswordService.postResetPassword(body);
      addToast('Vui lòng kiểm tra email của bạn!', { appearance: 'success' });
    } catch (error) {
      addToast('Email không tồn tại');
    }
  };
  useEffect(() => {
    const root = document.getElementById('root');
    root.style.overflowX = 'hidden';
  }, []);

  if (isLogin) {
    return <Redirect to={RouteBase.Home} />;
  }
  //!Render

  return (
    <Row className="custom-form g-0">
      <Col xs={12} md={6} className="pos-relative">
        <Formik
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={{
            email: '',
          }}
          validationSchema={loginschema}
          onSubmit={onSubmit}
        >
          {(propsFormik) => (
            <Form className="center-div">
              <ErrorFocus />
              <Row className="g-0">
                <div className="custom-form--header">
                  <h2 className="custom-form--title">{t('resetPassword:name')}</h2>
                  <span> {t('resetPassword:typePassword')}</span>
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('resetPassword:labelEmail')}</Label>
                  <FastField
                    component={InputField}
                    icon={<Mail />}
                    name="email"
                    className="form-control"
                    placeholder={t('resetPassword:labelPassword')}
                  />
                </div>
                <div className="d-flex justify-content-center mt-4">
                  <ButtonCommon
                    label={'Tiếp theo'}
                    type="submit"
                    className="w-100 button-primary"
                    isLoading={propsFormik.isSubmitting}
                  >
                    {t('resetPassword:continue')}
                  </ButtonCommon>
                </div>
              </Row>
            </Form>
          )}
        </Formik>
      </Col>
      <Col xs={0} md={6} className="image-wrap">
        <div className="custom-form--image-wrap">
          <LoginImage className="image-login" />
        </div>
      </Col>
    </Row>
  );
};
export default ResetPasswordPage;
