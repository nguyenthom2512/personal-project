import Paging from 'components/Paging';
import useGetOkrFeedBack from 'hooks/okr/useGetOkrFeedback';
import { findIndex, isEmpty } from 'lodash-es';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import authServices from 'services/authServices';
import { ReactComponent as Chart } from '../../../assets/svg/chart.svg';
import { ReactComponent as Cup } from '../../../assets/svg/cup.svg';
import { ReactComponent as Star } from '../../../assets/svg/star.svg';
import { ReactComponent as Avatar } from '../../../assets/svg/user-circle-solid.svg';
// import authServices from 'services/authServices';
import { isNull } from 'lodash-es';
import genImageUrl from 'helpers/genImageUrl';

const FeedbackReceived = (props) => {
  const { setTotal } = props;
  const { t } = useTranslation();
  const user = authServices.getUserLocalStorage();
  const [param, setParam] = useState({
    page: 1,
    page_size: 2,
    user_received: user?.user?.id,
  });
  const [data, dataStatic, loading, error, refetch] = useGetOkrFeedBack(param);
  const { received, sent } = dataStatic;

  useEffect(() => {
    if (isEmpty(dataStatic) || !dataStatic) {
      setTotal(0);
      return;
    }
    setTotal(received?.total + sent?.total);
  }, [dataStatic]);

  const indexMonthRank = findIndex(received?.rank_month || [], function (e) {
    return e == user?.user?.id;
  });

  const indexCycleRank = findIndex(received?.rank_department || [], function (e) {
    return e == user?.user?.id;
  });
  return (
    <>
      <div className="d-flex justify-content-between content align-items-center" style={{ marginTop: 20 }}>
        <div className="title">{t('CFRS:stars_received')}</div>
        {/* <Button outline className="bottom">
          <div className="align-items-center d-flex">
            <span>{t('CFRS:rank')}</span>
          </div>
        </Button> */}
      </div>
      <div className="content">
        <div className="d-flex overview-content" style={{ height: 130 }}>
          <div className="item">
            <span>
              <Star />
            </span>
            <span>{t('CFRS:stars_received_total')}</span>
            <span>{received?.total}</span>
          </div>
          <div className="item border-right border-left border-item">
            <span>
              <Cup />
            </span>
            <span>{t('CFRS:ranking_in_month')}</span>
            <span>{indexMonthRank >= 0 ? indexMonthRank + 1 : 'chưa có thứ hạng'}</span>
          </div>
          <div className="item">
            <span>
              <Chart />
            </span>
            <span>{t('CFRS:ranking_cycle')}</span>
            <span>{indexCycleRank >= 0 ? indexCycleRank + 1 : 'chưa có thứ hạng'}</span>
          </div>
        </div>
        <div className="list-feedback border-item border-bottom">
          {(data?.results || []).map((el) => (
            <div className="item" key={el?.id}>
              <div className="left-item">
                <span className="d-block">
                  {el?.user_data?.img_url ? (
                    <img
                      src={
                        el?.user_data?.img_url
                          ? genImageUrl(el?.user_data?.img_url)
                          : 'https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png'
                      }
                      alt="avatar"
                      style={{ marginRight: 10, height: '25px', width: '25px', borderRadius: '50%' }}
                    />
                  ) : (
                    <Avatar style={{ width: 25, marginRight: 10 }} />
                  )}
                  {el?.user_data?.full_name || ''}
                </span>
                <span className="d-block">{el?.criteria_title || ''}</span>
              </div>
              <div className="right-item">
                <span className="d-block">{moment(el?.created_time).format('DD-MM-YYY HH:mm:ss')}</span>
                <span className="d-block">
                  {(el?.rate || 0) >= 0 && !isNull(el?.rate) ? `+${el?.rate}` : el?.rate || 0}
                  <Star style={{ width: 20, height: 20, marginLeft: 5 }} />
                </span>
              </div>
            </div>
          ))}
        </div>
      </div>
      <div
        style={{
          padding: 10,
          backgroundColor: '#FFFF',
          display: 'flex',
        }}
      >
        {/* <span style={{ marginRight: 10 }}>Trang</span> */}
        <Paging count={data?.count || 0} onSelectPage={(page) => setParam({ ...param, page })} pageSize={2} />
      </div>
    </>
  );
};

export default FeedbackReceived;
