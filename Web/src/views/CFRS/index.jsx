import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Button } from 'reactstrap';
import { ReactComponent as Plus } from '../../assets/svg/plus.svg';
import FeedbackReceived from './Component/FeedbackReceived';
import FeedbackSend from './Component/FeedbackSend';
import { ReactComponent as Star } from '../../assets/svg/star.svg';
import { RouteBase } from 'constants/routeUrl';
const FeedbackOkrPage = () => {
  //!State
  const [total, setTotal] = useState(0);
  const { t } = useTranslation();
  const history = useHistory();
  // const [data, loading, error, refetch] = useGetOkrFeedBack(param);
  const pushToCreate = () => {
    history.push(RouteBase.CreateOkr);
  };
  //!Function

  //!Render
  return (
    <div className="listokr-container">
      <div className="okr-header d-flex ">
        <div className="d-flex">
          <h4 className="mb-4">{t('CFRS:Feedback_Recognition')}</h4>
          <Button outline className="bottom" style={{ marginLeft: 10, pointerEvents: 'none' }}>
            <div className="align-items-center d-flex">
              <span className="title-add " style={{ marginRight: 10, color: 'black' }}>
                {total}
              </span>
              <Star />
            </div>
          </Button>
        </div>
        <Button className="button-add" onClick={pushToCreate}>
          <div className="align-items-center d-flex">
            <Plus />
            <span className="title-add">{t('CFRS:add')}</span>
          </div>
        </Button>
      </div>
      <div className="row">
        <div className="col-12 col-lg-6">
          <FeedbackReceived setTotal={setTotal} />
        </div>
        <div className="col-12 col-lg-6">
          <FeedbackSend />
        </div>
      </div>
    </div>
  );
};
export default FeedbackOkrPage;
