import useGetListTodos from 'hooks/todos/useGetListTodos';
import React from 'react';

const Todos = (props) => {
  //! State
  const [data, loading] = useGetListTodos();

  //! Function

  //! Render
  return <div>Todos</div>;
};

export default Todos;
