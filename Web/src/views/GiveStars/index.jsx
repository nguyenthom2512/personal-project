import ButtonCommon from 'components/ButtonCommon';
import InputField from 'components/CustomField/InputField';
import SelectField from 'components/CustomField/SelectField';
import { Field, Form, Formik } from 'formik';
import { chooseField } from 'helpers';
import useGetUserOkr from 'hooks/quiz/useGetUserOkr';
import useGetDepartment from 'hooks/useGetDepartment';
import useDataListOKR from 'hooks/useGetListOKR';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useToasts } from 'react-toast-notifications';
import { Card, CardBody, Col, Label, Row } from 'reactstrap';
import authServices from 'services/authServices';
import feedbackServices from 'services/feedbackServices';
import * as Yup from 'yup';
const propTypes = {};

const validationGiveStarsSchema = Yup.object().shape({
  user_received: Yup.string().required('Đây là trường bắt buộc'),
  okr: Yup.string().required('Đây là trường bắt buộc'),
  quantity: Yup.number().typeError('Chỉ nhập số').required('Đây là trường bắt buộc'),
});
const GiveStars = (props) => {
  //! State
  const { t } = useTranslation();
  const [dataListOKR, , , refetchListOkr] = useDataListOKR();
  const [dataDepartment] = useGetDepartment();
  const [dataUser, , , refetchDataUser] = useGetUserOkr();
  const userData = authServices.getUserLocalStorage();
  const { addToast } = useToasts();

  const user = userData.user.id;

  //! Function

  const onSubmit = (value, formikHelper) => {
    formikHelper.setSubmitting(true);
    const { user_received, content, quantity, user, okr } = value;
    const body = { user_received, content, quantity, user, okr };
    feedbackServices
      .createFeedBack(body)
      .then(() => {
        addToast(t('common:giveStarsSuccess'), { appearance: 'success' });
      })
      .catch((err) => {
        addToast(t('common:dontgiveStars'), { appearance: 'error' });
      })
      .finally(() => {
        formikHelper.setSubmitting(false);
      });
  };

  const onFieldChangeDepartment = (idDepartment) => {
    refetchListOkr({ department: idDepartment });
    refetchDataUser({ department: idDepartment }, true);
  };
  const onFieldChangeOkr = (idOkr) => {
    refetchDataUser({ okr: idOkr }, false);
  };

  //! Render
  return (
    <Formik
      initialValues={{
        department_name: '',
        // name_with_department: '',
        user_received: '',
        content: `<p>${t('common:giveStars')}</p>`,
        quantity: '',
        user,
        okr: '',
      }}
      onSubmit={onSubmit}
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={validationGiveStarsSchema}
    >
      {({ isSubmitting }) => {
        return (
          <Form>
            <Card>
              <CardBody>
                <Row>
                  <Col xs={12} md={12} lg={12} className="mt-3">
                    <Label className="mb-2">{t('common:selectDepartment')}</Label>
                    <Field
                      name="department_name"
                      component={SelectField}
                      data={chooseField(dataDepartment, 'department_name')}
                      defaultOption={t('common:selectDepartment')}
                      onFieldChange={onFieldChangeDepartment}
                    />
                  </Col>
                  <Col xs={12} md={12} lg={12} className="mt-3">
                    <Label className="mb-2">{t('common:selectOkrs')}</Label>
                    <Field
                      name="okr"
                      component={SelectField}
                      data={chooseField(dataListOKR, 'object_name')}
                      defaultOption={t('common:selectOkrs')}
                      onFieldChange={onFieldChangeOkr}
                    />
                  </Col>
                  <Col xs={12} md={12} lg={12} className="mt-3">
                    <Label className="mb-2">{t('common:chooseRecipient')}</Label>
                    <Field
                      name="user_received"
                      component={SelectField}
                      data={chooseField(dataUser, 'name_with_department')}
                    />
                  </Col>
                  <Col xs={12} md={12} lg={12} className="mt-3">
                    <Label className="mb-2">{t('common:enterNumverStars')}</Label>
                    <Field name="quantity" component={InputField} />
                  </Col>
                  <div className="d-flex justify-content-center mt-3">
                    <ButtonCommon type="submit" label={t('common:giveStars')} isLoading={isSubmitting} />
                  </div>
                </Row>
              </CardBody>
            </Card>
          </Form>
        );
      }}
    </Formik>
  );
};

GiveStars.propTypes = propTypes;
export default GiveStars;
