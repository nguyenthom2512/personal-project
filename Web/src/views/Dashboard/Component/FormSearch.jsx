import SelectField from 'components/CustomField/SelectField';
import { QUARTER } from 'constants/date';
import { Field, Formik } from 'formik';
import { chooseField } from 'helpers';
import useGetDepartment from 'hooks/useGetDepartment';
import { isEmpty } from 'lodash-es';
import React from 'react';
import { useTranslation } from 'react-i18next';

const FormSearch = ({ setParam }) => {
  const [dataDepartment] = useGetDepartment();

  const { t } = useTranslation();
  const onSubmit = (values) => {
    const arr = values.created_time__range.split(',');
    const { created_time__range, ...body } = values;
    setParam({
      ...body,
      start_time: isEmpty(arr) ? '' : arr[0],
      end_time: isEmpty(arr) ? '' : arr[1],
    });
  };
  return (
    <Formik
      initialValues={{
        department: '',
        created_time__range: '',
      }}
    >
      {({ values }) => (
        <div className="row dashboard-container__form-search">
          <div className="col-12 col-md-3">
            <Field
              as="select"
              name="department"
              className="form-control"
              component={SelectField}
              data={chooseField(dataDepartment, 'department_name')}
              defaultOption={t('common:select')}
              onFieldChange={(elm) => onSubmit({ ...values, department: elm })}
            />
          </div>
          <div className="col-12 col-md-3">
            <Field
              className="filter-time ms-2"
              name="created_time__range"
              component={SelectField}
              data={QUARTER()}
              defaultOption={t('treeOkr:selectQuarter')}
              onFieldChange={(elm) => {
                onSubmit({ ...values, created_time__range: elm });
              }}
            />
          </div>
          {/* <div className="col-12 col-md-4 d-flex justify-content-end ">
            <Field
                  className="form-control"
                  name="text"
                  type="text"
                  placeholder="Tìm kiếm"
                  style={{ maxWidth: 300 }}
                />
          </div> */}
        </div>
      )}
    </Formik>
  );
};

export default FormSearch;
