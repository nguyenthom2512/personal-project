import useGetDashboard from 'hooks/okr/useGetDashboard';
import React, { useEffect, useState } from 'react';
import { Line } from 'react-chartjs-2';
import { useTranslation } from 'react-i18next';
import FormSearch from './Component/FormSearch';

const Dashboard = (props) => {
  const [param, setParam] = useState({
    department: 2,
    start_time: '2021-10-01',
    end_time: '2021-10-31',
  });

  const [data] = useGetDashboard(param);
  const { t } = useTranslation();

  //! Render
  const totalStats = [
    {
      label: t('dashboard:totalOkr'),
      value: data?.total_okr,
    },
    {
      label: t('dashboard:totalOkrChallenge'),
      value: data?.total_okr_by_challenge,
    },
    {
      label: t('dashboard:totalOkrInspiration'),
      value: data?.total_okr_by_inspiration,
    },
  ];

  return (
    <div className="dashboard-container listokr-container">
      <div className="okr-header justify-contents-start">
        <h4 className="mb-4">{t('dashboard:dashboard')}</h4>
        <FormSearch setParam={setParam} />
      </div>

      <div className="dashboard-container__progresses" style={{ backgroundColor: '#FFF', marginTop: 30 }}>
        <div className="align-items-center d-flex content border-item border-bottom">
          <div className="title">{t('dashboard:weeklyProgress')}</div>
        </div>
        <div className="content">
          <div className="ms-1 dashboard-container__progresses__list ">
            {totalStats.map((el) => (
              <div key={el.label} className="dashboard-container__progresses__item me-3">
                <div className="box-shadow ">
                  <h4>{el.value}</h4>
                  <h5>{el.label}</h5>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
      <div style={{ backgroundColor: '#FFF', marginTop: 30 }}>
        <div className="align-items-center d-flex content border-item border-bottom">
          <div className="title">{t('dashboard:cfrsStatus')}</div>
        </div>
        <Line
          data={{
            labels: [1500, 1600, 1700, 1750, 1800, 1850, 1900, 1950, 1999, 2050],
            datasets: [
              {
                data: [86, 114, 106, 106, 107, 111, 133, 221, 783, 2478],
                label: t('dashboard:africa'),
                borderColor: '#3e95cd',
                fill: false,
              },
              {
                data: [282, 350, 411, 502, 635, 809, 947, 1402, 2700, 2267],
                label: t('dashboard:asia'),
                borderColor: '#8e5ea2',
                fill: false,
              },
              {
                data: [168, 170, 178, 190, 203, 276, 408, 547, 675, 734],
                label: t('dashboard:europe'),
                borderColor: '#3cba9f',
                fill: false,
              },
              {
                data: [40, 20, 10, 16, 24, 38, 74, 167, 508, 784],
                label: t('dashboard:latinAmerica'),
                borderColor: '#e8c3b9',
                fill: false,
              },
              {
                data: [6, 3, 2, 2, 7, 26, 82, 172, 312, 433],
                label: t('dashboard:northAmerica'),
                borderColor: '#c45850',
                fill: false,
              },
            ],
          }}
          options={{
            title: {
              display: true,
              text: t('dashboard:worldPopulationPerRegion'),
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          }}
          // style={{ backgroundColor: 'red', height: 200 }}
          // width="180"
          height="80"
        />
      </div>
    </div>
  );
};
export default Dashboard;
