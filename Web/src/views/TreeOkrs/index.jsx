import { Link } from '@material-ui/core';
import SelectField from 'components/CustomField/SelectField';
import { QUARTER } from 'constants/date';
import { RouteBase } from 'constants/routeUrl';
import { Field, Form, Formik } from 'formik';
import { chooseField } from 'helpers';
import useGetDepartment from 'hooks/useGetDepartment';
import useToggleDialog from 'hooks/useToggleDialog';
import qs from 'query-string';
import React, { Fragment, useEffect, useRef, useState } from 'react';
import Tree from 'react-d3-tree';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router';
import { Button, UncontrolledTooltip } from 'reactstrap';
import listOkrService from 'services/listOkrService';
import * as Yup from 'yup';
import DialogCheckinfo from './Compoment/DialogshowCheckinfo';

const treeListOkr = Yup.object().shape({
  department: Yup.string()
    .when('created_time__range', {
      is: (value) => Boolean(value),
      then: Yup.string().required('Vui lòng chọn phòng ban'),
    })
    .nullable(),
});

//! Component
const ButtonActions = ({ idNodeDatum = '', onSubmitDialog = () => {}, nameNoteTree = '' }) => {
  //! State
  const [open, toggle, shouldRender] = useToggleDialog();
  //! Render
  return (
    <Fragment>
      <Button onClick={toggle} style={{ height: '35px', marginTop: '15px', marginLeft: '13px' }}>
        Kiểm tra
      </Button>
      {shouldRender && (
        <DialogCheckinfo
          isOpen={open}
          toggle={toggle}
          idNodeDatum={idNodeDatum}
          onSubmit={onSubmitDialog}
          nameNoteTree={nameNoteTree}
        />
      )}
    </Fragment>
  );
};

//! Main Component
const TreeOkr = () => {
  const dataLocal = sessionStorage.getItem('treeScreen');
  const objLocalData = JSON.parse(dataLocal || '{}');
  const [trees, setTrees] = useState(objLocalData?.trees || {});
  const { t } = useTranslation();
  const history = useHistory();
  // const { setLoading } = useContext(IndicatorContext);
  const [data, , error] = useGetDepartment();
  const [translateTree, setTranslate] = useState({ x: 0, y: 0 });
  const treeWrapper = document.getElementById('treeWrapper');

  useEffect(() => {
    const width = treeWrapper?.offsetWidth || 0;
    setTranslate({ x: width * 0.5, y: 20 });
  }, [treeWrapper]);

  useEffect(() => {
    error && alert(error);
  }, [error]);

  const convertData = (el) => {
    return {
      name: el.object_name,
      id: el.id,
      children: el?.children || [],
      percent_completed: `${(el?.percent_completed * 100).toFixed(2)}%`,
      is_match: el.is_match,
      attributes: [
        {
          title: t('treeOkr:progress'),
          value: `${(el?.percent_completed * 100).toFixed(2)}%`,
        },
      ],
    };
  };

  const saveLocalData = (trees, department, created_time__range) => {
    const treeScreen = {
      trees,
      department,
      created_time__range,
    };
    sessionStorage.setItem('treeScreen', JSON.stringify(treeScreen));
  };

  const dataTree = async (okr_parent, department, created_time__range) => {
    // setLoading(true);
    const params = {
      okr_parent__isnull: !Boolean(okr_parent),
      okr_parent,
      department,
      created_time__range,
    };
    const parent = {
      name: data.find((e) => e.id?.toString() === department?.toString())?.department_name,
    };
    try {
      const listOkr = await listOkrService.getListOkr(params);
      if (!!okr_parent) {
        const createTreeChildren = (listOkr?.data.results || []).map((elm) => {
          return convertData(elm);
        });
        // tạo data tree
        const child = (elm) => {
          return elm.map((element) => {
            if (element.id.toString() === okr_parent.toString()) {
              return { ...element, children: createTreeChildren };
            } else {
              if (element.children.length > 0) {
                return {
                  ...element,
                  children: child(element.children),
                };
              } else {
                return element;
              }
            }
          });
        };
        setTrees({ ...trees, children: child(trees.children) });
        saveLocalData({ ...trees, children: child(trees.children) }, department, created_time__range);
      } else {
        const createTree = (listOkr?.data.results || []).map((elm) => {
          return convertData(elm);
        });
        setTrees({ ...parent, children: createTree });
        saveLocalData({ ...parent, children: createTree }, department, created_time__range);
      }
    } catch (error) {
      // alert(error);
    } finally {
      // setLoading(false);
    }
  };

  const onSubmit = (values) => {
    dataTree('', values.department, values.created_time__range);
  };

  const formikRef = useRef(null);

  return (
    <>
      <Formik
        initialValues={{
          department: objLocalData?.department || '',
          created_time__range: objLocalData?.created_time__range || '',
          search: '',
        }}
        onSubmit={onSubmit}
        validateOnBlur={false}
        validateOnChange={false}
        innerRef={formikRef}
        validationSchema={treeListOkr}
      >
        {({ values, handleSubmit }) => {
          return (
            <Form className="form-body tree-okr">
              <div className="d-flex justify-content-between">
                <div>
                  <p className="tree-okr__header">{t('treeOkr:header')}</p>
                </div>
                <div className="d-flex">
                  <div style={{ minWidth: 200 }}>
                    <Field
                      className="form-control"
                      name="department"
                      component={SelectField}
                      data={chooseField(data, 'department_name')}
                      defaultOption={t('treeOkr:selectDepartment')}
                      onFieldChange={(elm) => handleSubmit({ ...values, department: elm })}
                    />
                  </div>
                  <div style={{ minWidth: 200 }} className="ms-3">
                    <Field
                      className="ms-2"
                      name="created_time__range"
                      component={SelectField}
                      data={QUARTER()}
                      defaultOption={t('treeOkr:selectQuarter')}
                      onFieldChange={(elm) => {
                        handleSubmit({ ...values, created_time__range: elm });
                      }}
                    />
                  </div>
                </div>
              </div>
              {values.department && (
                <div id="treeWrapper" className="d-flex w-100" style={{ height: '70vh' }}>
                  <Tree
                    translate={translateTree}
                    collapsible
                    data={trees}
                    orientation="vertical"
                    rootNodeClassName="node__root"
                    branchNodeClassName="node__branch"
                    leafNodeClassName="node__leaf"
                    pathFunc="step"
                    enableLegacyTransitions
                    separation={{ nonSiblings: 2, siblings: 2 }}
                    zoomable
                    zoom={1}
                    scaleExtent={{ max: 2, min: 0.2 }}
                    onNodeClick={(elm) => dataTree(elm.data.id, values.department)}
                    renderCustomNodeElement={({ nodeDatum }) => {
                      return (
                        <g>
                          <circle
                            r="20"
                            fill={!nodeDatum.id || nodeDatum.is_match ? '#18202e' : 'red'}
                            stroke={!nodeDatum.id || nodeDatum.is_match ? '#18202e' : 'red'}
                            strokeWidth="0px"
                            onClick={() => dataTree(nodeDatum.id, values.department)}
                          />
                          <foreignObject x="20" y="-15" width="200px" height="200%" style={{ overflow: 'visible' }}>
                            <div
                              style={{
                                overflow: 'hidden',
                                whiteSpace: 'nowrap',
                                textOverflow: 'ellipsis',
                                color: 'blue',
                                // wordWrap: 'break-word',
                                marginLeft: 14,
                              }}
                              onClick={() => {
                                history.push({
                                  pathname: RouteBase.ListOKR,
                                  search: qs.stringify({
                                    okr_parent: nodeDatum?.id,
                                    department: values?.department,
                                  }),
                                });
                              }}
                            >
                              <span href="#" id={`controlledTooltip${nodeDatum?.id || '1'}`}>
                                <Link to="">{nodeDatum.name}</Link>
                              </span>
                              <UncontrolledTooltip
                                placement="auto-end"
                                target={`controlledTooltip${nodeDatum?.id || '1'}`}
                              >
                                {nodeDatum.name}
                              </UncontrolledTooltip>
                            </div>
                            {nodeDatum.id && !nodeDatum.is_match && (
                              <ButtonActions
                                nameNoteTree={nodeDatum.name}
                                idNodeDatum={nodeDatum.id}
                                onSubmitDialog={() => {}}
                              />
                            )}
                          </foreignObject>

                          {!!nodeDatum?.attributes &&
                            nodeDatum?.attributes.map((elm, index) => {
                              return (
                                <text
                                  className="tree-okr__tree-text"
                                  key={index.toString()}
                                  fill="black"
                                  x="20"
                                  dy="20"
                                  dx="12"
                                  strokeWidth="0"
                                  color="red"
                                >
                                  {elm.title}: {elm.value}
                                </text>
                              );
                            })}
                        </g>
                      );
                    }}
                  />
                </div>
              )}
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

export default TreeOkr;
