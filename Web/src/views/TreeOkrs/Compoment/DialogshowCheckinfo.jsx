import TableCommon from 'components/TableCommon';
import { CONFIDENT } from 'helpers';
import useGetOkrTree from 'hooks/okr/useGetOkrtree';
import React from 'react';
import { useToasts } from 'react-toast-notifications';
import { Card, CardHeader, Modal, ModalBody, ModalHeader } from 'reactstrap';
import { ReactComponent as GroupIcon } from '../../../assets/svg/Group.svg';

const propTypes = {};

const DialogCheckinfo = (props) => {
  const { isOpen, toggle, idNodeDatum = '', nameNoteTree } = props;
  //! State
  const { addToast } = useToasts();
  const [data, , ,] = useGetOkrTree(idNodeDatum);

  //! Function

  const columns = [
    {
      Header: 'Kết quả chính',
      accessor: 'key_result',
      classNameCell: 'key-result',
    },
    {
      Header: 'Mục tiêu',
      accessor: 'target',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: 'Đơn vị',
      accessor: 'unit_name',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
    },
    {
      Header: 'Đạt được',
      accessor: 'current_done',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: 'Tiến độ',
      accessor: 'processed',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
      Cell: (item) => {
        return <div className="percent-completed">{(item?.processed * 100)?.toFixed(2)}%</div>;
      },
    },
    {
      Header: 'Thay đổi',
      accessor: 'percent_changed',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
      Cell: (item) => {
        return <div>{(item?.percent_changed * 100)?.toFixed(2)}%</div>;
      },
    },
    {
      Header: 'Mức độ tự tin',
      accessor: 'confident',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
      Cell: (item) => {
        return <div className="text-center">{CONFIDENT?.[item?.confident]}</div>;
      },
    },
    {
      Header: 'Kế hoạch',
      accessor: 'plan_url',
      Cell: (item) => {
        return (
          <div>
            <GroupIcon
              type="button"
              onClick={() => {
                if (!item.plan_url) return addToast('Không có link kế hoạch');
                window.open(item?.plan_url);
              }}
            />
          </div>
        );
      },
    },
    {
      Header: 'Kết quả',
      accessor: 'result_url',
      Cell: (item) => {
        return (
          <div>
            <GroupIcon
              type="button"
              onClick={() => {
                if (!item.result_url) return addToast('Không có link kết quả');
                window.open(item?.result_url);
              }}
            />
          </div>
        );
      },
    },
  ];
  //! Render
  return (
    <Modal size="xl" isOpen={isOpen} toggle={toggle}>
      <ModalHeader>{nameNoteTree}</ModalHeader>
      <ModalBody>
        <div>
          <Card>
            <CardHeader>Chi tiết KR</CardHeader>
            <TableCommon columns={columns} data={data?.okr_result} />
          </Card>
          <div className="my-3"></div>
          <Card>
            <CardHeader>OKRs cấp dưới</CardHeader>
            {(data?.okr_child || []).map((elm, index) => {
              return <TableCommon key={index} title={elm?.object_name} columns={columns} data={elm?.okr_result} />;
            })}
          </Card>
        </div>
      </ModalBody>
    </Modal>
  );
};

DialogCheckinfo.propTypes = propTypes;
export default DialogCheckinfo;
