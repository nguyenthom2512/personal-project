import React, { useState, useEffect } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import PropTypes from 'prop-types';
import CreateOkr from 'views/CreateOkr';
import { useTranslation } from 'react-i18next';

const propTypes = {
  toggle: PropTypes.func,
  isShowModal: PropTypes.bool,
  onUpdateSuccess: PropTypes.func,
  onUpdateFailed: PropTypes.func,
  dataDetail: PropTypes.object,
};

const DetailModal = ({
  toggle = () => {},
  isShowModal = false,
  dataDetail = {},
  onUpdateSuccess = () => {},
  onUpdateFailed = () => {},
  dataUser,
}) => {
  const [initData, setInitData] = useState(dataDetail);
  const { t } = useTranslation();

  useEffect(() => {
    const userResponsible = [];
    const userAccountable = [];
    const userInform = [];
    const userConsult = [];
    dataDetail?.raic_user?.forEach((element) => {
      switch (element.raic_type) {
        case 0:
          userResponsible.push(element?.user_data?.id);
          break;
        case 1:
          userAccountable.push(element?.user_data?.id);
          break;
        case 2:
          userInform.push(element?.user_data?.id);
          break;
        case 3:
          userConsult.push(element?.user_data?.id);
          break;
        default:
          break;
      }
    });
    const newInitData = {
      ...initData,
      user_responsible: userResponsible,
      user_accountable: userAccountable,
      user_inform: userInform,
      user_consult: userConsult,
    };
    setInitData(newInitData);
    return () => {};
  }, [dataDetail]);

  return (
    <Modal size="xl" isOpen={isShowModal} toggle={toggle}>
      <ModalHeader toggle={toggle}>{t('common:editOkr')}</ModalHeader>
      <ModalBody>
        <CreateOkr
          // value={values}
          initialData={initData}
          isEdit
          onUpdateSuccess={onUpdateSuccess}
          onUpdateFailed={onUpdateFailed}
        />
      </ModalBody>
    </Modal>
  );
};

DetailModal.propTypes = propTypes;
export default DetailModal;
