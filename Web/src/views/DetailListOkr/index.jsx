import React, { useEffect, useState } from 'react';
import { ReactComponent as Pen } from '../../assets/svg/pen.svg';
import { ReactComponent as Group } from '../../assets/svg/Group.svg';
import useGetDetailListOkr from 'hooks/useGetDetailListOkr';
import { useHistory, useParams } from 'react-router';
import DetailModal from './DetailModal';
import useToggleDialog from 'hooks/useToggleDialog';
import { useToasts } from 'react-toast-notifications';
import deleteOkrService from 'services/deleteOkrService';
import { RouteBase } from 'constants/routeUrl';
import ButtonCommon from 'components/ButtonCommon';
import FeedBackOKR from './Components/OKEFeedBack';
import OkrResult from './Components/OkrResults';
import OkrRelateList from './Components/OkrRelateList';
import ConfirmModal from 'components/ConfirmModal';
import { includes } from 'lodash-es';
import useTreesView from 'hooks/trees/useTreesView';
import { useTranslation } from 'react-i18next';

const DetailListOkrPage = () => {
  //! State
  const { t } = useTranslation();
  const history = useHistory();
  const { addToast } = useToasts();
  const params = useParams();
  const { updateTree } = useTreesView();

  const id = params?.id || '';
  const [dataDetail, loadingDetail, errorDetail, refetchDetail] = useGetDetailListOkr(id);

  const [openDialog, toggleDialog, shouldRenderDialog] = useToggleDialog();
  const [openModalDelete, toggleModalDelete, shoulRenderModalDelete] = useToggleDialog();
  const dataLocal = sessionStorage.getItem('treeScreen');
  const objLocalData = JSON.parse(dataLocal || '{}');

  //! Function
  const removeItem = (arrayChildren) => {
    return (arrayChildren || [])
      .map((elm) => {
        if (elm.id.toString() === id.toString()) {
          return null;
        } else {
          if (elm.children.length > 0) {
            return {
              ...elm,
              children: removeItem(elm.children),
            };
          } else {
            return elm;
          }
        }
      })
      .filter((e) => e);
  };

  const handleDelete = async () => {
    try {
      await deleteOkrService.deleteOkr(id);
      toggleModalDelete();
      addToast(t('common:deleteSuccess'), { appearance: 'success' });
      const udpateLocalTree = {
        ...objLocalData,
        trees: {
          ...objLocalData.trees,
          children: removeItem(objLocalData?.trees?.children),
        },
      };
      sessionStorage.setItem('treeScreen', JSON.stringify(udpateLocalTree));
      history.push(RouteBase.ListOKR);
    } catch (error) {
      addToast(t('common:deleteFail'));
    }
  };

  const onUpdateSuccess = async (helperFormik, values) => {
    const name = values?.object_name || '';
    try {
      //* Call func after update successfully
      await refetchDetail();
      helperFormik.setSubmitting(false);
      toggleDialog();
      addToast(t('common:success'), { appearance: 'success' });
      updateTree({ id, name });
    } catch (err) {
      helperFormik.setSubmitting(false);
      addToast(err, { appearance: 'error' });
    }
  };

  const onUpdateFailed = (err, helperFormik) => {
    addToast(err, { appearance: 'error' });
    helperFormik.setSubmitting(false);
  };

  //! Render
  // if (loadingDetail) {
  //   return <span>Loading...</span>;
  // }

  if (errorDetail) {
    return <span style={{ color: 'red' }}>{errorDetail.toString()}</span>;
  }

  return (
    <div className="okr-container">
      <div className="d-flex justify-content-end mb-3">
        <ButtonCommon
          className="me-2 button-create"
          onClick={toggleDialog}
          icon={<Pen />}
          label={t('common:editOkr')}
          outline
        />
        <ButtonCommon onClick={toggleModalDelete} icon={<Group />} label={t('common:deleteOkr')} outline />
        {/* <ButtonCommon className="ms-2" onClick={toggleModalDelete} label="Tặng sao" /> */}
      </div>
      <OkrResult
        result={dataDetail?.okr_result}
        refetchResults={refetchDetail}
        idOkr={dataDetail?.id}
        onSuccessEditResult={() => {
          refetchDetail();
        }}
        onSuccessDelete={() => {
          refetchDetail();
        }}
      />

      <OkrRelateList loadingDetail={loadingDetail} />

      <FeedBackOKR okrFeedBack={dataDetail} />

      {shoulRenderModalDelete && (
        <ConfirmModal
          isOpen={openModalDelete}
          toggle={toggleModalDelete}
          content={t('common:areYouSureYouWantToDelete')}
          onSubmit={handleDelete}
        />
      )}
      {shouldRenderDialog && (
        <DetailModal
          isShowModal={openDialog}
          toggle={toggleDialog}
          dataDetail={dataDetail}
          onUpdateSuccess={onUpdateSuccess}
          onUpdateFailed={onUpdateFailed}
          // dataRaic={dataUser}
        />
      )}
    </div>
  );
};
export default DetailListOkrPage;
