import React, { Fragment } from 'react';
import TableCommon from 'components/TableCommon';
import { Modal, ModalBody } from 'reactstrap';
import { CONFIDENT } from 'helpers';
import { ReactComponent as GroupIcon } from '../../../assets/svg/Group.svg';
import { useTranslation } from 'react-i18next';

const propTypes = {};

const DialogRelateResults = ({ isOpen, toggle, dataRelate = {} }) => {
  //! State
  const { t } = useTranslation();

  //! Function
  const columns = [
    {
      Header: t('common:keyResult'),
      accessor: 'key_result',
      classNameCell: 'text-ellipsis',
    },
    {
      Header: t('common:target'),
      accessor: 'target',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: t('common:unit'),
      accessor: 'unit_name',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
    },
    {
      Header: t('common:currentDone'),
      accessor: 'current_done',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: t('common:processed'),
      accessor: 'processed',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
      styleHeader: {
        width: '108px',
      },
      Cell: (item) => {
        return <div className="percent-completed">{item?.processed?.toFixed(2)}%</div>;
      },
    },
    {
      Header: t('common:percentChanged'),
      accessor: 'percent_changed',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: t('common:confident'),
      accessor: 'confident',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
      Cell: (item) => {
        return <div className="text-center">{CONFIDENT?.[item?.confident]}</div>;
      },
    },
    {
      Header: t('common:planUrl'),
      accessor: 'plan_url',
      Cell: (item) => {
        return (
          <div>
            <GroupIcon className="icon" onClick={() => window.open(item?.plan_url)} />
          </div>
        );
      },
    },
    {
      Header: t('common:result'),
      accessor: 'result_url',
      Cell: (item) => {
        return (
          <div>
            <GroupIcon className="icon" onClick={() => window.open(item?.result_url)} />
          </div>
        );
      },
    },
  ];

  //! Render
  return (
    <Fragment>
      <Modal isOpen={isOpen} toggle={toggle} style={{ maxWidth: '1542px', display: 'flex' }}>
        <ModalBody>
          <TableCommon title={t('common:relate')} columns={columns} data={dataRelate?.okr_result || []} />
        </ModalBody>
      </Modal>
    </Fragment>
  );
};

DialogRelateResults.propTypes = propTypes;
export default DialogRelateResults;
