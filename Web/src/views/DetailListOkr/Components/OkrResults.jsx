import React, { Fragment } from 'react';
import TableCommon from 'components/TableCommon';
import { ReactComponent as GroupIcon } from '../../../assets/svg/Group.svg';
import { useToasts } from 'react-toast-notifications';
import CellActions from '../Cells/CellActions';
import { useDispatch } from 'react-redux';
import { updateOkrRequest } from 'redux/modules/okrList';
import OkrServices from 'services/OkrServices';
import { CONFIDENT } from 'helpers';
import { useTranslation } from 'react-i18next';

const propTypes = {};

const OkrResult = ({ idOkr = '', result = [], onSuccessEditResult = () => {}, onSuccessDelete = () => {} }) => {
  //! State
  const { addToast } = useToasts();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  //! Function

  const onSubmitDelete = async (result) => {
    try {
      const { id } = result;
      const response = await OkrServices.deleteOkrResult(id);
      if (response.status === 204) {
        addToast(t('common:deleteSuccess'), { appearance: 'success' });
        onSuccessDelete && onSuccessDelete();
      }
    } catch (error) {
      addToast(t('common:deleteFail'), { appearance: 'error' });
    }
  };

  const onSubmitEdit = (values, helperFormik, { toggle }) => {
    dispatch(
      updateOkrRequest(values, {
        onSuccess: () => {
          addToast(t('common:success'), { appearance: 'success' });
          onSuccessEditResult && onSuccessEditResult();
          toggle();
        },
        onFailed: (err) => {},
      }),
    );
  };

  const columns = [
    {
      Header: t('common:keyResult'),
      accessor: 'key_result',
      classNameCell: 'key-result',
    },
    {
      Header: t('common:keyResult'),
      accessor: 'target',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: t('common:unit'),
      accessor: 'unit_name',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
    },
    {
      Header: t('common:currentDone'),
      accessor: 'current_done',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
    },
    {
      Header: t('common:processed'),
      accessor: 'processed',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
      Cell: (item) => {
        return <div className="percent-completed">{(item?.processed * 100)?.toFixed(2)}%</div>;
      },
    },
    {
      Header: t('common:percentChanged'),
      accessor: 'percent_changed',
      classNameCell: 'text-center',
      classNameHeader: 'text-center',
      Cell: (item) => {
        return <div>{(item?.percent_changed * 100)?.toFixed(2)}%</div>;
      },
    },
    {
      Header: t('common:confident'),
      accessor: 'confident',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
      Cell: (item) => {
        return <div className="text-center">{CONFIDENT?.[item?.confident]}</div>;
      },
    },
    {
      Header: t('common:plan'),
      accessor: 'plan_url',
      Cell: (item) => {
        return (
          <div>
            <GroupIcon
              type="button"
              onClick={() => {
                if (!item.plan_url) return addToast(t('common:noPlanLink'));
                window.open(item?.plan_url);
              }}
            />
          </div>
        );
      },
    },
    {
      Header: t('common:result'),
      accessor: 'result_url',
      Cell: (item) => {
        return (
          <div>
            <GroupIcon
              type="button"
              onClick={() => {
                if (!item.result_url) return addToast(t('common:noResultLink'));
                window.open(item?.result_url);
              }}
            />
          </div>
        );
      },
    },
    {
      Header: t('common:action'),
      accessor: 'id',
      Cell: (item) => {
        return (
          <CellActions
            idOkr={idOkr}
            result={item}
            onSubmitEdit={onSubmitEdit}
            onSubmitDelete={onSubmitDelete}
            hasDelete={result.length > 1}
          />
        );
      },
    },
  ];

  //! Render
  return (
    <Fragment>
      <TableCommon title={t('common:detailOKRs')} columns={columns} data={result} />
    </Fragment>
  );
};

OkrResult.propTypes = propTypes;
export default OkrResult;
