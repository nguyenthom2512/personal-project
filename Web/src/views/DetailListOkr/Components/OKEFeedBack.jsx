import { Field, Form, Formik } from 'formik';
import React from 'react';
import { Button, Label } from 'reactstrap';
import ReactQuill from 'react-quill';
import ErrorMessages from 'components/CustomField/ErrorMessage';
import SelectField from 'components/CustomField/SelectField';
import authServices from 'services/authServices';
import useGetCriteraFeedBack from 'hooks/okr/useGetCriteraFeedBack';
import OkrServices from 'services/OkrServices';
import { isResponseSuccess } from 'helpers';
import { useToasts } from 'react-toast-notifications';
import { chooseField } from 'helpers';
import { useTranslation } from 'react-i18next';

const FeedBackOKR = ({ okrFeedBack = {} }) => {
  //! State
  const userData = authServices.getUserLocalStorage();
  const user = userData?.user?.id;
  const { addToast } = useToasts();
  const [options] = useGetCriteraFeedBack();
  const { t } = useTranslation();

  //! Function
  const onSubmit = async (values, { resetForm }) => {
    if (values?.user_received === values?.user) {
      addToast(t('common:chooseAnotherAccountToDonateStars'));
    } else {
      try {
        const response = await OkrServices.sendFeedback(values);
        if (isResponseSuccess(response?.status)) {
          resetForm();
          addToast(t('common:success'), { appearance: 'success' });
        }
      } catch (error) {
        if (error.data.messsage) {
          addToast(t('common:dontEnoughStars'));
        }
      }
    }
  };

  //! Render
  return (
    <Formik
      initialValues={{
        content: '',
        criteria: '',
        okr: okrFeedBack?.id,
        user_received: okrFeedBack?.user,
        user,
      }}
      onSubmit={onSubmit}
      enableReinitialize
      validateOnBlur={false}
      validateOnChange={false}
    >
      {({ values, setFieldValue, errors, setValues, handleSubmit }) => {
        return (
          <Form className="form-body">
            <div className="feedback_container">
              <div className="title__feedback">{t('common:evaluate')}</div>
              <div className="separator__feedback" />
              <div className="form-group row feedback">
                <Label className="mb-1 col-2 pt-2">{t('common:criteria')}</Label>
                <div className="col-10">
                  <Field
                    as="select"
                    name="criteria"
                    className="form-control"
                    component={SelectField}
                    defaultOption={t('common:selectEvaluate')}
                    data={chooseField(options, 'title_rate')}
                  />
                </div>
              </div>

              <div className="form-group row feedback">
                <Label className="mb-1 col-2 pt-2">{t('common:content')}</Label>
                <div className="col-10">
                  <div>
                    <ReactQuill
                      placeholder={t('common:content')}
                      style={{ backgroundColor: 'white' }}
                      value={values.content}
                      onChange={(text) => {
                        setFieldValue('content', text, false);
                      }}
                    />
                    <ErrorMessages name={'content'} />
                  </div>
                </div>
              </div>
              <div className="d-flex justify-content-center">
                <Button onClick={handleSubmit} className="btn-feedback-okr">
                  {t('common:complete')}
                </Button>
              </div>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};
export default FeedBackOKR;
