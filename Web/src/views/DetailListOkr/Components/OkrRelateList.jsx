import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import TableCommon from 'components/TableCommon';
import { Button } from 'reactstrap';
import useGetRelateList from '../../../hooks/okr/useGetRelateList';
import { useParams } from 'react-router';
import { CONFIDENT } from 'helpers';
import useToggleDialog from 'hooks/useToggleDialog';
import DialogRelateResults from '../Dialog/DialogRelateResults';
import { useTranslation } from 'react-i18next';

const propTypes = {
  okrRelateList: PropTypes.array,
  loadingDetail: PropTypes.bool,
};

const OkrRelateList = ({ loadingDetail = false }) => {
  //! State
  const { t } = useTranslation();
  const params = useParams();
  const id = params?.id || '';
  const [relateList, loading, error, refetchRelateList] = useGetRelateList({ id });
  const [openModalRelate, toggleModalRelate, shouldeRenderModalRelate] = useToggleDialog();
  const [relateOKR, setRelateOKR] = useState({ okr_result: [] });
  const onClickRelateResult = (relateData) => {
    setRelateOKR(relateData);
    toggleModalRelate();
  };

  useEffect(() => {
    if (loadingDetail) {
      refetchRelateList();
    }
    return () => {};
  }, [loadingDetail]);

  //! Function
  const columns = [
    {
      Header: t('common:target'),
      styleHeader: {
        width: 240,
      },
      accessor: 'object_name',
      classNameCell: 'text-ellipsis',
    },
    {
      Header: t('common:keyResult'),
      accessor: 'count_result',
      classNameHeader: 'text-center',

      Cell: (item) => {
        return (
          <Button className="button-result" onClick={() => onClickRelateResult(item)}>{`${item?.count_result} ${t(
            'common:keyResult',
          ).toLowerCase()}`}</Button>
        );
      },
    },
    {
      Header: t('common:processed'),
      accessor: 'percent_completed',
      classNameHeader: 'text-center',
      styleHeader: {
        width: '108px',
      },
      Cell: (item) => {
        return <div className="percent-completed">{item?.percent_completed?.toFixed(2)}%</div>;
      },
    },
    {
      Header: t('common:percentChanged'),
      accessor: 'percent_changed',
      classNameHeader: 'text-center',
      Cell: (item) => {
        return (
          <div className="d-flex justify-content-center">
            <div className="percent-completed">{(item?.percent_changed * 100)?.toFixed(2)}%</div>
          </div>
        );
      },
    },
    {
      Header: t('common:staff'),
      accessor: 'user.full_name',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
    },
    {
      Header: t('common:confident'),
      accessor: 'confident',
      classNameHeader: 'text-center',
      classNameCell: 'text-center',
      Cell: (item) => {
        return <div className="text-center">{CONFIDENT?.[item?.confident]}</div>;
      },
    },
  ];

  //! Render
  return (
    <Fragment>
      <TableCommon title={t('common:relate')} columns={columns} data={relateList} />
      {shouldeRenderModalRelate && (
        <DialogRelateResults isOpen={openModalRelate} toggle={toggleModalRelate} dataRelate={relateOKR} />
      )}
    </Fragment>
  );
};

OkrRelateList.propTypes = propTypes;
export default OkrRelateList;
