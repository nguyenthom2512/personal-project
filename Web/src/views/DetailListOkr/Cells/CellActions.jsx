import React from 'react';
import ModalEditResults from 'components/ModalEditResults';
import useToggleDialog from 'hooks/useToggleDialog';
import { ReactComponent as EditIcon } from '../../../assets/svg/EDIT.svg';
import { ReactComponent as DeleteIcon } from '../../../assets/svg/delete.svg';
import ConfirmModal from 'components/ConfirmModal';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';

const propTypes = {
  idOkr: PropTypes.string,
  result: PropTypes.Object,
  onSubmitEdit: PropTypes.func,
  onSubmitDelete: PropTypes.func,
  hasDelete: PropTypes.bool,
};

const CellActions = ({
  idOkr = '',
  result = {},
  onSubmitEdit = () => {},
  onSubmitDelete = () => {},
  hasDelete = false,
}) => {
  //! State
  const { t } = useTranslation();
  const [openModalEditResults, toggleModalEditResults, shouldModalEditResults] = useToggleDialog();
  const [openModalDeleteResults, toggleModalDeleteResults, shouldModalDeleteResults] = useToggleDialog();

  //! Function

  //! Render
  return (
    <div>
      {shouldModalEditResults && (
        <ModalEditResults
          isOpen={openModalEditResults}
          toggle={toggleModalEditResults}
          initialValues={{ ...result, unit: result?.unit?.toString(), okr: idOkr }}
          title={t('common:editKR')}
          onSubmit={onSubmitEdit}
          unitDisable
        />
      )}
      {shouldModalDeleteResults && (
        <ConfirmModal
          isOpen={openModalDeleteResults}
          toggle={toggleModalDeleteResults}
          content={t('common:areYouSureYouWantToDelete')}
          onSubmit={() => onSubmitDelete(result)}
          labelBtnSubmit={t('common:confirm')}
          labelBtnCancel={t('common:cancel')}
        />
      )}
      <EditIcon type="button" className="me-3" onClick={toggleModalEditResults} />
      {hasDelete && (
        <DeleteIcon
          type="button"
          className="icon"
          onClick={() => {
            toggleModalDeleteResults();
          }}
        />
      )}
    </div>
  );
};

CellActions.propTypes = propTypes;
export default CellActions;
