import { ReactComponent as Lock } from 'assets/svg/lock.svg';
import { ReactComponent as Mail } from 'assets/svg/mail.svg';
import ButtonCommon from 'components/ButtonCommon';
import InputField from 'components/CustomField/InputField';
import ErrorFocus from 'components/ErrorFocus';
import { RouteBase } from 'constants/routeUrl';
import { FastField, Form, Formik } from 'formik';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { Col, Label, Row } from 'reactstrap';
import { login } from 'redux/modules/auth';
import { GetAuthSelector } from 'redux/selectors/auth';
import * as Yup from 'yup';
import { ReactComponent as LoginImage } from '../../assets/svg/login-image.svg';

const LoginPage = (props) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const auth = GetAuthSelector();
  const { isLogin } = auth;

  const loginschema = Yup.object({
    email: Yup.string().required(t('common:required')).email(t('common:emailValidate')),
    password: Yup.string().required(t('common:required')),
  });

  useEffect(() => {
    const root = document.getElementById('root');
    root.style.overflowX = 'hidden';
  }, []);

  if (isLogin) {
    return <Redirect to={RouteBase.Home} />;
  }

  return (
    <Row className="custom-form g-0">
      <Col xs={12} md={6} className="pos-relative">
        <Formik
          validateOnBlur={false}
          validateOnChange={false}
          initialValues={{
            email: '',
            password: '',
          }}
          validationSchema={loginschema}
          onSubmit={(values, { setSubmitting }) => {
            const { email, password } = values;
            dispatch(
              login(
                email,
                password,
                (err) => {
                  setSubmitting(false);
                },
                () => {},
              ),
            );
          }}
        >
          {({ isSubmitting, setSubmitting }) => (
            <Form className="center-div">
              <ErrorFocus />
              <Row className="g-0">
                <div className="custom-form--header">
                  <h2 className="custom-form--title">{t('login:name')}</h2>
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('login:labelEmail')}</Label>
                  <FastField
                    component={InputField}
                    icon={<Mail />}
                    name="email"
                    className="form-control"
                    placeholder={t('login:placeHolderEmail')}
                  />
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('login:labelPassword')}</Label>
                  <FastField
                    icon={<Lock />}
                    component={InputField}
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder={t('login:placeHolderPassword')}
                  />
                </div>

                <div className="align-items-center d-flex form-group justify-content-between">
                  <div className="align-items-center d-flex">
                    <FastField color="dark" type="checkbox" name="challenge" />
                    <p className="login-text ms-2">{t('login:rememberLogin')}</p>
                  </div>
                  <Link
                    to={RouteBase.ResetPassword}
                    className="custom-form--forgot-pass custom-form--forgot-pass__no-decor"
                  >
                    {t('login:forgotPassword')}
                  </Link>
                </div>
                <div className="d-flex justify-content-center mt-4">
                  <ButtonCommon
                    label={'Đăng nhập'}
                    type="submit"
                    className="w-100 button-primary"
                    isLoading={isSubmitting}
                  >
                    {t('register:register')}
                  </ButtonCommon>
                </div>
                <div className="custom-form--forgot-pass custom-form--forgot-pass__gray mt-4">
                  {t('login:youHaveAccount')}
                  <Link to={RouteBase.Register} className="custom-form--forgot-pass">
                    {t('login:createAccount')}
                  </Link>
                </div>
              </Row>
            </Form>
          )}
        </Formik>
      </Col>
      <Col xs={0} md={6} className="image-wrap">
        <div className="custom-form--image-wrap">
          <LoginImage className="image-login" />
        </div>
      </Col>
    </Row>
  );
};
export default LoginPage;
