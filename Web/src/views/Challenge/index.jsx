import useGetDataComment from 'hooks/comment/useGetDataComment';
import useGetDataOkrChallenge from 'hooks/comment/useGetDataOkrChallenge';
import React, { useState } from 'react';
import { useParams } from 'react-router';
import { useToasts } from 'react-toast-notifications';
import { Card, CardBody } from 'reactstrap';
import authServices from 'services/authServices';
import commentService from 'services/commentService';
import CommentItem from './Component/CommentItem';
import SendComment from './Component/SendComment';
import UserComment from './Component/UserComment';

const propTypes = {};
const ChallengePage = (props) => {
  //! State

  const params = useParams();
  const id = params?.id || '';
  // const [data, setData] = useState('');
  // const [isEdit, setIsEdit] = useState('');
  // const [dataTemp, setDataTempt] = useState([]);
  const [dataOkrChallenge, loadingOkrChallenge, ,] = useGetDataOkrChallenge(id);
  const [dataComment, loadingComment, , refetchComment] = useGetDataComment({ challenge: id });
  const { addToast } = useToasts();
  const userData = authServices.getUserLocalStorage();
  //! Function

  //! Render

  return (
    <Card className="challenge">
      <CardBody>
        <UserComment data={dataOkrChallenge} />
        <SendComment
          onSubmitInput={async (valueInput, { resetForm }) => {
            try {
              const response = await commentService.postDataComment({
                title: valueInput,
                user: userData?.user?.id,
                todo: '',
                challenge: params?.id,
              });
              addToast('Thành công!', { appearance: 'success' });
              resetForm();
              refetchComment();
              // const newArr = [...dataComment.results, response?.data];
              // setDataTempt(newArr);
            } catch (error) {}
          }}
        />
        <div className="comment-container mt-3">
          {(dataComment?.results || []).map((el, key) => {
            return <CommentItem key={`${el.id}`} idxComment={key} values={el} refetchList={refetchComment} />;
          })}
        </div>
      </CardBody>
    </Card>
  );
};

ChallengePage.propTypes = propTypes;
export default ChallengePage;
