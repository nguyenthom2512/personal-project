import ButtonCommon from 'components/ButtonCommon';
import PropTypes from 'prop-types';
import React from 'react';
import { IoIosClose } from 'react-icons/io';
import { Modal, ModalBody, ModalFooter } from 'reactstrap';

const propTypes = {
  isShowPopover: PropTypes.func,
  togglePopover: PropTypes.func,
  handleEdit: PropTypes.func,
  handleDelete: PropTypes.func,
};

const ModalComment = ({
  isOpen = () => {},
  toggle = () => {},
  content = '',
  question = '',
  onClickDelete = () => {},
  onClickClear = () => {},
  values,
}) => {
  //! State

  //! Function
  const onDelete = () => {};
  //! Render
  return (
    <div className="popover-comment__container">
      <Modal isOpen={isOpen} toggle={toggle}>
        <ModalBody className="d-flex justify-content-between" style={{ fontWeight: 'bold' }}>
          {content}
          <IoIosClose size="20" onClick={onClickClear} />
        </ModalBody>
        <ModalFooter className="d-block">
          <div>{question}</div>
          <div className="d-flex justify-content-end">
            <ButtonCommon type="button" onClick={onClickDelete} label={'Delete'} />
          </div>
        </ModalFooter>
      </Modal>
    </div>
  );
};

ModalComment.propTypes = propTypes;
export default ModalComment;
