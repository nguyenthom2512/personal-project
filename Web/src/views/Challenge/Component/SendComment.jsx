import PropTypes from 'prop-types';
import React, { useRef, useState } from 'react';
import { Form, Input } from 'reactstrap';

const propTypes = {
  onSubmitInput: PropTypes.func,
  valueDefault: PropTypes.string,
};

const SendComment = ({
  valueDefault = '',
  // , imageDefault = ''
  onSubmitInput = () => {},
}) => {
  //! State
  const [valueInput, setValueInput] = useState(valueDefault);
  // const [valueImage, setValueImage] = useState(imageDefault);
  // console.log('position-relative', valueImage ? 'block' : 'none');
  const refFileSelected = useRef();

  const listFeature = [
    // {
    //   name: 'Upload',
    //   icon: <FiCamera />,
    //   onClick: () => {
    //     refFileSelected.current.click();
    //   },
    // },
    // {
    //   name: 'Upload1',
    //   icon: <FiCamera />,
    //   onClick: () => {},
    // },
    // {
    //   name: 'Upload2',
    //   icon: <FiCamera />,
    //   onClick: () => {},
    // },
    // {
    //   name: 'Upload3',
    //   icon: <FiCamera />,
    //   onClick: () => {},
    // },
  ];

  //! Function
  const resetForm = () => {
    setValueInput('');
    // setValueImage('');
  };

  const onChange = (e) => {
    setValueInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSubmitInput &&
      onSubmitInput(
        valueInput,
        //  valueImage,
        { resetForm },
      );
  };

  // const handleDeleteIcon = () => {
  //   console.log(123);
  //   setValueImage('');
  // };

  // const onChangeImage = (event) => {
  //   if (event.target.files && event.target.files[0]) {
  //     setValueImage(URL.createObjectURL(event.target.files[0]));
  //   }
  // };

  //! Render
  return (
    <Form onSubmit={handleSubmit} className="send-comment">
      <div className="position-relative">
        <Input
          name="comment"
          value={valueInput}
          className="input"
          onChange={onChange}
          placeholder="Viết bình luận..."
        />
        {/* <div className="icon">
          {listFeature.map((el, index) => (
            <div className="icon-element" key={el.name} onClick={el.onClick}>
              {el.icon}
            </div>
          ))}
        </div> */}
      </div>

      {/* <label htmlFor="fileSelect">
        <TiCameraOutline className="challenge-container__icon" onClick={handleClick} />
      </label> */}
      {/* <input
        ref={refFileSelected}
        type="file"
        style={{ display: 'none' }}
        accept="image/png, image/gif, image/jpeg"
        onChange={onChangeImage}
      />
      <div className={`${valueImage ? 'd-flex' : 'd-none'} align-items-center justify-content-between`}>
        <img id="img" src={valueImage} alt="Girl in a jacket" width="88" height="88" />
        <IoIosClose
          size="16"
          onClick={() => {
            handleDeleteIcon();
          }}
        />
      </div> */}
    </Form>
  );
};

SendComment.propTypes = propTypes;
export default SendComment;
