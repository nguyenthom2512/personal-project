import PopoverCommon from 'components/PopoverCommon';
import { BASE_URL_IMAGE } from 'constants/api';
import useGetDataComment from 'hooks/comment/useGetDataComment';
import useToggleDialog from 'hooks/useToggleDialog';
import moment from 'moment';
import React, { useRef, useState } from 'react';
import { BsThreeDots } from 'react-icons/bs';
import { FaEdit, FaTrash } from 'react-icons/fa';
import { useToasts } from 'react-toast-notifications';
import authServices from 'services/authServices';
import commentService from 'services/commentService';
import ModalComment from './ModalComment';
import SendComment from './SendComment';

const propTypes = {};
//! Component

//! Main component
const CommentItem = ({ values, idxComment, refetchList }) => {
  //! State
  const refMore = useRef(null);
  const [openDeleteDialog, toggleDeleteDialog, shouldRenderDeleteDialog] = useToggleDialog();
  const [openPopover, togglePopover, shouldRenderPopover] = useToggleDialog();
  const [isEdit, setIsEdit] = useState(false);
  // const [changeImage, setChangeImage] = useState('');

  const { addToast } = useToasts();
  const userData = authServices.getUserLocalStorage();

  //! Function
  const onClickClear = () => {
    toggleDeleteDialog();
  };

  const onClickDelete = async () => {
    // const arr = [...values];
    // const deleteArr = arr.find((el, index) => el.id !== index.id);
    try {
      const responseDelete = await commentService.deleteDataComment(values?.id);
      addToast('Xóa thành công!', { appearance: 'success' });
      toggleDeleteDialog();
      refetchList();
    } catch (error) {}
  };

  const onSubmitEdit = async (changeInput) => {
    //* 1: Call API Update
    try {
      const response = await commentService.putDataComment(values?.id, {
        id: values?.id,
        user: userData?.user?.id,
        title: changeInput,
      });
      //* 2: After api update successfully, call refetch API get List
      addToast('Thành công!', { appearance: 'success' });
      await refetchList();
      //* 3: turn off edit
      await setIsEdit(false);
    } catch (error) {}
  };
  // const onChangeImage = (e) => {
  //   setChangeImage(e.target.value);
  // };
  //! Render
  const listMore = [
    {
      name: 'Edit',
      icon: <FaEdit />,
      onClick: () => {
        //:TODO
        setIsEdit(true);
        togglePopover();
      },
    },
    {
      name: 'Delete',
      icon: <FaTrash />,
      onClick: () => {
        //:TODO
        toggleDeleteDialog();
      },
    },
  ];

  return (
    <div className="comment-item">
      <div className="comment-item__avatar">
        <img
          height="40px"
          width="40px"
          style={{ borderRadius: '50%' }}
          src={
            Boolean(values?.user_data?.img_url)
              ? `${BASE_URL_IMAGE}${values?.user_data?.img_url}`
              : 'https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png'
          }
          alt="avatar"
          // src={`${BASE_URL_IMAGE}${userData?.user?.img_url}`}
        />
      </div>

      <div className="comment-item__content">
        <p className="comment-item__content__nickname">{values?.user_data?.full_name}</p>

        {isEdit ? (
          <SendComment valueDefault={values?.title} onSubmitInput={onSubmitEdit} />
        ) : (
          <p className="comment-item__content__comment">{values?.title}</p>
        )}
      </div>
      <div className="mt-1"> {moment(values?.created_time).fromNow()} </div>
      {/* {isEdit ? (
        <div
          style={{ display: values.url_image ? 'block' : 'none' }}
          className="comment-item__image d-flex justify-content-between"
        >
          <img id="img" src={values.url_image} alt="Girl in a jacket" />
          <IoIosClose size="20" onClick={handleDelete} />
        </div>
      ) : (
        <div
          style={{ display: values.url_image ? 'block' : 'none' }}
          className="comment-item__image d-flex justify-content-between"
        >
          <img id="img" src={values.url_image} alt="Girl in a jacket" />
        </div>
      )} */}
      {/* {!isEdit && (
        <div className={`${values.url_image ? 'd-flex' : 'd-none'} comment-item__image justify-content-between`}>
          <img onChange={onChangeImage} id="img" src={values.url_image} alt="Girl in a jacket" />
        </div>
      )} */}

      <div ref={refMore} className={`${openPopover ? 'active' : ''} comment-item__actions`}>
        <span id={`comment-${idxComment}`} onClick={togglePopover} className={`${openPopover ? 'active' : ''}`}>
          <BsThreeDots />
        </span>

        {shouldRenderPopover && (
          <PopoverCommon
            className="comment-popover-more"
            target={`comment-${idxComment}`}
            toggle={togglePopover}
            isOpen={openPopover}
          >
            {listMore.map((el) => (
              <div key={el.name} onClick={el.onClick}>
                {el.icon} {el.name}
              </div>
            ))}
          </PopoverCommon>
        )}
      </div>

      {shouldRenderDeleteDialog && (
        <ModalComment
          question={'Are you sure you want to delete this comment?'}
          content={'Delete comment'}
          isOpen={openDeleteDialog}
          toggle={toggleDeleteDialog}
          onClickClear={onClickClear}
          onClickDelete={onClickDelete}
        />
      )}
    </div>
  );
};

CommentItem.propTypes = propTypes;
export default CommentItem;
