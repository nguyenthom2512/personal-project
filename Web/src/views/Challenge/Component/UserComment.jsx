import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { BASE_URL_IMAGE } from 'constants/api';
import moment from 'moment';

const propTypes = {};

const UserComment = ({ data }) => {
  console.log('data', Boolean(data?.user_data?.img_url));
  //! State
  const dispatch = useDispatch();

  //! Function

  //! Render
  return (
    <div>
      <div className="d-flex align-items-center">
        <div>
          <img
            height="40px"
            width="40px"
            style={{ borderRadius: '50%' }}
            alt="avatar"
            src={
              Boolean(data?.user_data?.img_url)
                ? `${BASE_URL_IMAGE}${data?.user_data?.img_url}`
                : 'https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png'
            }
          />
        </div>
        <div className="ms-3">
          <div style={{ fontWeight: 'bold' }}>{data?.user_data?.full_name || ''}</div>
          <div> {moment(data?.created_time).fromNow()} </div>
        </div>
      </div>
      <div className="mt-3 mb-3 ms-2" style={{ fontSize: '1.3rem' }}>
        <div className="mb-2s">
          <span style={{ fontWeight: 'bold' }}>OKR</span>: {data?.okr_name}{' '}
        </div>
        <div>
          <span style={{ fontWeight: 'bold' }}> Thách thức</span>: {data?.content}{' '}
        </div>
      </div>
    </div>
  );
};

UserComment.propTypes = propTypes;
export default UserComment;
