import React, { Fragment } from 'react';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import useGetOkrDetail from 'hooks/okr/useGetOkrDetail';
import useToggleDialog from 'hooks/useToggleDialog';
import DialogAddResult from './DialogAddResult';
import LoadingIndicator from 'components/LoadingIndicator';
import { useDispatch } from 'react-redux';
import { useToasts } from 'react-toast-notifications';
import { updateOkrRequest } from 'redux/modules/okrList';
import DialogActionListOkr from './DialogActionListOkr';
import OkrServices from 'services/OkrServices';
import { ReactComponent as GroupIcon } from '../../../assets/svg/Group.svg';
import { CONFIDENT } from 'helpers';
import moment from 'moment';

const DialogListResult = ({
  toggle,
  isOpen,
  idOkr = '',
  onSuccessEditResult = () => {},
  refetchListOKr,
  onSuccessDelete = () => {},
}) => {
  //! State
  const [okrDetail, loading, error, refetchDetail] = useGetOkrDetail({ id: idOkr, shouldTrigger: !!idOkr });
  const [openAddDialog, toggleAddDialog, shouldRenderAddDialog] = useToggleDialog();
  const { addToast } = useToasts();
  const dispatch = useDispatch();

  //! Function

  // const processKRParent = (values) => {
  //   const { okr_result } = values;
  // };

  const onSubmitDelete = async (result, toggleDelete) => {
    try {
      const { id } = result;
      const response = await OkrServices.deleteOkrResult(id);
      if (response.status === 204) {
        addToast('Xóa thành công!', { appearance: 'success' });
        onSuccessDelete && onSuccessDelete();
        refetchDetail();
        toggleDelete();
      }
    } catch (error) {
      addToast('Xoá không thành công!', { appearance: 'error' });
    }
  };

  const onSubmitEdit = (values, helperFormik, { toggle }) => {
    dispatch(
      updateOkrRequest(values, {
        onSuccess: () => {
          addToast('Thành công!', { appearance: 'success' });
          onSuccessEditResult && onSuccessEditResult();
          refetchDetail();
          toggle();
        },
        onFailed: (err) => {},
      }),
    );
  };

  //! Render
  return (
    <Modal isOpen={isOpen} toggle={toggle} style={{ maxWidth: '1000px', display: 'flex' }}>
      <ModalHeader>
        <div className="okr__objectname text-ellipsis">{okrDetail?.object_name}</div>
        <div className="okrDetail-header"></div>
      </ModalHeader>
      <ModalBody className="overflow-auto">
        {loading ? (
          <LoadingIndicator />
        ) : (
          <Fragment>
            <table className="table table-valign-middle">
              <thead>
                <tr>
                  <th className="text-center text-uppercase ">{'Kết quả chính'}</th>
                  <th className="text-center text-uppercase ">{'Mục tiêu'}</th>
                  <th className="text-center text-uppercase ">{'Đơn vị'}</th>
                  <th className="text-center text-uppercase ">{'Đạt được'}</th>
                  <th className="text-center text-uppercase ">{'Tiến độ'}</th>
                  <th className="text-center text-uppercase ">{'Thay đổi'}</th>
                  <th className="text-center text-uppercase ">{'Mức độ tự tin'}</th>
                  <th className="text-center text-uppercase ">{'Kế hoạch'}</th>
                  <th className="text-center text-uppercase ">{'Kết quả'}</th>
                  <th className="text-center text-uppercase ">{'Ngày hoàn thành KR'}</th>
                  <th className="text-center text-uppercase ">{'Hành động'}</th>
                </tr>
              </thead>
              <tbody>
                {(okrDetail?.okr_result || []).map((elm, index) => {
                  return (
                    <tr key={elm.key}>
                      <td className="text-center text-ellipsis">{elm?.key_result}</td>
                      <td className="text-center">{elm?.target}</td>
                      <td className="text-center">{elm?.unit_name}</td>
                      <td className="text-center">{elm?.current_done}</td>
                      <td className="text-center">
                        <div className="d-flex justify-content-center">
                          <div className="percent-completed">{(elm?.processed * 100)?.toFixed(2)}%</div>
                        </div>
                      </td>
                      <td className="text-center">{elm?.percent_changed}</td>
                      <td className="text-center">{CONFIDENT?.[elm?.confident]}</td>
                      <td className="text-center">
                        <GroupIcon
                          type="button"
                          onClick={() => {
                            if (!elm.plan_url) return addToast('Không có link kế hoạch');
                            window.open(elm?.plan_url);
                          }}
                        />
                      </td>
                      <td className="text-center">
                        <GroupIcon
                          type="button"
                          onClick={() => {
                            if (!elm.result_url) return addToast('Không có link kết quả');
                            window.open(elm?.result_url);
                          }}
                        />
                      </td>
                      <td className="text-center">{moment(elm?.deadline).format('DD/MM/YYYY')}</td>
                      <td className="text-center">
                        <DialogActionListOkr
                          idOkr={idOkr}
                          result={elm}
                          onSubmitEdit={onSubmitEdit}
                          onSubmitDelete={onSubmitDelete}
                          hasDelete={okrDetail?.okr_result.length > 1}
                          dataKR={okrDetail}
                        />
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>

            {shouldRenderAddDialog && (
              <DialogAddResult
                isOpen={openAddDialog}
                toggle={toggleAddDialog}
                dataKR={okrDetail}
                refetchDetail={refetchDetail}
                refetchListOKr={refetchListOKr}
              />
            )}
          </Fragment>
        )}
        {okrDetail?.okr_result?.length < 5 && <Button onClick={toggleAddDialog}>Thêm KR</Button>}
      </ModalBody>
    </Modal>
  );
};

export default DialogListResult;
