import { Form, Formik } from 'formik';
import { chooseField } from 'helpers';
import useGetUnit from 'hooks/useGetUnitOkr';
import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button, Modal, ModalFooter, ModalHeader } from 'reactstrap';
import { updateResultKr } from 'redux/modules/okrList';
import * as Yup from 'yup';
import KeyResult from '../Component/AddResult';

const linkRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;

const validateEdit = Yup.object().shape({
  key_result: Yup.string().required('Đây là trường bắt buộc'),
  target: Yup.number()
    .typeError('Trường này chỉ nhập số')
    .min(1, 'Bắt buộc phải lớn hơn 0')
    .required('Đây là trường bắt buộc'),
  unit: Yup.string().required('Đây là trường bắt buộc'),
  current_done: Yup.string().required('Đây là trường bắt buộc'),
  okr: Yup.string().required('Đây là trường bắt buộc'),
  deadline: Yup.date().typeError('Thời gian sai định dạng').required('Đây là trường bắt buộc'),
  plan_url: Yup.string().matches(linkRegex, 'Không đúng định dạng trang'),
  result_url: Yup.string().matches(linkRegex, 'Không đúng định dạng trang'),
});

const DialogAddResult = (props) => {
  //! State
  const { isOpen, toggle, className, dataKR, refetchDetail, refetchListOKr } = props;
  const dispatch = useDispatch();
  const [dataUnit] = useGetUnit();

  const [defaultKR, setDefaultKR] = useState({
    user: dataKR?.user,
    okr: dataKR?.id,
    key_result: '',
    target: '',
    current_done: 0,
    deadline: '',
    unit: '',
  });

  useEffect(() => {
    const newFillKr = {
      unit: dataKR.okr_result[0].unit,
    };
    setDefaultKR({ ...defaultKR, ...newFillKr });
  }, [dataKR]);
  //! Function

  const onSubmitEdit = (data) => {
    dispatch(
      updateResultKr(data, {
        onSuccess: () => {
          refetchDetail();
          refetchListOKr();
          toggle();
        },
        onFailed: (err) => {},
      }),
    );
  };

  //! Render
  return (
    <Formik
      initialValues={defaultKR}
      validateOnBlur={false}
      validateOnChange={false}
      onSubmit={onSubmitEdit}
      validationSchema={validateEdit}
      enableReinitialize
    >
      {(propsFormik) => (
        <Modal
          isOpen={isOpen}
          toggle={toggle}
          className={className}
          backdrop={propsFormik.isSubmitting ? 'static' : true}
        >
          <Form>
            <ModalHeader toggle={toggle}>Thêm mới results</ModalHeader>

            <KeyResult
              unitData={chooseField(dataUnit, 'unit_name')}
              unitDisable={dataKR.result_parent}
              dataKrParent={dataKR}
            />

            <ModalFooter>
              <Button onClick={propsFormik.handleSubmit} isLoading={propsFormik.isSubmitting}>
                Tạo mới
              </Button>
              <Button type="button" className="btn" onClick={toggle}>
                Huỷ
              </Button>
            </ModalFooter>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};

export default DialogAddResult;
