import React from 'react';
import ModalEditResults from 'components/ModalEditResults';
import useToggleDialog from 'hooks/useToggleDialog';
import { ReactComponent as EditIcon } from '../../../assets/svg/EDIT.svg';
import { ReactComponent as DeleteIcon } from '../../../assets/svg/delete.svg';
import ConfirmModal from 'components/ConfirmModal';

const propTypes = {};

const DialogActionListOkr = ({
  onSubmitEdit = () => {},
  onSubmitDelete = () => {},
  result,
  idOkr = '',
  hasDelete = false,
  dataKR,
}) => {
  //! State
  const [openModalEditResults, toggleModalEditResults, shouldModalEditResults] = useToggleDialog();
  const [openModalDeleteResults, toggleModalDeleteResults, shouldModalDeleteResults] = useToggleDialog();

  //! Function

  //! Render
  return (
    <div className="d-flex">
      {shouldModalEditResults && (
        <ModalEditResults
          isOpen={openModalEditResults}
          toggle={toggleModalEditResults}
          initialValues={{ ...result, unit: result?.unit?.toString(), okr: idOkr }}
          title="Sửa Kr"
          onSubmit={onSubmitEdit}
          unitDisable={dataKR.result_parent}
        />
      )}
      {shouldModalDeleteResults && (
        <ConfirmModal
          isOpen={openModalDeleteResults}
          toggle={toggleModalDeleteResults}
          content={`Bạn có chắc xoá result `}
          onSubmit={() => onSubmitDelete(result, toggleModalDeleteResults)}
        />
      )}
      <EditIcon type="button" className="me-3" onClick={toggleModalEditResults} />

      {hasDelete && (
        <DeleteIcon
          type="button"
          className="icon"
          onClick={() => {
            toggleModalDeleteResults();
          }}
        />
      )}
    </div>
  );
};

DialogActionListOkr.propTypes = propTypes;
export default DialogActionListOkr;
