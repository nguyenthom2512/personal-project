import SelectField from 'components/CustomField/SelectField';
import { Field, Form, Formik } from 'formik';
import React, { Fragment, useState } from 'react';
import { IoMdArrowDropdown } from 'react-icons/io';
import { MdKeyboardBackspace } from 'react-icons/md';
import { Button } from 'reactstrap';
import Paging from 'components/Paging';
import { Link } from 'react-router-dom';
import { RouteBase } from 'constants/routeUrl';
import { useHistory, useLocation } from 'react-router';
import qs from 'query-string';
import { CONFIDENT } from 'helpers';
import { QUARTER } from 'constants/date';
import { useTranslation } from 'react-i18next';
import useToggleDialog from 'hooks/useToggleDialog';
import DialogListResult from './Dialog/DialogListResult';
import useGetOkrList from 'hooks/okr/useGetOkrList';
import OkrServices from 'services/OkrServices';
import { useToasts } from 'react-toast-notifications';

const ListOkrsPage = () => {
  //! State
  const location = useLocation();
  const history = useHistory();
  const { t } = useTranslation();
  const queryString = qs.parse(location.search);
  const [param, setParam] = useState({
    page: 1,
    page_size: 5,
    department: queryString?.department || '',
    okr_parent__isnull: !Boolean(queryString?.okr_parent),
    okr_parent: queryString.okr_parent,
  });
  const { addToast } = useToasts();
  const [openListResult, toggleListResult, shouldRenderListResult] = useToggleDialog();

  const [okrSelected, setOkrSelected] = useState({});
  const { data: okrList, refetch } = useGetOkrList(param);

  //! Effect

  //! Function
  const onSubmit = (values) => {
    setParam(values);
  };

  const goBack = () => history.push(RouteBase.TreeOKR);

  const onClickResult = (okr = {}) => () => {
    toggleListResult();
    setOkrSelected(okr);
  };

  const handleDeleteOkrResult = async (idResult, refetchDetail) => {
    try {
      await OkrServices.deleteOkrResult(idResult);
      await refetchDetail();
      await refetch();
      addToast('Xóa thành công!', { appearance: 'success' });
    } catch (error) {
      // alert(error.response);
    }
  };

  //! Render

  return (
    <Fragment>
      <Formik
        enableReinitialize
        initialValues={param}
        onSubmit={onSubmit}
        validateOnBlur={false}
        validateOnChange={false}
      >
        {({ values, setFieldValue, errors, setValues, handleSubmit }) => (
          <Form className="form-body">
            <div className="listokr-container">
              <div className="okr-header d-flex justify-content-between">
                <div className="my-2 text-header">
                  <span className="iconback cursor-pointer">
                    <MdKeyboardBackspace onClick={goBack} />
                  </span>
                  OKRs công ty
                </div>
                <div className="field-header d-flex">
                  <div style={{ minWidth: 200 }}>
                    <Field
                      className="filter-time ms-2"
                      name="created_time__range"
                      component={SelectField}
                      data={QUARTER()}
                      defaultOption={t('treeOkr:selectQuarter')}
                      onFieldChange={(elm) => {
                        onSubmit({ ...values, created_time__range: elm });
                      }}
                    />
                  </div>
                </div>
              </div>
              <div className="text-okr p-lg-2">OKRs</div>
              <table style={{ width: '100%' }} className="table table-valign-middle">
                <thead>
                  <tr>
                    <th>{'Mục tiêu OKRs'}</th>
                    <th className="text-center">{'Kết quả chính'}</th>
                    <th className="text-center">{'Tiến độ '}</th>
                    <th className="text-center">{'Thay đổi'}</th>
                    <th className="text-center">{'Nhân viên'}</th>
                    <th className="text-center">{'Mức độ tự tin'}</th>
                  </tr>
                </thead>
                <tbody>
                  {(okrList?.results || []).map((elm) => {
                    return (
                      <tr key={elm.id}>
                        <td className="text-ellipsis">
                          <Link to={RouteBase.DetailOkrWithID(elm.id)}>
                            <IoMdArrowDropdown style={{ color: 'blue', fontSize: '15px', display: 'none' }} />
                            {elm?.object_name}
                          </Link>
                        </td>
                        <td className="text-center">
                          <Button className="button-result" onClick={onClickResult(elm)}>
                            {(elm?.okr_result || []).length} kết quả
                          </Button>
                        </td>
                        <td className="text-center">
                          <div className="d-flex justify-content-center">
                            <div className="percent-completed">{(elm?.percent_completed * 100)?.toFixed(2)}%</div>
                          </div>
                        </td>
                        <td className="text-center ">{elm?.percent_changed * 100}%</td>
                        <td className="text-center">{elm?.user?.name_with_department}</td>
                        <td className="text-center">{CONFIDENT?.[elm?.confident]}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <Paging
              count={okrList?.count}
              onSelectPage={(page) => {
                setParam({ ...param, page });
              }}
              pageSize={param.page_size}
            />
          </Form>
        )}
      </Formik>

      {shouldRenderListResult && (
        <DialogListResult
          isOpen={openListResult}
          toggle={toggleListResult}
          idOkr={okrSelected?.id}
          handleDelete={handleDeleteOkrResult}
          refetchListOKr={refetch}
          onSuccessEditResult={() => {
            refetch();
          }}
          onSuccessDelete={() => {
            refetch();
          }}
        />
      )}
    </Fragment>
  );
};
export default ListOkrsPage;
