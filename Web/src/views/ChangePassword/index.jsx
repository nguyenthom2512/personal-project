import React from 'react';
import { Col, Form, Label, Row } from 'reactstrap';
import { FastField, Formik } from 'formik';
import InputField from 'components/CustomField/InputField';
import { useTranslation } from 'react-i18next';
import { ReactComponent as LoginImage } from '../../assets/svg/login-image.svg';
import { ReactComponent as Lock } from 'assets/svg/lock.svg';
import { useHistory } from 'react-router';
import ButtonCommon from 'components/ButtonCommon';

const propTypes = {};

const ChangePasswordPage = (props) => {
  //! State
  const { t } = useTranslation();
  const history = useHistory();
  //! Function

  //! Render
  return (
    <Row className="custom-form g-0">
      <Col xs={6} md={6} className="position-relative">
        <Formik
          validateOnBlur={false}
          validateOnChange={false}
          //   initialValues={{
          //     email: '',
          //     password: '',
          //     full_name: '',
          //     passwordConfirmation: '',
          //   }}
          //   onSubmit={onSubmit}
          //   validationSchema={validateRegister}
        >
          {({ isSubmitting, handleSubmit }) => (
            <Form className="center-div">
              {/* <ErrorFocus /> */}
              <Row className="g-0">
                <div className="custom-form--header">
                  <h2 className="custom-form--title">{t('changePassword:name')}</h2>
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('changePassword:labelPassword')}</Label>
                  <FastField
                    icon={<Lock />}
                    component={InputField}
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder={t('changePassword:placeHolderPassword')}
                  />
                </div>
                <div className="pb-3">
                  <Label className="custom-form--label">{t('changePassword:labelConfirmPassword')}</Label>
                  <FastField
                    icon={<Lock />}
                    component={InputField}
                    name="passwordConfirmation"
                    type="password"
                    className="form-control"
                    placeholder={t('changePassword:placeHolderConfirmPassword')}
                  />
                </div>
                <div className="d-flex justify-content-center mt-4">
                  <ButtonCommon
                    // isLoading={propsFormik.isSubmitting}
                    label={'Đổi mật khẩu'}
                    isLoading={isSubmitting}
                    type="submit"
                    className="w-100 button-primary"
                    onClick={handleSubmit}
                  >
                    {t('changePassword:changePassword')}
                  </ButtonCommon>
                </div>
              </Row>
            </Form>
          )}
        </Formik>
      </Col>
      <Col xs={6} md={6}>
        <div className="custom-form--image-wrap">
          <LoginImage className="image-login" />
        </div>
      </Col>
    </Row>
  );
};

ChangePasswordPage.propTypes = propTypes;
export default ChangePasswordPage;
