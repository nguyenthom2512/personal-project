import React, { Fragment, useCallback, useEffect, useMemo, useState } from 'react';
import AddResult from 'components/AddResult';
import { Button, Card, CardBody, CardHeader, Col, Label, Row } from 'reactstrap';
import { chooseField } from 'helpers';
import { FiPlusCircle } from 'react-icons/fi';
import { MdClear } from 'react-icons/md';
import { Field } from 'formik';
import TooltipOkr from 'components/AddResult/TooltipOkr';
import InputField from 'components/CustomField/InputField';
import smart from '../Data/smart';
import SelectField from 'components/CustomField/SelectField';
import DateFieldFormik from 'components/CustomField/DateFieldFormik';
import authServices from 'services/authServices';
import useDataCreateOKR from 'hooks/useGetDataCreateOKR';
import { isEmpty } from 'lodash';
import SelectMultipleField from 'components/CustomField/SelectMultipleField';
import { useTranslation } from 'react-i18next';

const propTypes = {};

const ArrayOfOkrResult = (props) => {
  //! State
  const { t } = useTranslation();
  const [suggest, setSuggest] = useState([]);
  const [dataOKR] = useDataCreateOKR();
  const { arrayHelpers } = props;
  const { form, name, push, remove } = arrayHelpers;
  const { values, setFieldValues } = form;
  const thisValue = values[name];

  const { default_okr: defaultOkr } = values;

  //! Function
  const clickSuggest = useCallback(
    (item, index) => () => {
      const { created_time, id, okr, ...remainData } = item;
      const newKR = { ...remainData, user: 6 };
      setFieldValues(`${name}.${index}`, newKR);
    },
    [name, setFieldValues],
  );

  const dataOKRParent = useMemo(() => {
    return dataOKR?.OKR || [];
  }, [dataOKR?.OKR]);

  const userRAIC = (dataOKR?.AppUser || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.full_name,
    };
  });

  useEffect(() => {
    const valueOKRParent = values.okr_parent;

    const dataSuggest = dataOKRParent.find((elm) => elm.id.toString() === valueOKRParent.toString());
    if (dataSuggest && !isEmpty(dataSuggest)) {
      setSuggest(dataSuggest.okr_result);
    }
  }, [dataOKRParent, values.okr_parent]);

  //! Render
  return (
    <Fragment>
      <div className="form-group row">
        <Label className="col-2"></Label>
        <div className="col-10">
          {thisValue.map((_, index) => {
            return (
              <Card key={`${name}-${index}`} className="add_result-card mt-2">
                <div className="d-flex justify-content-end">
                  <Button className={`btn-clear p-1 ${values.okr_result.length < 2 && 'd-none'}`} type="button">
                    <MdClear size="16" onClick={() => remove(index)} />
                  </Button>
                </div>
                <CardBody>
                  <div>
                    {suggest.map((elm) => (
                      <Button
                        outline
                        color="primary"
                        className="rounded-pill me-2"
                        key={elm.id}
                        onClick={clickSuggest(elm, index)}
                      >
                        {elm.key_result}
                      </Button>
                    ))}
                  </div>
                  <Row className="row">
                    <Col className="col-8">
                      <Label className="w-100">
                        <TooltipOkr
                          id={'key_result'}
                          target={'key_result'}
                          textTooltip={smart.A}
                          subTitle={
                            <>
                              (A) {t('common:keyResult')}
                              <span style={{ color: 'red' }}> *</span>
                            </>
                          }
                        />
                      </Label>
                      <Field
                        name={`${name}.${index}.key_result`}
                        className="form-control"
                        placeholder={t('common:enterKeyResult')}
                        component={InputField}
                      />
                    </Col>
                    <Col className="col-4 justify-content-center">
                      <Label className="w-100">
                        <TooltipOkr
                          id={'targetOkr'}
                          target={'targetOkr'}
                          subTitle={
                            <>
                              (M) {t('common:target')}
                              <span style={{ color: 'red' }}> *</span>
                            </>
                          }
                          textTooltip={smart.M}
                        />
                      </Label>
                      <Field
                        name={`${name}.${index}.target`}
                        className="form-control"
                        placeholder={t('common:enterTarget')}
                        component={InputField}
                      />
                    </Col>
                  </Row>
                  <Row className="row">
                    <Col className="col-4">
                      <Label className="w-100">
                        {t('common:selectUnit')} <span style={{ color: 'red' }}>*</span>
                      </Label>
                      <Field
                        as="select"
                        name={`${name}.${index}.unit`}
                        className="form-control"
                        // label="unit_name"
                        component={SelectField}
                        disabled={values.result_parent}
                        data={chooseField(dataOKR?.Unit, 'unit_name')}
                        defaultOption={t('common:selectUnit')}
                      />
                    </Col>
                    <Col className="col-4">
                      <Label className="w-100">{t('common:planUrl')}</Label>
                      <Field
                        name={`${name}.${index}.plan_url`}
                        className="form-control"
                        placeholder={t('common:useGoogleSheets')}
                        component={InputField}
                      />
                    </Col>
                    <Col className="col-4">
                      <Label className="w-100">{t('common:resultUrl')}</Label>
                      <Field
                        name={`${name}.${index}.result_url`}
                        className="form-control"
                        placeholder={t('common:useGoogleSheets')}
                        component={InputField}
                      />
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      <Label className="w-100">
                        <TooltipOkr
                          id={'deadline'}
                          target={'deadline'}
                          subTitle={
                            <>
                              (T) {t('common:deadline')}
                              <span style={{ color: 'red' }}> *</span>
                            </>
                          }
                          textTooltip={smart.T}
                        />
                      </Label>
                      <Field
                        component={DateFieldFormik}
                        minDate={new Date()}
                        name={`${name}.${index}.deadline`}
                        className="w-100 form-control"
                      />
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            );
          })}
        </div>
      </div>

      {/* Danh sach nguoi chiu trach nhiem */}
      <div className="form-group row">
        <Label className="col-2"></Label>
        <div className="align-items-center col-10">
          <Card className="add_result-card">
            <CardHeader>{t('common:listOfResponsiblePerson')}</CardHeader>
            <CardBody>
              <Row>
                <Col xs="12" md="6">
                  <div className="form-group">
                    <Label className="mb-1">{t('common:performer')}</Label>
                    <Field
                      as="select"
                      name="user_responsible"
                      className="form-control"
                      label="full_name"
                      component={SelectMultipleField}
                      data={userRAIC}
                      defaultOption={t('common:select')}
                    />
                  </div>
                </Col>
                <Col xs="12" md="6">
                  <div className="form-group">
                    <Label className="mb-1">{t('common:testerOkr')}</Label>
                    <Field
                      as="select"
                      name="user_accountable"
                      className="form-control"
                      label="full_name"
                      component={SelectMultipleField}
                      data={userRAIC}
                      defaultOption={t('common:select')}
                    />
                  </div>
                </Col>
              </Row>
              <Row>
                <Col xs="12" md="6">
                  <div className="form-group">
                    <Label className="mb-1">{t('common:announcerOkr')}</Label>
                    <Field
                      as="select"
                      name="user_inform"
                      className="form-control"
                      label="full_name"
                      component={SelectMultipleField}
                      data={userRAIC}
                      defaultOption={t('common:select')}
                    />
                  </div>
                </Col>
                <Col xs="12" md="6">
                  <div className="form-group">
                    <Label className="mb-1">{t('common:counselorOkr')}</Label>
                    <Field
                      as="select"
                      name="user_consult"
                      className="form-control"
                      label="full_name"
                      component={SelectMultipleField}
                      data={userRAIC}
                      defaultOption={t('common:select')}
                    />
                  </div>
                </Col>
              </Row>
            </CardBody>
          </Card>
        </div>
      </div>
      {thisValue.length < 5 ? (
        <div className="form-group row">
          <Label className="mb-1 col-2"></Label>
          <div className="align-items-center col-10 d-flex">
            <FiPlusCircle size="16" className="text-black me-2" onClick={() => push(defaultOkr)} />
            {t('common:addKeyResult')}
          </div>
        </div>
      ) : (
        <span></span>
      )}
    </Fragment>
  );
};

ArrayOfOkrResult.propTypes = propTypes;
export default ArrayOfOkrResult;
