const smart = {
  S: 'S - Specifict/Stretching\nOKR phải cụ thể và có tính thách thức',
  M: 'M - Measurable\nKR mục tiêu phải có thể đo lường',
  A: 'A - Achievable/Agreed\n OKR có tính khả thi, đồng lòng',
  R: 'R - Realative\nOKR có tính liên kết',
  T: 'T - Time-bound\nOKR phải có thời hạn cụ thể',
};

export default smart;
