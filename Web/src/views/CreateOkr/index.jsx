import ButtonCommon from 'components/ButtonCommon';
import ArrayField from 'components/CustomField/ArrayField';
import InputField from 'components/CustomField/InputField';
import SelectField from 'components/CustomField/SelectField';
import { FastField, Field, Form, Formik } from 'formik';
import { chooseField, raicType } from 'helpers';
import useTreesView from 'hooks/trees/useTreesView';
import useGetOkrDetail from 'hooks/okr/useGetOkrDetail';
import useDataCreateOKR from 'hooks/useGetDataCreateOKR';
import useDataListOKR from 'hooks/useGetListOKR';
import useToggleDialog from 'hooks/useToggleDialog';
import PropTypes from 'prop-types';
import React, { useMemo, useState } from 'react';
import { FiPlusCircle } from 'react-icons/fi';
import { MdClear } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { useToasts } from 'react-toast-notifications';
import { Button, Col, Label } from 'reactstrap';
import { createOkr, updateOkr } from 'redux/modules/createOkr';
import authServices from 'services/authServices';
import ModalQuiz from 'views/QuizOkr/ModalQuiz';
import * as Yup from 'yup';
import ArrayOfOkrResult from './Components/ArrayOfOkrResult';
import { values } from 'lodash-es';
import { RouteBase } from 'constants/routeUrl';
import { useTranslation } from 'react-i18next';

const linkRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;

const CreateOkrSchema = (t) =>
  Yup.object().shape({
    object_name: Yup.string().required(t('validationForm:thisIsARequiredField')),
    confident: Yup.string().required(t('validationForm:thisIsARequiredField')),
    user: Yup.string().required(t('validationForm:thisIsARequiredField')),
    department: Yup.string().required(t('validationForm:thisIsARequiredField')),
    okr_result: Yup.array().of(
      Yup.object().shape({
        user: Yup.string().required(t('validationForm:thisIsARequiredField')),
        key_result: Yup.string().required(t('validationForm:thisIsARequiredField')),
        target: Yup.number()
          .typeError(t('validationForm:thisFieldCanOnlyEnterNumbers'))
          .min(1, t('validationForm:mustBeGreaterThan1'))
          .required(t('validationForm:thisIsARequiredField')),
        current_done: Yup.string().required(t('validationForm:thisIsARequiredField')),
        unit: Yup.string().required(t('validationForm:thisIsARequiredField')),
        deadline: Yup.date()
          .typeError(t('validationForm:timeIsNotValid'))
          .required(t('validationForm:thisIsARequiredField')),
        plan_url: Yup.string().matches(linkRegex, t('validationForm:incorrectLinkFormat')).nullable(true),
        result_url: Yup.string().matches(linkRegex, t('validationForm:incorrectLinkFormat')).nullable(true),
      }),
    ),

    result_parent: Yup.string()
      .when('okr_parent', {
        is: (value) => Boolean(value),
        then: Yup.string().required(t('validationForm:thisIsARequiredField')),
      })
      .nullable(),
  });
let idDepartment = '';

const propTypes = {
  initialData: PropTypes.object,
  isEdit: PropTypes.bool,
  onUpdateSuccess: PropTypes.func,
  onUpdateFailed: PropTypes.func,
};

const FormOkr = (props) => {
  //! State
  const { t } = useTranslation();
  const { isEdit = false, onUpdateSuccess = () => {}, onUpdateFailed = () => {} } = props;
  const dispatch = useDispatch();
  const history = useHistory();
  const [idOkrCreate, setIdOkrCreate] = useState();
  const { createTree } = useTreesView();

  const userData = authServices.getUserLocalStorage();
  const user = userData?.user?.id;
  const idOkrParent = props?.initialData?.okr_parent;
  const { addToast } = useToasts();

  const [dataOKR] = useDataCreateOKR();
  const [dataListOKR, , , refetchListOkr] = useDataListOKR({
    fields: 'id,owner_okr',
    department: idDepartment,
  });

  const [okrDetail, , , refetchOkrDetail] = useGetOkrDetail({ id: idOkrParent, shouldTrigger: !!idOkrParent });
  const [openDialog, toggleDialog, shouldRenderDialog] = useToggleDialog();

  const [defaultKR, setDefaultKR] = useState({
    key_result: '',
    target: '',
    current_done: 0,
    plan_url: '',
    result_url: '',
    deadline: '',
    unit: '',
    user,
  });
  const listOKR = (dataListOKR || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.owner_okr,
    };
  });

  const listKR = (okrDetail?.okr_result || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.key_result,
    };
  });

  //! Function
  const createUser = (arrayUser = [], type) => {
    return arrayUser.map((el) => {
      return {
        raic_type: type,
        user: el,
        okr: '',
        todo: '',
      };
    });
  };

  const onSubmit = (values, helperFormik) => {
    const { user_responsible, user_accountable, user_inform, user_consult, ...remainValues } = values;
    delete remainValues.default_okr;

    const userResponsible = createUser([...user_responsible], raicType.R);
    const userAccountable = createUser([...user_accountable], raicType.A);
    const userInform = createUser([...user_inform], raicType.I);
    const userConsult = createUser([...user_consult], raicType.C);
    const okrRelate = [...values.okr_relate];
    const userData = [...userResponsible, ...userAccountable, ...userInform, ...userConsult];
    const body = {
      ...remainValues,
      raic_user: userData,
      okr_relate: okrRelate.filter((el) => Boolean(el)),
    };
    if (isEdit) {
      dispatch(
        updateOkr(props.initialData.id, body, {
          onFailed: (err) => {
            onUpdateFailed && onUpdateFailed(err, helperFormik);
          },
          onSuccess: (data) => {
            onUpdateSuccess && onUpdateSuccess(helperFormik, values);
          },
        }),
      );
    } else {
      dispatch(
        createOkr(body, {
          onFailed: (err) => {
            addToast(err, { appearance: 'error' });
            helperFormik.setSubmitting(false);
          },
          onSuccess: (data) => {
            setIdOkrCreate(data);
            addToast(t('common:success'), { appearance: 'success' });
            toggleDialog();
            helperFormik.setSubmitting(false);
          },
        }),
      );
    }
  };

  const onChangeDepartment = (department) => {
    idDepartment = department;
    refetchListOkr({
      fields: 'id,owner_okr',
      department,
    });
  };

  const processKRParent = (idKR, { values, setValues }) => {
    const { okr_result } = values;
    const dataKR = (okrDetail?.okr_result || []).find((el) => el.id.toString() === idKR.toString());
    const fillField = { key_result: dataKR?.key_result, unit: dataKR?.unit };

    const newOKRResult = okr_result.map((el) => {
      return {
        ...el,
        ...fillField,
      };
    });

    // Change default OKR follow key_result
    const newDefaultOkr = {
      ...(values?.default_okr || {}),
      key_result: dataKR?.key_result,
      unit: dataKR?.unit,
    };
    setValues({ ...values, default_okr: newDefaultOkr, okr_result: newOKRResult, result_parent: idKR });
  };

  const refetchListKR = (okrParentId) => {
    refetchOkrDetail(okrParentId);
  };

  //! Render
  const initialValues = useMemo(() => {
    const default_okr = {
      key_result: '',
      target: '',
      current_done: 0,
      plan_url: '',
      result_url: '',
      deadline: '',
      unit: '',
      user,
    };

    if (props.initialData) {
      return {
        default_okr,
        ...props.initialData,
      };
    }

    return {
      default_okr,
      object_name: '',
      content: '',
      is_done: false,
      confident: 0,
      challenge: false,
      percent_changed: 0,
      user,
      department: '',
      okr_parent: '',
      user_responsible: [],
      user_accountable: [],
      user_inform: [],
      user_consult: [],
      okr_relate: [null],
      okr_result: [defaultKR],
      result_parent: '',
    };
  }, [defaultKR, user, props.initialData]);

  return (
    <div className="create_okr-container">
      {!isEdit && (
        <div className="d-flex justify-content-between ms-4">
          <h5 className="create-subtitle">{t('common:addOkr')} </h5>
        </div>
      )}

      <div className={`${isEdit ? '' : 'ms-4'} container`}>
        <Formik
          initialValues={initialValues}
          validationSchema={CreateOkrSchema(t)}
          onSubmit={onSubmit}
          validateOnBlur={false}
          enableReinitialize
          validateOnChange={false}
        >
          {({ values, setFieldValue, setValues, isSubmitting, handleSubmit }) => {
            return (
              <Form className="p-3">
                <div className="form-group row">
                  <Label className="mb-1 col-md-2 col-xs-12 pt-2">
                    {t('common:department')} <span style={{ color: 'red' }}>*</span>
                  </Label>
                  <Col xs="12" md="6">
                    <Field
                      name="department"
                      className="form-control"
                      component={SelectField}
                      data={chooseField(dataOKR.Department, 'department_name')}
                      defaultOption={t('common:selectDepartment')}
                      onFieldChange={(e) => {
                        onChangeDepartment(e);
                        setValues({
                          ...values,
                          department: e,
                          okr_parent: '',
                          result_parent: '',
                        });
                      }}
                    />
                  </Col>
                </div>

                <div className="form-group row">
                  <Label className="mb-1 col-md-2 col-xs-12 pt-2">{t('common:okrParent')} </Label>
                  <Col xs="12" md="6">
                    <Field
                      as="select"
                      name="okr_parent"
                      className="form-control"
                      component={SelectField}
                      data={values.department ? listOKR : []}
                      defaultOption={t('common:selectOkrs')}
                      onFieldChange={(okrParentId) => {
                        if (!okrParentId) {
                          setFieldValue('result_parent', '');
                        } else {
                          refetchListKR(okrParentId);
                        }
                      }}
                    />
                  </Col>
                </div>
                <div className="form-group row">
                  <Label className="mb-1 col-md-2 col-xs-12 pt-2">{t('common:krParent')}</Label>
                  <Col xs="12" md="6">
                    <Field
                      as="select"
                      name="result_parent"
                      className="form-control"
                      component={SelectField}
                      data={values.okr_parent ? listKR : []}
                      defaultOption={t('common:selectKR')}
                      onFieldChange={(idKR) => processKRParent(idKR, { values, setValues })}
                    />
                  </Col>
                </div>
                <div className="form-group row">
                  <Label className="mb-1 col-2 pt-2">
                    {t('common:myTarget')} <span style={{ color: 'red' }}>*</span>
                  </Label>
                  <div className="col-10">
                    <FastField
                      name="object_name"
                      className="form-control"
                      placeholder={t('common:myTarget')}
                      component={InputField}
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <Label className="mb-1 col-2 pt-2">{t('common:content')}</Label>
                  <div className="col-10">
                    <FastField
                      name="content"
                      className="form-control"
                      placeholder={t('common:content')}
                      component={InputField}
                    />
                  </div>
                </div>

                <ArrayField name="okr_result" Component={ArrayOfOkrResult} />

                <div className="form-group row">
                  <Label className="col-md-2 col-xs-12 pt-3">{t('common:okrRelate')}</Label>
                  <div className="col-md-5 col-xs-12 me-2">
                    {values.okr_relate.map((_, index) => (
                      <div key={index} className="okr-relate_input mt-2">
                        <Field
                          name={`okr_relate[${index}]`}
                          className="form-control"
                          component={SelectField}
                          data={listOKR || []}
                          defaultOption={t('common:selectOkrs')}
                        />
                        <Button
                          className={`btn-clear p-1 ${index < 1 && 'd-none'}`}
                          type="button"
                          onClick={() => {
                            setFieldValue(
                              'okr_relate',
                              values.okr_relate.filter((e, ind) => ind !== index),
                            );
                          }}
                        >
                          <MdClear size="16" />
                        </Button>
                      </div>
                    ))}
                  </div>
                  <div className="col-md-2 col-md-3 d-flex">
                    <div className="mt-3">
                      <FiPlusCircle
                        size="16"
                        className="text-black me-2"
                        onClick={() => setFieldValue('okr_relate', [...values.okr_relate, null])}
                      />
                    </div>
                    <div className="text-add_okr-relate">{t('common:addOkrRelate')}</div>
                  </div>
                </div>
                <div className="d-flex aligh-center offset-2">
                  <ButtonCommon
                    className="button-common me-2"
                    onClick={() => history.push(RouteBase.TreeOKR)}
                    label={t('common:cancel')}
                    type={'button'}
                  />
                  <ButtonCommon
                    className="button-common"
                    onClick={handleSubmit}
                    label={isSubmitting ? 'Loading...' : isEdit ? t('common:update') : t('common:save')}
                    outline
                  />
                </div>
              </Form>
            );
          }}
        </Formik>

        {shouldRenderDialog && (
          <ModalQuiz
            isShowModal={openDialog}
            toggle={toggleDialog}
            toggleDialog={toggleDialog}
            data={idOkrCreate}
            dataOKR={dataOKR}
          />
        )}
      </div>
    </div>
  );
};

FormOkr.propTypes = propTypes;
export default FormOkr;
