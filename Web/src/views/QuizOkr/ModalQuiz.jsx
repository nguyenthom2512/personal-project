import { RouteBase } from 'constants/routeUrl';
import PropTypes from 'prop-types';
import React from 'react';
import { useHistory } from 'react-router';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import SelectQuiz from './SelectQuiz';

const propTypes = {
  isShowModal: PropTypes.func,
  toggle: PropTypes.func,
  data: PropTypes.string,
  dataOKR: PropTypes.string,
  toggleDialog: PropTypes.func,
};

const ModalQuiz = ({ isShowModal, toggle, data, dataOKR, toggleDialog }) => {
  //! State
  const history = useHistory();

  //! Function

  //! Render
  return (
    <Modal size="xl" isOpen={isShowModal} toggle={toggle}>
      <ModalHeader
        toggle={() => {
          history.push(RouteBase.TreeOKR);
        }}
      >
        <div className="text-ellipsis">{data.object_name}</div>
      </ModalHeader>
      <ModalBody>
        <SelectQuiz toggleDialog={toggleDialog} inForOkr={data} dataOKR={dataOKR} />
      </ModalBody>
    </Modal>
  );
};

ModalQuiz.propTypes = propTypes;
export default ModalQuiz;
