import ButtonCommon from 'components/ButtonCommon';
import QuizItem from 'components/QuizItem';
import { Form, Formik } from 'formik';
import useGetDataDetailQuiz from 'hooks/quiz/useGetDetailQuiz';
import useGetInspiration from 'hooks/inspiration/useGetInspiration';
import useToggleDialog from 'hooks/useToggleDialog';
import React, { useEffect, useRef, useState } from 'react';
import { useToasts } from 'react-toast-notifications';
import authServices from 'services/authServices';
import getQuizService from 'services/getQuizService';
import * as Yup from 'yup';
import ModalError from './ModalError';
import { useHistory, useParams } from 'react-router';
import { RouteBase } from 'constants/routeUrl';

const propTypes = {};

const QuizOkrSchema = Yup.object().shape({
  question_quiz: Yup.array().of(
    Yup.object().shape({
      answer: Yup.string().required('Đây là trường bắt buộc'),
    }),
  ),
});

const QuizOkrPage = (props) => {
  //! State
  const params = useParams();
  const idInspiration = params?.id || '';
  const userData = authServices.getUserLocalStorage();
  const user = userData?.user?.id;

  const interval = useRef(null);
  const { addToast } = useToasts();
  const history = useHistory();

  const [openDialog, toggleDialog, shouldRenderDialog] = useToggleDialog();
  const [dataIns, loadingIns, errorIns, refetchIns] = useGetInspiration(idInspiration);
  const [dataQuiz, loadingQuiz, errorQuiz, refetchQuiz] = useGetDataDetailQuiz(dataIns?.quiz || '');

  console.log('dataIns', dataIns);

  console.log('dataQuiz', dataQuiz);

  const [idQueue, setIdQueue] = useState();
  const [time, setTime] = useState(0);
  const [showQuiz, setShowQuiz] = useState(true);

  console.log('dataIns', dataIns);

  //! Function
  useEffect(() => {
    setTime(dataQuiz?.duration || 0);
    // setTime(5);
  }, [dataQuiz]);

  const countDown = async () => {
    setShowQuiz(false);
    try {
      const response = await getQuizService.postDataForQuiz({ quiz: dataIns.quiz, user, inspiration: idInspiration });
      response && setIdQueue(response.data.id);
      if (interval.current) clearInterval(interval.current);
      interval.current = setInterval(() => {
        setTime((time) => {
          if (time <= 1) {
            clearInterval(interval);
            toggleDialog();
            return 0;
          }
          return time - 1;
        });
      }, 1000);
    } catch (error) {
      let errText = error.statusText;
      if (error?.response?.data) {
        errText = error?.response?.data.toString();
      }
      addToast(errText, { appearance: 'error' });
    }
  };

  useEffect(() => {
    return () => {
      interval.current && clearInterval(interval.current);
    };
  }, []);

  useEffect(() => {
    // setTime(5);
    setTime(dataQuiz?.duration || 0);
  }, [dataQuiz]);

  const onSubmit = async (body) => {
    try {
      const response = await getQuizService.putDataDetailForQuizOkr({
        body: { ...body, quiz: dataIns.quiz, inspiration: idInspiration },
        id: idQueue,
      });
      if (response) {
        addToast('Hoàn thành bài Quiz!', { appearance: 'success' });
        history.push(RouteBase.TreeOKR);
      }
    } catch (error) {
      let errText = error.statusText;
      if (error?.response?.data) {
        errText = error?.response?.data.toString();
      }
      addToast(errText, { appearance: 'error' });
    }
  };
  const processInitialValue = () => {
    const array = (dataQuiz?.questions || []).map((elm) => {
      return {
        question: elm.id,
        answer: '',
      };
    });
    const newData = {
      question_quiz: array,
      quiz: 1,
      user,
    };
    return newData;
  };
  const closeQuiz = () => {
    history.push(RouteBase.TreeOKR);
  };
  //! Render
  if (loadingQuiz) {
    return <span>Loading...</span>;
  }
  if (errorIns) {
    return <span style={{ color: 'red' }}>{errorIns.toString()}</span>;
  }
  if (errorQuiz) {
    return <span style={{ color: 'red' }}>{errorQuiz.toString()}</span>;
  }
  return (
    <>
      <Formik
        initialValues={processInitialValue(dataQuiz.questions)}
        validationSchema={QuizOkrSchema}
        onSubmit={onSubmit}
        validateOnBlur={false}
        enableReinitialize
        validateOnChange={false}
      >
        {({ values, setFieldValue, setValues, isSubmitting, errors, handleSubmit }) => {
          return (
            <Form>
              {/* <div className="quiz-container"> */}
              {showQuiz ? (
                <div className="d-flex justify-content-between quiz-okr">
                  {/* {time == dataQuiz?.duration ? ( */}
                  <ButtonCommon type="button" onClick={countDown} name="startTime" label="Bắt đầu" />
                  {/* ) : ( */}
                  {/* )} */}
                </div>
              ) : (
                <div>
                  <div className="d-flex justify-content-between quiz-okr">
                    <p className="align-items-center text-center">{time}</p>
                  </div>
                  {(dataQuiz?.questions || []).map((item, index) => {
                    return (
                      <>
                        <QuizItem item={item} index={index} key={index} />
                      </>
                    );
                  })}
                </div>
              )}

              {idQueue && (
                <div className="d-flex justify-content-center quiz-footer">
                  {' '}
                  <ButtonCommon type="submit" className="btn-done" name="completed" label="Hoàn thành" />
                </div>
              )}
              {/* </div> */}
            </Form>
          );
        }}
      </Formik>
      {shouldRenderDialog && (
        <ModalError
          isShowModal={openDialog}
          toggle={toggleDialog}
          titeleModal={'Hết thời gian làm bài!'}
          handleQuiz={closeQuiz}
          // toggleDialog={toggleDialog}
          // data={idOkrCreate}
          // dataOKR={dataOKR}
        />
      )}
    </>
  );
};

QuizOkrPage.propTypes = propTypes;
export default QuizOkrPage;
