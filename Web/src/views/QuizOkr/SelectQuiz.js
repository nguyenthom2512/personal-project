import ButtonCommon from 'components/ButtonCommon';
import CheckBoxFormikField from 'components/CustomField/CheckBoxFormikField';
import SelectField from 'components/CustomField/SelectField';
import SelectMultipleField from 'components/CustomField/SelectMultipleField';
import TextAreaField from 'components/CustomField/TextAreaField';
import { RouteBase } from 'constants/routeUrl';
import { Field, Form, Formik } from 'formik';
import { chooseField } from 'helpers';
import useGetDataQuizOkr from 'hooks/quiz/useGetQuiz';
import React from 'react';
import { useHistory } from 'react-router';
import { useToasts } from 'react-toast-notifications';
import { Col, Row } from 'reactstrap';
import authServices from 'services/authServices';
import getQuizService from 'services/getQuizService';
import * as Yup from 'yup';

const propTypes = {};

const ChallengeOkrSchema = Yup.object().shape({
  quiz: Yup.string().when('inspiration', {
    is: true,
    then: Yup.string().required('Đây là trường bắt buộc'),
  }),
  content: Yup.string().when('challenge', {
    is: true,
    then: Yup.string().required('Đây là trường bắt buộc'),
  }),
  // .required('Đây là trường bắt buộc'),
});
const SelectQuiz = ({ inForOkr, dataOKR, toggleDialog }) => {
  //! State
  const [getQuiz, loadingQuiz, errorQuiz] = useGetDataQuizOkr();
  const userData = authServices.getUserLocalStorage();
  const { addToast } = useToasts();
  const history = useHistory();

  //! Function
  const callApiChallenge = async (data) => {
    if (data?.value?.challenge) {
      const responseChallenge = await getQuizService.postOkrChallenge(data.dataChallenge);
      return responseChallenge;
    } else {
      return null;
    }
  };
  const callApiInspiration = async (data) => {
    if (data?.value?.inspiration) {
      const responseInspiration = await getQuizService.postOkrInspriration(data.dataInspiration);
      return responseInspiration;
    } else {
      return null;
    }
  };
  const onSubmit = async (value) => {
    const dataChallenge = value.company
      ? {
          okr: value.okr,
          public: value.public,
          content: value.content,
          challenged_company: value.challenged_company,
          user: value.user_create,
        }
      : {
          okr: value.okr,
          public: value.public,
          content: value.content,
          challenged_user: value.challenged_user,
          challenged_department: value.challenged_department,
          user: value.user_create,
        };

    const dataInspiration = {
      okr: value.okr,
      user: value.user_create,
      quiz: value.quiz,
      user_do: value.user_do,
    };

    callApiChallenge({ value, dataChallenge })
      .then((res) => {})
      .catch((err) => {
        addToast(err, { appearance: 'Thất bại!' });
      })
      .finally(() => {
        callApiInspiration({ value, dataInspiration });
      });

    Promise.all([])
      .then((values) => {
        let err = [];
        values.map((el) => {
          if (el?.error) {
            err = [...err, el.errorMessage];
          }
        });
        if (err.length > 0) {
          addToast(err, { appearance: 'error' });
        } else {
          addToast('Thành công!', { appearance: 'success' });
          history.push(RouteBase.TreeOKR);
        }
      })
      .catch((er) => console.log({ er }))
      .finally(() => {});
  };
  //! Render
  if (loadingQuiz) {
    return <span>Loading...</span>;
  }
  if (errorQuiz) {
    return (
      <div>
        <span style={{ color: 'red' }}>{errorQuiz.toString()}</span>;
      </div>
    );
  }

  return (
    <Formik
      initialValues={{
        inspiration: false,
        challenge: false,
        company: false,
        public: false,
        content: '',
        // okr: 219,
        okr: inForOkr.id,
        challenged_company: dataOKR.Company, // test id =2
        challenged_department: [],
        challenged_user: [],
        user_create: userData?.user?.id,
        quiz: '',
        user_do: [],
      }}
      validationSchema={ChallengeOkrSchema}
      onSubmit={onSubmit}
      validateOnBlur={false}
      // enableReinitialize={isEdit}
      validateOnChange={false}
    >
      {({ values, setFieldValue, setValues, isSubmitting, handleSubmit, errors }) => {
        return (
          <Form>
            <Row>
              <Col xs="12" md="6">
                <div>
                  <Field className="mt-3" component={CheckBoxFormikField} name="challenge" label="Thách thức" />{' '}
                  <Field className="mt-3" component={TextAreaField} name="content" placeholder="Nội dung" />
                  <div className="mt-3">
                    <span className="mt-3">Thách thức đến</span>
                    <Field className="mt-3" component={CheckBoxFormikField} label="Công ty" name="company" />
                    <div className="mt-3">
                      <Field
                        name="challenged_department"
                        className="form-control"
                        // label="department_name"
                        component={SelectMultipleField}
                        data={chooseField(dataOKR.Department, 'department_name')}
                        defaultOption="Lựa chọn phòng ban"
                        disabled={values.company ? true : false}
                      />
                    </div>
                    <div className="mt-3">
                      <Field
                        name="challenged_user"
                        className="form-control"
                        label="full_name"
                        component={SelectMultipleField}
                        data={chooseField(dataOKR.AppUser, 'full_name')}
                        defaultOption="Lựa chọn người dùng"
                        disabled={values.company ? true : false}
                      />
                    </div>
                    <div className="mt-3">
                      <Field component={CheckBoxFormikField} label="Công khai" name="public" />
                    </div>
                  </div>
                </div>
              </Col>
              <Col xs="12" md="6">
                <div>
                  <Field className="mt-3" component={CheckBoxFormikField} name="inspiration" label="Cảm hứng" />
                  <div className="mt-3">
                    <Field
                      name="quiz"
                      className="form-control"
                      component={SelectField}
                      data={chooseField(getQuiz, 'title')}
                      defaultOption="Lựa chọn câu hỏi"
                      disabled={!values.inspiration ? true : false}
                    />
                  </div>
                  <div className="mt-3">
                    <Field
                      name="user_do"
                      className="form-control"
                      label="full_name"
                      component={SelectMultipleField}
                      data={chooseField(dataOKR.AppUser, 'full_name')}
                      defaultOption="Lựa chọn người dùng"
                      disabled={!values.inspiration ? true : false}
                    />
                  </div>
                </div>
              </Col>
            </Row>
            <Row className="d-flex justify-content-center mt-4">
              <ButtonCommon type="submit" className="col-1" label="Lưu" isLoading={isSubmitting} />
            </Row>
          </Form>
        );
      }}
    </Formik>
  );
};

SelectQuiz.propTypes = propTypes;
export default SelectQuiz;
