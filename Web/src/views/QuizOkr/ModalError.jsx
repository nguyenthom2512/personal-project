import ButtonCommon from 'components/ButtonCommon';
import PropTypes from 'prop-types';
import React from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';

const propTypes = {
  isShowModal: PropTypes.func,
  toggle: PropTypes.func,
  titeleModal: PropTypes.string,
  handleQuiz: PropTypes.func,
};

const ModalError = ({ isShowModal = () => {}, toggle = () => {}, titeleModal = '', handleQuiz = () => {} }) => {
  //! State

  //! Function

  //! Render
  return (
    <Modal isOpen={isShowModal} toggle={toggle}>
      <ModalHeader className="d-flex fw-bold justify-content-center modal-body modal-header">{titeleModal}</ModalHeader>
      <ModalBody className="d-flex justify-content-center modal-body">
        <ButtonCommon onClick={handleQuiz} label="Đồng ý" outline />
      </ModalBody>
    </Modal>
  );
};

ModalError.propTypes = propTypes;
export default ModalError;
