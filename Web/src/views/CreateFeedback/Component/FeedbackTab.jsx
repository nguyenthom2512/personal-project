import InputField from 'components/CustomField/InputField';
import SelectField from 'components/CustomField/SelectField';
import TextAreaField from 'components/CustomField/TextAreaField';
import { Field, Formik } from 'formik';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from 'reactstrap';
const FeedbackTab = () => {
  const { t } = useTranslation();
  return (
    <Formik>
      {() => (
        <>
          <div className="row">
            <div className="col-12 col-lg-6">
              <div className="col-12 d-flex align-items-center mb-4">
                <span style={{ marginRight: 10, width: '30%' }}>Loại ghi nhận: </span>
                <div style={{ minWidth: 300 }}>
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    label="department_name"
                    component={SelectField}
                    defaultOption="Lựa chọn"
                  />
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mb-4 ">
                <span style={{ marginRight: 10, width: '30%' }}>Người được ghi nhận: </span>
                <div style={{ minWidth: 300 }}>
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    label="department_name"
                    component={SelectField}
                    defaultOption="Lựa chọn"
                  />
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mb-4">
                <span style={{ marginRight: 10, width: '30%' }}>Chu kỳ: </span>
                <div style={{ minWidth: 300 }}>
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    label="department_name"
                    component={SelectField}
                    // data={dataDepartment}
                    //   data={chooseField(dataDepartment, 'department_name')}
                    defaultOption="Lựa chọn"
                    //   onFieldChange={(elm) => onSubmit({ ...values, department: elm })}
                  />
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mb-4">
                <span style={{ marginRight: 10, width: '30%' }}>OKRs: </span>
                <div style={{ minWidth: 300 }}>
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    label="department_name"
                    component={SelectField}
                    // data={dataDepartment}
                    //   data={chooseField(dataDepartment, 'department_name')}
                    defaultOption="Lựa chọn"
                    //   onFieldChange={(elm) => onSubmit({ ...values, department: elm })}
                  />
                </div>
              </div>
              <div className="col-12 d-flex align-items-center mb-4">
                <span style={{ marginRight: 10, width: '30%' }}>Tiêu chí: </span>
                <div style={{ minWidth: 300 }}>
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    // label="department_name"
                    component={SelectField}
                    // data={dataDepartment}
                    //   data={chooseField(dataDepartment, 'department_name')}
                    defaultOption="Lựa chọn"
                    //   onFieldChange={(elm) => onSubmit({ ...values, department: elm })}
                  />
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-6">
              <p>
                {`'Recognition' (Ghi nhận) là những đánh giá đặc biệt từ người quản lý đối với nhân viên về sự nỗ lực trong công việc. Sự ghi nhận diễn ra khi kết thúc một OKRs, hoặc trong một tình huống đặc biệt cần sớm Ghi nhận dù đang trong một chu kỳ OKRs.`}
              </p>
            </div>
            <div className="col-12 col-lg-12">
              <div className="col-12 d-flex">
                <span style={{ marginRight: 10, width: '17%' }}>Nội dung: </span>
                <div style={{ minWidth: 300, width: '100%', paddingLeft: 5 }}>
                  <Field
                    as="select"
                    name="department"
                    className="form-control"
                    component={TextAreaField}
                    type="area"
                    style={{ height: 200 }}
                    // data={dataDepartment}
                    //   data={chooseField(dataDepartment, 'department_name')}
                    defaultOption="Lựa chọn"
                    //   onFieldChange={(elm) => onSubmit({ ...values, department: elm })}
                  />
                </div>
              </div>
            </div>
            <div className="col-12 col-lg-12 mt-4">
              <div className="col-12 d-flex">
                <span style={{ marginRight: 10, width: '17%' }}></span>
                <div className="d-flex" style={{ minWidth: 300, width: '100%', paddingLeft: 5 }}>
                  <Button outline className="bottom">
                    <div className="align-items-center d-flex">
                      <span>Hủy bỏ</span>
                    </div>
                  </Button>
                  <Button outline className="bottom" style={{ marginLeft: 20 }}>
                    <div className="align-items-center d-flex">
                      <span>Lưu</span>
                    </div>
                  </Button>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </Formik>
  );
};

export default FeedbackTab;
