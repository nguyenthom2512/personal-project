import { Tab } from 'bootstrap';
import React from 'react';
import Tabs from 'react-bootstrap/Tabs';
import FeedbackTab from './Component/FeedbackTab';
import RecognitionTab from './Component/RecognitionTab';

export default () => {
  return (
    <div className="listokr-container">
      <div className="okr-header d-flex justify-content-between">
        <h4 className="mb-4">Tạo phản hồi, ghi nhận</h4>
      </div>
      <Tabs defaultActiveKey="phanHoi" transition={false} id="noanim-tab-example" className="mb-3">
        <Tab eventKey="phanHoi" title="Phản hồi">
          <FeedbackTab />
        </Tab>
        <Tab eventKey="ghiNhan" title="Ghi nhận">
          <RecognitionTab />
        </Tab>
      </Tabs>
    </div>
  );
};
