import React from 'react';
import PriceItem from './Component/PriceItem';

const propTypes = {};

const PricingPage = (props) => {
  //! State

  //! Function

  //! Render
  return (
    <div className="price-item__page justify-content-center pricing-container">
      <div>
        <div className="pricing-container__title">Bảng giá</div>
        <PriceItem />
      </div>
    </div>
  );
};

PricingPage.propTypes = propTypes;
export default PricingPage;
