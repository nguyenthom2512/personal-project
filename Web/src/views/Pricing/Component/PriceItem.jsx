import ButtonCommon from 'components/ButtonCommon';
import paymentServices from 'services/paymentServices';
import useGetPackage from 'hooks/payment/useGetPackage';
import useGetPayment from 'hooks/payment/useGetPackage';
import React from 'react';
import { ReactComponent as ClickIcon } from '../../../assets/svg/click.svg';
const propTypes = {};

const PriceItem = (props) => {
  //! State
  const [dataPackage, , ,] = useGetPackage();

  //! Function
  const onClickChoose = async () => {
    try {
      const response = await paymentServices.paymentPost({ package_id: 1 });
      console.log('response1', response);
      window.open(response?.data?.url);
      // if (response.status === 201) {
      //   addToast('Thanh toán thành công!', { appearance: 'success' });
      // }
    } catch (error) {
      // addToast('Thanh toán thành công!', { appearance: 'error' });
    }
  };

  //! Render
  return (
    <div className="d-flex align-items-center">
      {dataPackage.map((elm, index) => {
        return (
          <div
            key={elm}
            style={{ flex: '1', marginLeft: '30px' }}
            className={`d-flex flex-column ${
              index == '1' ? 'price-item-advise__container' : ''
            } price-item__container price-item`}
          >
            <div className="price-item__container-header">
              <div className="d-flex justify-content-between ">
                <div className="price-item__container-header_title__text">{elm.package_name}</div>
                {elm.is_recommend == '1' && (
                  <div className="price-item__container-header_title__advise">
                    <span className="price-item_advise_text"> Khuyên dùng</span>
                  </div>
                )}
              </div>

              <div className="price-item__container-header_subtitle">
                <span className="price-item__container-header_subtitle-subtag">
                  {(elm.price || 0).toLocaleString('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                  })}{' '}
                </span>
                <span>/{elm.duration}Tháng </span>
              </div>
            </div>

            <div className="detail-price__body ">
              <div className="d-flex align-items-center">
                <div className="price-item__container-content__icon">
                  <ClickIcon />
                </div>
                <div className="price-item__container-content__text">
                  <span className="price-item__container-content__text_bold me-1">{elm?.description}</span>
                </div>
              </div>
              <div className="price-item__container-footer">
                <ButtonCommon className="col-12" label="Lựa chọn" onClick={onClickChoose} />{' '}
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

PriceItem.propTypes = propTypes;
export default PriceItem;
