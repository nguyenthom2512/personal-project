import { createContext } from 'react';

export const IndicatorContext = createContext({
  loading: false,
  setLoading: (value) => {},
});
