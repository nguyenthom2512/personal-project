import flatten from 'flat';
import message from './message.json';
import sidebar from './sidebar.json';
import common from './common.json';
import login from './login.json';
import treeOkr from './treeOkr.json';
import checkin from './checkin.json';
import CFRS from './CFRS.json';
import resetPassword from './resetPassword.json';
import register from './register.json';
import changePassword from './changePassword.json';
import dashboard from './dashboard.json';
import validationForm from './validationForm.json';

const locale = {
  message: flatten(message, {
    delimiter: '_',
  }),
  sidebar: flatten(sidebar, {
    delimiter: '_',
  }),
  common: flatten(common, {
    delimiter: '_',
  }),
  login: flatten(login, {
    delimiter: '_',
  }),

  treeOkr: flatten(treeOkr, {
    delimiter: '_',
  }),
  checkin: flatten(checkin, {
    delimiter: '_',
  }),
  CFRS: flatten(CFRS, {
    delimiter: '_',
  }),
  resetPassword: flatten(resetPassword, {
    delimiter: '_',
  }),
  register: flatten(register, {
    delimiter: '_',
  }),
  changePassword: flatten(changePassword, {
    delimiter: '_',
  }),
  dashboard: flatten(dashboard, {
    delimiter: '_',
  }),
  validationForm: flatten(validationForm, {
    delimiter: '_',
  }),
};
export default locale;
