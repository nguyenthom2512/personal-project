import React, { useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import './scss/styles.scss';
import SecureRoute from 'routes/SecureRoute';
import PrivateRoute from 'components/PrivateRoute';
import { RouteBase } from 'constants/routeUrl';
import { checkAuth } from 'redux/modules/auth';
import LoginPage from 'views/Login';
import RegisterPage from 'views/Register';
import { ToastProvider } from 'react-toast-notifications';
import { GetAuthSelector } from 'redux/selectors';
import LoadingIndicator from 'components/LoadingIndicator';
import { IndicatorContext } from 'context';
import httpServices from 'services/httpServices';
import { useToasts } from 'react-toast-notifications';
import ResetPasswordPage from 'views/ResetPassword';
import PricingPage from 'views/Pricing';
import ChangePasswordPage from 'views/ChangePassword';

const AppWithToast = ({ children }) => {
  const toast = useToasts();

  useEffect(() => {
    httpServices.addToastToInterceptors(toast);
  }, []);

  return children;
};

const App = () => {
  const dispatch = useDispatch();
  const auth = GetAuthSelector();

  const [loading, setLoading] = React.useState(false);
  const setLoadingState = (value) => {
    setLoading(value);
  };

  const loadingStore = {
    loading,
    setLoading: setLoadingState,
  };
  useEffect(() => {
    dispatch(checkAuth());
  }, [dispatch]);

  if (auth.loadingAuth) {
    return null;
  }

  //! Render
  return (
    <ToastProvider autoDismissTimeout={3000} autoDismiss newestOnTop>
      <AppWithToast>
        <IndicatorContext.Provider value={loadingStore}>
          <Router>
            <Switch>
              <Route path={RouteBase.Login} exact component={LoginPage} />
              <Route path={RouteBase.ChangePassWord} exact component={ChangePasswordPage} />
              <Route path={RouteBase.ResetPassword} exact component={ResetPasswordPage} />
              <Route path={RouteBase.Register} exact component={RegisterPage} />
              <Route path={RouteBase.Pricing} exact component={PricingPage} />
              <PrivateRoute path={RouteBase.Home} component={SecureRoute} />
            </Switch>
          </Router>
          {loading && <LoadingIndicator />}
        </IndicatorContext.Provider>
      </AppWithToast>
    </ToastProvider>
  );
};

export default App;
