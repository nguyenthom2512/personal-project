import { useHistory } from 'react-router';
import queryString from 'query-string';
import { useCallback } from 'react';

const useQueryParams = () => {
  const history = useHistory();
  const search = history?.location?.search || '';

  const setQueryParams = useCallback(
    (field = '', value = '') => {
      const searchParsed = queryString.parse(search);
      searchParsed[field] = value;
      history.replace({
        pathname: history.location.pathname,
        search: queryString.stringify(searchParsed),
      });
    },
    [history, search],
  );

  return {
    queryParams: queryString.parse(search),
    setQueryParams,
  };
};

export default useQueryParams;
