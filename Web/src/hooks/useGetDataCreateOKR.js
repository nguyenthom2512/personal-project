import { useEffect, useState } from 'react';
import dataForCreateService from 'services/dataCreateOkrService';

const useDataCreateOKR = (filters) => {
  const [data, setData] = useState({ AppUser: [], Department: [], Unit: [] });
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await dataForCreateService.getDataForCreateOkr();
      setData(response?.data || { AppUser: [], Department: [], Unit: [] });
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dataForCreateService.getDataForCreateOkr();
        setData(response?.data || { AppUser: [], Department: [], Unit: [] });
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return [data, loading, error, refetch];
};

export default useDataCreateOKR;
