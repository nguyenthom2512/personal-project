import { useEffect, useState } from 'react';
import reportServices from 'services/reportServices';

const useGetDataReport = (param) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async (params) => {
    try {
      const response = await reportServices.getReportCheckin(params);
      setData(response?.data || []);
    } catch (error) {
      setError(error);
    }
  };

  return [data, loading, error, refetch];
};

export default useGetDataReport;
