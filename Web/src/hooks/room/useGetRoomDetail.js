import { isObject } from 'lodash';
import { useEffect, useState } from 'react';
import roomServices from 'services/roomServices';

const useGetRoomDetail = (id) => {
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await roomServices.getRoom(id);
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      }
    });
  };

  //   useEffect(() => {
  //     setLoading(true);
  //     const fetchData = async () => {
  //       try {
  //         const response = await roomServices.getRoom(id);
  //         //* api
  //         if (isObject(response?.data)) {
  //           setData(response?.data);
  //         }
  //       } catch (error) {
  //         setError(error);
  //       } finally {
  //         setLoading(false);
  //       }
  //     };

  //     fetchData();
  //   }, [id]);

  return [data, loading, error, refetch];
};

export default useGetRoomDetail;
