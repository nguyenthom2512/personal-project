import { useEffect, useState } from 'react';
import detailListOkrService from 'services/detailListOkrService';
import { isObject } from 'lodash';

const useGetDetailListOkr = (id) => {
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = () => {
    return new Promise(async (resolve, reject) => {
      setLoading(true);
      try {
        const response = await detailListOkrService.getDetailListOkr(id);
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      } finally {
        setLoading(false);
      }
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await detailListOkrService.getDetailListOkr(id);
        //* api
        if (isObject(response?.data)) {
          setData(response?.data);
        }
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  return [data, loading, error, refetch];
};

export default useGetDetailListOkr;
