import { useEffect, useState } from 'react';
import { isObject } from 'lodash';
import deleteOkrService from 'services/deleteOkrService';

const useDeleteOkr = (id) => {
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const deleteOKR = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await deleteOkrService.deleteOkr(id);
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      }
    });
  };

  //   useEffect(() => {
  //     const fetchData = async () => {
  //       try {
  //         setLoading(true);
  //         const response = await deleteOkrService.deleteOkr(id);
  //         //* api
  //         if (isObject(response?.data)) {
  //           setData(response?.data);
  //         }
  //         setLoading(false);
  //       } catch (error) {
  //         setError(error);
  //         setLoading(false);
  //       }
  //     };
  //     fetchData();
  //   }, [id]);

  return [data, loading, error, deleteOKR];
};

export default useDeleteOkr;
