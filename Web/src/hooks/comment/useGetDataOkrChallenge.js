import { isObject } from 'lodash';
import { useEffect, useState } from 'react';
import dataQuizOkrService from 'services/getQuizService';

const useGetDataOkrChallenge = (id) => {
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = () => {
    return new Promise(async (resolve, reject) => {
      setLoading(true);
      try {
        const response = await dataQuizOkrService.getOkrChallenge(id);
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      } finally {
        setLoading(false);
      }
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dataQuizOkrService.getOkrChallenge(id);
        //* api
        if (isObject(response?.data)) {
          setData(response?.data);
        }
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  return [data, loading, error, refetch];
};

export default useGetDataOkrChallenge;
