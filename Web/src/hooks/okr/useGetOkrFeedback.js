import { useEffect, useState } from 'react';
import feedbackServices from 'services/feedbackServices';

const useGetOkrFeedBack = (params) => {
  const [data, setData] = useState({});
  const [dataStatic, setDataStatic] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const refetch = async () => {
    try {
      const response = await feedbackServices.getFeedbackReceived(params);
      const responseStatic = await feedbackServices.getFeedbackStatic();
      setData(response?.data || []);
      setDataStatic({ ...responseStatic?.data });
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await feedbackServices.getFeedbackReceived(params);
        setData({ ...response?.data });
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [params]);

  useEffect(() => {
    const fetchDataStatic = async () => {
      try {
        setLoading(true);
        const response = await feedbackServices.getFeedbackStatic();
        setDataStatic({ ...response?.data });
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };
    fetchDataStatic();
  }, []);
  return [data, dataStatic, loading, error, refetch];
};

export default useGetOkrFeedBack;
