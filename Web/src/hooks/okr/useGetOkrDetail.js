import { isEmpty, isUndefined } from 'lodash-es';
import { useEffect, useState } from 'react';
import OkrServices from 'services/OkrServices';

const useGetOkrDetail = ({ id, shouldTrigger = false }) => {
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async (idOkr) => {
    if (isEmpty(idOkr + '') || isUndefined(idOkr)) {
      if (isEmpty(id + '') || isUndefined(id)) {
        setData([]);
        return;
      }
    }
    try {
      const response = await OkrServices.getOkrDetail(idOkr ? idOkr : id);
      setData(response?.data || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      if (isEmpty(id + '') || isUndefined(id)) {
        setData([]);
        return;
      }
      try {
        setLoading(true);
        const response = await OkrServices.getOkrDetail(id);
        setData(response?.data || []);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };
    if (id && shouldTrigger) {
      fetchData();
    }
  }, [shouldTrigger, id]);

  return [data, loading, error, refetch];
};

export default useGetOkrDetail;
