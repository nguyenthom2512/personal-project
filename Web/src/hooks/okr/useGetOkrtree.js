import { useEffect, useRef, useState } from 'react';
import OkrServices from 'services/OkrServices';

const useGetOkrTree = (id) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const refetch = async () => {
    try {
      const response = await OkrServices.getOkrTree(id);
      setData(response?.data || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await OkrServices.getOkrTree(id);
        setData(response?.data || []);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  return [data, loading, error, refetch];
};

export default useGetOkrTree;
