import { useEffect, useState } from 'react';
import OkrServices from 'services/OkrServices';

const useGetRelateList = ({ id }) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await OkrServices.getOkeRelateList(id);
      setData(response?.data.okr_relate || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await OkrServices.getOkeRelateList(id);
        setData(response?.data?.okr_relate || []);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return [data, loading, error, refetch];
};

export default useGetRelateList;
