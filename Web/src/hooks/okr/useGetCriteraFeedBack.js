import { useEffect, useState } from 'react';
import OkrServices from 'services/OkrServices';

const useGetCriteraFeedBack = (filters) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const refetch = async (params) => {
    try {
      const response = await OkrServices.getCriteriaFeedBack(params);
      setData(response?.data || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect((params) => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await OkrServices.getCriteriaFeedBack(params);
        setData(response?.data || []);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return [data, loading, error, refetch];
};

export default useGetCriteraFeedBack;
