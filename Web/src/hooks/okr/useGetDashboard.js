import { useEffect, useState } from 'react';
import dashboardService from 'services/dashboardService';

const useGetOkrFeedBack = (params) => {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const refetch = async () => {
    try {
      const response = await dashboardService.getWorkProcessData(params);
      setData(response?.data || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dashboardService.getWorkProcessData(params);
        setData({ ...response?.data });
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [params]);

  return [data, loading, error, refetch];
};

export default useGetOkrFeedBack;
