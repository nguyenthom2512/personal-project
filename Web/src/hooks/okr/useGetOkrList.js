import { useEffect, useState } from 'react';
import okrResultServices from 'services/OkrServices';

const useGetOkrList = (params) => {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const refetch = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await okrResultServices.getOkrList(params);
        setData(response?.data);
        resolve();
      } catch (error) {
        setError(error);
        reject(error);
      }
    });
  };

  useEffect(() => {
    const fetch = async () => {
      setLoading(true);
      try {
        const response = await okrResultServices.getOkrList(params);
        setData(response?.data);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetch();
  }, [params]);

  return { data, loading, error, refetch };
};

export default useGetOkrList;
