import { isObject } from 'lodash';
import { useEffect, useState } from 'react';
import { useToasts } from 'react-toast-notifications';
import dataQuizOkrService from 'services/getQuizService';

const useGetDataQuizOkr = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await dataQuizOkrService.getDataForQuiz();
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      }
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dataQuizOkrService.getDataForQuiz();

        //* api
        if (isObject(response?.data)) {
          setData(response?.data);
        }
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return [data, loading, error, refetch];
};

export default useGetDataQuizOkr;
