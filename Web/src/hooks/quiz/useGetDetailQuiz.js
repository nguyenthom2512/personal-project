import { isObject } from 'lodash';
import { useEffect, useState } from 'react';
import getQuizService from 'services/getQuizService';

const useGetDataDetailQuiz = (id) => {
  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await getQuizService.getDataDetailForQuizOkr(id);
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      }
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await getQuizService.getDataDetailForQuizOkr(id);
        //* api
        if (isObject(response?.data)) {
          setData(response?.data);
        }
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    if (id) fetchData();
  }, [id]);

  return [data, loading, error, refetch];
};

export default useGetDataDetailQuiz;
