import { isObject } from 'lodash';
import { useEffect, useState } from 'react';
import getUserService from 'services/getUserService';

const useGetUserOkr = (param) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = (params, isDepartment) => {
    return new Promise(async (resolve, reject) => {
      try {
        let response = {};
        if (isDepartment) {
          response = await getUserService.getDataUser(params);
        } else {
          response = await getUserService.getDataOkrUser(params);
        }
        if (isObject(response?.data)) {
          setData(response?.data);
          resolve(response?.data);
        }
      } catch (error) {
        setError(error);
        reject(error);
      }
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await getUserService.getDataUserOkr(param);
        //* ap
        if (isObject(response?.data)) {
          setData(response?.data);
        }
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return [data, loading, error, refetch];
};

export default useGetUserOkr;
