import { isObject } from 'lodash';
import NotificationModel from 'models/notification.model';
import { useEffect, useState } from 'react';
import notificationService from 'services/notificationService';

let totalPage = 1;
let params = { page: 1, page_size: 20 };

const useGetDataNotification = () => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loadMore, setLoadMore] = useState(false);

  const refetch = () => {
    params = { ...params, page: 1 };
    totalPage = 1;
    return new Promise(async (resolve, reject) => {
      try {
        const response = await notificationService.getNotification(params);
        if (isObject(response?.data)) {
          totalPage = Math.ceil(response?.data?.count / params.page_size);
          setData(NotificationModel.parseDataFromResponseList(response));

          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      }
    });
  };

  const nextPage = () => {
    if (totalPage === params.page || loadMore) return;
    params = { ...params, page: (params.page += 1) };
    setLoadMore(true);
    return new Promise(async (resolve, reject) => {
      try {
        const response = await notificationService.getNotification(params);
        if (isObject(response?.data)) {
          totalPage = Math.ceil(response?.data?.count / params.page_size);

          //* api
          setData((prev) => [...prev, ...NotificationModel.parseDataFromResponseList(response)]);
          resolve();
        }
      } catch (error) {
        setError(error);
        reject();
      } finally {
        setLoadMore(false);
      }
    });
  };

  useEffect(() => {
    const fetchData = async () => {
      params = { ...params, page: 1 };
      try {
        setLoading(true);
        const response = await notificationService.getNotification();
        totalPage = Math.ceil(response?.data?.count / params.page_size);
        //* api
        setData(NotificationModel.parseDataFromResponseList(response));
      } catch (error) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return [data, loading, error, refetch, nextPage];
};

export default useGetDataNotification;
