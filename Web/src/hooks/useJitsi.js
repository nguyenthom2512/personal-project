import { useEffect, useState } from 'react';
import { CONFIG_JITSI, INTERFACE_CONFIG } from 'helpers/jitsiConfig';

const useDeleteOkr = ({ roomId = '', height = '', userInfo = {} }) => {
  const OPTIONS = {
    roomName: '',
    width: '100%',
    height: 500,
    configOverwrite: CONFIG_JITSI,
    interfaceConfigOverwrite: INTERFACE_CONFIG,
    parentNode: document.querySelector('#jitsi-iframe'),
    userInfo: {
      displayName: '',
    },
  };
  const [jitsi, setJitsi] = useState(null);
  useEffect(() => {
    if (window.JitsiMeetExternalAPI) {
      setJitsi(new window.JitsiMeetExternalAPI({ ...OPTIONS, roomId, height, userInfo }));
    } else {
      alert('JitsiMeetExternalAPI not loaded');
    }
  }, []);
  return [jitsi];
};

export default useDeleteOkr;
