import TreeModel from 'models/tree.model';
import { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import listOkrService from 'services/listOkrService';

const useGetNodeByDepartment = (
  params = {
    ...TreeModel.initialFilters(),
    okr_parent__isnull: false,
    okr_parent: '',
  },
  shouldTrigger = true,
) => {
  const { t } = useTranslation();
  const [trees, setTrees] = useState({});

  //! Function
  const convertData = useCallback(
    (el) => {
      return {
        name: el.object_name,
        id: el.id,
        children: el?.children || [],
        percent_completed: `${(el?.percent_completed * 100).toFixed(2)}%`,
        is_match: el.is_match,
        attributes: [
          {
            title: t('treeOkr:progress'),
            value: `${(el?.percent_completed * 100).toFixed(2)}%`,
          },
        ],
      };
    },
    [t],
  );

  useEffect(() => {
    const fetch = async () => {
      try {
        const response = await listOkrService.getListOkr(params);
        const data = TreeModel.parseDataFromResponse(response);
        const results = data?.results?.map((el) => convertData(el));

        setTrees(results);
      } catch (error) {}
    };
    if (shouldTrigger) {
      fetch();
    }
  }, [shouldTrigger]);

  return {
    trees,
    setTrees,
  };
};

export default useGetNodeByDepartment;
