import { isArray, isEmpty } from 'lodash-es';
import { useCallback } from 'react';

const findTreeById = (id) => {
  let itemFounded = {};

  const recursion = (array = []) => {
    for (let i = 0; i < array.length; i++) {
      if (array[i]?.id?.toString() === id?.toString()) {
        itemFounded = array[i];
        break;
      }

      if (isArray(array[i].children)) {
        recursion(array[i].children);
      }
    }

    return itemFounded;
  };

  return recursion;
};

const useTreesView = () => {
  const departmentTrees = JSON.parse(sessionStorage.getItem('treeScreen') || '{}');

  const updateTree = useCallback(
    ({ id = '', name = '' }) => {
      const itemFounded = findTreeById(id)(departmentTrees.trees.children);
      if (!isEmpty(itemFounded)) {
        itemFounded.name = name;
        sessionStorage.setItem('treeScreen', JSON.stringify(departmentTrees));
      }
    },
    [departmentTrees],
  );

  // const createTree = useCallback(({ data }) => {
  //   if (data?.department.toString() === departmentTrees?.department.toString()) {
  //     if (!data.okr_parent) {
  //       const newChildren = [...departmentTrees.trees.children];
  //       newChildren.push({
  //         name: data.object_name,
  //         id: data.id,
  //         percent_completed: '',
  //         children: [],
  //         attributes: [{}],
  //       });
  //       sessionStorage.setItem('treeScreen', JSON.stringify({ ...departmentTrees, children: newChildren }));
  //     }
  //   }
  // });

  return {
    trees: departmentTrees,
    updateTree,
  };
};
export default useTreesView;
