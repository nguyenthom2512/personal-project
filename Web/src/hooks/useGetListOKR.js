import { useEffect, useState } from 'react';
import listOkrService from 'services/listOkrService';

const useDataListOKR = (param) => {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async (params) => {
    try {
      const response = await listOkrService.getDataListOkr(params);
      setData(response?.data || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await listOkrService.getDataListOkr(param);
        setData(response?.data || []);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };
    fetchData();
  }, []);

  return [data, loading, error, refetch];
};

export default useDataListOKR;
