import React from 'react';

const LoadingIndicator = () => {
  return (
    <div className="indicator-loading">
      <div className="lds-ripple">
        <div></div>
        <div></div>
      </div>
    </div>
  );
};

export default LoadingIndicator;
