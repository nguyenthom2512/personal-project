import Sidebar from 'components/Sidebar';
import React from 'react';
import { HiOutlineMenu } from 'react-icons/hi';
import { useDispatch } from 'react-redux';
import { Button, Collapse } from 'reactstrap';

const propTypes = {};

const NavBarCommon = ({ toggle = () => {}, isOpen = false, content, icon }) => {
  //! State
  const dispatch = useDispatch();

  //! Function

  //! Render
  return (
    <div>
      <Button color="ordinary" onClick={toggle}>
        {icon}
      </Button>
      {content}
    </div>
  );
};

NavBarCommon.propTypes = propTypes;
export default NavBarCommon;
