import React from 'react';
// import { Toast, ToastBody, ToastHeader } from 'reactstrap';

// const Notication = ({ show, toggle }) => {
//   return (
//     <Toast isOpen={show}>
//       <ToastHeader toggle={toggle} icon="danger">
//         Reactstrap
//       </ToastHeader>
//       <ToastBody>This is a toast with a danger icon — check it out!</ToastBody>
//     </Toast>
//   );
// };
// export default Notication;
import { useToasts } from 'react-toast-notifications';
import { Button } from 'reactstrap';

export const ToastDemo = ({ content }) => {
  const { addToast } = useToasts();
  return (
    <Button
      onClick={() =>
        addToast(content, {
          appearance: 'error',
          autoDismiss: true,
        })
      }
    >
      Add Toast
    </Button>
  );
};
