import NotificationItem from 'components/NotificationItem';
import useGetDataNotification from 'hooks/notification/useGetDataNotification';
import PropTypes from 'prop-types';
import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Button, PopoverBody, PopoverHeader, UncontrolledPopover } from 'reactstrap';
import { ReactComponent as Bell } from '../../assets/svg/bell.svg';

const propTypes = {
  popoverOpen: PropTypes.bool,
  togglePopover: PropTypes.func,
  isShowPopover: PropTypes.func,
  title: PropTypes.string,
  subTitle: PropTypes.string,
  color: PropTypes.string,
};

const PopoverNotification = ({ isShowPopover, togglePopover, title, subTitle, color }) => {
  //! State
  // const [dataNotification, , , refetchNotification, nextPage]
  const [dataNotification, loading, error, refetchNotification, nextPage] = useGetDataNotification();
  // const [loadingMore, setLoadingMore] = useState(true);
  // const [items, setItem] = useState(Array.from({ length: 20 }));
  // const divRef = useRef(null);
  // console.log(divRef.current, 'hihihihihi');

  //! Function

  //! Render
  return (
    <div className="popover-notification__container">
      <Button
        onClick={() => {
          !isShowPopover && refetchNotification();
        }}
        className="border-0"
        outline
        id="PopoverFocus"
        type="button"
        label="Launch Popover"
      >
        <Bell fill={color || '#212121'} stroke={color || '#212121'} />
      </Button>
      <UncontrolledPopover
        hideArrow={true}
        isOpen={isShowPopover}
        toggle={togglePopover}
        placement="bottom"
        target="PopoverFocus"
        trigger="legacy"
        className="popover-notification"
      >
        <PopoverHeader>
          <div className="d-flex justify-content-between">
            <div>{title}</div>
          </div>
        </PopoverHeader>
        {dataNotification?.length == 0 ? (
          <PopoverBody>Không có thông báo</PopoverBody>
        ) : (
          <PopoverBody style={{ height: '50vh', padding: 0 }}>
            <InfiniteScroll
              dataLength={dataNotification.length}
              next={nextPage}
              hasMore={true}
              scrollableTarget="scrollableDiv"
              refreshFunction={refetchNotification}
              // pullDownToRefresh
              // pullDownToRefreshThreshold={50}
              // pullDownToRefreshContent={<h3 style={{ textAlign: 'center' }}>&#8595; Pull down to refresh</h3>}
              // releaseToRefreshContent={<h3 style={{ textAlign: 'center' }}>&#8593; Release to refresh</h3>}
            >
              <div
                id="scrollableDiv"
                style={{ height: '50vh', overflowX: 'hidden', overflowY: 'scroll', padding: '0' }}
              >
                {(dataNotification || []).map((item) => (
                  <NotificationItem
                    key={item.id}
                    element={item}
                    refetchList={refetchNotification}
                    closeNotifiList={togglePopover}
                  />
                ))}
              </div>
            </InfiniteScroll>
          </PopoverBody>
        )}
      </UncontrolledPopover>
    </div>
  );
};

PopoverNotification.propTypes = propTypes;
export default PopoverNotification;
