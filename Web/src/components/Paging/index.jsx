import React from 'react';
import ReactPaginate from 'react-paginate';

const Paging = ({ count = 0, pageSize = 1, onSelectPage = () => {} }) => {
  return (
    <div className="custom-paging">
      <ReactPaginate
        pageCount={Math.ceil((count || 0) / pageSize)}
        pageRangeDisplayed={pageSize}
        marginPagesDisplayed={1}
        onPageChange={(elm) => {
          onSelectPage(elm.selected + 1);
        }}
        containerClassName="pagination"
        activeClassName="active"
        pageLinkClassName="page-link"
        breakLinkClassName="page-link"
        nextLinkClassName="page-link"
        previousLinkClassName="page-link"
        pageClassName="page-item"
        breakClassName="page-item"
        nextClassName="page-item"
        previousClassName="page-item"
        previousLabel={<>&laquo;</>}
        nextLabel={<>&raquo;</>}
      />
    </div>
  );
};

export default Paging;
