import ButtonCommon from 'components/ButtonCommon';
import InputField from 'components/CustomField/InputField';
import SelectField from 'components/CustomField/SelectField';
import { FastField, Field, Form, Formik } from 'formik';
import { chooseField } from 'helpers';
import useGetUnit from 'hooks/useGetUnitOkr';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Label, Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap';
import * as Yup from 'yup';

const linkRegex = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i;
const EditOkrList = (t) =>
  Yup.object().shape({
    key_result: Yup.string().required(t('validationForm:thisIsARequiredField')),
    target: Yup.number()
      .typeError(t('validationForm:thisFieldCanOnlyEnterNumbers'))
      .min(1, t('validationForm:mustBeGreaterThan1'))
      .required(t('validationForm:thisIsARequiredField')),
    unit: Yup.string().required(t('validationForm:thisIsARequiredField')),
    plan_url: Yup.string().nullable().matches(linkRegex, t('validationForm:incorrectLinkFormat')),
    result_url: Yup.string().nullable().matches(linkRegex, t('validationForm:incorrectLinkFormat')),
  });

const ModalEditResults = ({ toggle, isOpen, title = '', initialValues = {}, onSubmit = () => {}, unitDisable }) => {
  //! State
  const { t } = useTranslation();
  const [dataUnit] = useGetUnit();

  //! Render
  return (
    <div>
      <Formik
        enableReinitialize
        onSubmit={(values, helperFormik) => onSubmit(values, helperFormik, { toggle })}
        initialValues={initialValues}
        validationSchema={EditOkrList(t)}
        validateOnBlur={false}
        validateOnChange={false}
      >
        {({ values, setFieldValue, errors, setValues, handleSubmit }) => {
          return (
            <Form>
              <Modal isOpen={isOpen} toggle={toggle} style={{ maxWidth: '900px' }}>
                <ModalHeader toggle={toggle}>{title}</ModalHeader>
                <ModalBody>
                  <div>
                    <div className="form-group row">
                      <Label className="mb-1 col-2">{t('common:keyResult')}</Label>
                      <div className="col-10">
                        <FastField name="key_result" className="form-control" component={InputField} />
                      </div>
                    </div>
                    <div className="form-group row">
                      <Label className="mb-1 col-2">{t('common:target')}</Label>
                      <div className="col-10">
                        <FastField name="target" className="form-control" component={InputField} />
                      </div>
                    </div>
                    <div className="form-group row">
                      <Label className="mb-1 col-2">{t('common:unit')}</Label>
                      <div className="col-10">
                        <Field
                          as="select"
                          name={`unit`}
                          className="form-control"
                          component={SelectField}
                          data={chooseField(dataUnit, 'unit_name')}
                          defaultOption={t('common:selectUnit')}
                          disabled={unitDisable}
                        />
                      </div>
                    </div>
                    <div className="form-group row">
                      <Label className="mb-1 col-2">{t('common:planUrl')}</Label>
                      <div className="col-10">
                        <Field
                          name={`plan_url`}
                          className="form-control"
                          placeholder={t('common:useGoogleSheets')}
                          component={InputField}
                        />{' '}
                      </div>
                    </div>
                    <div className="form-group row">
                      <Label className="mb-1 col-2">{t('common:resultUrl')}</Label>
                      <div className="col-10">
                        <Field
                          name={`result_url`}
                          className="form-control"
                          placeholder={t('common:useGoogleSheets')}
                          component={InputField}
                        />{' '}
                      </div>
                    </div>
                  </div>
                </ModalBody>
                <ModalFooter>
                  <ButtonCommon type="submit" onClick={handleSubmit} label={t('common:update')} />
                  <ButtonCommon onClick={toggle} label={t('common:cancel')} link />
                </ModalFooter>
              </Modal>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};
export default ModalEditResults;
