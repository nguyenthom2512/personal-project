import ButtonCommon from 'components/ButtonCommon';
import React from 'react';
import { Button, Modal, ModalBody, ModalFooter } from 'reactstrap';

const AcceptDeletaPage = ({ toggle = () => {}, isShowModal = false, handleDelete = () => {}, titeleModalDelete }) => {
  if (!isShowModal) {
    return null;
  }
  return (
    <Modal isOpen={isShowModal} toggle={toggle}>
      <ModalBody>{titeleModalDelete}</ModalBody>
      <ModalFooter>
        <ButtonCommon onClick={handleDelete} label="Xoá" outline />
        <ButtonCommon onClick={toggle} label="Huỷ" outline />
      </ModalFooter>
    </Modal>
  );
};
export default AcceptDeletaPage;
