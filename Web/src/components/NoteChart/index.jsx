import PropTypes from 'prop-types';
import React from 'react';

const propTypes = {
  label: PropTypes.string,
  style: PropTypes.string,
  type: PropTypes.number,
};

const NoteChart = ({ label, style, type }) => {
  //! State

  //! Function

  //! Render

  const getColorByType = () => {
    switch (type) {
      case 0:
        return {
          border: '1px solid rgba(255, 99, 132, 0.2)',
          backgroundColor: 'rgba(255, 99, 132, 1)',
        };
      case 1:
        return {
          border: '1px solid rgba(54, 162, 235, 0.2)',
          backgroundColor: 'rgba(54, 162, 235, 1)',
        };
      case 2:
        return {
          backgroundColor: 'rgba(255, 206, 86, 0.2)',
          border: '1px solid rgba(255, 206, 86, 1)',
        };
      case 3:
        return {
          backgroundColor: 'rgba(153, 102, 255, 0.2)',
          border: 'rgba(153, 102, 255, 1)',
        };

      default:
        break;
    }
  };

  return (
    <div className="align-items-center d-flex pt-3">
      <div
        style={{
          ...{
            backgroundColor: getColorByType().backgroundColor,
            border: getColorByType().border,
            height: '20px',
            width: '20px',
          },
          ...style,
        }}
      ></div>
      <div className="ms-2" style={{ fontSize: '14px', lineHeight: '16px' }}>
        {label}
      </div>
    </div>
  );
};

NoteChart.propTypes = propTypes;
export default NoteChart;
