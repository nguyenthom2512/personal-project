import React from 'react';
import PropTypes from 'prop-types';
import { get } from 'lodash';
import { Table } from 'reactstrap';

const propTypes = {
  title: PropTypes.string,
  columns: PropTypes.arrayOf(
    PropTypes.shape({
      Header: PropTypes.string,
      accessor: PropTypes.string,
      Cell: PropTypes.func,
      styleHeader: PropTypes.object,
      classNameHeader: PropTypes.string,
      styleCell: PropTypes.object,
      classNameCell: PropTypes.string,
    }),
  ),
  data: PropTypes.array,
};

const TableCommon = ({ title = '', columns = [], data = [] }) => {
  //! State

  //! Function

  //! Render
  return (
    <div className="table-common">
      {title && (
        <div className="table-common__title">
          <div className="text-ellipsis">{title}</div>
        </div>
      )}
      <Table className="overflow-auto">
        <thead>
          <tr>
            {columns.map((el, idx) => {
              return (
                <th style={el?.styleHeader || {}} className={`${el?.classNameHeader || ''} `} key={`${el?.id || idx}`}>
                  {el.Header}
                </th>
              );
            })}
          </tr>
        </thead>
        <tbody>
          {(data || []).map((el) => {
            return (
              <tr key={`${el.id}`} className="">
                {columns.map((col) => {
                  return (
                    <td
                      key={`${el?.id || ''} ${col.accessor}`}
                      className={col?.classNameCell || ''}
                      style={col?.styleCell || {}}
                    >
                      {col?.Cell ? col?.Cell(el) : get(el, col.accessor)}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </Table>
    </div>
  );
};

TableCommon.propTypes = propTypes;
export default TableCommon;
