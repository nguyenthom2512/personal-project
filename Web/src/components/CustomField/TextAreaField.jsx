import React from 'react';
import PropTypes from 'prop-types';
import ErrorMessages from './ErrorMessage';
import { Field } from 'formik';

const propTypes = {
  label: PropTypes.string,
  field: PropTypes.any,
  disabled: PropTypes.bool,
  style: PropTypes.object,
};

const TextAreaField = (props) => {
  //! State
  const { field, label = '', className, style, disabled } = props;
  const { name } = field;

  //! Function

  //! Render
  return (
    <div className="align-items-center">
      <span>{label}</span>
      <textarea
        disabled={disabled}
        color="dark"
        as="textarea"
        {...field}
        style={style}
        className={`form-control ${className}`}
      />

      <ErrorMessages name={name} />
    </div>
  );
};

TextAreaField.propTypes = propTypes;
export default TextAreaField;
