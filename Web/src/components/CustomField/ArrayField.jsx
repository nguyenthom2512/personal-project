import React from 'react';
import { FieldArray } from 'formik';

const ArrayField = (props) => {
  const { name, Component, ...remainProps } = props;

  if (!Component) {
    return null;
  }

  return (
    <FieldArray
      name={name}
      render={(arrayHelpers) => <Component arrayHelpers={arrayHelpers} name={name} {...remainProps} />}
    />
  );
};

ArrayField.defaultProps = {
  name: '',
};

export default React.memo(ArrayField);
