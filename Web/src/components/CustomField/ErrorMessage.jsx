import React from 'react';
import { Field, getIn } from 'formik';

const ErrorMessages = ({ name }) => (
  <Field
    // className="test"
    name={name}
    render={({ form }) => {
      const error = getIn(form.errors, name);
      const touch = getIn(form.touched, name);
      return touch && error ? <div className="error-field">{touch && error ? error : null}</div> : null;
    }}
  />
);

export default ErrorMessages;
