import React, { Fragment } from 'react';
import ErrorMessages from './ErrorMessage';
import { Input } from 'reactstrap';
import _ from 'lodash';

const InputField = (props) => {
  const {
    form,
    field,
    maxLength,
    placeholder,
    type,
    label,
    disabled,
    onChangeCustomize,
    onKeyDown,
    style,
    invalid,
    className,
    icon,
    readOnly,
  } = props;
  const { name, value } = field;
  const { errors, touched } = form;

  const isInvalid = _.get(errors, name) && _.get(touched, name);

  // if (icon) {
  //   return (
  //     <Fragment>
  //       {label && <label htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />}
  //       <div className={`align-items-center d-flex ${className}`}>
  //         {icon}
  //         <input
  //           {...field}
  //           style={style}
  //           className="border-0 w-100 ps-2"
  //           onChange={onChangeCustomize || field.onChange}
  //           type={type}
  //           id={name}
  //           maxLength={maxLength}
  //           value={value}
  //           placeholder={placeholder}
  //           disabled={disabled}
  //           invalid={invalid || (!!errors[name] && touched[name])}
  //           onKeyDown={onKeyDown}
  //         />
  //       </div>
  //       <ErrorMessages name={name} />
  //     </Fragment>
  //   );
  // }

  return (
    <div className="position-relative">
      {label && <label htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />}
      {icon && <div className="input-left-icon">{icon}</div>}
      <Input
        {...field}
        style={{ height: '37.99px' }}
        // style={style}
        className={`${className} position-relative ${icon && 'input-padding-icon'}`}
        onChange={onChangeCustomize || field.onChange}
        type={type}
        id={name}
        maxLength={maxLength}
        value={value}
        placeholder={placeholder}
        disabled={disabled}
        invalid={invalid === 'true' || isInvalid}
        onKeyDown={onKeyDown}
        readOnly={readOnly}
      />
      <ErrorMessages name={name} />
    </div>
  );
};

InputField.defaultProps = {
  type: 'text',
  tabIndex: '0',
  invalid: 'false',
};

export default InputField;
