import DateFnsUtils from '@date-io/date-fns';
import { KeyboardDateTimePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components/macro';
import ErrorMessages from './ErrorMessage';
const Container = styled.div`
  .MuiOutlinedInput-input {
    padding: 9px;
  }
`;
const propTypes = {
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  label: PropTypes.string,
  disablePast: PropTypes.bool,
  disableFuture: PropTypes.bool,
  clearable: PropTypes.bool,
  style: PropTypes.object,
  errorText: PropTypes.string,
  minDate: PropTypes.any,
  maxDate: PropTypes.any,
  emptyLabel: PropTypes.string,
  disabled: PropTypes.bool,
};

const DateTimeFieldFormik = (props) => {
  //! State
  const {
    form = {},
    field = {},
    onChange,
    label = '',
    disablePast = false,
    disableFuture = false,
    initialFocusedDate = new Date(),
    clearable = true,
    format = 'dd/MM/yyyy HH:mm',
    style = {},
    errorText = '',
    minDate = undefined,
    maxDate = undefined,
    emptyLabel = 'dd/mm/yyyy hh:mm',
    disabled: disabledProps = false,
    className = '',
    minDateMessage = '',
    maxDateMessage = '',
    readOnly = false,
    invalidDateMessage = '',
    disableOnChangeDefault = false,
  } = props;
  const { name, value = null } = field;
  const { errors, touched, setFieldValue } = form;
  const disabled = disabledProps || readOnly;

  //! Function
  const handleDateChange = (time) => {
    if (!disableOnChangeDefault) {
      setFieldValue(name, time);
    }
    onChange && onChange(time);
  };

  //! Render
  // if (inFieldPermission) {
  //   if (!isView) {
  //     value = INPUTMASK;
  //     disabled = true;
  //     return (
  //       <Container>
  //         <TextField
  //           type="text"
  //           className={className}
  //           id={name}
  //           name={name}
  //           label={label}
  //           value={value}
  //           fullWidth
  //           onBlur={onBlur || field.onBlur}
  //           disabled={disabled}
  //           InputLabelProps={{
  //             shrink: true,
  //           }}
  //         />
  //       </Container>
  //     );
  //   }

  //   if (!isEdit) {
  //     disabled = true;
  //   }
  // }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Container>
        <KeyboardDateTimePicker
          clearable={clearable}
          className={className}
          style={{
            ...style,
            minWidth: '200px',
            // minWidth: '200px',
          }}
          name={name}
          id={name}
          label={label}
          format={format}
          value={value === '' ? null : value}
          onChange={handleDateChange}
          initialFocusedDate={initialFocusedDate}
          disablePast={disablePast}
          disableFuture={disableFuture}
          emptyLabel={emptyLabel}
          disabled={disabled}
          minDateMessage={minDateMessage}
          maxDateMessage={maxDateMessage}
          minDate={minDate}
          maxDate={maxDate}
          error={!!errorText || (touched[name] && !!errors[name])}
          invalidDateMessage={invalidDateMessage}
          KeyboardButtonProps={{ 'aria-label': 'change date' }}
          inputVariant="outlined"
        />
        <ErrorMessages name={name} />
      </Container>
    </MuiPickersUtilsProvider>
  );
};

DateTimeFieldFormik.propTypes = propTypes;
export default DateTimeFieldFormik;
