import React, { Fragment } from 'react';
import Select from 'react-select';
import ErrorMessages from './ErrorMessage';
import { ReactComponent as ChevronDown } from 'assets/svg/chevron-down.svg';

import _ from 'lodash';
// const customStyles = {
//   option: (styles, { data, isDisabled, isFocused, isSelected }) => {
//     return {
//       ...styles,
//       fontSize: '12px',
//       textAlign: 'left',
//       minWidth: '200px',
//     };
//   },
// };
const SelectField = ({
  form,
  field,
  data = [],
  label = '',
  defaultOption = '',
  onFieldChange = () => {},
  className = '',
  disabled = false,
}) => {
  //! State
  const { name, value } = field;
  const { errors, touched, setFieldValue } = form;

  const isInvalid = _.get(errors, name) && _.get(touched, name);

  //! Render
  const valueOption = () => {
    if (field?.value) {
      return data ? data?.find((option) => option?.value?.toString() === field?.value?.toString()) : '';
    }
    return '';
  };
  return (
    <Fragment>
      {label && <label htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />}

      <Select
        {...field}
        autoSize={false}
        closeMenuOnSelect={true}
        styles={{
          dropdownIndicator: {
            paddingRight: 24,
          },
        }}
        isClearable
        className={`${isInvalid ? 'select-box is-invalid' : ''}`}
        classNamePrefix="react-select"
        // isMulti
        options={data}
        name={field.name}
        isSearchable={false}
        components={{
          IndicatorSeparator: () => {
            return null;
          },
          DropdownIndicator: () => (
            <div style={{ paddingRight: 14 }}>
              <ChevronDown height="12px" />
            </div>
          ),
          Placeholder: () => <span className="placeholder-select">{defaultOption}</span>,
        }}
        placeholder={defaultOption}
        value={valueOption()}
        onChange={(option) => {
          setFieldValue(name, option?.value || '');
          onFieldChange(option?.value || '');
        }}
        onBlur={field.onBlur}
        isDisabled={disabled}
        // style={customStyles}
      />
      {/* <Input
        color="#6C757D"
        style={{ color: '#6C757D' }}
        type="select"
        name={name}
        value={value}
        className={`form-select ${className}`}
        id={name}
        disabled={disabled}
        onChange={(e) => {
          setFieldValue(name, e.target.value);
          onFieldChange(e.target.value);
        }}
      >
        <option style={{ color: '#6C757D' }} hidden>
          {defaultOption}
        </option>
        {data.map((elm, index) => {
          return (
            <option value={elm.id} id={elm.id} key={index}>
              {elm?.[label]}
            </option>
          );
        })}
      </Input> */}
      <ErrorMessages name={name} />
    </Fragment>
  );
};

export default SelectField;
