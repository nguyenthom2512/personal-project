import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import ErrorMessages from './ErrorMessage';

const propTypes = {
  label: PropTypes.string,
  defaultChecked: PropTypes.bool,
  disabled: PropTypes.bool,
};

const CheckBoxFormikField = (props) => {
  //! State
  const { label = '', defaultChecked, disabled } = props;

  //! Function

  //! Render
  return (
    <div className="align-items-center d-flex">
      <input
        defaultChecked={defaultChecked}
        disabled={disabled}
        color="dark"
        type="checkbox"
        style={{
          border: '1px solid #c7c7c7',
          boxSizing: 'border-box',
          borderRadius: '2px',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          maxWidth: 200,
        }}
      />
      <span
        className="ms-2"
        style={{
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          maxWidth: 200,
        }}
      >
        {label}
      </span>
    </div>
  );
};

CheckBoxFormikField.propTypes = propTypes;
export default CheckBoxFormikField;
