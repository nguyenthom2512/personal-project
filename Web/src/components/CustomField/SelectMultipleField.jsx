import React, { Fragment } from 'react';
import Select from 'react-select';
import ErrorMessages from './ErrorMessage';
import { ReactComponent as ChevronDown } from 'assets/svg/chevron-down.svg';

const SelectMultipleField = ({ data = [], field, form, defaultOption, disabled }) => {
  return (
    <div className="custom-multiple-select">
      <Select
        {...field}
        closeMenuOnSelect={false}
        styles={{
          dropdownIndicator: {
            paddingRight: 24,
          },
        }}
        isMulti
        options={data}
        name={field.name}
        components={{
          IndicatorSeparator: () => {
            return null;
          },
          DropdownIndicator: () => (
            <div style={{ paddingRight: 14 }}>
              <ChevronDown height="12px" />
            </div>
          ),
          Placeholder: () => <span className="placeholder-select">{defaultOption}</span>,
        }}
        placeholder={defaultOption}
        value={data ? data?.find((option) => option?.value?.toString() === field?.value?.toString()) : ''}
        onChange={(option) => {
          const onlyId = option.map((elm) => elm.value);
          form.setFieldValue(field.name, onlyId);
        }}
        onBlur={field.onBlur}
        isDisabled={disabled}
      />
      <ErrorMessages name={field.name} />
    </div>
  );
};

export default SelectMultipleField;
