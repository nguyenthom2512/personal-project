import React from 'react';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import PropTypes from 'prop-types';
import styled from 'styled-components/macro';
import ErrorMessages from './ErrorMessage';
import _ from 'lodash';

const Container = styled.div`
  .MuiOutlinedInput-input {
    padding: 9px;
  }
`;
const propTypes = {
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  label: PropTypes.string,
  disablePast: PropTypes.bool,
  disableFuture: PropTypes.bool,
  clearable: PropTypes.bool,
  style: PropTypes.object,
  errorText: PropTypes.string,
  minDate: PropTypes.any,
  maxDate: PropTypes.any,
  emptyLabel: PropTypes.string,
  disabled: PropTypes.bool,
};

const DateFieldFormik = (props) => {
  //! State
  const {
    form = {},
    field = {},
    onChange,
    onBlur,
    label = '',
    disablePast = false,
    disableFuture = false,
    initialFocusedDate = new Date(),
    clearable = true,
    format = 'dd/MM/yyyy',
    style = {},
    errorText = '',
    minDate = undefined,
    maxDate = undefined,
    emptyLabel = 'dd/mm/yyyy',
    disabled: disabledProps = false,
    className = '',
    minDateMessage = '',
    maxDateMessage = '',
    readOnly = false,
    invalidDateMessage = '',

    inputAble = false,
    disableOnChangeDefault = false,
  } = props;
  const { name, value = null } = field;
  const { errors, touched, setFieldValue, initialStatus, handleBlur } = form;

  const isInvalid = _.get(errors, name) && _.get(touched, name);
  const disabled = disabledProps || readOnly;

  //! Function
  const handleDateChange = (time) => {
    if (!disableOnChangeDefault) {
      setFieldValue(name, time);
    }
    onChange && onChange(time);
  };

  // useEffect(() => {
  //   setFieldTouched(name, true);
  // }, [setFieldTouched, name]);

  //! Render
  // if (inFieldPermission) {
  //   if (!isView) {
  //     value = INPUTMASK;
  //     disabled = true;
  //     return (
  //       <Container>
  //         <TextField
  //           type="text"
  //           className={className}
  //           id={name}
  //           name={name}
  //           label={label}
  //           value={value}
  //           fullWidth
  //           onBlur={onBlur || field.onBlur}
  //           disabled={disabled}
  //           InputLabelProps={{
  //             shrink: true,
  //           }}
  //         />
  //       </Container>
  //     );
  //   }

  //   if (!isEdit) {
  //     disabled = true;
  //   }
  // }

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      {label && <label htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />}
      <Container>
        <KeyboardDatePicker
          className={`${className} ${isInvalid ? 'date-picker is-invalid' : ''}`}
          style={{
            ...style,
            minWidth: '200px',
            // minWidth: '200px',
          }}
          name={name}
          id={name}
          label={label}
          format={format}
          value={value === '' ? null : value}
          onChange={handleDateChange}
          initialFocusedDate={initialFocusedDate}
          disablePast={disablePast}
          disableFuture={disableFuture}
          emptyLabel={emptyLabel}
          disabled={disabled}
          minDateMessage={minDateMessage}
          maxDateMessage={maxDateMessage}
          minDate={minDate}
          maxDate={maxDate}
          error={!!errorText || (touched[name] && !!errors[name])}
          invalidDateMessage={invalidDateMessage}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
          inputVariant="outlined"
          onBlur={handleBlur}
        />
        <ErrorMessages name={name} />
      </Container>
    </MuiPickersUtilsProvider>
  );
};

DateFieldFormik.propTypes = propTypes;
export default DateFieldFormik;
