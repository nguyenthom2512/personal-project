import React from 'react';
import PropTypes from 'prop-types';
import ErrorMessages from './ErrorMessage';
import { Link } from '@material-ui/core';
import { Input } from 'reactstrap';

const propTypes = {
  label: PropTypes.string,
  field: PropTypes.any,
  checked: PropTypes.bool,
  value: PropTypes.string,
  isValidate: PropTypes.bool,
  disabled: PropTypes.bool,
  size: PropTypes.number,
  className: PropTypes.string,
};

const RadioField = (props) => {
  //! State
  const { field, label = '', checked, value, isValidate, id, disabled, size, className } = props;
  const { name } = field;

  //! Function

  //! Render
  return (
    <div className={`radio-common align-items-center d-flex ${className}`}>
      <Input
        color="dark"
        type="radio"
        {...field}
        value={value}
        checked={checked}
        onChange={field.onChange}
        id={id}
        disabled={disabled}
      />

      <label htmlFor={id} className="ms-2">
        {label}
      </label>
      {!isValidate && <ErrorMessages name={name} />}
    </div>
  );
};

RadioField.propTypes = propTypes;
export default RadioField;
