import React from 'react';
import { Spinner } from 'reactstrap';

const propTypes = {};

const Loading = (props) => {
  //! State

  //! Function

  //! Render
  return <Spinner {...props} />;
};

Loading.propTypes = propTypes;
export default Loading;
