import React from 'react';
import { FormGroup, Input, Label } from 'reactstrap';
import { MdClear } from 'react-icons/md';

const Selection = ({ onClickClear }) => {
  return (
    <FormGroup className="align-items-center d-flex form-group">
      <Input type="select" name="select" id="exampleSelect">
        <option value="0">Tìm OKRs liên kết chéo</option>
        <option value="1">[Nguyễn Trọng Hùng] Đạt mức 7k</option>
        <option value="2">[Doan Tuan Anh] Thêm 2</option>
        <option value="3">[Doan Tuan Anh] Thêm 3 thành viên vào guild</option>
        <option value="4">[Doan Tuan Anh] Tuyển thêm 3 thành viên </option>
        <option value="5">[Phạm Huy Hoàng - Ark9] Watch 4 pro game mỗi ngày</option>
        <option value="6">[Nguyễn Trọng Hùng] Đạt mức 7k</option>
      </Input>
      <MdClear size="24" color="secondary" onClick={() => onClickClear()} />
    </FormGroup>
  );
};
export default Selection;
