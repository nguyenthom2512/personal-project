import React from 'react';
import { Popover, PopoverBody } from 'reactstrap';

const propTypes = {};

const PopOverCommon = (props) => {
  //! State
  const { children, target = '', isOpen = false, toggle = () => {}, ...remainingProps } = props;

  //! Function

  //! Render
  return (
    <Popover placement="left" trigger="legacy" isOpen={isOpen} target={target} toggle={toggle} {...remainingProps}>
      <PopoverBody>{children}</PopoverBody>
    </Popover>
  );
};

PopOverCommon.propTypes = propTypes;
export default React.memo(PopOverCommon);
