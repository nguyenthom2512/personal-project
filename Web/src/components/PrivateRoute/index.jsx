import { RouteBase } from 'constants/routeUrl';
import React, { useEffect } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import { GetAuthSelector } from 'redux/selectors/auth';
import authServices from 'services/authServices';
import httpServices from 'services/httpServices';

const AppWithToast = ({ children }) => {
  const toast = useToasts();

  useEffect(() => {
    httpServices.addToastToInterceptors(toast);
  }, []);

  return children;
};
const PrivateRoute = (props) => {
  const auth = GetAuthSelector();
  const dataUserStorage = authServices.getUserLocalStorage();
  const { isLogin } = auth;

  // Render
  if (dataUserStorage.isLogged || isLogin) {
    return (
      <>
        <AppWithToast>
          <Route {...props} />
        </AppWithToast>
      </>
    );
  }

  return <Redirect to={RouteBase.Login} />;
};

export default PrivateRoute;
