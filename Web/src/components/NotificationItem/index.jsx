import { RouteBase } from 'constants/routeUrl';
import { useHistory } from 'react-router';
import moment from 'moment';
import 'moment/locale/vi';
import React, { useRef, useState } from 'react';
import { RiMoreLine } from 'react-icons/ri';
import PropTypes from 'prop-types';
import useToggleDialog from 'hooks/useToggleDialog';
import PopoverCommon from 'components/PopoverCommon';
import { FaEdit, FaTrash, FaBellSlash } from 'react-icons/fa';
import notificationService from 'services/notificationService';
import { useToasts } from 'react-toast-notifications';
import genImageUrl from 'helpers/genImageUrl';

const propTypes = {
  element: PropTypes.object,
  refetchList: PropTypes.func,
};

const NotificationItem = ({ element = {}, refetchList = () => {}, closeNotifiList = () => {} }) => {
  //! State
  // const [dataNotification, errorNotification, loadingNotification] = useGetDataNotification();
  const history = useHistory();
  const refMore = useRef(null);
  const [openPopover, togglePopover, shouldRenderPopover] = useToggleDialog();

  //! Function
  const { addToast } = useToasts();

  //! Function
  const handleDelete = async () => {
    try {
      await notificationService.deleteNotification(element.id);
      addToast('Xóa thông báo thành công!', { appearance: 'success' });
      await refetchList();
    } catch (error) {}
  };
  const handleMuteNoti = async () => {
    console.log(element);
    try {
      const resMute = await notificationService.muteNotification({
        body: { okr: element.okr_id },
      });
      console.log('resMute', resMute);
      addToast('Tắt thông báo thành công!', { appearance: 'success' });
      await refetchList();
    } catch (error) {}
  };

  const typeNotification = async () => {
    closeNotifiList();
    if (!element?.is_read) {
      try {
        await notificationService.updateNotification({
          id: element?.id,
          body: { is_read: true },
        });
      } catch (error) {
        addToast(error, { appearance: 'error' });
      }
    }

    switch (element.type_notification) {
      case 0:
        history.push(RouteBase.ChallengeOkrWithID(element.data_id));
        break;
      case 1:
        history.push(RouteBase.QuizOkrWithID(element.data_id));

        break;
      case 2:
        history.push(RouteBase.DetailOkrWithID(element.data_id));
        break;
      case 3:
        history.push(RouteBase.CallCheckinWithId(element.data_id));
        break;
      case 4:
        history.push(RouteBase.CheckinDetailWithID(element.data_id));
        break;
      default:
        break;
    }
  };

  const listMore = [
    {
      name: 'Tắt thông báo',
      icon: <FaBellSlash />,
      onClick: handleMuteNoti,
    },
    {
      name: 'Xoá',
      icon: <FaTrash />,
      onClick: handleDelete,
    },
  ];

  //! Render
  return (
    <div
      onClick={typeNotification}
      className="notification-item d-flex"
      style={{ backgroundColor: element?.is_read ? 'white' : '#E5E5E5' }}
    >
      <div className="notification-item__avt" style={{ paddingRight: '12px' }}>
        <img
          src={
            element?.user_sent_data?.img_url
              ? genImageUrl(element?.user_sent_data?.img_url)
              : 'https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png'
          }
          alt="Avt"
          height="48px"
          width="48px"
          style={{ borderRadius: '50%' }}
        />
      </div>
      <div className="notification-item__content" style={{ flex: '1' }}>
        <div>
          <span className="notification-item__name">{element?.user_sent_data?.full_name}</span>
          <span className="notification-item__text">{element?.content}</span>
        </div>
        <div className="mt-1">
          <span className="opacity-75">{moment(element?.created_time).fromNow()} </span>
        </div>
      </div>

      <div ref={refMore} className={`${openPopover ? 'active' : ''} notification-item__action`}>
        <span
          id={`notification-${element.id}`}
          onClick={(e) => {
            e.preventDefault();
            e.stopPropagation();
            togglePopover();
          }}
          className={`${openPopover ? 'active' : ''}`}
        >
          <RiMoreLine />
        </span>

        {shouldRenderPopover && (
          <PopoverCommon
            className="comment-popover-more"
            target={`notification-${element.id}`}
            toggle={togglePopover}
            isOpen={openPopover}
          >
            {listMore.map((el) => (
              <div key={el.name} onClick={el.onClick}>
                {el.icon} {el.name}
              </div>
            ))}
          </PopoverCommon>
        )}
      </div>
    </div>
  );
};

NotificationItem.propTypes = propTypes;
export default NotificationItem;
