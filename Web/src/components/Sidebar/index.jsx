import { RouteBase } from 'constants/routeUrl';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link, useLocation } from 'react-router-dom';
import { ReactComponent as ArchiveCheck } from '../../assets/svg/archive-check.svg';
import { ReactComponent as Comment } from '../../assets/svg/b-comment.svg';
import { ReactComponent as ChartBar } from '../../assets/svg/chart-bar-32.svg';
import { ReactComponent as Messenger } from '../../assets/svg/messenger.svg';
import { ReactComponent as Microsoft } from '../../assets/svg/microsoft.svg';
import { ReactComponent as Phone } from '../../assets/svg/phone.svg';
import { ReactComponent as Question } from '../../assets/svg/question.svg';
import { ReactComponent as SavedItem } from '../../assets/svg/saved-items.svg';
import { ReactComponent as Plus } from '../../assets/svg/plus.svg';
import { ReactComponent as Star } from '../../assets/svg/star.svg';
import { HiOutlineMenu } from 'react-icons/hi';
import { Button, ButtonDropdown, Col, DropdownItem, DropdownToggle, Row } from 'reactstrap';
import ButtonCommon from 'components/ButtonCommon';
import PopoverNotification from 'components/PopoverNotification';
import { BASE_URL_IMAGE } from 'constants/api';
import { GoChevronDown } from 'react-icons/go';
import DropdownMenu from 'react-overlays/esm/DropdownMenu';
import { useDispatch } from 'react-redux';
import authServices from 'services/authServices';
import { logout } from 'redux/modules/auth';
import { BsPlusCircle } from 'react-icons/bs';
import { BsFillStarFill } from 'react-icons/bs';
import { IoIosLogOut } from 'react-icons/io';
import { RiAccountCircleFill } from 'react-icons/ri';

const Sidebar = ({ isMobile = false, onClickMenu = () => {} }) => {
  //! State
  const dispatch = useDispatch();
  const history = useState();
  const { t } = useTranslation();
  const [isShowNavBar, setIsShowNavBar] = useState(false);
  const [dropdownOpen, setOpen] = useState(false);
  const [popoverOpen, setPopoverOpen] = useState(false);
  const userData = authServices.getUserLocalStorage();

  //! Function
  const toggle = () => setOpen(!dropdownOpen);

  const confirmLogout = () => dispatch(logout());

  const pushToCreate = () => history.push(RouteBase.CreateOkr);
  const pushToAccount = () => history.push(RouteBase.PersonalInfor);
  const pushToGiveStars = () => history.push(RouteBase.GiveStars);

  const togglePopover = () => setPopoverOpen(!popoverOpen);

  // const toggleNavBar = () => setIsShowNavBar(!isShowNavBar);
  const { pathname } = useLocation();

  const menu = [
    {
      icon: <Microsoft />,
      label: t('sidebar:dashboard'),
      to: RouteBase.Dashboard,
    },
    {
      icon: <ArchiveCheck />,
      label: t('sidebar:checkin'),
      to: RouteBase.CheckinOkr,
    },
    {
      icon: <SavedItem />,
      label: t('sidebar:okrs'),
      to: RouteBase.TreeOKR,
    },
    {
      icon: <Comment />,
      label: t('sidebar:cfrs'),
      to: RouteBase.CFRS,
    },
    {
      icon: <ChartBar />,
      label: t('sidebar:report'),
      to: RouteBase.ReportOKR,
    },
  ];

  const topMenu = [
    {
      icon: <BsFillStarFill size="22" />,
      label: t('common:giveStars'),
      to: RouteBase.GiveStars,
    },
    {
      icon: <BsPlusCircle size="22" />,
      label: t('common:add'),
      to: RouteBase.CreateOkr,
    },
  ];

  const signOut = [
    {
      icon: <IoIosLogOut size="27" />,
      label: t('common:logout'),
      to: 'signout',
    },
  ];

  const dataButton = [{ icon: <Phone /> }, { icon: <Messenger /> }, { icon: <Question /> }];

  const onClickLink = () => {
    if (isMobile) {
      //* mobile -> close when user navigate
      onClickMenu();
    }
  };

  //! Render
  const renderMenuItem = (menuItem) => {
    return (
      <Link
        onClick={(e) => {
          if (menuItem.to === 'signout') {
            e.preventDefault();
            confirmLogout();
          } else {
            onClickLink();
          }
        }}
        to={menuItem.to}
        className={`menu ${pathname === menuItem.to && 'menu--selected'}`}
        key={menuItem.label}
      >
        {menuItem.icon} <span className="menu-name">{menuItem.label}</span>
      </Link>
    );
  };

  const renderButton = (button, index) => {
    return (
      <Link to="" className="custom-button" key={index.toString()}>
        {button.icon}
      </Link>
    );
  };

  if (isMobile) {
    const mobileMenu = [...topMenu, ...menu, ...signOut];

    return (
      <div className="sidebar-mobile">
        <div className="sidebar-mobile__menu">
          <h4>Okrs Online</h4>
          <div className="d-flex align-items-center">
            <div className="bell-icon">
              <PopoverNotification
                color="#fff"
                isShowPopover={popoverOpen}
                togglePopover={togglePopover}
                title={'Thông báo'}
                subTitle={'Câu hỏi'}
              />
            </div>
            <HiOutlineMenu onClick={onClickMenu} size="26px" />
          </div>
        </div>
        <div className="align-items-center d-flex" style={{ flex: 1 }}>
          <Link
            onClick={onClickLink}
            to={RouteBase.PersonalInfor}
            style={{ flex: 1, color: 'transparent' }}
            className={`menu ${pathname === RouteBase.PersonalInfor && 'menu--selected'}`}
          >
            <div xs="12" md="12" lg="12" sm="12" onClick={pushToAccount}>
              <img
                src={`${BASE_URL_IMAGE}${userData?.user?.img_url}`}
                alt="avatar"
                className="rounded-circle"
                style={{ height: 45, width: 45 }}
              />
              <span style={{ paddingLeft: '20px', color: '#fff', border: 0 }}>{userData?.user?.full_name}</span>
            </div>
          </Link>
        </div>
        <div className="sidebar-mobile__container">{mobileMenu.map((elm) => renderMenuItem(elm))}</div>
      </div>
    );
  }

  return (
    <div className="sidebar">
      <div className="title-wrap d-flex align-items-center">
        <span className="title">Okrs Online</span>
      </div>
      <div className="separator" />
      <div>{menu.map((elm) => renderMenuItem(elm))}</div>
      <div className="btn-custom-container">{dataButton.map((elm, index) => renderButton(elm, index))}</div>
    </div>
  );
};

export default Sidebar;
