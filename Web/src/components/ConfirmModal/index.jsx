import ButtonCommon from 'components/ButtonCommon';
import React from 'react';
import { Modal, ModalBody, ModalFooter } from 'reactstrap';
import PropTypes from 'prop-types';
import { Form, Formik } from 'formik';

const propTypes = {
  toggle: PropTypes.func,
  isOpen: PropTypes.bool,
  handleDelete: PropTypes.func,
  content: PropTypes.string,
  labelBtnSubmit: PropTypes.string,
  labelBtnCancel: PropTypes.string,
};

const ConfirmModal = ({
  isOpen = false,
  toggle = () => {},
  onSubmit = () => {},
  content = '',
  labelBtnSubmit = '',
  labelBtnCancel = '',
}) => {
  return (
    <Formik initialValues={{}} onSubmit={onSubmit}>
      {(propsFormik) => (
        <Modal isOpen={isOpen} toggle={toggle}>
          <Form>
            <ModalBody>{content}</ModalBody>
            <ModalFooter>
              <ButtonCommon type="submit" isLoading={propsFormik.isSubmitting} label={labelBtnSubmit} />
              <ButtonCommon onClick={toggle} label={labelBtnCancel} link />
            </ModalFooter>
          </Form>
        </Modal>
      )}
    </Formik>
  );
};

ConfirmModal.propTypes = propTypes;
export default ConfirmModal;
