import DateFieldFormik from 'components/CustomField/DateFieldFormik';
import InputField from 'components/CustomField/InputField';
import SelectField from 'components/CustomField/SelectField';
import { Field } from 'formik';
import { isEmpty } from 'lodash-es';
import React, { useEffect, useState } from 'react';
import { MdClear } from 'react-icons/md';
import { Button, Card, CardBody, Col, Label, Row } from 'reactstrap';
import smart from 'views/CreateOkr/Data/smart';
import TooltipOkr from './TooltipOkr';

const AddResult = ({
  setFieldValue,
  index,
  relateResult = [],
  unitData = [],
  valueOKRParent = '',
  dataOKRParent = [],
  values = {},
  setValues = () => {},
  disableUnit = false,
}) => {
  const [suggest, setSuggest] = useState([]);

  useEffect(() => {
    const dataSuggest = dataOKRParent.find((elm) => elm.id.toString() === valueOKRParent.toString());
    if (dataSuggest && !isEmpty(dataSuggest)) {
      setSuggest(dataSuggest.okr_result);
    }
  }, [dataOKRParent, valueOKRParent]);

  const removeItem = () => {
    const newArr = [...values.okr_result];
    setFieldValue(
      'okr_result',
      newArr.filter((_, idx) => idx !== index),
    );
  };

  const clickSuggest = (itemSuggest) => {
    const { created_time, id, okr, ...remainData } = itemSuggest;
    const newKR = { ...remainData, user: 6 };
    values.okr_result[index] = newKR;
    setValues({ ...values });
  };

  return (
    <Card className="add_result-card mt-2">
      <div className="d-flex justify-content-end">
        <Button className={`btn-clear p-1 ${values.okr_result.length < 2 && 'd-none'}`} type="button">
          <MdClear size="16" onClick={removeItem} />
        </Button>
      </div>
      <CardBody>
        <div>
          {suggest.map((elm) => (
            <Button
              outline
              color="primary"
              className="rounded-pill me-2"
              key={elm.id}
              onClick={() => clickSuggest(elm)}
            >
              {elm.key_result}
            </Button>
          ))}
        </div>
        <Row className="row">
          <Col className="col-8">
            <Label className="w-100">
              <TooltipOkr
                id={'key_result'}
                target={'key_result'}
                // subTitle={'(A) Kết quả chính '}
                subTitle={
                  <>
                    (A) Kết quả chính
                    <span style={{ color: 'red' }}> *</span>
                  </>
                }
                textTooltip={smart.A}
              />
            </Label>
            <Field
              name={`okr_result[${index}].key_result`}
              className="form-control"
              placeholder="Nhập kết quả chính"
              component={InputField}
            />
          </Col>
          <Col className="col-4 justify-content-center">
            <Label className="w-100">
              <TooltipOkr
                id={'targetOkr'}
                target={'targetOkr'}
                //  subTitle={'(M) Mục tiêu'}
                subTitle={
                  <>
                    (M) Mục tiêu
                    <span style={{ color: 'red' }}> *</span>
                  </>
                }
                textTooltip={smart.M}
              />
            </Label>
            <Field
              name={`okr_result[${index}].target`}
              className="form-control"
              placeholder="Nhập mục tiêu"
              component={InputField}
            />
          </Col>
        </Row>
        <Row className="row">
          <Col className="col-4">
            <Label className="w-100">
              Chọn đơn vị <span style={{ color: 'red' }}>*</span>
            </Label>
            <Field
              as="select"
              name={`okr_result[${index}].unit`}
              className="form-control"
              label="unit_name"
              component={SelectField}
              disabled={values.result_parent}
              // disabled={disableUnit}
              data={unitData}
              defaultOption="Chọn đơn vị"
            />
          </Col>
          <Col className="col-4">
            <Label className="w-100">Link kế hoạch</Label>
            <Field
              name={`okr_result[${index}].plan_url`}
              className="form-control"
              placeholder="Sử dụng Google Sheet nếu có"
              component={InputField}
            />
          </Col>
          <Col className="col-4">
            <Label className="w-100">Link kết quả</Label>
            <Field
              name={`okr_result[${index}].result_url`}
              className="form-control"
              placeholder="Sử dụng Google Sheet nếu có"
              component={InputField}
            />
          </Col>
        </Row>

        <Row>
          <Col>
            <Label className="w-100">
              <TooltipOkr
                id={'deadline'}
                target={'deadline'}
                subTitle={
                  <>
                    (T) Thời gian hoàn thành
                    <span style={{ color: 'red' }}> *</span>
                  </>
                }
                textTooltip={smart.T}
              />
            </Label>
            <Field
              component={DateFieldFormik}
              minDate={new Date()}
              name={`okr_result[${index}].deadline`}
              className="w-100 form-control"
            />
          </Col>
        </Row>
      </CardBody>
    </Card>
  );
};
export default AddResult;
