import React, { useState } from 'react';
import { Tooltip, UncontrolledTooltip } from 'reactstrap';
import { BsFillInfoCircleFill } from 'react-icons/bs';

const TooltipOkr = ({ toggle, textTooltip, subTitle, id, target }) => {
  return (
    <div>
      {subTitle}
      <span style={{ textDecoration: 'underline', color: 'blue', marginLeft: 5 }} href="#" id={id}>
        <BsFillInfoCircleFill className="tooltip-icon" />{' '}
      </span>
      <UncontrolledTooltip placement="right" target={target}>
        {textTooltip}
      </UncontrolledTooltip>
    </div>
  );
};

export default TooltipOkr;
