import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import RadioField from 'components/CustomField/RadioField';
import { Field } from 'formik';
import { Col, Row } from 'reactstrap';
import genImageUrl from 'helpers/genImageUrl';
import ErrorMessages from 'components/CustomField/ErrorMessage';

const propTypes = {
  field: PropTypes.any,
  titleQuestion: PropTypes.string,
  classNameImg: PropTypes.Image,
  item: PropTypes.object,
  onChange: PropTypes.func,
};

const QuizItem = (props) => {
  const { item, index } = props;
  //! State
  const dispatch = useDispatch();

  //! Function

  //! Render
  return (
    <div className="quiz-container">
      <div className="align-items-center d-flex quiz-container-header">
        <div className="quiz-container-header-question">Câu hỏi số {index + 1}</div>
        <div className="quiz-container-header-button">
          <button className="button">Single Choice</button>
        </div>
      </div>

      <div className="quiz-container-content">
        <div className="quiz-container-content-text">{item?.content}</div>
        {(item?.attachment_question || []).map((img) => {
          return (
            <>
              <img src={genImageUrl(img?.file_url)} alt="img" width="490px" height="276px"></img>
            </>
          );
        })}
      </div>

      <div className="d-flex quiz-container-footer">
        <Row>
          {(item?.answers || []).map((element) => {
            return (
              <Col xs={6} md={6} key={element.id} className="align-items-baseline d-flex mb-4">
                <Field
                  // className="radio-field"
                  component={RadioField}
                  value={element.id}
                  name={`question_quiz[${index}].answer`}
                  isValidate
                />
                <div>{element.content}</div>
              </Col>
            );
          })}
        </Row>
      </div>

      <div>
        {' '}
        <ErrorMessages name={`question_quiz[${index}].answer`} />
      </div>
    </div>
  );
};

QuizItem.propTypes = propTypes;
export default QuizItem;
