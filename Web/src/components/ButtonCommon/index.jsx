import React from 'react';
import { Button } from 'reactstrap';
import PropTypes from 'prop-types';
import Loading from 'components/Loading';

const propTypes = {
  type: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
  label: PropTypes.string,
  icon: PropTypes.element,
  onClick: PropTypes.func,
  outline: PropTypes.bool,
  isLoading: PropTypes.bool,
  disabled: PropTypes.bool,
};

const ButtonCommon = ({
  type = 'button',
  color = 'secondary',
  className = '',
  style = {},
  label = '',
  icon = '',
  onClick = () => {},
  outline = false,
  link = false,
  isLoading = false,
  disabled = false,
  spanClassName = '',
}) => {
  //! State

  //! Function

  //! Render
  return (
    <Button
      style={style}
      className={`${className || ''} ${link ? 'is-link' : ''}`}
      type={type}
      color={color}
      onClick={onClick}
      disabled={disabled}
      outline={outline}
    >
      {icon && <div className="icon d-flex">{icon}</div>}
      <span className={`sub-title ${spanClassName}`}>{isLoading ? <Loading size="sm" /> : label}</span>
    </Button>
  );
};

ButtonCommon.propTypes = propTypes;
export default ButtonCommon;
