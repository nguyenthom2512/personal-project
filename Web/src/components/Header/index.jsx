import ButtonCommon from 'components/ButtonCommon';
import PopoverNotification from 'components/PopoverNotification';
import { BASE_URL_IMAGE } from 'constants/api';
import { RouteBase } from 'constants/routeUrl';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GoChevronDown } from 'react-icons/go';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { Button, ButtonDropdown, DropdownItem, DropdownMenu, DropdownToggle, Navbar } from 'reactstrap';
import { logout } from 'redux/modules/auth';
import authServices from 'services/authServices';
import { ReactComponent as Plus } from '../../assets/svg/plus.svg';
import { ReactComponent as Star } from '../../assets/svg/star.svg';
import { HiOutlineMenu } from 'react-icons/hi';
import NavBarCommon from 'components/NavBar';
import Sidebar from 'components/Sidebar';
import { HiOutlinePlusCircle } from 'react-icons/hi';
import { MdArrowDropDownCircle } from 'react-icons/md';

const Header = (props) => {
  //! State
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const history = useHistory();
  const [isShowNavBar, setIsShowNavBar] = useState(false);
  const [dropdownOpen, setOpen] = useState(false);
  const [popoverOpen, setPopoverOpen] = useState(false);
  const userData = authServices.getUserLocalStorage();

  //! Function
  const toggle = () => setOpen(!dropdownOpen);

  const confirmLogout = () => dispatch(logout());

  const pushToCreate = () => history.push(RouteBase.CreateOkr);
  const pushToAccount = () => history.push(RouteBase.PersonalInfor);
  const pushToGiveStars = () => history.push(RouteBase.GiveStars);

  const togglePopover = () => setPopoverOpen(!popoverOpen);

  const toggleNavBar = () => setIsShowNavBar(!isShowNavBar);

  //! Render
  return (
    <div className="fixed-header">
      <Navbar className="header" expand="md">
        <div className="align-items-center nav-menu d-flex">
          {/* REPONSIVE */}
          <div className="is-mobile">
            <HiOutlineMenu onClick={toggleNavBar} size="26px" />

            {isShowNavBar && <Sidebar isMobile onClickMenu={toggleNavBar} />}
          </div>

          <div className="is-desktop">
            <div>
              <Button className="button-add" onClick={pushToCreate}>
                <div className="align-items-center d-flex">
                  <Plus />
                  <span className="title-add">{t('common:add')}</span>
                </div>
              </Button>
            </div>
            <div>
              <ButtonCommon
                onClick={pushToGiveStars}
                label={t('common:giveStars')}
                outline
                icon={<Star height="16px" width="16px" />}
              />
            </div>
            <div className="bell-icon">
              <PopoverNotification
                isShowPopover={popoverOpen}
                togglePopover={togglePopover}
                title={'Thông báo'}
                subTitle={'Câu hỏi'}
              />
            </div>
            <div>
              <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle className="dropdown-custom">
                  <img
                    src={`${BASE_URL_IMAGE}${userData?.user?.img_url}`}
                    alt="avatar"
                    className="rounded-circle"
                    style={{ height: 30, width: 30 }}
                  />
                  <span>{userData?.user?.full_name}</span>
                  <GoChevronDown size={20} color="#21212" />
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem divider />
                  <DropdownItem className="dropdown-custom__dropdown-item" onClick={pushToAccount}>
                    {t('common:account')}
                  </DropdownItem>
                  <DropdownItem className="dropdown-custom__dropdown-item" onClick={confirmLogout}>
                    {t('common:logout')}
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </div>
        </div>
      </Navbar>
      <div className="separator" />
    </div>
  );
};

export default Header;
