import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Button, ButtonDropdown, DropdownItem, DropdownToggle } from 'reactstrap';
import ButtonCommon from 'components/ButtonCommon';
import PopoverNotification from 'components/PopoverNotification';
import DropdownMenu from 'react-overlays/esm/DropdownMenu';
import { ReactComponent as Plus } from '../../assets/svg/plus.svg';
import { ReactComponent as Star } from '../../assets/svg/star.svg';
import { useTranslation } from 'react-i18next';
import { BASE_URL_IMAGE } from 'constants/api';
import { GoChevronDown } from 'react-icons/go';

const propTypes = {};

const NavBar = (props) => {
  const {
    pushToCreate,
    pushToGiveStars,
    popoverOpen,
    togglePopover,
    dropdownOpen,
    toggle,
    pushToAccount,
    confirmLogout,

    userData,
  } = props;
  //! State
  const dispatch = useDispatch();
  const { t } = useTranslation();
  //! Function

  //! Render
  return (
    <div>
      <div>
        <Button className="button-add" onClick={pushToCreate}>
          <div className="align-items-center d-flex">
            <Plus />
            <span className="title-add">{t('common:add')}</span>
          </div>
        </Button>
      </div>
      <div>
        <ButtonCommon
          onClick={pushToGiveStars}
          label={t('common:giveStars')}
          outline
          icon={<Star height="16px" width="16px" />}
        />
      </div>
      <div className="bell-icon">
        <PopoverNotification
          isShowPopover={popoverOpen}
          togglePopover={togglePopover}
          title={'Thông báo'}
          subTitle={'Câu hỏi'}
        />
      </div>
      <div>
        <ButtonDropdown isOpen={dropdownOpen} toggle={toggle}>
          <DropdownToggle className="dropdown-custom">
            <img
              src={`${BASE_URL_IMAGE}${userData?.user?.img_url}`}
              alt="avatar"
              className="rounded-circle"
              style={{ height: 30, width: 30 }}
            />
            <span>{userData?.user?.full_name}</span>
            <GoChevronDown size={20} color="#21212" />
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem divider />
            <DropdownItem className="dropdown-custom__dropdown-item" onClick={pushToAccount}>
              {t('common:account')}
            </DropdownItem>
            <DropdownItem className="dropdown-custom__dropdown-item" onClick={confirmLogout}>
              {t('common:logout')}
            </DropdownItem>
          </DropdownMenu>
        </ButtonDropdown>
      </div>
    </div>
  );
};

NavBar.propTypes = propTypes;
export default NavBar;
