import Header from 'components/Header';
import Sidebar from 'components/Sidebar';
import React from 'react';

const DefaultLayout = (props) => {
  const { children } = props;

  return (
    <div className="custom-body">
      <div>
        <Sidebar />

        <div className="body-custom">
          <Header />
          <main className="main-container">{children}</main>
          {/* <Footer /> */}
        </div>
      </div>
    </div>
  );
};

export default DefaultLayout;
