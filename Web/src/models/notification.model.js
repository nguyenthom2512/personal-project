import { isArray } from 'lodash';

class NotificationModel {
  constructor({
    content = '',
    created_time = '',
    data_id = '',
    id = '',
    is_read = '',
    title = '',
    user = '',
    user_sent = '',
    user_sent_data = { full_name: '', img_url: null },
    type_notification = 0,
    okr_id = '',
  }) {
    this.content = content;
    this.created_time = created_time;
    this.data_id = data_id;
    this.id = id;
    this.is_read = is_read;
    this.title = title;
    this.user = user;
    this.user_sent = user_sent;
    this.user_sent_data = user_sent_data;
    this.type_notification = type_notification;
    this.okr_id = okr_id;
  }

  static parseDataFromResponseList(response) {
    if (isArray(response?.data?.results)) {
      return response?.data?.results?.map((item) => {
        const newItem = new NotificationModel(item);
        return newItem;
      });
    }

    return [];
  }
}

export default NotificationModel;
