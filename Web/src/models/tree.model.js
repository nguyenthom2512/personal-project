import { isObject } from 'lodash-es';

class TreeModel {
  constructor() {
    this.count = 0;
    this.next = null;
    this.previous = null;
    this.results = [];
  }
  static initialFilters(values = {}) {
    return {
      department: values?.department || '',
      created_time__range: values?.created_time__range || '',
      search: '',
    };
  }

  static parseDataFromResponse(response) {
    const nextData = new TreeModel();
    if (isObject(response?.data)) {
      nextData.count = response?.data?.count;
      nextData.next = response?.data?.next;
      nextData.previous = response?.data?.previous;
      nextData.results = response?.data?.results;

      return nextData;
    }

    return nextData;
  }
}

export default TreeModel;
