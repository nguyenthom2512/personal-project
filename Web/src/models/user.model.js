import { convertToDateCommon, momentInstance } from 'helpers';

class UserModel {
  constructor({
    id = '',
    date_of_birth = '',
    department = '',
    email = '',
    fb_link = '',
    full_name = '',
    img_url = '',
    phone_number = '',
    gender = '',
    company = '',
  } = {}) {
    this.id = id;
    this.date_of_birth = date_of_birth;
    this.department = department;
    this.email = email;
    this.fb_link = fb_link;
    this.full_name = full_name;
    this.img_url = img_url;
    this.phone_number = phone_number;
    this.gender = gender;
    this.company = company;
  }

  static parseRequestToEdit(values = {}) {
    const nextUser = new UserModel({
      id: values.id,
      date_of_birth: values.date_of_birth || '',
      department: values.department || '',
      email: values.email || '',
      fb_link: values.fb_link || '',
      full_name: values?.full_name || '',
      phone_number: values?.phone_number || '',
      img_url: values?.img_url || '',
      gender: values?.gender || '',
      company: values?.company || '',
    });

    // Custom
    nextUser.date_of_birth = convertToDateCommon(values.date_of_birth, 'YYYY-MM-DD');

    return nextUser;
  }
}

export default UserModel;
