import moment from 'moment';
import { useTranslation } from 'react-i18next';

const DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm:ss';
const DATE_FORMAT = 'DD/MM/YYYY';
const FORMAT_QUARTER = 'YYYY-MM-DD';
const DATE_TIME_ALL = '[Thời gian: ]HH[ giờ ]mm[, ngày ]DD[, tháng ]MM[, năm ]YYYY';
const DATE_TIME_SERVER = 'YYYY-MM-DDTHH:mm:ss';
const quarter = (currentQuarter) => {
  const current = moment().quarter(currentQuarter);
  return `${current.startOf('quarter').format(FORMAT_QUARTER)},${current.endOf('quarter').format(FORMAT_QUARTER)}`;
};
const QUARTER = () => {
  const { t } = useTranslation();
  return [
    {
      label: t('common:quarter1'),
      value: quarter(1),
      id: quarter(1),
    },
    {
      label: t('common:quarter2'),
      value: quarter(2),
      id: quarter(2),
    },
    {
      label: t('common:quarter3'),
      value: quarter(3),
      id: quarter(3),
    },
    {
      label: t('common:quarter4'),
      value: quarter(4),
      id: quarter(4),
    },
  ];
};

export { DATE_TIME_FORMAT, DATE_FORMAT, DATE_TIME_ALL, QUARTER, FORMAT_QUARTER, DATE_TIME_SERVER };
