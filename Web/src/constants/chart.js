export const backgroundColor = [
  'rgba(255, 99, 132, 0.2)',
  'rgba(54, 162, 235, 0.2)',
  'rgba(255, 206, 86, 0.2)',
  'rgba(153, 102, 255, 0.2)',
];
export const borderColor = [
  'rgba(255, 99, 132, 0.2)',
  'rgba(54, 162, 235, 0.2)',
  'rgba(255, 206, 86, 0.2)',
  'rgba(153, 102, 255, 0.2)',
];
export const labelChartItem = ['OKRs tiến độ 0%', 'OKRs tiến độ 1-40%', 'OKRs tiến độ 41-70%', 'OKRs tiến độ trên 70%'];
export const labelChartBar = [
  'OKRs tiến độ 0%',
  'OKRs tiến độ 1-40%',
  'OKRs tiến độ 41-70%',
  'OKRs tiến độ trên 70%',
  '',
  'OKRs tiến độ 0%',
  'OKRs tiến độ 1-40%',
  'OKRs tiến độ 41-70%',
  'OKRs tiến độ trên 70%',
];
export const backgroundColorChartBar = [
  'rgba(255, 99, 132, 0.2)',
  'rgba(54, 162, 235, 0.2)',
  'rgba(255, 206, 86, 0.2)',
  'rgba(153, 102, 255, 0.2)',
  'transparent',
  'rgba(255, 99, 132, 0.2)',
  'rgba(54, 162, 235, 0.2)',
  'rgba(255, 206, 86, 0.2)',
  'rgba(153, 102, 255, 0.2)',
];
export const borderColorChartBar = [
  'rgba(255, 99, 132, 1)',
  'rgba(54, 162, 235, 1)',
  'rgba(255, 206, 86, 1)',
  'rgba(153, 102, 255, 1)',
  'transparent',
  'rgba(255, 99, 132, 1)',
  'rgba(54, 162, 235, 1)',
  'rgba(255, 206, 86, 1)',
  'rgba(153, 102, 255, 1)',
];
