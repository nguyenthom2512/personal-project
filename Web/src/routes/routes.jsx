import { lazy } from 'react';
import withErrorBoundary from 'components/HOCs/withErrorBoundary';
import { RouteBase } from 'constants/routeUrl';

const HomePage = lazy(() => import('views/Home'));
const CreateOkr = lazy(() => import('views/CreateOkr'));
const Dashboard = lazy(() => import('views/Dashboard'));
const ListOkrsPage = lazy(() => import('views/ListOkrs'));
const TreeOkrs = lazy(() => import('views/TreeOkrs'));
const CheckinOkrPage = lazy(() => import('views/CheckinOkr'));
const CallCheckinPage = lazy(() => import('views/CallCheckin'));

const Page404 = lazy(() => import('views/Page404'));
const DetailListOkrPage = lazy(() => import('views/DetailListOkr'));
const CheckinDetailPage = lazy(() => import('views/CheckInDetail'));
const ReportPage = lazy(() => import('views/Report/index'));
const QuizOkrPage = lazy(() => import('views/QuizOkr'));
const CFRSPage = lazy(() => import('views/CFRS'));
const CreateFeedback = lazy(() => import('views/CreateFeedback'));
const PersonalInforPage = lazy(() => import('views/PersonalInfor'));
const ResetPasswordPage = lazy(() => import('views/ResetPassword'));
const ChallengePage = lazy(() => import('views/Challenge'));
const GiveStarsPage = lazy(() => import('views/GiveStars'));
const PricingPage = lazy(() => import('views/Pricing'));
const ListQuizPage = lazy(() => import('views/ListQuiz'));

const Todos = lazy(() => import('views/Todos'));

//* For secured route
const routes = [
  { path: RouteBase.Dashboard, name: 'Dashboard', component: withErrorBoundary(Dashboard) },
  { path: RouteBase.CreateOkr, exact: true, name: 'CreateOkr', component: withErrorBoundary(CreateOkr) },
  { path: RouteBase.ListOKR, exact: true, name: 'ListOkrs', component: withErrorBoundary(ListOkrsPage) },
  { path: RouteBase.TreeOKR, exact: true, name: 'TreeOkrs', component: withErrorBoundary(TreeOkrs) },
  { path: RouteBase.CheckinOkr, exact: true, name: 'CheckinOkr', component: withErrorBoundary(CheckinOkrPage) },
  {
    path: RouteBase.CallCheckinOkrWithId,
    exact: true,
    name: 'CallCheckinOKR',
    component: withErrorBoundary(CallCheckinPage),
  },
  {
    path: RouteBase.DetailListOkrPage,
    exact: true,
    name: 'DetailListOkrPage',
    component: withErrorBoundary(DetailListOkrPage),
  },
  { path: '/todos', exact: true, name: 'Todos', component: withErrorBoundary(Todos) },
  { path: RouteBase.ListOKR, exact: true, name: 'ListOkrsPage', component: withErrorBoundary(ListOkrsPage) },
  {
    path: RouteBase.CheckinDetail,
    exact: true,
    name: 'CheckinDetailPage',
    component: withErrorBoundary(CheckinDetailPage),
  },
  {
    path: RouteBase.ReportOKR,
    exact: true,
    name: 'ReportPage',
    component: withErrorBoundary(ReportPage),
  },
  {
    path: RouteBase.QuizOkr,
    exact: true,
    name: 'QuizOkrPage',
    component: withErrorBoundary(QuizOkrPage),
  },
  {
    path: RouteBase.CFRS,
    exact: true,
    name: 'CFRSPage',
    component: withErrorBoundary(CFRSPage),
  },
  {
    path: RouteBase.CreateFeedback,
    exact: true,
    name: 'CreateFeedback',
    component: withErrorBoundary(CreateFeedback),
  },
  {
    path: RouteBase.PersonalInfor,
    exact: true,
    name: 'PersonalInforPage',
    component: withErrorBoundary(PersonalInforPage),
  },
  {
    path: RouteBase.Challenge,
    exact: true,
    name: 'ChallengePage',
    component: withErrorBoundary(ChallengePage),
  },
  {
    path: RouteBase.GiveStars,
    exact: true,
    name: 'GiveStarsPage',
    component: withErrorBoundary(GiveStarsPage),
  },
  {
    path: RouteBase.Pricing,
    exact: true,
    name: 'PricingPage',
    component: withErrorBoundary(PricingPage),
  },
  {
    path: RouteBase.ListQuiz,
    exact: true,
    name: 'ListQuizPage',
    component: withErrorBoundary(ListQuizPage),
  },
  { path: RouteBase.Home, exact: true, name: 'Home', component: withErrorBoundary(TreeOkrs) },
  { name: '404', component: withErrorBoundary(Page404) },
];

export default routes;
