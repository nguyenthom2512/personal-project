import { RESULT } from 'constants/api';
import httpService from './httpServices';

class UpdateOkrService {
  patchUpdateOkr(body) {
    return httpService.patch(RESULT + body.id, body);
  }
}
export default new UpdateOkrService();
