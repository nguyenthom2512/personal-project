import { DATA_FOR_CREATE_OKR } from 'constants/api';
import httpService from './httpServices';

class dataForCreateService {
  getDataForCreateOkr() {
    return httpService.get(DATA_FOR_CREATE_OKR);
  }
}

export default new dataForCreateService();
