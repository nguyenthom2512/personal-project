import { LOGIN_OKR } from 'constants/api';
import { isEmpty } from 'lodash';
import httpServices from './httpServices';

class AuthServices {
  saveUserLocalStorage(data = {}) {
    localStorage.setItem('user', JSON.stringify(data));
  }

  getUserLocalStorage() {
    const dataUser = localStorage.getItem('user');
    if (!!dataUser) {
      return JSON.parse(dataUser);
    }
    return {};
  }

  clearUserLocalStorage() {
    localStorage.removeItem('user');
  }

  postLoginOkr(body) {
    return httpServices.post(LOGIN_OKR, body);
  }

  updateNewLocalStorage(body) {
    if (!isEmpty(body)) {
      const userData = this.getUserLocalStorage();
      const nextData = { ...userData };
      nextData.user = body;

      localStorage.setItem('user', JSON.stringify(nextData));
    }
  }
}

export default new AuthServices();
