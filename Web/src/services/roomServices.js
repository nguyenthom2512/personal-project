import { ROOM, ROOM_WITH_ID } from 'constants/api';
import httpService from './httpServices';

class roomServices {
  getRoom(id) {
    return httpService.get(ROOM_WITH_ID(id), {});
  }
  createRoom(body) {
    return httpService.post(ROOM, body);
  }
}

export default new roomServices();
