import { FEEDBACK, FEEDBACK_STATIC } from 'constants/api';
import { convertParamsToQuery } from 'helpers';
import httpService from './httpServices';

class feedbackService {
  getFeedbackReceived(param) {
    return httpService.get(`${FEEDBACK}${convertParamsToQuery(param)}`);
  }

  getFeedbackStatic() {
    return httpService.get(FEEDBACK_STATIC);
  }

  createFeedBack(body) {
    return httpService.post(FEEDBACK, body);
  }
}

export default new feedbackService();
