import { INSPIRATION_WITH_ID, PAYMENT } from 'constants/api';
import httpServices from './httpServices';

class inspirationServices {
  getInspiration(id) {
    return httpServices.get(INSPIRATION_WITH_ID(id));
  }
}
export default new inspirationServices();
