import {
  OKR_CHALLENGE_WITH_ID,
  POST_OKR_CHALLENGE,
  POST_OKR_INSPRIRATION,
  POST_USER_QUIZ,
  QUIZ_DETAIL_OKR,
  QUIZ_OKR,
  SUBMIT_QUIZ_DETAIL,
} from 'constants/api';
import httpService from './httpServices';

class dataQuizOkrService {
  getDataForQuiz() {
    return httpService.get(QUIZ_OKR);
  }
  getDataDetailForQuizOkr(id) {
    return httpService.get(QUIZ_DETAIL_OKR(id));
  }
  postDataForQuiz(body) {
    return httpService.post(POST_USER_QUIZ, body);
  }
  putDataDetailForQuizOkr({ id = '', body = {} }) {
    return httpService.put(SUBMIT_QUIZ_DETAIL(id), body);
  }
  postOkrChallenge(body) {
    return httpService.post(POST_OKR_CHALLENGE, body);
  }
  postOkrInspriration(body) {
    return httpService.post(POST_OKR_INSPRIRATION, body);
  }
  getOkrChallenge(id) {
    return httpService.get(OKR_CHALLENGE_WITH_ID(id));
  }
}

export default new dataQuizOkrService();
