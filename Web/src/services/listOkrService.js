import { LIST_OKR, OKR } from 'constants/api';
import httpService from './httpServices';

class listOkrService {
  getDataListOkr(param) {
    return httpService.get(LIST_OKR, { params: param });
  }
  getListOkr(param) {
    return httpService.get(OKR, { params: param });
  }
  getOkrDetail(id) {
    return httpService.get(OKR + id);
  }
}

export default new listOkrService();
