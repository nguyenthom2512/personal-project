// import { NOTIFICATION } from 'constants/api';
import { MUTE_NOTIFICATION, NOTIFICATION, NOTIFICATION_WITH_ID } from 'constants/api';
import httpService from './httpServices';

class notificationService {
  getNotification(params = {}) {
    return httpService.get(NOTIFICATION, { params });
  }
  deleteNotification(id) {
    return httpService.delete(NOTIFICATION_WITH_ID(id));
  }
  updateNotification({ id, body }) {
    return httpService.patch(NOTIFICATION_WITH_ID(id), body);
  }
  muteNotification({ body }) {
    return httpService.patch(MUTE_NOTIFICATION, body);
  }
}

export default new notificationService();
