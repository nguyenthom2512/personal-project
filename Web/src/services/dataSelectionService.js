import { DEPARTMENT, UNIT, OKR } from 'constants/api';
import httpService from './httpServices';

class dataSelectionServices {
  getDepartment() {
    return httpService.get(DEPARTMENT);
  }
  getUnit() {
    return httpService.get(UNIT);
  }
}

export default new dataSelectionServices();
