import { OKR } from 'constants/api';
import httpService from './httpServices';

class CreateOkrService {
  postCreateOkr(body) {
    return httpService.post(OKR, body);
  }
}
export default new CreateOkrService();
