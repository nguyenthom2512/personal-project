import { RESULT } from 'constants/api';
import httpService from './httpServices';

class deleteOkrServices {
  deleteResult(id) {
    return httpService.delete(RESULT + id);
  }
}
export default new deleteOkrServices();
