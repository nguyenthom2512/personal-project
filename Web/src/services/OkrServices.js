import { RESULT, OKR, FEEDBACK, CRITERIA, RELATE_OKR, OKR_TREE } from 'constants/api';
import httpService from './httpServices';

class OkrServices {
  getOkrList(params = {}) {
    return httpService.get(OKR, {
      params,
    });
  }

  getOkrDetail(id) {
    return httpService.get(`${OKR}${id}`);
  }

  getOkrResultDetail(id = '') {
    return httpService.get(`${RESULT}${id}`);
  }

  deleteOkrResult(id = '') {
    return httpService.delete(RESULT + id);
  }

  sendFeedback(body) {
    return httpService.post(FEEDBACK, body);
  }

  getCriteriaFeedBack(params) {
    return httpService.get(CRITERIA, params);
  }

  getOkeRelateList(id) {
    return httpService.get(RELATE_OKR(id));
  }
  getOkrTree(id) {
    return httpService.get(OKR_TREE(id));
  }
}
export default new OkrServices();
