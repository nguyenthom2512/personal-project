import { PACKAGE } from 'constants/api';
import httpService from './httpServices';

class packageService {
  getPackage() {
    return httpService.get(PACKAGE);
  }
}

export default new packageService();
