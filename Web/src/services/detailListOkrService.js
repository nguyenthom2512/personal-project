import { DETAILS_OKR } from 'constants/api';
import httpService from './httpServices';

class detailListOkrServices {
  getDetailListOkr(id) {
    return httpService.get(DETAILS_OKR(id));
  }
}

export default new detailListOkrServices();
