import { PAYMENT } from 'constants/api';
import httpServices from './httpServices';

class paymentService {
  paymentPost(body) {
    return httpServices.post(PAYMENT, body);
  }
}
export default new paymentService();
