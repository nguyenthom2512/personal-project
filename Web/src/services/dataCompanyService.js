import { COMPANY } from 'constants/api';
import httpService from './httpServices';

class DataCompanyService {
  getDataCompany() {
    return httpService.get(COMPANY);
  }
}
export default new DataCompanyService();
