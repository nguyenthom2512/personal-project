import { DELETE_OKR } from 'constants/api';
import httpService from './httpServices';

class deleteOkrService {
  deleteOkr(id) {
    return httpService.delete(DELETE_OKR(id));
  }
}

export default new deleteOkrService();
