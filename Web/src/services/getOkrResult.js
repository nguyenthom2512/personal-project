import { OKR } from 'constants/api';
import httpService from './httpServices';

class GetOkrService {
  getOkrList(params = {}) {
    return httpService.get(OKR, {
      params,
    });
  }

  getOkrResult(body) {
    return httpService.get(OKR + body.id, body);
  }
}
export default new GetOkrService();
