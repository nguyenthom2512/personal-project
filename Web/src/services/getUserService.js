import { OKR_USER, USER_OKR } from 'constants/api';
import httpService from './httpServices';

class dataUserokr {
  getDataUser(param) {
    return httpService.get(USER_OKR, { params: param });
  }
  getDataOkrUser(param) {
    return httpService.get(OKR_USER, { params: param });
  }
}

export default new dataUserokr();
