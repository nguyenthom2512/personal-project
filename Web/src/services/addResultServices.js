import { RESULT } from 'constants/api';
import httpService from './httpServices';

class addResultKr {
  addResult(body) {
    return httpService.post(RESULT, body);
  }
}

export default new addResultKr();
