import axios from 'axios';
import { BASE_URL, LOGIN_OKR } from 'constants/api';
import { RouteBase } from 'constants/routeUrl';
import { logout } from 'redux/modules/auth';

class Services {
  constructor() {
    this.axios = axios.create({
      baseURL: BASE_URL,
      timeout: 60000,
      // headers: {
      //   Authorization: 'Token 4dc282ee56b8b0f7b504387039de1316e3c26109',
      // },
    });
    this.interceptors = null;
    this.axios.defaults.withCredentials = true;
  }

  attachTokenToHeader(token) {
    this.interceptors = this.axios.interceptors.request.use(
      function (config) {
        // Do something before request is sent
        config.headers.Authorization = `Token ${token}`;
        return config;
      },
      function (error) {
        return Promise.reject(error);
      },
    );
  }

  addToastToInterceptors(toast) {
    const { addToast } = toast || {};
    this.interceptors = this.axios.interceptors.response.use(
      (response) => {
        if (response.config.parse) {
          //perform the manipulation here and change the response object
        }
        return response;
      },
      (error) => {
        if (error?.response?.status === 403) {
          window.location.replace(RouteBase.Pricing);
        } else if (error?.response?.status === 401) {
          addToast('Sai tài khoản hoặc mật khẩu');
        }
        // if (error.response?.config?.url !== LOGIN_OKR) {
        //   addToast(error.toString(), {
        //     appearance: 'error',
        //     autoDismiss: true,
        //   });
        // }
        return Promise.reject(error);
      },
    );
  }

  addReduxInterceptors(store) {
    const { dispatch } = store;
    this.interceptors = this.axios.interceptors.response.use(
      (response) => {
        if (response.config.parse) {
          //perform the manipulation here and change the response object
        }
        return response;
      },
      (error) => {
        if (error.response.status === 401 && error.response?.config?.url !== LOGIN_OKR) {
          dispatch(logout());
          window.location.reload();
        }
        return Promise.reject(error);
      },
    );
  }

  removeInterceptors() {
    this.axios.interceptors.request.eject(this.interceptors);
  }

  source() {
    return this.axios.CancelToken.source();
  }

  isCancel(error) {
    return this.axios.isCancel(error);
  }

  get(...arg) {
    return this.axios.get(...arg);
  }

  post(...arg) {
    return this.axios.post(...arg);
  }

  delete(...arg) {
    return this.axios.delete(...arg);
  }

  put(...arg) {
    return this.axios.put(...arg);
  }

  patch(...arg) {
    return this.axios.patch(...arg);
  }
}

export default new Services();
