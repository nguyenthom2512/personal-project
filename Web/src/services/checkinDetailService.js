import { CHECKIN_DETAIL } from 'constants/api';
import httpService from './httpServices';

class dataCheckinDetailService {
  postCheckinDetail(body) {
    return httpService.post(CHECKIN_DETAIL, body);
  }
  getCheckinDetail(id) {
    return httpService.get(`${CHECKIN_DETAIL + id}`);
  }
}
export default new dataCheckinDetailService();
