import { UPLOAD_IMG, USER_OKR } from 'constants/api';
import httpServices from './httpServices';

class UserService {
  uploadAvatar(file) {
    const form = new FormData();
    form.append('file', file);
    return httpServices.post(UPLOAD_IMG, form);
  }

  updateUser(body = {}) {
    return httpServices.patch(`${USER_OKR}${body.id}`, body);
  }
}

export default new UserService();
