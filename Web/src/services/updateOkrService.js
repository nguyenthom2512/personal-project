import { UPDATE_OKR } from 'constants/api';
import httpService from './httpServices';

class UpdateOkrService {
  patchUpdateOkr(id, body) {
    return httpService.patch(UPDATE_OKR(id), body);
  }
}
export default new UpdateOkrService();
