import { RESET_PASSWORD } from 'constants/api';
import httpService from './httpServices';

class resetPasswordService {
  postResetPassword(body) {
    return httpService.post(RESET_PASSWORD, body);
  }
}

export default new resetPasswordService();
