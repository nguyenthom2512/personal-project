import { DEPARTMENT, UNIT, OKR, REPORT_OKR, REPORT_CHECKIN } from 'constants/api';
import httpService from './httpServices';

class reportServices {
  getReportOkr(param) {
    return httpService.get(REPORT_OKR, { params: param });
  }
  getReportCheckin(param) {
    return httpService.get(REPORT_CHECKIN, { params: param });
  }
}

export default new reportServices();
