import { COMMENT, COMMENT_WITH_ID } from 'constants/api';
import httpService from './httpServices';

class dataCommentService {
  getDataComment(params = {}) {
    return httpService.get(COMMENT, { params });
  }
  postDataComment(body) {
    return httpService.post(COMMENT, body);
  }
  putDataComment(id, body) {
    return httpService.put(COMMENT_WITH_ID(id), body);
  }
  deleteDataComment(id) {
    return httpService.delete(COMMENT_WITH_ID(id));
  }
}
export default new dataCommentService();
