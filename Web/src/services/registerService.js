import { REGISTER_OKR } from 'constants/api';
import httpServices from './httpServices';

class registerService {
  postRegister(body) {
    return httpServices.post(REGISTER_OKR, body);
  }
}
export default new registerService();
