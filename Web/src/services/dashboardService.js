import { DASHBOARD } from 'constants/api';
import { convertParamsToQuery } from 'helpers';
import httpService from './httpServices';

class feedbackService {
  getWorkProcessData(param) {
    return httpService.get(`${DASHBOARD}${convertParamsToQuery(param)}`);
  }

  // getFeedbackStatic() {
  //   return httpService.get(FEEDBACK_STATIC);
  // }
}

export default new feedbackService();
