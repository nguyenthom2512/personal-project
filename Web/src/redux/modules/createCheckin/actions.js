import * as types from 'redux/types';

export const createCheckin = (data, callbacks) => {
  return {
    data,
    type: types.REQUEST_CREATE_CHECKIN,
    callbacks,
  };
};
export const createCheckinSuccess = (project) => {
  return {
    type: types.CREATE_CHECKIN_SUCCESS,
    project,
  };
};
export const createCheckinFail = (error) => {
  return {
    type: types.CREATE_CHECKIN_FAIL,
    error,
  };
};
