import { isEmpty } from 'lodash-es';
import { takeLatest, put } from 'redux-saga/effects';
import * as types from 'redux/types';
import checkinDetailService from 'services/checkinDetailService';

function* createCheckin({ data, callbacks }) {
  try {
    const response = yield checkinDetailService.postCheckinDetail(data);

    yield put({ type: types.CREATE_CHECKIN_SUCCESS, data: response.data });
    callbacks.onSuccess && callbacks.onSuccess(response.data);
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.CREATE_CHECKIN_FAIL, error: errStr });
    callbacks.onFailed && callbacks.onFailed(errStr);
  }
}

export function* createCheckinSaga() {
  yield takeLatest(types.REQUEST_CREATE_CHECKIN, createCheckin);
}
