import { OKR } from 'constants/api';
import { isEmpty } from 'lodash';
import { takeLatest, put } from 'redux-saga/effects';
import * as types from 'redux/types';
import httpServices from 'services/httpServices';

function* checkinOkr({ payload }) {
  try {
    const response = yield httpServices.get(OKR, {
      params: payload,
    });

    if (response.status === 200) {
      yield put({ type: types.REQUEST_CHECKIN_OKR_SUCCESS, data: response.data });
      // authServices.saveUserLocalStorage(response);
    } else {
      yield put({ type: types.REQUEST_CHECKIN_OKR_FAILED, error: response.data });
    }
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.REQUEST_CHECKIN_OKR_FAILED, error: errStr });
  }
}
export function* checkinOkrSaga() {
  yield takeLatest(types.REQUEST_CHECKIN_OKR, checkinOkr);
}
