import * as types from 'redux/types';

export const checkinOKR = (payload) => ({
  type: types.REQUEST_CHECKIN_OKR,
  payload,
});

export const checkinOkrSuccess = (data) => ({
  type: types.REQUEST_CHECKIN_OKR_SUCCESS,
  data,
});
export const checkinOkrFail = (error) => ({
  type: types.REQUEST_CHECKIN_OKR_FAILED,
  error,
});
