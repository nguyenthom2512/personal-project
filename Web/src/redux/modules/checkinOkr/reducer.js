import produce from 'immer';
import * as types from 'redux/types';

const initialState = {
  data: {
    id: 0,
    checkin_result: [],
  },
  error: '',
  loading: false,
  type: '',
};

export const checkinOkrReducer = (state = initialState, action) => {
  return produce(state, (draftState) => {
    draftState.type = action.type;
    switch (action.type) {
      case types.REQUEST_CHECKIN_OKR:
        draftState.loading = true;
        break;
      case types.REQUEST_CHECKIN_OKR_SUCCESS:
        draftState.dataParent = action.data;
        break;
      case types.REQUEST_CHECKIN_OKR_FAILED:
        draftState.error = action.error;
        break;
      default:
        break;
    }
  });
};
