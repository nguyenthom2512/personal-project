import * as types from 'redux/types';

export const okrDetailRequest = (payload) => ({
  type: types.OKR_DETAIL_REQUEST,
  payload,
});

export const okrDetailSuccess = (data) => ({
  type: types.OKR_DETAIL_SUCCESS,
  data,
});

export const okrDetailFailed = (error) => ({
  type: types.OKR_DETAIL_FAILED,
  error,
});
// export const okrListParentRequest = (payload) => ({
//   type: types.OKR_LIST_PARENT_REQUEST,
//   payload,
// });

// export const okrListParentSuccess = (data) => ({
//   type: types.OKR_LIST_PARENT_SUCCESS,
//   data,
// });

// export const okrListParentFailed = (error) => ({
//   type: types.OKR_LIST_PARENT_FAILED,
//   error,
// });
