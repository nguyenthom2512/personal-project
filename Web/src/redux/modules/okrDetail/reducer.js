import produce from 'immer';
import * as types from 'redux/types';

const initialState = {
  data: {},
  error: '',
  loading: false,
  type: '',
};

export const okrDetailListReducer = (state = initialState, action) => {
  return produce(state, (draftState) => {
    draftState.type = action.type;
    switch (action.type) {
      case types.OKR_DETAIL_REQUEST:
        draftState.loading = true;
        break;
      case types.OKR_DETAIL_SUCCESS:
        draftState.data = action.data;
        break;
      case types.OKR_DETAIL_FAILED:
        draftState.error = action.error;
        break;
      //   case types.OKR_LIST_PARENT_REQUEST:
      //     draftState.loading = true;
      //     break;
      //   case types.OKR_LIST_PARENT_SUCCESS:
      //     draftState.dataParent = action.data;
      //     break;
      //   case types.OKR_LIST_PARENT_FAILED:
      //     draftState.error = action.error;
      //     break;
      default:
        break;
    }
  });
};
