import produce from 'immer';
import * as types from 'redux/types';

const initialState = {
  auth: {
    isLogin: false,
    error: null,
    loadingAuth: true,
  },
};

export const authReducer = (state = initialState, action) => {
  return produce(state, (draftState) => {
    switch (action.type) {
      case types.REQUEST_LOGIN:
        draftState.auth.isLogin = false;
        break;

      case types.REQUEST_LOGIN_SUCCESS:
        draftState.auth.isLogin = true;
        draftState.auth.loadingAuth = false;
        break;

      case types.REQUEST_LOGIN_FAILED:
        draftState.auth.isLogin = false;
        draftState.auth.error = action.error;
        draftState.auth.loadingAuth = false;
        break;
      case types.REQUEST_CHECK_AUTH:
        draftState.auth.loadingAuth = true;
        break;
      case types.DONE_CHECK_AUTH:
        draftState.auth.loadingAuth = false;
        break;
      default:
        break;
    }
  });
};
