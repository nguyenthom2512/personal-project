import { takeLatest, put } from 'redux-saga/effects';
import * as types from 'redux/types';
import authServices from 'services/authServices';
import { isEmpty } from 'lodash';
import httpServices from 'services/httpServices';

export const errorLoginType = {
  unknown: '1',
  unAuth: '2',
};

function* login({ payload, callback }) {
  const { email, password } = payload;
  const { onError, onSuccess } = callback;

  try {
    const response = yield authServices.postLoginOkr({ email: email.trim(), password });
    if (response.status === 200) {
      authServices.saveUserLocalStorage(response.data);
      httpServices.attachTokenToHeader(response.data.token);
      onSuccess && onSuccess();
      yield put({ type: types.REQUEST_LOGIN_SUCCESS, data: response.data });
    } else {
      onError && onError(errorLoginType.unknown);
      yield put({ type: types.REQUEST_LOGIN_FAILED, error: 'Đăng nhập không thành công' });
    }
  } catch (error) {
    const errorText = errorLoginType.unAuth;
    onError && onError(errorText);
    yield put({ type: types.REQUEST_LOGIN_FAILED, error: errorText });
  }
}

function* logout() {
  try {
    yield authServices.clearUserLocalStorage();
    yield put({ type: types.DONE_CHECK_AUTH });
    window.location.reload();
  } catch (error) {}
}

function* checkAuth() {
  const dataUser = authServices.getUserLocalStorage();
  if (!isEmpty(dataUser)) {
    httpServices.attachTokenToHeader(dataUser.token);
    yield put({ type: types.REQUEST_LOGIN_SUCCESS });
  } else {
    yield put({ type: types.DONE_CHECK_AUTH });
  }
}

export function* authSaga() {
  yield takeLatest(types.REQUEST_LOGIN, login);
  yield takeLatest(types.REQUEST_CHECK_AUTH, checkAuth);
  yield takeLatest(types.REQUEST_LOGOUT, logout);
}
