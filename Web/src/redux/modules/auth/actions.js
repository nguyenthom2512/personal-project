import * as types from 'redux/types';

export const login = (email, password, onError, onSuccess) => ({
  type: types.REQUEST_LOGIN,
  payload: {
    email,
    password,
  },
  callback: {
    onError,
    onSuccess,
  },
});

export const checkAuth = () => ({
  type: types.REQUEST_CHECK_AUTH,
});

export const logout = () => ({
  type: types.REQUEST_LOGOUT,
});
