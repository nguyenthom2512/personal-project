import produce from 'immer';
import * as types from 'redux/types';

const initialState = {
  data: {},
};

export const saveTreesOkrReducer = (state = initialState, action) => {
  return produce(state, (draftState) => {
    draftState.type = action.type;
    switch (action.type) {
      case types.ADD_TREE_TO_REDUCER:
        draftState.data = action.data;
        break;
      case types.EDIT_TREE_TO_REDUCER:
        draftState.data = action.data;
        break;
      case types.DELETE_TREE_TO_REDUCER:
        draftState.data = action.data;
        break;
      case types.SAVE_TREE_TO_REDUCER:
        draftState.data = action.data;
        break;

      default:
        break;
    }
  });
};
