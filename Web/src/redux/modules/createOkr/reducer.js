import produce from 'immer';
import * as types from 'redux/types';

const initialState = {
  data: {},
  error: '',
  type: '',
};

export const createOkrReducer = (state = initialState, action) => {
  return produce(state, (draftState) => {
    draftState.type = action.type;
    switch (action.type) {
      case types.REQUEST_CREATE_OKR:
        draftState.data = action.data;
        break;

      case types.REQUEST_CREATE_OKR_SUCCESS:
        draftState.data = action.data;
        break;

      case types.REQUEST_CREATE_OKR_FAILED:
        draftState.error = action.error;
        break;

      default:
        break;
    }
  });
};
