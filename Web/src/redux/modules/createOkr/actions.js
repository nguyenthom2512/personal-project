import * as types from 'redux/types';

export const createOkr = (data, callbacks) => {
  return {
    data,
    type: types.REQUEST_CREATE_OKR,
    callbacks,
  };
};
export const createOkrSuccess = (project) => {
  return {
    type: types.REQUEST_CREATE_OKR_SUCCESS,
    project,
  };
};
export const createOkrFail = (error) => {
  return {
    type: types.REQUEST_CREATE_OKR_FAILED,
    error,
  };
};
export const createOkrReset = (error) => {
  return {
    type: types.REQUEST_CREATE_OKR_RESET,
    error,
  };
};

// update
export const updateOkr = (id, data, callbacks) => {
  return {
    id,
    data,
    type: types.REQUEST_UPDATE_OKR,
    callbacks,
  };
};
export const updateOkrSuccess = (project) => {
  return {
    type: types.REQUEST_UPDATE_OKR_SUCCESS,
    project,
  };
};
export const updateOkrFail = (error) => {
  return {
    type: types.REQUEST_UPDATE_OKR_FAILED,
    error,
  };
};
export const updateOkrReset = (error) => {
  return {
    type: types.REQUEST_UPDATE_OKR_RESET,
    error,
  };
};
