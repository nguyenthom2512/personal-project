import { isEmpty } from 'lodash-es';
import { takeLatest, put } from 'redux-saga/effects';
import * as types from 'redux/types';
import createOkrServices from 'services/createOkrServices';
import updateOkrService from 'services/updateOkrService';

function* createOkr({ data, callbacks }) {
  try {
    const response = yield createOkrServices.postCreateOkr(data);

    yield put({ type: types.REQUEST_CREATE_OKR_SUCCESS, data: response.data });
    callbacks.onSuccess && callbacks.onSuccess(response.data);
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.REQUEST_CREATE_OKR_FAILED, error: errStr });
    callbacks.onFailed && callbacks.onFailed(errStr);
  }
}

function* updateOkr({ id, data, callbacks }) {
  try {
    const response = yield updateOkrService.patchUpdateOkr(id, data);

    yield put({ type: types.REQUEST_UPDATE_OKR_SUCCESS, data: response.data });
    callbacks.onSuccess && callbacks.onSuccess(response.data);
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.REQUEST_UPDATE_OKR_FAILED, error: errStr });
    callbacks.onFailed && callbacks.onFailed(errStr);
  }
}

export function* createOkrSaga() {
  yield takeLatest(types.REQUEST_CREATE_OKR, createOkr);
  yield takeLatest(types.REQUEST_UPDATE_OKR, updateOkr);
}
