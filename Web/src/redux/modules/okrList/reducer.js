import produce from 'immer';
import * as types from 'redux/types';

const initialState = {
  data: {
    count: 0,
    results: [],
  },
  dataParent: {
    count: 0,
    next: null,
    previous: null,
    results: [],
  },
  error: '',
  loading: false,
  type: '',
};

export const okrListReducer = (state = initialState, action) => {
  return produce(state, (draftState) => {
    draftState.type = action.type;
    switch (action.type) {
      case types.OKR_LIST_REQUEST:
        draftState.loading = true;
        break;
      case types.OKR_LIST_SUCCESS:
        draftState.data = action.data;
        draftState.loading = false;

        break;
      case types.OKR_LIST_FAILED:
        draftState.error = action.error;
        draftState.loading = false;

        break;

      // update
      case types.REQUEST_UPDATE_KR:
        draftState.loading = action.loading;
        break;
      case types.REQUEST_UPDATE_KR_SUCCESS:
        draftState.dataParent = action.data;
        break;
      case types.REQUEST_UPDATE_KR_FAILED:
        draftState.error = action.error;
        break;

      //get result
      case types.REQUEST_GET_OKR:
        draftState.error = action.error;
        break;
      case types.REQUEST_GET_OKR_SUCCESS:
        draftState.dataParent = action.data;
        break;
      case types.REQUEST_GET_OKR_FAILED:
        draftState.error = action.error;
        break;
      // add result

      case types.REQUEST_POST_RESULT:
        draftState.error = action.error;
        break;
      case types.REQUEST_POST_RESULT_SUCCESS:
        draftState.dataParent = action.data;
        break;
      case types.REQUEST_POST_RESULT_FAILED:
        draftState.error = action.error;
        break;
      default:
        break;
    }
  });
};
