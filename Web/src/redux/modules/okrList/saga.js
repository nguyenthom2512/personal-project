import { OKR, RESULT } from 'constants/api';
import { isEmpty } from 'lodash';
import { takeLatest, put } from 'redux-saga/effects';
import * as types from 'redux/types';
import httpServices from 'services/httpServices';
import addResultServices from 'services/addResultServices';
import getOkrResult from 'services/getOkrResult';
import updateOkrServices from 'services/updateOkrServices';

function* okrList({ payload }) {
  try {
    const response = yield httpServices.get(OKR, {
      params: payload,
    });
    if (response.status === 200) {
      yield put({ type: types.OKR_LIST_SUCCESS, data: response.data });
      // authServices.saveUserLocalStorage(response);
    } else {
      yield put({ type: types.OKR_LIST_FAILED, error: response.data });
    }
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.OKR_LIST_FAILED, error: errStr });
  }
}
function* okrListParent({ payload }) {
  try {
    const response = yield httpServices.get(OKR, {
      params: payload,
    });
    if (response.status === 200) {
      yield put({ type: types.OKR_LIST_PARENT_SUCCESS, data: response.data });
    } else {
      yield put({ type: types.OKR_LIST_PARENT_FAILED, error: response.data });
    }
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.OKR_LIST_PARENT_FAILED, error: errStr });
  }
}
function* updateOkr({ data, callbacks }) {
  try {
    const response = yield updateOkrServices.patchUpdateOkr(data);
    yield put({ type: types.REQUEST_UPDATE_KR_SUCCESS, data: response.data });
    callbacks.onSuccess && callbacks.onSuccess(response.data);
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.REQUEST_UPDATE_KR_FAILED, error: errStr });
    callbacks.onFailed && callbacks.onFailed(errStr);
  }
}
function* getOkr({ data, callbacks }) {
  try {
    const response = yield getOkrResult.getOkrResult(data);
    yield put({ type: types.REQUEST_GET_OKR_SUCCESS, data: response.data });
    callbacks.onSuccess && callbacks.onSuccess(response.data);
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.REQUEST_GET_OKR_FAILED, error: errStr });
    callbacks.onFailed && callbacks.onFailed(errStr);
  }
}

function* addResult({ data, callbacks }) {
  try {
    const response = yield addResultServices.addResult(data);
    yield put({ type: types.REQUEST_POST_RESULT_SUCCESS, data: response.data });
    callbacks.onSuccess && callbacks.onSuccess(response.data);
  } catch (error) {
    let errStr = error;
    if (!isEmpty(error.response)) {
      errStr = error.response.statusText;
    }
    yield put({ type: types.REQUEST_POST_RESULT_FAILED, error: errStr });
    callbacks.onFailed && callbacks.onFailed(errStr);
  }
}

export function* okrListSaga() {
  yield takeLatest(types.REQUEST_POST_RESULT, addResult);
  yield takeLatest(types.REQUEST_GET_OKR, getOkr);
  yield takeLatest(types.REQUEST_UPDATE_KR, updateOkr);
  yield takeLatest(types.OKR_LIST_REQUEST, okrList);
  yield takeLatest(types.OKR_LIST_PARENT_REQUEST, okrListParent);
}
