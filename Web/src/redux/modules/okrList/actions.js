import * as types from 'redux/types';

export const okrListRequest = (payload) => ({
  type: types.OKR_LIST_REQUEST,
  payload,
});

export const okrListSuccess = (data) => ({
  type: types.REQUEST_CHECK_AUTH,
  data,
});

export const okrListFailed = (error) => ({
  type: types.REQUEST_LOGOUT,
  error,
});
// export const okrListParentRequest = (payload) => ({
//   type: types.OKR_LIST_PARENT_REQUEST,
//   payload,
// });

// export const okrListParentSuccess = (data) => ({
//   type: types.OKR_LIST_PARENT_SUCCESS,
//   data,
// });

// export const okrListParentFailed = (error) => ({
//   type: types.OKR_LIST_PARENT_FAILED,
//   error,
// });
// update okr
export const updateOkrRequest = (data, callbacks) => {
  return {
    data,
    type: types.REQUEST_UPDATE_KR,
    callbacks,
  };
};
export const updateOkrSuccess = (project) => {
  return {
    type: types.REQUEST_UPDATE_KR_SUCCESS,
    project,
  };
};
export const updateOkrFail = (error) => {
  return {
    type: types.REQUEST_UPDATE_KR_FAILED,
    error,
  };
};
// get api result
export const getOkrRequest = (data, callbacks) => {
  return {
    data,
    type: types.REQUEST_GET_OKR,
    callbacks,
  };
};
export const getOkrSuccess = (project) => {
  return {
    type: types.REQUEST_GET_OKR_SUCCESS,
    project,
  };
};
export const getOkrFail = (error) => {
  return {
    type: types.REQUEST_GET_OKR_FAILED,
    error,
  };
};
//update result
export const updateResultKr = (data, callbacks) => {
  return {
    data,
    type: types.REQUEST_POST_RESULT,
    callbacks,
  };
};
export const updateResultKrSuccess = (project) => {
  return {
    type: types.REQUEST_POST_RESULT_SUCCESS,
    project,
  };
};
export const updateResultKrFail = (error) => {
  return {
    type: types.REQUEST_POST_RESULT_FAILED,
    error,
  };
};
