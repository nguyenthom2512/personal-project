import { combineReducers } from 'redux';
import { all, spawn, call } from 'redux-saga/effects';
import { authSaga, authReducer } from './modules/auth';
import { checkinOkrSaga } from './modules/checkinOkr';
import { createOkrSaga } from './modules/createOkr';
import { createOkrReducer } from './modules/createOkr';
import { okrListSaga, okrListReducer } from './modules/okrList';
import { createCheckinSaga, createCheckinReducer } from './modules/createCheckin';

export function* rootSagas() {
  const sagas = [authSaga, okrListSaga, createOkrSaga, checkinOkrSaga, createCheckinSaga];

  yield all(
    sagas.map((saga) =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e) {}
        }
      }),
    ),
  );
}

export const rootReducers = combineReducers({
  authReducer,
  createOkrReducer,
  okrListReducer,
  createCheckinReducer,
});
