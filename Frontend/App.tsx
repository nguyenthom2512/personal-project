/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { ReactNode, useEffect } from 'react';
import MainApp from './sources';
import './sources/Locales/IMLocalize';
import { DEFAULT_LIGHT_THEME } from './sources/Theme/DefaultLight.theme';
import { ThemeProvider } from './sources/Theme/Theme.context';
import messaging from '@react-native-firebase/messaging';
import { DeviceEventEmitter } from 'react-native';
import { EventName } from './sources/Helpers/constant';
// import { useDispatch } from 'react-redux';
// import { getTodoDashboardRequest } from './sources/Redux/Actions/todoDashboardActions';

const App: () => ReactNode = () => {
  //   async function registerAppWithFCM() {
  //     await messaging().registerDeviceForRemoteMessages();
  //   }

  // const dispatch = useDispatch();

  useEffect(() => {
    // registerAppWithFCM();
    messaging().setBackgroundMessageHandler(async (remoteMessage) => {
      console.log('Message handled in the background!', remoteMessage);
    });
    messaging().onMessage(async (message) => {
      DeviceEventEmitter.emit(EventName.onReceivedNotification)
    });
  }, []);
  return (
    <ThemeProvider initial={DEFAULT_LIGHT_THEME}>
      <MainApp />
    </ThemeProvider>
  );
};

export default App;
