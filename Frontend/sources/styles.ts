import { StyleSheet } from 'react-native';
import { device } from './Helpers';
import { Theme } from './Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    loadingScreen: {
      flex: 1,
      backgroundColor: 'transparent',
      alignItems: 'center',
      paddingTop: device.height * 0.35,
    },
    textLoading: {
      fontSize: theme.fontSize.f24,
      fontWeight: '700',
      marginBottom: theme.spacing.p12,
    },
  });
