import messaging from '@react-native-firebase/messaging';
import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { ActivityIndicator, LogBox, View } from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor, store } from '../Store';
import { AppText } from './Components';
import AppLoading from './Components/AppLoading';
import { CallContext, IndicatorContext } from './Context';
import { ICallContext } from './interfaces/call.interface';
import { LoadingContext } from './interfaces/loading.interface';
// @ts-ignore
import ModalCall from './Modules/Call';
import RootStackScreen from './Routes';
import styles from './styles';
import { useTheme, useThemeAwareObject } from './Theme';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { WEB_CLIENT_ID } from './Helpers/constant';

GoogleSignin.configure({
  webClientId: WEB_CLIENT_ID,
});

LogBox.ignoreLogs([
  'VirtualizedLists should never be nested',
  'Remote debugger is in a background tab which may cause apps to perform slowly',
  'DevTools failed to load SourceMap',
  'Illegal node ID set as an input for Animated.DiffClamp node',
]);

export default () => {
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const [isLoading, setIsLoading] = React.useState(true);
  const [loading, setLoading] = React.useState(false);
  const [roomId, setRoomId] = React.useState<number | string>(-1);

  const setLoadingState = (value: boolean) => {
    setLoading(value);
  };

  const setRoomState = (value: number | string) => {
    setRoomId(value);
  };

  const loadingStore: LoadingContext = {
    loading,
    setLoading: setLoadingState,
  };
  const callStore: ICallContext = {
    roomId,
    setRoomId: setRoomState,
  };

  React.useEffect(() => {
    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      console.log('remoteMessage', remoteMessage);
      //   Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });
    setTimeout(() => {
      setRoomId(0);
      setIsLoading(false);
      SplashScreen.hide();
    }, 800);
    return unsubscribe;
  }, []);

  const renderLoadingScreen = () => {
    return (
      <View style={Styles.loadingScreen}>
        <AppText style={Styles.textLoading}>OKRs Online</AppText>
        <ActivityIndicator color={theme.color.violet} />
      </View>
    );
  };

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <NavigationContainer>
          <IndicatorContext.Provider value={loadingStore}>
            <CallContext.Provider value={callStore}>
              {isLoading ? renderLoadingScreen() : <RootStackScreen />}
              {Boolean(roomId) && <ModalCall roomId={roomId} />}
            </CallContext.Provider>
            <AppLoading isVisible={loading} />
          </IndicatorContext.Provider>
        </NavigationContainer>
      </PersistGate>
    </Provider>
  );
};
