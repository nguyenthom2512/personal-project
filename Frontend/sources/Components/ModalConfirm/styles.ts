import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flex1: {
      flex: 1,
    },
    headerContainer: {
      //   borderTopLeftRadius: theme.spacing.p16,
      //   borderTopRightRadius: theme.spacing.p16,
      //   backgroundColor: theme.color.white,
      // backgroundColor: '#E5E5E5',
      //   maxHeight: 58,
      padding: theme.spacing.p8,
      alignItems: 'center',
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: '#E5E5E5',
    },
    bodyWrap: {
      flex: 1,
      backgroundColor: '#00000080',
      justifyContent: 'center',
      padding: theme.spacing.p16,
    },
    safeBottom: {
      backgroundColor: theme.color.white,
      padding: theme.spacing.p16,
      paddingBottom: theme.spacing.p24,
    },
    footerWrap: {
      flexDirection: 'row',
      borderBottomRightRadius: theme.spacing.p16,
      borderBottomLeftRadius: theme.spacing.p16,
      padding: theme.spacing.p8,
      backgroundColor: theme.color.white,
      //   flex: 1,
    },
    headerTitle: {
      textAlign: 'center',
      fontSize: theme.fontSize.f16,
      fontWeight: '700',
    },
    bodyContainer: { backgroundColor: theme.color.white, borderRadius: 8 },
  });
  return styles;
};
