import React from 'react';
import { useTranslation } from 'react-i18next';
import { TextStyle, TouchableOpacity, View, ViewStyle } from 'react-native';
import Modal from 'react-native-modal';
import { AppButton, AppText } from '..';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';
interface PropsModal {
  visible: boolean;
  iconLeft?: any;
  iconRight?: any;
  onLeftPress?: () => void;
  onRightPress?: () => void;
  titleHeader?: string;
  contentHeader?: any;
  children?: any;
  onRequestClose?: any;
  contentStyle?: ViewStyle;
  type?: boolean;
  headerStyles?: TextStyle;
  labelYes?: string;
  labelNo?: string;
  onPressYes?: () => void;
  onPressNo?: () => void;
}

const ModalConfirm = (props: PropsModal) => {
  const {
    onPressYes = () => {},
    onPressNo = () => {},
    labelYes = '',
    labelNo = '',
  } = props;
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const HeaderModal = () => {
    return (
      <View style={Styles.headerContainer}>
        <View>
          {props.iconLeft ? (
            <TouchableOpacity
              onPress={props.onLeftPress}
              style={{
                paddingHorizontal: 16,
                height: '100%',
                justifyContent: 'center',
              }}
            >
              {props.iconLeft}
            </TouchableOpacity>
          ) : (
            <View style={{ width: 56 }} />
          )}
        </View>
        <View style={{ flex: 1 }}>
          {props.type ? (
            props.contentHeader
          ) : (
            <AppText style={[Styles.headerTitle, props.headerStyles]}>
              {props.titleHeader || t('common:notice')}
            </AppText>
          )}
        </View>
        <View>
          {props.iconRight ? (
            <TouchableOpacity
              onPress={props.onRightPress}
              style={{
                paddingHorizontal: 16,
                height: '100%',
                justifyContent: 'center',
              }}
            >
              {props.iconRight}
            </TouchableOpacity>
          ) : (
            <View style={{ width: 56 }} />
          )}
        </View>
      </View>
    );
  };

  return (
    <Modal
      style={{ margin: 0 }}
      coverScreen={true}
      avoidKeyboard
      isVisible={props.visible}
      backdropOpacity={0.2}
      // transparent={true}
      // onRequestClose={props.onRequestClose}
      useNativeDriver
      useNativeDriverForBackdrop
      hideModalContentWhileAnimating
    >
      <View style={Styles.bodyWrap}>
        {props.visible ? (
          <View style={Styles.bodyContainer}>
            <HeaderModal />
            <View style={[Styles.safeBottom, props.contentStyle]}>
              {props.children}
            </View>
            <View style={Styles.footerWrap}>
              <View style={{ flex: 0.5 }} />
              <AppButton
                containerStyle={{ flex: 1 }}
                title={labelYes || t('common:yes')}
                onPress={onPressYes}
              />
              <View style={{ flex: 0.1 }} />
              <AppButton
                cancel
                containerStyle={{ flex: 1 }}
                title={labelNo || t('common:no')}
                onPress={onPressNo}
              />
            </View>
          </View>
        ) : (
          <View />
        )}
      </View>
    </Modal>
  );
};

export default ModalConfirm;
