import React from 'react';
import {
  Text,
  TextStyle,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';
import Modal from 'react-native-modal';
import { Modalize } from 'react-native-modalize';
interface PropsModal {
  visible: boolean;
  iconLeft?: any;
  iconRight?: any;
  onLeftPress?: () => void;
  onRightPress?: () => void;
  titleHeader?: string;
  contentHeader?: any;
  headerText?: string;
  children?: any;
  onRequestClose?: any;
  contentStyle?: ViewStyle;
  type?: boolean;
  headerStyles?: TextStyle;
  avoidKeyboard?: boolean;
  onClickOutside?: () => void;
}

export default (props: PropsModal) => {
  const { onClickOutside = () => {} } = props;
  const Styles = useThemeAwareObject(styles);

  const { avoidKeyboard = true } = props;

  const HeaderModal = () => {
    return (
      <View style={Styles.headerContainer}>
        <View>
          {props.iconLeft ? (
            <TouchableOpacity
              onPress={props.onLeftPress}
              style={{
                paddingHorizontal: 16,
                height: '100%',
                justifyContent: 'center',
              }}
            >
              {props.iconLeft}
            </TouchableOpacity>
          ) : (
            <View style={{ width: 56 }} />
          )}
        </View>
        <View style={{ flex: 1 }}>
          {props.type ? (
            props.contentHeader
          ) : (
            <Text
              style={[
                { textAlign: 'center', fontSize: 18, fontWeight: '600' },
                props.headerStyles,
              ]}
            >
              {props.headerText}
            </Text>
          )}
        </View>
        <View>
          {props.iconRight ? (
            <TouchableOpacity
              onPress={props.onRightPress}
              style={{
                paddingHorizontal: 16,
                height: '100%',
                justifyContent: 'center',
              }}
            >
              {props.iconRight}
            </TouchableOpacity>
          ) : (
            <View style={{ width: 56 }} />
          )}
        </View>
      </View>
    );
  };

  return (
    <Modal
      style={{ margin: 0 }}
      coverScreen={true}
      avoidKeyboard={avoidKeyboard}
      isVisible={props.visible}
      backdropOpacity={0.2}
      // transparent={true}
      // onRequestClose={props.onRequestClose}
      useNativeDriver
      useNativeDriverForBackdrop
      hideModalContentWhileAnimating
      // {...props}
    >
      <TouchableWithoutFeedback onPress={onClickOutside}>
        <View style={Styles.bodyWrap}>
          <>
            <HeaderModal />
            <View style={[Styles.safeBottom, props.contentStyle]}>
              {props.children}
            </View>
          </>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
};
