import { StyleSheet } from 'react-native';
import { device } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    wrapper: {
      backgroundColor: theme.color.drawerContainer,
      height: '100%',
    //   maxWidth: device.width * 0.2
    },
    container: {
      paddingHorizontal: theme.spacing.p20,
    },
    content: {
      flexDirection: 'row',
      marginBottom: theme.spacing.p36,
      paddingRight: theme.spacing.p8,
      alignItems: 'center'
    },
    title: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.white,
      marginLeft: theme.spacing.p16,
    },
    iconClose: {
        marginBottom: theme.spacing.p40,
        marginTop: theme.spacing.p12
    }
  });
  return styles;
};
