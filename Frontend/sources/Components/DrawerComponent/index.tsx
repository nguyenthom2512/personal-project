import React from 'react';
import { SafeAreaView, StyleSheet, TouchableOpacity, View } from 'react-native';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppText from '../AppText';
import styles from './styles';
import Ionicons from 'react-native-vector-icons/Ionicons';
import OKRS from '../../Assets/Svg/okrs.svg';
import CheckIn from '../../Assets/Svg/checkIn.svg';
import CFR from '../../Assets/Svg/CFR.svg';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

interface Props {}

export default (props: Props) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dataCheckIn = [
    {
      icon: <OKRS />,
      title: t('checkIn:OKRs'),
      key: 'okrs',
    },
    {
      icon: <CheckIn />,
      title: t('checkIn:checkIn'),
      key: 'check-in',
    },
    {
      icon: <CFR />,
      title: t('checkIn:CFRs'),
      key: 'cfrs',
    },
    {
      icon: (
        <Ionicons
          name="pricetag"
          size={24}
          color={theme.color.white}
          style={{ marginTop: 0 }}
        />
      ),
      title: t('license:upgradeAccount'),
      key: 'License',
    },
  ];

  const pressAction = (key: string) => {
    if (key == 'check-in') {
      navigation.navigate('CheckIn');
    }
    if (key == 'okrs') {
      navigation.navigate('TabsScreen');
    }
    if (key == 'cfrs') {
      navigation.navigate('CFRS');
    }
    if (key == 'License') {
      navigation.navigate('License');
    }
  };
  return (
    <SafeAreaView style={Styles.wrapper}>
      <View style={Styles.container}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <Ionicons
            name="close"
            size={32}
            color={theme.color.gray}
            style={Styles.iconClose}
          />
        </TouchableOpacity>
        {dataCheckIn?.map((el: any, index: number) => {
          return (
            <TouchableOpacity
              style={Styles.content}
              key={index}
              onPress={() => pressAction(el.key)}
            >
              {el?.icon}
              <AppText style={Styles.title}>{el?.title}</AppText>
            </TouchableOpacity>
          );
        })}
      </View>
    </SafeAreaView>
  );
};
