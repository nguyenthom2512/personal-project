import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      backgroundColor: theme.color.black,
    },
  });
