import React from 'react';
import { ActivityIndicator, View } from 'react-native';
import Modal from 'react-native-modal';
import { device } from '../../Helpers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

// class AppLoading extends React.Component {
//   Styles = useThemeAwareObject(styles);
//   retn;
// }

export default ({ isVisible }: { isVisible: boolean }) => {
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  if (!isVisible) return null;

  return (
    <View
      // useNativeDriver
      // animationIn="fadeIn"
      // animationOut="fadeOut"
      style={{
        zIndex: 99,
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        height: device.height,
        width: device.width,
        backgroundColor: `${theme.color.black}50`,
        justifyContent: 'center',
        alignItems: 'center',
      }}
      // coverScreen={true}
    >
      <ActivityIndicator size="large" color={theme.color.violet} />
    </View>
  );
};
