import React from 'react';
import { View, StyleSheet } from 'react-native';
import { device } from '../../Helpers';
import { Theme, useThemeAwareObject } from '../../Theme';
import { AppText } from '../index';

interface AppFlatlistFooterProps {
  message?: string;
  screenEmpty?: boolean;
}
export default (props: AppFlatlistFooterProps) => {
  const Styles = useThemeAwareObject(styles);
  const { message, screenEmpty = false } = props;
  return (
    <View style={Styles.container}>
      {!screenEmpty && <AppText>{message || 'Danh sách rỗng'}</AppText>}
    </View>
  );
};

const styles = (theme: Theme) => {
  const style = StyleSheet.create({
    container: {
      height: device.height * 0.2,
      width: device.width,
      flex: 1,
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  return style;
};
