import { ErrorMessage } from 'formik';
import React from 'react';
import AppText from '../AppText';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';

const ErrorMessages = ({ name }: { name: string }) => {
  const Styles = useThemeAwareObject(styles);
  return (
    <ErrorMessage
      name={name}
      render={(errorMess: string) => {
        if (errorMess) {
          return <AppText style={Styles.errorText}>{errorMess}</AppText>;
        }
        return null;
      }}
    />
  );
};

export default ErrorMessages;
