import { StyleSheet } from 'react-native';
import { measure, padding } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    input: {
      height: measure.inputHeight,
      paddingHorizontal: theme.spacing.p8,
      // borderBottomWidth: 1,
    },
    errorText: {
      color: theme.color.torchRed,
      fontSize: theme.fontSize.f14,
      marginTop: theme.spacing.p4
    },
  });
