import { StyleSheet } from 'react-native';
import { fontSize, measure, responsivePixel } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      width: '100%',
    },
    input: {
      color: theme.color.shark,
      height: measure.selectHeight,
      paddingHorizontal: theme.spacing.p12,
      // paddingVertical: theme.spacing.p15,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.color.holderPlace,
      fontSize: theme.fontSize.f14,
      marginVertical: theme.spacing.p6,
      backgroundColor: theme.color.white,
    },
    errorText: {
      color: theme.color.torchRed,
    },
    labelStyle: {
      paddingTop: theme.spacing.p18,
      paddingBottom: theme.spacing.p2,
    },
    txtTitle: {
      color: theme.color.shark,
      fontSize: fontSize.f14,
      fontWeight: '400',
    },
    require: {
      fontSize: theme.fontSize.f16,
      color: theme.color.redAlert,
      marginLeft: theme.spacing.p4,
    },
  });
