import get from 'lodash/get';
import React, { useCallback } from 'react';
import {
  KeyboardTypeOptions,
  TextInput,
  TextStyle,
  View,
  ViewStyle
} from 'react-native';
import { useThemeAwareObject } from '../../Theme';
import { AppField, AppText, ErrorMessages } from '../index';
import styles from './styles';
interface Props {
  name: any;
  form?: any;
  field?: any;
  onChangeCustom?: any;
  placeholder?: any;
  memo?: boolean;
  customInputStyle?: TextStyle;
  containerStyle?: ViewStyle;
  keyboardType?: KeyboardTypeOptions | undefined;
  title?: string;
  secureTextEntry?: boolean;
  editable?: boolean;
  multiline?: boolean;
  required?: boolean;
  customValue?: string;
  notShowError?: boolean;
  customPlaceholderColor?: string;
}

const Input = (props: Props) => {
  const {
    form,
    field,
    onChangeCustom,
    placeholder,
    customInputStyle,
    keyboardType = 'default',
    secureTextEntry = false,
    containerStyle,
    editable = true,
    multiline = false,
    customValue,
    notShowError,
    customPlaceholderColor,
  } = props;
  const Styles = useThemeAwareObject(styles);
  const { name, value } = field;
  const { errors, setFieldValue } = form;

  const error = get(errors, name);

  const onChange = useCallback(
    (text) => {
      setFieldValue(name, text);
    },
    [name, setFieldValue],
  );

  return (
    <View style={[Styles.container, containerStyle]}>
      {props.title && (
        <View style={Styles.labelStyle}>
          <AppText style={Styles.txtTitle}>
            {props.title}
            {props.required && <AppText style={Styles.require}> * </AppText>}
          </AppText>
        </View>
      )}
      <TextInput
        onChangeText={onChangeCustom || onChange}
        placeholder={placeholder || ''}
        value={customValue || value}
        placeholderTextColor={'rgba(38, 39, 46, 0.5)'}
        style={[
          Styles.input,
          customInputStyle,
          !editable && { color: 'rgba(38, 39, 46, 0.5)' },
        ]}
        keyboardType={keyboardType}
        secureTextEntry={secureTextEntry}
        autoCapitalize="none"
        editable={editable}
        multiline={multiline}
        textAlignVertical={multiline ? 'top' : 'auto'}
      />
      {!notShowError && <ErrorMessages name={name} />}
    </View>
  );
};

export default (props: Props) => {
  const { memo, name, ...remainProps } = props;
  if (memo) {
    return <AppField name={name} component={Input} {...remainProps} />;
  }
  return <AppField name={name} component={Input} {...remainProps} />;
};
