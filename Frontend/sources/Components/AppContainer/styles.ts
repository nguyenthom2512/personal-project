import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      // backgroundColor: theme.color.backgroundColor,
    },
    content: {
      flex: 1,
    },
  });
  return styles;
};
