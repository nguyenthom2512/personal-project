import React from 'react';
import { View } from 'react-native';
import { useThemeAwareObject } from '../../Theme';
import AppHeader, { HeaderProps } from '../AppHeader';
import styles from './styles';

interface Props extends HeaderProps {
  children?: any;
}

export default (props: Props) => {
  const Styles = useThemeAwareObject(styles);

  return (
    <View style={Styles.container}>
      <AppHeader {...props} />
      <View style={Styles.content}>{props.children}</View>
    </View>
  );
};
