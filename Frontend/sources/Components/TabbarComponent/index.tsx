import {
  BottomTabBarOptions,
  BottomTabBarProps,
} from '@react-navigation/bottom-tabs';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { KeyboardAvoidingView, TouchableOpacity, View } from 'react-native';
// import { Modalize } from 'react-native-modalize';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import Feather from 'react-native-vector-icons/Feather';
// svg icon account
import SvgAccount from '../../Assets/Svg/account.svg';
import SvgAccountInactive from '../../Assets/Svg/accountInactive.svg';
// svg icon Home
import SvgHome from '../../Assets/Svg/home.svg';
import SvgHomeInactive from '../../Assets/Svg/homeInactive.svg';
// svg icon okr
import SvgOKR from '../../Assets/Svg/okr.svg';
import SvgOKRInactive from '../../Assets/Svg/okrInactive.svg';
// svg icon store
import SvgStore from '../../Assets/Svg/store.svg';
import SvgStoreInactive from '../../Assets/Svg/storeInactive.svg';

import { CreateMissions } from '../../Modules';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppText from '../AppText';
import styles from './styles';

import Modal from 'react-native-modal';

function TabbarComponent(props: BottomTabBarProps<BottomTabBarOptions>) {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const [isVisible, setIsVisible] = useState(false);
  //   const { ref, open, close } = useModalize();
  const open = () => setIsVisible(true);
  const close = () => setIsVisible(false);

  const iconTabbar = [
    {
      activeIcon: <SvgHome />,
      inActiveIcon: <SvgHomeInactive />,
    },
    {
      activeIcon: <SvgOKR />,
      inActiveIcon: <SvgOKRInactive />,
    },
    {
      activeIcon: (
        <View style={Styles.plusCenter}>
          <Feather
            name="plus"
            color={theme.color.white}
            size={theme.spacing.p20}
          />
        </View>
      ),
      inActiveIcon: (
        <View style={Styles.plusCenter}>
          <Feather
            name="plus"
            color={theme.color.white}
            size={theme.spacing.p20}
          />
        </View>
      ),
    },
    {
      activeIcon: <SvgStore />,
      inActiveIcon: <SvgStoreInactive />,
    },
    {
      activeIcon: <SvgAccount />,
      inActiveIcon: <SvgAccountInactive />,
    },
  ];

  const { state, descriptors, navigation } = props;

  return (
    <>
      <View style={[Styles.tabbarContainer, { paddingBottom: useSafeAreaInsets().bottom }]}>
        {(state.routes || []).map((route: any, index: number) => {
          const { options } = descriptors[route.key];
          const label =
            options.tabBarLabel !== undefined
              ? options.tabBarLabel
              : options.title !== undefined
                ? options.title
                : route.name;

          const isFocused = state.index === index;

          const onPress = (index: number) => {
            if (index === 2) {
              open();
            } else {
              close();
              const event = navigation.emit({
                type: 'tabPress',
                target: route.key,
                canPreventDefault: true,
              });

              if (!isFocused && !event.defaultPrevented) {
                // The `merge: true` option makes sure that the params inside the tab screen are preserved
                // @ts-ignore
                navigation.navigate({ name: route.name, merge: true });
              }
            }
          };

          return (
            <TouchableOpacity
              key={index}
              accessibilityRole="button"
              accessibilityState={isFocused ? { selected: true } : {}}
              accessibilityLabel={options.tabBarAccessibilityLabel}
              testID={options.tabBarTestID}
              onPress={() => onPress(index)}
              style={Styles.tabItemContainer}
            >
              {isFocused
                ? iconTabbar[index].activeIcon
                : iconTabbar[index].inActiveIcon}
              {index !== 2 && (
                <AppText
                  style={[
                    Styles.label,
                    isFocused
                      ? {
                        color: theme.color.violet,
                        fontWeight: '500',
                      }
                      : {
                        color: theme.color.mineShaft,
                        fontWeight: '500',
                      },
                  ]}
                >
                  {label}
                </AppText>
              )}
            </TouchableOpacity>
          );
        })}
      </View>
      <Modal
        isVisible={isVisible}
        style={{ margin: 0 }}
        hideModalContentWhileAnimating
      >
        <CreateMissions navigation={navigation} handleClose={() => close()} />
      </Modal>
    </>
  );
}

export default TabbarComponent;
