import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    plusCenter: {
      width: 40,
      height: 40,
      backgroundColor: theme.color.mediumPurple,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    tabItemContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: theme.spacing.p8,
    },
    label: {
      marginTop: theme.spacing.p4,
      fontSize: theme.fontSize.f12,
    },
    tabbarContainer: {
      flexDirection: 'row',
      backgroundColor: theme.color.white,
    },
  });
