import React, { useState } from 'react';
import { TextStyle } from 'react-native';
import { ViewStyle } from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import { useTheme } from '../../Theme';

interface Props {
  name: any;
  customInputStyle?: ViewStyle;
  title?: string;
  titleStyle?: TextStyle;
  onPress?: (checked: boolean) => void;
  disabled?: boolean;
  checked?: boolean;
}

export default 
(props: Props) => {
  const [checked, setChecked] = useState(props.checked || false);
  const { theme } = useTheme();

  const handlePress = () => {
    setChecked(!checked);
    props.onPress && props.onPress(!checked);
  };
  // checkBox with icon checkbox
  return (
    <CheckBox
    activeOpacity={0.7}
      containerStyle={{
        backgroundColor: 'transparent',
        borderWidth: 0,
        padding: 0,
        opacity: props.disabled ? 0.7 : 1,
      }}
      textStyle={props.titleStyle}
      title={props.title}
      disabled={props.disabled}
      checkedIcon={
        <Icon
          name="check-box"
          type="material"
          color={theme.color.violet}
          size={24}
          tvParallaxProperties
        />
      }
      uncheckedIcon={
        <Icon
          name="check-box-outline-blank"
          type="material"
          color={theme.color.violet}
          size={24}
          tvParallaxProperties
        />
      }
      checked={checked}
      onPress={handlePress}
    />
  );
};
