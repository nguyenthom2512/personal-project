import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    checkboxContainer: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    icon: {
      fontSize: theme.fontSize.f18,
      marginRight: theme.spacing.p4,
    },
  });
  return styles;
};
