import React from 'react';
import {
  View,
  ViewStyle,
  TouchableOpacity,
  FlatList,
  ImageStyle,
  PanResponder,
} from 'react-native';
import { Avatar } from 'react-native-elements';

import { useTheme, useThemeAwareObject } from '../../Theme';
import AppText from '../AppText';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import { useTranslation } from 'react-i18next';
import { Image } from 'react-native';
import serviceUrls from '../../Services/serviceUrls';
import { useNavigation, useRoute } from '@react-navigation/native';
import { TextStyle } from 'react-native';
import { includes } from 'lodash';
import Task from '../../Assets/Svg/task.svg';
import Comment from '../../Assets/Svg/comment.svg';
import ListAvatar from '../ListAvatar';

export interface TodoItem {
  todo_name: string;
  raic_user?: any[];
  priority?: any;
  img_url?: string;
  comment_count?: number;
  color?: string;
  type?: any;
  id?: number;
  department?: string;
  start_time?: string;
  end_time?: string;
  check_list?: any[];
  result?: string;
}
interface Props {
  containerStyles?: ViewStyle;
  data?: TodoItem[];
  onPress: () => void;
  title?: string;
  headerTodoStyle?: ViewStyle;
  showMenu?: boolean;
  imageStyle?: ImageStyle;
  containerTodo?: ViewStyle;
  textStyles?: TextStyle;
  showPriority?: boolean;
  onItemPress?: (id?: number) => void;
}
const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';
const DEFAULT_BANNER =
  'https://phongvu.vn/cong-nghe/wp-content/uploads/2021/06/laptop-chay-nhanh-hon-phong-vu-4.jpg';

interface TodoDataProps {
  data: TodoItem;
  containerStyles?: ViewStyle;
  imageStyle?: ImageStyle;
  showPriority?: boolean;
  onItemPress?: (id?: number) => void;
  onLongPress?: (positionX: number) => void;
}

export const TodoItemComponent = React.memo((props: TodoDataProps) => {
  const {
    data: item,
    containerStyles,
    imageStyle,
    showPriority,
    onItemPress,
    onLongPress,
  } = props;
  const {
    todo_name,
    raic_user = [],
    priority,
    img_url,
    comment_count,
    color,
    id,
    department,
    start_time,
    end_time,
    check_list = [],
    result,
  } = item;
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const route = useRoute();

  const navigation = useNavigation();

  // const isDone = check_list?.filter((el) => el.is_done === true )}
  const isDone = check_list?.filter((el) => el?.is_done);

  return (
    <View style={{ marginHorizontal: 8 }}>
      {!!img_url && (
        <Image
          source={{
            uri: img_url
              ? includes(img_url, 'http')
                ? img_url
                : `${serviceUrls.url.IMAGE}${img_url}`
              : DEFAULT_BANNER,
          }}
          resizeMode="cover"
          style={[
            {
              width: '100%',
              borderTopLeftRadius: 4,
              borderTopRightRadius: 4,
            },
            imageStyle,
          ]}
        />
      )}
      <TouchableOpacity
        style={[
          Styles.container,
          containerStyles,
          !!img_url ? { borderTopLeftRadius: 0, borderTopRightRadius: 0 } : {},
          { backgroundColor: `${priority?.color}dd` || 'pink' },
        ]}
        onLongPress={(event) =>
          onLongPress && onLongPress(event.nativeEvent.pageY)
        }
        onPress={() => {
          if (onItemPress) {
            onItemPress(id);
          } else {
            navigation.navigate('TodoDetails', { id });
          }
        }}
      >
        {!showPriority ? (
          <>
            <View style={Styles.headerWrapper}>
              <AppText style={[Styles.text]} numberOfLines={1}>
                {todo_name}
              </AppText>
            </View>
            <View style={Styles.contentWrapper}>
              {!!priority?.priority_name &&
                priority?.priority_name?.length > 0 && (
                  <View style={Styles.textTeam}>
                    <AppText
                      numberOfLines={1}
                      style={{ color: theme.color.white }}
                    >
                      {result ? department || 'OKR' : 'Cá nhân'}
                    </AppText>
                  </View>
                )}
              {/* <Flag color={priority?.color} /> */}
              <Feather
                name="flag"
                size={20}
                // color={priority?.color}
                color="red"
              />
              <AppText style={{ marginRight: theme.spacing.p8 }}>
                {priority?.priority_level}
              </AppText>
              <>
                <Comment />
              </>
              <AppText style={Styles.comment}>{comment_count}</AppText>
              {/* <FontAwesome5
                name="tasks"
                size={20}
                color={theme.color.mineShaft}
              /> */}
              <Task />
              <AppText style={Styles.checkList}>
                {isDone.length}/{check_list.length}
              </AppText>
              <ListAvatar listAvatar={raic_user} />
            </View>
          </>
        ) : (
          <View style={{ flexDirection: 'column' }}>
            <AppText
              style={{ marginBottom: theme.spacing.p4 }}
              numberOfLines={1}
            >
              {todo_name}
            </AppText>
            <ListAvatar listAvatar={raic_user} />
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
});

export default (props: Props) => {
  const {
    containerStyles,
    data = [],
    onPress,
    title,
    headerTodoStyle,
    showMenu,
    imageStyle,
    containerTodo,
    textStyles,
    showPriority,
    onItemPress,
  } = props;

  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);

  const { t } = useTranslation();
  const renderItemsTodo = () => (
    <FlatList
      showsVerticalScrollIndicator={false}
      contentContainerStyle={{ marginTop: 2 }}
      style={{ flexGrow: 1 }}
      data={data}
      renderItem={({ item, index }) => {
        return (
          <TodoItemComponent
            data={item}
            containerStyles={containerStyles}
            imageStyle={imageStyle}
            showPriority={showPriority}
            onItemPress={onItemPress}
          />
        );
      }}
      keyExtractor={(item, index) => index.toString()}
    />
  );
  const renderHeader = () => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: theme.spacing.p12,
      }}
    >
      <AppText style={[Styles.textTodo, textStyles]}>{title}</AppText>
      {!showMenu && (
        <TouchableOpacity onPress={onPress}>
          <Feather
            name="more-horizontal"
            size={24}
            color={`${theme.color.blackTitle}50`}
          />
        </TouchableOpacity>
      )}
    </View>
  );

  return (
    <>
      <View
        style={[
          Styles.containerTodo,
          headerTodoStyle,
          data?.length === 0 ? { paddingBottom: 0 } : {},
          containerTodo,
        ]}
      >
        {renderHeader()}
        {renderItemsTodo()}
      </View>
    </>
  );
};
