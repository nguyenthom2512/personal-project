import { StyleSheet } from 'react-native';
import { fontSize, padding } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      borderRadius: 4,
      paddingHorizontal: padding.p8,
      paddingVertical: padding.p8,
      marginBottom: 8,
    },
    text: {
      fontSize: fontSize.f14,
      flex: 1,
    },
    restAvatar: {
      borderRadius: 20,
      padding: 3.5,
      backgroundColor: theme.color.violet,
      marginLeft: -4,
      borderColor: theme.color.white,
      borderWidth: 2,
    },
    number: {
      color: theme.color.white,
      fontSize: fontSize.f12,
    },
    listAvatar: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    textTeam: {
      alignItems: 'center',
      width: '30%',
      backgroundColor: theme.color.blackBoder,
      borderRadius: 2,
      padding: theme.spacing.p4,
      marginRight:theme.spacing.p12
    },
    textTodo: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f18,
      fontWeight: 'bold',
      marginBottom: 12,
    },
    containerTodo: {
      backgroundColor: theme.color.white,
      paddingVertical: theme.spacing.p16,
      borderRadius: 4,
      // zIndex: 4
      // flex: 1
    },
    department: {
      backgroundColor: theme.color.blackBoder,
      borderRadius: 2,
      padding: theme.spacing.p4,
      marginRight: theme.spacing.p36,
      alignItems: 'center',
      marginTop: theme.spacing.p8,
    },
    comment: {
      marginLeft: 4,
      marginRight: theme.spacing.p12,
    },
    headerWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    contentWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: theme.spacing.p4,
    },
    checkList:{
      marginHorizontal: theme.spacing.p4
    }
  });
  return styles;
};
