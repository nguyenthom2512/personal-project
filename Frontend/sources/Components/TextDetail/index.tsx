/* eslint-disable react-hooks/exhaustive-deps */
import { useNavigation } from '@react-navigation/native';
import React from 'react';
import { View, ViewStyle } from 'react-native';
import { useTheme, useThemeAwareObject } from '../../Theme';
import { AppText } from '../index';
import styles from './styles';


interface Props {
    textKey: any;
    value: any;
    color?: any;
    containerStyle?: ViewStyle;
    style?: any;
}

export default (props: Props) => {

    const { textKey, value, color, containerStyle, style } = props;
    const Styles = useThemeAwareObject(styles);
    const { theme } = useTheme();

    const navigation = useNavigation();


    return (
        <View style={{ flexDirection: "row", marginVertical: 8 }}>
            <AppText style={Styles.textKey}>{textKey}</AppText>
            {color
                ?
                < View style={[Styles.flex3, containerStyle]}>
                    <AppText style={[Styles.value1, style]}>
                        {value}
                    </AppText>
                </View>
                :
                <AppText style={[Styles.value2, style]}>{value}</AppText>
            }
        </View >
    )
};
