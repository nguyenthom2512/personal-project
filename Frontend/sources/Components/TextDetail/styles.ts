import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flex1: { flex: 1 },
    flex3: { flex: 3 },
    textKey: {
      flex: 1,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.blackTitle,
      opacity: 0.5,
    },
    value1: {
      width: '30%',
      padding: theme.spacing.p6,
      borderRadius: 4,
      textAlign: 'center',
      color: theme.color.white,
      fontSize: theme.fontSize.f14,
    },
    value2: {
      flex: 3,
      color: theme.color.blackTitle,
    },
  });
  return styles;
};
