import React from 'react';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import { CheckBox, Icon, CheckBoxProps } from 'react-native-elements';
import { useTheme } from '../../Theme';

interface IAppRadio extends CheckBoxProps {
  title?:
    | string
    | React.ReactElement<
        {},
        | string
        | ((
            props: any,
          ) => React.ReactElement<
            any,
            string | any | (new (props: any) => React.Component<any, any, any>)
          >)
        | (new (props: any) => React.Component<any, any, any>)
      >;
}

const AppRadio = (props: IAppRadio) => {
  const { theme } = useTheme();
  return (
    <CheckBox
      {...props}
      checkedIcon={
        <Icon
          name="radio-button-checked"
          type="material"
          color={theme.color.violet}
          size={20}
          tvParallaxProperties
        />
      }
      uncheckedIcon={
        <Icon
          name="radio-button-unchecked"
          type="material"
          color={theme.color.violet}
          size={20}
          tvParallaxProperties
        />
      }
      disabled={props.disabled}
    />
  );
};

export default AppRadio;
