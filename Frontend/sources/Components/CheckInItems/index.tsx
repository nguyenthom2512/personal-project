import { useNavigation } from '@react-navigation/core';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { TouchableOpacity, View } from 'react-native';
import Collapsible from 'react-native-collapsible';
import { Icon } from 'react-native-elements';
import { Avatar } from 'react-native-elements/dist/avatar/Avatar';
import Modal from 'react-native-modal';
import Feather from 'react-native-vector-icons/Feather';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppButton from '../AppButton';
import AppText from '../AppText';
import styles from './styles';

interface Props {
  data: DataCheckIn;
  toDetails: (isDone?: boolean) => void;
  onModal: () => void;
}
interface DataCheckIn {
  id?: any;
  object_name?: string;
  okr_result?: any[];
  percent_completed?: any;
  percent_changed?: any;
  user?: any;
  confident: -1 | 0 | 1 | 2;
  last_checkin: {
    checkin_status: -1 | 0 | 1 | 2;
  };
}
const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';

export default (props: Props) => {
  let isDone = false;
  const { data: items, toDetails = () => {}, onModal } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const navigation = useNavigation();
  const [activeSections, setActiveSections] = useState<string[]>([]);
  const { t } = useTranslation();
  const Confident = {
    '-1': t('checkIn:notYetCheckIn'),
    '0': t('checkIn:bad'),
    '1': t('checkIn:normal'),
    '2': t('checkIn:good'),
  };
  const CheckIn = {
    '-1': t('checkIn:notYetCheckIn'),
    '0': t('checkIn:outOfDate'),
    '1': t('checkIn:lateCheckIn'),
    '2': t('checkIn:onTime'),
  };
  const {
    id,
    object_name,
    okr_result = [],
    percent_completed = 0,
    percent_changed = 0,
    user,
    confident,
    last_checkin,
  } = items;
  const percent = (value: any) => {
    if (value <= 0.25) {
      return theme.color.cinnabar;
    }
    if (value <= 0.75) {
      return theme.color.fireBush;
    }
    return theme.color.greenBox;
  };
  const changeColor = (el: any) => {
    switch (el) {
      case 2:
        return theme.color.greenBox;
      case 0:
        return theme.color.cinnabar;
      case 1:
        return theme.color.blueCerulean;
      default:
        return theme.color.fireBush;
    }
  };
  const changeColorStatus = (item: any) => {
    if (item.is_done) {
      return theme.color.greenBox;
    } else {
      switch (item?.last_checkin?.checkin_status) {
        case 0:
          return theme.color.fireBush;
        case 1:
          return theme.color.blueCerulean;
        case -1:
          return theme.color.cinnabar;
        default:
          // return theme.color.greenBox;
          return theme.color.cinnabar;
      }
    }
  };
  const menu = [
    {
      label: t('checkIn:results'),
      type: 'mainResult',
    },
    {
      label: t('checkIn:progress'),
      type: 'progress',
    },
    {
      label: t('checkIn:change'),
      type: 'change',
    },
    {
      label: t('checkIn:staff'),
      type: 'staff',
    },
    {
      label: t('checkIn:confident'),
      type: 'confident',
    },
    {
      label: t('checkIn:status'),
      type: 'status',
    },
  ];
  const changeCollapseStatus = (key: any) => {
    const newArray = [...activeSections];
    if (activeSections.includes(key)) {
      const indexKey = activeSections.findIndex((el) => el === key);
      newArray.splice(indexKey, 1);
      setActiveSections(newArray);
    } else {
      newArray.push(key);
      setActiveSections(newArray);
    }
  };

  const renderCheckinInfo = (el: any, item: any) => {
    return (
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          width: '100%',
          justifyContent: 'space-between',
        }}
      >
        <View style={{ flex: 1 }}>
          <AppText style={Styles.menu}>{item.label}</AppText>
        </View>
        <View style={{ flex: 2, alignItems: 'flex-start', paddingLeft: 24 }}>
          {contentByType(item.type, el)}
        </View>
      </View>
    );
  };

  const handleChooseRoom = (el: any) => {
    if (el.is_done) {
      toDetails(isDone);
    } else {
      setVisibleModal(true);
      setTextOkr(el.object_name);
    }
  };

  const checkInStatus = (item: any) => {
    let statusText = '';
    if (item.is_done) {
      isDone = true;
      return (statusText = 'Đã hoàn thành');
      // return status('Đã check in');
    } else {
      switch (item?.last_checkin?.checkin_status) {
        case 1:
          statusText = 'Check in đúng hạn';
          break;
        // return status('Check in đúng hạn');
        case 0:
          statusText = 'Check in muộn';
          break;
        // return status('Check in muộn');
        case -1:
          statusText = 'Chưa check in';
          break;
        // return status('Chưa check in');
        default:
          statusText = 'Chưa check in';
          break;
        // return status('Chưa check in');
      }
    }
    return statusText;
  };
  const contentByType = (type: string, el: any) => {
    switch (type) {
      case 'mainResult':
        return (
          <AppButton
            title={okr_result?.length + ' kết quả'}
            cancel
            onPress={() => {
              navigation.navigate('KrScreen', {
                param: el,
                refetchGetOKR: () => {},
                type: 'checkin',
              });
            }}
          />
        );
      case 'progress':
        return (
          // <AppButton
          //   title={`${(percent_completed * 100).toFixed(2)}%`}
          //   onPress={() => {}}
          //   style={{ backgroundColor: percent(percent_completed) }}
          // />
          <TouchableOpacity
            style={[
              {
                backgroundColor: percent(percent_completed),
              },
              Styles.progressButton,
            ]}
          >
            <AppText style={Styles.textProgress}>{`${(
              percent_completed * 100
            ).toFixed(2)}%`}</AppText>
          </TouchableOpacity>
        );
      case 'change':
        return <AppText>{`${(percent_changed * 100).toFixed(2)}%`}</AppText>;
      case 'staff':
        return (
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Avatar
              rounded
              source={{
                uri: user?.img_url
                  ? `${serviceUrls.url.IMAGE}${user?.img_url}`
                  : DEFAULT_AVATAR,
              }}
            />
            <AppText style={{ marginLeft: theme.spacing.p8 }}>
              {user?.full_name}
            </AppText>
          </View>
        );
      case 'confident':
        return (
          <AppText
            style={[{ color: changeColor(confident) }, Styles.confident]}
          >
            {Confident?.[confident] || Confident?.['-1']}
          </AppText>
        );
      case 'status':
        return (
          <TouchableOpacity
            onPress={() => {
              handleChooseRoom(el);
            }}
            style={[Styles.status, { borderColor: changeColorStatus(el) }]}
          >
            <AppText style={{ color: changeColorStatus(el) }}>
              {checkInStatus(el)}
            </AppText>
          </TouchableOpacity>
        );
      default:
        break;
    }
  };
  const [visibleModal, setVisibleModal] = useState(false);
  const [textOkr, setTextOkr] = useState('');
  return (
    <View>
      <View style={Styles.collapseContainer}>
        <TouchableOpacity
          onPress={() => changeCollapseStatus(id)}
          style={Styles.headerCollapse}
        >
          <View style={{ flex: 1 }}>
            <AppText numberOfLines={2}>{object_name}</AppText>
          </View>
          <Feather name="chevron-down" size={28} />
        </TouchableOpacity>
        <Collapsible
          collapsed={!activeSections.includes(id)}
          style={Styles.collapse}
        >
          <View style={{ marginBottom: theme.spacing.p16 }}>
            {menu?.map((elm, index) => (
              <View
                key={index.toString()}
                style={{
                  flexDirection: 'row',
                  paddingTop: theme.spacing.p16,
                }}
              >
                {renderCheckinInfo(items, elm)}
              </View>
            ))}
          </View>
        </Collapsible>
      </View>
      <Modal isVisible={visibleModal}>
        <View style={Styles.wrapModal}>
          <View style={Styles.contentModal}>
            <View style={Styles.iconStyle}>
              <View style={{ flex: 1 }}>
                <AppText style={Styles.titleModal} numberOfLines={2}>
                  {textOkr}
                </AppText>
              </View>
              <Icon
                name="x"
                type="feather"
                color={theme.color.gray}
                onPress={() => {
                  setVisibleModal(false);
                }}
                tvParallaxProperties
                // style={{  }}
              />
            </View>
            <View style={Styles.wrapButton} />
            <View style={{ alignItems: 'center' }}>
              <AppText style={Styles.askStyle}>{t('checkIn:askRoom')}</AppText>
            </View>
            <View style={Styles.contentButton}>
              <TouchableOpacity
                onPress={() => {
                  setVisibleModal(false);
                  toDetails(isDone);
                }}
                style={Styles.outLinebuttonModal}
              >
                <AppText style={{ color: theme.color.violet }}>CheckIn</AppText>
              </TouchableOpacity>
              <View>
                <TouchableOpacity
                  onPress={() => {
                    setVisibleModal(false);
                    onModal();
                  }}
                  style={Styles.buttonModal}
                >
                  <AppText style={{ color: theme.color.white }}>
                    {t('room:createRoom')}
                  </AppText>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};
