import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    collapse: {
      paddingHorizontal: theme.spacing.p16,
      borderTopWidth: 1,
      borderTopColor: theme.color.mercury,
      marginBottom: theme.spacing.p16,
      flexDirection: 'row',
    },
    menu: {
      color: theme.color.mineShaft,
    },
    collapseContainer: {
      marginBottom: theme.spacing.p12,
      backgroundColor: theme.color.white,
    },
    headerCollapse: {
      flexDirection: 'row',
      paddingHorizontal: 16,
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: theme.spacing.p12,
      flex: 1,
    },
    confident: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    status: {
      borderWidth: 1,
      padding: 8,
      borderRadius: 4,
    },
    wrapModal: {
      flex: 1,
      justifyContent: 'center',
    },
    contentModal: {
      backgroundColor: theme.color.backgroundColor,
      borderRadius: theme.spacing.p12,
    },
    iconStyle: {
      //  alignItems: 'flex-end',
      margin: theme.spacing.p12,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    titleModal: {
      fontSize: theme.fontSize.f18,
      fontWeight: 'bold',
      // width: '70%',
    },
    buttonModal: {
      marginVertical: theme.spacing.p8,
      marginRight: theme.spacing.p16,
      borderRadius: theme.spacing.p6,
      padding: theme.spacing.p12,
      backgroundColor: theme.color.violet,
    },
    askStyle: {
      textAlign: 'right',
      marginRight: theme.spacing.p16,
      marginVertical: theme.spacing.p8,
    },
    outLinebuttonModal: {
      marginVertical: theme.spacing.p8,
      marginHorizontal: theme.spacing.p16,
      borderRadius: theme.spacing.p6,
      borderWidth: 1,
      borderColor: theme.color.violet,
      padding: theme.spacing.p12,
    },
    contentButton: {
      flexDirection: 'row',
      // justifyContent: 'flex-end',
      alignItems:'center',
      justifyContent:'center'
    },
    wrapButton: {
      height: 0,
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    progressButton:{
      alignSelf:'flex-start',
      padding: theme.spacing.p8,
      borderRadius: theme.spacing.p4,
    },
    textProgress:{
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color:theme.color.white
    }
  });
  return styles;
};
