import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { responsivePixel } from '../../Helpers';
import { Theme, useTheme, useThemeAwareObject } from '../../Theme';

interface AppFlatlistFooterProps {
  isVisible: boolean;
}
export default (props: AppFlatlistFooterProps) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  if (!props.isVisible) {
    return null;
  }
  return (
    <View style={Styles.container}>
      <ActivityIndicator color={theme.color.violet} />
    </View>
  );
};

const styles = (theme: Theme) => {
  const style = StyleSheet.create({
    container: {
      height: responsivePixel(36),
      // flex: 1,
      alignSelf: 'center',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  return style;
};
