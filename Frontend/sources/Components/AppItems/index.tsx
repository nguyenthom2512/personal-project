import React from 'react';
import { TouchableOpacity, ViewStyle } from 'react-native';
import { useThemeAwareObject } from '../../Theme';
import { AppText } from '../index';
import styles from './styles';

interface Props {
  containerStyles?: ViewStyle;
  icons?: any;
  number?: number;
  onNavigation?: () => void;
}

export default (props: Props) => {
  const { containerStyles, icons, number,onNavigation } = props;
  const Styles = useThemeAwareObject(styles);
  return (
    <TouchableOpacity style={[Styles.container, containerStyles]} onPress={onNavigation}>
      <>{icons}</>
      <AppText style={Styles.textItems} numberOfLines={1}>{number}</AppText>
    </TouchableOpacity>
  );
};
