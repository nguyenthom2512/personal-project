import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      borderRadius: 4,
      padding: theme.spacing.p12,
      alignItems: 'center',
      flexDirection: 'row',
    },
    textItems: {
      color: theme.color.white,
      fontSize: theme.fontSize.f18,
      fontWeight: 'bold',
      marginLeft:theme.spacing.p12
    },
  });
  return styles;
};
