import { Platform, StyleSheet } from 'react-native';
import { fontSize, measure, padding } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      paddingVertical: theme.spacing.p4,
      width: '100%',
    },
    wrapInput: {
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.color.holderPlace,
      alignItems: 'center',
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p16,
      backgroundColor: theme.color.backgroundColor,
      padding: Platform.OS == 'ios' ? theme.spacing.p12 : 0
    },
    input: {
      flex: 1,
      color: theme.color.black,
      borderRadius: 4,
      fontSize: theme.fontSize.f16,
      backgroundColor: theme.color.backgroundColor,
      marginLeft: theme.spacing.p12,
    },
    errorText: {
      color: theme.color.torchRed,
    },
    labelStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    txtTitle: {
      color: theme.color.grayAbbey,
      fontSize: fontSize.f12,
      fontWeight: 'bold',
      paddingBottom: theme.spacing.p2,
      marginRight: theme.spacing.p4,
    },
    require: {
      fontSize: theme.fontSize.f16,
      color: theme.color.redAlert,
    },
  });
