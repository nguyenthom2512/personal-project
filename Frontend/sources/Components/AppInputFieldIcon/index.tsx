import get from 'lodash/get';
import React, { useCallback } from 'react';
import {
  KeyboardTypeOptions,
  TextInput,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import { measure } from '../../Helpers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import { ErrorMessages, AppField, AppText } from '../index';
import styles from './styles';
interface Props {
  name: any;
  form?: any;
  field?: any;
  onChangeCustom?: (text: string) => void;
  placeholder?: any;
  memo?: boolean;
  customInputStyle?: TextStyle;
  containerStyle?: ViewStyle;
  keyboardType?: KeyboardTypeOptions | undefined;
  title?: string;
  Info?: any;
  secureTextEntry?: boolean;
  editable?: boolean;
  multiline?: boolean;
  required?: boolean;
  icon?: any;
  customValue?: string;
  iconRight?:any;
  wrapInputStyles?: ViewStyle
}

const Input = (props: Props) => {
  const {
    form,
    field,
    onChangeCustom,
    placeholder,
    customInputStyle,
    keyboardType = 'default',
    secureTextEntry = false,
    containerStyle,
    editable = true,
    multiline = false,
    customValue,
    wrapInputStyles
  } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { name, value } = field;
  const { errors, setFieldValue } = form;

  const error = get(errors, name);

  const onChange = useCallback(
    (text) => {
      setFieldValue(name, text);
    },
    [name, setFieldValue],
  );

  return (
    <View style={[Styles.container, containerStyle]}>
      {props.title && (
        <View style={Styles.labelStyle}>
          <AppText style={Styles.txtTitle}>
            {props.title}
            {props.required && <AppText style={Styles.require}> * </AppText>}
          </AppText>
          <View>{props.Info}</View>
        </View>
      )}
      <View
        style={[Styles.wrapInput, wrapInputStyles ]}
      >
        <TouchableOpacity>{props.icon}</TouchableOpacity>
        <TextInput
          onChangeText={onChangeCustom || onChange}
          placeholder={placeholder || ''}
          value={customValue || value}
          placeholderTextColor={theme.color.gray}
          style={[Styles.input, customInputStyle]}
          keyboardType={keyboardType}
          secureTextEntry={secureTextEntry}
          autoCapitalize="none"
          editable={editable}
          multiline={multiline}
          textAlignVertical={multiline ? 'top' : 'auto'}
        />
        {props.iconRight}
      </View>
      <ErrorMessages name={name} />
    </View>
  );
};

export default (props: Props) => {
  const { memo, name, ...remainProps } = props;
  if (memo) {
    return <AppField name={name} component={Input} {...remainProps} />;
  }
  return <AppField name={name} component={Input} {...remainProps} />;
};
