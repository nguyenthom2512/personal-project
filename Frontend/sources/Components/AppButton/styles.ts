import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    defaultTitle: {
      fontWeight: '600',
      fontSize: 16,
      color: theme.color.white,
    },
    defaultOutline: {
      fontWeight: '600',
      fontSize: 16,
      color: theme.color.violet,
    },
    OutlineButton: {
      height: 48,
      width: '100%',
      borderColor: theme.color.violet,
    },
    defaultStyle: {
      height: 48,
      width: '100%',
      backgroundColor: theme.color.violet,
    },
  });
  return styles;
};
