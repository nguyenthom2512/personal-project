import React from 'react';
import {
  StyleProp,
  TextStyle,
  TouchableOpacity,
  ViewStyle,
} from 'react-native';
import styles from './styles';
import { Button } from 'react-native-elements';
import { useThemeAwareObject } from '../../Theme';

interface AppButtonProps {
  children?: any;
  title?: any;
  textStyle?: TextStyle;
  transparent?: boolean;
  style?: StyleProp<ViewStyle>;
  containerStyle?: StyleProp<ViewStyle>;
  cancel?: boolean;
  onPress: () => void;
  disabled?: boolean;
  loading?: boolean;
}

const AppButton = (props: AppButtonProps) => {
  const Styles = useThemeAwareObject(styles);
  return (
    <Button
      {...props}
      buttonStyle={
        !props.cancel
          ? [Styles.defaultStyle, props.style]
          : [Styles.OutlineButton, props.style]
      }
      title={props.title}
      type={!props.cancel ? 'solid' : 'outline'}
      titleStyle={
        !props.cancel
          ? [Styles.defaultTitle, props.textStyle]
          : [Styles.defaultOutline, props.textStyle]
      }
      onPress={props.onPress}
      TouchableComponent={TouchableOpacity}
      disabled={props.loading || props.disabled}
      disabledStyle={
        !props.cancel
          ? [Styles.defaultStyle, props.style]
          : [Styles.OutlineButton, props.style]
      }
      disabledTitleStyle={
        !props.cancel
          ? [Styles.defaultTitle, props.textStyle]
          : [Styles.defaultOutline, props.textStyle]
      }
      loading={props.loading}
    />
  );
};
export default AppButton;
