import { StyleSheet } from 'react-native';
import { fontSize, padding } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
      listAvatar: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  number: {
    color: theme.color.white,
    fontSize: fontSize.f12,
  },
  restAvatar: {
    borderRadius: 20,
    padding: 3.5,
    backgroundColor: theme.color.violet,
    marginLeft: -4,
    borderColor: theme.color.white,
    borderWidth: 2,
  },
});
return styles
}