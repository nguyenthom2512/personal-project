import React from 'react';
import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { Avatar } from 'react-native-elements';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppText from '../AppText';
import styles from './styles';

interface Props{
    listAvatar: any[];
};
const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';

export default (props: Props) =>{
    const {listAvatar} = props;
    const {t} = useTranslation();
    const {theme} = useTheme();
    const Styles = useThemeAwareObject(styles)
    return(
        <View
        style={[
          Styles.listAvatar,
          { marginRight: listAvatar?.length === 0 ? 0 : theme.spacing.p12 },
        ]}
      >
        {listAvatar.slice(0, 3).map((item, index) => {
          if (listAvatar.length > 2 && index === 2) {
            return (
              <View
                key={index.toString()}
                style={[
                  {
                    zIndex: index,
                  },
                  Styles.restAvatar,
                ]}
              >
                <AppText style={Styles.number}>
                  +{listAvatar.length - 2}
                </AppText>
              </View>
            );
          }
          return (
            <Avatar
              key={index.toString()}
              size={24}
              rounded
              title=""
              activeOpacity={0.7}
              source={{
                uri: item?.user_data?.img_url
                  ? `${serviceUrls.url.IMAGE}${item?.user_data?.img_url}`
                  : DEFAULT_AVATAR,
              }}
              containerStyle={{
                zIndex: index,
                marginLeft: index === 0 ? 0 : -4,
                borderColor: theme.color.white,
                borderWidth: 2,
              }}
            />
          );
        })}
      </View>
    )
}