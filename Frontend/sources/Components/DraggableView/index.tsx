import React, { useState } from "react";
import { Animated, PanResponder, StyleSheet, View } from "react-native";
interface Props {
  maxX?: number;
  onDrop: (value: number) => void;
  onDrag?: (value: boolean) => void;
  onReset?: () => void;
  width: number;
  minY?: number;
  maxY?: number;
  space?: number;
  children?: any;
  minX?: number;
  maxRangeY?: number;
}
const DraggableView = ({ maxX = 9999, minX = 0, onDrop, width, minY = 9999, maxY = 9999, maxRangeY, space = 16, onReset, children: children, onDrag }: Props) => {
  const [pan,] = useState(new Animated.ValueXY({ x: 0, y: 0 }));
  const [isShowDraggable, setShowDraggable] = useState(true);
  const [opacity,] = useState(new Animated.Value(1));
  const [rotate,] = useState(new Animated.Value(0));
  const [locationY, setLocationY] = useState(0);


  const panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: () => true,
    onMoveShouldSetPanResponderCapture: () => false,
    onPanResponderTerminationRequest: () => false,
    onStartShouldSetPanResponder: () => true,
    onPanResponderGrant: (e, gesture) => {
      if (locationY == 0 && gesture.y0 > 0) {
        setLocationY(gesture.y0);
      }
      onDrag && onDrag(false);
      Animated.spring(
        rotate,
        {
          toValue: 1,
          useNativeDriver: false
        }
      ).start();
      Animated.spring(
        opacity,
        {
          toValue: 0.8,
          useNativeDriver: false
        }
      ).start();
      pan.setOffset({
        x: 0,
        y: 0,
      });
      pan.setValue({ x: 0, y: 0 })
    },
    onPanResponderMove: (e, gesture) => {

      const isMinXRange = gesture.dx < minX;
      const isMaxXRange = gesture.dx >= maxX;
      const isMaxYRange = gesture.moveY >= maxY;
      const isMinYRange = gesture.dy <= -(locationY - minY);
      let newValue = { x: gesture.dx, y: gesture.dy };

      if (isMinYRange) {
        newValue = {
          x: gesture.dx,
          y: -(locationY - minY)
        };
      };
      if (isMaxYRange) {
        newValue = {
          x: gesture.dx,
          y: maxY - locationY
        };
      };
      if (isMaxXRange) {
        newValue = {
          x: maxX,
          y: gesture.dy
        }
      };
      if (isMinXRange) {
        newValue = {
          x: minX ?? 0,
          y: gesture.dy
        }
      };
      if (isMaxYRange && isMinXRange) {
        newValue = {
          x: minX ?? 0,
          y: maxY - locationY
        };
      };
      if (isMinYRange && isMinXRange) {
        newValue = {
          x: minX ?? 0,
          y: -(locationY - minY)
        };
      };
      if (isMaxYRange && isMaxXRange) {
        newValue = {
          x: maxX,
          y: maxY - locationY
        };
      };
      if (isMinYRange && isMaxXRange) {
        newValue = {
          x: maxX,
          y: -(locationY - minY)
        };
      };

      pan.setValue(newValue);
    },
    onPanResponderRelease: (e, gesture) => {
      onDrag && onDrag(true);
      Animated.timing(
        rotate,
        {
          toValue: 0,
          duration: 500,
          useNativeDriver: false
        }
      ).start();

      if (gesture.dx > width + space) {
        const distance = gesture.moveY - minY;
        onDrop(distance);
        Animated.timing(opacity, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: false
        }).start(() => {
          setShowDraggable(false);
        }
        );
      } else if (maxRangeY && (e.nativeEvent.pageY <= maxRangeY && e.nativeEvent.pageY >= minY)) {
        const distance = e.nativeEvent.pageY - minY;        
        onDrop(distance);
        Animated.timing(opacity, {
          toValue: 0,
          duration: 1000,
          useNativeDriver: false
        }).start(() => {
          setShowDraggable(false);
        })
      } else {
        Animated.timing(opacity, {
          toValue: 1,
          duration: 1000,
          useNativeDriver: false
        }).start();
        Animated.spring(
          pan,
          {
            toValue: { x: 0, y: 0 },
            useNativeDriver: false,
            friction: 5
          },
        ).start();
        onReset && onReset();
      }
      pan.flattenOffset();
    },
  });
  const spin = rotate.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '4deg']
  })
  if (isShowDraggable) {
    return (
      <Animated.View
        {...panResponder.panHandlers}
        style={[
          pan.getLayout(),
          styles.box,
          {
            width: width,
            opacity: opacity,
            transform: [{ rotate: spin }],
            marginLeft: space
          }
        ]}
      >
        {children}
      </Animated.View>
    );
  } else {
    return <View />
  }

};

const styles = StyleSheet.create({
  box: {
    marginTop: 8,
  },
});

export default DraggableView;