import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

const createStyles = (theme: Theme) => {
  const styles = StyleSheet.create({
    root: {
      alignItems: 'center',
      backgroundColor: theme.color.backgroundColor,
      justifyContent: 'center',
      marginTop: theme.spacing.p2,
      padding: theme.spacing.p2,
    },
    infoTextWrapper: {
      flexDirection: 'row',
      marginBottom: theme.spacing.p2,
    },
    infoText: {
      color: theme.color.torchRed,
      fontSize: 14,
    },
    infoTextBold: {
      fontWeight: 'bold',
    },
    buttonsWrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginTop: theme.spacing.p2,
    },
    button: {
      backgroundColor: theme.color.primary,
      borderRadius: 4,
      flex: 1,
      justifyContent: 'center',
      marginHorizontal: theme.spacing.p12,
      padding: theme.spacing.p2,
    },
    buttonText: {
      color: theme.color.redMonza,
      fontSize: 14,
      fontWeight: 'bold',
      textAlign: 'center',
    },
  });
  return styles;
};

export default createStyles;
