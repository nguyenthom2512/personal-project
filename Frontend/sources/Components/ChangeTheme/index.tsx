import React from 'react';
import {
  StyleProp,
  Text,
  TextStyle,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  DEFAULT_DARK_THEME,
  DEFAULT_LIGHT_THEME,
  useTheme,
  useThemeAwareObject,
} from '../../Theme';
import createStyles from './styles';

export const ChangeTheme = React.memo(() => {
  const Styles = useThemeAwareObject(createStyles);
  const { theme, setTheme, toggleTheme } = useTheme();

  const SetLightThemeButtonElement = React.useMemo(() => {
    return (
      <TouchableOpacity
        onPress={() => setTheme(DEFAULT_LIGHT_THEME)}
        activeOpacity={0.75}
        style={Styles.button}
      >
        <Text style={Styles.buttonText}>{'Set light Theme!'}</Text>
      </TouchableOpacity>
    );
  }, [setTheme, Styles]);

  const SetDarkThemeButtonElement = React.useMemo(() => {
    return (
      <TouchableOpacity
        onPress={() => setTheme(DEFAULT_DARK_THEME)}
        activeOpacity={0.75}
        style={Styles.button}
      >
        <Text style={Styles.buttonText}>{'Set dark Theme!'}</Text>
      </TouchableOpacity>
    );
  }, [setTheme, Styles]);

  const InfoTextBoldStyles = React.useMemo<StyleProp<TextStyle>>(() => {
    const infoTextBoldStyles: StyleProp<TextStyle> = [
      Styles.infoText,
      Styles.infoTextBold,
    ];

    return infoTextBoldStyles;
  }, [Styles]);

  return (
    <View style={Styles.root}>
      <View style={Styles.infoTextWrapper}>
        <Text style={Styles.infoText}>{'The current theme is: '}</Text>
        <Text style={InfoTextBoldStyles}>{theme.id}</Text>
      </View>
      <View style={Styles.buttonsWrapper}>
        {SetLightThemeButtonElement}
        {SetDarkThemeButtonElement}
      </View>
    </View>
  );
});
