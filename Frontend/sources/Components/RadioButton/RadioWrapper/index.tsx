import React, { useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { View, ViewStyle } from 'react-native';
import { Theme, useThemeAwareObject } from '../../../Theme';
import Radio from '../Radio';

interface Props {
  name: any;
  contentStyle?: ViewStyle;
  onPress?: () => void;
  checkBoxRight?: boolean;
  children: any;
  containerStyle?: ViewStyle;
}

export default (props: Props) => {
  const Styles = useThemeAwareObject(styles);
  return (
    <View style={[props.containerStyle, Styles.container]}>
      <View style={props.contentStyle}>{props.children}</View>
      <Radio name={props.name} />
    </View>
  );
};

const styles = (theme: Theme) =>
  StyleSheet.create({
    container: {
      width: '100%',
      justifyContent: 'space-between',
      flexDirection: 'row',
      alignItems: 'center',
    },
  });
