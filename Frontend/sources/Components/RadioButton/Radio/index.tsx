import React, { useState } from 'react';
import { ViewStyle } from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './styles';

interface Props {
  name?: any;
  customInputStyle?: ViewStyle;
  title?: string;
  titleStyle?: ViewStyle;
  onPress?: () => void;
  checkBoxRight?: boolean;
  checked?: boolean;
  disabled?: boolean;
  color?: string;
}

export default (props: Props) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const [checked, setChecked] = useState(props.checked);
  const onPress = () => {
    setChecked(!checked);
    props.onPress && props.onPress();
  };
  return (
    <CheckBox
      containerStyle={Styles.containerStyle}
      checked={checked}
      onPress={onPress}
      disabled={props.disabled}
      iconRight
      textStyle={{}}
      title={props.title}
      checkedIcon={
        <Icon
          name="radio-button-checked"
          type="material"
          color={props.color ? props.color : theme.color.violet}
          tvParallaxProperties
          size={20}
        />
      }
      uncheckedIcon={
        <Icon
          name="radio-button-unchecked"
          type="material"
          color={props.color ? props.color : theme.color.violet}
          tvParallaxProperties
          size={20}
        />
      }
    />
  );
};
