import { StyleSheet } from 'react-native';
import { measure, responsivePixel, statusBar } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      width: '100%',
    },
    content: {
      height: measure.selectHeight,
      alignItems: 'center',
      width: '100%',
      justifyContent: 'space-between',
      flexDirection: 'row',
      borderRadius: 4,
      borderWidth: 1,
      borderColor: theme.color.Iron,
      paddingHorizontal: theme.spacing.p12,
      backgroundColor: theme.color.backgroundColor,
      marginVertical: theme.spacing.p6,
    },
    txtTitle: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      marginRight: theme.spacing.p4,
    },
    multiSelectBtn: {
      flex: 1,
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      height: measure.inputHeight,
      fontSize: theme.fontSize.f14,
    },
    iconDown: {
      fontSize: theme.fontSize.f24,
      marginLeft: theme.spacing.p4,
      color: theme.color.Iron,
    },
    selectedValue: {
      backgroundColor: theme.color.lightGray,
      minHeight: responsivePixel(42),
      paddingHorizontal: theme.spacing.p12,
      flexDirection: 'row',
      alignItems: 'center',
    },
    selectValue0: {
      minHeight: responsivePixel(42),
      paddingHorizontal: theme.spacing.p12,
      flexDirection: 'row',
      alignItems: 'center',
    },
    selectValue1: {
      minHeight: responsivePixel(42),
      justifyContent: 'center',
      paddingHorizontal: theme.spacing.p12,
    },
    headerWrap: {
      height: responsivePixel(50),
      paddingHorizontal: theme.spacing.p16,
      justifyContent: 'center',
      alignItems: 'flex-start',
      backgroundColor: theme.color.blackHeader,
      borderBottomWidth: 1,
      borderBottomColor: theme.color.grayBoder,
    },
    backButton: {
      paddingTop: statusBar,
    },
    containerModal: {
      justifyContent: 'center',
      backgroundColor: theme.color.blackHeader,
      margin: 0,
    },
    require: {
      fontSize: theme.fontSize.f16,
      color: theme.color.redAlert,
    },
    searchBox: {
      borderBottomWidth: 1,
      borderColor: theme.color.mineShaft,
      minHeight: measure.inputHeight,
      paddingHorizontal: theme.spacing.p12,
      fontSize: theme.fontSize.f16,
    },
    txtValueSelected: {
      maxWidth: '95%',
      fontSize: theme.fontSize.f14,
      color: theme.color.shark,
      opacity: 1,
    },
    placeHolder: {
      opacity: 0.5,
      color: theme.color.blackTitle,
      fontSize: theme.fontSize.f14,
    },
    labelStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingTop: theme.spacing.p18,
      paddingBottom: theme.spacing.p2,
    },
    itemSeparator: {
      width: '100%',
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    removeIcon: {
      fontSize: theme.fontSize.f18,
      marginLeft: theme.spacing.p4,
      color: theme.color.Iron,
    },
    itemLabel: {
      color: theme.color.black,
      fontSize: theme.fontSize.f16,
      flex: 1,
    },
  });
  return styles;
};
