import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ActivityIndicator,
  FlatList,
  StyleProp,
  TextInput,
  TextStyle,
  TouchableOpacity,
  View,
  ViewStyle
} from 'react-native';
import { Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import Feather from 'react-native-vector-icons/Feather';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { appTimer, languageUtils } from '../../Helpers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppRadio from '../AppRadio';
import { AppFlatlistEmpty, AppText } from '../index';
import styles from './styles';

const { stringToSlug } = languageUtils;

interface Item {
  value: any;
  id: any;
}

export interface DataItem {
  label: string;
  value: string | number;
  unit?: any;
  key_result?: any;
  user?: number;
}

export interface DropDownProps {
  data?: Array<DataItem>;
  name?: string;
  title?: string;
  titleDropDown?: string;
  value?: any;
  onChoose?: (item: DataItem) => void;
  disabled?: boolean;
  require?: boolean;
  isSearch?: boolean;
  loading?: boolean;
  customContainer?: StyleProp<ViewStyle>;
  notify?: any;
  customContent?: StyleProp<ViewStyle>;
  placeHolder?: string;
  reload?: boolean;
  titleStyles?: TextStyle;
  onClear?: () => void;
}

export default (props: DropDownProps) => {
  const {
    title,
    onChoose,
    data,
    value: valueDefault,
    isSearch,
    loading,
    placeHolder,
    reload = false,
    titleStyles,
    onClear = () => null,
  } = props;

  const { t } = useTranslation();

  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const [isOpen, setOpen] = useState(false);
  const [value, setValue] = useState(valueDefault ? valueDefault : '');
  const [dataLocal, setDataLocal] = useState(data);

  const [loadingLocal, setLoading] = useState(false);

  const handleData = (itemSelected: DataItem) => () => {
    setOpen(!isOpen);
    setValue(itemSelected.value);
    onChoose && onChoose(itemSelected);
  };

  const getStyle = (itemProps: { item: any; index: number }) => {
    if (itemProps?.item?.value?.toString() === value?.toString()) {
      return Styles.selectedValue;
    }
    return Styles.selectValue0;
  };

  const renderItem = (itemProps: { item: DataItem; index: number }) => {
    return (
      <TouchableOpacity
        onPress={handleData(itemProps.item)}
        style={getStyle(itemProps)}
      >
        <AppText style={Styles.itemLabel}>{itemProps.item.label}</AppText>
        <AppRadio
          checked={itemProps?.item?.value?.toString() === value?.toString()}
          disabled
        />
      </TouchableOpacity>
    );
  };

  const closeModal = () => {
    setOpen(!isOpen);
  };

  useEffect(() => {
    setValue(props.value);
  }, [props.value]);

  useEffect(() => {
    setDataLocal(props.data);
  }, [props.data]);

  let label = '';
  const seletedValue =
    props.data &&
    props.data.filter(
      (elm) => value && elm.value.toString() === value.toString(),
    );
  if (seletedValue && seletedValue.length > 0) {
    label = seletedValue[0].label;
  }

  const deboundSearch = (txt: string) => {
    const newData = (_.cloneDeep(props.data) || []).filter((elm) => {
      const labelItem = stringToSlug(elm.label).toLowerCase();
      const txtInput = stringToSlug(txt).toLowerCase();
      return labelItem.includes(txtInput);
    });
    setDataLocal(newData);
    setLoading(false);
  };

  const onSearch = (txt: string) => {
    setLoading(true);
    appTimer.debounce(() => deboundSearch(txt), 800);
  };

  return (
    <>
      <View style={[Styles.container, props.customContainer]}>
        {title ? (
          <View style={Styles.labelStyle}>
            <AppText
              style={[
                Styles.txtTitle,
                titleStyles,
                { opacity: props.disabled ? 0.5 : 1 },
              ]}
            >
              {title}
              {props.require && <AppText style={Styles.require}> * </AppText>}
            </AppText>
            <View>{props.notify}</View>
          </View>
        ) : (
          <View style={{ alignItems: 'flex-end' }}>{props.notify}</View>
        )}

        <TouchableOpacity
          disabled={props.disabled}
          onPress={() => {
            setOpen(!isOpen), setDataLocal(data);
          }}
          style={{ opacity: props.disabled ? 0.5 : 1 }}
        >
          <View style={[Styles.content, props.customContent]}>
            {!value &&
              (props.placeHolder ? (
                <AppText style={Styles.placeHolder}>
                  {props.placeHolder}
                </AppText>
              ) : (
                <AppText style={Styles.placeHolder}>
                  {t('common:select')}
                </AppText>
              ))}
            <AppText numberOfLines={1} style={Styles.txtValueSelected}>
              {value && label.trim()}
            </AppText>
            <View
              style={[
                Styles.multiSelectBtn,
                { opacity: props.disabled ? 0.5 : 1 },
              ]}
            >
              {reload && (
                <View style={Styles.multiSelectBtn}>
                  <TouchableOpacity
                    onPress={() => {
                      setValue('');
                      if (reload) {
                        onClear();
                      }
                    }}
                    hitSlop={{ top: 20, bottom: 20, left: 50, right: 50 }}
                  >
                    <Feather name="x" style={Styles.removeIcon} />
                  </TouchableOpacity>
                </View>
              )}
              {loading ? (
                <ActivityIndicator size="small" />
              ) : (
                <MIcon name="chevron-down" style={Styles.iconDown} />
              )}
            </View>
          </View>
        </TouchableOpacity>
      </View>
      {isOpen && (
        <Modal
          style={Styles.containerModal}
          isVisible={isOpen}
          onBackdropPress={closeModal}
          animationIn="slideInUp"
          animationOut="slideOutDown"
        >
          <SafeAreaView style={{ flex: 1 }} mode="padding" edges={['top']}>
            <View style={Styles.headerWrap}>
              <Icon
                name="close"
                type="ionicon"
                color={theme.color.graySilver}
                onPress={closeModal}
                tvParallaxProperties
              />
            </View>
            <View style={{ backgroundColor: theme.color.white, flex: 1 }}>
              {isSearch && (
                <TextInput
                  style={Styles.searchBox}
                  placeholder={t('common:search')}
                  onChangeText={onSearch}
                  placeholderTextColor={theme.color.mineShaft}
                />
              )}
              <FlatList
                data={dataLocal}
                refreshing={loadingLocal}
                onRefresh={() => {}}
                renderToHardwareTextureAndroid
                decelerationRate={0.95}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                renderItem={renderItem}
                keyExtractor={(item) => item && item.value.toString()}
                ListEmptyComponent={() => <AppFlatlistEmpty />}
                ItemSeparatorComponent={() => (
                  <View style={Styles.itemSeparator} />
                )}
              />
            </View>
          </SafeAreaView>
        </Modal>
      )}
    </>
  );
};
