import React, { memo, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import CachedImage, { ImageStyle, ResizeMode } from 'react-native-fast-image';
import { Theme, useTheme, useThemeAwareObject } from '../../Theme';
import AppText from '../AppText';
interface IAppImage {
  onError?: any;
  onLoad?: any;
  imageStyle: ImageStyle;
  source: any;
  resizeMode?: ResizeMode;
}
const AppImage = memo(
  ({
    onError,
    onLoad,
    imageStyle,
    source,
    resizeMode,
    ...otherProps
  }: IAppImage) => {
    const [isLoading, setLoading] = useState(true);
    const [isErrored, setIsErrored] = useState(false);
    const { t } = useTranslation();
    const { theme } = useTheme();
    const Styles = useThemeAwareObject(styles);
    const renderPlaceholder = () => {
      return (
        <View style={{ ...imageStyle, ...Styles.placeholder }}>
          <ActivityIndicator size="small" color={theme.color.white} />
        </View>
      );
    };
    const renderErrorImage = () => {
      return (
        <View style={{ ...imageStyle, ...Styles.placeholder }}>
          {/* <AppText style={Styles.errorText}>
            {t('common:failedToLoadImage')}
          </AppText> */}
        </View>
      );
    };

    const CachedImageMemoized = useMemo(() => {
      return (
        <CachedImage
          resizeMode={resizeMode}
          source={source}
          style={[imageStyle, Styles.image]}
          onError={() => {
            setLoading(false);
            setIsErrored(true);
            onError && onError();
          }}
          onLoad={(e) => {
            setLoading(false);
            onLoad && onLoad(e);
          }}
        />
      );
    }, [onError, onLoad, imageStyle, otherProps]);

    return (
      <View style={imageStyle}>
        {CachedImageMemoized}
        {isLoading && renderPlaceholder()}
        {isErrored && renderErrorImage()}
        {/* {renderPlaceholder()}
        {renderErrorImage()} */}
      </View>
    );
  },
);

const styles = (theme: Theme) =>
  StyleSheet.create({
    image: {
      position: 'absolute',
      zIndex: -1,
    },
    placeholder: {
      backgroundColor: `${theme.color.black}90`,
      position: 'absolute',
      top: 0,
      left: 0,
      zIndex: 1,
      justifyContent: 'center',
      textAlign: 'center',
      alignItems: 'center',
    },
    errorText: {
      color: `${theme.color.black}90`,
    },
  });

export default AppImage;
