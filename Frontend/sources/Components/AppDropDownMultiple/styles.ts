import { StyleSheet } from 'react-native';
import { measure, responsivePixel, statusBar } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      paddingVertical: theme.spacing.p4,
      width: '100%',
    },
    content: {
      height: measure.selectHeight,
      alignItems: 'center',
      width: '100%',
      justifyContent: 'space-between',
      flexDirection: 'row',
      borderRadius: 4,
      borderWidth: 1,
      borderColor: theme.color.Iron,
      paddingHorizontal: theme.spacing.p12,
      backgroundColor: theme.color.backgroundColor,
      marginVertical: theme.spacing.p6,
    },
    txtTitle: {
      color: theme.color.grayAbbey,
      fontSize: theme.fontSize.f14,
      //   fontWeight: 'bold',
      paddingBottom: theme.spacing.p2,
      marginRight: theme.spacing.p4,
    },
    multiSelectBtn: {
      flex: 1,
      width: '100%',
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
      paddingLeft: theme.spacing.p12,
      paddingBottom: theme.spacing.p2,
      height: measure.inputHeight,
    },
    iconDown: {
      fontSize: theme.fontSize.f24,
      marginLeft: theme.spacing.p4,
      color: theme.color.Iron,
    },
    selectedValue: {
      backgroundColor: theme.color.primary,
      minHeight: responsivePixel(42),
      justifyContent: 'center',
      paddingHorizontal: theme.spacing.p12,
    },
    selectValue0: {
      // backgroundColor: theme.color.white,
      // minHeight: responsivePixel(42),
      justifyContent: 'flex-start',
      alignItems: 'center',
      // paddingVertical: theme.spacing.p8,
      flexDirection: 'row',
      // borderWidth: 1,
      // margin: theme.fontSize.f4,
      // borderRadius: theme.fontSize.f4,
    },
    selectValue1: {
      backgroundColor: theme.color.mystic,
      minHeight: responsivePixel(42),
      justifyContent: 'center',
      paddingHorizontal: theme.spacing.p12,
    },
    headerWrap: {
      height: responsivePixel(50),
      paddingHorizontal: theme.spacing.p16,
      justifyContent: 'space-between',
      alignItems: 'center',
      backgroundColor: theme.color.blackHeader,
      // shadowColor: theme.color.black,
      // shadowOffset: {
      //   width: 0,
      //   height: 2,
      // },
      // shadowOpacity: 0.25,
      // shadowRadius: 3.84,
      // elevation: 5,
      borderBottomWidth: 1,
      borderBottomColor: theme.color.grayBoder,
      flexDirection: 'row',
    },
    backButton: {
      paddingTop: statusBar,
    },
    containerModal: {
      justifyContent: 'center',
      backgroundColor: theme.color.blackHeader,
      margin: 0,
    },
    require: {
      fontSize: theme.fontSize.f16,
      color: theme.color.redAlert,
    },
    searchBox: {
      borderBottomWidth: 1,
      borderColor: theme.color.grayBoder,
      minHeight: measure.inputHeight,
      paddingHorizontal: theme.spacing.p12,
      fontSize: theme.fontSize.f16,
    },
    txtValueSelected: {
      maxWidth: '95%',
      fontSize: theme.fontSize.f16,
      color: theme.color.blackTitle,
      opacity: 1,
    },
    placeHolder: {
      opacity: 0.7,
      color: theme.color.blackTitle,
      fontSize: theme.fontSize.f14,
    },
    labelStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingBottom: theme.spacing.p2,
    },
    WrapButton: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      marginHorizontal: theme.fontSize.f11,
    },
    cancel: {
      color: theme.color.redMonza,
      fontWeight: 'bold',
    },
    confirm: {
      color: theme.color.greenLimeade,
      fontWeight: 'bold',
    },
    titleStyle: {
      fontSize: theme.fontSize.f16,
      fontWeight: '500',
    },
    itemSeparator: {
      width: '100%',
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    buttonStyles: {
      marginVertical: theme.spacing.p24,
      paddingHorizontal: theme.spacing.p16,
    },
  });
  return styles;
};
