import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { TextStyle } from 'react-native';
import {
  ActivityIndicator,
  FlatList,
  StyleProp,
  TextInput,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import { appTimer, languageUtils } from '../../Helpers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppButton from '../AppButton';
import AppCheckBox from '../AppCheckBox';
import { AppFlatlistEmpty, AppText } from '../index';
import styles from './styles';

const { stringToSlug } = languageUtils;

interface Item {
  value: any;
  id: any;
}

interface DataItem {
  label: string;
  value: string | number;
}

export interface DropDownProps {
  data: Array<DataItem>;
  name?: string;
  title?: string;
  titleDropDown?: string;
  value?: any;
  onChoose?: (item: any) => void;
  disabled?: boolean;
  require?: boolean;
  isSearch?: boolean;
  loading?: boolean;
  customContainer?: StyleProp<ViewStyle>;
  notify?: any;
  customContent?: StyleProp<ViewStyle>;
  placeholder?: string;
  titleStyles?: TextStyle;
}

export default (props: DropDownProps) => {
  const {
    title,
    onChoose,
    data,
    value: valueDefault = [],
    isSearch,
    loading,
    placeholder,
    titleStyles
  } = props;

  const { t } = useTranslation();

  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const [isOpen, setOpen] = useState(false);
  const [value, setValue] = useState<Array<any>>(
    valueDefault ? valueDefault : [],
  );
  const [dataLocal, setDataLocal] = useState(data);
  const [loadingLocal, setLoading] = useState(false);
  const listSelected = dataLocal?.filter((elm) => {
    return value?.includes(elm.value);
  });

  const selectedLable = listSelected.map((elm) => elm.label);
  const handleData = (itemSelected: DataItem) => () => {
    setValue((prevData) => {
      if (prevData.includes(itemSelected.value)) {
        return prevData.filter((elm) => elm !== itemSelected.value);
      }
      return [...prevData, itemSelected.value];
    });
  };

  const handelConfirm = () => {
    onChoose && onChoose(value);
    setOpen(!isOpen);
  };

  const renderItem = (itemProps: { item: DataItem; index: number }) => {
    return (
      <TouchableOpacity
        style={[
          Styles.selectValue0,
          {
            backgroundColor: value.includes(itemProps.item.value)
              ? theme.color.lightGray
              : theme.color.white,
          },
        ]}
        onPress={handleData(itemProps.item)}
      >
        <AppCheckBox
          containerStyle={{
            backgroundColor: 'transparent',
            borderWidth: 0,
            flex: 1
          }}
          title={itemProps.item.label}
          textStyle={Styles.titleStyle}
          onPress={handleData(itemProps.item)}
          checked={value.includes(itemProps.item.value)}
        />
      </TouchableOpacity>
    );
  };

  const closeModal = () => {
    setOpen(!isOpen);
  };

  useEffect(() => {
    setValue(props?.value || []);
  }, [props.value]);

  useEffect(() => {
    setDataLocal(props.data);
  }, [props.data]);

  const deboundSearch = (txt: string) => {
    const newData = (_.cloneDeep(props.data) || []).filter((elm) => {
      const labelItem = stringToSlug(elm.label).toLowerCase();
      const txtInput = stringToSlug(txt).toLowerCase();
      return labelItem.includes(txtInput);
    });
    setDataLocal(newData);
    setLoading(false);
  };

  const onSearch = (txt: string) => {
    setLoading(true);
    appTimer.debounce(() => deboundSearch(txt), 800);
  };

  return (
    <>
      <View style={[Styles.container, props.customContainer]}>
        {title && (
          <View style={Styles.labelStyle}>
            <AppText
              style={[Styles.txtTitle, titleStyles, { opacity: props.disabled ? 0.5 : 1 }]}
            >
              {title}
              {props.require && <AppText style={Styles.require}> * </AppText>}
            </AppText>
            <View>{props.notify}</View>
          </View>
        )}
        <TouchableOpacity
          disabled={props.disabled}
          onPress={() => {
            setOpen(!isOpen), setDataLocal(data);
          }}
          style={{ opacity: props.disabled ? 0.5 : 1 }}
        >
          <View style={[Styles.content, props.customContent]}>
            <View style={{ flex: 1, justifyContent: 'center' }}>
              {!selectedLable.toString().trim() ? (
                <AppText style={Styles.placeHolder}>
                  {props.placeholder || t('common:select')}
                </AppText>
              ) : (
                <AppText numberOfLines={1} style={Styles.txtValueSelected}>
                  {selectedLable.toString().trim()}
                </AppText>
              )}
            </View>
            {loading ? (
              <ActivityIndicator size="small" />
            ) : (
              <MIcon name="chevron-down" style={Styles.iconDown} />
            )}
          </View>
        </TouchableOpacity>
      </View>

      {isOpen && (
        <Modal
          style={Styles.containerModal}
          useNativeDriver
          isVisible={isOpen}
          onBackdropPress={closeModal}
          animationIn="slideInUp"
          animationOut="slideOutDown"
        >
          <SafeAreaView style={{ flex: 1 }} edges={['top']} mode="padding">
            <View style={Styles.headerWrap}>
              <Icon
                name="close"
                type="ionicon"
                color={theme.color.graySilver}
                onPress={closeModal}
                tvParallaxProperties
              />
              {/* <Icon
                name="checkmark-sharp"
                type="ionicon"
                color={theme.color.greenLimeade}
                onPress={handelConfirm}
                tvParallaxProperties
              /> */}
              {/* <TouchableOpacity onPress={handelConfirm}>
                <AppText style={{ color: theme.color.graySilver }}>
                  {t('common:save')}
                </AppText>
              </TouchableOpacity> */}
            </View>
            <View style={{ flex: 1, backgroundColor: theme.color.white }}>
              {isSearch && (
                <TextInput
                  style={Styles.searchBox}
                  placeholder={t('common:search')}
                  onChangeText={onSearch}
                  placeholderTextColor={theme.color.mineShaft}
                />
              )}
              <FlatList
                data={dataLocal}
                refreshing={loadingLocal}
                onRefresh={() => {}}
                renderToHardwareTextureAndroid
                decelerationRate={0.95}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                renderItem={renderItem}
                keyExtractor={(item) => item && item.value.toString()}
                ListEmptyComponent={() => <AppFlatlistEmpty />}
                ItemSeparatorComponent={() => (
                  <View style={Styles.itemSeparator} />
                )}
              />
            <AppButton
              title={t('createMissions:save')}
              onPress={handelConfirm}
              containerStyle={Styles.buttonStyles}
            />
            </View>
          </SafeAreaView>
        </Modal>
      )}
    </>
  );
};
