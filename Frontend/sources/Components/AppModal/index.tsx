import React, { useEffect, useState } from 'react';
import {
  Button,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';
import { Icon } from 'react-native-elements';
import { useTheme, useThemeAwareObject } from '../../Theme';

interface PropsModal {
  visible: boolean;
  onToggleModal?: () => void;
  children?: any;
  propEnd?: any;
  dropPress?: () => void;
  titleHeader?: string;
  contentHeader?: any;
}

export default (props: PropsModal) => {
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const HeaderModal = () => {
    return (
      <View style={Styles.headerContainer}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}
        >
          <View style={{ alignItems: 'center' }}>
            {!props.contentHeader ? (
              <TouchableWithoutFeedback onPress={() => setModalVisible(false)}>
                <Icon
                  name="x"
                  type="feather"
                  color={theme.color.gray}
                  tvParallaxProperties
                />
              </TouchableWithoutFeedback>
            ) : (
              <View style={{ flexDirection: 'row' }}>
                <TouchableWithoutFeedback>
                  <Icon
                    name="arrowleft"
                    type="antdesign"
                    tvParallaxProperties
                    color={theme.color.blackTitle}
                  />
                </TouchableWithoutFeedback>
                <View style={{ marginLeft: 13 }}>{props.contentHeader}</View>
              </View>
            )}
          </View>
          <View style={{ justifyContent: 'center' }}>
            <Text style={Styles.titleHeader}>{props.titleHeader}</Text>
          </View>
          <View />
        </View>
      </View>
    );
  };

  const [isModalVisible, setModalVisible] = useState(false);

  useEffect(() => {
    setModalVisible(props.visible);
  }, [props.visible]);

  const toggleModal = () => {
    setModalVisible(props.visible);
  };

  return (
    <Modal
      isVisible={isModalVisible}
      style={{ margin: 0, justifyContent: 'flex-end' }}
      onBackdropPress={props.dropPress}
    >
      <HeaderModal />
      <View
        style={{
          width: '100%',
          height: 1,
          backgroundColor: theme.color.mercury,
        }}
      />
      <View style={{ backgroundColor: theme.color.white }}>
        {props.children}
      </View>
    </Modal>
  );
};
