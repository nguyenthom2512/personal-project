import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    headerContainer: {
      borderTopLeftRadius: 16,
      borderTopRightRadius: 16,
      paddingVertical: 12,
      backgroundColor: theme.color.violet,
      paddingHorizontal: 16,
    },
    titleHeader: {
      fontWeight: 'bold',
      fontSize: 18,
      color: theme.color.blackTitle,
    },
  });
