import React, { forwardRef } from 'react';
import { Text, TouchableOpacity, View, ViewStyle } from 'react-native';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';
import Modal from 'react-native-modal';
import { Modalize, ModalizeProps } from 'react-native-modalize';
interface PropsModal extends ModalizeProps {
  iconLeft?: any;
  iconRight?: any;
  onLeftPress?: () => void;
  onRightPress?: () => void;
  titleHeader?: string;
  contentHeader?: any;
  headerText?: string;
  children?: any;
  onRequestClose?: any;
  contentStyle?: ViewStyle;
  type?: boolean;
  disableSwipe?: boolean;
  onClosed?: () => void;
  headerComponent?: boolean;
}

export default forwardRef((props: PropsModal, ref: any) => {
  const {
    disableSwipe = true,
    onClosed = () => {},
    onRightPress = () => null,
    onLeftPress = () => null,
  } = props;
  const Styles = useThemeAwareObject(styles);
  const HeaderModal = () => {
    return (
      <View style={Styles.headerContainer}>
        <View>
          {props.iconLeft ? (
            <TouchableOpacity
              onPress={onLeftPress}
              style={{
                paddingHorizontal: 16,
                height: '100%',
                justifyContent: 'center',
              }}
            >
              {props.iconLeft}
            </TouchableOpacity>
          ) : (
            <View style={{ width: 56 }} />
          )}
        </View>
        <View style={{ flex: 1 }}>
          {props.type ? (
            props.contentHeader
          ) : (
            <Text
              style={{ textAlign: 'center', fontSize: 18, fontWeight: '600' }}
            >
              {props.headerText}
            </Text>
          )}
        </View>
        <View>
          {props.iconRight ? (
            <TouchableOpacity
              onPress={onRightPress}
              style={{
                paddingHorizontal: 16,
                height: '100%',
                justifyContent: 'center',
              }}
            >
              {props.iconRight}
            </TouchableOpacity>
          ) : (
            <View style={{ width: 56 }} />
          )}
        </View>
      </View>
    );
  };

  return (
    <>
      <Modalize
        HeaderComponent={() => (props.headerComponent ? <HeaderModal /> : null)}
        ref={ref}
        adjustToContentHeight={true}
        panGestureEnabled={disableSwipe}
        onClosed={onClosed}
        {...props}
      >
        <View style={Styles.bodyWrap}>
          {props.headerComponent ? null : <HeaderModal />}
          <View style={[Styles.safeBottom, props.contentStyle]}>
            {props.children}
          </View>
        </View>
      </Modalize>
    </>
  );
});
