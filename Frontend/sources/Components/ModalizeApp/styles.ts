import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flex1: {
      flex: 1,
    },
    headerContainer: {
      borderTopLeftRadius: 16,
      borderTopRightRadius: 16,
      backgroundColor: theme.color.white,
      // backgroundColor: '#E5E5E5',
      alignItems: 'center',
      flexDirection: 'row',
      maxHeight: 58,
      borderBottomWidth: 1,
      borderBottomColor: '#E5E5E5',
    },
    bodyWrap: {
      flex: 1,
      // backgroundColor: '#00000080',
      // alignItems: 'flex-end',
    },
    safeBottom: {
      flex: 1,
      backgroundColor: theme.color.white,
      paddingBottom: theme.spacing.p24,
    },
  });
  return styles;
};
