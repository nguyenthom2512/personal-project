import { StyleSheet } from 'react-native';
import { measure } from '../../Helpers';
import { Theme } from '../../Theme';
export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      // flex: 1,
      // backgroundColor: theme.color.primary,
      backgroundColor: theme.color.blackHeader,
      paddingTop: theme.spacing.p8,
      paddingBottom: theme.spacing.p8,
      zIndex:3
    },
    contentHeader: {
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
      height: measure.headerHeight,
    },
    viewLeft: { width: 56 },
    iconLeftBack: {
      color: theme.color.white,
      fontSize: theme.fontSize.f24,
      marginHorizontal: 16,
    },
    titleWrap: {
      flex: 1,
      alignItems: 'center',
    },
    txtTitle: {
      color: theme.color.white,
      fontSize: theme.fontSize.f18,
      fontWeight: '700',
    },
    iconRightWrapper: {
      justifyContent: 'space-between',
      alignItems: 'center',
      flexDirection: 'row',
    },
  });
  return styles;
};
