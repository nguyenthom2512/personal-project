/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback } from 'react';
import {
  ViewProps,
  View,
  ViewStyle,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import styles from './styles';
import { AppText } from '../index';
import { useNavigation, DrawerActions } from '@react-navigation/native';
import { useIsDrawerOpen } from '@react-navigation/drawer';
import { Icon } from 'react-native-elements';
import { useTheme, useThemeAwareObject } from '../../Theme';
import { Avatar, Badge, withBadge } from 'react-native-elements';
export interface HeaderProps extends ViewProps {
  isBack?: boolean;
  isMenu?: boolean;
  onLeftPress?: () => void;
  onRightPress?: () => void;
  iconLeft?: string;
  iconRight?: any;
  title?: string;
  typeIconLeft?: string;
  typeIconRight?: string;
  typeIconRightAdd?: string;
  iconRightAdd?: any;
  onRightAddPress?: () => void;
  styleContainer?: ViewStyle;
  svgRight?: any;
  svgRightAdd?: any;
  // customIconRightComponent?: React.ReactElement;
  value?: number;
  isShowBadge?: boolean;
}
export default (props: HeaderProps) => {
  const { value, isShowBadge } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const navigation = useNavigation();
  const isDrawerOpen = useIsDrawerOpen();
  const onBackPress = () => navigation.goBack();
  const onPressMenu = () => {
    if (isDrawerOpen) {
      navigation.dispatch(DrawerActions.closeDrawer);
      return;
    }
    navigation.dispatch(DrawerActions.openDrawer);
  };
  const onLeftPress = () => props.onLeftPress && props.onLeftPress();
  const onRightPress = () => props.onRightPress && props.onRightPress();

  const onRightAddPress = () =>
    props.onRightAddPress && props.onRightAddPress();

  const RenderLeft = useCallback(() => {
    if (props.isBack) {
      return (
        <Icon
          name="arrow-left"
          type={props.typeIconLeft || 'feather'}
          color={theme.color.white}
          onPress={onBackPress}
          containerStyle={Styles.iconLeftBack}
          tvParallaxProperties
        />
      );
    }
    if (props.isMenu) {
      return (
        <Icon
          name="menu"
          type={props.typeIconLeft || 'feather'}
          color={theme.color.white}
          onPress={onPressMenu}
          containerStyle={Styles.iconLeftBack}
          tvParallaxProperties
        />
      );
    }
    if (props.iconLeft) {
      return (
        <Icon
          name={props.iconLeft}
          type={props.typeIconLeft || 'feather'}
          color={theme.color.white}
          onPress={onLeftPress}
          tvParallaxProperties
          containerStyle={Styles.iconLeftBack}
        />
      );
    }
    return <View style={Styles.viewLeft} />;
  }, [props.isBack, props.isMenu, props.iconLeft, props.typeIconLeft]);
  const RenderTitle = useCallback(() => {
    return (
      <View style={Styles.titleWrap}>
        <AppText style={Styles.txtTitle} numberOfLines={1} ellipsizeMode="tail">
          {props.title}
        </AppText>
      </View>
    );
  }, [props.title]);
  const RenderRight = useCallback(() => {
    if (props.iconRight || props.iconRightAdd) {
      return (
        <View style={Styles.iconRightWrapper}>
          {props.iconRight &&
            (props.svgRight ? (
              <TouchableOpacity
                style={Styles.iconLeftBack}
                onPress={onRightPress}
              >
                {props.iconRight}
              </TouchableOpacity>
            ) : (
              <View>
                <Icon
                  name={props.iconRight}
                  type={props.typeIconRight || 'feather'}
                  color={theme.color.white}
                  onPress={onRightPress}
                  tvParallaxProperties
                  containerStyle={Styles.iconLeftBack}
                />
                {isShowBadge && (
                  <Badge
                    value={value}
                    status="primary"
                    containerStyle={{
                      position: 'absolute',
                      top: -theme.spacing.p8,
                      right: theme.spacing.p8,
                    }}
                    badgeStyle={{ backgroundColor: theme.color.violet }}
                  />
                )}
              </View>
            ))}
          {props.iconRightAdd &&
            (props.svgRightAdd ? (
              <TouchableOpacity
                style={Styles.iconLeftBack}
                onPress={onRightAddPress}
              >
                {props.iconRightAdd}
              </TouchableOpacity>
            ) : (
              <Icon
                name={props.iconRightAdd}
                type={props.typeIconRightAdd || 'feather'}
                color={theme.color.white}
                onPress={onRightAddPress}
                tvParallaxProperties
                containerStyle={Styles.iconLeftBack}
              />
            ))}
        </View>
      );
    }
    if (props.iconRight) {
      return (
        <Icon
          name={props.iconRight}
          type={props.typeIconRight || 'feather'}
          color={theme.color.white}
          onPress={onRightPress}
          tvParallaxProperties
          containerStyle={Styles.iconLeftBack}
        />
      );
    }
    return <View style={Styles.viewLeft} />;
  }, [
    props.iconRight,
    props.iconRightAdd,
    props.typeIconRight,
    props.typeIconRightAdd,
    props.onRightPress,
  ]);

  return (
    <SafeAreaView style={[Styles.container, props.styleContainer]}>
      <View style={Styles.contentHeader}>
        <RenderLeft />
        <RenderTitle />
        <RenderRight />
      </View>
    </SafeAreaView>
  );
};
