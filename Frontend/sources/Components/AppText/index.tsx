import React from 'react';
import { StyleProp, Text, TextProps, TextStyle } from 'react-native';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface AppTextProps extends TextProps {
  children?: any;
  style?: StyleProp<TextStyle>;
}

export default (props: AppTextProps) => {
  const Styles = useThemeAwareObject(styles);
  return (
    <Text {...props} style={[Styles.defaultText, props.style]}>
      {props.children}
    </Text>
  );
};
