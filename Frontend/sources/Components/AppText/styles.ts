import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    defaultText: {
      color: theme.color.black,
      fontSize: theme.fontSize.f16,
    },
  });
