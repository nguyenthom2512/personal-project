import { FormikProps } from 'formik';
import { isEmpty } from 'lodash';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ColorValue,
  Modal,
  ScrollView,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Collapsible from 'react-native-collapsible';
import Feather from 'react-native-vector-icons/Feather';
import AppContainer from '../../../Components/AppContainer';
import { device } from '../../../Helpers';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import AppInputField from '../../AppInputField';
import AppText from '../../AppText';
import styles from './styles';

interface Props {
  onCloseModal: () => void;
  results: any[];
  visible: boolean;
  index: any;
}

export default (props: Props) => {
  const [activeSections, setActiveSections] = useState<number[]>([]);
  const { results, onCloseModal = () => {}, visible, index } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { t } = useTranslation();

  const RenderTarget = ({ text = '', value = '' }) => {
    return (
      <View style={Styles.target}>
        <View style={{ flex: 1 }}>
          <AppText numberOfLines={1} style={Styles.titleStyle}>
            {text}
          </AppText>
        </View>
        <View style={{ flex: 2 }}>
          <AppText numberOfLines={1} style={Styles.titleStyle}>
            {value}
          </AppText>
        </View>
      </View>
    );
  };
  const [state, setstate] = useState('');
  const RenderInput = ({ title = '', value = '' }) => {
    return (
      <View style={{ paddingVertical: 10 }}>
        <AppText
          style={[Styles.titleStyle, { marginVertical: 10, opacity: 0.7 }]}
        >
          {title}
        </AppText>
        <TextInput
          value={value || ''}
          placeholderTextColor={theme.color.gray}
          style={[Styles.input]}
          editable={false}
          multiline={true}
          textAlignVertical="top"
        />
      </View>
    );
  };

  const changeCollapseCheckIn = (key: any) => {
    const newArray = [...activeSections];
    if (activeSections.includes(key)) {
      const indexKey = activeSections.findIndex((el) => el === key);
      newArray.splice(indexKey, 1);
      setActiveSections(newArray);
    } else {
      newArray.push(key);
      setActiveSections(newArray);
    }
  };

  const percent = (value: any) => {
    if (value <= 0.25) {
      return theme.color.cinnabar;
    }
    if (value <= 0.75) {
      return theme.color.fireBush;
    }
    return theme.color.greenBox;
  };

  const changeColor = (el: any): ColorValue => {
    switch (el) {
      case 0:
        return theme.color.cinnabar;
      case 1:
        return theme.color.blueChathams;
      case 2:
        return theme.color.greenBox;
      default:
        return theme.color.fireBush;
    }
  };

  const Confident = [
    {
      value: -1,
      label: t('checkIn:notYetCheckIn'),
    },
    {
      value: 0,
      label: t('checkIn:bad'),
    },
    {
      value: 1,
      label: t('checkIn:normal'),
    },
    {
      value: 2,
      label: t('checkIn:good'),
    },
  ];

  const labelConfident = (confident: number) => {
    const returnConfident = Confident.find((elm) => elm.value === confident);
    if (isEmpty(returnConfident)) return t('checkIn:notYetCheckIn');
    return returnConfident?.label ?? t('checkIn:notYetCheckIn');
  };

  return (
    <Modal visible={visible}>
      <AppContainer
        title={t('checkIn:historyCheckIn')}
        iconLeft="arrow-left"
        onLeftPress={() => onCloseModal()}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            backgroundColor: theme.color.mercury,
            paddingVertical: theme.spacing.p16,
          }}
        >
          {results.map((el: any, indx: any) => {
            return (el?.result_data || []).map((e: any, i: any) => {
              return (
                <View style={Styles.itemContainer} key={e.id.toString()}>
                  <TouchableOpacity
                    style={Styles.headerCollapse}
                    onPress={() => changeCollapseCheckIn(e?.id)}
                  >
                    <AppText numberOfLines={1} style={Styles.title}>
                      {e?.key_result}
                    </AppText>
                    <Feather
                      name="chevron-down"
                      size={28}
                      color={theme.color.mineShaft}
                    />
                  </TouchableOpacity>
                  <Collapsible
                    style={{
                      backgroundColor: theme.color.white,
                      flex: 1,
                    }}
                    align="top"
                    collapsed={!activeSections.includes(e?.id)}
                  >
                    <View style={Styles.collapseContainer}>
                      <RenderTarget
                        text={t('checkIn:target')}
                        value={e.target}
                      />
                      <RenderTarget
                        text={t('checkIn:numberPass')}
                        value={e.current_done}
                      />
                      <View style={Styles.progress}>
                        <AppText
                          numberOfLines={1}
                          style={[Styles.titleStyle, { flex: 1 }]}
                        >
                          {t('checkIn:progress')}
                        </AppText>
                        <View style={{ flex: 1 }}>
                          <TouchableOpacity
                            activeOpacity={1}
                            style={[
                              Styles.progressButton,
                              { backgroundColor: percent(e?.processed / 100) },
                            ]}
                          >
                            <AppText style={Styles.textProgress}>{`${
                              (e?.processed * 100)?.toFixed(2) || 0
                            }%`}</AppText>
                          </TouchableOpacity>
                        </View>
                        <View style={{ flex: 1 }} />
                      </View>
                      <View style={Styles.confident}>
                        <AppText
                          numberOfLines={1}
                          style={[Styles.titleStyle, { flex: 1 }]}
                        >
                          {t('checkIn:confident')}
                        </AppText>
                        <AppText
                          style={[
                            Styles.titleStyle,
                            {
                              flex: 1,
                              color: changeColor(el.confident),
                              opacity: 1,
                            },
                          ]}
                        >
                          {labelConfident(el.confident) || labelConfident(-1)}
                        </AppText>
                        <View style={{ flex: 1 }} />
                      </View>
                      <RenderInput
                        title={t('checkIn:progressQuestion')}
                        value={el.process_note}
                      />
                      <RenderInput
                        title={t('checkIn:workLateQuestion')}
                        value={el.overdue_note}
                      />
                      <RenderInput
                        title={t('checkIn:troubleNote')}
                        value={el.trouble_note}
                      />
                      <RenderInput
                        title={t('checkIn:troubleQuestion')}
                        value={el.solution_note}
                      />
                    </View>
                  </Collapsible>
                </View>
              );
            });
          })}
        </ScrollView>
      </AppContainer>
    </Modal>
  );
};
