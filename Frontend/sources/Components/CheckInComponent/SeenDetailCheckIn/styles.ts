import { StyleSheet } from 'react-native';
import { device, measure } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    headerCollapse: {
      flexDirection: 'row',
      backgroundColor: theme.color.white,
      alignItems: 'center',
      padding: theme.spacing.p8,
      justifyContent: 'space-between',
      paddingHorizontal: theme.spacing.p16,
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '500',
    },
    chooseConfident: {
      marginBottom: theme.spacing.p20,
      color: theme.color.mineShaft,
    },
    target: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginVertical: theme.spacing.p12,
    },
    text: {
      marginLeft: theme.spacing.p12,
      color: theme.color.mineShaft,
    },
    confident: {
      flexDirection: 'row',
    },
    progress: {
      flexDirection: 'row',
      alignItems: 'center',
      // flex: 1,
      marginVertical: theme.spacing.p16,
    },
    titleStyle: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      opacity: 0.5,
    },
    container: {
      paddingHorizontal: theme.spacing.p16,
    },
    input: {
      color: theme.color.black,
      paddingHorizontal: theme.spacing.p12,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.color.holderPlace,
      fontSize: theme.fontSize.f14,
      backgroundColor: theme.color.backgroundColor,
      height: 100,
    },
    itemContainer: {
      backgroundColor: theme.color.white,
      borderRadius: 4,
      marginHorizontal: theme.spacing.p16,
      marginBottom: theme.spacing.p16,
    },
    collapseContainer: {
      flex: 1,
      paddingHorizontal: theme.spacing.p16,
    },
    
    progressButton: {
      alignSelf: 'flex-start',
      padding: theme.spacing.p8,
      borderRadius: theme.spacing.p4,
    },
    textProgress: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.white,
    },
  });
  return styles;
};
