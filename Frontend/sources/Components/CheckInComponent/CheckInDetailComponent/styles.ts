import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    itemContainer: {
      backgroundColor: theme.color.white,
      // flexDirection: 'row',
      marginBottom: 16,
      borderRadius: 4,
    },
    title: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.shark,
      opacity: 0.5,
    },
    items: {
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      paddingHorizontal: theme.spacing.p16,
      paddingTop: theme.spacing.p16,
    },
    statusWrapper: {
      flex: 2,
      borderWidth: 1,
      padding: theme.spacing.p4,
      borderRadius: 4,
    },
    txtDate: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.shark,
    },
  });
  return styles;
};
