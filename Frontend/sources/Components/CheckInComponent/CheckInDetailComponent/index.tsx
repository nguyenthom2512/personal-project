import moment from 'moment';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ColorValue, TouchableOpacity, View } from 'react-native';
import { DDMMYYYY } from '../../../Helpers/dateTime';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppText from '../../AppText';
import styles from './styles';
import { useNavigation } from '@react-navigation/core';
import { Formik, FormikProps } from 'formik';
import SeenDetailCheckIn from '../SeenDetailCheckIn';

interface Props {
  dateCheckIn: any;
  formik: FormikProps<any>;
  dataResult: any;
}

// interface Props {
//   dateCheckIn: DataCheckIn;
// }
// interface DataCheckIn {
//   checkin_date: number;
//   checkin_status: 0 | 1;
// }

export default (props: Props) => {
  //! State
  const { dateCheckIn: items } = props;
  const { t } = useTranslation();
  const { theme } = useTheme();
  const navigation = useNavigation();
  const Styles = useThemeAwareObject(styles);

  const { checkin_date, checkin_status } = items;
  const [show, setShow] = useState(false);
  const checkInStatus = (item: any) => {
    let statusText = '';
    if (item.is_done) {
      return status('Đã hoàn thành');
    } else {
      switch (item.checkin_status) {
        case 1:
          return status('Check in đúng hạn');
        case 0:
          return status('Check in muộn');
        case -1:
          return status('Chưa check in');
        default:
          return status('Chưa check in');
      }
    }
  };
  const title = [
    {
      label: t('checkIn:checkInDateNext'),
      type: 'dateNext',
    },
    {
      label: t('checkIn:status'),
      type: 'status',
    },
  ];

  const Status = {
    // '0': t('checkIn:draft'),
    // '1': t('checkIn:approved'),
    '0': 'Check in muộn',
    '1': 'Check in đúng hạn',
    '-1': 'Chưa check in',
  };
  const changeColor = (el: any): ColorValue => {
    switch (el) {
      // case 0:
      //   return theme.color.fireBush;
      // case 1:
      //   return theme.color.greenBox;
      // default:
      //   return theme.color.greenBox;
      case 0:
        return theme.color.cinnabar;
      case 1:
        return theme.color.blueChathams;
      case 2:
        return theme.color.greenBox;
      default:
        return theme.color.fireBush;
    }
  };
  const renderCheckinInfo = (el: any, item: any) => {
    return (
      <View
        style={[
          Styles.items,
          {
            paddingBottom: item.type == 'status' ? 16 : 0,
          },
        ]}
      >
        <View style={{ flex: 1 }}>
          <AppText style={Styles.title}>{item.label}</AppText>
        </View>
        <View style={{ flex: 2, alignItems: 'flex-start', paddingLeft: 24 }}>
          {renderTabHistory(item.type)}
        </View>
      </View>
    );
  };
  const renderTabHistory = (type: string) => {
    switch (type) {
      case 'dateNext':
        return (
          <AppText>
            {`${t('checkIn:checkIn')} ${moment(checkin_date).format(DDMMYYYY)}`}
          </AppText>
        );
      case 'status':
        return (
          <View
            style={[
              Styles.statusWrapper,
              { borderColor: changeColor(items?.checkin_status) },
            ]}
          >
            <AppText
              style={{
                color: changeColor(items?.checkin_status),
              }}
            >
              {/* {Status?.[items?.checkin_status] || Status?.[0]} */}
            </AppText>
          </View>
        );
      default:
        break;
    }
  };
  const checkInDate = (title: string, value: any) => {
    return (
      <View style={[Styles.items]}>
        <View style={{ flex: 1 }}>
          <AppText style={Styles.title}>{title}</AppText>
        </View>
        <View
          style={{
            flex: 2,
            alignItems: 'flex-start',
            paddingLeft: 24,
          }}
        >
          <AppText style={Styles.txtDate}>
            {`${t('checkIn:checkIn')} ${moment(value).format(DDMMYYYY)}`}
          </AppText>
        </View>
      </View>
    );
  };
  const status = (value: any) => {
    return (
      <View style={[Styles.items, { paddingBottom: 16 }]}>
        <View style={{ flex: 1 }}>
          <AppText style={Styles.title}>{t('checkIn:status')}</AppText>
        </View>

        <View
          style={{
            flex: 2,
            alignItems: 'flex-start',
            paddingLeft: theme.spacing.p24,
          }}
        >
          <View
            style={[
              Styles.statusWrapper,
              { borderColor: changeColor(items?.checkin_status) },
            ]}
          >
            <AppText
              style={{
                color: changeColor(items?.checkin_status),
                fontSize: theme.fontSize.f14,
                fontWeight: '400',
              }}
            >
              {value}
            </AppText>
          </View>
        </View>
      </View>
    );
  };
  const checkInDetail = (index: any) => {
    return (
      <View style={[Styles.items, { paddingBottom: 16 }]}>
        <View style={{ flex: 1 }}>
          <AppText style={Styles.title}>{t('checkIn:status')}</AppText>
        </View>
        <TouchableOpacity
          style={{
            flex: 2,
            alignItems: 'flex-start',
            paddingLeft: 24,
          }}
          onPress={() => {
            setShow(true);
            // navigation.navigate('SeenDetailCheckIn', {
            //   items: items,
            //   formik: props.formik,
            //   index,
            // items: items?.okr_result,
            // });
          }}
        >
          <AppText
            style={{
              color: theme.color.violet,
              fontSize: theme.fontSize.f14,
              fontWeight: '500',
            }}
          >
            Xem chi tiết
          </AppText>
        </TouchableOpacity>
      </View>
    );
  };

  //! Render
  return (
    <View style={{ marginBottom: theme.spacing.p16 }}>
      {/* {title?.map((elm) => (
        <View style={Styles.itemContainer}>
          {renderCheckinInfo(elm.type, elm)}
        </View>
      ))} */}
      {items.length ? (
        items.map((elm: any, index: any) => {
          return (
            <View style={Styles.itemContainer} key={index.toString()}>
              {checkInDate(t('checkIn:checkInDate'), elm.created_time)}
              {elm.checkin_date &&
                checkInDate(t('checkIn:checkInDateNext'), elm.checkin_date)}
              {checkInStatus(elm)}
              {checkInDetail(index)}
              {show && (
                <SeenDetailCheckIn
                  results={elm?.checkin_result || []}
                  visible={show}
                  onCloseModal={() => setShow(false)}
                  key={index.toString()}
                  index={index}
                />
              )}
            </View>
          );
        })
      ) : (
        <AppText style={{ textAlign: 'center' }}>Không có lịch sử</AppText>
      )}
    </View>
  );
};
