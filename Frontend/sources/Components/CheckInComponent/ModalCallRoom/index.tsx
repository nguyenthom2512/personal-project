import { ErrorMessage, Formik } from 'formik';
import moment from 'moment';
import React, { forwardRef, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import * as Yup from 'yup';
import { AppInputField, AppText } from '../../../Components/';
import { IndicatorContext } from '../../../Context';
import isDarkMode from '../../../Helpers/darkmode';
import roomService from '../../../Services/roomService';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import ErrorMessages from '../../ErrorMessages';
import ModalizeApp from '../../ModalizeApp';
import styles from './style';

interface Props {
  onCloseModal: () => void;
  initData: any;
}

export default forwardRef((props: Props, ref: any) => {
  const { onCloseModal = () => {}, initData } = props;

  const { t } = useTranslation();
  const { setLoading } = useContext(IndicatorContext);
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const [show, setShow] = useState(false);
  const [date, setDate] = useState<Date | ''>('');

  const [initialValue, setInitValue] = useState({
    room_url: '',
    room_id: '',
    room_name: '',
    start_time: '',
    okr_id: '',
    password: '',
    checkin: '',
    user_relate: [],
  });
  const checkInforRoom = Yup.object().shape({
    room_name: Yup.string().required(t('common:required')),
    start_time: Yup.string().required(t('common:required')),
  });

  const createRoomCall = (id: any) => `checkin${id}_${moment().unix()}`;

  useEffect(() => {
    const roomId = createRoomCall(initData.id);
    setInitValue({
      room_url: `/call-checkin-okr/${roomId}`,
      room_id: roomId,
      room_name: initData.object_name,
      start_time: '',
      okr_id: initData.id,
      password: '',
      checkin: '',
      user_relate: [],
    });
  }, [initData]);

  const handleDateTime = (dateTime: any, setFieldValue: any) => {
    const startTime = moment(dateTime).format('YYYY-MM-DDTHH:mm:ss');
    setFieldValue('start_time', startTime);
    setDate(dateTime);
    setShow(false);
  };

  const onSubmit = (values: any) => {
    setLoading(true);
    onCloseModal();
    roomService.createRoom(
      values,
      (err) => {
        setLoading(false);
        Alert.alert(t('common:notice'), err || t('room:failedRoom'));
      },
      () => {
        setLoading(false);
        Alert.alert(t('common:notice'), t('room:successRoom'), [
          {
            text: 'OK',
            onPress: () => onCloseModal(),
          },
        ]);
      },
    );
  };

  return (
    <ModalizeApp
      ref={ref}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('room:createRoom')}
    >
      <Formik
        initialValues={initialValue}
        enableReinitialize
        validationSchema={checkInforRoom}
        onSubmit={(value) => onSubmit(value)}
      >
        {({ values, setFieldValue, handleSubmit }) => {
          return (
            <View style={Styles.wrapModal}>
              <AppInputField
                placeholder={t('room:nameRoom')}
                name="room_name"
                title={t('room:nameRoom')}
              />
              {/* <AppInputField
                placeholder={t('room:passRoom')}
                name="password"
                title={t('room:passRoom')}
              /> */}
              <View>
                <AppText style={Styles.txtTitle}>{t('room:startTime')}</AppText>
                <TouchableOpacity
                  onPress={() => setShow(true)}
                  style={Styles.wrapContent}
                >
                  <AppText
                    style={[
                      Styles.txtDate,
                      values.start_time
                        ? {}
                        : { color: theme.color.blackTitle, opacity: 0.5 },
                    ]}
                  >
                    {values.start_time
                      ? moment(date).format('DD-MM-YYYY HH:mm')
                      : 'DD-MM-YYYY HH:mm'}
                  </AppText>
                  <Icon
                    name="calendar"
                    tvParallaxProperties
                    type="font-awesome"
                    color={theme.color.violet}
                  />
                </TouchableOpacity>
                <AppText style={{ color: theme.color.redAlert }}>
                  <ErrorMessages name="start_time" />
                </AppText>
              </View>
              {show && (
                <DateTimePickerModal
                  locale="vi"
                  themeVariant="light"
                  isDarkModeEnabled={isDarkMode()}
                  isVisible={show}
                  mode="datetime"
                  date={new Date()}
                  onConfirm={(dateTime) => {
                    handleDateTime(dateTime, setFieldValue);
                  }}
                  onCancel={() => setShow(false)}
                  minimumDate={new Date()}
                />
              )}
              <AppButton
                onPress={() => handleSubmit()}
                title={t('room:createRoom')}
                style={{ marginTop: theme.spacing.p8 }}
              />
            </View>
          );
        }}
      </Formik>
    </ModalizeApp>
  );
});
