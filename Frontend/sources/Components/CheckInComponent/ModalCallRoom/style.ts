import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    wrapModal: {
      marginHorizontal: theme.spacing.p12,
    },
    txtTitle: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      marginTop: theme.spacing.p16,
    },
    wrapContent: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 4,
      borderWidth: 1,
      borderColor: theme.color.mercury,
      paddingVertical: 10,
      paddingHorizontal: theme.spacing.p12,
      marginVertical: theme.spacing.p12,
    },
    txtDate: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
  });
  return styles;
};
