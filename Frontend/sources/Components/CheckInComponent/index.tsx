import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { TouchableOpacity, View } from 'react-native';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppCheckboxField from '../AppCheckboxField';
import AppText from '../AppText';
import styles from './styles';
import Calendar from '../../Assets/Svg/date.svg';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import isDarkMode from '../../Helpers/darkmode';
import { ErrorMessage, FormikProps } from 'formik';
import moment from 'moment';
import { DDMMYYYY } from '../../Helpers/dateTime';
import AppButton from '../AppButton';
import AppDropDown from '../AppDropDown';
import ErrorMessages from '../../Components/ErrorMessages';

interface Props {
  formik: FormikProps<any>;
  onComplete: () => void;
}

export default (props: Props) => {
  const { formik, onComplete } = props;
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const [show, setShow] = useState(false);

  const showDatepicker = () => {
    if (formik.values?.is_done) {
      return;
    }
    setShow(true);
  };

  const handleConfirm = (date: any) => {
    const selected = new Date(date);
    formik.setFieldValue(`checkin_date`, selected);
    setShow(false);
  };

  const hideDatePicker = () => {
    setShow(false);
  };

  const onCheckBox = (e: any) => {
    formik.setFieldValue(`is_done`, e);
  };
  const onChooseConfident = (e: string | number) => {
    formik.setFieldValue(`confident`, e);
  };
  const dataDropDown = [
    {
      label: t('checkIn:bad'),
      value: '0',
    },
    {
      label: t('checkIn:normal'),
      value: '1',
    },
    {
      label: t('checkIn:good'),
      value: '2',
    },
  ];
  return (
    <View style={Styles.container}>
      <AppCheckboxField
        title={t('checkIn:complete')}
        name="check-box"
        titleStyle={Styles.titleStyle}
        onPress={onCheckBox}
      />
      <AppDropDown
        disabled={false}
        placeHolder={t('checkIn:chooseConfident')}
        data={dataDropDown}
        onChoose={(el) => onChooseConfident(el.value)}
      />
      {/* <AppText style={{ color: theme.color.redAlert }}> */}
        <ErrorMessages name="confident" />
      {/* </AppText> */}
      <View style={Styles.dateCheckIn}>
        <AppText style={Styles.title}>{t('checkIn:checkInDateNext')}</AppText>
        <TouchableOpacity
          activeOpacity={formik.values?.is_done ? 1 : 0.6}
          style={Styles.daysBox}
          onPress={() => showDatepicker()}
          disabled={formik.values?.is_done ? true : false}
        >
          <AppText style={Styles.txtDate}>
            {moment(formik.values?.checkin_date).isValid()
              ? moment(formik.values?.checkin_date).format(DDMMYYYY)
              : 'DD-MM-YYYY'}
          </AppText>
          <Calendar />
        </TouchableOpacity>
      </View>
      <View style={{ alignItems: 'flex-end' }}>
        <ErrorMessages name="checkin_date" />
      </View>
      <View style={{ flexDirection: 'row', marginTop: theme.spacing.p16 }}>
        <View style={{ flex: 1 }}>
          <AppButton
            title={t('checkIn:completeCheckIn')}
            onPress={onComplete}
          ></AppButton>
        </View>
      </View>
      {show && (
        <DateTimePickerModal
          locale="vi"
          is24Hour
          themeVariant="light"
          isDarkModeEnabled={isDarkMode()}
          isVisible={show}
          mode="date"
          date={
            moment(formik.values?.checkin_date).isValid()
              ? moment(formik.values?.checkin_date).toDate()
              : new Date()
          }
          onConfirm={(date) => handleConfirm(date)}
          onCancel={hideDatePicker}
          minimumDate={new Date()}
        />
      )}
    </View>
  );
};
