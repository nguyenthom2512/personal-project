import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    titleStyle: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    container: {
      backgroundColor: theme.color.white,
      marginTop: theme.spacing.p16,
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
    },
    title: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.charade,
      opacity: 0.5,
    },
    daysBox: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderWidth: 1.5,
      borderColor: theme.color.mercury,
      padding: theme.spacing.p12,
      borderRadius: 4,
      marginLeft: theme.spacing.p16,
    },
    txtDate: {
      color: theme.color.charade,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    dateCheckIn: {
      flexDirection: 'row',
      alignItems: 'center',
    },
  });
  return styles;
};
