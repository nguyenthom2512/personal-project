import { StyleSheet } from 'react-native';
import { device } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    headerCollapse: {
      flexDirection: 'row',
      backgroundColor: theme.color.white,
      alignItems: 'center',
      padding: theme.spacing.p8,
      justifyContent: 'space-between',
      paddingHorizontal: theme.spacing.p16,
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '500',
      flex: 1,
    },
    chooseConfident: {
      marginBottom: 20,
      paddingHorizontal: theme.spacing.p16,
      color: theme.color.mineShaft,
      fontSize: theme.fontSize.f14,
    },
    target: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
    },
    text: {
      marginLeft: theme.spacing.p12,
      color: theme.color.mineShaft,
    },
    confident: {
      padding: theme.spacing.p16,
      flexDirection: 'row',
    },
    progress: {
      padding: theme.spacing.p16,
      flexDirection: 'row',
      alignItems: 'center',
      //   flex: 1,
      //   backgroundColor: 'green'
    },
    titleStyle: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      opacity: 0.7,
    },
    container: {
      paddingHorizontal: theme.spacing.p16,
      flex: 1,
    },
    progressButton: {
      alignSelf: 'flex-start',
      padding: theme.spacing.p8,
      borderRadius: theme.spacing.p4,
    },
    textProgress: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.white,
    },
  });
  return styles;
};
