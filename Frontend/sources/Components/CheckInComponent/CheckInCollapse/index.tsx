import { FormikProps } from 'formik';
import { isEmpty, values } from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ColorValue, View } from 'react-native';
import { TouchableOpacity } from 'react-native';
import Collapsible from 'react-native-collapsible';
import Feather from 'react-native-vector-icons/Feather';
import { device, padding } from '../../../Helpers';
import useGetUnit from '../../../Hooks/getUnit';
import { OkrResult } from '../../../interfaces/okr.interface';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import AppCheckboxField from '../../AppCheckboxField';
import AppDropDown from '../../AppDropDown';
import AppInputField from '../../AppInputField';
import AppText from '../../AppText';
import styles from './styles';
import { ErrorMessages } from '../../../Components';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
interface IRenderTarget {
  text: string;
  name: string;
  handleTotalDone?: (value: string) => void;
  editable?: boolean;
  formik: FormikProps<any>;
  index?: number;
  unitName?: string;
}
interface Props {
  formik: FormikProps<any>;
  data: OkrResult[];
  okCheckIn: any[];
}

const RenderTarget = ({
  text = '',
  name = '',
  handleTotalDone = (value) => {},
  editable = true,
  formik,
  index,
  unitName,
}: IRenderTarget) => {
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const nameInput = name.split('.')[1];
  const okr_result = formik?.errors?.okr_result;
  //@ts-ignore
  const error = okr_result ? okr_result[index || 0]?.[nameInput] : '';
  return (
    <View style={Styles.target}>
      <View style={{ flex: 1 }}>
        <AppText style={[Styles.titleStyle, { opacity: 0.5 }]}>{text}</AppText>
      </View>
      <View style={{ flex: 1 }}>
        <AppInputField
          editable={editable}
          name={name}
          customInputStyle={{
            borderColor: error ? theme.color.redAlert : theme.color.mineShaft,
            fontSize: theme.fontSize.f14,
            padding: 0,
            height: theme.spacing.p40,
            paddingVertical: 0,
          }}
          containerStyle={{}}
          onChangeCustom={handleTotalDone}
          keyboardType="numeric"
          placeholder={error || ''}
          notShowError
        />
      </View>
      <View style={{ flex: 1 }}>
        <AppText
          style={[
            Styles.titleStyle,
            { opacity: 0.5, marginLeft: theme.spacing.p8 },
          ]}
        >
          {unitName}
        </AppText>
      </View>
    </View>
  );
};
export default (props: Props) => {
  const { data, okCheckIn } = props;
  const [activeSections, setActiveSections] = useState<number[]>([]);
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { t } = useTranslation();
  const [dataUnit] = useGetUnit();
  const [isShowCollapse, setIsShowCollapse] = useState(false);

  useEffect(() => {
    // setActiveSections();
    const active: number[] = [];
    (data || []).forEach((elm) => {
      active.push(elm.id);
    });
    setActiveSections(active);
  }, [data]);

  const checkDisable = (el: any) => {
    const index = okCheckIn[0]?.checkin_result?.findIndex(
      (value: any) => value?.result == el.id && value.is_done,
    );
    return index;
  };

  const changeCollapseCheckIn = (key: any) => {
    const newArray = [...activeSections];
    if (activeSections.includes(key)) {
      const indexKey = activeSections.findIndex((el) => el === key);
      newArray.splice(indexKey, 1);
      setActiveSections(newArray);
    } else {
      newArray.push(key);
      setActiveSections(newArray);
    }
  };
  const Confident = [
    {
      value: -1,
      label: t('checkIn:notYetCheckIn'),
    },
    {
      value: 0,
      label: t('checkIn:bad'),
    },
    {
      value: 1,
      label: t('checkIn:normal'),
    },
    {
      value: 2,
      label: t('checkIn:good'),
    },
  ];
  const dataDropDown = [
    {
      label: t('checkIn:bad'),
      value: '0',
    },
    {
      label: t('checkIn:normal'),
      value: '1',
    },
    {
      label: t('checkIn:good'),
      value: '2',
    },
  ];
  const changeColor = (el: any): ColorValue => {
    switch (el) {
      case 0:
        return theme.color.cinnabar;
      case 1:
        return theme.color.blueChathams;
      case 2:
        return theme.color.greenBox;
      default:
        return theme.color.fireBush;
    }
  };
  const percent = (value: any) => {
    if (value <= 0.25) {
      return theme.color.cinnabar;
    }
    if (value <= 0.75) {
      return theme.color.fireBush;
    }
    return theme.color.greenBox;
  };

  const labelConfident = (confident: number) => {
    const returnConfident = Confident.find((elm) => elm.value === confident);
    if (isEmpty(returnConfident)) return t('checkIn:notYetCheckIn');
    return returnConfident?.label ?? t('checkIn:notYetCheckIn');
  };

  const handleTotalDone = (e: string, index: number) => {
    props.formik.setFieldValue(
      `okr_result[${index}].processed`,
      (Number(e) * 100) / Number(props.formik.values.okr_result[index].target),
    );
    props.formik.setFieldValue(`okr_result[${index}].total_done`, e);
  };

  const checkBox = (e: boolean, index: number) => {
    props.formik.setFieldValue(`okr_result[${index}].is_done`, e);
  };

  const onChooseConfident = (e: string | number, index: number) => {
    props.formik.setFieldValue(`okr_result[${index}].confident`, e);
  };

  const renderInput = (
    title: string,
    nameInput: string,
    disable?: boolean,
    index?: number,
  ) => {
    const name = nameInput.split('.')[1];
    const okr_result = props?.formik?.errors?.okr_result;
    const error = okr_result
      ? //@ts-ignore
        props?.formik?.errors?.okr_result[index || 0]?.[name]
      : '';
    return (
      <View style={Styles.container}>
        <AppText style={Styles.titleStyle}>{title}</AppText>
        <AppInputField
          name={nameInput}
          multiline
          customPlaceholderColor={'red'}
          customInputStyle={{
            minHeight: 100,
            borderColor: error ? theme.color.redAlert : theme.color.mineShaft,
          }}
          placeholder={error || ''}
          editable={!disable}
          notShowError
        />
      </View>
    );
  };

  return (
    <>
      {data?.map((el, index) => {
        const disable = checkDisable(el) >= 0;

        return (
          <View key={index.toString()} style={{ marginBottom: 16 }}>
            <TouchableOpacity
              style={Styles.headerCollapse}
              onPress={() => changeCollapseCheckIn(el?.id)}
            >
              <AppText numberOfLines={1} style={Styles.title}>
                {el?.key_result}
              </AppText>
              {isShowCollapse ? (
                <Feather
                  name="chevron-down"
                  size={28}
                  color={theme.color.mineShaft}
                />
              ) : (
                <MaterialIcons name="keyboard-arrow-down" size={28} />
              )}
            </TouchableOpacity>
            <Collapsible
              style={{ backgroundColor: theme.color.white, height: 1100 }}
              align="top"
              collapsed={!activeSections.includes(el?.id)}
            >
              <View style={{ flex: 1, width: device.width - 32 }}>
                <RenderTarget
                  name={`okr_result[${index}].target`}
                  text={t('checkIn:target')}
                  editable={false}
                  unitName={el.unit_name}
                  formik={props.formik}
                  index={index}
                />
                <RenderTarget
                  name={`okr_result[${index}].total_done`}
                  text={t('checkIn:numberPass')}
                  handleTotalDone={(value) => handleTotalDone(value, index)}
                  editable={!disable}
                  unitName={el.unit_name}
                  formik={props.formik}
                  index={index}
                />
                <View style={Styles.progress}>
                  <AppText
                    style={[Styles.titleStyle, { flex: 1, opacity: 0.5 }]}
                  >
                    {t('checkIn:progress')}
                  </AppText>
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      style={[
                        {
                          backgroundColor: percent(
                            props.formik.values.okr_result[index]?.processed /
                              (disable ? 1 : 100),
                          ),
                        },
                        Styles.progressButton,
                      ]}
                    >
                      <AppText style={Styles.textProgress}>{`${
                        (
                          props.formik.values?.okr_result[index]?.processed *
                          (disable ? 100 : 1)
                        ).toFixed(2) || 0
                      }%`}</AppText>
                    </TouchableOpacity>
                  </View>
                  <View style={{ flex: 1 }}></View>
                </View>
                <View style={Styles.confident}>
                  <AppText
                    style={[Styles.titleStyle, { flex: 1.5, opacity: 0.5 }]}
                  >
                    {t('checkIn:confident')}
                  </AppText>
                  <AppText
                    style={{
                      color: changeColor(el.confident),
                      fontSize: theme.fontSize.f14,
                    }}
                  >
                    {labelConfident(el.confident) || labelConfident(-1)}
                  </AppText>
                  <View style={{ flex: 1 }} />
                </View>
                {renderInput(
                  t('checkIn:progressQuestion'),
                  `okr_result[${index}].process_note`,
                  disable,
                  index,
                )}
                {renderInput(
                  t('checkIn:workLateQuestion'),
                  `okr_result[${index}].overdue_note`,
                  disable,
                  index,
                )}
                {renderInput(
                  t('checkIn:troubleNote'),
                  `okr_result[${index}].trouble_note`,
                  disable,
                  index,
                )}
                {renderInput(
                  t('checkIn:troubleQuestion'),
                  `okr_result[${index}].solution_note`,
                  disable,
                  index,
                )}
                <View>
                  <AppCheckboxField
                    title={t('checkIn:complete')}
                    name="checkBox"
                    onPress={(value) => checkBox(value, index)}
                    disabled={disable}
                    titleStyle={{
                      color: theme.color.shark,
                      fontWeight: '400',
                      fontSize: theme.fontSize.f14,
                    }}
                  />
                  <AppText style={Styles.chooseConfident}>
                    {t('checkIn:chooseCompleteConfident')}
                  </AppText>
                  <View
                    style={{
                      paddingHorizontal: theme.spacing.p16,
                      marginBottom: theme.spacing.p16,
                    }}
                  >
                    <AppDropDown
                      disabled={disable}
                      value={props.formik.values.okr_result[index]?.confident}
                      placeHolder={t('checkIn:chooseConfident')}
                      data={dataDropDown}
                      onChoose={(el) => onChooseConfident(el.value, index)}
                    />
                    <AppText>
                      <ErrorMessages name={`okr_result[${index}].confident`} />
                    </AppText>
                  </View>
                </View>
              </View>
            </Collapsible>
          </View>
        );
      })}
    </>
  );
};
