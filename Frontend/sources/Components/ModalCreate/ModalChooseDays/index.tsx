import { FormikProps } from 'formik';
import moment from 'moment';
import React, { forwardRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SafeAreaView, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AppButton } from '../..';
import isDarkMode from '../../../Helpers/darkmode';
import { DDMMYYYY } from '../../../Helpers/dateTime';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppText from '../../AppText';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  minDate?: any;
}

let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const { onCloseModal = () => {}, formik, minDate } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const [date, setDate] = useState<Date | ''>(formik.values.date || '');
  const [show, setShow] = useState(false);

  useEffect(() => {
    setDate(formik.values.date || '');
  }, [formik.values.date]);

  const showDatepicker = () => {
    setShow(true);
  };

  const handleConfirm = (date: any) => {
    const selected = new Date(date);
    // formik.setFieldValue('date',moment(selected).format('YYYY-MM-DD'));
    setDate(selected);
    setShow(false);
  };

  const hideDatePicker = () => {
    setShow(false);
  };
  const handleCloseModal = () => {
    if (!isSave) {
      setDate(formik.values.date);
    }
    isSave = false;
  };

  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:chooseDays')}
    >
      <SafeAreaView>
        <View style={Styles.button}>
          <AppText style={Styles.titleSelect}>
            {t('createMissions:selectDate')}
          </AppText>
          <View style={Styles.datepickerStyle}>
            <TouchableOpacity
              onPress={() => showDatepicker()}
              style={Styles.wrapContent}
            >
              <AppText style={Styles.txtDate}>
                {moment(date).isValid()
                  ? moment(date).format(DDMMYYYY)
                  : 'DD-MM-YYYY'}
              </AppText>
              <Icon
                name="calendar"
                tvParallaxProperties
                type="font-awesome"
                color={theme.color.violet}
              />
              {/* </View> */}
            </TouchableOpacity>
            <TouchableOpacity onPress={() => setDate('')}>
              <Ionicons name="close" size={24} />
            </TouchableOpacity>
          </View>
          <AppButton
            containerStyle={Styles.buttonContainer}
            title={t('createMissions:save')}
            onPress={() => {
              isSave = true;
              //   formik.setFieldValue('date', date);
              // TODO: check again this logic impact to TodoDetails
              formik.setFieldValue(
                'date',
                date ? moment(date).format('YYYY-MM-DD') : '',
              );
              onCloseModal();
            }}
          />
        </View>
        {show && (
          <DateTimePickerModal
            locale="vi"
            is24Hour
            themeVariant="light"
            isDarkModeEnabled={isDarkMode()}
            isVisible={show}
            mode="date"
            date={moment(date).isValid() ? moment(date).toDate() : new Date()}
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
            minimumDate={new Date()}
          />
        )}
      </SafeAreaView>
    </ModalizeApp>
  );
});
