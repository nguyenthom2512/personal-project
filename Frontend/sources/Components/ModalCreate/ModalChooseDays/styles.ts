import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    button: {
      marginHorizontal: theme.spacing.p16,
    },
    titleSelect: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.charade,
      opacity: 0.5,
      marginTop: theme.spacing.p16,
    },
    datepickerStyle: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    wrapContent: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 4,
      borderWidth: 1,
      borderColor: theme.color.mercury,
      paddingVertical: theme.spacing.p12,
      paddingHorizontal: theme.spacing.p12,
      marginRight: theme.spacing.p12,
      marginVertical: theme.spacing.p8,
    },

    txtDate: {
      color: theme.color.charade,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    buttonContainer: {
      marginVertical: theme.spacing.p8,
    },
  });
  return styles;
};
