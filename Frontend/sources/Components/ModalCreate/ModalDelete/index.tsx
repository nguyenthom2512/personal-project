import { FormikProps } from 'formik';
import React, { forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, View } from 'react-native';
import { device } from '../../../Helpers';
import Modal from 'react-native-modal';
import { Theme, useTheme } from '../../../Theme';
import AppButton from '../../AppButton';
import AppText from '../../AppText';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  modalVisible: boolean;
  onDeleteItem: () => void;
}

export default (props: Props) => {
  const { t } = useTranslation();
  const { modalVisible = false, onCloseModal = () => {}, onDeleteItem = () => {} } = props;
  const { theme } = useTheme();
  const Styles = themeStyle(theme);

  return (
    <Modal onBackdropPress={onCloseModal} isVisible={modalVisible}>
      <View style={Styles.containerModalStyles}>
        <View style={{alignItems: 'center'}}>
          <AppText style={Styles.textDelete}>
            {t('todoDetail:deleteTodo')}
          </AppText>
        </View>
        <View style={Styles.buttonContainer}>
          <AppButton
            containerStyle={{ flex: 1, marginRight: 12 }}
            
            title={t('todoDetail:cancel')}
            onPress={onCloseModal}
          />
          <AppButton
            containerStyle={{ flex: 1 }}
            cancel
            title={t('todoDetail:yes')}
            onPress={onDeleteItem}
          />
        </View>
      </View>
    </Modal>
  );
};

const themeStyle = (theme: Theme) => {
  return StyleSheet.create({
    inputStyles: {
      borderWidth: 0,
      height: device.height / 2.5,
    },
    containerModalStyles: {
      backgroundColor: theme.color.white,
    //   height: device.height / 4.5,
      borderRadius: theme.spacing.p8,
    //   paddingVertical: 36,
      justifyContent: 'space-between',
      padding: theme.spacing.p16,
    //   paddingBottom: theme.spacing.p16,

    },
    buttonContainer: {
      flexDirection: 'row',
      paddingBottom: theme.spacing.p12,
    },
    textDelete: {
      fontSize: theme.fontSize.f20,
      marginBottom: theme.spacing.p40
    },
  });
};
