import { StyleSheet } from 'react-native';
import { device } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    itemContainer: {
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      paddingVertical: theme.spacing.p20,
      paddingHorizontal: theme.spacing.p16,
    },
    text: {
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      marginLeft: 12,
    },
    button: {
      marginTop: 24,
      paddingHorizontal: theme.spacing.p40
    },
    textButton: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
      color: theme.color.black,
    },
    flex1:{
      flex: 1
    },
  });
  return styles;
};
