import { FormikProps } from 'formik';
import React, { forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { SafeAreaView, TouchableOpacity, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Link from '../../../Assets/Svg/link.svg';
import Mission from '../../../Assets/Svg/mission.svg';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import AppText from '../../AppText';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';
import Delete from '../../../Assets/Svg/delete.svg';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  chooseAction: (key: string) => void;
  isCreate?: boolean;
}

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const { onCloseModal = () => {}, chooseAction } = props;
  const Styles = useThemeAwareObject(styles);
  const data = [
    {
      key: 'duplicate',
      icon: <Mission />,
      text: t('createMissions:duplicateMission'),
    },
    // {
    //   key: 'Link',
    //   icon: <Link />,
    //   text: t('createMissions:copyLink'),
    // },
    {
      key: 'deleteMission',
      icon: <Delete />,
      text: t('createMissions:deleteMission'),
    },
  ];
  const renderItems = () => (
    <View>
      {data.map((el, index) => {
        if (index === 2 && props.isCreate) return;
        return (
          <TouchableOpacity
            key={index.toString()}
            style={Styles.itemContainer}
            onPress={() => {
              chooseAction(el.key);
            }}
          >
            {el.icon}
            <AppText style={Styles.text}>{el.text}</AppText>
          </TouchableOpacity>
        );
      })}
    </View>
  );
  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:otherFunction')}
    >
      <SafeAreaView>
        {/* <View style={Styles.header}>
          <TouchableOpacity onPress={onCloseModal}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>
            {t('createMissions:otherFunction')}
          </AppText>
          <View />
        </View> */}
        {renderItems()}
      </SafeAreaView>
    </ModalizeApp>
  );
});
