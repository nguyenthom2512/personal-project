import { Formik } from 'formik';
import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, View } from 'react-native';
import { Icon } from 'react-native-elements';
import * as yup from 'yup';
import { AppInputField } from '../..';
import { IndicatorContext } from '../../../Context';
import TodoTag from '../../../Services/TodoTag';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import ModalApp from '../../ModalApp';
import styles from './styles';

interface Props {
  modalVisible: boolean;
  onCloseModal: () => void;
  refetchTag: () => void;
  user: any;
}

export default (props: Props) => {
  const { theme } = useTheme();
  const { t } = useTranslation();
  const {
    modalVisible = false,
    onCloseModal = () => {},
    refetchTag,
    user,
  } = props;
  const Styles = useThemeAwareObject(styles);
  const { setLoading } = useContext(IndicatorContext);

  const validate = yup.object().shape({
    tag_name: yup.string().required(t('common:required')),
    tag_code: yup.string().required(t('common:required')),
  });

  const onSubmit = async (value: any) => {
    setLoading(true);
    TodoTag.createTodoTag(
      value,
      (err) => {
        let mess = err;
        if (err === 'UNKNOWN') {
          mess = t('common:unknownError');
        }
        Alert.alert(t('common:notice'), mess);
        setLoading(false);
      },
      () => {
        onCloseModal();
        refetchTag();
        setLoading(false);
      },
    );
  };

  return (
    <ModalApp
      iconLeft={
        <Icon
          name="x"
          type="feather"
          color={theme.color.gray}
          tvParallaxProperties
          onPress={onCloseModal}
        />
      }
      headerText={t('createMissions:createTag')}
      visible={modalVisible}
      onClickOutside={onCloseModal}
    >
      <View style={Styles.wrapContent}>
        <Formik
          initialValues={{
            tag_name: '',
            tag_code: '',
            user: user,
          }}
          validationSchema={validate}
          onSubmit={onSubmit}
        >
          {({ values, handleSubmit }) => {
            return (
              <View>
                <AppInputField
                  name="tag_name"
                  title={t('createMissions:tagName')}
                  placeholder={t('createMissions:tagName')}
                />
                <AppInputField
                  name="tag_code"
                  title={t('createMissions:tagCode')}
                  placeholder={t('createMissions:tagCode')}
                />
                <AppButton
                  containerStyle={{ marginTop: theme.spacing.p12 }}
                  title={t('createMissions:save')}
                  onPress={handleSubmit}
                />
              </View>
            );
          }}
        </Formik>
      </View>
    </ModalApp>
  );
};
