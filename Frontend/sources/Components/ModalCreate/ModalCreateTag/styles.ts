import { StyleSheet } from 'react-native';
import { color, fontSize, padding } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    title: {
      fontSize: fontSize.f18,
      fontWeight: '600',
    },
    button: {
      marginHorizontal: 16,
    },
    wrapContent: {
      padding: theme.spacing.p8,
    },
    flex1: {
      flex: 1,
    },
  });
  return styles;
};
