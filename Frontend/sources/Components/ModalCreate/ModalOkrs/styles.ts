import { StyleSheet } from 'react-native';
// import { color, fontSize, padding } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    checkBox: {
      backgroundColor: theme.color.white,
      // marginLeft: 16,
      // marginRight: 16,
    },
    textStyle: {
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      marginLeft: 0,
      flex: 1
    },
    addNew: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
    },
    textAdd: {
      fontSize: theme.fontSize.f16,
      marginLeft: theme.spacing.p8,
      color: theme.color.violet,
    },
    button: {
      marginHorizontal: theme.spacing.p16,
      marginBottom: theme.spacing.p16,
    },
    wrapContent: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      borderWidth: 0,
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
      flex: 1
    },
    okrWrap: {
      padding: theme.spacing.p16,
    },
    titleStyles:{
      fontSize: theme.fontSize.f14,
      fontWeight:'400',
      color: theme.color.black,
      marginBottom: theme.spacing.p8
    }
  });
  return styles;
};
