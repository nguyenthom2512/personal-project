import { useNavigation } from '@react-navigation/core';
import { FormikProps } from 'formik';
import React, { forwardRef, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  FlatList,
  Modal,
  SafeAreaView,
  TouchableOpacity,
  View,
} from 'react-native';
import { CheckBox, Icon } from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AppDropDown } from '../..';
import { IndicatorContext } from '../../../Context';
import useGetOKR from '../../../Hooks/getOkr';
import useGetOkrResult from '../../../Hooks/getOkrResult';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import { DataItem } from '../../AppDropDown';
import AppRadio from '../../AppRadio';
import AppText from '../../AppText';
import ModalApp from '../../ModalApp';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';

interface Props {
  onCloseModal: () => void;
  closeModal?: () => void;
  formik: FormikProps<any>;
  navigation?: any;
}

let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { onCloseModal = () => {}, formik } = props;
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const navigation = useNavigation();
  const { setLoading } = useContext(IndicatorContext);

  const [okrId, setOkr] = useState<number | string | null>(formik.values.okrId);
  const [result, setResult] = useState<number | string | null>(
    formik.values.result,
  );

  useEffect(() => {
    setOkr(formik.values.okrId);
  }, [formik.values.okrId]);

  useEffect(() => {
    setResult(formik.values.result);
  }, [formik.values.result]);

  const handleCloseModal = () => {
    if (!isSave) {
      setResult(formik.values.result);
      setOkr(formik.values.okrId);
    }
    isSave = false;
  };

  const [data, loading, error, refetch] = useGetOKR();
  const [dataResult, loadingResult, errorResult, refetchResult] =
    useGetOkrResult({ id: okrId });

  useEffect(() => {
    if (Boolean(error)) {
      Alert.alert(t('common:notice'), error);
    }
  }, []);

  useEffect(() => {
    if (okrId) {
      refetchResult();
    }
  }, [okrId]);

  const dataOkr: Array<DataItem> = (data || []).map((elm: any) => {
    return {
      label: elm.owner_okr,
      value: elm.id,
    };
  });

  const optionResult: Array<DataItem> = (dataResult?.okr_result || []).map(
    (elm: any) => {
      return {
        label: elm.key_result,
        value: elm.id,
      };
    },
  );

  const renderOption = () => (
    <View style={{ flex: 1 }}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={optionResult}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              onPress={() => {
                if (item.value === result) {
                  setResult('');
                } else {
                  setResult(item.value);
                }
              }}
              style={Styles.wrapContent}
            >
              <AppText style={Styles.textStyle}>{item.label}</AppText>
              <AppRadio
                iconRight
                checked={item.value === result}
                onPress={() => {
                  if (item.value === result) {
                    setResult('');
                  } else {
                    setResult(item.value);
                  }
                }}
              />
            </TouchableOpacity>
          );
        }}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:chooseOkrs')}
    >
      <SafeAreaView>
        {/* <View style={Styles.header}>
          <TouchableOpacity onPress={() => onCloseModal()}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>
            {t('createMissions:chooseOkrs')}
          </AppText>
          <View />
        </View> */}
        <View>
          <View style={Styles.okrWrap}>
            <AppDropDown
              loading={loading}
              value={formik.values.okrId}
              isSearch
              title={t('createMissions:selectOkr')}
              data={dataOkr}
              onChoose={(item) => setOkr(item.value)}
              titleStyles={Styles.titleStyles}
            />
          </View>
          {renderOption()}
          <TouchableOpacity
            onPress={() => {
              if (props.navigation) {
                props.navigation.navigate(t('tabbar:create'), {
                  screen: 'CreateOKR',
                  params: { id: '' },
                });
                props.closeModal && props.closeModal();
              } else {
                navigation.navigate('CreateOKR', { id: '' });
              }
              props.onCloseModal();
            }}
          >
            {dataResult?.okr_result?.length <= 5 ? (
              <View style={Styles.addNew}>
                <Feather name="plus" size={26} color={theme.color.violet} />
                <AppText style={Styles.textAdd}>
                  {t('createMissions:addNew')}
                </AppText>
              </View>
            ) : (
              <View />
            )}
          </TouchableOpacity>
          <AppButton
            title={t('createMissions:save')}
            onPress={() => {
              isSave = true;
              formik.setFieldValue('result', result);
              formik.setFieldValue('okrId', okrId);
              const resultItem = optionResult.find((el) => el.value === result);
              formik.setFieldValue('resultName', resultItem?.label || '');
              onCloseModal();
            }}
            containerStyle={Styles.button}
          />
        </View>
      </SafeAreaView>
    </ModalizeApp>
  );
});
