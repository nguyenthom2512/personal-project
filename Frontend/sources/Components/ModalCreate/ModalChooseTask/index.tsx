import { FormikProps } from 'formik';
import React, { forwardRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { SafeAreaView, View } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import useGetCreateOKR from '../../../Hooks/getDataCreateOKR';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import { DataItem } from '../../AppDropDown';
import AppDropDownMultiple from '../../AppDropDownMultiple';
import ModalizeApp from '../../ModalizeApp';
import styles from './style';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  isGetInfo?: boolean;
}
let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { onCloseModal = () => {}, formik, isGetInfo } = props;
  const [dataCreateOKR, loadingCreateOKR, errorCreateOKR, refetchCreateOKR] =
    useGetCreateOKR();
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  // process map user
  const mapDataUser = (array: Array<DataItem> = []) =>
    array.map((elm: DataItem) => elm.value || elm);

  const [responsible, setResponsible] = useState(
    mapDataUser(formik.values.user_responsible),
  );
  const [accountable, setAccountable] = useState(
    mapDataUser(formik.values.user_accountable),
  );
  const [inform, setInform] = useState(mapDataUser(formik.values.user_inform));
  const [consult, setConsult] = useState(
    mapDataUser(formik.values.user_consult),
  );
  const handleCloseModal = () => {
    if (!isSave) {
      setResponsible(mapDataUser(formik.values.user_responsible));
      setAccountable(mapDataUser(formik.values.user_accountable));
      setInform(mapDataUser(formik.values.user_inform));
      setConsult(mapDataUser(formik.values.user_consult));
    }
    isSave = false;
  };

  // create new data when values changes
  useEffect(() => {
    setResponsible(mapDataUser(formik.values.user_responsible));
  }, [formik.values.user_responsible]);
  useEffect(() => {
    setAccountable(mapDataUser(formik.values.user_accountable));
  }, [formik.values.user_accountable]);
  useEffect(() => {
    setInform(mapDataUser(formik.values.user_inform));
  }, [formik.values.user_inform]);
  useEffect(() => {
    setConsult(mapDataUser(formik.values.user_consult));
  }, [formik.values.user_consult]);

  const RAIC = (dataCreateOKR?.AppUser || []).map((elm) => ({
    value: elm?.id,
    label: elm?.full_name,
    avatar: elm?.img_url,
  }));

  const getValueFromId = (dataUser: any[]) => {
    const returnData = isGetInfo
      ? (RAIC || []).filter((elm: any) => {
          const index = dataUser.findIndex((id: number) => id === elm.value);
          return index >= 0;
        })
      : dataUser;

    return returnData;
  };

  const renderUser = () => (
    <View>
      <View style={{ paddingHorizontal: theme.spacing.p16, paddingTop: theme.spacing.p16 }}>
        <AppDropDownMultiple
          value={responsible}
          isSearch
          title={t('createOkr:Okr:userResponsible')}
          data={RAIC}
          onChoose={(value) => {
            setResponsible(value);
          }}
          titleStyles={Styles.titleStyles}
        />
        <AppDropDownMultiple
          value={accountable}
          isSearch
          title={t('createOkr:Okr:userAccountable')}
          data={RAIC}
          onChoose={(value) => setAccountable(value)}
          titleStyles={Styles.titleStyles}
        />
        <AppDropDownMultiple
          value={inform}
          isSearch
          title={t('createOkr:Okr:userInform')}
          data={RAIC}
          onChoose={(value) => setInform(value)}
          titleStyles={Styles.titleStyles}
        />
        <AppDropDownMultiple
          value={consult}
          isSearch
          title={t('createOkr:Okr:userConsult')}
          data={RAIC}
          onChoose={(value) => setConsult(value)}
          titleStyles={Styles.titleStyles}
        />
      </View>
    </View>
  );
  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => {
            onCloseModal();
          }}
        />
      }
      headerText={t('createMissions:assignTasks')}
    >
      <SafeAreaView>
        {/* <View style={Styles.header}>
          <TouchableOpacity onPress={onCloseModal}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>
            {t('createMissions:assignTasks')}
          </AppText>
          <View />
        </View> */}
        {renderUser()}
        <View style={Styles.wrapContent}>
          <AppButton
            title={t('createMissions:save')}
            onPress={() => {
              isSave = true;
              formik.setFieldValue(
                'user_responsible',
                getValueFromId(responsible),
              );
              formik.setFieldValue(
                'user_accountable',
                getValueFromId(accountable),
              );
              formik.setFieldValue('user_inform', getValueFromId(inform));
              formik.setFieldValue('user_consult', getValueFromId(consult));
              onCloseModal();
            }}
          />
        </View>
      </SafeAreaView>
    </ModalizeApp>
  );
});
