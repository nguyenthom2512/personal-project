import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    textStyle: {
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      marginLeft: 16,
    },
    // button: {
    //   marginHorizontal: 16
    // },
    wrapContent: {
      paddingHorizontal: 16,
      marginTop: 16,
      marginBottom: 16,
    },
    wrapSelection: {
      borderWidth: 0,
      backgroundColor: theme.color.wildSand,
    },
    infoUser: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    name: {
      fontSize: theme.fontSize.f14,
      fontWeight: '600',
      marginLeft: 8,
    },
    wrapItem: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      borderBottomWidth: 1,
      paddingBottom: 8,
    },
    ButtonRaic: {
      height: theme.fontSize.f24,
      width: theme.fontSize.f24,
      // backgroundColor: theme.color.violet,
      marginLeft: theme.fontSize.f8,
      borderWidth: 1,
      borderRadius: 2,
      alignItems: 'center',
      justifyContent: 'center',
      borderColor: theme.color.mercury,
    },
    styleKeyRS: {
      marginTop: theme.spacing.p8,
      padding: theme.spacing.p8,
      borderRadius: theme.spacing.p8,
      borderWidth: 1,
      borderColor: theme.color.Iron,
      marginVertical: theme.spacing.p24,
    },
    flex1: {
      flex: 1,
    },
    titleStyles:{
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color:theme.color.black,
      // marginVertical: theme.spacing.p8
    }
  });
  return styles;
};
