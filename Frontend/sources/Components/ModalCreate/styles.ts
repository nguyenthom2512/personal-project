import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: 'white',
      borderTopRightRadius: theme.spacing.p16,
      borderTopLeftRadius: theme.spacing.p16,
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: 'bold',
    },
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    okrBox: {
      borderWidth: 1.5,
      borderColor: theme.color.mercury,
      flexDirection: 'row',
      padding: theme.spacing.p8,
      alignItems: 'center',
    },
    daysBox: {
      borderWidth: 1.5,
      borderColor: theme.color.mercury,
      flexDirection: 'row',
      padding: theme.spacing.p8,
      marginLeft: 8,
      alignItems: 'center',
    },
    options: {
      flexDirection: 'row',
      marginVertical: 16,
    },
    placeholder: {
      fontSize: theme.fontSize.f16,
      borderWidth: 0,
      paddingHorizontal: 0,
    },
    containerStyle: {
      borderWidth: 0,
      paddingHorizontal: 0,
    },
    submission: {
      fontSize: theme.fontSize.f16,
      marginBottom: 16,
    },
    content: {
      paddingHorizontal: theme.spacing.p16,
    },
    line: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    actions: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
    },
    button: {
      width: 64,
    },
    addSubmissionWrap: {
      flexDirection: 'row',
      alignItems: 'flex-start',
    },
    containerSubStyle: {
      borderWidth: 0,
    },
    btnRemove: {
      padding: theme.spacing.p8,
      // backgroundColor: theme.color.graySilver,
      borderRadius: 4,
    },
    iconAdd: {
      marginTop: theme.spacing.p20,
    },
    input: {
      color: theme.color.dartBackground,
    },
    picture: {
      flexDirection: 'row',
      padding: theme.spacing.p8,
      marginLeft: 8,
      alignItems: 'center',
    },
  });
  return styles;
};
