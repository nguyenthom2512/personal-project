import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    checkBox: {
      backgroundColor: theme.color.white,
      borderWidth: 0,
      // marginLeft: 16,
      // marginRight: 16,
    },
    textStyle: {
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      marginLeft: theme.spacing.p16,
    },
    addNew: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
    },
    textAdd: {
      fontSize: theme.fontSize.f16,
      marginLeft: theme.spacing.p8,
      color: theme.color.violet,
    },
    button: {
      marginHorizontal: theme.spacing.p16,
      marginBottom: theme.spacing.p16,
    },
    wrapContent: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      // paddingHorizontal: 16,
      paddingRight: theme.spacing.p16,
      borderWidth: 0,
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
    },
    flex1: {
      flex: 1,
    },
  });
  return styles;
};
