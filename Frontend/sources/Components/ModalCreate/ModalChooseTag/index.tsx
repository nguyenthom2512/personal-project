import { FormikProps } from 'formik';
import React, { forwardRef, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, FlatList, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AppFlatlistEmpty } from '../..';
import { IndicatorContext } from '../../../Context';
import { device } from '../../../Helpers';
import useGetTodoTag from '../../../Hooks/getTodoTag';
import TodoTag from '../../../Services/TodoTag';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import AppCheckBox from '../../AppCheckBox';
import { DataItem } from '../../AppDropDown';
import AppText from '../../AppText';
import ModalizeApp from '../../ModalizeApp';
import ModalCreateTag from '../ModalCreateTag';
import styles from './styles';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  user: any;
  isGetInfo?: boolean;
}
let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const { onCloseModal = () => {}, formik, user, isGetInfo } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { setLoading } = useContext(IndicatorContext);
  const [dataTodoTag, loadingTag, errorTag, refetchTag] = useGetTodoTag();
  const [showCreateTag, setCreateTag] = useState(false);
  const [value, setValue] = useState<Array<any>>(
    formik.values.tag.map((tag: DataItem) => tag?.value || tag),
  );

  const todoTag: DataItem[] = (dataTodoTag || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.tag_name,
    };
  });

  useEffect(() => {
    setValue(formik.values.tag.map((tag: DataItem) => tag?.value || tag));
  }, [formik.values.tag]);

  const removeTag = (id: number | string) => {
    Alert.alert(t('common:notice'), t('createMissions:askTag'), [
      {
        text: t('common:cancel'),
        onPress: () => {},
      },
      {
        text: t('common:confirm'),
        onPress: () => {
          setLoading(true);
          TodoTag.deleteTodoTag(
            id,
            (err) => {
              let mess = err;
              if (err === 'UNKNOWN') {
                mess = t('common:unknownError');
              }
              Alert.alert(t('common:notice'), mess);
              setLoading(false);
            },
            () => {
              // onCloseModal();
              refetchTag();
              setLoading(false);
            },
          );
        },
      },
    ]);
  };

  const handleChange = (item: DataItem) => {
    setValue((previousState) => {
      if (previousState.includes(item.value)) {
        return previousState.filter((elm) => elm !== item.value);
      }
      return [...previousState, item.value];
    });
    isSave = false;
  };

  const getChecked = (item: DataItem) => {
    return value.includes(item.value);
  };

  const renderOption = () => (
    <View>
      <FlatList
        ListEmptyComponent={<AppFlatlistEmpty screenEmpty />}
        showsVerticalScrollIndicator={false}
        refreshing={loadingTag}
        onRefresh={() => refetchTag()}
        data={todoTag}
        renderItem={({ item, index }) => (
          <View style={Styles.wrapContent}>
            <View style={{ flex: 1 }}>
              <AppCheckBox
                checked={getChecked(item)}
                containerStyle={Styles.checkBox}
                title={item?.label || ''}
                textStyle={Styles.textStyle}
                onPress={() => handleChange(item)}
              />
            </View>
            <Icon
              name="trash-alt"
              type="font-awesome-5"
              color={theme.color.torchRed}
              size={20}
              onPress={() => removeTag(item.value)}
              tvParallaxProperties
            />
          </View>
        )}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );

  const handleCloseModal = () => {
    if (!isSave) {
      setValue(formik.values.tag.map((tag: DataItem) => tag?.value || tag));
    }
    isSave = false;
  };

  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={onCloseModal}
        />
      }
      headerText={t('createMissions:chooseTag')}
      disableSwipe={true}
      //   contentStyle={{ maxHeight: '70%' }}
      //   adjustToContentHeight={false}
      //   modalHeight={device.height * 0.48}
      headerComponent
      FooterComponent={() => {
        return (
          <View>
            <TouchableOpacity
              style={Styles.addNew}
              onPress={() => setCreateTag(true)}
            >
              <Feather name="plus" size={26} color={theme.color.violet} />
              <AppText style={Styles.textAdd}>
                {t('createMissions:addNew')}
              </AppText>
            </TouchableOpacity>
            <AppButton
              title={t('createMissions:save')}
              onPress={() => {
                isSave = true;
                formik.setFieldValue(
                  'tag',
                  isGetInfo
                    ? todoTag.filter((tag) => {
                        const index = value.findIndex(
                          (tagId) => tagId === tag.value,
                        );
                        return index >= 0;
                      })
                    : value,
                );
                onCloseModal();
              }}
              containerStyle={Styles.button}
            />
          </View>
        );
      }}
    >
      <ScrollView>
        {/* <View style={Styles.header}>
          <TouchableOpacity onPress={onCloseModal}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>
            {t('createMissions:chooseTag')}
          </AppText>
          <View />
        </View> */}
        {renderOption()}
        {showCreateTag && (
          <ModalCreateTag
            modalVisible={showCreateTag}
            onCloseModal={() => setCreateTag(false)}
            refetchTag={refetchTag}
            user={user}
          />
        )}
      </ScrollView>
    </ModalizeApp>
  );
});
