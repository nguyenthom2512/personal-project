import { StyleSheet } from 'react-native';
import { device } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
      // maxHeight: 400,
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    infoUser: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    name: {
      fontSize: theme.fontSize.f14,
      fontWeight: '600',
      marginLeft: theme.spacing.p8,
    },
    comment: {
      // marginTop: 8,
      // marginBottom: ,
    },
    time: {
      color: theme.color.gray,
    },
    line: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      marginVertical: theme.spacing.p12,
    },
    input: {
      backgroundColor: theme.color.whiteSmoke,
      padding: theme.spacing.p16,
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      flex: 8,
      borderTopLeftRadius: theme.spacing.p4,
      borderBottomLeftRadius: theme.spacing.p4,
      color:theme.color.black
    },
    iconFlight: {
      backgroundColor: theme.color.violet,
      padding: theme.spacing.p16,
      alignItems: 'center',
      flex: 1,
      borderTopRightRadius: theme.spacing.p4,
      borderBottomRightRadius: theme.spacing.p4,
    },
    commentWrapper: {
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p16,
      borderRadius: theme.spacing.p8,
      marginBottom: theme.spacing.p20,
    },
    iconEdit: {
      position: 'absolute',
      right: theme.spacing.p2,
      top: theme.spacing.p28,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
      paddingHorizontal: theme.spacing.p12,
      paddingVertical: theme.spacing.p4,
      
      // borderColor: theme.color.lightGray,
      // borderWidth: 1,
      borderRadius: theme.spacing.p4,
      backgroundColor: theme.color.white,
    },
    wrapEdit: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: 3,
      borderRadius: theme.spacing.p4,
      borderWidth: 1,
      backgroundColor: theme.color.violet,
      borderColor: theme.color.white,
      marginBottom: theme.spacing.p4,
      padding: theme.spacing.p2,
    },
    wrapButtonMin: {
      flexDirection: 'row',
      flex: 1,
      justifyContent: 'flex-end',
      marginBottom:theme.spacing.p8
    },
    ButtonMin: {
      backgroundColor: theme.color.graySilver,
      marginLeft: theme.spacing.p4,
      paddingHorizontal: theme.spacing.p4,
      borderRadius: theme.spacing.p4,
      borderWidth: 1,
    },
  });
  return styles;
};
