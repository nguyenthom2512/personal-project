import React, { forwardRef, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  FlatList,
  KeyboardAvoidingView,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { Avatar } from 'react-native-elements/dist/avatar/Avatar';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Flight from '../../../Assets/Svg/flight.svg';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppText from '../../AppText';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../Redux/Reducers';
import serviceUrls from '../../../Services/serviceUrls';
import moment from 'moment';
import Menu from '../../../Assets/Svg/menu.svg';
import Pen from '../../../Assets/Svg/pen.svg';
import Trash from '../../../Assets/Svg/trash.svg';
import { AppButton } from '../..';
import { FormikProps } from 'formik';
import { Comment } from '../../../interfaces/comment.interface';
import useGetCommentTodo from '../../../Hooks/getCommentTodo';
import commentService from '../../../Services/commentService';
import {
  getPriorityRequest,
  getTodoListRequest,
} from '../../../Redux/Actions/todoListActions';
import { isEmpty } from 'lodash';
import { backTodoComment } from '../../../Redux/Actions/todoCommentActions';
import { device } from '../../../Helpers';
import { noDateTodoRequest } from '../../../Redux/Actions/noDateTodoActions';
import { isIOS } from 'react-native-elements/dist/helpers';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

let page = 1;
const page_size = 20;
interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  idTodo?: number;
}

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const checkComment = useSelector((state: RootState) => state.commentReducer);
  const { onCloseModal = () => {}, formik, idTodo } = props;
  const [dataComment, loading, error, refechComment] =
    useGetCommentTodo(idTodo);
  const [data, setData] = useState<Comment[]>(dataComment);
  const [temp, setTempt] = useState(['']);

  const refFlastList = useRef<any>(null);

  const refreshTodo = () => {
    const start_time = moment().format('YYYY-MM-DD');
    const end_time = moment().add(1, 'days').format('YYYY-MM-DD');
    dispatch(getTodoListRequest({ start_time, end_time }));
    dispatch(noDateTodoRequest({ page, page_size }));
    dispatch(getPriorityRequest(''));
  };

  useEffect(() => {
    if (idTodo) {
      convertApi(dataComment?.results);
    }
    return () => {
      const start_time = moment().format('YYYY-MM-DD');
      const end_time = moment().add(1, 'days').format('YYYY-MM-DD');
      dispatch(getTodoListRequest({ start_time, end_time }));
    };
  }, [dataComment]);
  const convertApi = (param: any) => {
    const newValue = (param || []).map((e: any, index: number) => ({
      ...e,
      created_time: moment(e.created_time).format('HH:mm'),
    }));
    setData(newValue.reverse());
    const temptArr = (param || []).map((e: any) => e.title);
    formik.setFieldValue('comment_count', newValue.length);
    setTempt(temptArr.reverse());
  };
  useEffect(() => {
    if (checkComment.data) {
      setData([]);
      setTempt(['']);
      dispatch(backTodoComment());
    }
  }, [checkComment.data]);

  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const userData = useSelector((state: RootState) => state.userReducer);
  const userID = userData?.data?.user?.id;

  const userName = userData?.data?.user?.full_name;
  const userImg = userData?.data?.user?.img_url;
  const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';
  const avatar = userImg
    ? `${serviceUrls.url.IMAGE}/${userImg}`
    : DEFAULT_AVATAR;

  const [valueCmt, setValueCmt] = useState('');
  const [showMenu, setShowMenu] = useState<number | null | undefined>(null);
  const [detailMenu, setDetailMenu] = useState<number | null | undefined>(null);
  const [editCmt, setEditCmt] = useState<number | null | undefined>(null);

  const handleDelete = (index: number) => {
    Alert.alert(t('common:notice'), t('createMissions:askComment'), [
      {
        text: 'Cancel',
        onPress: () => {},
      },
      {
        text: 'Ok',
        onPress: () => {
          if (idTodo) {
            commentService.deleteTodoComment(
              data[index].id,
              (err) => {
                let mess = err;
                if (err === 'UNKNOWN') {
                  mess = t('common:unknownError');
                }
                Alert.alert(t('common:notice'), mess);
              },
              () => {
                refreshTodo();
                refechComment();
              },
            );
          } else {
            const newData = [...data];
            newData.splice(index, 1);
            setData(newData);
            const temptArr = newData.map((e) => e.title);
            setTempt(temptArr);
            handleSetFieldValue(newData);
          }
        },
      },
    ]);
  };
  const renderItem = ({ item, index }: { item: Comment; index: number }) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setShowMenu(index), setDetailMenu(undefined);
        }}
        // activeOpacity={0.5}
      >
        <View style={{ flex: 1 }}>
          <View style={[Styles.infoUser, { justifyContent: 'space-between' }]}>
            <View style={Styles.infoUser}>
              <Avatar source={{ uri: avatar }} rounded size={40} />
              <AppText style={Styles.name}>{userName}</AppText>
            </View>
            <TouchableOpacity
              onPress={() => {
                setDetailMenu(index);
                setShowMenu(undefined);
              }}
            >
              <Menu />
            </TouchableOpacity>
          </View>
          <TextInput
            autoFocus={true}
            multiline
            style={{
              color: theme.color.black,
              marginVertical: theme.spacing.p8,
            }}
            editable={editCmt == index}
            onChangeText={(text) => {
              setTempt((prev) => {
                const newTempt = [...prev];
                newTempt[index] = text;
                return newTempt;
              });
              // setData((prev) => {
              //   const newData = [...prev];
              //   newData[index].title = text;
              //   return newData;
              // });
              // setData((prev) =>
              //   prev.map((el, id) => {
              //     if (id === index) {
              //       return {
              //         ...el,
              //         title: text,
              //       };
              //     }
              //     return {
              //       ...el,
              //     };
              //   }),
              // );
            }}
            value={temp[index]}
          />
          {editCmt == index && (
            <View style={Styles.wrapButtonMin}>
              <TouchableOpacity
                onPress={() => {
                  setTempt((prev) => {
                    const newTempt = [...prev];
                    newTempt[index] = item.title;
                    return newTempt;
                  });
                  setEditCmt(undefined);
                }}
                style={{ marginRight: theme.spacing.p16 }}
              >
                <Ionicons name="close" size={24} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  const newData = [...data];
                  newData[index].title = temp[index];
                  setData((prev) => {
                    return newData;
                  });
                  setEditCmt(undefined);
                  if (idTodo) {
                    editValue(item.id, index);
                  } else handleSetFieldValue(newData);
                }}
              >
                <MaterialIcons name="done" size={24} />
              </TouchableOpacity>

              {/* <AppButton
                containerStyle={{ flex: 1, marginRight: theme.spacing.p8 }}
                onPress={() => {
                  const newData = [...data];
                  newData[index].title = temp[index];
                  setData((prev) => {
                    return newData;
                  });
                  setEditCmt(undefined);
                  if (idTodo) {
                    editValue(item.id, index);
                  } else handleSetFieldValue(newData);
                }}
                title={t('common:save')}
              />
              <AppButton
                containerStyle={{ flex: 1 }}
                cancel
                title={t('common:cancel')}
                onPress={() => {
                  setTempt((prev) => {
                    const newTempt = [...prev];
                    newTempt[index] = item.title;
                    return newTempt;
                  });
                  setEditCmt(undefined);
                }}
              /> */}
            </View>
          )}
          <AppText style={Styles.time}>{item?.created_time}</AppText>
          <View style={Styles.line} />
          {detailMenu == index && (
            <View style={Styles.iconEdit}>
              <TouchableOpacity
                onPress={() => {
                  setEditCmt(index);
                  setDetailMenu(undefined);
                }}
                style={Styles.wrapEdit}
              >
                <AntDesign name="edit" size={20} color={theme.color.black} />
                <AppText
                  style={{
                    marginLeft: theme.spacing.p4,
                    color: theme.color.black,
                    fontSize: theme.fontSize.f14,
                  }}
                >
                  {t('common:edit')}
                </AppText>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  handleDelete(index);
                  setDetailMenu(undefined);
                }}
                style={[Styles.wrapEdit, { marginTop: theme.spacing.p4 }]}
              >
                <AntDesign name="delete" size={16} color={theme.color.black} />
                <AppText
                  style={{
                    marginLeft: theme.spacing.p4,
                    color: theme.color.black,
                    fontSize: theme.fontSize.f14,
                  }}
                >
                  {t('common:delete')}
                </AppText>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </TouchableWithoutFeedback>
    );
  };
  const renderComment = () => {
    return (
      <View style={Styles.container}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={data || []}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };
  const handleSubmit = () => {
    const newTime = moment(new Date()).format('HH:mm');
    if (valueCmt) {
      const newValue = {
        title: valueCmt,
        user: userID,
        todo: idTodo,
      };
      const newData = [
        ...data,
        {
          title: valueCmt,
          created_time: newTime,
          user: userID,
        },
      ];
      setData(newData);
      setValueCmt('');
      const temptArr = newData.map((e) => e.title);
      setTempt(temptArr);
      if (idTodo) {
        commentService.createTodoComment(
          newValue,
          (err) => {
            let mess = err;
            if (err === 'UNKNOWN') {
              mess = t('common:unknownError');
            }
            Alert.alert(t('common:notice'), mess);
          },
          () => {
            refreshTodo();
            refechComment();
          },
        );
        formik.setFieldValue('comment_count', newData.length);
      } else {
        handleSetFieldValue(newData);
      }
    }
  };
  const handleSetFieldValue = (arrData?: Array<Comment>) => {
    const newData = (arrData || []).map(
      ({ created_time, user_data, id, ...remainData }) => remainData,
    );
    formik.setFieldValue('todo_comment', newData);
  };
  const editValue = (id: any, index: any) => {
    const newData = data.map(
      ({ created_time, user_data, id, ...remainData }) => remainData,
    );
    commentService.updateComment(
      id,
      newData[index],
      (err) => {
        let mess = err;
        if (err === 'UNKNOWN') {
          mess = t('common:unknownError');
        }
        Alert.alert(t('common:notice'), mess);
      },
      () => {
        refechComment();
      },
    );
  };
  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      keyboardAvoidingOffset={idTodo ? 64 : 0}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:comment')}
      disableSwipe={false}
      headerComponent
      //   adjustToContentHeight={false}
      //   modalHeight={device.height * 0.48}
      // modalHeight={device.height - 80}
      scrollViewProps={{ showsVerticalScrollIndicator: false }}
      FooterComponent={() => {
        return (
          <View style={Styles.commentWrapper}>
            <TextInput
              placeholder={t('createMissions:writeComment')}
              style={Styles.input}
              value={valueCmt}
              onChangeText={(e) => setValueCmt(e)}
              editable={true}
              placeholderTextColor={theme.color.gray}
            />
            <TouchableOpacity style={Styles.iconFlight} onPress={handleSubmit}>
              <Flight />
            </TouchableOpacity>
          </View>
        );
      }}
    >
      <SafeAreaView>{renderComment()}</SafeAreaView>
    </ModalizeApp>
  );
});
