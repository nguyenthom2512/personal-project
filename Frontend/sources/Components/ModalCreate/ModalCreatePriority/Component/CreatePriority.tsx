import { Formik, FormikHelpers } from 'formik';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { useSelector } from 'react-redux';
import * as yup from 'yup';
import { AppButton, AppInputField, ColorPicker } from '../../..';
import { RootState } from '../../../../Redux/Reducers';
import { useTheme } from '../../../../Theme';

interface CreatePriorityProp {
  onSubmit: (
    values: any,
    formikHelpers: FormikHelpers<any>,
  ) => void | Promise<any>;
}

const CreatePriority = (props: CreatePriorityProp) => {
  const { t } = useTranslation();
  const { theme } = useTheme();

  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const validate = yup.object().shape({
    priority_name: yup.string().required(t('common:required')),
    color: yup.string().required(t('common:required')),
  });

  return (
    <Formik
      initialValues={{
        priority_name: '',
        color: '#ffffff',
        user: user,
        priority_level: '',
      }}
      validationSchema={validate}
      onSubmit={props.onSubmit}
    >
      {({ values, handleSubmit, setFieldValue }) => {
        return (
          <View>
            <AppInputField
              name="priority_name"
              title={t('createMissions:priorityName')}
              placeholder={t('createMissions:priorityName')}
            />
            <AppInputField
              name="color"
              title={t('createMissions:priorityColor')}
              placeholder={t('createMissions:priorityColor')}
              customInputStyle={{ backgroundColor: `${values.color}` }}
              editable={false}
            />
            <View style={{ height: 300 }}>
              <ColorPicker
                color={values.color}
                thumbSize={40}
                sliderSize={40}
                noSnap={false}
                row={false}
                onColorChange={(elm: string) => setFieldValue('color', elm)}
                onColorChangeComplete={(elm: string) =>
                  setFieldValue('color', elm)
                }
                swatchesLast={true}
                swatches={true}
                discrete={false}
                swatchesOnly={false}
                shadeSliderThumb={false}
                shadeWheelThumb={false}
                autoResetSlider={false}
              />
            </View>
            <AppButton
              containerStyle={{ marginTop: theme.spacing.p12 }}
              title={t('createMissions:save')}
              onPress={handleSubmit}
            />
          </View>
        );
      }}
    </Formik>
  );
};

export default CreatePriority;
