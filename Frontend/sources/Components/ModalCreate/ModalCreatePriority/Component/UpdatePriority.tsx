import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, StyleSheet, TouchableOpacity, View } from 'react-native';
import DraggableFlatList, {
  RenderItemParams,
  ScaleDecorator,
} from 'react-native-draggable-flatlist';
import { useSelector } from 'react-redux';
import { AppButton, AppText } from '../../..';
import { device } from '../../../../Helpers';
import { Priority } from '../../../../interfaces/priority.interface';
import { RootState } from '../../../../Redux/Reducers';
import priorityService from '../../../../Services/priorityService';
import { Theme, useTheme, useThemeAwareObject } from '../../../../Theme';

type Item = {
  // label: string;
  // height: number;
  // width: number;
  color: string;
  id: string | number;
  priority_level: string | number;
  priority_name: string;
  user: string;
};

interface UpdatePriorityProps {
  dataPriority: Priority[];
  refetch: () => void;
  onCloseModal: () => void;
}

const UpdatePriority = (props: UpdatePriorityProps) => {
  const Styles = useThemeAwareObject(styles);

  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const initialData: Item[] = _.sortBy(
    props.dataPriority || [],
    'priority_level',
  ).map((d) => {
    return {
      color: d.color,
      id: d.id,
      priority_level: d.priority_level,
      priority_name: d.priority_name,
      user,
    };
  });
  const [dataPriority, setDataPriority] = useState(initialData || []);
  const { t } = useTranslation();
  const { theme } = useTheme();

  useEffect(() => {
    props.refetch();
  }, []);

  useEffect(() => {
    setDataPriority(initialData);
  }, [props.dataPriority.length]);

  const renderItem = ({ item, drag, isActive }: RenderItemParams<Item>) => {
    return (
      <ScaleDecorator activeScale={0.95}>
        <TouchableOpacity
          activeOpacity={1}
          style={[
            Styles.rowItem,
            {
              backgroundColor: item.color,
            },
          ]}
          onLongPress={drag}
        >
          <AppText style={Styles.text}>{item.priority_name}</AppText>
        </TouchableOpacity>
      </ScaleDecorator>
    );
  };

  const onDragEnd = (dragData: any) => {
    const newDataSort = [...dragData.data].map((elm, index) => {
      return { ...elm, priority_level: index };
    });
    setDataPriority(newDataSort);
  };

  const onSave = () => {
    priorityService.updatePriority(
      dataPriority,
      (error) => {
        Alert.alert(t('common:notice', error));
      },
      () => {
        props.onCloseModal();
        props.refetch();
      },
    );
  };

  return (
    <View style={Styles.modalContent}>
      <View style={{ flex: 1 }}>
        <DraggableFlatList
          data={dataPriority}
          onDragEnd={onDragEnd}
          keyExtractor={(item) => item.id.toString()}
          renderItem={renderItem}
        />
      </View>
      <AppButton
        containerStyle={{ marginTop: theme.spacing.p12 }}
        title={t('createMissions:save')}
        onPress={onSave}
      />
    </View>
  );
};

export default UpdatePriority;

const styles = (theme: Theme) =>
  StyleSheet.create({
    rowItem: {
      paddingVertical: theme.spacing.p16,
      paddingHorizontal: theme.spacing.p16,
      borderRadius: 4,
      width: '100%',
      marginVertical: theme.spacing.p4,
    },
    text: {
      color: theme.color.white,
      fontSize: theme.fontSize.f16,
    },
    modalContent: {
      height: device.height * 0.7,
    },
  });
