import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, SafeAreaView, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { IndicatorContext } from '../../../Context';
import useGetPriority from '../../../Hooks/getPriority';
import priorityService from '../../../Services/priorityService';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import ModalApp from '../../ModalApp';
import CreatePriority from './Component/CreatePriority';
import UpdatePriority from './Component/UpdatePriority';
import styles from './style';
interface Props {
  modalVisible: boolean;
  onCloseModal: () => void;
  refetchPriority?: () => void;
}
export default (props: Props) => {
  const {
    modalVisible = false,
    onCloseModal = () => {},
    refetchPriority = () => {},
  } = props;

  const theme = useTheme();
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { setLoading } = useContext(IndicatorContext);

  const [createSuccess, setCreateSuccess] = useState(true);

  const [dataPriority, loadingPriority, errorPriority, refetch] =
    useGetPriority();

  const onSubmit = async (value: any) => {
    value.priority_level = dataPriority.length + 1;

    // setLoading(true);
    priorityService.createTodoTag(
      value,
      (err) => {
        let mess = err;
        if (err === 'UNKNOWN') {
          mess = t('common:unknownError');
        }
        Alert.alert(t('common:notice'), mess);
        setLoading(false);
      },
      () => {
        setCreateSuccess(false);
        // onCloseModal();
        refetchPriority();
        setLoading(false);
      },
    );
  };

  return (
    <ModalApp
      visible={modalVisible}
      avoidKeyboard={false}
      //@ts-ignore
      onClickOutside={onCloseModal}
      iconLeft={
        <Icon
          name="x"
          type="feather"
          color={theme.theme.color.gray}
          tvParallaxProperties
          onPress={onCloseModal}
        />
      }
      headerText={t('createMissions:addPriority')}
    >
      <View style={Styles.wrapContent}>
        {createSuccess ? (
          // <UpdatePriority />

          <CreatePriority onSubmit={onSubmit} />
        ) : (
          <UpdatePriority
            dataPriority={dataPriority}
            refetch={refetch}
            onCloseModal={onCloseModal}
          />
          // <CreatePriority onSubmit={onSubmit} />
        )}
      </View>
    </ModalApp>
  );
};
