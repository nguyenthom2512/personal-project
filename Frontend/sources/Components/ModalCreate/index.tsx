import { useNavigation } from '@react-navigation/native';
import { FormikProps } from 'formik';
import moment from 'moment';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
// import { AppImage, AppInputField, ErrorMessages } from '../index';
import Clock from '../../Assets/Svg/clock.svg';
import Date from '../../Assets/Svg/date.svg';
// import File from '../../Assets/Svg/file.svg';
import Message from '../../Assets/Svg/message.svg';
import Tag from '../../Assets/Svg/tag.svg';
import User from '../../Assets/Svg/user.svg';
import { useTheme, useThemeAwareObject } from '../../Theme';
import AppButton from '../AppButton';
import AppText from '../AppText';
import styles from './styles';
import AntDesign from 'react-native-vector-icons/AntDesign';
// import FastImage from 'react-native-fast-image';
import { imageAPILink } from '../../Helpers/constant';
import AppInputField from '../AppInputField';
import ErrorMessages from '../ErrorMessages';
import AppImage from '../AppImage';
import { useDispatch } from 'react-redux';
import { resetCommentTodo } from '../../Redux/Actions';

import { Avatar, Badge, Icon, withBadge } from 'react-native-elements';
import Fontisto from 'react-native-vector-icons/Fontisto';

interface Props {
  onChosseOkr: () => void;
  onChooseDays: () => void;
  pressAction: (key: string) => void;
  onChooseImage: () => void;
  formik: FormikProps<any>;
  handleClose?: () => void;
}

export default (props: Props) => {
  const {
    onChosseOkr,
    onChooseDays,
    pressAction,
    formik,
    onChooseImage,
    handleClose,
  } = props;

  const dispatch = useDispatch();

  // const [dataTodo, loadingTodo, errorTodo, refetchTodo] = useGetTodoSelection();

  const navigation = useNavigation();
  const onClose = () => {
    dispatch(resetCommentTodo.resetTodoComment());
    formik.resetForm();
    Keyboard.dismiss();
    if (!!handleClose) {
      handleClose();
    } else {
      navigation.goBack();
    }
  };

  // const todoSelection = (dataTodo || []).map((elm) => {a
  //   return {
  //     value: elm.id,
  //     label: elm.todo_name,
  //   };
  // });
  // const { setLoading } = useContext(IndicatorContext);
  // useEffect(() => {
  //   setLoading(loadingTodo);
  // }, [loadingTodo]);

  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const renderMissions = (placeholder: string, name: string) => {
    return (
      <AppInputField
        name={name}
        placeholder={placeholder}
        containerStyle={Styles.containerStyle}
        customInputStyle={Styles.placeholder}
      />
    );
  };
  const renderSubMissions = (placeholder: string, name: string) => {
    return (
      <AppInputField
        name={name}
        placeholder={placeholder}
        // containerStyle={Styles.containerSubStyle}
        customInputStyle={Styles.containerSubStyle}
      />
    );
  };

  const userData =
    formik.values.user_accountable?.length +
    formik.values.user_consult?.length +
    formik.values.user_inform.length +
    formik.values.user_responsible.length;

  const listIcon = () => [
    {
      icon: (
        <View>
          <Tag />
          {renderBadge(formik.values.tag?.length)}
        </View>
      ),
      key: 'tag',
    },
    {
      icon: (
        <View>
          {formik.errors?.priority ? (
            <Feather name="flag" size={24} color={theme.color.redAlert } />
          ) : (
            <Ionicons
              name="md-flag"
              size={24}
              color={formik.values?.priority_color?.color || theme.color.gray }
            />
          )}
        </View>
      ),
      key: 'priority',
    },
    {
      icon: (
        <View>
          <Fontisto
            name="stopwatch"
            color={
              formik.values?.start_time && formik.values.end_time
                ? theme.color.greenBox
                : theme.color.gray
            }
            size={24}
          />
        </View>
      ),
      key: 'clock',
    },
    {
      icon: (
        <View>
          <Message />
          {renderBadge(formik.values.todo_comment?.length)}
        </View>
      ),
      key: 'comment',
    },
    {
      icon: (
        <View>
          <User />
          {renderBadge(userData)}
        </View>
      ),
      key: 'task',
    },
    {
      icon: (
        <Feather name="more-horizontal" size={24} color={theme.color.gray} />
      ),
      key: 'more',
    },
  ];

  const removeSubmission = useCallback(
    (index) => {
      formik.setFieldValue(
        'check_list',
        formik.values.check_list.filter(
          (elm: any, ind: number) => ind !== index,
        ),
      );
    },
    [formik.values.check_list],
  );

  const addSubmission = useCallback(() => {
    formik.setFieldValue('check_list', [...formik.values.check_list, '']);
  }, [formik.values.check_list]);

  const renderBadge = (value: any) => {
    return (
      <Badge
        value={value}
        status="primary"
        containerStyle={{
          position: 'absolute',
          top: -theme.spacing.p8,
          right: -theme.spacing.p8,
        }}
        badgeStyle={{ backgroundColor: theme.color.violet }}
      />
    );
  };

  return (
    <KeyboardAvoidingView
      style={{ flex: 1, justifyContent: 'flex-end' }}
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <View style={Styles.container}>
        <View style={Styles.header}>
          <TouchableOpacity onPress={onClose}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>{t('homepage:createMissions')}</AppText>
          <View />
        </View>
        <View style={Styles.content}>
          <View style={Styles.options}>
            <View style={{ flex: 2 }}>
              <TouchableOpacity
                style={Styles.okrBox}
                onPress={() => {
                  onChosseOkr();
                  Keyboard.dismiss();
                }}
              >
                <Feather name="folder" size={18} color={theme.color.gray} />
                <AppText
                  style={{ flex: 1, paddingLeft: theme.spacing.p8 }}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {formik.values.resultName
                    ? formik.values.resultName
                    : t('homepage:chooseOkr')}
                </AppText>
              </TouchableOpacity>
              <ErrorMessages name="result" />
            </View>
            <View style={{ flex: 2 }}>
              <TouchableOpacity
                style={Styles.daysBox}
                onPress={() => {
                  onChooseDays();
                  Keyboard.dismiss();
                }}
              >
                <Date />
                <AppText
                  style={{ paddingLeft: theme.spacing.p8 }}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {formik.values.date
                    ? moment(formik.values.date).format('DD-MM-YYYY')
                    : t('homepage:chooseDay')}
                </AppText>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1, alignItems: 'center' }}>
              <TouchableOpacity
                style={Styles.picture}
                onPress={() => {
                  onChooseImage(), Keyboard.dismiss();
                }}
              >
                {props.formik.values.img_url ? (
                  <AppImage
                    resizeMode="cover"
                    source={{
                      uri: imageAPILink(props.formik.values.img_url),
                    }}
                    imageStyle={{ height: 32, width: 32, borderRadius: 8 }}
                  />
                ) : (
                  <AntDesign
                    name="picture"
                    size={24}
                    color={theme.color.gray}
                  />
                )}
                <AppText
                  style={{ paddingLeft: theme.spacing.p8 }}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                ></AppText>
              </TouchableOpacity>
            </View>
          </View>
          {renderMissions(t('homepage:missionName'), 'todo_name')}
          {renderMissions(t('homepage:missionDescription'), 'description')}
          <AppText style={Styles.submission}>
            {t('homepage:missionSub')}
          </AppText>
          <View style={Styles.addSubmissionWrap}>
            <Feather
              name="plus"
              size={20}
              color={theme.color.gray}
              style={Styles.iconAdd}
              onPress={addSubmission}
            />

            <View style={{ flex: 1 }}>
              {formik.values.check_list.map((elm: any, index: number) => (
                <View
                  key={index}
                  style={[Styles.addSubmissionWrap, { alignItems: 'center' }]}
                >
                  <View style={{ flex: 1 }}>
                    {renderSubMissions(
                      t('homepage:addMission'),
                      `check_list[${index}]`,
                    )}
                  </View>
                  {formik.values.check_list.length > 1 && (
                    <TouchableOpacity
                      style={Styles.btnRemove}
                      onPress={() => removeSubmission(index)}
                    >
                      <Feather
                        name="minus"
                        size={20}
                        color={theme.color.gray}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              ))}
            </View>
          </View>
        </View>
        <View style={Styles.line} />
        <View style={Styles.actions}>
          {listIcon().map((el, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                pressAction(el.key);
                Keyboard.dismiss();
              }}
            >
              {el.icon}
            </TouchableOpacity>
          ))}
          <AppButton
            title={t('homepage:add')}
            onPress={() => {
              formik.errors?.priority &&
                Alert.alert(
                  t('common:notice'),
                  t('createMissions:requiredPriority'),
                );
              formik.handleSubmit();
            }}
            containerStyle={Styles.button}
          />
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};
