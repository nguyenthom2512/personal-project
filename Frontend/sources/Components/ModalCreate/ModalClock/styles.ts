import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    renderTime: {
      paddingHorizontal: 16,
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      color: theme.color.charade,
      opacity: 0.5,
      marginTop: theme.spacing.p16,
    },
    containerTime: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    wrapContent: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingVertical: theme.spacing.p12,
      paddingHorizontal: theme.spacing.p12,
      borderWidth: 1,
      borderColor: theme.color.mercury,
      borderRadius: 4,
      marginRight: theme.spacing.p12,
      marginVertical: theme.spacing.p8,
    },
    txtAclock: {
      color: theme.color.charade,
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
    },
    button: {
      marginHorizontal: theme.spacing.p16,
      marginVertical: theme.spacing.p8,
    },
  });
  return styles;
};
