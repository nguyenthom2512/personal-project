import { FormikProps } from 'formik';
import _, { isEmpty } from 'lodash';
import moment from 'moment';
import React, { forwardRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert, SafeAreaView,
  TouchableOpacity,
  View
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AppButton } from '../..';
import Clock from '../../../Assets/Svg/clockViolet.svg';
import isDarkMode from '../../../Helpers/darkmode';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppText from '../../AppText';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';
interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  isOptional?: boolean;
}

let isStart = false;
let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { onCloseModal = () => {}, formik, isOptional = false } = props;
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const [show, setShow] = useState(false);
  const [startTime, setStartTime] = useState(formik.values.start_time || '');
  const [endTime, setEndTime] = useState(formik.values.end_time || '');

  useEffect(() => {
    setStartTime(formik.values.start_time);
  }, [formik.values.start_time]);

  useEffect(() => {
    setEndTime(formik.values.end_time);
  }, [formik.values.end_time]);

  const showDatepicker = (start: boolean) => {
    setShow(true);
    isStart = start;
  };

  const handleCheckDay = (isStartTime: any) => {
    if (
      !isEmpty(formik.values.date) &&
      !formik.values.date.includes('Invalid date')
    ) {
      showDatepicker(isStartTime);
    } else {
      Alert.alert(
        t('common:notice'),
        t('createMissions:selectDateBeforeSelectTime'),
      );
    }
  };

  const hideDatePicker = () => {
    setShow(false);
  };

  const getLogicRequire = (start: string, end: string) => {
    if (isOptional) {
      return (start && !end) || (end && !start);
    }
    return !start || !end;
  };

  const handleCloseModal = () => {
    if (!isSave) {
      setStartTime(formik.values.start_time);
      setEndTime(formik.values.end_time);
    }
    isSave = false;
  };

  const handleConfirm = (time: Date) => {
    let selectedDate = '';
    const hour = moment(time).get('hour');
    const minute = moment(time).get('minute');
    const second = moment(time).get('second');
    const newTime = moment(time).format('HH:mm');
    const compareTime = (start: string, end: string, checkStart: boolean) => {
      if (start < end) {
        {
          checkStart ? setStartTime(newTime) : setEndTime(newTime);
        }
      } else {
        Alert.alert('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
        {
          checkStart ? setStartTime('') : setEndTime('');
        }
      }
    };

    if (isStart) {
      {
        _.isEmpty(endTime)
          ? setStartTime(newTime)
          : compareTime(newTime, endTime, true);
      }
    } else {
      compareTime(startTime, newTime, false);
    }
    setShow(false);
  };
  const deleteTime = (isStartTime: boolean) => {
    if (isStartTime) {
      setStartTime('');
    } else {
      setEndTime('');
    }
  };

  const renderTime = (title: any, moment: any, isStartTime: boolean) => (
    <View style={Styles.renderTime}>
      <View>
        <AppText style={Styles.title}>{title}</AppText>
        <View style={Styles.containerTime}>
          <TouchableOpacity
            style={Styles.wrapContent}
            onPress={() => handleCheckDay(isStartTime)}
          >
            <AppText style={Styles.txtAclock}>{moment}</AppText>
            <Clock />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => deleteTime(isStartTime)}>
            <Ionicons name="close" size={24} color={theme.color.charade} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:chooseTime')}
    >
      <SafeAreaView>
        {renderTime(
          t('createMissions:timeStart'),
          !_.isEmpty(startTime) ? startTime : 'HH:mm',
          true,
        )}
        {renderTime(
          t('createMissions:timeEnd'),
          !_.isEmpty(endTime) ? endTime : 'HH:mm',
          false,
        )}
        <AppButton
          title={t('createMissions:save')}
          onPress={() => {
            isSave = true;
            if (getLogicRequire(startTime, endTime)) {
              Alert.alert(t('common:notice'), t('common:noticeInformation'));
            } else {
              formik.setFieldValue('start_time', startTime);
              formik.setFieldValue('end_time', endTime);
              onCloseModal();
            }
          }}
          containerStyle={Styles.button}
        />
        {show && (
          <DateTimePickerModal
            locale="vi"
            is24Hour
            themeVariant="light"
            isDarkModeEnabled={isDarkMode()}
            isVisible={show}
            mode="time"
            onConfirm={(time) => handleConfirm(time)}
            onCancel={hideDatePicker}
          />
        )}
      </SafeAreaView>
    </ModalizeApp>
  );
});
