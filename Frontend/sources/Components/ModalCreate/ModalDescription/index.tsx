import { FormikProps } from 'formik';
import React, { forwardRef } from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, TextInput, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { device } from '../../../Helpers';
import { Theme, useTheme } from '../../../Theme';
import AppInputField from '../../AppInputField';
import ModalizeApp from '../../ModalizeApp';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useState } from 'react';
import { AppText } from '../../../Components';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
}

let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const { onCloseModal = () => {}, formik } = props;
  const { theme } = useTheme();
  const Styles = themeStyle(theme);
  const [description, setDescription] = useState(
    formik.values.description || '',
  );

  const handleCloseModal = () => {
    if (!isSave) {
      setDescription(formik.values.description || '');
    }
    isSave = false;
  };

  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconRight={
        <AppText style={{ color: theme.color.mineShaft }}>
          {t('common:save')}
        </AppText>
        // <MaterialIcons
        //   name="done"
        //   size={24}
        //   color={theme.color.mineShaft}
        //   onPress={() => {
        //     isSave = true;
        //     formik.setFieldValue('description', description);
        //     onCloseModal();
        //   }}
        // />
      }
      onRightPress={() => {
        isSave = true;
        formik.setFieldValue('description', description);
        onCloseModal();
      }}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => {
            onCloseModal();
          }}
        />
      }
      headerText={t('todoDetail:description')}
    >
      {/* <AppInputField
        multiline
        name={`process_note`}
        customInputStyle={Styles.inputStyles}
        onChangeCustom={setDescription}
        placeholder={
          formik.values.description || t('todoDetail:thisIsDescription')
        }
      /> */}
      <TextInput
        multiline
        value={description}
        onChangeText={setDescription}
        placeholder={t('todoDetail:description')}
        placeholderTextColor={theme.color.gray}
        style={Styles.input}
        textAlignVertical="top"
      />
    </ModalizeApp>
  );
});

const themeStyle = (theme: Theme) => {
  return StyleSheet.create({
    inputStyles: {
      borderWidth: 0,
      height: device.height / 2.5,
    },
    input: {
      color: theme.color.black,
      height: device.height / 2.5,
      paddingHorizontal: theme.spacing.p12,
      borderColor: theme.color.holderPlace,
      fontSize: theme.fontSize.f14,
      backgroundColor: theme.color.backgroundColor,
    },
  });
};
