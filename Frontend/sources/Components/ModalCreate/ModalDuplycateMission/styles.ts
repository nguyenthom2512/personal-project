import { StyleSheet } from 'react-native';
import { responsivePixel } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flex1: {
      flex: 1,
    },
    selectValue0: {
      backgroundColor: theme.color.white,
      minHeight: responsivePixel(42),
      justifyContent: 'center',
      paddingHorizontal: theme.spacing.p12,
    },
    selectValue1: {
      backgroundColor: theme.color.mystic,
      minHeight: responsivePixel(42),
      justifyContent: 'center',
      paddingHorizontal: theme.spacing.p12,
    },
    wrapContent: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      paddingRight: theme.spacing.p16,
      borderWidth: 0,
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
    },
    label: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f16,
      flex: 1,
      fontWeight: '400',
    },
    button: {
      marginHorizontal: theme.spacing.p16,
      marginBottom: theme.spacing.p16,
    },
  });
  return styles;
};
