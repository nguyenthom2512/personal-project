import { FormikProps } from 'formik';
import _, { isEmpty } from 'lodash';
import React, { forwardRef, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, FlatList, SafeAreaView, TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AppButton, AppText } from '../../../Components';
import { IndicatorContext } from '../../../Context';
import { device } from '../../../Helpers';
import useGetTodoSelection from '../../../Hooks/getTodoSelection';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import { DataItem } from '../../AppDropDown';
import AppRadio from '../../AppRadio';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
}
export const RAIC_TYPE = {
  Responsible: 0,
  Accountable: 1,
  Inform: 2,
  Consult: 3,
};
let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { onCloseModal = () => {}, formik } = props;
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { setLoading } = useContext(IndicatorContext);
  const [param, setParam] = useState({
    page: 1,
    page_size: 20,
  });

  const [dataTodo, loadingTodo, errorTodo, refetchTodo] =
    useGetTodoSelection(param);

  const [chooseMisson, setChooseMisson] = useState<number | string>();
  const [itemMisson, setItemMisson] = useState<any>({});

  useEffect(() => {
    !loadingTodo && refetchTodo();
  }, [param]);

  const convertRaic = (params: any) => {
    let user_responsible: any = []; //lên
    let user_accountable: any = [];
    let user_inform: any = [];
    let user_consult: any = [];
    (params?.raic_user || []).map((e: any) => {
      switch (e.raic_type) {
        case RAIC_TYPE.Responsible:
          user_responsible.push(e.user);
          break;
        case RAIC_TYPE.Accountable:
          user_accountable.push(e.user);
          break;
        case RAIC_TYPE.Inform:
          user_inform.push(e.user);
          break;
        case RAIC_TYPE.Consult:
          user_consult.push(e.user);
          break;
        default:
          break;
      }
    });
    // params['raic_user'] = [];
    params['user_responsible'] = user_responsible;
    params['user_accountable'] = user_accountable;
    params['user_inform'] = user_inform;
    params['user_consult'] = user_consult;
  };

  const handleData = (item: any) => {
    convertRaic(item);
    isSave = false;
    let tag: any = [];
    {
      item.tag.map((elm: any) => {
        tag.push(elm.id);
      });
    }
    formik.setValues({
      ...formik.values,
      okrId: item.okr_id,
      result: item.result,
      resultName: item.result_name,
      todo_name: item.todo_name,
      description: item.description,
      tag: tag,
      priority: item.priority.id,
      priority_color: item.priority,
      //   date: item.start_time,
      //   start_time: item.start_time,
      //   end_time: item.end_time,
      date: '',
      start_time: '',
      end_time: '',
      user_responsible: item.user_responsible,
      user_accountable: item.user_accountable,
      user_inform: item.user_inform,
      user_consult: item.user_consult,
    });
    setTimeout(() => {
      onCloseModal();
    }, 200);
  };

  const handleCloseModal = () => {
    if (!isSave) {
      setChooseMisson('');
    }
    isSave = false;
  };

  const getStyle = (itemProps: { item: any; index: number }) => {
    if (itemProps.index % 2 === 0) {
      return Styles.selectValue0;
    } else {
      return Styles.selectValue1;
    }
  };

  const optionMisson: Array<DataItem> = (dataTodo?.results || []).map(
    (elm: any) => {
      return {
        label: elm.todo_name,
        value: elm.id,
      };
    },
  );

  const setChecked = (item: any) => {
    if (item.value === chooseMisson) {
      setChooseMisson('');
      setItemMisson({});
    } else {
      setChooseMisson(item.value);
      const dataNew = dataTodo?.results.find((e) => e.id == item.value);
      setItemMisson(dataNew);
    }
  };

  const renderItem = (itemProps: { item: any; index: number }) => {
    return (
      <TouchableOpacity
        onPress={() => setChecked(itemProps.item)}
        style={Styles.wrapContent}
      >
        <AppRadio
          iconRight
          checked={itemProps.item.value === chooseMisson}
          onPress={() => setChecked(itemProps.item)}
        />
        <AppText style={Styles.label}>{itemProps.item.label}</AppText>
      </TouchableOpacity>
    );
  };
  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:duplicateMission')}
    >
      <SafeAreaView style={{ maxHeight: (device.height * 2) / 3 }}>
        <FlatList
          data={optionMisson}
          keyExtractor={(item, index) => index.toString()}
          renderItem={renderItem}
          onEndReachedThreshold={0.1}
          onEndReached={() => {
            if (param.page < Math.ceil(dataTodo?.count / param.page_size)) {
              setParam({ ...param, page: param.page + 1 });
            }
          }}
        />
        <AppButton
          title={t('createMissions:duplicateMission')}
          onPress={() => handleData(itemMisson)}
          containerStyle={Styles.button}
          disabled={!isEmpty(itemMisson) ? false : true}
        />
      </SafeAreaView>
    </ModalizeApp>
  );
});
