import { FormikProps } from 'formik';
import React, { forwardRef, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, SafeAreaView, TouchableOpacity, View } from 'react-native';
import {
  Asset,
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary,
} from 'react-native-image-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { AppImage } from '../..';
import Upload from '../../../Assets/Svg/upload.svg';
import { device } from '../../../Helpers';
import { imageAPILink } from '../../../Helpers/constant';
import { createFormImage } from '../../../Helpers/convertData';
import { uploadImage } from '../../../Services/serviceHandle';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import AppText from '../../AppText';
import ModalizeApp from '../../ModalizeApp';
import styles from './styles';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
}

const configImagePicker: ImageLibraryOptions = {
  selectionLimit: 1,
  mediaType: 'photo',
};

let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const { onCloseModal = () => {}, formik } = props;
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const [imageItem, setImageItem] = useState<Asset>();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (!formik.values?.img_url) {
      setImageItem(undefined);
    }
  }, [formik.values?.img_url]);

  const onClickAddImage = () => {
    launchImageLibrary(configImagePicker, (res: ImagePickerResponse) => {
      if (res.didCancel) {
        return;
      }
      if (res.assets) {
        const item: Asset = res?.assets[0];
        setImageItem(item);
      }
    });
  };
  const handleCloseModal = () => {
    if (!isSave) {
      setImageItem(props.formik.values.img_url);
    }
    isSave = false;
  };

  return (
    <ModalizeApp
      ref={ref}
      onClosed={handleCloseModal}
      onRequestClose={onCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:chooseImage')}
    >
      <SafeAreaView>
        {/* <View style={Styles.header}>
          <TouchableOpacity disabled={loading} onPress={onCloseModal}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>
            {t('createMissions:chooseImage')}
          </AppText>
          <View />
        </View> */}
        <View style={Styles.imageStyles}>
          <TouchableOpacity
            onPress={onClickAddImage}
            style={Styles.imageContainer}
            disabled={loading}
          >
            {imageItem?.uri || props.formik.values.img_url ? (
              <AppImage
                source={{
                  uri: imageItem?.uri
                    ? imageItem.uri
                    : imageAPILink(props.formik.values?.img_url),
                }}
                imageStyle={Styles.image}
              />
            ) : (
              <Upload />
            )}
            <AppText style={Styles.text}>
              {t('createMissions:chooseFile')}
            </AppText>
          </TouchableOpacity>
          <AppButton
            disabled={loading}
            title={t('createMissions:save')}
            onPress={() => {
              isSave = true;
              if (!imageItem) {
                onCloseModal();
                return;
              }
              setLoading(true);
              uploadImage(createFormImage(imageItem))
                .then((res) => {
                  if (res.error) {
                    return Alert.alert(t('common:notice'), res.errorMessage);
                  }
                  props.formik.setFieldValue('img_url', res.response.file);
                  onCloseModal();
                })
                .catch((er) => {
                  Alert.alert(t('common:notice'), 'UNKNOWN');
                })
                .finally(() => {
                  setLoading(false);
                });
            }}
          />
        </View>
      </SafeAreaView>
    </ModalizeApp>
  );
});
