import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    // header: {
    //   borderBottomWidth: 1,
    //   borderBottomColor: theme.color.mercury,
    //   flexDirection: 'row',
    //   paddingHorizontal: theme.spacing.p20,
    //   paddingVertical: theme.spacing.p12,
    //   justifyContent: 'space-between',
    // },
    // title: {
    //   fontSize: theme.fontSize.f16,
    //   fontWeight: 'bold',
    // },
    image: {
      width: 200,
      height: 200,
      borderRadius: 8,
    },
    imageContainer: {
      alignItems: 'center',
    },
    text: {
      fontWeight: '500',
      fontSize: theme.fontSize.f20,
      marginVertical: theme.spacing.p16,
    },
    flex1: {
      flex: 1,
    },
    imageStyles: {
      borderStyle: 'dashed',
      borderWidth: 2,
      borderColor: theme.color.mineShaft,
      marginHorizontal:theme.spacing.p16,
      alignItems:'center',
      justifyContent:'center',
      padding: theme.spacing.p16,
      marginTop: theme.spacing.p16
    }
  });
  return styles;
};
