import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
      paddingHorizontal: theme.spacing.p16,
      position: 'relative',
    },
    title: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    header: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      flexDirection: 'row',
      paddingHorizontal: theme.spacing.p20,
      paddingVertical: theme.spacing.p12,
      justifyContent: 'space-between',
    },
    checkBox: {
      borderWidth: 0,
      marginLeft: 0,
      justifyContent: 'flex-start',
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p8,
    },
    textStyle: {
      fontSize: theme.fontSize.f16,
      fontWeight: '400',
      marginLeft: 0,
    },
    button: {
      marginHorizontal: theme.spacing.p16,
      marginBottom: theme.spacing.p16,
    },
    flex1: {
      flex: 1,
    },
    addNew: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p8,
    },
    textAdd: {
      fontSize: theme.fontSize.f16,
      marginLeft: 8,
      color: theme.color.violet,
    },
    rowBack: {
      alignItems: 'center',
      backgroundColor: '#DDD',
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingLeft: 15,
    },
    backRightBtn: {
      alignItems: 'center',
      bottom: 0,
      justifyContent: 'center',
      position: 'absolute',
      top: 0,
      width: 75,
    },
    backRightBtnRight: {
      backgroundColor: 'red',
      right: 0,
    },
    backTextWhite: {
      color: '#FFF',
    },
  });
  return styles;
};
