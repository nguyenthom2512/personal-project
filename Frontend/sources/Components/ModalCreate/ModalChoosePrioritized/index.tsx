import { useNavigation } from '@react-navigation/core';
import { FormikProps } from 'formik';
import React, { forwardRef, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  FlatList,
  Modal,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import useGetPriority from '../../../Hooks/getPriority';
import { Priority } from '../../../interfaces/priority.interface';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import AppButton from '../../AppButton';
import AppRadio from '../../AppRadio';
import AppText from '../../AppText';
import ModalCreatePriority from '../ModalCreatePriority';
import styles from './styles';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Icon } from 'react-native-elements';
import { IndicatorContext } from '../../../Context';
import priorityService from '../../../Services/priorityService';
import { AppModal } from '../..';
import ModalApp from '../../ModalApp';
import ModalizeApp from '../../ModalizeApp';
import { device } from '../../../Helpers';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  isGetFullInfo?: boolean;
  isEdit?: boolean;
  // priority?: number | string;
}
let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { t } = useTranslation();
  const navigation = useNavigation();
  const { onCloseModal = () => {}, formik, isGetFullInfo = false } = props;
  const Styles = useThemeAwareObject(styles);
  const { setLoading } = useContext(IndicatorContext);
  const { theme } = useTheme();
  const [showCreatePriority, setCreatePriority] = useState(false);
  const [dataPriority, loadingPriority, errorPriority, refetchPriority] =
    useGetPriority();

  const [checked, setChecked] = useState<number | string | undefined>(
    props.formik.values.priority,
  );

  useEffect(() => {
    setChecked(formik.values.priority?.id || formik.values.priority);
  }, [props.formik.values.priority]);

  const handleCloseModal = () => {
    if (!isSave) {
      setChecked(formik.values.priority?.id || formik.values.priority);
    }
    isSave = false;
  };

  const renderOptionItem = ({
    item,
    index,
  }: {
    item: Priority;
    index: number;
  }) => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        style={{
          ...Styles.container,
          backgroundColor:
            index % 2 ? theme.color.white : theme.color.lightGray,
        }}
        onPress={() => {
          if (checked?.toString() === item.id?.toString()) {
            setChecked('');
          } else {
            setChecked(item.id);
          }
        }}
      >
        <Ionicons name="flag" color={item.color} size={theme.fontSize.f22} />
        <View style={Styles.checkBox}>
          <AppText>{item?.priority_name}</AppText>
        </View>
        <AppRadio
          checked={checked?.toString() === item.id?.toString()}
          disabled
        />
      </TouchableOpacity>
    );
  };

  const removePriority = (id: number | string) => {
    Alert.alert(t('common:notice'), t('createMissions:askTag'), [
      {
        text: t('common:cancel'),
        onPress: () => {},
      },
      {
        text: t('common:confirm'),
        onPress: () => {
          setLoading(true);
          priorityService.deleteTodoPriority(
            id,
            (err) => {
              let mess = err;
              if (err === 'UNKNOWN') {
                mess = t('common:unknownError');
              }
              Alert.alert(t('common:notice'), mess);
              setLoading(false);
            },
            () => {
              refetchPriority();
              setLoading(false);
            },
          );
        },
      },
    ]);
  };

  const renderOption = () => (
    <View style={{ flex: 1 }}>
      <SwipeListView
        useFlatList={true}
        data={dataPriority || []}
        renderItem={renderOptionItem}
        renderHiddenItem={(rowData, rowMap) => (
          <View style={Styles.rowBack}>
            <TouchableOpacity
              style={[Styles.backRightBtn, Styles.backRightBtnRight]}
              onPress={() => removePriority(rowData.item.id)}
            >
              <AppText style={Styles.backTextWhite}>Delete</AppText>
            </TouchableOpacity>
          </View>
        )}
        //   leftOpenValue={75}
        rightOpenValue={-75}
        onRowOpen={(rowKey, rowMap) => {
          setTimeout(() => {
            rowMap[rowKey].closeRow();
          }, 2000);
        }}
        onRefresh={refetchPriority}
        refreshing={loadingPriority}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => index.toString()}
        alwaysBounceVertical
      />
      {/* <FlatList
        onRefresh={refetchPriority}
        refreshing={loadingPriority}
        showsVerticalScrollIndicator={false}
        data={dataPriority.sort((a,b) => a.id - b.id) || []}
        renderItem={renderOptionItem}
        keyExtractor={(item, index) => index.toString()}
        alwaysBounceVertical
      /> */}
    </View>
  );

  return (
    <ModalizeApp
      panGestureEnabled={false}
      headerComponent
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          onPress={() => onCloseModal()}
        />
      }
      headerText={t('createMissions:choosePrioritized')}
      disableSwipe={true}
      //   adjustToContentHeight={false}
      //   modalHeight={device.height * 0.48}
      FooterComponent={() => {
        return (
          <View>
            <TouchableOpacity
              style={Styles.addNew}
              onPress={() => setCreatePriority(true)}
            >
              <Feather name="plus" size={26} color={theme.color.violet} />
              <AppText style={Styles.textAdd}>
                {t('createMissions:addNew')}
              </AppText>
            </TouchableOpacity>
            <AppButton
              title={t('createMissions:save')}
              onPress={() => {
                isSave = true;
                formik.setValues({
                  ...formik.values,
                  priority_color: dataPriority.find(
                    (elm) => elm.id === checked,
                  ),
                  priority: isGetFullInfo
                    ? dataPriority.find((elm) => elm.id === checked)
                    : checked,
                });

                onCloseModal();
              }}
              containerStyle={Styles.button}
            />
          </View>
        );
      }}
    >
      <SafeAreaView style={{ flex: 1 }}>
        {/* <View style={Styles.header}>
          <TouchableOpacity onPress={onCloseModal}>
            <Ionicons name="close-sharp" size={24} color={theme.color.gray} />
          </TouchableOpacity>
          <AppText style={Styles.title}>
            {t('createMissions:choosePrioritized')}
          </AppText>
          <View />
        </View> */}
        {renderOption()}
      </SafeAreaView>
      {showCreatePriority && (
        <ModalCreatePriority
          modalVisible={showCreatePriority}
          onCloseModal={() => setCreatePriority(!showCreatePriority)}
          refetchPriority={refetchPriority}
        />
      )}
    </ModalizeApp>
  );
});
