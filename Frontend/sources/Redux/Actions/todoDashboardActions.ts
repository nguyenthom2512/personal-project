import actionTypes from '../ActionTypes';

export const getTodoDashboardRequest = (params?: any) => {
  return {
    type: actionTypes.GET_TODO_DASHBOARD_REQUEST,
    payload: params,
  };
};

export const getTodoDashboardFailed = (error: any) => {
  return {
    type: actionTypes.GET_TODO_DASHBOARD_FAILED,
    error,
  };
};

export const getTodoDashboardSuccess = (payload: any) => {
  return {
    type: actionTypes.GET_TODO_DASHBOARD_SUCCESS,
    payload,
  };
};
