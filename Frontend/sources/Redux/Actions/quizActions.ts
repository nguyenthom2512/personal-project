import ActionTypes from "../ActionTypes"

export const sendQuiz = (body: object, callBack: any) => {
    return {
        type: ActionTypes.SEND_QUIZ_REQUEST,
        body,
        callBack,
    }
}
export const sendQuizSuccess = (body: any) => {
    return {
        type: ActionTypes.SEND_QUIZ_SUCCESS,
        body,
    }
}
export const sendQuizFailed = (error: any) => {
    return {
        type: ActionTypes.SEND_QUIZ_FAILED,
        error,
    }
}
export const postUserQuiz = (body: object, callBack?: any) => {
    return {
        type: ActionTypes.POST_USER_REQUEST,
        body,
        callBack,
    }
}