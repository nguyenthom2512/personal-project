import actionTypes from '../ActionTypes';

/*
 * Reducer actions related with login
 */

export const loginRequest = (body: object, callBacks: any) => {
  return {
    type: actionTypes.LOGIN_REQUEST,
    body,
    callBacks,
  };
};

export const loginSocialRequest = (body: object, callBacks: any) => {
    return {
      type: actionTypes.LOGIN_SOCIAL_REQUEST,
      body,
      callBacks,
    };
  };

export const loginFailed = (error: any) => {
  return {
    type: actionTypes.LOGIN_FAILED,
    error,
  };
};

export const loginSuccess = (response: object) => {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    response,
  };
};

// logout
export const logout = () => {
  return {
    type: actionTypes.LOGOUT,
  };
};

export const getUserRequest = (params?: any) => {
  return {
    type: actionTypes.GET_USER_REQUEST,
    payload: params,
  };
};

export const getUserRequestSuccess = (data: any) => {
  return {
    type: actionTypes.GET_USER_SUCCESS,
    body: data,
  };
};

export const getUserRequestFailed = (error: any) => {
  return {
    type: actionTypes.GET_USER_FAILED,
    error,
  };
};
