import actionTypes from '../ActionTypes';

/*
 * Reducer actions related with login
 */

export const getTodoListRequest = (params: any) => {
  return {
    type: actionTypes.GET_TODO_LIST_REQUEST,
    payload: params,
  };
};

export const getTodoListFailed = (error: any) => {
  return {
    type: actionTypes.GET_TODO_LIST_FAILED,
    error,
  };
};

export const getTodoListSuccess = (payload: any) => {
  return {
    type: actionTypes.GET_TODO_LIST_SUCCESS,
    payload,
  };
};

// Priority
export const getPriorityRequest = (params: string) => {
  return {
    type: actionTypes.GET_PRIORITY_REQUEST,
    payload: params,
  };
};

export const getPriorityFailed = (error: any) => {
  return {
    type: actionTypes.GET_PRIORITY_FAILED,
    error,
  };
};

export const getPrioritySuccess = (payload: any) => {
  return {
    type: actionTypes.GET_PRIORITY_SUCCESS,
    payload,
  };
};

//Todo details
export const getTodoDetail = (params: any) => {
  return {
    type: actionTypes.GET_TODO_DETAIL_REQUEST,
    payload: params,
  };
};

export const getTodoDetailFailed = (error: any) => {
  return {
    type: actionTypes.GET_TODO_LIST_FAILED,
    error,
  };
};

export const getTodoDetailSuccess = (payload: any) => {
  return {
    type: actionTypes.GET_TODO_DETAIL_SUCCESS,
    payload,
  };
};

//Completed Work
export const getCompletedWork = (params: any) => {
  return {
    type: actionTypes.GET_COMPLETED_WORK,
    payload: params,
  };
};

export const getCompletedWorkFailed = (error: any) => {
  return {
    type: actionTypes.GET_COMPLETED_WORK_FAILED,
    error,
  };
};

export const getCompletedWorkSuccess = (dataSuccess: any) => {
  return {
    type: actionTypes.GET_COMPLETED_WORK_SUCCESS,
    data: dataSuccess.data,
    payload: dataSuccess.payload,
  };
};
//Incomplete Work

export const getInCompleteWork = (params: any) => {
  return {
    type: actionTypes.GET_IN_COMPLETED_WORK,
    payload: params,
  };
};

export const getInCompleteWorkFailed = (error: any) => {
  return {
    type: actionTypes.GET_IN_COMPLETED_WORK_FAILED,
    error,
  };
};

export const getInCompleteWorkSuccess = (dataSuccess: any) => {
  return {
    type: actionTypes.GET_IN_COMPLETED_WORK_SUCCESS,
    data: dataSuccess.data,
    payload: dataSuccess.payload,
  };
};



