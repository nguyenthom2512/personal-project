

// GET_OKR_RAIC

import ActionTypes from "../ActionTypes"

export const getOkrRaic = () =>{
  return {
     type: ActionTypes.GET_OKR_RAIC_REQUEST,
  }
}

export const getOkrRaicSuccess = (body: any) => {
  return {
    type:  ActionTypes.GET_OKR_RAIC_SUCCESS,
    body,
  }
}
export const getOkrRaicFailed = (error: any) => {
  return {
    type:  ActionTypes.GET_OKR_RAIC_FAILED,
    error,
  }
}