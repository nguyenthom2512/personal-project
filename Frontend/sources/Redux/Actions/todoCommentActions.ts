import ActionTypes from "../ActionTypes";

export const resetTodoComment = () => {
  return{
    type: ActionTypes.RESET_TODO_COMMENT,
  }
}

export const backTodoComment = () => {
  return{
    type: ActionTypes.BACK_TODO_COMMENT,
  }
}