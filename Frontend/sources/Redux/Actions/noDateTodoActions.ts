import ActionTypes from '../ActionTypes';

interface ParamsPage {
  page: number;
  page_size: number;
}

export const noDateTodoRequest = (params: ParamsPage) => {
  return {
    type: ActionTypes.NO_DATE_TODO_REQUEST,
    payload: params,
  };
};

export const noDateTodoSuccess = (dataSuccess: any) => {
  return {
    type: ActionTypes.NO_DATE_TODO_SUCCESS,
    data: dataSuccess.data,
    payload: dataSuccess.payload,
  };
};

export const noDateTodoFailed = (error: any) => {
  return {
    type: ActionTypes.NO_DATE_TODO_FAILED,
    error,
  };
};
