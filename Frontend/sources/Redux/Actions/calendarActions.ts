import { CalendarType } from '../../Modules/AgendaCalendar';
import ActionTypes from '../ActionTypes';

export const setTypeCalendar = (calendarType: CalendarType | null) => {
  return {
    type: ActionTypes.CHANGE_CALENDAR_TYPE,
    calendarType,
  };
};

export const getCalendarRequest = (
  params: { start_time: Date | string; end_time: Date | string },
  isToday: boolean = false,
) => {
  return {
    type: ActionTypes.GET_CALENDAR_REQUEST,
    payload: params,
    isToday,
  };
};

export const getCalendarSuccess = (payload: any) => {
  return {
    type: ActionTypes.GET_CALENDAR_SUCCESS,
    payload,
  };
};

export const getTodoListSuccess = (payload: any[]) => {
  return {
    type: ActionTypes.GET_TODO_LIST_SUCCESS,
    payload,
  };
};

export const getCalendarFailed = (error: string) => {
  return {
    type: ActionTypes.GET_CALENDAR_FAILED,
    error,
  };
};
