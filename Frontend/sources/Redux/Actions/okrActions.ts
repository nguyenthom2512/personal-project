import actionTypes from '../ActionTypes';

// getListOKR
export const getListOKRRequest = (body: object) => {
  return {
    type: actionTypes.GET_LIST_OKR_REQUEST,
    body,
  };
};
export const getListOKRFailed = (error: any) => {
  return {
    type: actionTypes.GET_LIST_OKR_FAILED,
    error,
  };
};
export const getListOKRSuccess = (body: object) => {
  return {
    type: actionTypes.GET_LIST_OKR_SUCCESS,
    body,
  };
};
export const getListOkrParentRequest = (body: object) => {
  return {
    type: actionTypes.GET_LIST_OKR_PARENT_REQUEST,
    body,
  };
};
export const getListOkrParentFailed = (error: any) => {
  return {
    type: actionTypes.GET_LIST_OKR_PARENT_FAILED,
    error,
  };
};
export const getListOkrParentSuccess = (body: object) => {
  return {
    type: actionTypes.GET_LIST_OKR_PARENT_SUCCESS,
    body,
  };
};
// create OKR
export const createOKRRequest = (body: object, callBack: any) => {
  return {
    type: actionTypes.CREATE_OKR_REQUEST,
    body,
    callBack,
  };
};
export const createOKRSuccess = (body: object) => {
  return {
    type: actionTypes.CREATE_OKR_SUCCESS,
    body,
  };
};

export const createOKRFailed = (error: any) => {
  return {
    type: actionTypes.CREATE_OKR_FAILED,
    error,
  };
};

export const getListOkrFilter = (body: any) => {
  return {
    type: actionTypes.GET_LIST_OKR_FILTER,
    body,
  };
};

export const resetListOkrFilter = () => {
  return {
    type: actionTypes.RESET_LIST_OKR_FILTER,
  };
};

export const getListOkrFilterFailed = (error: any) => {
  return {
    type: actionTypes.GET_LIST_OKR_FILTER_FAILED,
    error,
  };
};
export const getListOkrFilterSuccess = (body: object) => {
  return {
    type: actionTypes.GET_LIST_OKR_FILTER_SUCCESS,
    body,
  };
};
//update
export const upDateResultRequest = (body: object, callBack: any) => {
  return {
    type: actionTypes.UPDATE_RESULT_REQUEST,
    body,
    callBack,
  };
};
export const upDateResultFailed = (error: any) => {
  return {
    type: actionTypes.UPDATE_RESULT_FAILED,
    error,
  };
};
export const upDateResultSuccess = (body: object) => {
  return {
    type: actionTypes.UPDATE_RESULT_SUCCESS,
    body,
  };
};
// getOkrResult
export const getOkrResultRequest = (body: object) => {
  return {
    type: actionTypes.GET_OKR_RESULT_REQUEST,
    body,
  };
};
// Update OKr
export const updateOkrRequest = (body: Object, callBack: any) => {
  return {
    type: actionTypes.UPDATE_OKR_REQUEST,
    body,
    callBack,
  };
};
export const updateOkrFailed = (error: any) => {
  return {
    type: actionTypes.UPDATE_OKR_FAILED,
    error,
  };
};

export const updateOkrSuccess = (body: object) => {
  return {
    type: actionTypes.UPDATE_OKR_SUCCESS,
    body,
  };
};

//Delete Okr

export const DeleteOkrRequest = (body: Object, callBack: any) => {
  return {
    type: actionTypes.DELETE_OKR_REQUEST,
    body,
    callBack,
  };
};
export const DeleteOkrFailed = (error: any) => {
  return {
    type: actionTypes.DELETE_OKR_FAILED,
    error,
  };
};

export const DeleteOkrSuccess = (body: object) => {
  return {
    type: actionTypes.DELETE_OKR_SUCCESS,
    body,
  };
};

// delete OkrResult
export const DeleteOkrResultRequest = (id: string, callBack: any) => {
  return {
    type: actionTypes.DELETE_OKR_RESULT_REQUEST,
    id,
    callBack,
  };
};
// createKr
export const createKrRequest = (body: object, callBack: any) => {
  return {
    type: actionTypes.CREATE_KR_REQUEST,
    body,
    callBack,
  };
};
