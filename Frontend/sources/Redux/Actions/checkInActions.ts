import ActionTypes from '../ActionTypes';

interface ParamsPage {
  page: number;
  page_size: number;
  department?: string | number;
  created_time__range?: string | number;
}
//Check-in
export const getCheckIn = (params: ParamsPage) => {
  return {
    type: ActionTypes.GET_CHECK_IN,
    payload: params,
  };
};
export const getCheckInSuccess = (dataSuccess: any) => {
  return {
    type: ActionTypes.GET_CHECK_IN_SUCCESS,
    data: dataSuccess.data,
    payload: dataSuccess.payload,
  };
};
export const getCheckInFailed = (error: any) => {
  return {
    type: ActionTypes.GET_CHECK_IN_FAILED,
    error,
  };
};
//Check-in-Details
export const getCheckInDetails = (params: any) => {
  return {
    type: ActionTypes.GET_CHECK_IN_DETAILS,
    payload: params,
  };
};
export const getCheckInDetailsSuccess = (payload: any) => {
  return {
    type: ActionTypes.GET_CHECK_IN_DETAILS_SUCCESS,
    payload,
  };
};
export const getCheckInDetailsFailed = (error: any) => {
  return {
    type: ActionTypes.GET_CHECK_IN_DETAILS_FAILED,
    error,
  };
};
//Create-check-in
export const postCreateCheckIn = (
  params: any,
  callback: {
    onError: (er: string) => void;
    onSuccess: () => void;
  },
) => {
  return {
    type: ActionTypes.POST_CREATE_CHECK_IN,
    payload: params,
    callback
  };
};
export const postCreateCheckInSuccess = (payload: any) => {
  return {
    type: ActionTypes.POST_CREATE_CHECK_IN_SUCCESS,
    payload,
  };
};
export const postCreateCheckInFailed = (error: any) => {
  return {
    type: ActionTypes.POST_CREATE_CHECK_IN_FAILED,
    error,
  };
};
