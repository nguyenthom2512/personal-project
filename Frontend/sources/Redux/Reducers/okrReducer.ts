import { IOkrReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

const initialState: IOkrReducer = {
  data: {
    results: [],
  },
  dataParent: {
    results: [],
  },

  type: '',
  loading: false,
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_LIST_OKR_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_LIST_OKR_SUCCESS:
      if (action.body.page == 1) {
        return {
          ...state,
          data: action.body.response,
          errorMessage: '',
          loading: false,
        };
      } else {
        const newData = action.body.response;
        const oldData = state.data;
        const resultData = [...oldData.results, ...newData.results];
        return {
          ...state,
          data: { ...newData, results: resultData },
          errorMessage: '',
          loading: false,
        };
      }
    case actionTypes.GET_LIST_OKR_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    // get list okr parent
    case actionTypes.GET_LIST_OKR_PARENT_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_LIST_OKR_PARENT_SUCCESS:
      return {
        ...state,
        dataParent: action.body.response,
        loading: true,
      };
    case actionTypes.GET_LIST_OKR_PARENT_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    // create OKR
    case actionTypes.CREATE_OKR_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.CREATE_OKR_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        loading: false,
      };
    case actionTypes.CREATE_OKR_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    case actionTypes.GET_LIST_OKR_FILTER:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.RESET_LIST_OKR_FILTER:
      return {
        ...state,
        loading: false,
        data: {
          results: [],
        },
      };
    case actionTypes.GET_LIST_OKR_FILTER_SUCCESS:
      return {
        ...state,
        data: {
          results: action.body,
        },
        errorMessage: '',
      };
    case actionTypes.GET_LIST_OKR_FILTER_FAILED:
      return {
        ...state,
        errorMessage: action.error,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
