import { ICalendarReducer } from '../../interfaces/reducer.interface';
import { CalendarType } from '../../Modules/AgendaCalendar';
import actionTypes from '../ActionTypes';

export interface IUserReducer {
  calendarType?: CalendarType;
  type: string;
  listEventByDay?: any[];
  listEvent?: any[];
  todoList?: any[];
  errorMessage: string;
}

const initialState: IUserReducer = {
  calendarType: CalendarType.TODAY,
  type: '',
  listEventByDay: [],
  todoList: [],
  listEvent: [],
  errorMessage: '',
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.CHANGE_CALENDAR_TYPE:
      return {
        ...state,
        calendarType: action.calendarType,
      };
    case actionTypes.GET_CALENDAR_REQUEST:
      return {
        ...state,
        errorMessage: '',
      };
    case actionTypes.GET_CALENDAR_FAILED:
      return {
        ...state,
        errorMessage: action.error,
      };
    case actionTypes.GET_CALENDAR_SUCCESS:
      return {
        ...state,
        ...action.payload,
        errorMessage: '',
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
