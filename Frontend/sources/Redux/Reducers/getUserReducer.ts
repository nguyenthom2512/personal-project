import actionTypes from '../ActionTypes';

const initialState = {
  data: [],
  type: '',
  errorMessage: '',
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_USER_REQUEST:
      return {
        ...state,
        errorMessage: '',
      };
    case actionTypes.GET_USER_FAILED:
      return {
        ...state,
        errorMessage: action.error,
      };
    case actionTypes.GET_USER_SUCCESS:
      return {
        ...state,
        data: action.payload,
        errorMessage: '',
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
