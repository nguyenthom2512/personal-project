import { combineReducers } from 'redux';
// Redux: Root Reducer
import userReducer from './userReducer';
import okrReducer from './okrReducer';
import todoListReducer from './todoListReducer';
import calendarReducer from './calendarReducer';
import { RootReducer } from '../../interfaces/reducer.interface';
import priorityReducer from './priorityReducer';
import raicReducer from './raicReducer';
import noDateTodoReducer from './noDateTodoReducer';
import checkInReducer from './checkInReducer';
import completedWorkReducer from './completedWorkReducer';
import commentReducer from './commentReducer';
import incompleteWorkReducer from './incompleteWorkReducer';
import todoDashboardReducer from './todoDashboardReducer';
import getUserReducer from './getUserReducer';

const rootReducer = combineReducers({
  userReducer,
  okrReducer,
  todoListReducer,
  calendarReducer,
  priorityReducer,
  raicReducer,
  noDateTodoReducer,
  checkInReducer,
  completedWorkReducer,
  commentReducer,
  incompleteWorkReducer,
  todoDashboardReducer,
  getUserReducer
});
// Exports
export type RootState = ReturnType<typeof rootReducer>;
export default rootReducer;
