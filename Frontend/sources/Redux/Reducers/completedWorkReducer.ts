import { IOkrReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

export interface CompletedWork {
  type: string;
  data: {
    results: any[];
    count: number;
    previous?: string | null;
    next?: string | null;
  };
  loading: boolean;
  noPage: number;
}

const initialState: CompletedWork = {
  type: '',
  data: {
    results: [],
    count: 0,
    previous: null,
    next: null,
  },
  loading: false,
  noPage: 1,
};

const processData = (currentData: any, newData: any, page: number) => {
  return {
    ...newData,
    results:
      page === 1
        ? newData.results
        : [...currentData.results, ...newData.results],
  };
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_COMPLETED_WORK:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_COMPLETED_WORK_SUCCESS:
      return {
        ...state,
        errorMessage: '',
        loading: false,
        data: processData(state.data, action.data, action.payload.page),
        noPage: Math.ceil(action.data.count / action.payload.page_size),
      };
    case actionTypes.GET_COMPLETED_WORK_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
