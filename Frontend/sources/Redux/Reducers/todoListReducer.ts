import { ITodolistReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

const initialState: ITodolistReducer = {
  data: [],
  type: '',
  errorMessage: '',
  dataDetail: {},
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_TODO_LIST_REQUEST:
      return {
        ...state,
        errorMessage: '',
      };
    case actionTypes.GET_TODO_LIST_FAILED:
      return {
        ...state,
        data: [],
        errorMessage: action.error,
      };
    case actionTypes.GET_TODO_LIST_SUCCESS:
      return {
        ...state,
        data: action.payload,
        errorMessage: '',
      };
    case actionTypes.GET_TODO_DETAIL_REQUEST:
      return {
        ...state,
        errorMessage: '',
      };
    case actionTypes.GET_TODO_DETAIL_FAILED:
      return {
        ...state,
        errorMessage: action.error,
      };
    case actionTypes.GET_TODO_DETAIL_SUCCESS:
      return {
        ...state,
        dataDetail: action.payload,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
