import { IUserReducer } from '../../interfaces/reducer.interface';
import { setToken } from '../../Services/serviceHandle';
import actionTypes from '../ActionTypes';

const initialState: IUserReducer = {
  data: {},
  type: '',
  loading: false,
  errorMessage: '',
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  if (
    action.type.includes('FAILED') &&
    action?.error?.toLowerCase() === 'Invalid token.'.toLowerCase()
  ) {
    setToken('');
    return initialState;
  }
  switch (action.type) {
    case actionTypes.LOGIN_REQUEST:
      return {
        ...state,
        errorMessage: '',
        loading: false,
        data: {},
      };
    case actionTypes.LOGIN_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        data: action.response,
        errorMessage: '',
        loading: false,
      };
    case actionTypes.LOGOUT:
      return initialState;
    case actionTypes.GET_USER_SUCCESS:
      console.log('______________________', action)
      return {
        ...state,
        data: { ...state.data, user: action.body.response },
        errorMessage: '',
        loading: false,
      };
    default:
      return state;
  }
};
