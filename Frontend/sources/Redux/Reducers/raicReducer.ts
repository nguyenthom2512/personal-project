import { IOkrReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

const initialState: IOkrReducer = {
  data: {
    results: [],
  },
  dataParent: {
    results: [],
  },

  type: '',
  loading: false,
};

export default (state = initialState, action: any) => {
  state.type = action.type;

  switch (state.type) {
    case actionTypes.GET_OKR_RAIC_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_OKR_RAIC_SUCCESS:
      return {
        ...state,
        data: {
          results: action.body,
        },
        errorMessage: '',
        loading: false,
      };
    case actionTypes.GET_OKR_RAIC_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
