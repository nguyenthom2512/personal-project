import { ITodolistReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

export interface CheckIn {
  type: string;
  data: {
    results: any[];
    count: number;
    previous?: string | null;
    next?: string | null;
  };
  loading: boolean;
  noPage: number;
  dataDetails: any;
}

const initialState: CheckIn = {
  type: '',
  data: {
    results: [],
    count: 0,
    previous: null,
    next: null,
  },
  loading: false,
  noPage: 1,
  dataDetails: { ok_checkin: [] },
};
const processData = (currentData: any, newData: any, page: number) => {
  return {
    ...newData,
    results:
      page === 1
        ? newData.results
        : [...currentData.results, ...newData.results],
  };
};
export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    //Check-in-reducer
    case actionTypes.GET_CHECK_IN:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_CHECK_IN_FAILED:
      return {
        ...state,
        errorMessage: action.error,
        loading: false,
      };
    case actionTypes.GET_CHECK_IN_SUCCESS:
      return {
        ...state,
        loading: false,
        data: processData(state.data, action.data, action.payload.page),
        noPage: Math.ceil(action.data.count / action.payload.page_size),
        errorMessage: '',
      };
    //check-in-details-reducer
    case actionTypes.GET_CHECK_IN_DETAILS:
      return {
        ...state,
      };
    case actionTypes.GET_CHECK_IN_DETAILS_SUCCESS:
      return {
        ...state,
        dataDetails: action.payload,
      };
    case actionTypes.GET_CHECK_IN_DETAILS_FAILED:
      return {
        ...state,
        error: action.error,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
