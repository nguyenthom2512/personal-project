import { ITodolistReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

const initialState: ITodolistReducer = {
  data: [],
  type: '',
  errorMessage: '',
  dataDetail: {},
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_TODO_DASHBOARD_REQUEST:
      return {
        ...state,
        errorMessage: '',
      };
    case actionTypes.GET_TODO_DASHBOARD_FAILED:
      return {
        ...state,
        data: [],
        errorMessage: action.error,
      };
    case actionTypes.GET_TODO_DASHBOARD_SUCCESS:
      return {
        ...state,
        data: action.payload,
        errorMessage: '',
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
