import { ITodolistReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

const initialState: ITodolistReducer = {
  data: [],
  type: '',
  errorMessage: '',
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_PRIORITY_REQUEST:
      return {
        ...state,
        errorMessage: '',
      };
    case actionTypes.GET_PRIORITY_FAILED:
      return {
        ...state,
        errorMessage: action.error,
      };
    case actionTypes.GET_PRIORITY_SUCCESS:
      return {
        ...state,
        data: action.payload,
        errorMessage: '',
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
