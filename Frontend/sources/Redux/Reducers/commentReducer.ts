import ActionTypes from '../ActionTypes';

export interface commentTodo {
  data: boolean;
  type: string;
}

const initialState: commentTodo = {
  data: false,
  type: '',
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case ActionTypes.RESET_TODO_COMMENT:
      return {
        ...state,
        data: true,
      };
    case ActionTypes.BACK_TODO_COMMENT:
      return {
        ...state,
        data: false,
      };
    case ActionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
