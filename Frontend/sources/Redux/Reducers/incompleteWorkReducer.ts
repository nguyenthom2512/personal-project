import { IOkrReducer } from '../../interfaces/reducer.interface';
import actionTypes from '../ActionTypes';

export interface IncompleteWork {
  type: string;
  data: {
    results: any[];
  };
}

const initialState: IncompleteWork = {
  type: '',
  data: {
    results: [],
   
  },
  
};

export default (state = initialState, action: any) => {
  state.type = action.type;
  switch (action.type) {
    case actionTypes.GET_IN_COMPLETED_WORK:
      return {
        ...state,
      };
    case actionTypes.GET_IN_COMPLETED_WORK_SUCCESS:
      return {
        ...state,
        data: action.payload,
        errorMessage: '',
      };
    case actionTypes.GET_IN_COMPLETED_WORK_FAILED:
      return {
        ...state,
        errorMessage: action.error,
      };
    case actionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
};
