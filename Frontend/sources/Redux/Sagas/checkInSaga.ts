import { put, call } from 'redux-saga/effects';
import { apiGet, apiPost, IReturnType } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as checkInActions from '../Actions/checkInActions';
import _ from 'lodash';

export function* checkIn(actions: any) {
  const url = serviceUrls.url.okr;
  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);
    if (response.error) {
      yield put(checkInActions.getCheckInFailed(response.errorMessage));
    } else {
      yield put(
        checkInActions.getCheckInSuccess({
          data: response.response,
          payload: actions.payload,
        }),
      );
    }
  } catch (error) {
    yield put(checkInActions.getCheckInFailed(error));
  }
}

export function* checkInDetails(actions: any) {
  const url = `${serviceUrls.url.okr}${actions.payload}`;

  try {
    const dataDetails: IReturnType = yield call(apiGet, url, actions.payload);
    if (dataDetails.error) {
      yield put(
        checkInActions.getCheckInDetailsFailed(dataDetails.errorMessage),
      );
    } else {
      yield put(checkInActions.getCheckInDetailsSuccess(dataDetails.response));
    }
  } catch (error) {
    yield put(checkInActions.getCheckInDetailsFailed(error));
  }
}

export function* createCheckIn(actions: any) {
  const { callback, payload } = actions;
  const url = serviceUrls.url.checkIn;

  try {
    const dataDetails: IReturnType = yield call(apiPost, url, payload);

    if (dataDetails.error) {
      callback?.onError && callback.onError(dataDetails.errorMessage);
      yield put(
        checkInActions.postCreateCheckInFailed(dataDetails.errorMessage),
      );
    } else {
      callback?.onError && callback.onSuccess();
      yield put(checkInActions.getCheckInDetails(payload.okr));
    }
  } catch (error) {
    callback?.onError && callback.onError('UNKNOWN');

    yield put(checkInActions.postCreateCheckInFailed(error));
  }
}
