import { call, put } from '@redux-saga/core/effects';
import _ from 'lodash';
import { useState } from 'react';
import { array } from 'yup';
import {
  apiDelete,
  apiGet,
  apiPatch,
  apiPost,
  apiPut,
} from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as okrActions from '../Actions/raicAction';

//get OKR RAIC
export function* getOkrRAIC(payload: any) {
  const url = serviceUrls.url.raic;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiGet, url,'');
    
    if (response.error && !_.isEmpty(response.response)) {
      yield put(okrActions.getOkrRaicFailed(response.error));
    } else {
      yield put(
        okrActions.getOkrRaicSuccess(response.response),
      );
    }
  } catch (error) {
    yield put(okrActions.getOkrRaicFailed(error));
  }
}