import { takeEvery, all, takeLatest } from 'redux-saga/effects';
import actionTypes from '../ActionTypes';
import * as userSaga from './userSaga';
import * as okrSaga from './okrSaga';
import * as todoListSaga from './todoListSaga';
import * as prioritySaga from './prioritySaga';
import * as calendarSaga from './calendarSaga';
import * as raicSaga from './raicSaga';
import * as noDateTodoSaga from './noDateTodoSaga';
import * as checkInSaga from './checkInSaga';
import * as quizSaga from './quizSaga';
import * as completedWorkSaga from './completedWorkSaga';
import * as incompleteWorkSaga from './incompleteWorkSaga';
import * as todoDashboardSaga from './todoDashboardSaga';

export default function* watch() {
  yield all([takeEvery(actionTypes.LOGIN_REQUEST, userSaga.login)]);
  yield all([
    takeEvery(actionTypes.LOGIN_SOCIAL_REQUEST, userSaga.loginSocial),
  ]);
  yield all([takeEvery(actionTypes.GET_LIST_OKR_REQUEST, okrSaga.getListOKR)]);
  yield all([
    takeEvery(
      actionTypes.GET_LIST_OKR_PARENT_REQUEST,
      okrSaga.getListOkrParent,
    ),
  ]);
  yield all([takeEvery(actionTypes.CREATE_OKR_REQUEST, okrSaga.createOKR)]);
  yield all([takeEvery(actionTypes.GET_LIST_OKR_FILTER, okrSaga.filterOkr)]);
  yield all([
    takeEvery(actionTypes.GET_TODO_LIST_REQUEST, todoListSaga.todoList),
  ]);
  yield all([
    takeEvery(actionTypes.UPDATE_RESULT_REQUEST, okrSaga.upDateResult),
  ]);
  yield all([
    takeEvery(actionTypes.GET_CALENDAR_REQUEST, calendarSaga.calendar),
  ]);
  yield all([takeEvery(actionTypes.GET_LIST_OKR_FILTER, okrSaga.filterOkr)]);
  yield all([takeEvery(actionTypes.UPDATE_OKR_REQUEST, okrSaga.updateOkr)]);
  yield all([takeEvery(actionTypes.DELETE_OKR_REQUEST, okrSaga.DeleteOkr)]);

  yield all([takeEvery(actionTypes.LOGOUT, userSaga.logout)]);
  yield all([
    takeLatest(actionTypes.GET_PRIORITY_REQUEST, prioritySaga.priority),
  ]);
  yield all([
    takeEvery(actionTypes.DELETE_OKR_RESULT_REQUEST, okrSaga.DeleteOkrResult),
  ]);
  yield all([takeEvery(actionTypes.CREATE_KR_REQUEST, okrSaga.createKr)]);
  yield all([takeEvery(actionTypes.GET_OKR_RAIC_REQUEST, raicSaga.getOkrRAIC)]);
  yield all([
    takeEvery(actionTypes.NO_DATE_TODO_REQUEST, noDateTodoSaga.noDateTodo),
  ]);

  yield all([takeEvery(actionTypes.GET_CHECK_IN, checkInSaga.checkIn)]);
  yield all([
    takeEvery(actionTypes.GET_CHECK_IN_DETAILS, checkInSaga.checkInDetails),
  ]);
  yield all([
    takeEvery(actionTypes.POST_CREATE_CHECK_IN, checkInSaga.createCheckIn),
  ]);

  yield all([takeEvery(actionTypes.SEND_QUIZ_REQUEST, quizSaga.sendQuiz)]);
  yield all([takeEvery(actionTypes.POST_USER_REQUEST, quizSaga.postUserQuiz)]);
  yield all([
    takeEvery(actionTypes.GET_TODO_DETAIL_REQUEST, todoListSaga.todoDetail),
  ]);
  yield all([
    takeEvery(actionTypes.GET_COMPLETED_WORK, completedWorkSaga.completedWork),
  ]);
  yield all([
    takeEvery(
      actionTypes.GET_IN_COMPLETED_WORK,
      incompleteWorkSaga.incompleteWork,
    ),
  ]);
  yield all([
    takeEvery(
      actionTypes.GET_TODO_DASHBOARD_REQUEST,
      todoDashboardSaga.todoDashBoard,
    ),
  ]);
  yield all([takeEvery(actionTypes.GET_USER_REQUEST, userSaga.getUserDetail)]);
}
