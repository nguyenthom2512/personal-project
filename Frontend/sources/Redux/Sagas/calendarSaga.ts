import { put, call } from 'redux-saga/effects';
import { apiGet, apiPost } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as calendarActions from '../Actions/calendarActions';
import _ from 'lodash';
import moment from 'moment';
import { dateFormat } from '../../Helpers/constant';

export function* calendar(actions: {
  payload: { start_time: Date | string; end_time: Date | string };
  type: string;
  isToday: boolean;
}) {
  const url = serviceUrls.url.todoGroup;
  try {
    const respone = yield call(apiGet, url, actions.payload);
    if (respone.error) {
      yield put(calendarActions.getCalendarFailed(respone.errorMessage));
    } else {
      const payload: any = {
        listEvent: _.flatten(Object.values(respone.response)),
        listEventByDay: respone.response,
      };
      const todayData =
        respone.response[moment().format(dateFormat.year_month_day)];
      if (!_.isEmpty(todayData)) {
        payload['todoList'] = todayData;
      }
      yield put(calendarActions.getCalendarSuccess(payload));
    }
  } catch (error) {
    yield put(calendarActions.getCalendarFailed(error));
  }
}
