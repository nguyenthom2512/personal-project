import { put, call } from 'redux-saga/effects';
import { apiGet, IReturnType } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as todoListActions from '../Actions/todoListActions';
import _ from 'lodash';

export function* todoList(actions: any) {
  const url = serviceUrls.url.todoGroup;
  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);

    if (response.error) {
      yield put(todoListActions.getTodoListFailed(response.errorMessage));
    } else {
      yield put(todoListActions.getTodoListSuccess(response.response));
    }
  } catch (error) {
    yield put(todoListActions.getTodoListFailed(error));
  }
}
export function* todoDetail(actions: any) {
  const url = `${serviceUrls.url.todo}${actions.payload}`;
  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);

    if (response.error) {
      yield put(todoListActions.getTodoDetailFailed(response.errorMessage));
    } else {
      yield put(todoListActions.getTodoDetailSuccess(response.response));
    }
  } catch (error) {
    yield put(todoListActions.getTodoDetailFailed(error));
  }
}
