import { call, put } from '@redux-saga/core/effects';
import { apiGet, IReturnType } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import { todoDashboardActions } from '../Actions';

export function* todoDashBoard(actions: any) {
  const url = serviceUrls.url.dashboard;
  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);

    if (response.error) {
      yield put(
        todoDashboardActions.getTodoDashboardFailed(response.errorMessage),
      );
    } else {
      yield put(
        todoDashboardActions.getTodoDashboardSuccess(response.response),
      );
    }
  } catch (error) {
    yield put(todoDashboardActions.getTodoDashboardFailed(error));
  }
}
