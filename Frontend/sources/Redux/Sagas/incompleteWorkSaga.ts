import { put, call } from 'redux-saga/effects';
import { apiGet, apiPost, IReturnType } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as todoListActions from '../Actions/todoListActions';
import _ from 'lodash';

export function* incompleteWork(actions: any) {
  
  const url = `${serviceUrls.url.todo}`;  

  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);
    if (response.error) {
      yield put(todoListActions.getInCompleteWorkFailed(response.errorMessage));
    } else {
      yield put(
        todoListActions.getInCompleteWorkSuccess({
          data: response.response,
        }),
      );
    }
  } catch (error) {
    yield put(todoListActions.getInCompleteWorkFailed(error));
  }
}
