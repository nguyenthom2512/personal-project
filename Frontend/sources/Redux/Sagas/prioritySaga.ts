import { put, call } from 'redux-saga/effects';
import { apiGet, apiPost, IReturnType } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as todoListActions from '../Actions/todoListActions';
import _ from 'lodash';

export function* priority(actions: any) {
  const url = serviceUrls.url.priority;
  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);
    // const { response: resData } = response;
    // const newResponse = resData?.map((el: any) => {
    //   return {
    //     ...el,
    //     ...{
    //       priority_todo: el.priority_todo.map((elm: any, index: number) => {
    //         return {
    //           ...elm,
    //           ...{
    //             image:
    //               index % 2 == 0
    //                 ? 'https://phongvu.vn/cong-nghe/wp-content/uploads/2021/06/laptop-chay-nhanh-hon-phong-vu-4.jpg'
    //                 : null,
    //           },
    //         };
    //       }),
    //     },
    //   };
    // });

    if (response.error) {
      yield put(todoListActions.getPriorityFailed(response.errorMessage));
    } else {
      yield put(todoListActions.getPrioritySuccess(response.response));
    }
  } catch (error) {
    yield put(todoListActions.getPriorityFailed(error));
  }
}
