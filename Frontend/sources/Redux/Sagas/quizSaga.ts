import { call } from "@redux-saga/core/effects";
import _ from "lodash";
import { apiPatch, apiPost } from "../../Services/serviceHandle";
import serviceUrls from "../../Services/serviceUrls";

export function* sendQuiz(payload: any) {
    const url = serviceUrls.url.sendQuiz + payload.body.id;
    try {
        const response: {
            errorMessage: string;
            detail: any;
            error: boolean;
            response?: any;
            results?: any;
        } = yield call(apiPatch, url, payload.body.data);


        if (response.error && _.isEmpty(response.results)) {
            payload?.callBack?.onFailed &&
                payload?.callBack?.onFailed(response.error);
        } else {
            payload?.callBack?.onSuccess &&
                payload?.callBack?.onSuccess(response.response);
        }
    } catch (error) {
        payload?.callBack?.onFailed && payload?.callBack?.onFailed(error);
    }
}


export function* postUserQuiz(payload: any) {

    const url = serviceUrls.url.userQuiz;

    try {
        const response: {
            errorMessage: string;
            detail: any;
            error: boolean;
            response?: any;
            results?: any;
        } = yield call(apiPost, url, payload.body);


        if (response.error && _.isEmpty(response.results)) {
            payload?.callBack?.onFailed &&
                payload?.callBack?.onFailed(response.error);
        } else {
            payload?.callBack?.onSuccess &&
                payload?.callBack?.onSuccess(response.response);
        }
    } catch (error) {
        payload?.callBack?.onFailed && payload?.callBack?.onFailed(error);
    }

}