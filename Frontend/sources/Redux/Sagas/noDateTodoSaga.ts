import { put, call } from 'redux-saga/effects';
import { apiGet, apiPost, IReturnType } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as noDateTodoActions from '../Actions/noDateTodoActions';
import _ from 'lodash';

export function* noDateTodo(actions: any) {
  const url = `${serviceUrls.url.todo}?start_time__isnull=True`;

  try {
    const response: IReturnType = yield call(apiGet, url, actions.payload);

    if (response.error) {
      yield put(noDateTodoActions.noDateTodoFailed(response.errorMessage));
    } else {
      yield put(
        noDateTodoActions.noDateTodoSuccess({
          data: response.response,
          payload: actions.payload,
        }),
      );
    }
  } catch (error) {
    yield put(noDateTodoActions.noDateTodoFailed(error));
  }
}
