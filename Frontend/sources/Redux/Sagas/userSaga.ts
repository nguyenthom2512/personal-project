import { put, call } from 'redux-saga/effects';
import { apiGet, apiPost, setToken } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as userActions from '../Actions/userActions';
import _ from 'lodash';

export function* login(payload: any) {
  const { body, callBacks } = payload;
  const url = serviceUrls.url.login;
  try {
    const res: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
    } = yield call(apiPost, url, body);
    if (res.error) {
      callBacks?.onError && callBacks?.onError(res.errorMessage);
      yield put(userActions.loginFailed(res.errorMessage));
    } else {
      callBacks?.onSuccess && callBacks?.onSuccess(res.errorMessage);
      yield put(userActions.loginSuccess(res.response));
    }
  } catch (error) {
    callBacks?.onError && callBacks?.onError(error);
    yield put(userActions.loginFailed(error));
  }
}
export function* loginSocial(payload: any) {
  const { body, callBacks } = payload;
  const url = serviceUrls.url.loginSocial;
  try {
    const res: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
    } = yield call(apiPost, url, body);
    if (res.error) {
      callBacks?.onError && callBacks?.onError(res.errorMessage);
      yield put(userActions.loginFailed(res.errorMessage));
    } else {
      callBacks?.onSuccess && callBacks?.onSuccess(res.errorMessage);
      yield put(userActions.loginSuccess(res.response));
    }
  } catch (error) {
    callBacks?.onError && callBacks?.onError(error);
    yield put(userActions.loginFailed(error));
  }
}

export function* getUserDetail(actions: any) {
  const url = `${serviceUrls.url.raic}${actions.payload}`;
  try {
    // @ts-ignore
    const response = yield call(apiGet, url);
    console.log('______________________', response, actions.payload)
    if (response.error) {
      yield put(userActions.getUserRequestFailed(response.errorMessage));
    } else {
      yield put(userActions.getUserRequestSuccess(response));
    }
  } catch (error) {
    yield put(userActions.getUserRequestFailed(error));
  }
}



export function* logout() {
  setToken('');
}
