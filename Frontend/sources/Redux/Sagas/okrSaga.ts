import { call, put } from '@redux-saga/core/effects';
import _ from 'lodash';
import { useState } from 'react';
import { array } from 'yup';
import {
  apiDelete,
  apiGet,
  apiPatch,
  apiPost,
  apiPut,
} from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import * as okrActions from '../Actions/okrActions';

export const TKRError = {
  other: 'OTHER',
  user: 'diffUSER',
};

//getListOKR
export function* getListOKR(payload: any) {
  const url = serviceUrls.url.okr;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiGet, url, payload.body);
    if (response.error && !_.isEmpty(response.results)) {
      yield put(okrActions.getListOKRFailed(response.error));
    } else {
      yield put(
        okrActions.getListOKRSuccess({
          response: response.response,
          page: payload.body.page,
        }),
      );
    }
  } catch (error) {
    yield put(okrActions.getListOKRFailed(error));
  }
}
export function* getListOkrParent(payload: any) {
  const url = serviceUrls.url.okr;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiGet, url, payload.body);
    if (response.error && !_.isEmpty(response.results)) {
      yield put(okrActions.getListOkrParentFailed(response.error));
    } else {
      yield put(
        okrActions.getListOkrParentSuccess({
          response: response.response,
          page: payload.body.page,
        }),
      );
    }
  } catch (error) {
    yield put(okrActions.getListOkrParentFailed(error));
  }
}

// create OKR
export function* createOKR(payload: any) {
  const url = serviceUrls.url.okr;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiPost, url, payload?.body);
    if (response.error) {
      payload?.callBack?.onFailed && payload?.callBack?.onFailed(response);
      yield put(okrActions.createOKRFailed(response));
    } else {
      payload?.callBack?.onSuccess && payload?.callBack?.onSuccess(response);
      yield put(okrActions.createOKRSuccess(response));
    }
    // if (response.error && !_.isEmpty(response.results)) {
    //   yield put(userActions.getListOKRFailed(response.error));
    // } else {
    //   yield put(userActions.getListOKRSuccess(response.response));
    // }
  } catch (error) {
    payload.onFailed && payload.onFailed(error);
    // yield put(userActions.getListOKRFailed(error));
  }
}

export function* filterOkr(payload: any) {
  const url = serviceUrls.url.okrOwner;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiGet, url, payload.body);

    if (response.error && !_.isEmpty(response.results)) {
      yield put(okrActions.getListOkrFilterFailed(response.error));
    } else {
      yield put(okrActions.getListOkrFilterSuccess(response.response));
    }
  } catch (error) {
    yield put(okrActions.getListOKRFailed(error));
  }
}

//update result
export function* upDateResult(payload: any) {
  const url = serviceUrls.url.result + payload.body.id;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiPatch, url, payload.body);

    if (response.error) {
      let errorText = response.errorMessage;
      if (
        response.detail.message &&
        response.detail.message.toString() === 'user is different token'
      ) {
        errorText = TKRError.user;
      }
      payload?.callBack?.onFailed && payload?.callBack?.onFailed(errorText);
    } else {
      payload?.callBack?.onSuccess &&
        payload?.callBack?.onSuccess(response.response);
    }
  } catch (error) {
    payload?.callBack?.onFailed && payload?.callBack?.onFailed(error);
  }
}

//Update Okr
export function* updateOkr(payload: any) {
  const url = serviceUrls.url.okr + payload.body.id;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiPut, url, payload.body);
    console.log('response', response);

    if (response.error) {
      payload?.callBack?.onFailed &&
        payload?.callBack?.onFailed(
          response?.detail?.toString() || response.errorMessage,
        );
    } else {
      payload?.callBack?.onSuccess &&
        payload?.callBack?.onSuccess(response.response);
    }
  } catch (error) {
    payload?.callBack?.onFailed && payload?.callBack?.onFailed(error);
  }
}

//Delete OKR
export function* DeleteOkr(payload: any) {
  const url = serviceUrls.url.okr + payload.body;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiDelete, url, payload.body);

    if (response.error && _.isEmpty(response.results)) {
      payload?.callBack?.onFailed &&
        payload?.callBack?.onFailed(response.error);
    } else {
      payload?.callBack?.onSuccess &&
        payload?.callBack?.onSuccess(response.response);
    }
  } catch (error) {
    payload?.callBack?.onFailed && payload?.callBack?.onFailed(error);
  }
}
//delete OkrResult
export function* DeleteOkrResult(payload: any) {
  const url = serviceUrls.url.result + payload.id;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiDelete, url, payload.body);
    if (response.error && _.isEmpty(response.results)) {
      payload?.callBack?.onFailed &&
        payload?.callBack?.onFailed(response.error);
    } else {
      payload?.callBack?.onSuccess &&
        payload?.callBack?.onSuccess(response.response);
    }
  } catch (error) {
    payload?.callBack?.onFailed && payload?.callBack?.onFailed(error);
  }
}

// create KR
export function* createKr(payload: any) {
  const url = serviceUrls.url.result;
  try {
    const response: {
      errorMessage: string;
      detail: any;
      error: boolean;
      response?: any;
      results?: any;
    } = yield call(apiPost, url, payload?.body);
    if (response.error) {
      payload?.callBack?.onFailed && payload?.callBack?.onFailed(response);
      yield put(okrActions.createOKRFailed(response));
    } else {
      payload?.callBack?.onSuccess && payload?.callBack?.onSuccess(response);
      yield put(okrActions.createOKRSuccess(response));
    }
  } catch (error) {
    payload.onFailed && payload.onFailed(error);
  }
}
