import { useNavigation, useRoute } from '@react-navigation/core';
import { Field, Formik } from 'formik';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ActivityIndicator,
  Alert,
  Image,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';
import {
  AppButton,
  AppContainer,
  AppText,
  ErrorMessages,
} from '../../Components';
import AppRadio from '../../Components/AppRadio';
import useGetDataDetailQuiz from '../../Hooks/getQuizDetail';
import { quizActions } from '../../Redux/Actions';
import { RootState } from '../../Redux/Reducers';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface Props {}
export default (props: Props) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const route = useRoute<any>();
  const { quizData, inspiration } = route.params;
  console.log('quizData, inspriration', quizData, inspiration);

  const [dataQuiz, loading, error, refetchQuiz] = useGetDataDetailQuiz(
    quizData.quiz,
  );
  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const [idquiz, setIdQuiz] = useState<number>(0);
  const [time, setTime] = useState<number>(0);
  const interval = useRef<any>(null);

  console.log('idquiz', idquiz);

  useEffect(() => {
    setTime(dataQuiz?.duration || 0);
  }, [dataQuiz]);

  const countDown = () => {
    interval.current = setInterval(() => {
      setTime((time) => {
        if (time <= 1) {
          clearInterval(interval.current);
          Alert.alert(t('common:notice'), t('common:endTime'), [
            {
              text: `${t('common:ok')}`,
              onPress: () => {
                navigation.goBack();
              },
            },
          ]);
          return 0;
        }
        return time - 1;
      });
    }, 1000);
  };
  useEffect(() => {
    return () => {
      interval.current && clearInterval(interval.current);
    };
  }, []);
  const processInitialValue = () => {
    const array = (dataQuiz?.questions || []).map((elm) => {
      return {
        question: elm.id,
        answer: '',
      };
    });
    const newData = {
      question_quiz: array,
      quiz: quizData?.quiz,
      user,
    };
    return newData;
  };
  const validateCreateKr = yup.object().shape({
    question_quiz: yup.array().of(
      yup.object().shape({
        answer: yup.string().required(t('common:required')),
      }),
    ),
  });
  const onSubmit = (data: object) => {
    dispatch(
      quizActions.sendQuiz(
        { data: { ...data, quiz: quizData?.quiz }, id: idquiz },
        {
          onSuccess: (success: any) => {
            console.log('success', success);
            Alert.alert(
              t('common:notice'),
              t('quiz:quizDone', { point: success.total_point }),
              [
                {
                  text: `${t('common:ok')}`,
                  onPress: () => navigation.goBack(),
                },
              ],
            );
          },
          onFailed: (error: any) => {},
        },
      ), 
    );
  };
  
  return (
    <AppContainer title={`${t('quiz:title')} ${quizData.okr_name}`} isBack>
      {dataQuiz ? (
        <Formik
          initialValues={processInitialValue()}
          validationSchema={validateCreateKr}
          onSubmit={onSubmit}
        >
          {({
            handleChange,
            values,
            handleSubmit,
            setFieldValue,
            setValues,
            errors,
          }) => {
            console.log('asdd', { errors });

            return (
              <View style={Styles.containerScreen}>
                <View style={Styles.wrapButton}>
                  {/* {time == dataQuiz.duration && ( */}
                  <AppButton
                    disabled={time !== dataQuiz.duration}
                    title={time == dataQuiz.duration ? t('common:start') : time}
                    onPress={() => {
                      dispatch(
                        quizActions.postUserQuiz(
                          { user: user, quiz: quizData?.quiz, inspiration },
                          {
                            onSuccess: (success: any) => {
                              console.log('success', success);

                              setIdQuiz(success.id);
                              countDown();
                            },
                            onFailed: (error: any) => {
                              Alert.alert(t('common:notice'), error.toString());
                            },
                          },
                        ),
                      );
                    }}
                    containerStyle={Styles.buttonStart}
                  />
                  {/* )} */}
                  {/* <View style={Styles.wrapTime}>
                    <AppText>{time}</AppText>
                  </View> */}
                </View>
                <View style={{ flex: 1 }}>
                  <ScrollView showsVerticalScrollIndicator={false}>
                    {(dataQuiz?.questions || []).map((elm: any, index: any) => (
                      <View key={elm.id} style={Styles.container}>
                        <AppText style={{ fontWeight: 'bold' }}>
                          Câu hỏi số {index + 1}:{' '}
                          <AppText style={{ fontWeight: 'normal' }}>
                            {elm.content}
                          </AppText>
                        </AppText>
                        {(elm.attachment_question || []).map((el: any) => (
                          <Image
                            style={Styles.imageStyle}
                            source={{
                              uri: `${serviceUrls.url.IMAGE}${el?.file_url}`,
                            }}
                          />
                        ))}
                        {(elm.answers || []).map((e: any) => (
                          <TouchableOpacity
                            activeOpacity={1}
                            onPress={() => {
                              if (
                                values.question_quiz[index]?.answer === e.id
                              ) {
                                setFieldValue(
                                  `question_quiz[${index}].answer`,
                                  '',
                                );
                              } else {
                                setFieldValue(
                                  `question_quiz[${index}].answer`,
                                  e.id,
                                );
                              }
                            }}
                          >
                            <View style={Styles.answerWrap}>
                              <Field
                                name={`question_quiz[${index}].answer`}
                                title={e.content}
                                component={AppRadio}
                                checked={
                                  values.question_quiz[index].answer === e.id
                                }
                                disabled={idquiz ? true : false}
                                containerStyle={{
                                  paddingRight: 16,
                                  backgroundColor: 'white',
                                  // borderWidth: 0,
                                }}
                                textStyle={{ fontWeight: '500' }}
                              />
                            </View>
                          </TouchableOpacity>
                        ))}
                        <ErrorMessages
                          name={`question_quiz[${index}].answer`}
                        />
                      </View>
                    ))}
                  </ScrollView>
                </View>
                {idquiz ? (
                  <AppButton
                    containerStyle={{ margin: theme.spacing.p16 }}
                    title={t('common:save')}
                    onPress={handleSubmit}
                  />
                ) : null}
              </View>
            );
          }}
        </Formik>
      ) : (
        <View style={Styles.emptyData}>
          {loading ? (
            <ActivityIndicator />
          ) : (
            <AppText>{t('common:cantFindQuiz')}</AppText>
          )}
        </View>
      )}
    </AppContainer>
  );
};
