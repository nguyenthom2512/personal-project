import { StyleSheet } from 'react-native';
import { fontSize } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    containerScreen: {
      backgroundColor: theme.color.white,
      flex: 1
    },
    flex1: { flex: 1 },
    answerWrap: { flex: 1, marginHorizontal: -16 },
    container: {
      padding: theme.spacing.p16,
      backgroundColor: theme.color.white,
      shadowColor: theme.color.grayBox,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      margin: theme.spacing.p16,
      borderRadius: theme.spacing.p8,
    },
    wrapButton: {
      height: 80,
      alignItems: 'center',
      paddingTop: 16,
    },
    wrapTime: {
      position: 'absolute',
      top: 16,
      right: 10,
      borderWidth: 1,
      borderRadius: 36,
      width: 48,
      height: 48,
      alignItems: 'center',
      justifyContent: 'center',
    },
    imageStyle: {
      width: '100%',
      height: 200,
      resizeMode: 'contain',
      marginVertical: theme.spacing.p4,
    },
    buttonStart: { width: '50%' },
    emptyData: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  });
  return styles;
};
