import { useNavigation } from '@react-navigation/native';
import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, FlatList, TouchableOpacity, View } from 'react-native';
import { AppButton, AppContainer, AppImage, AppText } from '../../Components';
import { PresentItem } from '../../interfaces/present.interface';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import MiniDiamond from '../../Assets/Svg/miniDiamond.svg';
import styles from './styles';
import todoDashboardReducer from '../../Redux/Reducers/todoDashboardReducer';
import presentService from '../../Services/presentService';
import { IndicatorContext } from '../../Context';
import { useEffect } from 'react';
import { useState } from 'react';

interface Props {}

const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';

export default (props: Props) => {
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const navigation = useNavigation();
  const { setLoading } = useContext(IndicatorContext);
  const [dataList, setDataList] = useState([]);
  const [reRender, setReRender] = useState(false);
  useEffect(() => {
    presentService
      .getListPresent()
      .then((res: any) => {
        setDataList(res.response);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        Alert.alert(t('common:notice'), err);
      });
  }, [reRender]);

  return (
    <AppContainer isBack title={t('common:listGift')}>
      <View style={{ flex: 1, paddingHorizontal: theme.spacing.p20 }}>
        <FlatList
          data={dataList}
          renderItem={({ item, index }: any) => {
            return (
              <View style={Styles.itemContainer}>
                <View
                  style={{
                    flexDirection: 'row',
                    flex: 1,
                  }}
                >
                  <AppImage
                    source={{
                      uri: item?.present_data?.img_url
                        ? `${serviceUrls.url.IMAGE}${item?.present_data?.img_url}`
                        : DEFAULT_AVATAR,
                    }}
                    imageStyle={Styles.imageStyles}
                  />
                  <View style={Styles.infoItem}>
                    <AppText style={Styles.textStyles} numberOfLines={1}>
                      {item?.present_data?.present_name}
                    </AppText>
                    <View style={Styles.diamondWrapper}>
                      <View style={Styles.diamondContent}>
                        <AppText style={Styles.point}>
                          {item?.present_data.price}
                        </AppText>
                        <MiniDiamond />
                      </View>
                    </View>
                  </View>
                </View>
                <View style={Styles.btnReceived}>
                  <AppButton
                    onPress={() => {
                      Alert.alert(
                        t('common:is_received'),
                        `${item?.present_data?.present_name}`,
                        [
                          {
                            text: t('common:cancel'),
                            onPress: () => {},
                          },
                          {
                            text: t('common:confirm'),
                            onPress: () => {
                              presentService
                                .putPresentChange(item.id, {
                                  quantity: item.quantity,
                                  present: item.present,
                                  is_receive: true,
                                })
                                .then((res) => {
                                  setReRender(!reRender);
                                })
                                .catch((err) => {
                                  Alert.alert(t('common:notice'), err);
                                });
                            },
                          },
                        ],
                      );
                    }}
                    title={t('common:received')}
                    cancel={item.is_receive}
                    disabled={item.is_receive}
                  />
                </View>
              </View>
            );
          }}
          keyExtractor={(_, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          // onRefresh={refetch}
          // refreshing={loading}
        />
      </View>
    </AppContainer>
  );
};
// [1,1,1,1,1,1,1,1]
