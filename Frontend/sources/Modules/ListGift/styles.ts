import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    numberDiamond: {
      fontSize: theme.fontSize.f36,
      marginRight: theme.spacing.p8,
    },
    diamondText: {
      flexDirection: 'row',
      alignItems: 'center',
      flex: 1,
    },
    diamondStyles: {
      flexDirection: 'row',
      alignItems: 'center',
      backgroundColor: theme.color.white,
      padding: theme.spacing.p12,
      borderRadius: theme.spacing.p4,
    },
    container: {
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
      flex: 1,
    },
    itemContainer: {
      backgroundColor: theme.color.white,
      marginTop: theme.spacing.p16,
      padding: theme.spacing.p16,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: theme.spacing.p8,
    },
    imageStyles: {
      width: 50,
      height: 50,
      borderRadius: theme.spacing.p4,
    },
    infoItem: {
      flexDirection: 'column',
      flex: 2,
      marginLeft: theme.spacing.p12,
    },
    diamondWrapper: {
      flexDirection: 'row',
      flex: 1,
    },
    diamondContent: {
      flexDirection: 'row',
      backgroundColor: theme.color.lightGray,
      borderRadius: theme.spacing.p20,
      paddingHorizontal: theme.spacing.p8,
      alignItems: 'center',
      marginTop: theme.spacing.p4,
    },
    point: {
      marginRight: theme.spacing.p4,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    centeredView: {
      backgroundColor: theme.color.white,
      borderRadius: theme.spacing.p8,
      justifyContent: 'space-between',
      padding: theme.spacing.p16,
      alignItems: 'center',
    },
    modalView: {
      flexDirection: 'row',
      alignItems: 'center',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    buttonContainer: {
      flexDirection: 'row',
      marginVertical: theme.spacing.p20,
      marginTop: theme.spacing.p28,
      paddingHorizontal: theme.spacing.p16,
    },
    textStyles: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    headerStyles: {
      fontSize: theme.fontSize.f20,
    },
    myDiamond: {
      alignItems: 'center',
      marginBottom: theme.spacing.p12,
      borderRadius: theme.spacing.p28,
      backgroundColor: theme.color.mercury,
      paddingHorizontal: theme.spacing.p16,
      padding: theme.spacing.p8,
      marginTop: theme.spacing.p12,
    },
    textModal: {
      marginTop: theme.spacing.p16,
      fontSize: theme.fontSize.f18,
    },
    btnReceived: {
      width:  100
    }
  });
  return styles;
};
