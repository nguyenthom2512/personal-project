import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    headerContainer: {
      backgroundColor: theme.color.blackHeader,
    },
    iconName: {
      width: 48,
      height: 48,
      borderRadius: 24,
      marginRight: theme.spacing.p16,
    },
    line: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    txtFromNow: {
      fontSize: theme.fontSize.f14,
      color: theme.color.shark,
      opacity: 0.5,
      marginTop: theme.spacing.p8,
    },
    wrapItem: {
      flexDirection: 'row',
      padding: theme.spacing.p16,
    },
    textDesc: {
      color: theme.color.black,
    },
  });
  return styles;
};
