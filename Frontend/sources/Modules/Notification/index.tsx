import { useNavigation } from '@react-navigation/core';
import moment from 'moment';
import 'moment/locale/vi';
import React, { useContext, useRef, useState } from 'react';
import { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  DeviceEventEmitter,
  FlatList,
  Image,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native';
import { Modalize } from 'react-native-modalize';
import Feather from 'react-native-vector-icons/Feather';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from 'react-redux';
import { AppContainer, AppFlatlistFooter, AppText } from '../../Components';
import ModalConfirm from '../../Components/ModalConfirm';
import ModalizeApp from '../../Components/ModalizeApp';
import { CallContext, IndicatorContext } from '../../Context';
import { dateFormat, EventName } from '../../Helpers/constant';
import useGetNotification from '../../Hooks/getNotification';
import useToggleDialog from '../../Hooks/toggleModal';
import { NotificationItem } from '../../interfaces/notification.interface';
import { getTodoDashboardRequest } from '../../Redux/Actions/todoDashboardActions';
import { RootState } from '../../Redux/Reducers';
import dataOkr from '../../Services/dataOkr';
import getQuizService from '../../Services/getQuizService';
import notification from '../../Services/notification';
import roomServices from '../../Services/roomServices';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

const Notification = () => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const navigation = useNavigation();
  const { roomId, setRoomId } = useContext(CallContext);
  const { setLoading } = useContext(IndicatorContext);
  const [itemNoti, setItemNoti] = useState<any>();
  // const [isVisible, dataConfirm, , openDialog, closeDialog] = useToggleDialog();
  const [isVisible, dataConfirm, toggle, openDialog, closeDialog] =
    useToggleDialog();
  const RefModal = useRef<Modalize>(null);
  const openModal = () => {
    RefModal.current?.open();
  };
  const closeModal = () => {
    RefModal.current?.close();
  };

  const [dataNotification, loading, error, refetchNoti, nextPage, loadMore] =
    useGetNotification();

  const dashboardReducer = useSelector(
    (state: RootState) => state.todoDashboardReducer.data,
  );

  const dispatch = useDispatch();

  useEffect(() => {
    const notificationListener = DeviceEventEmitter.addListener(
      EventName.onReceivedNotification,
      () => {
        setTimeout(() => {
          refetchNoti();
        }, 2000);
      },
    );
    return () => {
      notificationListener.remove();
    };
  }, []);

  const typeNotification = async (item: any) => {
    switch (item.type_notification) {
      case 0:
        navigation.navigate('Challenge', { id: item.data_id });
        break;
      case 1:
        setLoading(true);
        const resInspriration = await getQuizService.getOkrInspriration(
          item.data_id,
        );
        setLoading(false);
        if (resInspriration.error) {
          Alert.alert(t('common:notice'), t('common:noInformationFound'));
        } else {
          navigation.navigate('Quiz', {
            quizData: resInspriration?.response,
            inspiration: item.data_id,
          });
        }
        console.log('resInspriration', resInspriration);
        // navigation.navigate('Quiz', { id: item.data_id });

        break;
      case 2:
        setLoading(true);
        const response = await dataOkr.getOkrResult(item.data_id);
        setLoading(false);
        if (!response.error) {
          navigation.navigate('OkrDetail', { item: response.response });
        } else {
          Alert.alert(t('common:notice'), t('common:noInformationFound'));
        }
        break;
      case 3:
        setLoading(true);
        const resRoomDetail = await roomServices.getRoomDetail({
          room_id: item.data_id,
        });
        setLoading(false);
        if (resRoomDetail.error) {
          let errorMess = resRoomDetail.errorMessage;
          if (
            resRoomDetail.errorMessage?.toLowerCase() ===
            'Not found.'.toLowerCase()
          ) {
            errorMess = t('notification:roomNotFound');
          }
          Alert.alert(t('common:notice'), errorMess);
        } else {
          // setTimeout(() => {
          console.log('______________ timeout');
          openDialog(resRoomDetail.response);
          // }, 2000);
        }
        // setRoomId(item.data_id);
        break;
      case 4:
        navigation.navigate('CheckInDetails', {
          id: item.data_id,
          onRefresh: () => {},
          isDone: true,
        });
        break;
      default:
        break;
    }
  };

  const turnOffNoti = (item: NotificationItem) => {
    closeModal();
    try {
      Alert.alert(t('common:notice'), t('common:confirmTurnOffNotifications'), [
        {
          text: t('common:ok'),
          onPress: async () => {
            const response = await notification.muteNotification({
              okr: item.okr_id,
            });
            if (response.error) {
              setLoading(false);
              Alert.alert(
                t('common:notice'),
                t('common:turnOffNotificationsFailed'),
              );
            } else {
              setLoading(false);
              Alert.alert(
                t('common:notice'),
                t('common:turnOffNotificationsSuccess'),
                [
                  {
                    text: t('common:ok'),
                    onPress: () => refetchNoti(),
                  },
                ],
              );
            }
          },
        },
        { text: t('common:cancel') },
      ]);
    } catch (error) {
      console.log('error', error);
    }
  };

  const onDelete = (id: any) => {
    closeModal();
    Alert.alert(t('common:notice'), t('common:confirmDeleteCmt'), [
      {
        text: `${t('common:ok')}`,
        onPress: async () => {
          const response = await notification.deleteNotification(id);
          if (response.error) {
            setLoading(false);
            Alert.alert(t('common:notice'), t('common:deleteFailed'));
          } else {
            setLoading(false);
            Alert.alert(t('common:notice'), t('common:deleteSuccess'), [
              {
                text: t('common:ok'),
                onPress: () => refetchNoti(),
              },
            ]);
          }
        },
      },
      { text: t('common:cancel') },
    ]);
  };

  return (
    <AppContainer
      title={t('notification:notification')}
      isBack
      styleContainer={Styles.headerContainer}
    >
      <FlatList
        showsVerticalScrollIndicator={false}
        refreshing={loading}
        onRefresh={() => refetchNoti()}
        data={dataNotification}
        onEndReached={nextPage}
        onEndReachedThreshold={Platform.OS === 'android' ? 70 : 70}
        ListFooterComponent={<AppFlatlistFooter isVisible={loadMore} />}
        keyExtractor={(item, index) => item.id.toString()}
        renderItem={({ item }) => {
          return (
            <>
              <TouchableOpacity
                onPress={async () => {
                  typeNotification(item);
                  const response = await notification.updateNotification(
                    item.id,
                    {
                      is_read: true,
                    },
                  );
                  refetchNoti();
                  dispatch(getTodoDashboardRequest());
                }}
                style={[
                  Styles.wrapItem,
                  {
                    backgroundColor: item.is_read
                      ? theme.color.white
                      : theme.color.mercury,
                  },
                ]}
              >
                {item?.user_sent_data?.img_url ? (
                  <Image
                    style={Styles.iconName}
                    source={{
                      uri: `${serviceUrls.url.IMAGE}${item?.user_sent_data?.img_url}`,
                    }}
                  />
                ) : (
                  <Image
                    style={Styles.iconName}
                    source={require('../../Assets/anhDaiDien.jpg')}
                  />
                )}
                <View style={{ flex: 1 }}>
                  <AppText>
                    <AppText style={{ fontWeight: '700' }}>
                      {item.user_sent_data.full_name}{' '}
                    </AppText>
                    {item.content}
                  </AppText>
                  <AppText style={Styles.txtFromNow}>
                    {moment(item.created_time).fromNow()}
                  </AppText>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      setItemNoti(item);
                      openModal();
                    }}
                  >
                    <Feather
                      name="more-horizontal"
                      size={theme.fontSize.f24}
                      color={theme.color.gray}
                    />
                  </TouchableOpacity>
                </View>
              </TouchableOpacity>
              <View style={Styles.line} />
            </>
          );
        }}
      />
      <ModalizeApp
        ref={RefModal}
        onRequestClose={closeModal}
        iconLeft={
          <Ionicons
            name="close-sharp"
            size={24}
            color={theme.color.gray}
            onPress={() => closeModal()}
          />
        }
        headerText="Tuỳ chọn"
      >
        <TouchableOpacity
          style={Styles.wrapItem}
          onPress={() => turnOffNoti(itemNoti)}
        >
          <Feather
            name="bell-off"
            size={theme.fontSize.f24}
            color={theme.color.gray}
          />
          <AppText style={{ marginLeft: 12 }}>
            {t('notification:turnOffNotifications')}
          </AppText>
        </TouchableOpacity>
        <View style={Styles.line} />
        <TouchableOpacity
          style={Styles.wrapItem}
          onPress={() => onDelete(itemNoti?.id)}
        >
          <Feather
            name="trash"
            size={theme.fontSize.f24}
            color={theme.color.gray}
          />
          {/* <Trash /> */}
          <AppText style={{ marginLeft: 12 }}>
            {t('notification:clearNotifications')}
          </AppText>
        </TouchableOpacity>
        <View style={Styles.line} />
      </ModalizeApp>
      <ModalConfirm
        visible={isVisible}
        titleHeader={t('notification:joinRoom')}
        onPressNo={() => {
          closeDialog();
        }}
        onPressYes={() => {
          closeDialog();
          setTimeout(() => {
            setRoomId(dataConfirm.id);
          }, 500);
        }}
      >
        <AppText style={Styles.textDesc}>
          {!!dataConfirm?.id &&
            t('notification:confirmJoinCall', {
              time: dataConfirm?.start_time
                ? moment(dataConfirm?.start_time).format(dateFormat.timeDate)
                : '',
              remainTime: dataConfirm?.start_time
                ? moment(dataConfirm?.start_time).fromNow()
                : '',
              roomName: dataConfirm?.room_name || '',
            })}
        </AppText>
      </ModalConfirm>
    </AppContainer>
  );
};

export default Notification;
