import { StyleSheet } from 'react-native';
import {
  isIOS,
  ScreenHeight,
  ScreenWidth,
} from 'react-native-elements/dist/helpers';
import { headerCalendarHeight } from '../../Helpers/constant';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    containerHeader: {
      opacity: 1,
      width: ScreenWidth,
      backgroundColor: theme.color.blackHeader,
      flexDirection: 'row',
      justifyContent: 'space-around',
      paddingVertical: theme.spacing.p20,
      alignItems: 'center',
      height: headerCalendarHeight,
      left: -ScreenWidth / 2 - 4,
      top: -8,
      position: 'absolute',
      zIndex: 99,
    },
    headerItem: {
      alignItems: 'center',
      paddingHorizontal: 10,
      paddingVertical: 4,
      borderRadius: 4,
    },
    textDay: {
      color: theme.color.backgroundColor,
      opacity: 0.5,
      fontSize: theme.fontSize.f14,
    },
    textDate: {
      color: theme.color.backgroundColor,
      fontSize: theme.fontSize.f18,
      fontWeight: isIOS ? '600' : 'bold',
      padding: 4,
    },
    titleEvent: {
      color: '#26272E',
      fontSize: theme.fontSize.f14,
      // textAlign: 'justify',
    },
    subTitle: {
      color: theme.color.backgroundColor,
      fontSize: theme.fontSize.f16,
      fontWeight: isIOS ? '600' : 'bold',
      marginBottom: theme.spacing.p8,
    },

    bodyContainerTodayType: {
      backgroundColor: theme.color.dartBlue,
    },

    calendarContainerStyle: {
      flex: 1,
      padding: 4,
      zIndex: 1,
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: '50%',
    },

    activeHeader: {
      backgroundColor: theme.color.mediumPurple,
    },
    todoListContainer: {
      flex: 1,
      position: 'absolute',
      top: headerCalendarHeight,
      left: 0,
      bottom: 0,
      right: 0,
      padding: theme.spacing.p4,
    },

    calendarContainer: {
      zIndex: 1,
      flex: 1,
      borderRadius: 4,
      shadowColor: theme.color.blackBoder,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    contentContainer: {
      zIndex: 0,
      backgroundColor: `${theme.color.whiteSmoke}A0`,
      position: 'absolute',
      left: 8,
      top: headerCalendarHeight + 4,
      bottom: 4,
      borderRadius: 4,
    },
    backgroundImage: { flex: 1, flexDirection: 'row', zIndex: 1 },
    iconArrow: {
      color: 'rgba(255, 255, 255, 0.5)',
      paddingHorizontal: theme.spacing.p4,
    },
    plusCenter: {
      width: 40,
      height: 40,
      backgroundColor: theme.color.mediumPurple,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
      left: 10,
      bottom: 10,
    },
    bodyContainerStyle: {
      paddingVertical: 12,
      marginTop: headerCalendarHeight,
      backgroundColor: theme.color.dartBlue,
      opacity: 0.9,
      borderRadius: theme.spacing.p8,
    }
  });
  return styles;
};
