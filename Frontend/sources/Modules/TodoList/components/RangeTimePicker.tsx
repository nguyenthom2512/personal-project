import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import { AppText } from '../../../Components';
import { minuteInHour } from '../../../Helpers/constant';
import { useTheme } from '../../../Theme';

const hours = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]
const minutes = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59]


interface RangeTimePickerProps {
  startTime?: number;
  endTime?: number;
  onClose: () => void;
  onSummit: ({ startTime, endTime }: { startTime: number, endTime: number }) => void;
}

const RangeTimePicker = ({ onClose, startTime = 0, endTime = 0, onSummit }: RangeTimePickerProps) => {
  const { theme } = useTheme();
  const {t} = useTranslation();
  const [dataPicker, setDataPicker] = useState({
    startTimeHour: Math.floor(startTime / minuteInHour),
    startTimeMinute: Math.floor(startTime % minuteInHour),
    endTimeHour: Math.floor(endTime / minuteInHour),
    endTimeMinute: Math.floor(endTime % minuteInHour)
  });

  useEffect(() => {
    const startTimeHour = Math.floor(startTime / minuteInHour);
    const startTimeMinute = Math.floor(startTime % minuteInHour);
    const endTimeHour = Math.floor(endTime / minuteInHour);
    const endTimeMinute = Math.floor(endTime % minuteInHour);
    let newDataPicker = dataPicker;

    setDataPicker({ ...newDataPicker, startTimeHour: startTimeHour, startTimeMinute: startTimeMinute, endTimeHour: endTimeHour, endTimeMinute: endTimeMinute })
  }, [startTime, endTime])

  const onChangeFieldDataPicker = (type: 'startTimeHour' | 'startTimeMinute' | 'endTimeHour' | 'endTimeMinute', value: number) => {
    dataPicker[type] = value;
    setDataPicker({ ...dataPicker });
  }

  const handleSubmit = () => {
    const startTimeMinute = dataPicker.startTimeHour * minuteInHour + dataPicker.startTimeMinute;
    const endTimeMinute = dataPicker.endTimeHour * minuteInHour + dataPicker.endTimeMinute;
    onSummit({ startTime: startTimeMinute, endTime: endTimeMinute });
  }

  const renderCarousel = (listData: number[], type: 'startTimeHour' | 'startTimeMinute' | 'endTimeHour' | 'endTimeMinute') => {
    return (
      <Carousel
        firstItem={listData.findIndex((elm) => elm == dataPicker[type])}
        activeAnimationType="decay"
        loop
        vertical
        layout={'stack'}
        layoutCardOffset={8}
        ref={(c) => c}
        data={listData}
        onSnapToItem={(index) => onChangeFieldDataPicker(type, listData[index])}
        renderItem={({ item, _index }: any) => renderItemSlider(item)}
        itemHeight={100}
        sliderHeight={130}
        slideStyle={{ alignItems: 'center', justifyContent: 'center' }}
      />
    )
  }

  const renderItemSlider = (value: any) => (
    <View style={[styles.boxTime, { backgroundColor: theme.color.mediumPurple }]}>
      <AppText style={[{ fontSize: theme.fontSize.f32 }, styles.text]}>{value}</AppText>
    </View>
  )

  const renderTimePicker = (title: string, typeHour: 'startTimeHour' | 'endTimeHour', typeMinute: | 'startTimeMinute' | 'endTimeMinute') => {
    return (
      <View style={[styles.container, { backgroundColor: theme.color.lavenderBlue, }]}>
        <AppText>{title}</AppText>
        <View style={styles.rowCenter}>
          {renderCarousel(hours, typeHour)}
          <Text style={[{ fontSize: theme.fontSize.f40 }, styles.text]}>:</Text>
          {renderCarousel(minutes, typeMinute)}
        </View>
      </View>
    )
  }

  return (
    <View>
      <View style={{ flexDirection: 'row' }}>
        {renderTimePicker(t('calendar:startTime'), 'startTimeHour', 'startTimeMinute')}
        {renderTimePicker(t('calendar:endTime'), 'endTimeHour', 'endTimeMinute')}
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity onPress={handleSubmit} style={[{ backgroundColor: theme.color.mediumPurple }, styles.button]}>
          <AppText style={[{ fontSize: theme.fontSize.f16 }, styles.text]}>{t('calendar:update')}</AppText>
        </TouchableOpacity>
        <TouchableOpacity onPress={onClose} style={[{ backgroundColor: theme.color.mediumPurple }, styles.button]}>
          <Text style={[{ fontSize: theme.fontSize.f16 }, styles.text]}>{t('calendar:cancel')}</Text>
        </TouchableOpacity>
      </View>

    </View>

  )
}

const styles = StyleSheet.create({
  container: { flex: 1, alignItems: 'center', borderRadius: 8, marginHorizontal: 8, paddingVertical: 16 },
  text: { fontWeight: 'bold', color: 'white' },
  rowCenter: { alignItems: 'center', justifyContent: 'center', flexDirection: 'row' },
  boxTime: { height: 70, width: 70, borderRadius: 16, alignItems: 'center', justifyContent: 'center' },
  buttonContainer: { flexDirection: 'row', justifyContent: 'space-evenly', marginTop: 16 },
  button: { padding: 8, width: 150, alignItems: 'center' }
})

export default RangeTimePicker;