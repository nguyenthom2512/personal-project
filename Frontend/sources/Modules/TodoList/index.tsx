import {
  DrawerActions,
  useNavigation,
  StackActions,
} from '@react-navigation/native';
import moment from 'moment';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ImageBackground,
  View,
  FlatList,
  LayoutChangeEvent,
  TouchableOpacity,
  DeviceEventEmitter,
  Alert,
} from 'react-native';
import Calendar, {
  CalendarHeaderProps,
  DateRangeHandler,
  Mode,
} from '../../Components/react-native-big-calendar/index';
import { useDispatch, useSelector } from 'react-redux';
import { AppHeader, AppText } from '../../Components';
import DraggableView from '../../Components/DraggableView';
import { TodoItemComponent } from '../../Components/TodoItems';
import {
  dateFormat,
  headerCalendarHeight,
  minuteInHour,
  pixelHourCalendar,
} from '../../Helpers/constant';
import { handleDataEvent } from '../../Helpers/untils';
import { EventCalendar } from '../../Modules/AgendaCalendar';
import { getCalendarRequest } from '../../Redux/Actions/calendarActions';
import { noDateTodoRequest } from '../../Redux/Actions/noDateTodoActions';
import { getTodoListRequest } from '../../Redux/Actions/todoListActions';
import { RootState } from '../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import RangeTimePicker from './components/RangeTimePicker';
import Modal from 'react-native-modal';
import { apiPatch } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import { IndicatorContext } from '../../Context';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import ActionTypes from '../../Redux/ActionTypes';

const padding = 8;
let page = 1;
const page_size = 20;
const scrollOffsetMinutes = (moment().get('hours') - 2) * pixelHourCalendar;
const TodoListScreen = () => {
  const [currentDateSelected, setcurrentDateSelected] = useState(new Date());
  const [] = useState(new Date());
  const [listNoDateToDo, setListNoDateToDo] = useState<any[]>([]);
  const [mainLayout, setMainLayout] = useState({
    left: 0,
    top: 0,
    width: 50,
    height: 50,
  });
  const [isScroll, setScroll] = useState(true);
  const [dataModal, setDataModal] = useState<{
    endTime: number;
    startTime: number;
    todoId: number | null;
  }>({ endTime: 0, startTime: 0, todoId: null });
  const { setLoading } = useContext(IndicatorContext);

  const { theme } = useTheme();
  const navigation = useNavigation();
  const Styles = useThemeAwareObject(styles);
  const dispatch = useDispatch();

  const contentOffsetY = useRef(0);
  const calendarReducer = useSelector(
    (state: RootState) => state.calendarReducer,
  );
  const noDateTodoReducer = useSelector(
    (state: RootState) => state.noDateTodoReducer,
  );
  const { t } = useTranslation();

  const maxY = mainLayout.top + mainLayout.height - padding;
  const maxX = mainLayout.width / 2 + padding;
  const draggableViewWidth = mainLayout.width / 2 - padding * 2;

  const onGetLayout = (event: LayoutChangeEvent) => {
    const { x, y, height, width } = event.nativeEvent.layout;
    setMainLayout({
      left: x,
      top: y,
      width: width,
      height: height,
    });
  };

  useEffect(() => {
    const listener = DeviceEventEmitter.addListener('closeDrawer', (_e) => {
      navigation.dispatch(DrawerActions.closeDrawer);
    });
    dispatch(noDateTodoRequest({ page, page_size }));
    return () => {
      listener.remove();
    };
  }, []);

  useEffect(() => {
    if (noDateTodoReducer.type == ActionTypes.NO_DATE_TODO_SUCCESS) {
      setListNoDateToDo(noDateTodoReducer.data.results);
    }
  }, [noDateTodoReducer]);

  useEffect(() => {
    onRefresh();
  }, [currentDateSelected]);

  const onRefresh = () => {
    let start_time = moment(currentDateSelected).format(
      dateFormat.year_month_day,
    );
    let end_time = moment(currentDateSelected)
      .add(1, 'days')
      .format(dateFormat.year_month_day);
    dispatch(getCalendarRequest({ start_time, end_time }));
  };

  const onDrop = (value: number, itemEvent: any) => {
    const start =
      ((value + contentOffsetY.current - pixelHourCalendar / 2) * minuteInHour) / pixelHourCalendar;
    onUpdateTodo({ startTime: start, endTime: start + 60 }, itemEvent.id);
  };

  const onUpdateTodo = async (
    data: { startTime: number; endTime: number },
    todoId?: number,
  ) => {
    const { startTime, endTime } = data;
    if (moment().startOf('days').isAfter(currentDateSelected)) {
      Alert.alert(t('common:notice'), t('createMissions:validateDateTime'));
      return onCloseModal(todoId);
    }
    if (endTime < startTime) {
      Alert.alert(t('common:notice'), t('createMissions:validateTime'));
      return onCloseModal(todoId);
    }
    setLoading(true);
    try {
      const res = await apiPatch(
        `${serviceUrls.url.todo}${todoId || dataModal?.todoId}`,
        {
          start_time: moment(currentDateSelected)
            .startOf('dates')
            .add(data.startTime, 'minutes')
            .format(dateFormat.dateTimeServer),
          end_time: moment(currentDateSelected)
            .startOf('dates')
            .add(data.endTime, 'minutes')
            .format(dateFormat.dateTimeServer),
          date: moment(currentDateSelected)
            .startOf('dates')
            .format(dateFormat.year_month_day)
        },
      );
      onCloseModal(todoId);

      if (res && res.error) {
        return Alert.alert(
          t('common:notice'),
          res.detail?.message[0] || res.errorMessage,
        );
      }

      const start_time = moment().format(dateFormat.year_month_day);
      const end_time = moment().add(1, 'days').format(dateFormat.year_month_day);

      onRefresh();
      dispatch(noDateTodoRequest({ page: 1, page_size }));
      dispatch(getTodoListRequest({ start_time, end_time }));
    } catch (error: any) {
      Alert.alert(
        t('common:notice'),
        error?.toString() || t('createMissions:createTodoError'),
      );
      return onCloseModal(todoId);
    } finally {
      setLoading(false);
    }
  };

  const onEdit = (data: { start: Date; end: Date; id?: number }) => {
    setDataModal({
      endTime: moment(data.end).hours() * 60 + moment(data.end).minutes(),
      startTime:
        moment(data.start).hours() * 60 + moment(data.start).minutes() || 0,
      todoId: data.id || null,
    });
  };

  const onCloseModal = (todoId?: number) => {
    const todoRemove = noDateTodoReducer.data.results?.find(
      (el: { id: number | null | undefined }) => el.id == todoId,
    );
    if (!!todoRemove) {
      setListNoDateToDo([...listNoDateToDo, todoRemove]);
    }
    setDataModal({
      endTime: 0,
      startTime: dataModal?.startTime || 0,
      todoId: null,
    });
  };

  const renderEvent = (data: EventCalendar, touchableOpacityProps: any) => {
    const { title } = data;
    touchableOpacityProps.style[2].backgroundColor = data.color;
    return (
      <TouchableOpacity {...touchableOpacityProps}>
        <AppText numberOfLines={2} style={Styles.titleEvent}>
          {title}
        </AppText>
      </TouchableOpacity>
    );
  };

  const renderDayName = (day: number) => {
    switch (day) {
      case 0:
        return t('common:sortNameSunday');
      case 1:
        return t('common:sortNameMonDay');
      case 2:
        return t('common:sortNameTuesday');
      case 3:
        return t('common:sortNameWednesday');
      case 4:
        return t('common:sortNameThursday');
      case 5:
        return t('common:sortNameFriday');
      case 6:
        return t('common:sortNameSaturday');
      default:
        return '';
    }
  };

  const renderHeader = (
    data: React.PropsWithChildren<
      CalendarHeaderProps<EventCalendar> & { mode: Mode }
    >,
  ) => {
    const currentDate = data.dateRange[0].toDate();
    let day = moment(currentDate).startOf('isoWeeks');
    const listDate: Date[] = [];
    const rangeDate = 7;
    listDate.push(day.toDate());
    while (listDate.length < rangeDate) {
      day.add(1, 'days');
      listDate.push(day.toDate());
    }
    const onChangeCurrentDate = (date: Date) => () => {
      data.onPressDateHeader && data.onPressDateHeader(date);
    };
    return (
      <View style={Styles.containerHeader}>
        <Icon
          iconStyle={Styles.iconArrow}
          type="entypo"
          name="chevron-thin-left"
          size={25}
          onPress={onChangeCurrentDate(
            moment(currentDate).add(-1, 'weeks').endOf('isoWeeks').toDate(),
          )}
        />
        {listDate?.map((item: Date, index: number) => {
          return (
            <TouchableOpacity
              key={index.toString()}
              onPress={onChangeCurrentDate(item)}
              style={[
                Styles.headerItem,
                data.dateRange[0].date() == moment(item).date() &&
                Styles.activeHeader,
              ]}
            >
              <AppText style={Styles.textDay}>
                {renderDayName(moment(item).day())}
              </AppText>
              <AppText style={Styles.textDate}>
                {moment(item.toString()).date()}
              </AppText>
            </TouchableOpacity>
          );
        })}
        <Icon
          iconStyle={Styles.iconArrow}
          type="entypo"
          name="chevron-thin-right"
          size={25}
          onPress={onChangeCurrentDate(
            moment(currentDate).add(1, 'weeks').startOf('isoWeeks').toDate(),
          )}
        />
      </View>
    );
  };
  return (
    <>
      <AppHeader
        // isBack
        iconLeft={'arrow-left'}
        typeIconLeft={'feather'}
        onLeftPress={() => navigation.dispatch(StackActions.pop(100))}
        iconRightAdd={'menu'}
        onRightAddPress={() => {
          navigation.dispatch(DrawerActions.toggleDrawer);
        }}
      />
      <View onLayout={onGetLayout} style={{ flex: 1 }}>
        <ImageBackground
          style={Styles.backgroundImage}
          source={{
            uri: 'https://hanoimoi.com.vn/Uploads/images/tuandiep/2021/09/07/dulich.JPG',
          }}
        >
          <View
            style={[
              Styles.contentContainer,
              {
                width: mainLayout.width / 2 - padding,
              },
            ]}
          />
          <View
            style={[
              Styles.todoListContainer,
              {
                zIndex: isScroll ? 0 : 4,
              },
            ]}
          >
            <View style={{ flex: 1 }}>
              <FlatList
                scrollEnabled={isScroll}
                contentContainerStyle={{ marginBottom: 25 }}
                scrollToOverflowEnabled
                nestedScrollEnabled
                extraData={listNoDateToDo.length}
                keyExtractor={(item, _index) => item.id.toString()}
                data={listNoDateToDo}
                renderItem={({ item }) => (
                  <DraggableView
                    children={
                      <TodoItemComponent
                        data={item}
                        showPriority
                        containerStyles={{
                          backgroundColor:
                            item?.priority?.color || theme.color.pink,
                        }}
                      />
                    }
                    width={draggableViewWidth}
                    maxX={maxX}
                    minY={mainLayout.top}
                    maxY={maxY}
                    space={padding}
                    onDrop={(value) => onDrop(value, item)}
                    onDrag={(value) => setScroll(value)}
                  />
                )}
              />
              {/* <View style={Styles.plusCenter}>
                <Icon name="plus" type="feather" color={theme.color.white} />
              </View> */}
            </View>
          </View>
          <View style={Styles.calendarContainerStyle}>
            <View style={Styles.calendarContainer}>
              <Calendar
                onScroll={(event: {
                  nativeEvent: { contentOffset: { y: number } };
                }) => {
                  contentOffsetY.current =
                    event.nativeEvent.contentOffset.y -
                    headerCalendarHeight -
                    padding;
                }}
                renderHeader={(data) => renderHeader(data)}
                mode="day"
                date={currentDateSelected}
                weekEndsOn={0}
                events={handleDataEvent(calendarReducer.listEvent ?? []) || []}
                scrollOffsetMinutes={scrollOffsetMinutes}
                height={600}
                bodyContainerStyle={Styles.bodyContainerStyle}
                onPressDateHeader={(date) => {
                  setcurrentDateSelected(date);
                }}
                onSwipe={(date) => {
                  setcurrentDateSelected(date);
                }}
                renderEvent={renderEvent}
                swipeEnabled={true}
                onPressEvent={onEdit}
                theme={{
                  typography: {
                    xs: {
                      fontSize: theme.fontSize.f14,
                    },
                  },
                  palette: {
                    nowIndicator: theme.color.backgroundColor,
                    gray: {
                      '100': theme.color.grayShaft,
                      '200': theme.color.grayShaft,
                      '300': theme.color.gray,
                    },
                  },
                }}
              />
            </View>
          </View>
        </ImageBackground>
        {dataModal.endTime > 0 && (
          <Modal
            useNativeDriver
            animationIn="fadeInUp"
            animationOut="fadeInDown"
            onBackdropPress={onCloseModal}
            style={{ width: '100%', marginLeft: 0 }}
            isVisible={dataModal.endTime > 0}
          >
            <RangeTimePicker
              onSummit={onUpdateTodo}
              onClose={onCloseModal}
              startTime={dataModal?.startTime ?? 0}
              endTime={dataModal?.endTime ?? 0}
            />
          </Modal>
        )}
      </View>
    </>
  );
};
export default TodoListScreen;
