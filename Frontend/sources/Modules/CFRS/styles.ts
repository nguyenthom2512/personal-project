import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    headerContent: {
      backgroundColor: 'white',
      padding: 16,
      marginTop: 16,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: 4,
    },
    starWrapper: {
      width: 100,
      borderRadius: theme.spacing.p20,
      backgroundColor: '#F4A921',
      padding: 10,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems:'center'
    },
    textStar: {
      color: theme.color.white,
      paddingRight: theme.spacing.p12,
      fontSize:theme.fontSize.f18
    },
  });
  return styles;
};
