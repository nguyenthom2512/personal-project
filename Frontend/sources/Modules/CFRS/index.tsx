import { useNavigation } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Text, TouchableOpacity, View } from 'react-native';
import Star from '../../Assets/Svg/star.svg';
import { AppContainer, AppText } from '../../Components';
import { apiGet } from '../../Services/serviceHandle';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import FeedbackRecived from './Components/FeedbackRecived';
import FeedbackSend from './Components/FeedbackSend';
import styles from './styles';

interface Props {}

export default () => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const navigation = useNavigation();
  const [dataStatic, setDataStatic] = useState<any>({});

  const [isModalVisible, setModalVisible] = useState(false);
  const [tabActive, setTabActive] = useState('recieved');
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const getStatic = () => {
    apiGet(`${serviceUrls.url.feedback_static}`, {}).then((e) => {
      setDataStatic(e.response);
    });
  };
  useEffect(() => {
    getStatic();
  }, []);
  return (
    <AppContainer title={t('checkIn:CFRs')} isMenu>
      <View style={{ paddingHorizontal: 16 }}>
        <View style={Styles.headerContent}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <AppText style={{ fontSize: theme.fontSize.f20 }}>
              {t('common:feedback')}
            </AppText>
          </View>
          <View style={Styles.starWrapper}>
            <AppText style={Styles.textStar}>
              {(dataStatic?.received?.total || 0) -
                (dataStatic?.sent?.total || 0)}
            </AppText>
            <Star fill={theme.color.white} />
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            borderTopWidth: 3,
            borderStyle: 'solid',
            borderColor: '#f7f7f7',
            backgroundColor: '#FFFF',
            justifyContent: 'space-around',
          }}
        >
          <TouchableOpacity
            style={{
              width: '50%',
              height: 50,
              justifyContent: 'center',
              alignItems: 'center',
              borderStyle: 'solid',
              borderBottomWidth: tabActive == 'recieved' ? 1 : 0,
              borderColor: theme.color.violet,
            }}
            onPress={() => {
              setTabActive('recieved');
            }}
          >
            <AppText
              style={{
                color:
                  tabActive == 'recieved'
                    ? theme.color.violet
                    : theme.color.gray,
              }}
            >
              {t('common:receive')}
            </AppText>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '50%',
              height: 50,
              justifyContent: 'center',
              alignItems: 'center',
              borderStyle: 'solid',
              borderBottomWidth: tabActive == 'send' ? 1 : 0,
              borderColor: theme.color.violet,
            }}
            onPress={() => {
              setTabActive('send');
            }}
          >
            <AppText
              style={{
                color:
                  tabActive == 'send' ? theme.color.violet : theme.color.gray,
              }}
            >
              {t('common:youSend')}
            </AppText>
          </TouchableOpacity>
        </View>
        {tabActive == 'recieved' && (
          <FeedbackRecived received={dataStatic.received} />
        )}
        {tabActive == 'send' && <FeedbackSend sent={dataStatic.sent} />}
      </View>
    </AppContainer>
  );
};
