import { useNavigation } from '@react-navigation/core';
import moment from 'moment';
import React, { useEffect, useState } from 'react';
import { FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import { useSelector } from 'react-redux';
import Star from '../../../Assets/Svg/star.svg';
import BigStar from '../../../Assets/Svg/bigStar.svg';
import Cup from '../../../Assets/Svg/cup.svg';
import Chart from '../../../Assets/Svg/chart.svg';
import { AppImage, AppText } from '../../../Components';
import { convertParamsToQuery } from '../../../Helpers';
import { RootState } from '../../../Redux/Reducers';
import { apiGet } from '../../../Services/serviceHandle';
import serviceUrls from '../../../Services/serviceUrls';
import { findIndex } from 'lodash';
export default ({ received }: { received: any }) => {
  const [data, setData] = useState<any>([]);
  const navigation = useNavigation();
  const userReducer = useSelector((state: RootState) => state.userReducer);
  const [param, setParam] = useState({
    page: 1,
    page_size: 2,
    user_received: userReducer?.data?.user.id,
  });
  const [scolled, setScrolled] = useState(false);
  useEffect(() => {
    apiGet(
      `${serviceUrls.url.feedback}${convertParamsToQuery(param)}`,
      {},
    ).then((e) => {
      setData(e.response.results);
    });
  }, [param]);

  const indexMonthRank = findIndex(
    received?.rank_month || [],
    function (e: any) {
      return e == userReducer?.data?.user.id;
    },
  );

  const indexCycleRank = findIndex(
    received?.rank_department || [],
    function (e) {
      return e == userReducer?.data?.user.id;
    },
  );

  const handleLoadMore = () => {
    if (!setScrolled) return;
    setParam({ ...param, page: param.page + 1 });
  };
  const renderItem = ({ item }: { item: any }) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('TransactionDetailScreen')}
        key={item.id}
      >
        <View
          style={{
            flexDirection: 'row',
            padding: 12,
            backgroundColor: 'white',
            marginTop: 16,
            justifyContent: 'space-between',
            borderRadius: 4,
          }}
        >
          <View
            style={{
              width: 32,
              height: 32,
              borderRadius: 16,
              backgroundColor: 'gray',
              marginRight: 12,
            }}
          >
            <AppImage
              source={{
                uri: `${serviceUrls.url.IMAGE}${item?.user_received_data?.img_url}`,
              }}
              imageStyle={{ width: '100%', height: '100%', borderRadius: 16 }}
            />
          </View>
          <View style={{ flex: 1 }}>
            <AppText
              style={{ fontSize: 16, color: '#292D3A', fontWeight: '600' }}
            >
              {item?.user_data?.full_name || ''}
            </AppText>
            <AppText
              style={{
                fontSize: 12,
                fontWeight: '400',
              }}
            >
              {item?.criteria_title}
            </AppText>
          </View>
          <View>
            <AppText style={{ fontSize: 14, color: 'gray', fontWeight: '400' }}>
              {moment(item.created_time).format('DD/MM/YYYY')}
            </AppText>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'flex-end',
              }}
            >
              <AppText>{item?.rate || 0}</AppText>
              <Star fill={'#F4A921'} />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          marginTop: 16,
          height: 150,
          // backgroundColor: "red"
        }}
      >
        <View
          style={{
            width: '30%',
            justifyContent: 'center',
            alignItems: 'center',
            marginRight: 16,
            backgroundColor: '#FFF',
            padding: 16,
          }}
        >
          <BigStar fill={'#F4A921'} />
          <AppText
            style={{
              fontSize: 20,
              fontWeight: '600',
              marginTop: 8,
              marginBottom: 4,
            }}
          >
            {received?.total || 0}
          </AppText>
          <AppText
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#26272E',
              opacity: 0.7,
              textAlign: 'center',
            }}
          >
            Tổng sao nhận được
          </AppText>
        </View>
        <View
          style={{
            width: '30%',
            justifyContent: 'center',
            marginRight: 16,
            alignItems: 'center',
            backgroundColor: '#FFF',
            padding: 15,
          }}
        >
          <Cup fill={'#F4A921'} />
          <AppText
            style={{
              fontSize: 20,
              fontWeight: '600',
              marginTop: 8,
              marginBottom: 4,
            }}
          >
            {indexMonthRank >= 0 ? indexMonthRank + 1 : '0'}
          </AppText>
          <AppText
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#26272E',
              opacity: 0.7,
              textAlign: 'center',
            }}
          >
            Xếp hạng tháng
          </AppText>
        </View>
        <View
          style={{
            width: '30%',
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: '#FFF',
            padding: 15,
          }}
        >
          <Chart fill={'#F4A921'} />
          <AppText
            style={{
              fontSize: 20,
              fontWeight: '600',
              marginTop: 8,
              marginBottom: 4,
            }}
          >
            {indexCycleRank >= 0 ? indexCycleRank + 1 : '0'}
          </AppText>
          <AppText
            style={{
              fontSize: 14,
              fontWeight: '400',
              color: '#26272E',
              opacity: 0.7,
              textAlign: 'center',
            }}
          >
            Xếp hạng chu kì
          </AppText>
        </View>
      </View>
      <FlatList
        data={data}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
        refreshing
        onScroll={() => {
          setScrolled(true);
        }}
        onEndReached={() => {
          handleLoadMore();
        }}
      />
    </>
  );
};
