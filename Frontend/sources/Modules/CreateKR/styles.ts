import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    inputDatetime: {
      flex: 1,
      borderWidth: 1,
      borderColor: theme.color.holderPlace,
      backgroundColor: theme.color.backgroundColor,
      justifyContent: 'center',
      borderRadius: 4,
      paddingVertical: theme.spacing.p15,
      paddingHorizontal: theme.spacing.p12,
      marginVertical: theme.spacing.p8,
    },
    txtMomentDate: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    txtDeadline: {
      color: theme.color.gray,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
  });
  return styles;
};
