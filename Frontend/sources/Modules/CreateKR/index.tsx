import { useNavigation, useRoute } from '@react-navigation/core';
import { Formik } from 'formik';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, Modal, Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { ScrollView } from 'react-native-gesture-handler';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import {
  AppButton,
  AppContainer,
  AppDropDown,
  AppInputField,
  AppText,
  ErrorMessages,
} from '../../Components';
import { IndicatorContext } from '../../Context';
import { fontSize, padding } from '../../Helpers';
import useGetUnit from '../../Hooks/getUnit';
import { okrActions } from '../../Redux/Actions';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface PropsCreateKR {
  modalCreateKR?: any;
  setCloseModalCreateKR: () => void;
  refetch?: () => void;
  okr: any;
  user: any;
  unit: any;
}
const regMatch =
  /^((http|https):\/\/)?(www.)?(?!.*(http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+(\/)?.([\w\?[a-zA-Z-_%\/@?]+)*([^\/\w\?[a-zA-Z0-9_-]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/;

const CreateKR = (props: PropsCreateKR) => {
  const { setCloseModalCreateKR = () => null } = props;
  const indicatorContext = useContext(IndicatorContext);
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const route = useRoute();
  const { refetch, okr, user, okr_parent, unit }: any = route.params;
  const [dataUnit] = useGetUnit();
  const okrUnit = (dataUnit || []).map((elm) => {
    return {
      value: elm?.id,
      label: elm?.unit_name,
    };
  });

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const showDatePicker = () => {
    setDatePickerVisibility(!isDatePickerVisible);
  };
  const handleConfirm = (date: any, setFieldValue: any) => {
    const selected = moment(date).toISOString();
    setFieldValue('deadline', selected);
    showDatePicker();
  };
  const validateCreateKr = Yup.object().shape({
    key_result: Yup.string().required(t('common:required')),
    target: Yup.string().required(t('common:required')),
    unit: Yup.string().required(t('common:required')),
    user: Yup.string().required(t('common:required')),
    deadline: Yup.string().required(t('common:required')),
    okr: Yup.string().required(t('common:required')),
    current_done: Yup.string().required(t('common:required')),
    plan_url: Yup.string().matches(regMatch, t('common:requiredLink')),
    result_url: Yup.string().matches(regMatch, t('common:requiredLink')),
  });

  const [defaultKR, setDefaultKr] = useState({
    key_result: '',
    target: '',
    unit: '',
    plan_url: '',
    result_url: '',
    deadline: '',
    current_done: 0,
    user: user,
    okr: okr,
  });

  useEffect(() => {
    if (okr_parent != null) {
      const newFillKr = {
        unit: unit,
      };
      setDefaultKr({ ...defaultKR, ...newFillKr });
      setDisabled(true);
    }
  }, [okr_parent]);

  const [disabled, setDisabled] = useState(false);

  const onSubmit = (data: any) => {
    indicatorContext.setLoading(true);
    dispatch(
      okrActions.createKrRequest(data, {
        onSuccess: (success: any) => {
          indicatorContext.setLoading(false);
          Alert.alert('', `${t('common:createKrSuccess')}`, [
            {
              text: `${t('common:ok')}`,
              onPress: () => {
                navigation.goBack();
                refetch();
              },
            },
          ]);
        },
        onFailed: (error: any) => {
          indicatorContext.setLoading(false);
          Alert.alert(`${t('common:createKrFailed')}`, error.errorMessage);
        },
      }),
    );
  };
  return (
    <>
      <AppContainer
        title={`${t('common:create')} KR`}
        iconLeft="arrow-left"
        onLeftPress={() => {
          navigation.goBack(), refetch();
        }}
      >
        <ScrollView>
          <Formik
            initialValues={defaultKR}
            validationSchema={validateCreateKr}
            onSubmit={onSubmit}
            enableReinitialize
          >
            {({
              handleChange,
              values,
              handleSubmit,
              setFieldValue,
              setValues,
              errors,
            }) => {
              return (
                <View style={{ paddingHorizontal: padding.p8 }}>
                  <View style={{ flex: 1 }}>
                    <AppInputField
                      placeholder={`${t('createOkr:Okr:import')} ${t(
                        'createOkr:OkrKR:keyResult',
                      )}`}
                      name="key_result"
                    />
                  </View>

                  <View style={{ flex: 1 }}>
                    <AppInputField
                      placeholder={`${t('createOkr:Okr:import')} ${t(
                        'createOkr:OkrKR:target',
                      )}`}
                      name="target"
                      keyboardType="numeric"
                    />
                  </View>
                  <View style={{ flex: 1 }}>
                    <AppDropDown
                      placeHolder={t('OKR:unit')}
                      value={values.unit}
                      isSearch
                      onChoose={(value) => {
                        setFieldValue('unit', value.value);
                      }}
                      data={okrUnit}
                      disabled={disabled}
                    />
                    <ErrorMessages name="unit" />
                  </View>

                  <AppInputField
                    placeholder={`${t('createOkr:Okr:import')} ${t(
                      'createOkr:OkrKR:planUrl',
                    )}`}
                    name="plan_url"
                  />
                  <AppInputField
                    placeholder={`${t('createOkr:Okr:import')} ${t(
                      'createOkr:OkrKR:resultUrl',
                    )}`}
                    name="result_url"
                  />
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      style={Styles.inputDatetime}
                      onPress={() => showDatePicker()}
                    >
                      <AppText style={Styles.txtMomentDate}>
                        {values.deadline ? (
                          moment(values.deadline).format('DD/MM/YYYY')
                        ) : (
                          <Text style={Styles.txtDeadline}>{`${t(
                            'createOkr:OkrKR:deadline',
                          )}`}</Text>
                        )}
                      </AppText>
                    </TouchableOpacity>
                    <ErrorMessages name="deadline" />
                    <DateTimePickerModal
                      locale="vi"
                      isVisible={Boolean(isDatePickerVisible)}
                      mode="date"
                      onConfirm={(date) => handleConfirm(date, setFieldValue)}
                      onCancel={showDatePicker}
                      minimumDate={new Date()}
                    />
                  </View>
                  <AppButton
                    containerStyle={{ marginTop: padding.p8 }}
                    onPress={() => handleSubmit()}
                    title={t('common:create')}
                  />
                </View>
              );
            }}
          </Formik>
        </ScrollView>
      </AppContainer>
    </>
  );
};

export default CreateKR;
