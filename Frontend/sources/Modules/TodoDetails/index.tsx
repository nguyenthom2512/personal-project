import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import { Formik, FormikProps } from 'formik';
import moment from 'moment';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import { Avatar } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';
import Comment from '../../Assets/Svg/chat.svg';
import Clock from '../../Assets/Svg/clock.svg';
import Calendar from '../../Assets/Svg/date.svg';
import Description from '../../Assets/Svg/description.svg';
// import IconFile from '../../Assets/Svg/file.svg';
import Flag from '../../Assets/Svg/flag.svg';
import Tag from '../../Assets/Svg/tag.svg';
import User from '../../Assets/Svg/user.svg';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {
  AppButton,
  AppCheckboxField,
  AppContainer,
  AppInputField,
  AppText,
  ModalChoosePrioritized,
  ModalClock,
  ModalComment,
  ModalDelete,
  ModalMore,
} from '../../Components';
import { DataItem } from '../../Components/AppDropDown';
import ModalChooseDays from '../../Components/ModalCreate/ModalChooseDays';
import ModalChooseTag from '../../Components/ModalCreate/ModalChooseTag';
import ModalChooseTask from '../../Components/ModalCreate/ModalChooseTask';
import ModalDescription from '../../Components/ModalCreate/ModalDescription';
import ModalDuplycateMission from '../../Components/ModalCreate/ModalDuplycateMission';
import ModalOkrs from '../../Components/ModalCreate/ModalOkrs';
import { IndicatorContext } from '../../Context';
import { dateFormat, RAIC_TYPE } from '../../Helpers/constant';
import { DDMMYYYY, HHmm, YYYYMMDD } from '../../Helpers/dateTime';
import {
  CheckList,
  RaicUser,
  TodoDetail,
} from '../../interfaces/todo.interface';
import { todoListActions } from '../../Redux/Actions';
import {
  getPriorityRequest,
  getTodoDetail,
  getTodoListRequest,
} from '../../Redux/Actions/todoListActions';
import ActionTypes from '../../Redux/ActionTypes';
import { RootState } from '../../Redux/Reducers';
import serviceUrls from '../../Services/serviceUrls';
import todoServices from '../../Services/todoServices';
import { useTheme, useThemeAwareObject } from '../../Theme';
import DuplicateCurrentMission from './Components/DuplicateCurrentMission';
import Sublist from './Components/Sublist';
import styles from './styles';
import Octicons from 'react-native-vector-icons/Octicons';
import { noDateTodoRequest } from '../../Redux/Actions/noDateTodoActions';
import useGetTodoDashboard from '../../Hooks/getTodoDashboard';
import { getTodoDashboardRequest } from '../../Redux/Actions/todoDashboardActions';
import { Input } from 'react-native-elements/dist/input/Input';
import Folder from '../../Assets/Svg/file.svg'
import ListAvatar from '../../Components/ListAvatar';
const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';

let page = 1;
const page_size = 20;

interface Props {}

interface FormItem {
  icon: any;
  title: string;
  results: any;
  key: string;
  child?: any;
}
interface ParamsList {
  id: number;
  isDone: boolean;
}

export default (props: Props) => {
  const route = useRoute<any>();
  const navigation = useNavigation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { id, isDone } = route.params;
  const [detailTodo, setDetailTodo] = useState<TodoDetail>({});

  const [isShowModal, setIsShowModal] = useState('close');
  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;
  const validateSchema = yup.object().shape({
    priority: yup.string().required(t('common:errorPriority')),
    todo_name: yup.string().required(t('common:required')),
    // result: yup.number().required(t('common:required')),
    // result_name: yup.string().required(t('common:required')),
  });
  const dispatch = useDispatch();
  const { setLoading } = useContext(IndicatorContext);
  const tagRef = useRef<any>();
  const okrRef = useRef<any>();
  const dateRef = useRef<any>();
  const clockRef = useRef<any>();
  const priorityRef = useRef<any>();
  const taskRef = useRef<any>();
  const moreRef = useRef<any>();
  const duplicateRef = useRef<any>();
  const commentRef = useRef<any>();
  const descriptionRef = useRef<any>();
  const sublistRef = useRef<any>();

  useEffect(() => {
    dispatch(getTodoDetail(id));
  }, []);

  const todoDetailReducer = useSelector(
    (state: RootState) => state.todoListReducer,
  );

  const checkinDate = () => {
    return t('todoDetail:noDate');
  };

  useEffect(() => {
    if (todoDetailReducer.type === ActionTypes.GET_TODO_DETAIL_SUCCESS) {
      setDetailTodo(todoDetailReducer?.dataDetail);
    }
  }, [todoDetailReducer.type]);

  const toggleModal = (key: string) => {
    setIsShowModal('close');
    getRefByKey(key)?.current?.close();
  };

  const refreshTodo = () => {
    const start_time = moment().format('YYYY-MM-DD');
    const end_time = moment().add(1, 'days').format('YYYY-MM-DD');
    dispatch(getTodoListRequest({ start_time, end_time }));
    dispatch(noDateTodoRequest({ page, page_size }));
    //@ts-ignore
    dispatch(getPriorityRequest({ is_done: isDone }));
    dispatch(getTodoDashboardRequest({}));
  };

  const tagName: DataItem[] = (detailTodo?.tag || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.tag_name,
    };
  });

  const listAvatar = (detailTodo?.raic_user || []).map((el: any) => {
    return {
      value: el?.user,
      label: el?.user_data?.img_url,
    };
  });

  const renderModal = (formik: FormikProps<any>) => (
    <>
      <ModalDescription
        ref={descriptionRef}
        formik={formik}
        onCloseModal={() => toggleModal('description')}
      />
      <ModalChooseTag
        formik={formik}
        ref={tagRef}
        onCloseModal={() => toggleModal('tag')}
        user={user}
        isGetInfo
      />

      <ModalOkrs
        formik={formik}
        ref={okrRef}
        onCloseModal={() => toggleModal('okr')}
      />
      <ModalChooseDays
        formik={formik}
        ref={dateRef}
        onCloseModal={() => toggleModal('days')}
      />
      <ModalChoosePrioritized
        formik={formik}
        onCloseModal={() => toggleModal('priority')}
        isGetFullInfo
        ref={priorityRef}
      />
      <ModalComment
        ref={commentRef}
        onCloseModal={() => toggleModal('comment')}
        formik={formik}
        idTodo={id}
      />
      <ModalClock
        ref={clockRef}
        formik={formik}
        onCloseModal={() => toggleModal('clock')}
        isOptional
      />
      <ModalChooseTask
        formik={formik}
        onCloseModal={() => toggleModal('task')}
        isGetInfo
        ref={taskRef}
      />
      <ModalMore
        formik={formik}
        onCloseModal={() => toggleModal('more')}
        ref={moreRef}
        chooseAction={(key) => {
          if (key === 'deleteMission') {
            setIsShowModal(key);
            moreRef.current.close();
            return;
          }

          getRefByKey(key)?.current.open();
        }}
      />
      <DuplicateCurrentMission
        formik={formik}
        onCloseModal={() => toggleModal('duplicate')}
        ref={duplicateRef}
      />
      <Sublist
        onCloseModal={() => toggleModal('sublist')}
        formik={formik}
        ref={sublistRef}
      />
      {isShowModal === 'deleteMission' && (
        <ModalDelete
          modalVisible={Boolean(isShowModal)}
          formik={formik}
          onCloseModal={() => setIsShowModal('close')}
          onDeleteItem={() => {
            setLoading(true);
            setIsShowModal('close');
            todoServices.deleteTodoDetail(
              id,
              (err) => {
                let mess = err;
                if (err === 'UNKNOWN') {
                  mess = t('common:unknownError');
                }
                Alert.alert(t('common:notice'), mess);
                setLoading(false);
              },
              () => {
                refreshTodo();
                setLoading(false);
                navigation.goBack();
              },
            );
          }}
        />
      )}
    </>
  );

  const renderListAvatar = (values: any) => {
    const { user_responsible, user_accountable, user_inform, user_consult } =
      values;
    const users = [
      ...user_responsible,
      ...user_accountable,
      ...user_inform,
      ...user_consult,
    ];
    return (
      <ListAvatar listAvatar={users} />
      // <View style={Styles.listAvatar}>
      //   {users?.slice(0, 4).map((item: any, index: number) => {
      //     if (users?.length === 0)
      //       return <AppText>{t('todoDetail:noChoose')}</AppText>;
      //     if (users?.length > 3 && index === 3) {
      //       return (
      //         <View
      //           key={index.toString()}
      //           style={[
      //             {
      //               zIndex: index,
      //             },
      //             Styles.restAvatar,
      //           ]}
      //         >
      //           <AppText style={Styles.number}>+{users.length - 3}</AppText>
      //         </View>
      //       );
      //     }
      //     return (
      //       <Avatar
      //         key={index.toString()}
      //         size={26}
      //         rounded
      //         title=""
      //         activeOpacity={0.7}
      //         source={{
      //           uri: item?.avatar
      //             ? `${serviceUrls.url.IMAGE}${item?.avatar}`
      //             : DEFAULT_AVATAR,
      //         }}
      //         containerStyle={{
      //           zIndex: index,
      //           marginLeft: index === 0 ? 0 : -4,
      //           borderColor: theme.color.white,
      //           borderWidth: 2,
      //         }}
      //       />
      //     );
      //   })}
      // </View>
    );
  };

  const getRefByKey = (key: string) => {
    switch (key) {
      case 'tag':
        return tagRef;
      case 'priority':
        return priorityRef;
      case 'more':
        return moreRef;
      case 'comment':
        return commentRef;
      case 'clock':
        return clockRef;
      case 'duplicate':
        return duplicateRef;
      case 'task':
        return taskRef;
      case 'days':
        return dateRef;
      case 'description':
        return descriptionRef;
      case 'okr':
        return okrRef;
      case 'sublist':
        return sublistRef;
      default:
        break;
    }
  };

  const dataItems = (propsFormik: any) => [
    {
      icon: <Description height={24} width={24} />,
      title:
        propsFormik.values?.description || t('todoDetail:thisIsDescription'),
      results: '',
      key: 'description',
    },
    {
      icon: <Folder height={22} width={24} />,
      title: t('todoDetail:okrName'),
      results: propsFormik.values?.resultName,
      key: 'okr',
    },
    {
      icon: <Octicons name="checklist" size={32} color={theme.color.gray} />,
      title: t('todoDetail:checklist'),
      results: (propsFormik.values.check_list || []).length.toString(),
      key: 'sublist',
    },
    {
      icon: <Calendar height={24} width={24} />,
      title: t('todoDetail:day'),
      results: moment(propsFormik.values.date).isValid()
        ? moment(propsFormik.values.date).format(DDMMYYYY)
        : checkinDate(),
      key: 'days',
    },
    {
      icon: <Clock height={24} width={24} />,
      title: t('todoDetail:time'),
      results: propsFormik.values.start_time
        ? `${propsFormik.values.start_time} - ${propsFormik.values.end_time}`
        : t('todoDetail:noTime'),
      key: 'clock',
    },
    {
      icon: <Tag height={24} width={24} />,
      title: t('todoDetail:tag'),
      results: tagToString(propsFormik.values.tag),
      key: 'tag',
    },
    {
      icon: <Flag height={24} width={24} />,
      title: t('todoDetail:priority'),
      results: '',
      key: 'priority',
      child: (
        <View style={Styles.priority}>
          <Ionicons
            name="flag"
            color={propsFormik.values.priority?.color || theme.color.gray}
            size={theme.fontSize.f24}
          />
          <AppText style={Styles.textPriority}>
            {propsFormik?.values?.priority?.priority_name ||
              t('todoDetail:noChoose')}
          </AppText>
        </View>
      ),
    },
    {
      icon: <User height={24} width={24} />,
      title: t('todoDetail:tagMission'),
      results: '',
      key: 'task',
      child: renderListAvatar(propsFormik.values),
    },
    {
      icon: <Comment height={24} width={24} />,
      title: t('todoDetail:comment'),
      results: propsFormik.values.comment_count
        ? propsFormik.values.comment_count
        : detailTodo?.comment_count,
      key: 'comment',
    },
  ];

  const renderItems = (item: FormItem) => {
    const {
      icon = null,
      title = '',
      results = '',
      key = '',
      child = null,
    } = item;
    return (
      <TouchableOpacity
        style={Styles.itemContainer}
        onPress={() => {
          getRefByKey(key)?.current?.open();
        }}
      >
        {icon}
        <AppText numberOfLines={1} style={Styles.titleStyles}>
          {title}
        </AppText>
        <View style={Styles.resultStyles}>
          {child ? child : <AppText numberOfLines={1}>{results}</AppText>}
          <MaterialIcons
            name="keyboard-arrow-right"
            size={28}
            color={theme.color.black}
          />
        </View>
      </TouchableOpacity>
    );
  };

  const tagToString = (tag: DataItem[]) => {
    if (tag.length === 0) return t('todoDetail:noChoose');
    const labels = tag.map((el) => el.label);
    return labels.join(', ');
  };

  const getFilter = (type: number) => {
    const raicByType = detailTodo?.raic_user
      ?.filter((value: any) => value.raic_type === type)
      .map((raic: any) => ({
        value: raic?.user,
        label: raic?.user_data.full_name,
        avatar: raic.user_data?.img_url,
      }));
    return raicByType;
  };

  const initialValues = {
    raic_user: listAvatar || '',
    start_time: moment(detailTodo?.start_time).isValid()
      ? moment(detailTodo?.start_time).format(HHmm)
      : '',
    end_time: moment(detailTodo?.end_time).isValid()
      ? moment(detailTodo?.end_time).format(HHmm)
      : '',
    description: detailTodo?.description,
    confident: 0,
    is_done: detailTodo?.is_done || false,
    result: detailTodo?.result,
    okrId: detailTodo?.okr_id,
    user,
    resultName: detailTodo?.result_name || '', // remove when submit
    priority: detailTodo?.priority || { id: '', color: '', priority_name: '' },
    tag: tagName || [],
    todo_name: detailTodo?.todo_name || '',
    check_list: detailTodo.check_list,
    date: detailTodo?.date
      ? detailTodo?.date
      : detailTodo?.start_time
      ? moment(detailTodo?.start_time).format(YYYYMMDD)
      : '',
    priority_color: {
      color: '',
      priority_name: '',
    },
    user_responsible: getFilter(RAIC_TYPE.Responsible) || [], // remove when submit
    user_accountable: getFilter(RAIC_TYPE.Accountable) || [], // remove when submit
    user_inform: getFilter(RAIC_TYPE.Inform) || [], // remove when submit
    user_consult: getFilter(RAIC_TYPE.Consult) || [], // remove when submit
    img_url: '',
    comment_count: detailTodo?.comment_count || 0,
    todo_comment: detailTodo?.todo_comment || [],

    // duplicate todo
    duplicate: false,
    todo_name_duplicate: detailTodo?.todo_name || '',
    date_duplicate: '',
    start_time_duplicate: '',
    end_time_duplicate: '',
  };
  const convertValueToId = (array: any[], key: string) => {
    const data = array.map((el) => el[key]);
    return data;
  };
  const onSubmit = (values: any) => {
    const {
      user_responsible,
      user_accountable,
      user_inform,
      user_consult,
      start_time_duplicate,
      end_time_duplicate,
      todo_name_duplicate,
      date_duplicate,
      ...remainValue
    } = values;
    if (remainValue.start_time.includes('Invalid date')) {
      remainValue.start_time = '';
    }
    if (remainValue.end_time.includes('Invalid date')) {
      remainValue.end_time = '';
    }
    if (remainValue.date?.includes('Invalid date')) {
      remainValue.date = '';
    }
    const params = {
      ...remainValue,
      priority: values.priority.id || '',
      tag: convertValueToId(values.tag, 'value'),
      user_responsible: convertValueToId(user_responsible, 'value'), // remove when submit
      user_accountable: convertValueToId(user_accountable, 'value'), // remove when submit
      user_inform: convertValueToId(user_inform, 'value'), // remove when submit
      user_consult: convertValueToId(user_consult, 'value'),
      check_list: (values.check_list || []).map((el: any) => ({
        content: el.content,
        is_done: el.is_done,
      })),
    };
    setLoading(true);
    if (values.duplicate) {
      params.start_time = values.start_time_duplicate;
      params.end_time = values.end_time_duplicate;
      params.todo_name = values.todo_name_duplicate;
      params.date = values.date_duplicate
        ? moment(values.date_duplicate).format(YYYYMMDD)
        : '';
      params.check_list = (params.check_list || []).map(
        (el: any) => el.content,
      );

      todoServices.createTodo(
        params,
        (err) => {
          setLoading(false);
          Alert.alert(
            t('common:notice'),
            err
              ? err || t('todoDetail:createMissionFailed')
              : t('todoDetail:haveTodoThisTime'),
          );
        },
        () => {
          setLoading(false);
          //   dispatch(getTodoDetail(id));
          refreshTodo();
          Alert.alert(
            t('common:notice'),
            t('todoDetail:createMissionSuccess'),
            [
              {
                text: 'OK',
                onPress: () => navigation.navigate('HomePage'),
              },
            ],
          );
        },
      );
    } else {
      todoServices.updateTodo(
        detailTodo?.id,
        {
          ...params,
          date: values.date,
        },
        (err) => {
          setLoading(false);
          Alert.alert(
            t('common:notice'),
            err
              ? err || t('todoDetail:createMissionFailed')
              : t('todoDetail:haveTodoThisTime'),
          );
        },
        () => {
          setLoading(false);
          Alert.alert(t('common:notice'), t('todoDetail:updateSuccess'), [
            {
              text: 'OK',
              onPress: () => {
                dispatch(getTodoDetail(id));
                refreshTodo();
                navigation.goBack();
              },
            },
          ]);
        },
      );
    }
  };

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: theme.color.blackHeader }}>
      {!!detailTodo.id && (
        <Formik
          initialValues={initialValues}
          validateOnBlur
          validationSchema={validateSchema}
          onSubmit={onSubmit}
        >
          {(propsFormik) => {
            console.log('detailTodo', propsFormik.values, propsFormik.errors);

            return (
              <AppContainer
                isBack
                iconRight="more-horizontal"
                title={t('common:todoDetail')}
                onRightPress={() => moreRef?.current?.open()}
              >
                <ScrollView style={Styles.container}>
                  <View
                    style={[
                      Styles.todoNameStyles,
                    ]}
                  >
                    <Input
                      containerStyle={Styles.inputContainerStyles}
                      value={propsFormik.values.todo_name}
                      onChangeText={propsFormik.handleChange('todo_name')}
                      style={Styles.customInputStyles}
                      errorStyle={
                        propsFormik.errors.todo_name
                          ? { color: theme.color.redAlert }
                          : { height: 0 }
                      }
                      errorMessage={propsFormik.errors.todo_name}
                      inputContainerStyle={{ borderColor: theme.color.mercury }}
                    />
                  </View>
                  {dataItems(propsFormik).map((el: any) => renderItems(el))}
                  {!!propsFormik.values.start_time &&
                    !!propsFormik.values.date && (
                      <>
                        <AppCheckboxField
                          onPress={(checked) => {
                            propsFormik.setFieldValue('is_done', checked);
                          }}
                          name={'is_done'}
                          checked={propsFormik.values.is_done}
                          title={t('checkIn:complete')}
                        />
                      </>
                    )}
                  <AppButton
                    containerStyle={Styles.buttonStyles}
                    title={t('createMissions:save')}
                    onPress={() => {
                      if (propsFormik?.errors?.priority) {
                        Alert.alert(
                          t('common:notice'),
                          //@ts-ignore
                          propsFormik?.errors?.priority || '',
                        );
                      } else {
                        propsFormik.setFieldValue('duplicate', false);
                        propsFormik.handleSubmit();
                      }
                    }}
                  />
                </ScrollView>
                {renderModal(propsFormik)}
              </AppContainer>
            );
          }}
        </Formik>
      )}
    </SafeAreaView>
  );
};
