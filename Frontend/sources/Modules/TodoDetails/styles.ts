import { StyleSheet } from 'react-native';
import { isIOS, ScreenHeight } from 'react-native-elements/dist/helpers';
import { device } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: theme.color.white,
      height: '100%',
    },
    todoName: {
      fontSize: theme.fontSize.f18,
      fontWeight: '600',
      marginLeft: theme.spacing.p16,
      // marginTop: theme.spacing.p24,
      // marginBottom: theme.spacing.p32,
    },
    itemContainer: {
      flexDirection: 'row',
      backgroundColor: theme.color.lightGray,
      padding: theme.spacing.p16,
      alignItems: 'center',
      marginBottom: theme.spacing.p12,
    },
    listAvatar: {
      marginRight: theme.spacing.p12,
      flexDirection: 'row',
      alignItems: 'center',
    },
    restAvatar: {
      borderRadius: 20,
      padding: 3.5,
      backgroundColor: theme.color.violet,
      marginLeft: -4,
      borderColor: theme.color.white,
      borderWidth: 2,
    },
    number: {
      color: theme.color.white,
      fontSize: theme.fontSize.f12,
    },
    titleStyles: {
      flex: 1,
      marginLeft: theme.spacing.p12,
    },
    resultStyles: {
      flex: 2,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    priority: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    textPriority: {
      marginLeft: theme.spacing.p4,
    },
    buttonStyles: {
      marginHorizontal: theme.spacing.p16,
      marginVertical: theme.spacing.p16,
    },
    todoNameStyles: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: theme.spacing.p12
    },
    inputContainerStyles: {
      flex: 1,
      paddingHorizontal: theme.spacing.p16,
    },
    customInputStyles: {
      borderWidth: 0,
      fontSize: theme.fontSize.f18,
      fontWeight: '600',
    },
  });
  return styles;
};
