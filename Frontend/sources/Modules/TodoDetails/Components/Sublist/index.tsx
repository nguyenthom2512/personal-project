import { FormikProps } from 'formik';
import React, { forwardRef, useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ScrollView, TouchableOpacity, View } from 'react-native';
import { Input } from 'react-native-elements/dist/input/Input';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {
  AppButton,
  AppCheckboxField,
  AppInputField,
  AppText,
} from '../../../../Components';
import ModalizeApp from '../../../../Components/ModalizeApp';
import { useTheme, useThemeAwareObject } from '../../../../Theme';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import styles from './styles';
import AppInputFieldIcon from '../../../../Components/AppInputFieldIcon';
import { cloneDeep } from 'lodash';

interface Props {
  onCloseModal: () => void;
  formik: FormikProps<any>;
  isGetInfo?: boolean;
}

interface ChecklistProps {
  content: string;
  is_done: boolean;
}

let isSave = false;

export default forwardRef((props: Props, ref: any) => {
  const { onCloseModal = () => {}, formik } = props;
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const [checklist, setChecklist] = useState<ChecklistProps[]>(
    cloneDeep(formik.values.check_list),
  );

  const addSubmission = useCallback(() => {
    setChecklist((prev) => {
      const prevArr = cloneDeep(prev);
      prevArr.push({ content: '', is_done: false });
      return prevArr;
    });
  }, [checklist]);

  const handleClose = () => {
    if (!isSave) {
      setChecklist(cloneDeep(formik.values.check_list));
    }
    isSave = false;
  };

  const renderSubMissions = () => {
    if (checklist.length == 0) {
      return (
        <View
          style={{
            alignItems: 'center',
            width: '100%',
          }}
        >
          <AppText style={Styles.TextStyles}>
            {t('todoDetail:noChecklist')}
          </AppText>
        </View>
      );
    }

    return (
      <View style={{ flex: 1 }}>
        {checklist?.map((el: ChecklistProps, index: number) => {
          return (
            <View style={Styles.inputStyles}>
              <AppInputFieldIcon
                containerStyle={{ flex: 1 }}
                name={'checklist_11'}
                onChangeCustom={(e: string) => {
                  const newChecklist = cloneDeep(checklist);
                  newChecklist[index].content = e;
                  setChecklist(newChecklist);
                }}
                customValue={el.content}
              />
              <AppCheckboxField
                name={'is_done_11'}
                onPress={(checked) => {
                  const newChecklist = cloneDeep(checklist);

                  newChecklist[index].is_done = checked;
                  setChecklist(newChecklist);
                }}
                checked={el.is_done}
              />
              <TouchableOpacity onPress={() => removeSubmission(index)}>
                <Ionicons
                  name="close"
                  size={20}
                  color={theme.color.gray}
                  style={{ marginLeft: theme.spacing.p12 }}
                />
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  };

  const removeSubmission = useCallback(
    (index) => {
      setChecklist(
        checklist.filter((elm: any, ind: number) => {
          return ind !== index;
        }),
      );
    },
    [checklist],
  );

  return (
    <ModalizeApp
      ref={ref}
      onRequestClose={onCloseModal}
      onClosed={handleClose}
      headerText={t('todoDetail:checklist')}
      iconRight={
        <AppText style={{ color: theme.color.mineShaft }}>
          {t('common:save')}
        </AppText>
        // <MaterialIcons name="done" size={24} color={theme.color.mineShaft} />
      }
      onRightPress={() => {
        isSave = true;
        const newChecklist = checklist.filter((el) => !!el.content);
        setChecklist(newChecklist);
        formik.setFieldValue('check_list', newChecklist);
        onCloseModal();
      }}
      onLeftPress={onCloseModal}
      iconLeft={
        <Ionicons
          name="close-sharp"
          size={24}
          color={theme.color.gray}
          //   onPress={() => {
          //     onCloseModal();
          //   }}
        />
      }
    >
      <ScrollView style={Styles.inputContainer}>
        <View style={{ flex: 1 }}>
          <View style={[Styles.addSubmissionWrap, { alignItems: 'center' }]}>
            {renderSubMissions()}
          </View>
          <TouchableOpacity style={Styles.buttonStyles} onPress={addSubmission}>
            <AntDesign name="plus" size={18} color={theme.color.violet} />
            <AppText style={Styles.textAdd}>{t('todoDetail:addNew')}</AppText>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </ModalizeApp>
  );
});
