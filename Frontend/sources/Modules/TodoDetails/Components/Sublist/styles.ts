import { StyleSheet } from 'react-native';
import { device } from '../../../../Helpers';
import { Theme } from '../../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    inputContainer: {
      height: device.height / 3,
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
    },
    container: {
      flexDirection: 'row',
      borderWidth: 1,
      flex: 1,
    },
    addSubmissionWrap: {
      flexDirection: 'row',
      paddingRight: theme.spacing.p16,
    },
    iconAdd: {
      marginRight: theme.spacing.p8,
      marginTop: theme.spacing.p16,
    },
    buttonStyles: {
      marginBottom:theme.spacing.p16,
      marginVertical:theme.spacing.p8,
      flexDirection:'row',
      alignItems:'center'
    },
    TextStyles: {
      fontSize: theme.fontSize.f16,
      textAlign: 'center',
    },
    inputStyles:{
      flexDirection:'row',
      width:'100%',
      alignItems:'center',
      marginRight: theme.spacing.p24
    },
    textAdd:{
      color:theme.color.violet
    }
  });
  return styles;
};
