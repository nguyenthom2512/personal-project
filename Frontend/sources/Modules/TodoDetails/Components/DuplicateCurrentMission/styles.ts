import { StyleSheet } from 'react-native';
import { measure } from '../../../../Helpers';
import { Theme } from '../../../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      backgroundColor: theme.color.white,
      height: '100%',
    },
    textStyle: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      marginTop: theme.spacing.p16,
    },
    containerTime: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    wrapContent: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      height: measure.selectHeight,
      //   paddingVertical: theme.spacing.p10,
      paddingHorizontal: theme.spacing.p12,
      borderWidth: 1,
      borderColor: theme.color.mercury,
      borderRadius: 4,
      marginRight: theme.spacing.p12,
      marginVertical: theme.spacing.p8,
    },
    txtMomentDate: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
    containerInput: {
      paddingHorizontal: theme.spacing.p16,
    },
  });
