import moment from 'moment';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { TouchableOpacity, View } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Clock from '../../../../Assets/Svg/clockViolet.svg';
import { AppText } from '../../../../Components';
import isDarkMode from '../../../../Helpers/darkmode';
import { DDMMYYYY } from '../../../../Helpers/dateTime';
import { useTheme, useThemeAwareObject } from '../../../../Theme';
import { IDuplicateProps } from './index';
import styles from './styles';

const DateInput = ({ formik }: IDuplicateProps) => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);

  const [show, setShow] = useState(false);

  const hideDatePicker = () => {
    setShow(false);
  };

  const handleConfirm = (date: Date) => {
    formik.setFieldValue('date_duplicate', date);
    // setDateDuplicate(date);
    setShow(false);
  };

  const showDatepicker = () => {
    setShow(true);
  };

  const deleteDate = () => {
    formik.setFieldValue('date_duplicate', '');
  };

  return (
    <View>
      <View>
        <AppText style={Styles.textStyle}>
          {t('createMissions:dateStart')}
        </AppText>
        <View style={Styles.containerTime}>
          <TouchableOpacity
            style={Styles.wrapContent}
            onPress={() => showDatepicker()}
          >
            <AppText style={Styles.txtMomentDate}>
              {formik?.values?.date_duplicate
                ? moment(formik?.values?.date_duplicate).format(DDMMYYYY)
                : DDMMYYYY}
            </AppText>
            <Clock />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => deleteDate()}>
            <Ionicons name="close" size={24} />
          </TouchableOpacity>
        </View>
      </View>
      {show && (
        <DateTimePickerModal
          locale="vi"
          is24Hour
          themeVariant="light"
          isDarkModeEnabled={isDarkMode()}
          isVisible={show}
          mode="date"
          onConfirm={(time) => handleConfirm(time)}
          onCancel={hideDatePicker}
          minimumDate={new Date()}
        />
      )}
    </View>
  );
};

export default DateInput;
