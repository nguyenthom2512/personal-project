import { FormikProps } from 'formik/dist/types';
import _ from 'lodash';
import moment from 'moment';
import React, { forwardRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, TouchableOpacity, View } from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { SafeAreaView } from 'react-native-safe-area-context';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Clock from '../../../../Assets/Svg/clockViolet.svg';
import { AppButton, AppInputField, AppText } from '../../../../Components';
import ModalizeApp from '../../../../Components/ModalizeApp';
import isDarkMode from '../../../../Helpers/darkmode';
import { useTheme, useThemeAwareObject } from '../../../../Theme';
import DateInput from './DateInput';
import styles from './styles';

export interface IDuplicateProps {
  onCloseModal: () => void;
  formik: FormikProps<any>;
}

let isStart = false;
const formatTimeDuplicate = 'HH:mm';

const DuplicateCurrentMission = forwardRef(
  (props: IDuplicateProps, ref: any) => {
    const { onCloseModal = () => {}, formik } = props;
    const { t } = useTranslation();
    const { theme } = useTheme();
    const Styles = useThemeAwareObject(styles);

    const [show, setShow] = useState(false);

    const showDatepicker = (start: boolean) => {
      setShow(true);
      isStart = start;
    };

    const hideDatePicker = () => {
      setShow(false);
    };

    const deleteTime = (isStartTime: boolean) => {
      if (isStartTime) {
        formik.setFieldValue('start_time_duplicate', '');
      } else {
        formik.setFieldValue('end_time_duplicate', '');
      }
    };

    const renderTime = (title: string, moment: any, isStartTime: boolean) => (
      <View>
        <AppText style={Styles.textStyle}>{title}</AppText>
        <View style={Styles.containerTime}>
          <TouchableOpacity
            style={Styles.wrapContent}
            onPress={() => {
              if (formik.values.date_duplicate) {
                showDatepicker(isStartTime);
              } else {
                Alert.alert(
                  t('common:notice'),
                  t('createMissions:selectDateBeforeSelectTime'),
                );
              }
            }}
          >
            <AppText style={Styles.txtMomentDate}>{moment}</AppText>
            <Clock />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => deleteTime(isStartTime)}>
            <Ionicons name="close" size={24} />
          </TouchableOpacity>
        </View>
      </View>
    );

    const handleConfirm = (time: Date) => {
      let selectedDate = '';
      const hour = moment(time).get('hour');
      const minute = moment(time).get('minute');
      const second = moment(time).get('second');
      const newTime = moment(time).format(formatTimeDuplicate);
      const compareTime = (start: string, end: string, checkStart: boolean) => {
        if (
          moment(start, formatTimeDuplicate).isBefore(
            moment(end, formatTimeDuplicate),
          )
        ) {
          formik.setFieldValue(
            checkStart ? 'start_time_duplicate' : 'end_time_duplicate',
            newTime,
          );
        } else {
          Alert.alert('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc');
          formik.setFieldValue(
            checkStart ? 'start_time_duplicate' : 'end_time_duplicate',
            '',
          );
        }
      };

      const isEmptyStartTime = _.isEmpty(formik?.values?.start_time_duplicate);
      const isEmptyEndTime = _.isEmpty(formik?.values?.end_time_duplicate);

      if (isStart) {
        {
          if (isEmptyEndTime) {
            formik.setFieldValue('start_time_duplicate', newTime);
          } else {
            compareTime(newTime, formik?.values?.end_time_duplicate, true);
          }
        }
      } else {
        if (isEmptyStartTime) {
          formik.setFieldValue('end_time_duplicate', newTime);
        } else {
          compareTime(formik?.values?.start_time_duplicate, newTime, false);
        }
      }
      setShow(false);
    };
    return (
      <ModalizeApp
        ref={ref}
        onRequestClose={onCloseModal}
        iconLeft={
          <Ionicons
            name="close-sharp"
            size={24}
            color={theme.color.gray}
            onPress={() => onCloseModal()}
          />
        }
        headerText={t('createMissions:duplicateMission')}
      >
        <SafeAreaView>
          <View style={Styles.containerInput}>
            <AppInputField
              title={t('createMissions:todoName')}
              name="todo_name_duplicate"
            />
            <DateInput {...props} />
            {renderTime(
              t('createMissions:timeStart'),
              !_.isEmpty(formik?.values?.start_time_duplicate)
                ? formik?.values?.start_time_duplicate
                : formatTimeDuplicate,
              true,
            )}
            <View style={{ marginBottom: theme.spacing.p16 }}>
              {renderTime(
                t('createMissions:timeEnd'),
                !_.isEmpty(formik?.values?.end_time_duplicate)
                  ? formik?.values?.end_time_duplicate
                  : formatTimeDuplicate,
                false,
              )}
            </View>
            <AppButton
              title={t('createMissions:duplicateMission')}
              onPress={() => {
                formik.setFieldValue('duplicate', true);
                formik.handleSubmit();
              }}
            />
          </View>
          {show && (
            <DateTimePickerModal
              locale="vi"
              is24Hour
              themeVariant="light"
              isDarkModeEnabled={isDarkMode()}
              isVisible={show}
              mode="time"
              onConfirm={(time) => handleConfirm(time)}
              onCancel={hideDatePicker}
              // minimumDate={isStart ? (formik.values.date || new Date()) : (startTime ? new Date(startTime) : (formik.values.date || new Date()))}
            />
          )}
        </SafeAreaView>
      </ModalizeApp>
    );
  },
);

export default DuplicateCurrentMission;
