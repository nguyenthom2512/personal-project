import React, { useEffect } from 'react';
import { Alert, FlatList, View } from 'react-native';
import {
  AppButton,
  AppContainer,
  AppFlatlistEmpty,
  AppText,
} from '../../Components';
import { useThemeAwareObject } from '../../Theme';
// import IconCheck from '../../Assets/Svg/d-check.svg';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import useGetPackage from '../../Hooks/getPackage';
import isEmpty from 'lodash/isEmpty';
import { useNavigation } from '@react-navigation/core';

const License = () => {
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const navigation = useNavigation();

  const [data, loading, error, refetch] = useGetPackage();

  useEffect(() => {
    !isEmpty(error) &&
      Alert.alert(t('common:notice'), t('license:getListFailed'));
  }, [error]);

  const promotion = (label: string, desc: string) => {
    return (
      <View style={Styles.promotionContainer}>
        {/* <IconCheck /> */}
        <AppText style={Styles.promotionText}>
          {label}
          <AppText style={Styles.normalText}>{desc}</AppText>
        </AppText>
      </View>
    );
  };

  return (
    <AppContainer isMenu title={t('license:title')}>
      <FlatList
        onRefresh={refetch}
        refreshing={loading}
        data={data || []}
        contentContainerStyle={Styles.contentContainerStyle}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item, index) => `${item.id}`}
        ListEmptyComponent={() => <AppFlatlistEmpty screenEmpty={loading} />}
        renderItem={({ item }) => {
          return (
            <View style={Styles.container}>
              <View style={Styles.headerItemContainer}>
                <View style={Styles.headerItemWrap}>
                  <AppText style={Styles.headerText}>
                    {item.package_name}
                  </AppText>
                  {item.is_recommend && (
                    <View style={Styles.adviseWrap}>
                      <AppText style={Styles.adviseText}>
                        {t('license:suggestion')}
                      </AppText>
                    </View>
                  )}
                </View>

                <AppText style={Styles.priceText}>
                  {(item.price || 0).toLocaleString('vi-VN', {
                    style: 'currency',
                    currency: 'VND',
                  })}
                  <AppText style={Styles.durationText}>
                    {t('license:month', { month: item.duration })}
                  </AppText>
                </AppText>
              </View>
              <View style={Styles.bodyWrap}>
                {/* {promotion(
                  item.detail.accountActive,
                  t('license:onlineAccount'),
                )} */}
                {/* {promotion(item.detail.channel, t('license:chatChannel'))}
                {promotion(item.detail.storage, t('license:cloudStorage'))} */}
                {promotion('', item?.description ?? '')}
                <AppButton
                  title={t('common:choice')}
                  onPress={() =>
                    navigation.navigate('Payment', { packageDetail: item })
                  }
                  containerStyle={Styles.btnWrap}
                />
              </View>
            </View>
          );
        }}
      />
    </AppContainer>
  );
};

export default License;
