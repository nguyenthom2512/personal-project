import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    flex1: { flex: 1 },
    flex3: { flex: 3 },
    container: {
      // flexDirection: 'row',
      backgroundColor: theme.color.white,
      //   marginHorizontal: theme.spacing.p16,
      borderRadius: theme.spacing.p8,
      //   padding: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
      marginVertical: theme.spacing.p8,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      // paddingTop: 50
    },
    contentContainerStyle: {
      paddingHorizontal: theme.spacing.p16,
    },
    headerItemContainer: {
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
      paddingBottom: theme.spacing.p16,
      paddingLeft: theme.spacing.p16,
    },
    headerItemWrap: {
      justifyContent: 'space-between',
      flexDirection: 'row',
      marginBottom: theme.spacing.p8,
    },
    headerText: {
      fontWeight: '500',
      fontSize: theme.fontSize.f24,
      lineHeight: theme.fontSize.f28,
      /* identical to box height */
      color: theme.color.shark,
    },
    adviseWrap: {
      backgroundColor: theme.color.lightOrange,
      justifyContent: 'center',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p8,
      paddingVertical: theme.spacing.p4,
    },
    adviseText: {
      fontStyle: 'normal',
      fontWeight: '600',
      fontSize: theme.fontSize.f12,
      lineHeight: theme.fontSize.f18,
      textTransform: 'uppercase',
      color: theme.color.white,
    },
    priceText: {
      fontWeight: 'bold',
      fontSize: theme.fontSize.f24,
      lineHeight: theme.fontSize.f28,
      color: theme.color.shark,
    },
    durationText: {
      fontWeight: 'normal',
      fontSize: theme.fontSize.f16,
      color: `${theme.color.shark}70`,
    },
    promotionContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: theme.spacing.p16
    },
    bodyWrap: {
      padding: theme.spacing.p16,
      paddingBottom: 0
    },
    promotionText: {
      fontWeight: 'bold',
      fontSize: theme.fontSize.f16,
      lineHeight: theme.fontSize.f20,
      color: theme.color.shark,
    //   marginLeft: theme.spacing.p8,
    },
    normalText: {
      fontWeight: '400',
    },
    btnWrap: {
        marginTop: theme.spacing.p16,
    }
  });
