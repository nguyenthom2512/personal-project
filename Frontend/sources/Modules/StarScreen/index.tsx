import { useNavigation } from '@react-navigation/core';
import moment from 'moment';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, FlatList, Text, TouchableOpacity, View } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { useSelector } from 'react-redux';
import IconArrowLeft from '../../Assets/Svg/arrow-left.svg';
import IconArrowRight from '../../Assets/Svg/arrow-right.svg';
import IconStarBig from '../../Assets/Svg/star-big.svg';
import Star from '../../Assets/Svg/star.svg';
import {
  AppButton,
  AppContainer,
  AppFlatlistFooter,
  AppText,
} from '../../Components';
import { dateFormat } from '../../Helpers/constant';
import useGetFeedback from '../../Hooks/getFeedback';
import { FeedbackComment } from '../../interfaces/feedback.interface';
import { RootState } from '../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import GiveStar from '../GiveStar';
import styles from './styles';

interface Props {}

export default () => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const navigation = useNavigation();

  // reducer data
  const userReducer = useSelector(
    (state: RootState) => state.userReducer?.data,
  );

  // hook get data
  const [dataFeedback, loading, error, refetch, nextPage, loadMore] =
    useGetFeedback();

  useEffect(() => {
    if (error) Alert.alert(t('common:notice', 'mmmmm'));
  }, [error]);

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const RefModal = useRef<Modalize>(null);
  const openModal = () => {
    RefModal.current?.open();
  };
  const closeModal = () => {
    RefModal.current?.close();
  };

  const renderItem = ({
    item,
    index,
  }: {
    item: FeedbackComment;
    index: number;
  }) => {
    const isReceived = userReducer?.user?.id === item.user_received;
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('TransactionDetailScreen', item)}
      >
        <View style={Styles.itemContainer}>
          <View
            style={isReceived ? Styles.avatarWrapReceiver : Styles.avatarWrap}
          >
            {isReceived ? <IconArrowLeft /> : <IconArrowRight />}
          </View>
          <View style={{ flex: 1 }}>
            <Text style={Styles.titleText}>
              {isReceived
                ? t('starScreen:receiverStarfrom', {
                    user: item?.user_data?.full_name,
                  })
                : t('starScreen:sendStarfor', {
                    user: item?.user_received_data?.full_name,
                  })}
            </Text>
            <Text style={Styles.dateText}>
              {moment(item.created_time).format(dateFormat.DMYHm)}
            </Text>
          </View>
          <View>
            <View style={Styles.starPlusWrap}>
              <Text>{item?.quantity}</Text>
              <Star fill={theme.color.yellow} />
            </View>
            <Text style={Styles.successText}>{t('starScreen:success')}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <AppContainer isBack title={t('starScreen:title')}>
      <View style={Styles.starScrContainer}>
        <View style={Styles.container}>
          <View style={Styles.flexHCenter}>
            <AppText style={Styles.currentStarText}>
              {userReducer?.user?.rate_month || 0}
            </AppText>
            <IconStarBig
              fill={theme.color.yellow}
              height={40}
              style={{ height: 40, width: 40 }}
            />
          </View>
          <AppButton
            containerStyle={{ width: '40%' }}
            onPress={toggleModal}
            title="Tặng sao"
          />
          <GiveStar
            modalVisible={isModalVisible}
            onCloseModal={() => setModalVisible(!isModalVisible)}
            star={userReducer?.user?.total_rate || 0}
          />
          {/* <GiveStar
            ref={RefDay}
            formik={formik}
            onCloseModal={() => onClose()}
            star={userReducer?.user?.total_rate || 0}
          /> */}
        </View>
        <View style={{ flex: 1, paddingTop: theme.spacing.p8 }}>
          <FlatList
            showsVerticalScrollIndicator={false}
            refreshing={loading}
            onRefresh={refetch}
            data={dataFeedback}
            renderItem={renderItem}
            keyExtractor={(item: any, index: number) => index.toString()}
            onEndReached={() => nextPage()}
            ListFooterComponent={<AppFlatlistFooter isVisible={loadMore} />}
          />
        </View>
      </View>
    </AppContainer>
  );
};
