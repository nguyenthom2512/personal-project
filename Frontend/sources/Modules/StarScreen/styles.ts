import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      backgroundColor: theme.color.white,
      padding: theme.spacing.p16,
      marginTop: theme.spacing.p16,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      borderRadius: theme.spacing.p4,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    starScrContainer: {
      paddingHorizontal: theme.spacing.p16,
      flex: 1,
    },
    flexHCenter: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    itemContainer: {
      flexDirection: 'row',
      padding: theme.spacing.p16,
      backgroundColor: theme.color.white,
      marginTop: theme.spacing.p12,
      justifyContent: 'space-between',
      borderRadius: theme.spacing.p4,
      borderColor: theme.color.mercury,
      borderWidth: 1,
      alignItems: 'center',
    },
    avatarWrap: {
      width: 32,
      height: 32,
      borderRadius: theme.spacing.p16,
      backgroundColor: theme.color.jadeGreen,
      marginRight: theme.spacing.p12,
      justifyContent: 'center',
      alignItems: 'center',
    },
    avatarWrapReceiver: {
      width: 32,
      height: 32,
      borderRadius: theme.spacing.p16,
      backgroundColor: theme.color.lightOrange,
      marginRight: theme.spacing.p12,
      justifyContent: 'center',
      alignItems: 'center',
    },
    dateText: {
      fontSize: theme.fontSize.f12,
      fontWeight: '400',
      color: theme.color.shark,
      opacity: 0.5,
    },
    titleText: {
      fontSize: theme.fontSize.f14,
      color: theme.color.charade,
      paddingBottom: theme.spacing.p8,
    },
    starPlusWrap: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'flex-end',
    },
    currentStarText: {
      paddingRight: theme.spacing.p8,
      fontSize: theme.fontSize.f36,
    },
    successText: {
      textAlign: 'right',
      color: theme.color.greenBox,
    },
  });
