import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert, ScrollView, TouchableOpacity, View
} from 'react-native';
import { Icon } from 'react-native-elements';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import {
  AppButton,
  AppDropDown,
  AppInputField,
  AppText,
  ErrorMessages
} from '../../Components';
import ModalApp from '../../Components/ModalApp';
import { IndicatorContext } from '../../Context';
import { fontSize } from '../../Helpers';
import useGetUnit from '../../Hooks/getUnit';
import { okrActions } from '../../Redux/Actions';
import { RootState } from '../../Redux/Reducers';
import { TKRError } from '../../Redux/Sagas/okrSaga';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface PropsModalEdit {
  modalEdit?: any;
  setCloseModalEdit?: () => void;
  refetch?: () => void;
  okr: any;
  disableUnit?: boolean;
}

export default (props: PropsModalEdit) => {
  const { setCloseModalEdit = () => null, disableUnit = false } = props;
  const indicatorContext = useContext(IndicatorContext);
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const okrReducer = useSelector((state: RootState) => state.okrReducer);

  const [dataUnit] = useGetUnit();
  const okrUnit = (dataUnit || []).map((elm) => {
    return {
      value: elm?.id,
      label: elm?.unit_name,
    };
  });
  const regMatch =
    /^((http|https):\/\/)?(www.)?(?!.*(http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+(\/)?.([\w\?[a-zA-Z-_%\/@?]+)*([^\/\w\?[a-zA-Z0-9_-]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/;

  const validateEdit = Yup.object().shape({
    key_result: Yup.string().required(t('common:required')),
    target: Yup.string().required(t('common:required')),
    unit: Yup.string().required(t('common:required')),
    current_done: Yup.string().required(t('common:required')),
    okr: Yup.string().required(t('common:required')),
    plan_url: Yup.string().matches(regMatch, t('common:requiredLink')),
    result_url: Yup.string().matches(regMatch, t('common:requiredLink')),
  });

  const onSubmit = (data: any) => {
    indicatorContext.setLoading(true);
    dispatch(
      okrActions.upDateResultRequest(data, {
        onSuccess: (success: any) => {
          indicatorContext.setLoading(false);
          Alert.alert(t('common:notice'), `${t('common:updateSuccess')}`, [
            {
              text: `${t('common:ok')}`,
              onPress: () => {
                setCloseModalEdit();
                props?.refetch && props.refetch();
              },
            },
          ]);
        },
        onFailed: (error: any) => {
          indicatorContext.setLoading(false);
          let text = error;
          if (error === TKRError.user) {
            text = t('OKR:cannotUpdateOfother');
          }
          Alert.alert(t('common:notice'), `${text}`);
        },
      }),
    );
  };
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const showDatePicker = () => {
    setDatePickerVisibility(!isDatePickerVisible);
  };
  const handleConfirm = (date: any, setFieldValue: any) => {
    const selected = moment(date).toISOString();
    setFieldValue('deadline', selected);
    showDatePicker();
  };
  return (
    <ModalApp
      visible={!isEmpty(props.modalEdit)}
      iconLeft={
        <Icon
          name="x"
          type="feather"
          onPress={setCloseModalEdit}
          tvParallaxProperties
        />
      }
      headerText={t('common:editKr')}
    >
      <View>
        <ScrollView>
          <Formik
            initialValues={{
              ...props.modalEdit,
              key_result: props.modalEdit?.key_result?.toString(),
              target: props.modalEdit?.target?.toString(),
              unit: props.modalEdit?.unit?.toString(),
              unit_name: props.modalEdit?.unit_name?.toString(),
              current_done: props.modalEdit?.current_done?.toString(),
              plan_url: props.modalEdit?.plan_url?.toString(),
              result_url: props.modalEdit?.result_url?.toString(),
              deadline: props.modalEdit?.deadline?.toString(),
              okr: props.okr,
            }}
            validationSchema={validateEdit}
            onSubmit={onSubmit}
          >
            {({
              handleChange,
              values,
              handleSubmit,
              setFieldValue,
              setValues,
              errors,
            }) => {
              return (
                <View style={Styles.container}>
                  <AppText
                    style={{
                      fontSize: fontSize.f16,
                      fontWeight: '500',
                      color: theme.color.shark,
                      marginBottom: theme.spacing.p8,
                    }}
                  >
                    {t('OKR:keyResult')}
                  </AppText>
                  <AppInputField
                    placeholder={t('OKR:keyResult')}
                    name="key_result"
                  />
                  <AppInputField
                    placeholder={t('OKR:target')}
                    name={`target`}
                  />
                  <AppDropDown
                    placeHolder={t('createOkr:OkrKR:unit')}
                    value={values.unit}
                    isSearch
                    onChoose={(value) => {
                      setFieldValue('unit', value.value);
                      setFieldValue('unit_name', value.label);
                    }}
                    disabled={disableUnit}
                    data={okrUnit}
                  />
                  <AppInputField
                    placeholder={t('createOkr:OkrKR:planUrl')}
                    name={`plan_url`}
                  />
                  <AppInputField
                    placeholder={t('createOkr:OkrKR:resultUrl')}
                    name={`result_url`}
                  />
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity
                      style={Styles.wrapContent}
                      onPress={() => showDatePicker()}
                    >
                      <AppText style={Styles.txtAclock}>
                        {values.deadline
                          ? moment(values.deadline).format('DD-MM-YYYY')
                          : `${t('createOkr:OkrKR:deadline')}`}
                      </AppText>
                    </TouchableOpacity>
                    <ErrorMessages name="deadline" />
                    <DateTimePickerModal
                      locale="vi"
                      isVisible={Boolean(isDatePickerVisible)}
                      mode="date"
                      onConfirm={(date) => handleConfirm(date, setFieldValue)}
                      onCancel={showDatePicker}
                    />
                  </View>
                  <AppButton
                    containerStyle={{ marginTop: theme.spacing.p8 }}
                    onPress={() => handleSubmit()}
                    title={t('common:save')}
                  />
                </View>
              );
            }}
          </Formik>
        </ScrollView>
      </View>
    </ModalApp>
  );
};
