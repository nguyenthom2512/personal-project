import { StyleSheet } from 'react-native';
import { fontSize } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flexDirection: { flexDirection: 'row', alignItems: 'center' },
    container: {
      backgroundColor: theme.color.white,
      marginHorizontal: theme.spacing.p16,
      borderRadius: 8,
      padding: theme.spacing.p12,
      marginVertical: theme.spacing.p8,
    },
    wrapContent: {
      flex: 1,
      justifyContent: 'center',
      paddingVertical: theme.spacing.p15,
      paddingHorizontal: theme.spacing.p12,
      borderWidth: 1,
      borderColor: theme.color.holderPlace,
      borderRadius: 4,
      marginVertical: theme.spacing.p8,
      backgroundColor: theme.color.white,
    },
    txtAclock: {
      color: theme.color.charade,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
    },
  });
  return styles;
};
