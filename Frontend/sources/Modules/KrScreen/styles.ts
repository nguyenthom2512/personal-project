import { StyleSheet } from 'react-native';
import { fontSize } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flex1: { flex: 1 },
    flex3: { flex: 3 },
    container: {
      // flexDirection: 'row',
      backgroundColor: theme.color.white,
      marginHorizontal: theme.spacing.p16,
      borderRadius: theme.spacing.p8,
      // padding: theme.spacing.p12,
      marginVertical: theme.spacing.p8,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      // paddingTop: 50
    },
    objname: {
      flexDirection: 'row',
      paddingVertical: theme.spacing.p12,
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
      // position: 'absolute',
      // top: 0
    },
    nameWrap: {
      flexDirection: 'row',
      alignItems: 'center',
      marginVertical: theme.spacing.p8,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      backgroundColor: theme.color.white,
      padding: theme.spacing.p16,
    },
    iconName: {
      width: 48,
      height: 48,
      borderRadius: 24,
      marginRight: theme.spacing.p12,
      backgroundColor: 'gray',
    },
    workProgressWrap: { flex: 1, flexDirection: 'row', alignItems: 'center' },
    work_Target: {
      flex: 1,
      height: theme.spacing.p8,
      borderRadius: 4,
      marginRight: 4,
      backgroundColor: 'gray',
    },
    flexDirection: { flexDirection: 'row', marginVertical: theme.spacing.p8 },
    iconSeeMore: { alignItems: 'center', justifyContent: 'center' },

    underline: { textDecorationLine: 'underline', color: theme.color.violet },
    line: {
      height: 0,
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    absolute: {
      zIndex: 99,
      position: 'absolute',
      top: -50,
      right: 0,
      borderRadius: 4,
      backgroundColor: theme.color.white,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },
    viewWrapEdit: {
      flexDirection: 'row',
      paddingVertical: theme.spacing.p12,
      paddingHorizontal: theme.spacing.p16,
    },
    iconPlus: {
      zIndex: 1,
      position: 'absolute',
      margin: theme.spacing.p16,
      right: 0,
      bottom: 0,
      backgroundColor: theme.color.mediumPurple,
    },
    txtOkrDetail: {
      fontSize: theme.fontSize.f18,
      fontWeight: '400',
      color: theme.color.blackTitle,
    },
    plusCenter: {
      alignItems: 'center',
      justifyContent: 'center',
      right: 16,
      bottom: 20,
      position: 'absolute',
      zIndex: 999,
    },
  });
  return styles;
};
