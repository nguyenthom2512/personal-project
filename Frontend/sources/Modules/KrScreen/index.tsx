import { useNavigation, useRoute } from '@react-navigation/core';
import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  Linking,
  Modal,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import { Icon } from 'react-native-elements';
import { FAB } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { AppFlatlistEmpty, AppHeader, AppText, Radio } from '../../Components';
import { IndicatorContext } from '../../Context';
import useGetOkrResult from '../../Hooks/getOkrResult';
import { okrActions } from '../../Redux/Actions';
import { useTheme, useThemeAwareObject } from '../../Theme';
import CreateKR from '../CreateKR';
import ModalEdit from '../ModalEdit';
import styles from './styles';
import TrashCan from '../../Assets/Svg/trash-can.svg';
import Edit from '../../Assets/Svg/edit.svg';
import Pen from '../../Assets/Svg/pen.svg';
import Trash from '../../Assets/Svg/trash.svg';
import Menu from '../../Assets/Svg/menu.svg';
import TextDetail from '../../Components/TextDetail';
import moment from 'moment';
import { DDMMYYYY } from '../../Helpers/dateTime';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Feather from 'react-native-vector-icons/Feather';

interface PropsModalKR {
  isModalVisible?: any;
  closeModal?: any;
}
const KrScreen = (props: PropsModalKR) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const indicatorContext = useContext(IndicatorContext);
  const [show, setShow] = useState();
  const route = useRoute();
  const { refetchGetOKR, param, type, refresh }: any = route.params;

  const [dataOkrResult, loading, error, refetch]: any = useGetOkrResult({
    id: param?.id,
  });

  useEffect(() => {
    if (dataOkrResult.result_parent) {
      setDisableUnit(true);
    }
  }, [dataOkrResult]);
  const [disableUnit, setDisableUnit] = useState(false);
  const [modalEdit, setModalEdit] = useState<any>({});
  const ChangeModalEdit = () => {
    setModalEdit({});
  };
  const CONFIDENT = ['Kém', 'Bình thường', 'Rất tốt'];
  const createColor = (curentPercent: any) => {
    if (curentPercent === 2) {
      return theme.color.greenBox;
    } else if (curentPercent === 1) {
      return theme.color.fireBush;
    } else {
      return theme.color.cinnabar;
    }
  };
  const deleteKR = (elm: any) => {
    Alert.alert('', `${t('common:confirmDeleteKR')}`, [
      {
        text: `${t('common:ok')}`,
        onPress: () => {
          indicatorContext.setLoading(true);
          dispatch(
            okrActions.DeleteOkrResultRequest(elm.id, {
              onSuccess: (success: any) => {
                refetch();
                indicatorContext.setLoading(false);
                Alert.alert(`${t('common:deleteSuccess')}`);
              },
              onFailed: (error: any) => {
                indicatorContext.setLoading(false);
                Alert.alert(`${t('common:deleteFailed')}`, error.errorMessage);
              },
            }),
          );
        },
      },
      {
        text: `${t('common:cancel')}`,
        onPress: () => {},
      },
    ]);
  };
  const deleteOkr = (elm: any) => {
    Alert.alert('', `${t('common:confirmDeleteOKR')}`, [
      {
        text: `${t('common:ok')}`,
        onPress: () => {
          indicatorContext.setLoading(true);
          dispatch(
            okrActions.DeleteOkrRequest(elm, {
              onSuccess: (success: any) => {
                indicatorContext.setLoading(false);
                Alert.alert(t('common:deleteSuccess'), '', [
                  {
                    text: 'OK',
                    onPress: () => {
                      refresh && refresh();
                      navigation.navigate('TreeOKR');
                    },
                  },
                ]);
              },
              onFailed: (error: any) => {
                indicatorContext.setLoading(false);
                Alert.alert(`${t('common:deleteFailed')}`, error.errorMessage);
              },
            }),
          );
        },
      },
      {
        text: `${t('common:cancel')}`,
        onPress: () => {},
      },
    ]);
  };
  return (
    <>
      <View style={{ flex: 1 }}>
        {/* {dataOkrResult?.okr_result?.length <= 5 && type == 'keyResult' && (
          // // <FAB
          // //   icon="plus"
          //   onPress={() => {
          //     navigation.navigate('CreateKR', {
          //       refetch: refetch,
          //       okr: dataOkrResult?.id,
          //       user: dataOkrResult?.user,
          //       okr_parent: dataOkrResult?.okr_parent,
          //       unit: dataOkrResult?.okr_result[0].unit,
          //     });
          //   }}
          // //   style={Styles.plusCenter}
          // //   color={theme.color.white}
          // // />
          <TouchableOpacity
            style={Styles.plusCenter}
            onPress={() => {
              navigation.navigate('CreateKR', {
                refetch: refetch,
                okr: dataOkrResult?.id,
                user: dataOkrResult?.user,
                okr_parent: dataOkrResult?.okr_parent,
                unit: dataOkrResult?.okr_result[0].unit,
              });
            }}
          >
            <MaterialIcons
              name="add-task"
              size={40}
              color={theme.color.violet}
            />
          </TouchableOpacity>
        )} */}
        <AppHeader
          iconLeft="arrow-left"
          onLeftPress={() => {
            type == 'keyResult' && refetchGetOKR && refetchGetOKR();
            refresh && refresh();
            navigation.goBack();
          }}
          title={dataOkrResult?.object_name ?? param.object_name}
          svgRight
          iconRight={type == 'keyResult' ? <Edit /> : null}
          onRightPress={() => {
            type == 'keyResult' &&
              navigation.navigate('CreateOKR', {
                id: param.id,
                refetchScreen: refetch,
                refetchOkr: refetchGetOKR,
              });
          }}
          svgRightAdd
          iconRightAdd={type == 'keyResult' ? <TrashCan /> : null}
          onRightAddPress={() => {
            type == 'keyResult' && deleteOkr(param.id);
          }}
        />
        <View
          style={{
            margin: 16,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          <AppText style={Styles.txtOkrDetail}>{t('OKR:okrDetail')}</AppText>
          {dataOkrResult?.okr_result?.length < 5 && type == 'keyResult' && (
            <TouchableOpacity
              // style={Styles.plusCenter}
              onPress={() => {
                navigation.navigate('CreateKR', {
                  refetch: refetch,
                  okr: dataOkrResult?.id,
                  user: dataOkrResult?.user,
                  okr_parent: dataOkrResult?.okr_parent,
                  unit: dataOkrResult?.okr_result[0].unit,
                });
              }}
            >
              <Feather
                name="plus"
                size={theme.fontSize.f32}
                color={theme.color.violet}
              />
            </TouchableOpacity>
          )}
        </View>
        {loading ? (
          <ActivityIndicator size="large" color={theme.color.gray} />
        ) : (
          <View style={{ flex: 1, backgroundColor: theme.color.white }}>
            <FlatList
              // showsHorizontalScrollIndicator = {false}
              showsVerticalScrollIndicator={false}
              data={dataOkrResult?.okr_result}
              keyExtractor={(_, index) => index.toString()}
              renderItem={({ item: elm }) => {
                return (
                  <TouchableWithoutFeedback
                    // activeOpacity={1}
                    style={{ flex: 1 }}
                    onPress={() => setShow(undefined)}
                  >
                    <View style={Styles.container}>
                      <View style={Styles.objname}>
                        <AppText
                          style={Styles.flex1}
                          numberOfLines={1}
                          ellipsizeMode="tail"
                        >
                          {elm.key_result}
                        </AppText>
                        {type == 'keyResult' && (
                          <TouchableOpacity
                            onPress={() => {
                              setShow(elm.id);
                            }}
                          >
                            <Menu />
                          </TouchableOpacity>
                        )}
                      </View>
                      <View style={{ paddingHorizontal: theme.spacing.p16 }}>
                        {elm.id == show && (
                          <View style={Styles.absolute}>
                            <TouchableOpacity
                              activeOpacity={0.8}
                              onPress={() => {
                                setShow(undefined);
                                setModalEdit(elm);
                              }}
                              style={[
                                Styles.viewWrapEdit,
                                {
                                  borderBottomWidth: 1,
                                  borderColor: theme.color.mercury,
                                },
                              ]}
                            >
                              <Pen />
                              <AppText
                                style={{
                                  fontSize: theme.fontSize.f14,
                                  paddingHorizontal: theme.spacing.p12,
                                }}
                              >
                                {t('common:update')}
                              </AppText>
                            </TouchableOpacity>
                            {dataOkrResult.okr_result?.length > 1 && (
                              <TouchableOpacity
                                style={Styles.viewWrapEdit}
                                activeOpacity={0.8}
                                onPress={() => {
                                  setShow(undefined);
                                  deleteKR(elm);
                                }}
                              >
                                <Trash />
                                <AppText
                                  style={{
                                    fontSize: theme.fontSize.f14,
                                    paddingHorizontal: theme.spacing.p12,
                                  }}
                                >
                                  {t('common:delete')}
                                </AppText>
                              </TouchableOpacity>
                            )}
                          </View>
                        )}
                        <TextDetail
                          textKey={`${t('OKR:target')}:`}
                          value={elm.target}
                        />
                        <TextDetail
                          textKey={`${t('OKR:unit')}:`}
                          value={elm.unit_name}
                        />
                        <TextDetail
                          textKey={t('OKR:current_done')}
                          value={elm.current_done}
                        />
                        <TextDetail
                          textKey={t('OKR:percentCompleted')}
                          value={`${
                            elm.processed ? (elm.processed * 100).toFixed(2) : 0
                          }%`}
                          color
                          style={{
                            backgroundColor: createColor(elm.confident),
                          }}
                        />
                        <TextDetail
                          textKey={t('OKR:change')}
                          value={`${
                            elm.percent_changed
                              ? (elm.percent_changed * 100).toFixed(2)
                              : 0
                          }%`}
                        />
                        <TextDetail
                          textKey={t('OKR:confident')}
                          value={
                            CONFIDENT[elm.confident] || `${t('OKR:notCheckIn')}`
                          }
                          style={{ color: createColor(elm.confident) }}
                        />
                        <TextDetail
                          textKey={t('OKR:plan')}
                          value={
                            <TouchableOpacity
                              onPress={() => {
                                elm.plan_url
                                  ? Linking.openURL(elm.plan_url)
                                  : Alert.alert(
                                      `${t('common:unableToOpenURL')}`,
                                    );
                              }}
                            >
                              <AppText style={Styles.underline}>Link</AppText>
                            </TouchableOpacity>
                          }
                        />
                        <TextDetail
                          textKey={t('OKR:results')}
                          value={
                            <TouchableOpacity
                              onPress={() => {
                                elm.result_url
                                  ? Linking.openURL(elm.result_url)
                                  : Alert.alert(
                                      `${t('common:unableToOpenURL')}`,
                                    );
                              }}
                            >
                              <AppText style={Styles.underline}>Link</AppText>
                            </TouchableOpacity>
                          }
                        />
                        <TextDetail
                          textKey={t('OKR:deadline')}
                          value={
                            <AppText>
                              {moment(elm.deadline).format(DDMMYYYY)}
                            </AppText>
                          }
                        />
                      </View>
                    </View>
                  </TouchableWithoutFeedback>
                );
              }}
              ListEmptyComponent={() => <AppFlatlistEmpty />}
            />
            <ModalEdit
              modalEdit={modalEdit}
              setCloseModalEdit={ChangeModalEdit}
              refetch={refetch}
              okr={dataOkrResult?.id}
              disableUnit={disableUnit}
            />
          </View>
        )}
      </View>
    </>
  );
};
export default KrScreen;
