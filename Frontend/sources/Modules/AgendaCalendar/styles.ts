import { StyleSheet } from 'react-native';
import {
  isIOS,
  ScreenHeight,
  ScreenWidth,
} from 'react-native-elements/dist/helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    containerHeader: {
      width: '100%',
      backgroundColor: theme.color.blackHeader,
      flexDirection: 'row',
      justifyContent: 'space-around',
      paddingVertical: theme.spacing.p20,
      alignItems: 'center',
      height: 100,
    },
    containerHeaderOfMonthView: {
      width: '100%',
      flexDirection: 'row',
      borderBottomWidth: 1,
      borderBottomColor: theme.color.grayShaft,
      height: 38,
    },
    boxTextDay: {
      borderRightWidth: 1,
      borderRightColor: theme.color.grayShaft,
      alignItems: 'center',
      flex: 1,
      justifyContent: 'center',
    },
    textHeaderOfMonthView: {
      fontSize: theme.fontSize.f12,
      color: theme.color.backgroundColor,
      opacity: 0.5,
    },
    headerItem: {
      alignItems: 'center',
      paddingHorizontal: 10,
      paddingVertical: 4,
      borderRadius: 4,
    },
    textDay: {
      color: theme.color.backgroundColor,
      opacity: 0.5,
      fontSize: theme.fontSize.f14,
    },
    textDate: {
      color: theme.color.backgroundColor,
      fontSize: theme.fontSize.f18,
      fontWeight: isIOS ? '600' : 'bold',
      padding: 4,
    },
    titleEvent: {
      color: '#26272E',
      fontSize: theme.fontSize.f14,
      textAlign: 'justify',
    },
    subTitle: {
      color: theme.color.backgroundColor,
      fontSize: theme.fontSize.f16,
      fontWeight: isIOS ? '600' : 'bold',
      marginBottom: theme.spacing.p8,
    },
    todoListItem: {
      padding: theme.spacing.p8,
      borderRadius: 4,
      marginVertical: 4,
    },
    containerTodoList: {
      // backgroundColor: theme.color.dartBackground,
      backgroundColor: `${theme.color.whiteSmoke}A0`,
      width: ScreenWidth / 2 - 16,
      height: ScreenHeight - 180,
      zIndex: 1000,
      position: 'absolute',
      top: 104,
      left: 8,
      bottom: 4,
      borderRadius: 4,
      paddingVertical: theme.spacing.p8,
    },
    containerTimeline: {
      backgroundColor: theme.color.dartBlue,
      // backgroundColor: 'transparent',
      width: ScreenWidth / 2 - 8,
      height: ScreenHeight - 180,
      zIndex: 1000,
      position: 'absolute',
      top: 104,
      right: 8,
      bottom: 4,
      borderRadius: 4,
    },
    bodyContainerTodayType: {
      backgroundColor: theme.color.dartBlue,
    },
    bodyContainerDayType: {
      backgroundColor: theme.color.dartBlue,
      width: '50%',
      marginLeft: '50%',
    },
    bodyContainerWeekOrMonthType: {
      backgroundColor: theme.color.dartBlue,
      width: 0,
      marginLeft: '50%',
    },
    iconArrow: {
      color: 'rgba(255, 255, 255, 0.5)',
      paddingHorizontal: theme.spacing.p4,
    },
    calendarContainerStyle: { backgroundColor: theme.color.dartBackground },
    activeHeader: {
      backgroundColor: theme.color.mediumPurple,
    },
    scrollViewTimeline: {
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p8,
    },
  });
  return styles;
};
