import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    drawerItem: {
      borderRadius: 0,
      marginHorizontal: 0,
      padding: 8,
      marginVertical: 0,
      borderBottomColor: 'rgba(255, 255, 255, 0.7)',
      borderBottomWidth: 1,
    },
    containerIconClose: {
      width: '100%',
      alignItems: 'flex-end',
      padding: theme.spacing.p16,
    },
    container: {
      backgroundColor: theme.color.dartBlue,
    },
  });
  return styles;
};
