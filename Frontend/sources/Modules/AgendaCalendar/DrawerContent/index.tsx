import React from 'react';
import {
  DrawerContentComponentProps,
  DrawerContentScrollView,
  DrawerItem,
} from '@react-navigation/drawer';
import Animated from 'react-native-reanimated';
import { DeviceEventEmitter, View } from 'react-native';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './styles';
import { CalendarType } from '..';
import { useDispatch, useSelector } from 'react-redux';
import { setTypeCalendar } from '../../../Redux/Actions/calendarActions';
import { RootState } from '../../../Redux/Reducers';
import { useTranslation } from 'react-i18next';
import { useNavigation } from '@react-navigation/native';

const CustomDrawerContent = (props: DrawerContentComponentProps) => {
  const calendarReducer = useSelector(
    (state: RootState) => state.calendarReducer,
  );

  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const translateX = Animated.interpolate(props?.progress, {
    inputRange: [0, 1],
    outputRange: [-100, 0],
  });

  const renderLabel = (type: CalendarType | null) => {
    switch (type) {
      case CalendarType.DAY:
        return t('common:oneDay');
      case CalendarType.MONTH:
        return t('common:month');
      case CalendarType.WEEK:
        return t('common:week');
      case CalendarType.THREE_DAY:
        return t('common:threeDay');
      case CalendarType.TEAM:
        return t('common:teamList');
      default:
        return t('common:priorityList');
    }
  };

  const onCloseDrawer = () => {
    DeviceEventEmitter.emit('closeDrawer');
  };

  const renderDrawerItem = (type: CalendarType | null) => {
    return (
      <DrawerItem
        style={Styles.drawerItem}
        activeBackgroundColor={theme.color.mediumPurple}
        inactiveTintColor="rgba(255, 255, 255, 0.7)"
        activeTintColor={theme.color.backgroundColor}
        label={renderLabel(type)}
        focused={type == calendarReducer.calendarType}
        onPress={() => {
          dispatch(setTypeCalendar(type));
          if (type == CalendarType.DAY) {
            navigation.navigate('TodoList');
          } else if (type == CalendarType.TEAM) {
            navigation.navigate('TodoPriorityTeamList', {
              isPriority: true,
              isDone: false,
            });
          } else if (type === null) {
            navigation.navigate('TodoPriorityList', {
              isPriority: true,
              isDone: false,
            });
          } else {
            navigation.navigate('AgendaCalendar');
          }
          onCloseDrawer();
        }}
      />
    );
  };

  return (
    <DrawerContentScrollView style={Styles.container} {...props}>
      <Animated.View style={{ transform: [{ translateX }] }}>
        <View style={Styles.containerIconClose}>
          <Icon
            name="close"
            size={32}
            color={theme.color.backgroundColor}
            onPress={onCloseDrawer}
          />
        </View>
        {renderDrawerItem(CalendarType.DAY)}
        {renderDrawerItem(CalendarType.THREE_DAY)}
        {renderDrawerItem(CalendarType.WEEK)}
        {renderDrawerItem(CalendarType.MONTH)}
        {renderDrawerItem(CalendarType.TEAM)}
        {renderDrawerItem(null)}
      </Animated.View>
    </DrawerContentScrollView>
  );
};

export default CustomDrawerContent;
