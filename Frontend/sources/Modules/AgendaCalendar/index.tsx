import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { AppContainer, AppText } from '../../Components';
import { useTheme, useThemeAwareObject } from '../../Theme';
import Calendar, { HorizontalDirection } from '../../Components/react-native-big-calendar/index';

import moment from 'moment';
import styles from './styles';
import {
  DeviceEventEmitter,
  FlatList,
  ScrollView,
  TouchableOpacity,
  View,
  ActivityIndicator,
  Alert,
  ImageBackground,
  Platform,
} from 'react-native';
import {
  DrawerActions,
  useNavigation,
  StackActions,
} from '@react-navigation/native';
import { RootState } from '../../Redux/Reducers';
import ActionTypes from '../../Redux/ActionTypes';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import { getCalendarRequest } from '../../Redux/Actions/calendarActions';
import { dateFormat } from '../../Helpers/constant';
import { noDateTodoRequest } from '../../Redux/Actions/noDateTodoActions';
import { TodoItemComponent } from '../../Components/TodoItems';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { ScreenHeight, ScreenWidth } from 'react-native-elements/dist/helpers';
import { device } from '../../Helpers';
import { usePanResponder } from '../../Hooks/usePanResponder';
interface Props { }

export interface EventCalendar {
  title: string;
  start: Date;
  end: Date;
  type: number;
  id?: number;
  color: string;
}

interface TimeLine {
  time: string;
  listEvent: EventCalendar[];
}

export enum CalendarType {
  TODAY = 'TODAY',
  DAY = 'DAY',
  THREE_DAY = 'THREE_DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
  TEAM = 'TEAM',
}
let page = 1;
const page_size = 20;
const HEIGHT_TEMPLATE = 896;

export default () => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const [currentDateSelected, setcurrentDateSelected] = useState(new Date());
  const [dateChangeCalendar, setdateChangeCalendar] = useState(new Date());

  const calendarReducer = useSelector(
    (state: RootState) => state.calendarReducer,
  );
  const [calendarType, setCalendarType] = useState<CalendarType | null>(
    calendarReducer.calendarType || null,
  );
  const noDateTodoReducer = useSelector(
    (state: RootState) => state.noDateTodoReducer,
  );
  const hourMustMinus: number =
    Platform.OS === 'ios'
      ? (device.height * 2) / HEIGHT_TEMPLATE - 0.7
      : (device.height * 2) / HEIGHT_TEMPLATE;

  useEffect(() => {
    const listener = DeviceEventEmitter.addListener('closeDrawer', (_e) => {
      navigation.dispatch(DrawerActions.closeDrawer);
    });
    dispatch(noDateTodoRequest({ page, page_size }));
    return () => {
      listener.remove();
    };
  }, []);

  const onLoad = () => {
    if (noDateTodoReducer.loading || page >= noDateTodoReducer.noPage) {
      return;
    }
    page = page + 1;
    dispatch(noDateTodoRequest({ page, page_size }));
  };

  const onRefresh = () => {
    page = 1;
    dispatch(noDateTodoRequest({ page, page_size }));
  };

  useEffect(() => {
    if (calendarReducer.type == ActionTypes.CHANGE_CALENDAR_TYPE) {
      setCalendarType(calendarReducer.calendarType || null);

      setcurrentDateSelected(new Date());
    }
  }, [calendarReducer]);

  useEffect(() => {
    let start_time = moment(currentDateSelected).format(
      dateFormat.year_month_day,
    );
    let end_time = moment(currentDateSelected)
      .add(1, 'days')
      .format(dateFormat.year_month_day);
    if (calendarReducer.calendarType == CalendarType.MONTH) {
      start_time = moment(currentDateSelected)
        .startOf('months')
        .format(dateFormat.year_month_day);
      end_time = moment(currentDateSelected)
        .add(1, 'months')
        .startOf('months')
        .format(dateFormat.year_month_day);
    }
    if (calendarReducer.calendarType == CalendarType.WEEK) {
      start_time = moment(currentDateSelected)
        .startOf('isoWeeks')
        .format(dateFormat.year_month_day);
      end_time = moment(currentDateSelected)
        .endOf('isoWeeks')
        .format(dateFormat.year_month_day);
    }
    if (calendarReducer.calendarType == CalendarType.THREE_DAY) {
      start_time = moment(currentDateSelected).format(
        dateFormat.year_month_day,
      );
      end_time = moment(currentDateSelected)
        .add(2, 'days')
        .format(dateFormat.year_month_day);
    }
    dispatch(getCalendarRequest({ start_time, end_time }));
  }, [currentDateSelected]);


  const onSwipeHorizontal =
    (direction: HorizontalDirection) => {
      if (calendarReducer.calendarType == CalendarType.THREE_DAY) {
        setcurrentDateSelected(moment(currentDateSelected).add(direction === 'LEFT' ? 3 : -3, 'days').toDate());
      }
      if (calendarReducer.calendarType == CalendarType.WEEK) {
        setcurrentDateSelected(moment(currentDateSelected).add(direction === 'LEFT' ? 1 : -1, 'weeks').toDate());
      }
    }

  const panResponder = usePanResponder({ onSwipeHorizontal })

  const handleDataEvent = (data: any[]) => {
    return data.map((elm, index) => {
      return {
        id: elm.id,
        title: elm.todo_name,
        start: elm?.start_time
          ? moment(elm?.start_time).toDate()
          : new Date(elm?.start_time),
        end: elm?.end_time
          ? moment(elm?.end_time).toDate()
          : new Date(elm?.end_time),
        type: elm.priority.priority_level,
        color: elm.priority.color,
      };
    });
  };

  const handleDataEventByDay = (data: any) => {
    return Object.keys(data).map((elm, index) => {
      return {
        time: moment(elm).format('DD/MM'),
        listEvent: data[elm].map((item: any, index: number) => {
          return {
            title: item.todo_name,
            start: new Date(item?.start_time),
            end: new Date(item?.end_time),
            type: item.priority.priority_level,
          };
        }),
      };
    });
  };

  const handleBodyContainerStyle = () => {
    switch (calendarType) {
      case CalendarType.TODAY:
        return Styles.bodyContainerTodayType;
      case CalendarType.DAY:
        return Styles.bodyContainerDayType;
      default:
        return Styles.bodyContainerWeekOrMonthType;
    }
  };

  const renderDayName = (day: number) => {
    switch (day) {
      case 0:
        return t('common:sortNameSunday');
      case 1:
        return t('common:sortNameMonDay');
      case 2:
        return t('common:sortNameTuesday');
      case 3:
        return t('common:sortNameWednesday');
      case 4:
        return t('common:sortNameThursday');
      case 5:
        return t('common:sortNameFriday');
      case 6:
        return t('common:sortNameSaturday');
      default:
        return '';
    }
  };

  const renderMonthName = (month: number) => {
    switch (month) {
      case 0:
        return t('calendar:Jan');
      case 1:
        return t('calendar:Feb');
      case 2:
        return t('calendar:Mar');
      case 3:
        return t('calendar:Apr');
      case 4:
        return t('calendar:May');
      case 5:
        return t('calendar:Jun');
      case 6:
        return t('calendar:Jul');
      case 7:
        return t('calendar:Aug');
      case 8:
        return t('calendar:Sep');
      case 9:
        return t('calendar:Oct');
      case 10:
        return t('calendar:Nov');
      case 11:
        return t('calendar:Dec');
      default:
        return '';
    }
  };
  const onSelectDate = (date: Date) => {
    setcurrentDateSelected(
      /**
       * if seleted date same today => set today
       */
      moment(date).isSame(moment(), 'day') ? new Date() : date,
    );
  };

  const handleChangeTimeRange = (
    currentRange: Date[],
    type: CalendarType | null,
    typeChange: 'pre' | 'next',
  ) => {
    const isNextTime = typeChange == 'next';
    const timeChange = currentRange[isNextTime ? currentRange.length - 1 : 0];
    const dataChange = isNextTime ? 1 : -1;
    const dataChange3Day = isNextTime ? 1 : -3;
    switch (type) {
      case CalendarType.THREE_DAY:
        return moment(timeChange).add(dataChange3Day, 'days').toDate();
      case CalendarType.WEEK:
        return moment(timeChange)
          .add(dataChange, isNextTime ? 'days' : 'weeks')
          .toDate();
      case CalendarType.MONTH:
        return moment(timeChange).add(dataChange, 'months').toDate();
      default:
        break;
    }
  };

  const handleBackgroundColorEvent = (type: number) => {
    switch (type) {
      case 0:
        return theme.color.magicMint;
      case 1:
        return theme.color.salomie;
      case 2:
        return theme.color.pink;
      case 3:
        return theme.color.lavenderBlue;
      default:
        return theme.color.blizzardBlue;
    }
  };

  const renderHeaderForMonthView = (_data: any) => {
    return (
      <View style={Styles.containerHeaderOfMonthView}>
        {[1, 2, 3, 4, 5, 6, 0].map((elm) => (
          <View key={elm.toString()} style={Styles.boxTextDay}>
            <AppText style={Styles.textHeaderOfMonthView}>
              {renderDayName(elm)}
            </AppText>
          </View>
        ))}
      </View>
    );
  };

  const renderHeader = (data: any) => {
    const isShowDayType =
      calendarType == CalendarType.DAY || calendarType == CalendarType.TODAY;
    const isShowMonthType = calendarType == CalendarType.MONTH;
    let day =
      calendarType == CalendarType.THREE_DAY
        ? moment(currentDateSelected)
        : moment(currentDateSelected).startOf('isoWeeks');

    const listDate: Date[] = [];
    const listMonth: Date[] = [];

    const rangeDate = calendarType == CalendarType.THREE_DAY ? 3 : 7;
    if (!isShowMonthType) {
      listDate.push(day.toDate());
      while (listDate.length < rangeDate) {
        day.add(1, 'days');
        listDate.push(day.toDate());
      }
    } else {
      listMonth.push(day.toDate());
      while (listMonth.length < 3) {
        day.add(1, 'month');
        listMonth.push(day.toDate());
      }
    }

    return (
      <View style={Styles.containerHeader}>
        {!isShowDayType && (
          <Icon
            iconStyle={Styles.iconArrow}
            type="entypo"
            name="chevron-thin-left"
            size={25}
            onPress={() => {
              data.onPressDateHeader(
                handleChangeTimeRange(
                  isShowMonthType ? listMonth : listDate,
                  calendarType,
                  'pre',
                ),
              );
            }}
          />
        )}

        {isShowMonthType
          ? listMonth?.map((item: Date, index: number) => {
            return (
              <TouchableOpacity
                disabled={!isShowDayType && !isShowMonthType}
                key={index.toString()}
                onPress={() => {
                  data.onPressDateHeader(item.toString());
                }}
                style={Styles.headerItem}
              >
                <AppText
                  style={
                    moment(currentDateSelected).isSame(item, 'months')
                      ? Styles.textDate
                      : Styles.textDay
                  }
                >
                  {renderMonthName(moment(item.toString()).month())}
                </AppText>
              </TouchableOpacity>
            );
          })
          : listDate?.map((item: Date, index: number) => {
            return (
              <TouchableOpacity
                disabled={!isShowDayType && !isShowMonthType}
                key={index.toString()}
                onPress={() => {
                  data.onPressDateHeader(item);
                }}
                style={[
                  Styles.headerItem,
                  moment(currentDateSelected).isSame(item, 'date') &&
                  isShowDayType &&
                  Styles.activeHeader,
                ]}
              >
                <AppText style={Styles.textDay}>
                  {renderDayName(moment(item).day())}
                </AppText>
                <AppText style={Styles.textDate}>
                  {moment(item.toString()).date()}
                </AppText>
              </TouchableOpacity>
            );
          })}

        {!isShowDayType && (
          <Icon
            iconStyle={Styles.iconArrow}
            type="entypo"
            name="chevron-thin-right"
            size={25}
            onPress={() =>
              data.onPressDateHeader(
                handleChangeTimeRange(
                  isShowMonthType ? listMonth : listDate,
                  calendarType,
                  'next',
                ),
              )
            }
          />
        )}
        <ImageBackground
          style={{
            position: 'absolute',
            top: 100,
            zIndex: 1,
            left: 0,
            right: 0,
            height: ScreenHeight - 100,
            width: ScreenWidth,
          }}
          source={{
            uri: 'https://scontent.xx.fbcdn.net/v/t1.6435-9/243066043_5149824568377371_7520749492545220708_n.jpg?_nc_cat=109&ccb=1-5&_nc_sid=825194&_nc_ohc=-u-OoEHLGHsAX_t4SHy&tn=3xgQxwFRgqCAFYh1&_nc_ht=scontent.xx&oh=0e156da3077812d12330d88761131936&oe=6187C4B9',
          }}
        />
        {renderListTodo()}
        {!isShowDayType &&
          renderTimeLine(
            handleDataEventByDay(calendarReducer.listEventByDay || []),
          )}
      </View>
    );
  }

  const renderEvent = (data: EventCalendar, touchableOpacityProps: any) => {
    const isMonthView = calendarType == CalendarType.MONTH;
    const { title } = data;
    touchableOpacityProps.style[isMonthView ? 1 : 2].backgroundColor =
      data.color;
    return (
      <TouchableOpacity {...touchableOpacityProps}>
        <AppText
          numberOfLines={isMonthView ? 2 : undefined}
          style={[
            Styles.titleEvent,
            { fontSize: theme.fontSize[isMonthView ? 'f8' : 'f14'] },
          ]}
        >
          {title}
        </AppText>
      </TouchableOpacity>
    );
  };

  const renderItemTodoList = (
    item: EventCalendar,
    index: number,
    isWeekType: boolean,
  ) => {
    return (
      <TouchableOpacity
        key={index.toString()}
        style={[
          Styles.todoListItem,
          {
            backgroundColor: handleBackgroundColorEvent(item.type),
          },
        ]}
      >
        <AppText numberOfLines={isWeekType ? 1 : undefined}>
          {item?.title}
        </AppText>
      </TouchableOpacity>
    );
  };

  const renderSubList = (
    title: string,
    listData: EventCalendar[],
    isTodoList: boolean = false,
    idx: number = 0,
  ) => {
    return (
      <View
        key={title}
        style={{ marginBottom: 88 + useSafeAreaInsets().bottom }}
      >
        <AppText
          style={[
            Styles.subTitle,
            { fontSize: theme.fontSize[isTodoList ? 'f16' : 'f14'] },
          ]}
        >
          {title}
        </AppText>
        <FlatList
          data={listData}
          keyExtractor={(item) => item?.id?.toString() || ''}
          renderItem={({ item, index }) => {
            return renderItemTodoList(
              item,
              index,
              calendarType == CalendarType.WEEK && !isTodoList,
            );
          }}
          showsVerticalScrollIndicator={false}
          extraData={handleDataEvent(noDateTodoReducer?.data?.results).length}
          onEndReached={onLoad}
          onRefresh={onRefresh}
          refreshing={
            page === 1 &&
            noDateTodoReducer.type === ActionTypes.NO_DATE_TODO_REQUEST
          }
          ListFooterComponent={
            page < noDateTodoReducer.noPage ? <ActivityIndicator /> : null
          }
        />
      </View>
    );
  };

  const renderListTodo = () => {
    return (
      <View style={Styles.containerTodoList}>
        <View
          style={{
            marginBottom: theme.spacing.p24,
            // marginLeft: theme.spacing.p16,
          }}
        >
          {/* <AppText
            style={[
              Styles.subTitle,
              { fontSize: theme.fontSize['f16'], marginLeft: theme.spacing.p8 },
            ]}
          >
            {t('homepage:todoList')}
          </AppText> */}
          <FlatList
            data={noDateTodoReducer?.data?.results}
            keyExtractor={(item) => item?.id?.toString() || ''}
            renderItem={({ item, index }) => {
              return (
                <TodoItemComponent
                  data={item}
                  showPriority
                  containerStyles={{ backgroundColor: item.priority.color }}
                />
              );
            }}
            showsVerticalScrollIndicator={false}
            extraData={noDateTodoReducer?.data?.results.length ?? 0}
            onEndReached={onLoad}
            onRefresh={onRefresh}
            refreshing={
              page === 1 &&
              noDateTodoReducer.type === ActionTypes.NO_DATE_TODO_REQUEST
            }
            ListFooterComponent={
              page < noDateTodoReducer.noPage ? <ActivityIndicator /> : null
            }
          />
        </View>
      </View>
    );
  };

  const renderTimeLine = (timeline?: TimeLine[]) => {
    const isMonthType = calendarType == CalendarType.MONTH;
    return (
      <View style={Styles.containerTimeline}>
        {isMonthType ? (
          renderCalendarOfMonthView(
            handleDataEvent(calendarReducer.listEvent ?? []),
          )
        ) : (
            <ScrollView
              style={Styles.scrollViewTimeline}
              nestedScrollEnabled
              showsVerticalScrollIndicator={false}
              {...panResponder.panHandlers}
            >
              {timeline?.map((elm, index) =>
                renderSubList(elm?.time, elm.listEvent, false, index),
              )}
            </ScrollView>
          )}
      </View>
    );
  }


  const renderCalendarOfMonthView = (listEvent: EventCalendar[]) => {
    return (
      <Calendar
        renderHeaderForMonthView={renderHeaderForMonthView}
        onPressDateHeader={(date) => onSelectDate(date)}
        date={currentDateSelected}
        mode="month"
        events={listEvent}
        height={600}
        weekStartsOn={1}
        bodyContainerStyle={{
          ...Styles.bodyContainerTodayType,
          marginBottom: 130 - useSafeAreaInsets().bottom,
        }}
        calendarContainerStyle={Styles.calendarContainerStyle}
        renderEvent={renderEvent}
        swipeEnabled={true}
        onChangeDate={([start, end]) => { }}
        theme={{
          typography: {
            xs: {
              fontSize: theme.fontSize.f14,
            },
          },
          palette: {
            nowIndicator: theme.color.backgroundColor,
            gray: {
              '100': theme.color.grayShaft,
              '200': theme.color.grayShaft,
              '300': theme.color.gray,
              '500': theme.color.black,
              '800': theme.color.lightGray,
            },
          },
        }}
      />
    );
  };

  return (
    <AppContainer
      iconLeft={'arrow-left'}
      typeIconLeft={'feather'}
      iconRightAdd={calendarType !== CalendarType.TODAY ? 'menu' : undefined}
      onRightAddPress={() => {
        navigation.dispatch(DrawerActions.toggleDrawer);
      }}
      onLeftPress={() => {
        navigation.dispatch(StackActions.pop(100));
      }}
    >
      <Calendar
        onPressDateHeader={(date) => onSelectDate(date)}
        date={currentDateSelected}
        renderHeader={renderHeader}
        mode="day"
        weekEndsOn={0}
        events={handleDataEvent(calendarReducer.listEvent ?? []) || []}
        scrollOffsetMinutes={(moment().get('hours') - hourMustMinus) * 50}
        height={600}
        bodyContainerStyle={handleBodyContainerStyle()}
        calendarContainerStyle={Styles.calendarContainerStyle}
        renderEvent={renderEvent}
        swipeEnabled={true}
        onSwipe={(date) => {
          setcurrentDateSelected(new Date(date));
        }}
        onChangeDate={([start, end]) => {
          try {
            if (
              moment(dateChangeCalendar).isSame(start, 'day')
              // moment(dateChangeCalendar).format('DD-MM-YYYY') ===
              // moment(start).format('DD-MM-YYYY')
            ) {
              // setcurrentDateSelected(start);
              setdateChangeCalendar(currentDateSelected);
              return;
            }
            if (
              moment(currentDateSelected).isSame(start, 'day')
              // moment(currentDateSelected).format('DD-MM-YYYY') !==
              // moment(start).format('DD-MM-YYYY')
            ) {
              setcurrentDateSelected(moment(start).toDate());
              setdateChangeCalendar(moment(start).toDate());
            }
          } catch (error) {
            Alert.alert(t('common:notice'), JSON.stringify(error));
          }
        }}
        theme={{
          typography: {
            xs: {
              fontSize: theme.fontSize.f14,
            },
          },
          palette: {
            nowIndicator: theme.color.backgroundColor,
            gray: {
              '100': theme.color.grayShaft,
              '200': theme.color.grayShaft,
              '300': theme.color.gray,
            },
          },
        }}
      />
    </AppContainer>
  );
};
