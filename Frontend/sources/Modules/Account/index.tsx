import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { AppButton, AppContainer } from '../../Components';
import { userActions } from '../../Redux/Actions';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface Props {}

export default () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  return (
    <AppContainer title={t('account:account')}>
      {/* <AppButton
        title="Sao của bạn"
        onPress={() => navigation.navigate('StarScreen')}
        style={{ marginVertical: 8 }}
      /> */}
      <AppButton
        title={t('common:logout')}
        onPress={() => dispatch(userActions.logout())}
        style={{ marginVertical: 8 }}
      />
      <AppButton
        title={t('account:account')}
        onPress={() => navigation.navigate('ProFile')}
        style={{ marginVertical: 8 }}
      />
      {/* <AppButton
        title="Quiz"
        onPress={() => navigation.navigate('Quiz', { id: 1 })}
        style={{ marginVertical: 8 }}
      /> */}
    </AppContainer>
  );
};
