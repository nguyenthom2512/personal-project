import { Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, View } from 'react-native';
import { useSelector } from 'react-redux';
import yup from 'yup';
import {
  AppButton,
  AppContainer,
  AppInputField,
  ColorPicker,
} from '../../../Components';
import { IndicatorContext } from '../../../Context';
import useGetPriority from '../../../Hooks/getPriority';
import { RootState } from '../../../Redux/Reducers';
import priorityService from '../../../Services/priorityService';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './style';

const CreatePriority = () => {
// return <View style={{height: 20, width: 400, backgroundColor: 'green'}} ></View>
  const { t } = useTranslation();
  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const theme = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { setLoading } = useContext(IndicatorContext);

  const [codeColor, setCodeColor] = useState('#ffffff');
  //   const [modalColor, setModalColor] = useState(false);

  const validate = yup.object().shape({
    priority_name: yup.string().required(t('common:required')),
    color: yup.string().required(t('common:required')),
  });

  const [dataPriority, loadingPriority, errorPriority, refetch] =
    useGetPriority();

  const onSubmit = async (value: any) => {
    value.priority_level = dataPriority.length + 1;
    setLoading(true);
    priorityService.createTodoTag(
      value,
      (err) => {
        let mess = err;
        if (err === 'UNKNOWN') {
          mess = t('common:unknownError');
        }
        Alert.alert(t('common:notice'), mess);
        setLoading(false);
      },
      () => {
        // onCloseModal();
        // refetchPriority();
        setLoading(false);
      },
    );
  };

  return (
    <AppContainer title={t('OKR:OKRs_Company')}>
      <Formik
        initialValues={{
          priority_name: '',
          color: '',
          user: user,
          priority_level: '',
        }}
        // validationSchema={validate}
        onSubmit={(value) => onSubmit(value)}
      >
        {({ values, handleSubmit, setFieldValue }) => {
          return (
            <View>
              <AppInputField
                name="priority_name"
                title={t('createMissions:priorityName')}
                placeholder={t('createMissions:priorityName')}
              />
              {/* <Text style>{codeColor}</Text> */}
              <AppInputField
                name="color"
                title={t('createMissions:priorityColor')}
                placeholder={t('createMissions:priorityColor')}
                customInputStyle={{ backgroundColor: `${codeColor}` }}
                editable={false}
              />
              <View style={{ height: 300 }}>
                <ColorPicker
                  color={codeColor}
                  thumbSize={40}
                  sliderSize={40}
                  noSnap={false}
                  row={false}
                  onColorChange={(elm: string) => {
                    setCodeColor(elm);
                    setFieldValue('color', elm);
                  }}
                  onColorChangeComplete={(elm: string) => {
                    setCodeColor(elm);
                    setFieldValue('color', elm);
                  }}
                  swatchesLast={true}
                  swatches={true}
                  discrete={false}
                  swatchesOnly={false}
                  shadeSliderThumb={false}
                  shadeWheelThumb={false}
                  autoResetSlider={false}
                />
              </View>
              <AppButton
                containerStyle={{ marginTop: theme.theme.spacing.p12 }}
                title={t('createMissions:save')}
                onPress={handleSubmit}
              />
            </View>
          );
        }}
      </Formik>
    </AppContainer>
  );
};

export default CreatePriority;
