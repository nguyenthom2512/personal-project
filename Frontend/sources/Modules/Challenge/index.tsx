import { useNavigation, useRoute } from '@react-navigation/core';
import isEmpty from 'lodash/isEmpty';
import moment from 'moment';
import React, { useContext, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  ScrollView,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useSelector } from 'react-redux';
import Menu from '../../Assets/Svg/menu.svg';
import Pen from '../../Assets/Svg/pen.svg';
import Trash from '../../Assets/Svg/trash.svg';
import { AppContainer, AppFlatlistEmpty, AppText } from '../../Components';
import { IndicatorContext } from '../../Context';
import useGetChallengeDetail from '../../Hooks/getChallengeDetail';
import commentService from '../../Services/commentService';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface Props {}

export default (props: Props) => {
  //! State
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { setLoading } = useContext(IndicatorContext);
  const navigate = useNavigation();
  const route = useRoute<any>();
  const { id } = route.params;
  const [cmt, setCmt] = useState('');
  const refFlatList: any = useRef<any>();
  const [show, setShow] = useState();
  const [showEdit, setShowEdit] = useState<string | number>();
  const [valueCmtEdit, setValueCmtEdit] = useState('');
  const ref_input = useRef<any>();

  //! Function
  const userData = useSelector((state: any) => state.userReducer);
  const userID = userData?.data?.user?.id;

  const [dataChallenge, loading, error, refetchChallenge] =
    useGetChallengeDetail(id);

  const sendComment = async () => {
    let data = {
      title: cmt,
      user: userID,
      todo: '',
      challenge: id,
    };
    setLoading(true);
    try {
      const response = await commentService.postCommentTodo(data);
      if (response?.error) {
        setLoading(false);
      } else {
        refetchChallenge();
        setCmt('');
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
    }
  };
  const handleEdit = async (id: string | number) => {
    let data = {
      id: id,
      user: userID,
      title: valueCmtEdit,
    };
    setLoading(true);
    try {
      const response = await commentService.editComment(id, data);

      if (response?.error) {
        setLoading(false);
      } else {
        refetchChallenge();
        setShowEdit('');
        setLoading(false);
      }
    } catch (error) {
      setLoading(false);
    }
  };
  const handleDelete = async (id: any) => {
    Alert.alert(`${t('common:notice')}`, `${t('common:confirmDeleteCmt')}`, [
      {
        text: `${t('common:ok')}`,
        onPress: async () => {
          setLoading(true);
          try {
            const response = await commentService.deleteComment(id);
            if (response?.error) {
              setLoading(false);
            } else {
              refetchChallenge();
              setCmt('');
              setLoading(false);
            }
          } catch (error) {
            setLoading(false);
          }
        },
      },
      {
        text: `${t('common:cancel')}`,
        onPress: () => {},
      },
    ]);
  };

  const renderIcon = (item: any, index: number) => {
    return (
      <View style={Styles.absolute}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            setShowEdit(item.id);
            setShow(undefined);
            setValueCmtEdit(item.title);
            setTimeout(
              () =>
                refFlatList.current.scrollToIndex({
                  animated: true,
                  index,
                  viewOffset: 0,
                  viewPosition: 0.5,
                }),
              300,
            );
          }}
          style={[
            Styles.viewWrapEdit,
            {
              borderBottomWidth: 1,
              borderColor: theme.color.mercury,
            },
          ]}
        >
          <Pen />
          <AppText style={{ paddingHorizontal: theme.spacing.p12 }}>
            {t('common:update')}
          </AppText>
        </TouchableOpacity>

        <TouchableOpacity
          style={Styles.viewWrapEdit}
          activeOpacity={0.8}
          onPress={() => {
            handleDelete(item.id);
            setShow(undefined);
          }}
        >
          <Trash />
          <AppText style={{ paddingHorizontal: theme.spacing.p12 }}>
            {t('common:delete')}
          </AppText>
        </TouchableOpacity>
      </View>
    );
  };

  const renderImage = (imgUrl: any) => {
    return (
      <View>
        <Image
          style={Styles.iconName}
          source={{
            uri: imgUrl
              ? `${serviceUrls.url.IMAGE}${imgUrl}`
              : 'https://huyhoanhotel.com/wp-content/uploads/2016/05/765-default-avatar.png',
          }}
        />
      </View>
    );
  };

  const itemComment = ({ item, index }: any) => {
    return (
      <TouchableWithoutFeedback
        onPress={() => {
          setShow(undefined);
          setShowEdit('');
        }}
      >
        <View
          style={{ flexDirection: 'row', marginTop: theme.spacing.p24 }}
          // onLayout={(e) => {
          //   let { x, y, width, height } = e.nativeEvent.layout;
          // }}
        >
          {renderImage(item.user_data.img_url)}
          <View style={{ flex: 1 }}>
            {showEdit === item.id ? (
              <View>
                <TextInput
                  style={[
                    Styles.input,
                    {
                      borderRadius: theme.spacing.p8,
                      paddingHorizontal: theme.spacing.p12,
                    },
                  ]}
                  value={valueCmtEdit}
                  onChangeText={setValueCmtEdit}
                  editable={true}
                  multiline
                  placeholderTextColor={theme.color.black}
                  autoFocus={true}
                />
                <View style={Styles.wrapBtnUpdateCancel}>
                  <TouchableOpacity
                    onPress={() => {
                      handleEdit(item.id);
                    }}
                    style={Styles.btnUpdate}
                  >
                    <AppText style={{ color: theme.color.white }}>
                      {t('calendar:update')}
                    </AppText>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      setShowEdit('');
                    }}
                    style={Styles.btnCancel}
                  >
                    <AppText>{t('calendar:cancel')}</AppText>
                  </TouchableOpacity>
                </View>
              </View>
            ) : (
              <View style={{ flex: 1 }}>
                <View style={Styles.wrapTxtCmt}>
                  <View style={{ flex: 1 }}>
                    <AppText style={{ fontWeight: '700' }}>
                      {item.user_data.full_name}
                    </AppText>
                    <AppText>{item.title}</AppText>
                  </View>
                  {item.user == userID && (
                    <View>
                      <TouchableOpacity
                        onPress={() => {
                          !show ? setShow(item.id) : setShow(undefined);
                          setShowEdit('');
                        }}
                      >
                        <Menu />
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
                <AppText style={{ color: theme.color.shark, opacity: 0.5 }}>
                  {moment(item.created_time).fromNow()}
                </AppText>
                {item.id == show && renderIcon(item, index)}
              </View>
            )}
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  const renderCommentInput = () => {
    return (
      <View style={Styles.commentWrapper}>
        <TextInput
          placeholder={`${t('createMissions:writeComment')}...`}
          style={Styles.input}
          value={cmt}
          onChangeText={setCmt}
          editable={true}
          multiline
          textAlignVertical="top"
          placeholderTextColor={theme.color.black}
          ref={ref_input}
        />
        {!isEmpty(cmt) && (
          <TouchableOpacity
            onPress={sendComment}
            style={{ alignSelf: 'flex-end', marginBottom: theme.spacing.p4 }}
          >
            <Ionicons
              name="send"
              size={theme.fontSize.f28}
              color={theme.color.violet}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  //! Render
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={{ flex: 1 }}
    >
      <AppContainer isBack title={t('common:challenge')}>
        {/* <SafeAreaView> */}
        {dataChallenge?.id ? (
          <View style={{ marginHorizontal: theme.spacing.p16, flex: 1 }}>
            <View
              style={{
                flexDirection: 'row',
                marginVertical: theme.fontSize.f16,
              }}
            >
              {renderImage(dataChallenge?.user_data?.img_url)}
              <View>
                <AppText style={{ flex: 1, fontWeight: '700' }}>
                  {dataChallenge?.user_data?.full_name}
                </AppText>

                <AppText>
                  {moment(dataChallenge?.created_time).fromNow()}
                </AppText>
              </View>
            </View>
            <AppText>Okr: {dataChallenge?.okr_name}</AppText>
            <AppText>{dataChallenge?.content}</AppText>
            <TouchableOpacity
              style={Styles.btnComment}
              onPress={() => {
                ref_input.current.focus();
                setShow(undefined);
                setShowEdit('');
              }}
            >
              <MaterialCommunityIcons
                name="comment-outline"
                size={theme.fontSize.f24}
                color={theme.color.mineShaft}
              />
              <AppText style={{ marginLeft: theme.spacing.p8 }}>
                {t('todoDetail:comment')}
              </AppText>
            </TouchableOpacity>
            <FlatList
              ref={refFlatList}
              keyExtractor={(item) => item.id.toString()}
              style={{ flex: 1 }}
              data={dataChallenge?.challenge_comment || []}
              renderItem={({ item, index }) => itemComment({ item, index })}
              ListEmptyComponent={
                <AppFlatlistEmpty message={t('common:noComments')} />
              }
              showsVerticalScrollIndicator={false}
            />
            {!showEdit && renderCommentInput()}
          </View>
        ) : (
          <View
            style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}
          >
            <AppText style={{ textAlign: 'center' }}>
              {t('common:noInformationFound')}
            </AppText>
          </View>
        )}
        {/* </SafeAreaView> */}
      </AppContainer>
    </KeyboardAvoidingView>
  );
};
