import { Platform, StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    iconName: {
      width: 48,
      height: 48,
      borderRadius: 24,
      marginRight: theme.spacing.p16,
    },
    btnComment: {
      flexDirection: 'row',
      padding: theme.spacing.p12,
      justifyContent: 'center',
      borderTopWidth: 2,
      borderBottomWidth: 2,
      borderColor: '#E3E3E3',
      marginTop: theme.spacing.p8,
    },
    input: {
      fontSize: theme.fontSize.f16,
      flex: 1,
      borderRadius: theme.spacing.p28,
      color: theme.color.black,
      marginRight: theme.spacing.p12,
      backgroundColor: theme.color.alto,
      padding: theme.spacing.p12,
      paddingTop: theme.spacing.p12,
    },
    wrapTxtCmt: {
      backgroundColor: theme.color.alto,
      borderRadius: theme.spacing.p8,
      padding: theme.spacing.p12,
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    absolute: {
      zIndex: 99,
      position: 'absolute',
      top: 0,
      right: theme.spacing.p6,
      borderRadius: 4,
      backgroundColor: theme.color.white,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
    },

    viewWrapEdit: {
      flexDirection: 'row',
      padding: theme.spacing.p8,
    },
    wrapBtnUpdateCancel: {
      flexDirection: 'row',
      justifyContent: 'space-around',
      marginTop: theme.spacing.p8,
    },
    btnUpdate: {
      paddingVertical: theme.spacing.p8,
      paddingHorizontal: theme.spacing.p16,
      backgroundColor: theme.color.violet,
      borderRadius: theme.spacing.p8,
    },
    btnCancel: {
      paddingVertical: theme.spacing.p8,
      paddingHorizontal: theme.spacing.p16,
      backgroundColor: theme.color.alto,
      borderRadius: theme.spacing.p8,
    },
    commentWrapper: {
      flexDirection: 'row',
      marginVertical: theme.spacing.p12,
      alignItems: 'center',
    },
  });
  return styles;
};
