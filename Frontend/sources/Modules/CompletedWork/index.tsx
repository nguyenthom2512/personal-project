import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import { useTranslation } from 'react-i18next';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  TouchableOpacity,
  View,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { useDispatch, useSelector } from 'react-redux';
import DateTime from '../../Assets/Svg/dateTime.svg';
import { AppContainer, AppText } from '../../Components';
import { IndicatorContext } from '../../Context';
import { HHmm } from '../../Helpers/dateTime';
import { TodoCreateBody } from '../../interfaces/todo.interface';
import { getCompletedWork } from '../../Redux/Actions/todoListActions';
import ActionTypes from '../../Redux/ActionTypes';
import { RootState } from '../../Redux/Reducers';
import completedWorkReducer from '../../Redux/Reducers/completedWorkReducer';
import todoServices from '../../Services/todoServices';
import { Theme, useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface Props {}
interface ItemProps extends TodoCreateBody {
  id?: number;
  // todo_name: string;
  // start_time: string;
  // end_time: string;
  // date: string;
  // confident: number;
}
let page = 1;
const page_size = 20;
let is_done = true;
const iconData = (theme: Theme, chose: any, id: number | undefined) => [
  {
    icon: (
      <Fontisto
        name="smiley"
        size={28}
        color={chose[`${id}`] === 3 ? theme.color.white : theme.color.gray}
      />
    ),
    value: 3,
  },
  {
    icon: (
      <Fontisto
        name="slightly-smile"
        size={28}
        color={chose[`${id}`] === 2 ? theme.color.white : theme.color.gray}
      />
    ),
    value: 2,
  },
  {
    icon: (
      <Entypo
        name="emoji-neutral"
        size={28}
        color={chose[`${id}`] === 1 ? theme.color.white : theme.color.gray}
      />
    ),
    value: 1,
  },
  {
    icon: (
      <Entypo
        name="emoji-sad"
        size={28}
        color={chose[`${id}`] === 0 ? theme.color.white : theme.color.gray}
      />
    ),
    value: 0,
  },
];

export default () => {
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const dispatch = useDispatch();
  const completedWork = useSelector(
    (state: RootState) => state.completedWorkReducer,
  );
  const [chooseIcon, setChooseIcon] = useState<any>({});
  const { setLoading } = useContext(IndicatorContext);
  const navigation = useNavigation();

  useEffect(() => {
    if (
      completedWork?.data?.results.length > 0 &&
      Object.keys(chooseIcon).length === 0
    ) {
      const chosenOne: any = {};
      completedWork?.data?.results.forEach((element: ItemProps) => {
        chosenOne[`${element.id}`] = element.confident;
      });
      setChooseIcon(chosenOne);
    }
  }, [completedWork?.type]);

  const onLoad = () => {
    if (completedWork.loading || page >= completedWork.noPage) {
      return;
    }
    page = page + 1;
    dispatch(getCompletedWork({ page, page_size, is_done }));
  };

  const onRefresh = () => {
    page = 1;
    dispatch(getCompletedWork({ page, page_size, is_done }));
  };

  useEffect(() => {
    dispatch(getCompletedWork({ page, page_size, is_done }));
  }, []);

  const onChooseIcon = (elm: any) => {
    setChooseIcon(elm);
  };

  const renderItems = (item: ItemProps) => {
    const { id, todo_name, start_time, date, end_time, confident } = item || {};

    return (
      <View style={Styles.itemContainerStyles}>
        <View style={Styles.itemStyles}>
          <AppText>{todo_name}</AppText>
          <View style={Styles.timeInfo}>
            <EvilIcons name="clock" size={28} color={theme.color.gray} />
            <AppText style={Styles.textTime}>
              {moment(start_time).isValid()
                ? `${moment(start_time).format(HHmm)}-${moment(end_time).format(
                    HHmm,
                  )}`
                : t('todoDetail:noTime')}
            </AppText>
            <DateTime />
            <AppText style={Styles.textDate}>
              {date ?? t('todoDetail:noTime')}
            </AppText>
          </View>
        </View>
        <View style={Styles.iconWrapper}>
          {iconData(theme, chooseIcon, id)?.map((el, index) => {
            console.log();

            return (
              <TouchableOpacity
                key={el.value.toString()}
                style={[
                  Styles.iconContainerStyles,
                  { borderLeftWidth: index === 0 ? 0 : 1 },
                  { borderRightWidth: index === 3 ? 0 : 1 },
                  {
                    backgroundColor:
                      chooseIcon[`${id}`] === el.value
                        ? theme.color.violet
                        : theme.color.white,
                  },
                ]}
                onPress={() => {
                  const chosenOne = { ...chooseIcon };
                  if (chosenOne?.[`${id}`] !== el.value) {
                    // setLoading(true);
                    setChooseIcon(chosenOne);
                    id &&
                      todoServices.updateTodoConfident(
                        id,
                        { confident: el.value },
                        (err) => {
                          setLoading(false);
                          Alert.alert(
                            t('common:notice'),
                            err || t('todoDetail:updateError'),
                          );
                        },
                        () => {
                          setLoading(false);
                          chosenOne[`${id}`] = el.value;
                          dispatch(
                            getCompletedWork({ page, page_size, is_done }),
                          );
                          //   Alert.alert(
                          //     t('common:notice'),
                          //     t('todoDetail:updateSuccess'),
                          //     [
                          //       {
                          //         text: 'OK',
                          //         onPress: () => {
                          //           // dispatch(getCompletedWork({ page, page_size, is_done }));
                          //           // refreshTodo();
                          //           // navigation.goBack();
                          //         },
                          //       },
                          //     ],
                          //   );
                        },
                      );
                  }
                }}
              >
                {el.icon}
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  };

  return (
    <AppContainer title={t('homepage:doneJob')} isBack>
      <View style={Styles.containerStyles}>
        <FlatList
          data={completedWork?.data?.results}
          keyExtractor={(item) => item?.id?.toString() || ''}
          renderItem={({ item, index }) => {
            return renderItems(item);
          }}
          showsVerticalScrollIndicator={false}
          onEndReached={onLoad}
          onRefresh={onRefresh}
          refreshing={
            page === 1 &&
            completedWork.type === ActionTypes.NO_DATE_TODO_REQUEST
          }
          ListFooterComponent={
            page < completedWork.noPage ? <ActivityIndicator /> : null
          }
        />
      </View>
    </AppContainer>
  );
};
