import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    itemContainerStyles: {
      backgroundColor: theme.color.white,
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.1,
      shadowRadius: 1.41,

      elevation: 2,
      borderRadius: theme.spacing.p4,
      marginBottom: theme.spacing.p16,
    },
    containerStyles: {
      paddingHorizontal: theme.spacing.p16,
      marginTop: theme.spacing.p16,
      // paddingVertical: theme.spacing.p16,
    },
    timeInfo: {
      flexDirection: 'row',
      marginTop: theme.spacing.p12,
      alignItems: 'center',
    },
    textTime: {
      color: theme.color.mineShaft,
      marginRight: theme.spacing.p16,
    },
    textDate: {
      color: theme.color.mineShaft,
      marginLeft: theme.spacing.p4,
    },
    iconContainerStyles: {
      flex: 1,
      borderWidth: 2,
      borderColor: theme.color.alto,
      padding: theme.spacing.p12,
      borderBottomWidth: 0,
      alignItems: 'center',
    },
    iconWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    itemStyles: {
      padding: theme.spacing.p16,
    },
  });
  return styles;
};
