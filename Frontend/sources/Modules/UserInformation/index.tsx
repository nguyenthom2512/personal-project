import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Modal,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { AppButton, AppContainer, AppImage, AppText } from '../../Components';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import Email from '../../Assets/Svg/email.svg';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/core';
import { userActions } from '../../Redux/Actions';
import Bell from '../../Assets/Svg/bell.svg';
import Folder from '../../Assets/Svg/folder.svg';
import Fontsize from '../../Assets/Svg/fontsize.svg';
import Private from '../../Assets/Svg/moon.svg';
import LockFull from '../../Assets/Svg/lock_2.svg';
import Ionicons from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import { DDMMYYYY } from '../../Helpers/dateTime';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { useState } from 'react';
import { BottomSheet, ListItem } from 'react-native-elements';

interface Props {}
export default (props: Props) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { i18n, t } = useTranslation();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const [showModal, setShowModal] = useState(false);
  console.log('showModal', showModal);

  const userData = useSelector((state: any) => state.userReducer.data.user);
  const listIcon = [
    // {
    //   icon: <Email />,
    //   key: 'email',
    //   label: t('login:email'),
    // },
    {
      icon: <Bell />,
      key: 'notification',
      label: t('common:notice'),
    },
    // {
    //   icon: <LockFull />,
    //   key: 'private',
    //   label: t('common:private'),
    // },
    // {
    //   icon: <Private />,
    //   key: 'displayMode',
    //   label: t('common:displayMode'),
    // },
    // {
    //   icon: <Fontsize />,
    //   key: 'fontSize',
    //   label: t('common:fontSizeText'),
    // },
    {
      icon: <Folder />,
      key: 'termsOfUse',
      label: t('common:termsOfUse'),
    },
    {
      icon: <Ionicons name="list" size={26} color={theme.color.gray} />,
      key: 'listGift',
      label: t('common:listGift'),
    },
    {
      icon: (
        <MaterialIcons name="language" size={26} color={theme.color.gray} />
      ),
      key: 'language',
      label: t('common:language'),
    },
  ];

  const pressAction = (key: string) => {
    switch (key) {
      case 'notification':
        return navigation.navigate('Notification');
      case 'listGift':
        return navigation.navigate('ListGift');
      case 'language':
        return setShowModal(!showModal);
      default:
        break;
    }
  };

  const listLanguage = [
    {
      title: t('common:vietnamese'),
      code: 'vi',
      onPress: () => changeLanguage('vi'),
    },
    {
      title: t('common:english'),
      code: 'en',
      onPress: () => changeLanguage('en'),
    },
    {
      title: 'Cancel',
      containerStyle: { backgroundColor: theme.color.violet },
      titleStyle: { color: theme.color.white },
      onPress: () => setShowModal(false),
    },
  ];

  const changeLanguage = (code: string) => {
    i18n.changeLanguage(code);
    setShowModal(false)
  };

  return (
    <AppContainer title={t('account:account')} isBack>
      <TouchableOpacity
        style={{
          backgroundColor: theme.color.backgroundColor,
          padding: theme.spacing.p16,
          flexDirection: 'row',
          alignItems: 'center',
        }}
        onPress={() => {
          navigation.navigate('UpdateProFile');
        }}
      >
        {userData?.img_url ? (
          <AppImage
            imageStyle={Styles.iconName}
            source={{ uri: `${serviceUrls.url.IMAGE}${userData?.img_url}` }}
          />
        ) : (
          <AppImage
            imageStyle={Styles.iconName}
            source={require('../../Assets/anhDaiDien.jpg')}
          />
        )}
        <View style={{ flex: 1 }}>
          <AppText style={Styles.fullName}>{userData?.full_name}</AppText>
          <AppText style={Styles.email}>{userData?.email}</AppText>
          <View
            style={[
              Styles.package,
              userData?.account_type === 'normal'
                ? {}
                : {
                    backgroundColor: theme.color.violet,
                  },
            ]}
          >
            <AppText
              style={{
                color:
                  userData?.account_type === 'normal'
                    ? theme.color.shark
                    : theme.color.white,
              }}
            >
              {userData?.account_type === 'normal'
                ? t('common:normal')
                : t('common:primary', {
                    time: moment(userData?.expired).format(DDMMYYYY),
                  })}
            </AppText>
          </View>
        </View>
      </TouchableOpacity>
      <ScrollView contentContainerStyle={{ flex: 1 }}>
        <View style={{ padding: 16 }}>
          {listIcon.map((el, index) => (
            <TouchableOpacity
              key={index}
              style={Styles.wrapItem}
              onPress={() => pressAction(el.key)}
            >
              {el.icon}
              <AppText style={Styles.txtItem}> {el.label}</AppText>
              <Feather
                name="chevron-right"
                size={28}
                color={theme.color.violet}
              />
            </TouchableOpacity>
          ))}
        </View>
      </ScrollView>
      <View>
        <AppButton
          title={t('common:logout')}
          onPress={() => dispatch(userActions.logout())}
          style={Styles.buttonContainer}
        />
      </View>
      <BottomSheet
        isVisible={showModal}
        containerStyle={{ backgroundColor: 'rgba(0.5, 0.25, 0, 0.2)' }}
      >
        {listLanguage.map((l, i) => {
          console.log('wwwwwwwww', i);

          return (
            <ListItem
              key={i}
              containerStyle={l.containerStyle}
              onPress={l.onPress}
            >
              <ListItem.Content>
                <ListItem.Title style={l.titleStyle}>{l.title}</ListItem.Title>
              </ListItem.Content>
            </ListItem>
          );
        })}
      </BottomSheet>
    </AppContainer>
  );
};
