import { StyleSheet } from 'react-native';
import { fontSize } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    iconName: {
      width: 48,
      height: 48,
      borderRadius: 24,
      marginRight: theme.spacing.p16,
    },
    wrapItem: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      padding: theme.spacing.p16,
      backgroundColor: theme.color.backgroundColor,
      borderRadius: theme.spacing.p4,
      marginBottom: theme.spacing.p16,
    },
    txtItem: {
      flex: 1,
      paddingLeft: theme.spacing.p12,
      fontSize: theme.fontSize.f16,
      color: theme.color.shark,
      fontWeight: '400',
    },
    userWrap: {
      backgroundColor: theme.color.backgroundColor,
      padding: theme.spacing.p16,
      flexDirection: 'row',
      alignItems: 'center',
    },
    fullName: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
    email: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.gray,
      marginTop: theme.spacing.p4,
    },
    buttonContainer: {
      marginVertical: theme.spacing.p16,
      paddingHorizontal: theme.spacing.p16,
    },
    package: {
      marginTop: theme.spacing.p4,
      paddingHorizontal: theme.spacing.p12,
      paddingVertical: theme.spacing.p4,
      backgroundColor: theme.color.lightGray,
      borderRadius: theme.spacing.p4,
    },
  });
