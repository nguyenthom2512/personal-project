import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, Image, ScrollView, TouchableOpacity, View } from 'react-native';
import {
  Asset,
  ImageLibraryOptions,
  ImagePickerResponse,
  launchImageLibrary,
} from 'react-native-image-picker';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import Camera from '../../../Assets/Svg/camera.svg';
import {
  AppButton,
  AppContainer,
  AppDropDown,
  AppInputField,
  AppText,
  ErrorMessages,
} from '../../../Components';
import { IndicatorContext } from '../../../Context';
import { padding } from '../../../Helpers';
import { createFormImage } from '../../../Helpers/convertData';
import useGetCompany from '../../../Hooks/getCompany';
import useGetDepartment from '../../../Hooks/getDepartment';
import { userActions } from '../../../Redux/Actions';
import userReducer from '../../../Redux/Reducers/userReducer';
import { uploadImage } from '../../../Services/serviceHandle';
import serviceUrls from '../../../Services/serviceUrls';
import userService from '../../../Services/userService';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './styles';

interface Props {}
const configImagePicker: ImageLibraryOptions = {
  selectionLimit: 1,
  mediaType: 'photo',
};
export default (props: Props) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { setLoading } = useContext(IndicatorContext);
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userData = useSelector((state: any) => state.userReducer.data.user);

  const [dataDepartment] = useGetDepartment();
  const [dataCompany] = useGetCompany();

  const [image, setImage] = useState('');
  useEffect(() => {
    setImage(userData?.img_url);
  }, [userData]);

  const department = (dataDepartment || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.department_name,
    };
  });
  const company = (dataCompany || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.company_name,
    };
  });
  const dataGender = [
    {
      value: 0,
      label: 'Nữ',
    },
    {
      value: 1,
      label: 'Nam',
    },
    {
      value: 2,
      label: 'Không xác định',
    },
  ];
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const showDatePicker = () => {
    setDatePickerVisibility(!isDatePickerVisible);
  };
  const handleConfirm = (date: any, setFieldValue: any) => {
    const selected = moment(date).toISOString();
    setFieldValue('date_of_birth', selected);
    showDatePicker();
  };
  const validate = Yup.object().shape({
    full_name: Yup.string().required(t('common:required')),
    gender: Yup.string().required(t('common:required')),
    date_of_birth: Yup.string().required(t('common:required')),
    phone_number: Yup.string().required(t('common:required')),
    // company: Yup.string().required(t('common:required')),
  });

  const onSubmit = async (data: any) => {
    setLoading(true);
    if (data && data.date_of_birth) {
      data.date_of_birth = moment(data.date_of_birth).format('YYYY-MM-DD');
    }
    delete data.email;
    try {
      const response = await userService.updateUser(data);
      setLoading(false);
      if (response.error) {
        const messageError =
          response.detail[Object.keys(response.detail)[0]][0] ??
          'Oops! something went wrong';
        Alert.alert(`${t('common:notice')}`, messageError);
      } else {
        Alert.alert(`${t('common:notice')}`, `${t('common:saveSuccess')}`, [
          {
            text: `${t('common:ok')}`,
            onPress: () => {
              dispatch(userActions.getUserRequest(data.id));
              navigation.goBack();
            },
          },
          //   {
          //     text: t('common:tryAgain'),
          //     onPress: () => onSubmit(data),
          //   },
        ]);
      }
    } catch (error) {
      console.log('catchError', error);
      setLoading(false);
    }
  };

  return (
    <AppContainer title={t('common:personalInfor')} isBack>
      <Formik
        initialValues={{
          id: userData?.id,
          img_url: userData?.img_url,
          full_name: userData?.full_name,
          email: userData?.email,
          gender: userData?.gender,
          date_of_birth: userData?.date_of_birth || '',
          phone_number: userData?.phone_number,
          company: userData?.company,
          department: userData?.department,
        }}
        validationSchema={validate}
        onSubmit={onSubmit}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          setFieldValue,
          setValues,
          errors,
        }) => {
          return (
            <ScrollView>
              <View
                style={{
                  paddingHorizontal: theme.spacing.p16,
                  backgroundColor: theme.color.lightGray,
                }}
              >
                <View
                  style={{
                    alignItems: 'center',
                    paddingVertical: theme.spacing.p32,
                  }}
                >
                  <View>
                    {userData?.img_url ? (
                      <Image
                        style={Styles.iconName}
                        source={{
                          uri: `${serviceUrls.url.IMAGE}${image}`,
                        }}
                      />
                    ) : (
                      <Image
                        style={Styles.iconName}
                        source={require('../../../Assets/anhDaiDien.jpg')}
                      />
                    )}
                    <View style={{ position: 'absolute', bottom: 0, right: 0 }}>
                      <TouchableOpacity
                        onPress={() => {
                          setLoading(true);
                          launchImageLibrary(
                            configImagePicker,
                            (res: ImagePickerResponse) => {
                              if (res.didCancel) {
                                setLoading(false);
                                return;
                              }
                              if (res.assets) {
                                const item: Asset = res?.assets[0];
                                uploadImage(createFormImage(item))
                                  .then((res) => {
                                    if (res.error) {
                                      return Alert.alert(
                                        t('common:notice'),
                                        res.errorMessage,
                                      );
                                    }
                                    setImage(res.response.file);
                                    setFieldValue('img_url', res.response.file);
                                  })
                                  .catch((er) => {
                                    Alert.alert(t('common:notice'), 'UNKNOWN');
                                  })
                                  .finally(() => {
                                    setLoading(false);
                                  });
                              }
                            },
                          );
                        }}
                      >
                        <Camera />
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
                <AppInputField
                  title={t('common:fullName')}
                  placeholder={t('common:fullName')}
                  name="full_name"
                />
                <AppInputField
                  title={t('common:emailAddress')}
                  placeholder={t('common:emailAddress')}
                  name="email"
                  editable={false}
                />
                <View>
                  <AppDropDown
                    title={t('common:gender')}
                    placeHolder={t('common:gender')}
                    value={values?.gender?.toString()}
                    isSearch
                    onChoose={(value) => {
                      setFieldValue('gender', value.value);
                    }}
                    data={dataGender}
                  />
                  <ErrorMessages name="gender" />
                </View>
                <View style={{ flex: 1 }}>
                  <AppText style={Styles.titleDob}>
                    {t('createOkr:OkrKR:dob')}
                  </AppText>
                  <TouchableOpacity
                    style={Styles.inputDatetime}
                    onPress={() => showDatePicker()}
                  >
                    <AppText style={Styles.textInputDob}>
                      {values.date_of_birth ? (
                        moment(values.date_of_birth).format('DD/MM/YYYY')
                      ) : (
                        <AppText
                          style={[
                            Styles.textInputDob,
                            { color: 'rgba(38, 39, 46, 0.5)' },
                          ]}
                        >{`${t('createOkr:OkrKR:dob')}`}</AppText>
                      )}
                    </AppText>
                  </TouchableOpacity>
                  <ErrorMessages name="date_of_birth" />
                  <DateTimePickerModal
                    locale="vi"
                    isVisible={Boolean(isDatePickerVisible)}
                    mode="date"
                    onConfirm={(date) => handleConfirm(date, setFieldValue)}
                    onCancel={showDatePicker}
                    maximumDate={new Date()}
                  />
                </View>
                <AppInputField
                  title={t('common:numberPhone')}
                  placeholder={t('common:numberPhone')}
                  name="phone_number"
                  editable={true}
                  keyboardType="numeric"
                />
                {userData.account_type !== 'normal' && (
                  <>
                    <AppDropDown
                      title={t('common:company')}
                      placeHolder={`${t('common:select')} ${t(
                        'common:company',
                      )}`}
                      value={values.company}
                      isSearch
                      onChoose={(value) => {
                        setFieldValue('company', value.value);
                      }}
                      data={company}
                    />
                    <AppDropDown
                      title={t('OKR:department')}
                      placeHolder={t('common:chooseDepartment')}
                      value={values.department}
                      isSearch
                      onChoose={(value) => {
                        setFieldValue('department', value.value);
                      }}
                      data={department}
                    />
                  </>
                )}
                <AppButton
                  containerStyle={{ marginTop: padding.p8 }}
                  onPress={() => handleSubmit()}
                  title={t('common:save')}
                />
              </View>
            </ScrollView>
          );
        }}
      </Formik>
    </AppContainer>
  );
};
