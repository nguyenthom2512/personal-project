import { StyleSheet } from 'react-native';
import { fontSize, responsivePixel } from '../../../Helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    iconName: { width: 80, height: 80, borderRadius: 40 },

    inputDatetime: {
      borderWidth: 1,
      borderColor: theme.color.holderPlace,
      backgroundColor: theme.color.backgroundColor,
      height: 48,
      justifyContent: 'center',
      borderRadius: 4,
      marginVertical: theme.spacing.p6,
      paddingVertical: theme.spacing.p15,
    },
    titleDob: {
      color: theme.color.shark,
      fontWeight: '400',
      fontSize: theme.fontSize.f14,
      paddingTop: theme.spacing.p18,
      paddingBottom: theme.spacing.p2,
    },
    textInputDob: {
      height: responsivePixel(48),
      paddingHorizontal: theme.spacing.p12,
      paddingVertical: theme.spacing.p15,
      color: theme.color.shark,
      fontSize: fontSize.f14,
      fontWeight: '400',
    },
  });
  return styles;
};
