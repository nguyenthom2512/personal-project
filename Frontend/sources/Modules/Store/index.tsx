import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { AppContainer } from '../../Components';
import { useThemeAwareObject } from '../../Theme';
import ItemStore from '../ItemStore';
import styles from './styles';

interface Props {}

export default () => {
  const dispatch = useDispatch();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  return <ItemStore number={30} />
};
