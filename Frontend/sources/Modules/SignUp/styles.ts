import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    safeviewStyles: {
      flex: 1,
      backgroundColor: theme.color.white,
    },
    header: {
      fontSize: theme.fontSize.f24,
      fontWeight: '700',
      marginVertical: theme.spacing.p24,
    },
    card: {
      flex: 1,
      backgroundColor: theme.color.white,
      paddingVertical: theme.spacing.p24,
      paddingHorizontal: theme.spacing.p20,
    },
    line: {
      flex: 1,
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    txt: {
      textAlign: 'center',
      fontSize: theme.fontSize.f16,
      color: theme.color.shark,
      opacity: 0.5,
    },
    wrapIcon: {
      backgroundColor: '#EEEEEE',
      height: 40,
      width: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    wrapIconSignUp: {
      flexDirection: 'row',
      marginTop: theme.spacing.p24,
      justifyContent: 'center',
    },
    iconSignUp: {
      padding: theme.spacing.p12,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: '#E1E1E1',
      marginRight: theme.spacing.p32,
    },
    iconApple: {
      padding: theme.spacing.p12,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: '#E1E1E1',
    },
    margintop8: { marginTop: theme.spacing.p8 },
  });
  return styles;
};
