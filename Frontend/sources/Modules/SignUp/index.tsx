import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import React, {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, ScrollView, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { AppButton, AppInputField, AppText } from '../../Components';
import { IndicatorContext } from '../../Context';
import { userActions } from '../../Redux/Actions';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import messaging from '@react-native-firebase/messaging';
import { requestUserPermission } from '../../Helpers/requestPermission';
import Feather from 'react-native-vector-icons/Feather';
import signUpServive from '../../Services/signUpServive';
import Mail from '../../Assets/Svg/mail.svg';
import Lock from '../../Assets/Svg/lock.svg';
import Profile from '../../Assets/Svg/profile.svg';
import Google from '../../Assets/Svg/google.svg';
import Facebook from '../../Assets/Svg/facebook.svg';
import Apple from '../../Assets/Svg/apple.svg';
import AppInputFieldIcon from '../../Components/AppInputFieldIcon';
import { G } from 'react-native-svg';
import {
  LoginButton,
  AccessToken,
  GraphRequest,
  GraphRequestManager,
  LoginManager,
} from 'react-native-fbsdk-next';
import socialSignin from '../../Hooks/socialSignin';
import { LOGIN_SOCIAL_TYPE } from '../../Helpers/constant';
import { SafeAreaView } from 'react-native-safe-area-context';
interface Props {}

export default () => {
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { theme } = useTheme();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const indicatorContext = useContext(IndicatorContext);
  const { facebook, google } = socialSignin();

  const [deviceToken, setDeviceToken] = useState<String>('');

  const validateLoginSchema = Yup.object().shape({
    full_name: Yup.string().trim().required(t('common:required')),
    email: Yup.string()
      .email(t('login:invalidEmail'))
      .required(t('common:required')),
    password: Yup.string()
      .required(t('common:required'))
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,128}$/,
        'Mật khẩu cần ít nhất chứa 8 ký tự, một chữ hoa, một chữ thường, một số và một ký tự đặc biệt',
      ),
    passwordConfirmation: Yup.string()
      .required(t('common:required'))
      .oneOf(
        [Yup.ref('password')],
        'Mật khẩu đã nhập không khớp. Hãy thử lại. ',
      ),
  });

  const onLogin = async (values: object) => {
    indicatorContext.setLoading(true);
    const response = await signUpServive.postSignUp(values);
    if (response.error) {
      indicatorContext.setLoading(false);
      Alert.alert(t('common:notice'), t('login:emailAlreadyExists'));
    } else {
      indicatorContext.setLoading(false);
      Alert.alert(t('common:notice'), t('login:checkMail'), [
        {
          text: t('common:ok'),
          onPress: () => {
            navigation.goBack();
          },
        },
      ]);
    }
  };

  const handleError = (error: any) => {
    indicatorContext.setLoading(false);
    if (error === 'Unable to login with provided credentials.') {
      Alert.alert(t('common:notice'), t('login:wrongPassword'));
    } else if (error == 'NETWORK_ERROR') {
      Alert.alert(t('common:notice'), t('login:networkError'));
    } else {
      Alert.alert(t('common:notice'), t('login:loginFailed'));
    }
  };

  useEffect(() => {
    requestUserPermission().then((res) => {
      messaging()
        .getToken()
        .then((token) => {
          setDeviceToken(token);
        })
        .catch((err) => {
          __DEV__ && console.log('err', err);
        });
    });
  }, []);

  const onLoginFacebook = () => {
    indicatorContext.setLoading(true);
    facebook().then(
      (res) => {
        dispatch(
          userActions.loginSocialRequest(
            {
              access_token: res.accessToken,
              login_type: LOGIN_SOCIAL_TYPE.FACEBOOK,
              device_id: deviceToken,
            },
            {
              onSuccess: () => indicatorContext.setLoading(false),
              onError: handleError,
            },
          ),
        );
      },
      (rej) => {
        indicatorContext.setLoading(false);
      },
    );
  };

  const onLoginGoogle = () => {
    indicatorContext.setLoading(true);
    google().then(
      (res) => {
        dispatch(
          userActions.loginSocialRequest(
            {
              access_token: res.accessToken,
              login_type: LOGIN_SOCIAL_TYPE.GOOGLE,
              device_id: deviceToken,
            },
            {
              onSuccess: () => indicatorContext.setLoading(false),
              onError: handleError,
            },
          ),
        );
      },
      (rej) => {
        indicatorContext.setLoading(false);
      },
    );
  };

  return (
    <Formik
      validateOnBlur={false}
      validationSchema={validateLoginSchema}
      initialValues={{
        email: '',
        full_name: '',
        password: '',
        passwordConfirmation: '',
      }}
      onSubmit={onLogin}
    >
      {({ handleSubmit }) => (
        <SafeAreaView style={Styles.safeviewStyles}>
          <ScrollView>
            <View style={Styles.card}>
              <TouchableOpacity
                style={Styles.wrapIcon}
                onPress={() => {
                  navigation.goBack();
                }}
              >
                <Feather
                  name="x"
                  size={theme.fontSize.f16}
                  color={theme.color.grayIcon}
                />
              </TouchableOpacity>
              <AppText style={Styles.header}>
                {t('login:signUpAccount')}
              </AppText>
              <View>
                <AppInputFieldIcon
                  name="email"
                  placeholder={t('login:emailPlaceholder')}
                  memo
                  containerStyle={Styles.margintop8}
                  icon={<Mail />}
                />
                <AppInputFieldIcon
                  name="full_name"
                  placeholder={t('login:yourName')}
                  memo
                  containerStyle={Styles.margintop8}
                  icon={<Profile />}
                />
                <AppInputFieldIcon
                  secureTextEntry
                  name="password"
                  placeholder={t('login:passwordPlaceholder')}
                  memo
                  containerStyle={Styles.margintop8}
                  icon={<Lock />}
                />
                <AppInputFieldIcon
                  secureTextEntry
                  name="passwordConfirmation"
                  placeholder={t('login:passwordConfirmation')}
                  memo
                  containerStyle={Styles.margintop8}
                  icon={<Lock />}
                />
                <AppButton
                  title={t('login:createAccount')}
                  onPress={handleSubmit}
                  style={{ marginVertical: theme.spacing.p12 }}
                />
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <View style={Styles.line} />
                  <AppText
                    style={[
                      Styles.txt,
                      { marginHorizontal: theme.spacing.p16 },
                    ]}
                  >
                    {t('common:or')}
                  </AppText>
                  <View style={Styles.line} />
                </View>
                <View style={Styles.wrapIconSignUp}>
                  <TouchableOpacity onPress={onLoginGoogle}>
                    <View style={Styles.iconSignUp}>
                      <Google />
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity onPress={onLoginFacebook}>
                    <View style={Styles.iconSignUp}>
                      <Facebook />
                    </View>
                  </TouchableOpacity>
                  <View style={Styles.iconApple}>
                    <Apple />
                  </View>
                </View>
                <AppText style={[Styles.txt, { marginTop: 76 }]}>
                  {t('login:youAlreadyHaveAnAccount')}
                </AppText>
                <AppButton
                  title={t('login:login')}
                  onPress={() => {
                    navigation.goBack();
                  }}
                  style={{ marginVertical: theme.spacing.p8 }}
                  cancel
                />
              </View>
            </View>
          </ScrollView>
        </SafeAreaView>
      )}
    </Formik>
  );
};
