import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'red',
  },
  checkboxContainer: { flexDirection: 'row', justifyContent: 'space-evenly' },
  customInputStyle: { backgroundColor: 'green' },
  customInputStyle1: { backgroundColor: 'blue' },
});
