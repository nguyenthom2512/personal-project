/* eslint-disable react-hooks/exhaustive-deps */
import { Formik } from 'formik';
import React, { useCallback, useState } from 'react';
import { Alert, FlatList, Text, View } from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import {
  AppButton,
  AppContainer,
  AppDropDown,
  AppInputField,
  AppText,
} from '../../Components';
import AppCheckboxField from '../../Components/AppCheckboxField';
import AppModal from '../../Components/AppModal';
import LanguageSelector from '../../Components/LanguageSelector';
import Radio from '../../Components/RadioButton/Radio';
import RadioWrapper from '../../Components/RadioButton/RadioWrapper';

import { userActions } from '../../Redux/Actions';
import styles from './styles';
import { useTranslation } from 'react-i18next';
import { LinearProgress } from 'react-native-elements';
import { ChangeTheme } from '../../Components/ChangeTheme';

const validateLoginSchema = Yup.object().shape({
  email: Yup.string()
    .email('abc email sai r')
    .required('Must be required field'),
  userName: Yup.string()
    .length(15, 'Tên đăng nhập')
    .required('Must be required field'),
});

interface Props {}

export default () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const onSignOut = useCallback(() => {
    dispatch(userActions.logout());
  }, []);

  const onSubmit = (e: any) => {};

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <AppContainer
      isMenu
      iconRight="menu"
      title="Template"
      onRightPress={() => Alert.alert('right press')}
      iconRightAdd="search"
    >
      <ChangeTheme />
      <AppText>Template</AppText>
      <LanguageSelector />
      <AppText>
        <Text style={{ height: 32, backgroundColor: 'red' }}>
          1234512345678s
        </Text>
        <Text>{t('common:yes')}</Text>
      </AppText>
      <AppText children={<Text>12345ádfghj</Text>} />
      <LinearProgress color="primary" value={1} variant="indeterminate" />
      <Radio name="ssss" disabled={true} checked={true} />
      <AppDropDown
        value="dropdown1"
        isSearch
        title="dropdown"
        titleDropDown="dropdown"
        data={[
          { value: 'dropdown1', label: 'dropdown 1' },
          { value: 'dropdown2', label: 'dropdown 2' },
          { value: 'dropdown3', label: 'dropdown 3' },
          { value: 'dropdown4', label: 'dropdown 4' },
        ]}
      />
      <Formik
        validateOnBlur={false}
        validateOnChange={false}
        validationSchema={validateLoginSchema}
        initialValues={{ email: '', userName: '', checkbox: true }}
        onSubmit={onSubmit}
      >
        {({ handleSubmit }) => (
          <View>
            {/* <Field type="checkbox" name="toggle" /> */}
            <AppInputField
              name="password"
              placeholder="Email input"
              customInputStyle={styles.customInputStyle}
            />
            <AppInputField
              name="email"
              placeholder="Email input"
              customInputStyle={styles.customInputStyle1}
            />

            <View style={styles.checkboxContainer}>
              <AppCheckboxField name="checkbox3" title="checkbox 3" />
            </View>
            <AppButton title={'Submit'} onPress={handleSubmit} cancel />
          </View>
        )}
      </Formik>
      <View style={{ height: 10 }}></View>
      <AppButton title={'Đăng xuất'} onPress={onSignOut} />
      <Radio name="abc" />
      <RadioWrapper children={<Text>abc</Text>} name="a" />
      <AppButton title="On Modal" onPress={toggleModal} />
      <AppModal
        visible={isModalVisible}
        onToggleModal={toggleModal}
        dropPress={toggleModal}
      >
        <View>
          <AppCheckboxField
            name="checkbox3"
            title="checkbox 3"
            onPress={() => setModalVisible(false)}
          />
          <AppCheckboxField name="checkbox3" title="checkbox 3" />
          <AppButton title="On Modal" onPress={toggleModal} />
        </View>
        {/* )} */}
      </AppModal>
      <View style={{ height: 10 }}></View>
      <AppButton title={'Đăng xuất'} onPress={onSignOut} />
      <Radio name="abc" />
      <RadioWrapper children={<Text>abc</Text>} name="a" />
      <AppButton title="On Modal" onPress={toggleModal} />
      <AppModal
        visible={isModalVisible}
        onToggleModal={toggleModal}
        dropPress={toggleModal}
      >
        <View>
          <AppCheckboxField
            name="checkbox3"
            title="checkbox 3"
            onPress={() => setModalVisible(false)}
          />
          <AppCheckboxField name="checkbox3" title="checkbox 3" />
          <AppButton title="On Modal" onPress={toggleModal} />
        </View>
      </AppModal>
    </AppContainer>
  );
};
