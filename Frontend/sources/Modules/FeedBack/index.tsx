import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, SafeAreaView, ScrollView, View } from 'react-native';
import {
  AppButton,
  AppContainer,
  AppDropDown,
  AppInputField,
  AppText,
  ErrorMessages,
} from '../../Components';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import { useRef } from 'react';
import { ErrorMessage, Formik } from 'formik';
import { useSelector } from 'react-redux';
import { RootState } from '../../Redux/Reducers';
import { device } from '../../Helpers';
import feedbackService from '../../Services/feedbackService';
import { IndicatorContext } from '../../Context';
import * as Yup from 'yup';
import { useNavigation, useRoute } from '@react-navigation/native';
import useGetCriteriaFeedBack from '../../Hooks/useGetCriteraFeedBack';
import { createDataSelect } from '../../Helpers/convertData';

interface Props {}

const data = [
  {
    value: 1,
    label: 'SDS',
  },
];

export default () => {
  const { theme } = useTheme();
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);

  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const { setLoading } = useContext(IndicatorContext);

  const validationSchema = Yup.object().shape({
    content: Yup.string().required(t('common:required')),
  });

  const route = useRoute();
  const dataFeedback: any = route.params;
  const navigation = useNavigation();

  const [options] = useGetCriteriaFeedBack();
  const criteriaData = createDataSelect(options, 'title_rate');

  const onSubmit = (values: any) => {
    setLoading(true);
    feedbackService.createFeedback(
      { ...values, content: `<p>${values.content}</p>` },
      (err) => {
        setLoading(false);
        Alert.alert(t('common:notice'), err);
      },
      () => {
        setLoading(false);
        Alert.alert(t('common:notice'), t('OKR:feedbackSuccess'));
        navigation.goBack();
      },
    );
  };

  return (
    <Formik
      initialValues={{
        content: '',
        criteria: '',
        okr: dataFeedback?.id,
        user_received: dataFeedback?.user,
        user,
      }}
      onSubmit={onSubmit}
      enableReinitialize
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={validationSchema}
    >
      {({ values, setFieldValue, errors, setValues, handleSubmit }) => {
        return (
          <AppContainer isBack title={t('OKR:feedback')}>
            <ScrollView
              keyboardDismissMode="interactive"
              style={Styles.container}
            >
              <AppText style={Styles.text}>{t('OKR:criteria')}</AppText>
              <AppDropDown
                name="criteria"
                placeHolder={t('OKR:criteria')}
                onChoose={(e) => setFieldValue('criteria', e.value)}
                data={criteriaData}
              />
              <AppText style={Styles.text}>{t('OKR:content')}</AppText>
              <AppInputField
                name="content"
                multiline
                customInputStyle={{ height: device.height / 3 }}
              />
              <AppButton
                title={t('common:save')}
                onPress={handleSubmit}
                containerStyle={Styles.buttonContainer}
              />
            </ScrollView>
          </AppContainer>
        );
      }}
    </Formik>
  );
};
