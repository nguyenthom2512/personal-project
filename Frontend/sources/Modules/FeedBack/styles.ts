import { StyleSheet } from 'react-native';
import { fontSize } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container:{
      paddingHorizontal: theme.spacing.p16,
    },
    text:{
      fontSize:theme.fontSize.f18,
      marginBottom:theme.spacing.p16,
      marginVertical: theme.spacing.p20
    },
    buttonContainer:{
      marginTop:theme.spacing.p20
    }
  });
  return styles
}