import React from 'react';
import { View } from 'react-native';
import { AppButton, AppContainer, AppText } from '../../Components';
import { useThemeAwareObject } from '../../Theme';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import styles from './styles';
import { device } from '../../Helpers';

const UpgradeAccount = ({
  onPressUpgrade = () => {},
}: {
  onPressUpgrade: () => void;
}) => {
  const Styles = useThemeAwareObject(styles);

  return (
    <AppContainer title="Nâng cấp tài khoản">
      <View style={Styles.container}>
        <MaterialIcons name="upgrade" size={device.width * 0.4} />
        <AppText style={Styles.textStyle}>
          Vui lòng nâng cấp tài khoản để sử dụng tính năng này
        </AppText>
        <AppButton
          containerStyle={Styles.btnContainer}
          title="Nâng cấp"
          onPress={onPressUpgrade}
        />
      </View>
    </AppContainer>
  );
};

export default UpgradeAccount;
