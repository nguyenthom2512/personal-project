import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      padding: theme.spacing.p16,
    },
    textStyle: {
      fontWeight: '500',
      fontSize: theme.fontSize.f20,
      textAlign: 'center',
    },
    btnContainer: {
      marginVertical: theme.spacing.p16,
    },
  });
