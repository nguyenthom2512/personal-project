import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    header: {
      fontSize: theme.fontSize.f20,
      fontWeight: '700',
      marginTop: theme.spacing.p24,
    },
    card: {
      flex: 1,
      backgroundColor: theme.color.white,
      paddingVertical: theme.spacing.p24,
      paddingHorizontal: theme.spacing.p12,
      shadowColor: theme.color.black,
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,
      elevation: 5,
      borderRadius: theme.spacing.p8,
    },

    wrapIcon: {
      backgroundColor: '#EEEEEE',
      height: 40,
      width: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    txtNotieEmail: {
      fontSize: 16,
      fontWeight: '400',
      color: theme.color.shark,
      marginTop: theme.fontSize.f12,
      marginBottom: theme.fontSize.f24,
    },
  });
  return styles;
};
