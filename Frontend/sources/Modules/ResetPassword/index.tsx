import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import React, { Fragment, useCallback, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import { AppButton, AppInputField, AppText } from '../../Components';
import { IndicatorContext } from '../../Context';
import { userActions } from '../../Redux/Actions';
import resetPassword from '../../Services/resetPassword';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import Mail from '../../Assets/Svg/mail.svg';
import AppInputFieldIcon from '../../Components/AppInputFieldIcon';

interface Props {}

export default () => {
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { theme } = useTheme();
  const navigation = useNavigation();
  const indicatorContext = useContext(IndicatorContext);

  const validateLoginSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('common:wrongEmailFormat'))
      .required(t('common:required')),
  });
  const onSubmit = async (body: object) => {
    indicatorContext.setLoading(true);
    const response = await resetPassword.resetPassword(body);
    if (response.error) {
      indicatorContext.setLoading(false);
    } else {
      indicatorContext.setLoading(false);
      Alert.alert(`${t('common:notice')}`, `${t('login:checkMail')}`, [
        {
          text: `${t('common:ok')}`,
          onPress: () => {
            navigation.goBack();
          },
        },
      ]);
    }
  };

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={validateLoginSchema}
      initialValues={{ email: '' }}
      onSubmit={onSubmit}
    >
      {({ handleSubmit }) => (
        <View style={Styles.card}>
          <TouchableOpacity
            style={Styles.wrapIcon}
            onPress={() => {
              navigation.goBack();
            }}
          >
            <Feather
              name="x"
              size={theme.fontSize.f16}
              color={theme.color.grayIcon}
            />
          </TouchableOpacity>
          <AppText style={Styles.header}>{t('login:forgotPassword')}</AppText>
          <AppText style={Styles.txtNotieEmail}>
            {t('login:valiInputEmail')}
          </AppText>
          <AppInputFieldIcon
            name="email"
            placeholder={t('login:emailPlaceholder')}
            memo
            icon={<Mail />}
          />
          <AppButton
            title={t('common:continue')}
            onPress={handleSubmit}
            style={{ marginVertical: theme.spacing.p12 }}
          />
        </View>
      )}
    </Formik>
  );
};
