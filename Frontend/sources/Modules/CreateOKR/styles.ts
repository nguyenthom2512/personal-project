import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    ViewInput: {
      borderWidth: 1,
      borderColor: theme.color.gray,
    },
    renderFlatList: {
      padding: 16,
      backgroundColor: theme.color.gray,
      margin: 10,
      flex: 1,
      borderRadius: 16,
    },
    styleDropDown: {
      marginVertical: 5,
      // backgroundColor: color.primary,
    },
    styleKeyRS: {
      marginTop: theme.spacing.p8,
      padding: theme.spacing.p8,
      // backgroundColor: theme.color.white,
      // shadowColor: '#000',
      // Offset: {
      //   width: 0,
      //   height: 2,
      // },
      // shadowOpacity: 0.25,
      // shadowRadius: 3.84,
      // elevation: 5,
    },

    // component suggestionList
    mr: {
      marginRight: theme.spacing.p8,
      marginVertical: theme.spacing.p4,
    },
    txtTitle: {
      color: theme.color.grayAbbey,
      fontSize: theme.fontSize.f14,
      fontWeight: 'bold',
      paddingBottom: theme.spacing.p2,
      marginRight: theme.spacing.p4,
    },
    titleAdd: {
      color: theme.color.violet,
      fontSize: theme.fontSize.f14,
      fontWeight: '600',
      lineHeight: theme.fontSize.f18,
    },
    buttonAdd: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: theme.spacing.p8,
      marginBottom: theme.spacing.p24,
    },
    wrap1: {
      backgroundColor: theme.color.backgroundColor,
      paddingHorizontal: theme.spacing.p16,
    },
    wrap2: {
      paddingHorizontal: theme.spacing.p16,
      paddingTop: theme.spacing.p24,
      backgroundColor: theme.color.lightGray,
    },
    wrap3: {
      backgroundColor: theme.color.backgroundColor,
      paddingHorizontal: theme.spacing.p16,
      paddingTop: theme.spacing.p16,
    },
  });
  return styles;
};
