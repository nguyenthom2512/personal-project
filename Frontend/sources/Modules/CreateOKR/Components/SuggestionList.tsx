import { isEmpty } from 'lodash';
import React, { useEffect, useState } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import { Button, Text } from 'react-native-elements';
import { fontSize } from '../../../Helpers';
import { useThemeAwareObject } from '../../../Theme';
import { OKR, OkrResult } from '../createOKR.interface';
import styles from '../styles';

interface SuggestionList {
  setValues: (values: any, shouldValidate?: boolean | undefined) => void;
  values: any;
  valueOKRParent: string;
  dataOKRParent: Array<OKR>;
  index: number;
}

export default (props: SuggestionList) => {
  const Styles = useThemeAwareObject(styles);

  const [suggest, setSuggest] = useState<Array<OkrResult>>([]);

  useEffect(() => {
    const dataSuggest = props.dataOKRParent.find(
      (elm) => elm?.id?.toString() === props.valueOKRParent?.toString(),
    );
    if (dataSuggest && !isEmpty(dataSuggest)) {
      setSuggest(dataSuggest.okr_result);
    }
  }, [props.dataOKRParent, props.valueOKRParent]);

  const clickSuggest = (itemSuggest: OkrResult) => {
    const { created_time, id, okr, ...remainData } = itemSuggest;
    const newKR = {
      ...remainData,
      user: 6,
      target: remainData.target.toString(),
    };
    props.values.okr_result[props.index] = newKR;
    props.setValues({ ...props.values });
  };

  const renderItem = ({ item }: { item: OkrResult }) => (
    <View style={Styles.mr}>
      <Button
        title={item.key_result}
        type="outline"
        titleStyle={{
          color: 'black',
          fontWeight: '500',
          fontSize: fontSize.f12,
        }}
        buttonStyle={{ borderRadius: 20 }}
        onPress={() => clickSuggest(item)}
      />
    </View>
  );

  return (
    <FlatList
      data={suggest}
      renderItem={renderItem}
      horizontal
      keyExtractor={(_, index) => `${index}`}
      numColumns={1}
    />
  );
};
