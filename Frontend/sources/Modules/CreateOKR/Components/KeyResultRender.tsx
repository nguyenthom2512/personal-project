import { FormikProps } from 'formik';
import moment from 'moment';
import React, { useState } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import {
  AppDropDown,
  AppInputField,
  AppText,
  ErrorMessages,
} from '../../../Components';
import SuggestionList from './SuggestionList';
import { useTranslation } from 'react-i18next';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import { measure } from '../../../Helpers';
import { DDMMYYYY } from '../../../Helpers/dateTime';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import styles from './styles';
interface propKeyResult {
  formik: FormikProps<any>;
  dataUnit: any;
  dataOwner: any;
  defaultOkrResult: object;
  fillKeyResult?: () => void;
  disableUnit?: boolean;
}

export default (props: propKeyResult) => {
  const {
    formik,
    dataUnit,
    dataOwner,
    defaultOkrResult,
    fillKeyResult = () => {},
    disableUnit = false,
  } = props;
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const showDatePicker = (index: any) => {
    setDatePickerVisibility(index.toString());
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date: any, setFieldValue: any) => {
    const selected = moment(date).toISOString();
    setFieldValue(`okr_result[${isDatePickerVisible}].deadline`, selected);
    hideDatePicker();
  };

  return (
    <>
      <FlatList
        data={formik.values.okr_result}
        renderItem={({ item, index }) => {
          return (
            <>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}
              >
                <AppText
                  style={{
                    fontSize: theme.fontSize.f16,
                    fontWeight: '500',
                  }}
                >
                  {t('createOkr:OkrKR:keyResult')}
                  <AppText style={Styles.require}>*</AppText>
                </AppText>
                {index >= 1 && (
                  <Icon
                    tvParallaxProperties
                    type="font-awesome"
                    name="minus-circle"
                    color={theme.color.torchRed}
                    onPress={() => {
                      const newValue = [...formik.values.okr_result];
                      newValue.splice(index, 1);
                      formik.setFieldValue('okr_result', newValue);
                    }}
                  />
                )}
              </View>

              <SuggestionList
                values={formik.values}
                setValues={formik.setValues}
                valueOKRParent={formik.values.okr_parent}
                dataOKRParent={dataOwner || []}
                index={index}
              />

              <View style={{ flex: 1 }}>
                <AppInputField
                  placeholder={`(A) ${t('createOkr:Okr:import')} ${t(
                    'createOkr:OkrKR:keyResult',
                  ).toLowerCase()}`}
                  name={`okr_result[${index}].key_result`}
                    title={`(A) ${t('createOkr:OkrKR:keyResult')}`}
                  required
                />
              </View>
              <View style={{ flex: 1 }}>
                <AppInputField
                  placeholder={`${t('createOkr:Okr:import')} ${t(
                    'createOkr:OkrKR:target',
                  )}`}
                  name={`okr_result[${index}].target`}
                  keyboardType="numeric"
                    title={`${t('createOkr:Okr:import')} ${t(
                      'createOkr:OkrKR:target',
                    )}`}
                  required
                />
              </View>
              <View style={{ flex: 1 }}>
                <AppDropDown
                  placeHolder={`${t('createOkr:Okr:import')} ${t(
                    'createOkr:OkrKR:unit',
                  ).toLowerCase()}`}
                  value={formik.values.okr_result[index]?.unit}
                  isSearch
                  onChoose={(value) =>
                    formik.setFieldValue(
                      `okr_result[${index}].unit`,
                      value.value,
                    )
                  }
                  data={dataUnit}
                  disabled={disableUnit}
                    title={`${t('createOkr:Okr:import')} ${t(
                      'createOkr:OkrKR:unit',
                    ).toLowerCase()}`}
                  require
                />
                <ErrorMessages name={`okr_result[${index}].unit`} />
              </View>

              <AppInputField
                placeholder={`${t('createOkr:Okr:import')} ${t(
                  'createOkr:OkrKR:planUrl',
                ).toLowerCase()}`}
                name={`okr_result[${index}].plan_url`}
                title={`${t('createOkr:Okr:import')} ${t(
                  'createOkr:OkrKR:planUrl',
                )}`}
              />
              <AppInputField
                placeholder={`${t('createOkr:Okr:import')} ${t(
                  'createOkr:OkrKR:resultUrl',
                ).toLowerCase()}`}
                name={`okr_result[${index}].result_url`}
                title={`${t('createOkr:Okr:import')} ${t(
                  'createOkr:OkrKR:resultUrl',
                )}`}
              />
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'flex-end',
                }}
              >
                <View style={{ flex: 1, marginVertical: theme.spacing.p4 }}>
                  {/* <AppText>(T) Thời gian hoàn thành</AppText> */}
                  {/* <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <AppText style={Styles.txtTitle}>
                                  {`(T) ${t('createOkr:OkrKR:deadline')}`}
                                </AppText>
                                <Icon
                                  type="material"
                                  name="info"
                                  color={theme.color.blueChathams}
                                  onPress={() => Alert.alert('', smart.T)}
                                />
                              </View> */}
                  <View>
                    <AppText style={Styles.txtTitle}>
                      {t('createOkr:OkrKR:deadline')}
                      <AppText style={Styles.require}> *</AppText>
                    </AppText>
                    <TouchableOpacity
                      style={{
                        borderWidth: 1,
                        borderColor: theme.color.holderPlace,
                        height: measure.selectHeight,
                        justifyContent: 'center',
                        borderRadius: 4,
                        backgroundColor: theme.color.backgroundColor,
                      }}
                      onPress={() => showDatePicker(index)}
                    >
                      <AppText
                        style={{
                          color: formik.values.okr_result[index].deadline
                            ? theme.color.shark
                            : theme.color.blackTitle,
                          fontSize: formik.values.okr_result[index].deadline
                            ? theme.fontSize.f14
                            : theme.fontSize.f14,
                          opacity: formik.values.okr_result[index].deadline
                            ? 1
                            : 0.5,
                          paddingHorizontal: theme.spacing.p12,
                        }}
                      >
                        {formik.values.okr_result[index].deadline
                          ? moment(
                              formik.values.okr_result[index].deadline,
                            ).format(DDMMYYYY)
                          : `(T) ${t('createOkr:OkrKR:deadline')}`}
                      </AppText>
                    </TouchableOpacity>
                    <ErrorMessages name={`okr_result[${index}].deadline`} />
                  </View>
                </View>
              </View>
              <DateTimePickerModal
                locale="vi"
                isVisible={Boolean(isDatePickerVisible)}
                mode="date"
                onConfirm={(date) => handleConfirm(date, formik.setFieldValue)}
                onCancel={hideDatePicker}
                minimumDate={new Date()}
              />
            </>
          );
        }}
        keyExtractor={(_, index) => `${index}`}
      />
      {formik.values.okr_result?.length < 5 && (
        <TouchableOpacity
          onPress={() => {
            formik.setFieldValue('okr_result', [
              ...formik.values.okr_result,
              defaultOkrResult,
            ]);
          }}
          style={Styles.buttonAdd}
        >
          <Icon
            name="add"
            type="ionicon"
            color={theme.color.violet}
            tvParallaxProperties
          />
          <AppText style={Styles.titleAdd}>{`${t('createOkr:Okr:add')} ${t(
            'createOkr:OkrKR:keyResult',
          )}`}</AppText>
        </TouchableOpacity>
      )}
    </>
  );
};
