import { useNavigation } from '@react-navigation/core';
import { Field, Formik } from 'formik';
import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, ScrollView, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { useSelector } from 'react-redux';
import * as yup from 'yup';
import {
  AppButton,
  AppCheckboxField,
  AppDropDown,
  AppInputField,
  AppText,
  ErrorMessages,
} from '../../../Components';
import AppDropDownMultiple from '../../../Components/AppDropDownMultiple';
import ModalApp from '../../../Components/ModalApp';
import { IndicatorContext } from '../../../Context';
import useGetDepartment from '../../../Hooks/getDepartment';
import useGetListQuiz from '../../../Hooks/getQuizList';
import getQuizService from '../../../Services/getQuizService';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './styles';

interface Props {
  isModalVisible: any;
  closeModal?: any;
  inforOKR: any;
  dataCreate?: any;
}

export default (props: Props) => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const navigation = useNavigation();
  const Styles = useThemeAwareObject(styles);

  const { loading, setLoading } = useContext(IndicatorContext);
  const dataUser = useSelector((state: any) => state.userReducer.data);

  const [dataDepartment, loadingDepartment, error, refetch] =
    useGetDepartment();
  const department = (props.dataCreate?.Department || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.department_name,
    };
  });

  const UserOkr = (props.dataCreate?.AppUser || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.full_name,
    };
  });

  const [dataQuiz, loadingQuiz, errorQuiz, refetchQuiz] = useGetListQuiz();
  const quiz = (dataQuiz || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.title,
    };
  });
  const initialValues = {
    inspiration: false,
    challenge: false,
    company: false,
    public: false,
    content: '',
    okr: props.inforOKR?.id,
    challenged_company: props.dataCreate.Company,
    challenged_department: [],
    challenged_user: [],

    user_create: dataUser.user?.id,
    quiz: '',
    user_do: [],
  };
  const validate = yup.object().shape({
    quiz: yup.string().when('inspiration', {
      is: true,
      then: yup.string().required(t('common:required')),
    }),
  });
  const onSubmit = async (data: any) => {
    const dataChallenge = data.company
      ? {
          okr: data.okr,
          public: data.public,
          content: data.content,
          challenged_company: data.challenged_company,
          user: data.user_create,
        }
      : {
          okr: data.okr,
          public: data.public,
          content: data.content,
          challenged_user: data.challenged_user,
          challenged_department: data.challenged_department,
          user: data.user_create,
        };
    const dataInspiration = {
      okr: data.okr,
      user_create: data.user_create,
      quiz: data.quiz,
      user_do: data.user_do,
      user: dataUser.user?.id,
    };
    setLoading(true);

    // callApiChallenge({ data, dataChallenge })
    //   .then((res) => {})
    //   .catch((err) => {
    //     // addToast(err, { appearance: 'Thất bại!' });
    //     Alert.alert(`${t('common:notice')}`, err.toString());
    //   })
    //   .finally(() => {
    //     callApiInspiration({ data, dataInspiration });
    //   });
    const challenge = await callApiChallenge({ data, dataChallenge });
    const inspiration = await callApiInspiration({ data, dataInspiration });
    const errorMess = [];
    if (challenge?.error) {
      errorMess.push(challenge.errorMessage);
    }
    if (inspiration?.error) {
      errorMess.push(inspiration.errorMessage);
    }

    if (errorMess.length > 0) {
      Alert.alert(t('common:notice'), errorMess.toString());
    } else {
      Alert.alert(`${t('common:notice')}`, `${t('common:saveSuccess')}`, [
        {
          text: `${t('common:ok')}`,
          onPress: () => {
            props.closeModal();
            // navigation.goBack();
          },
        },
      ]);
    }

    setLoading(false);
    //   Promise.all([
    //     // callApiChallenge({ data, dataChallenge }),
    //     // callApiInspiration({ data, dataInspiration }),
    //   ])
    //     .then((values) => {
    //       let err: any = [];
    //       values.map((el: any) => {
    //         if (el?.error) {
    //           err = [...err, el.errorMessage];
    //         }
    //       });
    //       if (err.length > 0) {
    //         Alert.alert(`${t('common:notice')}`, err.toString());
    //       } else {
    //         Alert.alert(`${t('common:notice')}`, `${t('common:saveSuccess')}`, [
    //           {
    //             text: `${t('common:ok')}`,
    //             onPress: () => {
    //               props.closeModal();
    //               // navigation.goBack();
    //             },
    //           },
    //         ]);
    //       }
    //     })
    //     .catch((er) => console.log({ er }))
    //     .finally(() => {
    //       setLoading(false);
    //     });
  };
  const callApiChallenge = async (data: any) => {
    if (data.data.challenge) {
      const challenge = await getQuizService.postOkrChallenge(
        data.dataChallenge,
      );
      return challenge;
    } else {
      return null;
    }
  };
  const callApiInspiration = async (data: any) => {
    console.log('overdueWork', data);

    if (data.data?.inspiration) {
      const inspiration = await getQuizService.postOkrInspriration(
        data.dataInspiration,
      );
      if (inspiration.error) {
        Alert.alert(`${t('common:notice')}`, inspiration.errorMessage);
      }
      return inspiration;
    } else {
      return null;
    }
  };

  return (
    <ModalApp
      visible={props.isModalVisible}
      onRequestClose={props.closeModal}
      headerText={props.inforOKR?.object_name}
      iconLeft={
        <Icon
          name="x"
          type="feather"
          color={theme.color.gray}
          onPress={props.closeModal}
          tvParallaxProperties
        />
      }
      contentStyle={{ maxHeight: '70%' }}
    >
      <Formik
        initialValues={initialValues}
        validationSchema={validate}
        onSubmit={onSubmit}
        validateOnBlur={false}
        enableReinitialize
      >
        {({
          handleChange,
          values,
          handleSubmit,
          setFieldValue,
          setValues,
          errors,
        }) => {
          return (
            <ScrollView>
              <View style={{ margin: 16 }}>
                <View style={Styles.wrapChallenge}>
                  <Field
                    name="challenge"
                    title={t('common:challenge')}
                    component={AppCheckboxField}
                    checked={values.challenge}
                    onPress={() => {
                      setFieldValue('challenge', !values.challenge);
                    }}
                  />
                  <View style={{ marginHorizontal: 16 }}>
                    <AppInputField
                      name="content"
                      placeholder={t('common:content')}
                      multiline={true}
                      customInputStyle={{
                        height: 120,
                      }}
                      editable={values.challenge ? true : false}
                    />
                    <AppText>Thách thức đến</AppText>
                    <Field
                      name="company"
                      title={t('common:company')}
                      component={AppCheckboxField}
                      checked={values.company}
                      onPress={() => {
                        setFieldValue('company', !values.company);
                      }}
                      disabled={!values.challenge ? true : false}
                    />
                    <Field
                      name="challenged_department"
                      placeholder={t('treeOKR:selectDepartment')}
                      component={AppDropDownMultiple}
                      data={department || []}
                      onChoose={(value: any) =>
                        setFieldValue('challenged_department', value)
                      }
                      disabled={
                        !values.challenge || values.company ? true : false
                      }
                    />
                    <ErrorMessages name="challenged_department" />
                    <Field
                      name="challenged_user"
                      placeholder={t('common:selectUser')}
                      component={AppDropDownMultiple}
                      data={UserOkr || []}
                      onChoose={(value: any) =>
                        setFieldValue('challenged_user', value)
                      }
                      disabled={
                        !values.challenge || values.company ? true : false
                      }
                    />
                    <ErrorMessages name="challenged_user" />
                  </View>
                  <Field
                    name="public"
                    title={t('common:public')}
                    component={AppCheckboxField}
                    checked={values.public}
                    onPress={() => {
                      setFieldValue('public', !values.public);
                    }}
                    disabled={!values.challenge ? true : false}
                  />
                </View>
                <View style={Styles.wrapInspiration}>
                  <Field
                    name="inspiration"
                    title={t('common:inspiration')}
                    component={AppCheckboxField}
                    checked={values.inspiration}
                    onPress={() => {
                      setFieldValue('inspiration', !values.inspiration);
                    }}
                  />
                  <View style={{ marginHorizontal: 16 }}>
                    <Field
                      name="quiz"
                      placeHolder={t('common:selectQuiz')}
                      component={AppDropDown}
                      data={quiz || []}
                      onChoose={(value: any) =>
                        setFieldValue('quiz', value.value)
                      }
                      disabled={!values.inspiration ? true : false}
                    />
                    <ErrorMessages name="quiz" />
                    <Field
                      name="user_do"
                      placeholder={t('common:selectUser')}
                      component={AppDropDownMultiple}
                      data={UserOkr || []}
                      onChoose={(value: any) => setFieldValue('user_do', value)}
                      disabled={!values.inspiration ? true : false}
                    />
                    <ErrorMessages name="user_do" />
                  </View>
                </View>
                <AppButton
                  loading={loading}
                  title={t('common:save')}
                  onPress={() => {
                    {
                      !values.challenge && !values.inspiration
                        ? Alert.alert(
                            `${t('common:notice')}`,
                            `${t('common:select')} ${t('common:challenge')} ${t(
                              'common:inspiration',
                            )}`,
                          )
                        : handleSubmit();
                    }
                  }}
                />
              </View>
            </ScrollView>
          );
        }}
      </Formik>
    </ModalApp>
  );
};
