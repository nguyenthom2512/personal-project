import { FormikProps } from 'formik';
import React from 'react';
import AppDropDownMultiple from '../../../Components/AppDropDownMultiple';
import { useTranslation } from 'react-i18next';

interface RaicProps {
  data: Array<any>;
  formik: FormikProps<any>;
}

export default (props: RaicProps) => {
  const { t } = useTranslation();
  const { data, formik } = props;

  return (
    <>
      <AppDropDownMultiple
        value={formik.values.user_responsible}
        isSearch
        placeholder={`(R) ${t('createOkr:Okr:userResponsible')}`}
        title={`(R) ${t('createOkr:Okr:userResponsible')}`}
        data={data}
        onChoose={(value) => formik.setFieldValue('user_responsible', value)}
      />
      <AppDropDownMultiple
        value={formik.values.user_accountable}
        isSearch
        placeholder={`(A) ${t('createOkr:Okr:userAccountable')}`}
        title={`(A) ${t('createOkr:Okr:userAccountable')}`}
        data={data}
        onChoose={(value) => formik.setFieldValue('user_accountable', value)}
      />
      <AppDropDownMultiple
        value={formik.values.user_inform}
        isSearch
        title={`(I) ${t('createOkr:Okr:userInform')}`}
        placeholder={`(I) ${t('createOkr:Okr:userInform')}`}
        data={data}
        onChoose={(value) => formik.setFieldValue('user_inform', value)}
      />
      <AppDropDownMultiple
        value={formik.values.user_consult}
        isSearch
        title={`(C) ${t('createOkr:Okr:userConsult')}`}
        placeholder={`(C) ${t('createOkr:Okr:userConsult')}`}
        data={data}
        onChoose={(value) => formik.setFieldValue('user_consult', value)}
      />
    </>
  );
};
