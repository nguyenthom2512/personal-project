import { FormikProps } from 'formik';
import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { AppDropDown, AppText } from '../../../Components';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import { useTranslation } from 'react-i18next';
import style from './styles';

interface propsRelate {
  formik: FormikProps<any>;
  data: Array<any>;
}

export default (props: propsRelate) => {
  const { formik, data } = props;
  const { theme } = useTheme();
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(style);

  return (
    <>
      {(formik.values.okr_relate || []).map((elm: any, index: any) => {
        return (
          <View
            key={index}
            style={{
              flexDirection: 'row',
              alignItems: 'flex-start',
            }}
          >
            <View style={{ flex: 1 }}>
              <AppDropDown
                value={formik.values?.okr_relate?.[index]}
                title={t('createOkr:Okr:okrRelate')}
                isSearch
                placeHolder={t('createOkr:Okr:okrRelate')}
                data={data}
                onChoose={(value) =>
                  formik.setFieldValue(`okr_relate.[${index}]`, value.value)
                }
                notify={
                  index >= 1 ? (
                    <Icon
                      tvParallaxProperties
                      type="font-awesome"
                      name="minus-circle"
                      color={theme.color.torchRed}
                      onPress={() => {
                        const newValue = [...formik.values.okr_relate];
                        formik.setFieldValue(
                          'okr_relate',
                          newValue.filter((_, idx) => idx !== index),
                        );
                      }}
                    />
                  ) : (
                    <View />
                  )
                }
                reload
                onClear={() => formik.setFieldValue(`okr_relate.[${index}]`, '')}
              />
            </View>
          </View>
        );
      })}
      <TouchableOpacity
        onPress={() => {
          formik.setFieldValue(`okr_relate`, [...formik.values.okr_relate, '']);
        }}
        style={Styles.buttonAdd}
      >
        <Icon
          name="add"
          type="ionicon"
          color={theme.color.violet}
          tvParallaxProperties
        />
        <AppText style={Styles.titleAdd}>{`${t('createOkr:Okr:add')} ${t(
          'createOkr:Okr:okrRelate',
        )}`}</AppText>
      </TouchableOpacity>
    </>
  );
};
