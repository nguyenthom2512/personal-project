import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    ViewInput: {
      borderWidth: 1,
      borderColor: theme.color.gray,
    },
    renderFlatList: {
      padding: theme.spacing.p16,
      backgroundColor: theme.color.gray,
      margin: 10,
      flex: 1,
      borderRadius: 16,
    },
    styleDropDown: {
      marginVertical: 5,
      // backgroundColor: color.primary,
    },
    styleKeyRS: {
      marginTop: theme.spacing.p8,
      padding: theme.spacing.p8,
      // backgroundColor: theme.color.white,
      // shadowColor: '#000',
      // Offset: {
      //   width: 0,
      //   height: 2,
      // },
      // shadowOpacity: 0.25,
      // shadowRadius: 3.84,
      // elevation: 5,
    },

    // component suggestionList
    mr: {
      marginRight: theme.spacing.p8,
      marginVertical: theme.spacing.p4,
    },
    txtTitle: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      paddingBottom: theme.spacing.p4,
      marginRight: theme.spacing.p4,
      marginTop: theme.spacing.p8,
    },
    titleAdd: {
      color: theme.color.violet,
      fontSize: theme.fontSize.f14,
      fontWeight: '600',
      lineHeight: theme.fontSize.f18,
    },
    buttonAdd: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: theme.spacing.p8,
      marginBottom: theme.spacing.p24,
    },
    require: {
      fontSize: theme.fontSize.f16,
      color: theme.color.redAlert,
    },

    //Quiz
    wrapChallenge: {
      paddingVertical: theme.spacing.p8,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.color.holderPlace,
    },
    wrapInspiration: {
      marginVertical: theme.spacing.p16,
      paddingVertical: theme.spacing.p8,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.color.holderPlace,
    },
  });
  return styles;
};
