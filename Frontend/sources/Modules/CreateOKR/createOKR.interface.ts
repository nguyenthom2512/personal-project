// export interface OkrSuperior {
//   user: string;
//   okr: string;
//   key_result: string;
//   target: Number;
//   current_done: string;
//   unit: string;
//   plan_url: string;
//   result_url: string;
//   deadline: string;
// }

// export interface OKRPeriod {
//   value: string;
//   label: string;
// }

export interface OkrResult {
  id: number;
  created_time: string;
  key_result: string;
  target: string | number;
  current_done: number;
  plan_url: string;
  result_url: string;
  deadline: string;
  user: number;
  okr: number;
  unit: number;
}

export interface User {
  id: number;
  full_name: string;
}
export interface OKR {
  id: number;
  object_name: string;
  owner_okr: string;
  count_result: number;
  percent_changed: number | string;
  confident: number;
  percent_completed: number | string;
  okr_result: Array<OkrResult>;
  user: User;
}
