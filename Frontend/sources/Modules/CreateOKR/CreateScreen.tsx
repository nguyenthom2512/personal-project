import { useNavigation, useRoute } from '@react-navigation/core';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, ScrollView, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import {
  AppButton,
  AppContainer,
  AppDropDown,
  AppInputField,
  AppText,
} from '../../Components';
import AppCheckboxField from '../../Components/AppCheckboxField';
import ErrorMessages from '../../Components/ErrorMessages';
import { IndicatorContext } from '../../Context';
import useGetCreateOKR from '../../Hooks/getDataCreateOKR';
import useGetOKR from '../../Hooks/getOkr';
import useGetOkrDetail from '../../Hooks/getOkrDetail';
import useGetOkrRelate from '../../Hooks/getOkrRelate';
import useGetOkrResult from '../../Hooks/getOkrResult';
import { okrActions, raicAction } from '../../Redux/Actions';
import { RootState } from '../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import KeyResultRender from './Components/KeyResultRender';
import ModalQuiz from './Components/ModalQuiz';
import OkrRelateRender from './Components/OkrRelateRender';
import RaicRender from './Components/RaicRender';
import styles from './styles';

const regMatch =
  /^((http|https):\/\/)?(www.)?(?!.*(http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+(\/)?.([\w\?[a-zA-Z-_%\/@?]+)*([^\/\w\?[a-zA-Z0-9_-]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/;

const RAIC_TYPE = {
  Responsible: 0,
  Accountable: 1,
  Inform: 2,
  Consult: 3,
};

const CreateScreen = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const indicatorContext = useContext(IndicatorContext);
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;
  const [inforOKRCreate, setInforOKRCreate] = useState();
  const [defaultOkrResult, setDefaultOkrResult] = useState({
    user,
    okr: '',
    key_result: '',
    target: '',
    current_done: 0,
    unit: '',
    plan_url: '',
    result_url: '',
    deadline: '',
  });

  const initialValues = {
    user, //  login => user
    department: '', //   phòng ban
    okr_parent: '', // okr cấp trên
    object_name: '', // mục tiêu okr
    is_done: false, // check false
    confident: 0, // mức độ tự tin
    okr_relate: [''], // liên kết chéo
    challenge: false, // thách thức
    user_responsible: [], //lên
    user_accountable: [],
    user_inform: [],
    user_consult: [],
    percent_changed: 0,
    raic_user: [],
    okr_result: [defaultOkrResult],
    result_parent: '',
  };
  const validateCreateOKR = Yup.object().shape({
    object_name: Yup.string().required(t('common:required')),
    confident: Yup.string().required(t('common:required')),
    user: Yup.string().required(t('common:required')),
    department: Yup.string().required(t('common:required')),
    okr_result: Yup.array().of(
      Yup.object().shape({
        user: Yup.string().required(t('common:required')),
        key_result: Yup.string().required(t('common:required')),
        target: Yup.number()
          .typeError(t('createOkr:targeTypeError'))
          .required(t('common:required'))
          .positive(t('createOkr:targeTypeError'))
          .integer(t('createOkr:targeTypeError')),
        current_done: Yup.string().required(t('common:required')),
        unit: Yup.string().required(t('common:required')),
        deadline: Yup.string().required(t('common:required')),
        plan_url: Yup.string().matches(regMatch, t('common:requiredLink')),
        result_url: Yup.string().matches(regMatch, t('common:requiredLink')),
      }),
    ),
    result_parent: Yup.string()
      .when('okr_parent', {
        is: (value) => Boolean(value),
        then: Yup.string().required(t('common:required')),
      })
      .nullable(),
  });

  //update screen

  const route = useRoute();
  let checkScreen = true;
  const { id, refetchScreen }: any = route.params;

  if (id != '') {
    checkScreen = false;
  }
  const [dataGetOkr, loadingDataOkr, errorOkr, refetchGetOKR] =
    useGetOkrDetail(id);
  const [defaultOKR, setDefaultOKR] = useState<any>(initialValues);

  const ConvertApi = (params: any, dataRelate?: any) => {
    const OkrRelate = (dataRelate?.okr_relate || []).map((el: any) => el.id);
    let user_responsible: any = []; //lên
    let user_accountable: any = [];
    let user_inform: any = [];
    let user_consult: any = [];
    (params?.raic_user || []).map((e: any) => {
      switch (e.raic_type) {
        case RAIC_TYPE.Responsible:
          user_responsible.push(e.user);
          break;
        case RAIC_TYPE.Accountable:
          user_accountable.push(e.user);
          break;
        case RAIC_TYPE.Inform:
          user_inform.push(e.user);
          break;
        case RAIC_TYPE.Consult:
          user_consult.push(e.user);
          break;
        default:
          break;
      }
    });
    (params?.okr_result || []).map((e: any, index: any) => {
      if (e.plan_url === null) {
        params.okr_result[index].plan_url = '';
      }
      if (e.result_url == null) {
        params.okr_result[index].result_url = '';
      }
      params.okr_result[index].target =
        params.okr_result[index].target.toString();
    });
    if (isEmpty(params.okr_relate)) {
      params.okr_relate = [''];
    }
    params['raic_user'] = [];
    params['user_responsible'] = user_responsible;
    params['user_accountable'] = user_accountable;
    params['user_inform'] = user_inform;
    params['user_consult'] = user_consult;
    indicatorContext.setLoading(false);
    setDefaultOKR(params);
  };

  useEffect(() => {
    indicatorContext.setLoading(true);
    if (id != '') {
      if (!isEmpty(dataGetOkr)) {
        indicatorContext.setLoading(true);
        dispatch(
          okrActions.getListOkrFilter({ department: dataGetOkr.department }),
        );
        refetch(dataGetOkr.okr_parent);
        if (!isEmpty(dataGetOkr?.result_parent?.toString())) {
          setDisableUnit(true);
          const newFillData = {
            key_result: dataGetOkr.okr_result[0].key_result,
            unit: dataGetOkr.okr_result[0].unit,
          };
          const temptKR = { ...defaultOkrResult, ...newFillData };
          setDefaultOkrResult(temptKR);
        }
        ConvertApi(dataGetOkr);
      } else {
        setDefaultOKR(initialValues);
        indicatorContext.setLoading(false);
      }
    } else {
      setDefaultOKR(initialValues);
      indicatorContext.setLoading(false);
    }
  }, [dataGetOkr]);

  // const { param }: any = route.params;
  const RaicData = useSelector((state: RootState) => state.raicReducer);
  const dataOkrOwner = useSelector((state: RootState) => state.okrReducer);
  const [dataOKR, loadingOKR, errorOKR, refetchOKR] = useGetOKR();
  const [dataCreateOKR, loadingCreateOKR, errorCreateOKR, refetchCreateOKR] =
    useGetCreateOKR();

  useEffect(() => {
    if (id != '') {
      indicatorContext.setLoading(true);
    }
    dispatch(raicAction.getOkrRaic());
    return () => {
      dispatch(okrActions.resetListOkrFilter());
    };
  }, []);

  const department = (dataCreateOKR?.Department || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.department_name,
    };
  });

  const okrRelate = (dataOKR || []).map((elm) => {
    return {
      value: elm?.id,
      label: elm?.owner_okr,
    };
  });

  const okrPeriod = (dataOkrOwner?.data?.results || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.owner_okr,
    };
  });

  const okrUnit = (dataCreateOKR?.Unit || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.unit_name,
    };
  });
  const RAIC = (dataCreateOKR?.AppUser || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.name_with_department,
    };
  });

  const handleItemRaic = (itemRaic: Array<any>, values: any, typeRaic: any) =>
    itemRaic.map((e: any) => {
      {
        values.raic_user = [
          ...values.raic_user,
          {
            raic_type: typeRaic,
            user: e,
            okr: null,
            todo: null,
          },
        ];
      }
    });

  const handleDataRAIC = (values: any) => {
    if (values.user_responsible !== undefined)
      handleItemRaic(values.user_responsible, values, RAIC_TYPE.Responsible);
    if (values.user_accountable !== undefined)
      handleItemRaic(values.user_accountable, values, RAIC_TYPE.Accountable);
    if (values.user_inform !== undefined)
      handleItemRaic(values.user_inform, values, RAIC_TYPE.Inform);
    if (values.user_consult !== undefined)
      handleItemRaic(values.user_consult, values, RAIC_TYPE.Consult);

    values.okr_relate = values.okr_relate.filter(Number);

    delete values.user_responsible;
    delete values.user_accountable;
    delete values.user_inform;
    delete values.user_consult;
  };

  const onSubmit = (values: any) => {
    handleDataRAIC(values);
    indicatorContext.setLoading(true);
    if (checkScreen)
      dispatch(
        okrActions.createOKRRequest(values, {
          onSuccess: (success: any) => {
            indicatorContext.setLoading(false);
            Alert.alert(t('createOkr:Okr:createSuccess'), '', [
              {
                text: `${t('common:ok')}`,
                onPress: () => {
                  refetchScreen && refetchScreen();
                  setInforOKRCreate(success.response);
                  toggleModal();
                },
              },
            ]);
          },
          onFailed: (error: any) => {
            indicatorContext.setLoading(false);
            Alert.alert(t('createOkr:Okr:createFalse'));
          },
        }),
      );
    else {
      dispatch(
        okrActions.updateOkrRequest(values, {
          onSuccess: (success: any) => {
            indicatorContext.setLoading(false);
            Alert.alert(t('common:notice'), t('common:updateSuccess'), [
              {
                text: t('common:ok'),
                onPress: () => {
                  refetchScreen && refetchScreen();
                  navigation.goBack();
                },
              },
            ]);
          },
          onFailed: (error: any) => {
            indicatorContext.setLoading(false);
            Alert.alert(t('common:updateFailed'), error.errorMessage);
          },
        }),
      );
    }
  };

  const setDataOkrOwner = (value: any) => {
    dispatch(okrActions.getListOkrFilter({ department: value.value }));
  };

  const [idKR, setIdKR] = useState<any>();
  const [dataOkrResult, loading, error, refetch]: any = useGetOkrResult({
    id: idKR,
  });
  const KR = (dataOkrResult?.okr_result || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.key_result,
      unit: elm?.unit,
      key_result: elm?.key_result,
    };
  });
  const [disableUnit, setDisableUnit] = useState(false);
  const handleKeyResult = (
    keyResult: string,
    unit: string,
    setFieldValue: any,
    Formik_okr_result: Array<any>,
  ) => {
    const newFillData = { key_result: keyResult, unit };
    const tempt = { ...defaultOkrResult, ...newFillData };
    setDefaultOkrResult(tempt);

    const newResult = Formik_okr_result.map((e, index) => {
      return {
        ...e,
        ...newFillData,
      };
    });
    setFieldValue('okr_result', newResult);
    setDisableUnit(true);
  };

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };
  return (
    <AppContainer
      title={checkScreen ? t('createOkr:Okr:createOkr') : t('OKR:updateOkr')}
      isBack
    >
      <ScrollView>
        <Formik
          initialValues={defaultOKR}
          validationSchema={validateCreateOKR}
          enableReinitialize
          onSubmit={onSubmit}
        >
          {(propsFormik) => {
            return (
              <View>
                <View style={Styles.wrap1}>
                  <AppDropDown
                    value={propsFormik.values.department}
                    isSearch
                    data={department}
                    onChoose={(value) => {
                      propsFormik.setFieldValue('department', value.value),
                        setDataOkrOwner(value);
                    }}
                    placeHolder={`${t('common:select')} ${t(
                      'createOkr:Okr:department',
                    ).toLowerCase()}`}
                    require
                    title={`${t('common:select')} ${t(
                      'createOkr:Okr:department',
                    ).toLowerCase()}`}
                  />
                  <ErrorMessages name="department" />
                  <AppDropDown
                    value={propsFormik.values.okr_parent}
                    isSearch
                    title={`${t('createOkr:Okr:okrParent')}`}
                    placeHolder={t('createOkr:Okr:okrParent')}
                    data={department ? okrPeriod : []}
                    onChoose={(value) => {
                      propsFormik.setFieldValue('okr_parent', value.value);
                      propsFormik.setFieldValue('result_parent', '');
                      setIdKR(value.value);
                      refetch();
                    }}
                    reload
                    onClear={() => {
                      propsFormik.setFieldValue('okr_parent', '');
                      propsFormik.setFieldValue('result_parent', '');
                      setIdKR('');
                      refetch();
                    }}
                    // notify={
                    //   <Icon
                    //     type="material"
                    //     name="info"
                    //     color={theme.color.blueChathams}
                    //     onPress={() => Alert.alert('', smart.R)}
                    //   />
                    // }
                  />
                  <AppDropDown
                    value={propsFormik.values.result_parent}
                    isSearch
                    data={KR}
                    onChoose={(value) => {
                      handleKeyResult(
                        value.key_result,
                        value.unit,
                        propsFormik.setFieldValue,
                        propsFormik.values.okr_result,
                      );
                      propsFormik.setFieldValue('result_parent', value.value);
                    }}
                    placeHolder={`${t('common:select')} ${t(
                      'createOkr:resultParent',
                    ).toLowerCase()}`}
                    reload
                    title={`${t('common:select')} ${t(
                      'createOkr:resultParent',
                    ).toLowerCase()}`}
                    onClear={() => {
                      propsFormik.setFieldValue('result_parent', '');
                    }}
                  />
                  <ErrorMessages name="result_parent" />
                  <AppInputField
                    title={t('createOkr:OkrKR:target')}
                    placeholder={`${t('createOkr:Okr:objectName')}`}
                    name="object_name"
                    multiline={true}
                    customInputStyle={{
                      height: 120,
                      justifyContent: 'flex-start',
                    }}
                    required
                  />
                  {/* <View>
                    <AppCheckboxField
                      name="challenge"
                      title={`(S) ${t('createOkr:Okr:okrChallenge')}`}
                      onPress={(checked) =>
                        propsFormik.setFieldValue('challenge', checked)
                      }
                    />
                  </View> */}
                  <RaicRender data={RAIC} formik={propsFormik} />
                </View>

                <View style={Styles.wrap2}>
                  <KeyResultRender
                    dataUnit={okrUnit}
                    dataOwner={dataOkrOwner?.data.results}
                    formik={propsFormik}
                    defaultOkrResult={defaultOkrResult}
                    disableUnit={disableUnit}
                  />
                </View>
                <View style={Styles.wrap3}>
                  <OkrRelateRender formik={propsFormik} data={okrRelate} />
                  <AppButton
                    containerStyle={{ marginTop: theme.spacing.p8 }}
                    onPress={() => {
                      propsFormik.handleSubmit();
                      // toggleModal();
                    }}
                    title={
                      checkScreen ? t('common:create') : t('common:update')
                    }
                  />
                </View>
              </View>
            );
          }}
        </Formik>
      </ScrollView>
      {isModalVisible && (
        <ModalQuiz
          isModalVisible={isModalVisible}
          closeModal={() => {
            toggleModal();
            navigation.goBack();
          }}
          inforOKR={inforOKRCreate}
          dataCreate={dataCreateOKR}
        />
      )}
    </AppContainer>
  );
};

export default CreateScreen;
