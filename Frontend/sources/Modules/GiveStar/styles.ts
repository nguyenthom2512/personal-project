import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    profileWrap: {
      flexDirection: 'row',
      alignItems: 'center',
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
      paddingVertical: theme.spacing.p12,
    },
    img: {
      width: 32,
      height: 32,
      borderRadius: theme.spacing.p16,
      marginHorizontal: theme.spacing.p6,
      backgroundColor: theme.color.gray,
    },
    name: {
      fontWeight: '400',
      fontSize: theme.fontSize.f14,
    },
    jobTitle: {
      fontSize: 12,
      color: theme.color.blackTitle,
      opacity: 0.5,
    },
    myStar: {
      alignItems: 'center',
      marginBottom: theme.spacing.p12,
      borderRadius: theme.spacing.p20,
      backgroundColor: theme.color.mercury,
      paddingHorizontal: theme.spacing.p16,
      padding: theme.spacing.p8,
      marginTop:theme.spacing.p12
    },
    dropdownStyles: {
      borderRadius: theme.spacing.p8,
      paddingHorizontal: theme.spacing.p16,
    },
    buttonStyles: {
      marginVertical: theme.spacing.p16,
    },
    textStyles: {
      fontSize: theme.fontSize.f18,
    },
    titleStyle: { 
      fontSize: theme.fontSize.f15,
      marginBottom: theme.spacing.p8
    },
    headerStyles:{
      fontSize: theme.fontSize.f20
    },
    placeholderStyles:{
      fontSize: theme.fontSize.f16
    }
  });
  return styles;
};
