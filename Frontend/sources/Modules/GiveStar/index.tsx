import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import {
  AppButton,
  AppDropDown,
  AppInputField,
  AppText,
} from '../../Components';
import ModalApp from '../../Components/ModalApp';
import { IndicatorContext } from '../../Context';
import useGetCreateOKR from '../../Hooks/getDataCreateOKR';
import useGetOKR from '../../Hooks/getOkr';
import useGetStarUser from '../../Hooks/getOkrUser';
import useGetOkrUser from '../../Hooks/getOkrUser';
import { UserItem } from '../../interfaces/okr.interface';
import { getUserRequest } from '../../Redux/Actions/userActions';
import { RootState } from '../../Redux/Reducers';
import feedbackService from '../../Services/feedbackService';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface Props {
  modalVisible: boolean;
  onCloseModal: () => void;
  star: number;
}

export default (props: Props) => {
  const { modalVisible, onCloseModal, star } = props;
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const [dataOKR, loading, error, refreshOkr] = useGetOKR();
  const [dataUser, loadingUser, errorUser, refetchUser] = useGetStarUser();
  const [modalGive, setModalGive] = useState(false);

  const toggleModalGive = (errors?: any) => {
    if (!errors?.user_received) {
      setModalGive(!modalGive);
      return;
    }
    Alert.alert('', errors.user_received);
  };

  const userReducer = useSelector(
    (state: RootState) => state.userReducer?.data,
  );

  const [dataCreateOKR] = useGetCreateOKR();
  const department = (dataCreateOKR?.Department || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.department_name,
    };
  });
  const okrList = (dataOKR || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.owner_okr,
      user: elm?.user?.id,
    };
  });

  const userList = (dataUser || []).map((elm?: UserItem) => {
    return {
      value: elm?.id || 0,
      label: elm?.full_name || '',
    };
  });

  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const { setLoading } = useContext(IndicatorContext);
  const dispatch = useDispatch();

  const validationSchema = Yup.object().shape({
    content: Yup.string().required(t('common:required')),
    quantity: Yup.number()
      .typeError(t('common:errorNumber'))
      .required(t('common:required'))
      .min(1, t('common:errorMinStar'))
      .max(userReducer?.user?.rate_month, t('common:errorMaxGiveStar')),
    user_received: Yup.string().required(t('common:errorUserReceived')),
  });

  const navigation = useNavigation();

  const onSubmit = (values: any) => {
    setLoading(true);
    feedbackService.createFeedback(
      { ...values, content: `<p>${values.content}</p>` },
      (err) => {
        setLoading(false);
        Alert.alert(t('common:notice'), err);
      },
      () => {
        setLoading(false);
        [
          {
            onPress: () => navigation.goBack(),
          },
        ];
        Alert.alert(t('common:notice'), t('common:giveStarSuccess'));
        const userData = userReducer.user;
        dispatch(getUserRequest(userData.id));
        setModalGive(false);
        onCloseModal();
      },
    );
  };

  return (
    <ModalApp
      visible={modalVisible}
      // type
      iconLeft={
        <Icon
          name="x"
          type="feather"
          color={theme.color.gray}
          tvParallaxProperties
        />
      }
      headerText="Tặng sao"
      headerStyles={Styles.headerStyles}
      onLeftPress={onCloseModal}
    >
      <Formik
        initialValues={{
          content: '',
          criteria: '',
          okr: '',
          user_received: '',
          user,
          quantity: '',
          department: '',
        }}
        onSubmit={onSubmit}
        // enableReinitialize
        validateOnBlur={true}
        // validateOnChange={true}
        validationSchema={validationSchema}
      >
        {({ handleSubmit, values, setFieldValue, errors }) => {
          return (
            <>
              <View style={Styles.dropdownStyles}>
                <AppDropDown
                  reload
                  titleStyles={Styles.titleStyle}
                  isSearch
                  title={t('common:chooseDepartment')}
                  data={department}
                  onChoose={(value) => {
                    refreshOkr({ department: value.value });
                    refetchUser({ department: value.value });
                    setFieldValue('department', value.value);
                    setFieldValue('okr', '');
                    setFieldValue('user_received', '');
                    return value;
                  }}
                  onClear={() => {
                    refetchUser({ okr: values.okr });
                    setFieldValue('user_received', '');
                  }}
                />
                <AppDropDown
                  reload
                  titleStyles={Styles.titleStyle}
                  isSearch
                  // disabled={!values.department}
                  title={t('common:chooseOkr')}
                  data={okrList}
                  onChoose={(value) => {
                    refetchUser({ okr: value.value });
                    setFieldValue('okr', value.value);
                    setFieldValue('user_received', '');
                    return value;
                  }}
                  value={values.okr}
                  onClear={() => {
                    refetchUser({ department: values.department });
                    setFieldValue('user_received', '');
                  }}
                />
                <AppDropDown
                  reload
                  isSearch
                  // disabled={!values.department}
                  titleStyles={Styles.titleStyle}
                  title={t('common:chooseUser')}
                  data={userList}
                  value={values.user_received}
                  onChoose={(value) => {
                    setFieldValue('user_received', value.value);
                    return value;
                  }}
                  onClear={() => {
                    setFieldValue('user_received', '');
                  }}
                />
              </View>
              <View style={{ marginHorizontal: 16 }}>
                <AppButton
                  containerStyle={Styles.buttonStyles}
                  textStyle={Styles.textStyles}
                  // cancel
                  title={t('common:giveStar')}
                  onPress={() => {
                    toggleModalGive(errors);
                    console.log('________________________', errors, values);
                  }}
                />
              </View>
              <ModalApp
                visible={modalGive}
                iconLeft={
                  <Icon
                    name="arrow-left"
                    type="feather"
                    color={theme.color.gray}
                    onPress={() => toggleModalGive()}
                    tvParallaxProperties
                  />
                }
                headerText={t('common:chooseNumber')}
                headerStyles={Styles.headerStyles}
              >
                <View style={{ marginHorizontal: 16, paddingVertical: 16 }}>
                  <AppInputField
                    name={'quantity'}
                    placeholder="Nhập số lượng sao"
                    keyboardType="numeric"
                  />
                  <AppInputField
                    name={'content'}
                    placeholder="Nội dung"
                    multiline
                    customInputStyle={{ height: 80 }}
                  />
                  <View>
                    <View style={Styles.myStar}>
                      <AppText>
                        {t('common:myStar')} {userReducer.user.rate_month}
                      </AppText>
                    </View>
                  </View>
                  <AppButton
                    onPress={() => {
                      //   Alert.alert(t('common:errorGiveStar'), errors.user_received)
                      handleSubmit();
                    }}
                    title={t('common:send')}
                  />
                </View>
              </ModalApp>
            </>
          );
        }}
      </Formik>
    </ModalApp>
  );
};
