import { isEmpty } from 'lodash';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ActivityIndicator, Alert, View } from 'react-native';
// @ts-ignore
import JitsiMeet, {
  JitsiMeetView,
  JitsiMeetEvents,
  // @ts-ignore
} from 'react-native-jitsi-meet';
import Modal from 'react-native-modal';
import { useSelector } from 'react-redux';
import { AppHeader } from '../../Components';
import { CallContext } from '../../Context';
import { DOMAIN_JITSI } from '../../Helpers/jitsiConfig';
import useGetRoomDetail from '../../Hooks/useGetRoomDetail';
import { RootState } from '../../Redux/Reducers';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';
import { Icon } from 'react-native-elements';

interface IVideoCall {
  roomId: number | string;
}

const VideoCall = (props: IVideoCall) => {
  const userReducer = useSelector((state: RootState) => state.userReducer);
  const { t } = useTranslation();
  const dataUserInfo = userReducer?.data?.user;

  const { roomId, setRoomId } = useContext(CallContext);

  const [data, , error, refetch] = useGetRoomDetail({ room_id: roomId });
  //   const [force, setForceRender] = useState({});

  //   const navigation = useNavigation();

  useEffect(() => {
    if (error) {
      Alert.alert(t('common:notice'), error, [
        {
          onPress: () => setRoomId(0),
        },
      ]);
    }
  }, [error]);

  useEffect(() => {
    // refetch();
    return () => {
      setRoomId(0);
      JitsiMeet.endCall();
    };
  }, []);

  useEffect(() => {
    if (!isEmpty(data)) {
      // JitsiMeet.
      //   JitsiMeet && JitsiMeet.endCall();
      setTimeout(() => {
        const url = `${DOMAIN_JITSI}/${data?.room_id}`;
        // can also be only room name and will connect to jitsi meet servers
        const userInfo = {
          displayName: dataUserInfo?.full_name,
          email: dataUserInfo?.email,
          avatar: dataUserInfo?.img_url || 'http://gravatar.com/avatar/avatar',
        };
        // setForceRender(true);
        JitsiMeet.call(url, userInfo);
      }, 1000);
    }
  }, [data]);

  const onConferenceTerminated = (nativeEvent: any) => {
    if (__DEV__) {
      console.log('onConferenceTerminated', nativeEvent);
    }
    JitsiMeet.endCall();
    setRoomId(0);
  };

  const onConferenceJoined = (nativeEvent: any) => {
    if (__DEV__) {
      console.log('onConferenceJoined', nativeEvent);
    }
    // setForceRender({ ...force });
    /* Conference joined event */
  };

  const onConferenceWillJoin = (nativeEvent: any) => {
    if (__DEV__) {
      console.log('onConferenceWillJoin', nativeEvent);
    }
    // setForceRender({ ...force });
    /* Conference will join event */
  };

  //   if (!forceRender) {
  //     return <AppContainer isMenu />;
  //   }

  const Styles = useThemeAwareObject(styles);

  return (
    <Modal
      isVisible={Boolean(roomId)}
      style={[
        Styles.modalStyle,
        roomId > 0 ? {} : { height: 0, width: 0, opacity: 0 },
      ]}
      backdropOpacity={1}
      hasBackdrop={false}
    >
      {/* <View>
        <Icon
          name="arrow-left"
          type={'feather'}
          onPress={() => onConferenceTerminated(null)}
          tvParallaxProperties
        />
      </View> */}
      <JitsiMeetView
        onConferenceTerminated={onConferenceTerminated}
        onConferenceJoined={onConferenceJoined}
        onConferenceWillJoin={onConferenceWillJoin}
        style={Styles.jitsiContainer}
      />
    </Modal>
  );
};

export default VideoCall;
