import { StyleSheet } from 'react-native';
import { device } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    callContainer: {
      flex: 1,
      backgroundColor: theme.color.white,
      position: 'absolute',
      zIndex: 99999999999999,
      top: 0,
      left: 0,
      width: device.width,
      height: device.height,
    },
    jitsiContainer: {
      flex: 1,
      height: '100%',
      width: '100%',
      backgroundColor: 'transparent',
    },
    modalStyle: {
      margin: 0,
      backgroundColor: theme.color.white,
    },
  });
