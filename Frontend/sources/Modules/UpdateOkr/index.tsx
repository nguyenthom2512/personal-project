import { useNavigation, useRoute } from '@react-navigation/core';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  FlatList,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import { Button } from 'react-native-elements';
import { Icon } from 'react-native-elements/dist/icons/Icon';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';
import {
  AppButton,
  AppContainer,
  AppDropDown,
  AppInputField,
  AppText,
} from '../../Components';
import AppCheckboxField from '../../Components/AppCheckboxField';
import AppDropDownMultiple from '../../Components/AppDropDownMultiple';
import ErrorMessages from '../../Components/ErrorMessages';
import { IndicatorContext } from '../../Context';
import { measure } from '../../Helpers';
import useGetCreateOKR from '../../Hooks/getDataCreateOKR';
import useGetOKR from '../../Hooks/getOkr';
import useGetOkrDetail from '../../Hooks/getOkrDetail';
import { okrActions, raicAction } from '../../Redux/Actions';
import { RootState } from '../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import SuggestionList from '../CreateOKR/Components/SuggestionList';
import smart from '../CreateOKR/Data/smart';
import styles from './style';
import _ from 'lodash';
import { DDMMYYYY } from '../../Helpers/dateTime';
const RAIC_TYPE = {
  Responsible: 0,
  Accountable: 1,
  Inform: 2,
  Consult: 3,
};

const regMatch =
  /^((http|https):\/\/)?(www.)?(?!.*(http|https|www.))[a-zA-Z0-9_-]+(\.[a-zA-Z]+)+(\/)?.([\w\?[a-zA-Z-_%\/@?]+)*([^\/\w\?[a-zA-Z0-9_-]+=\w+(&[a-zA-Z0-9_]+=\w+)*)?$/;

const UpdateOkr = () => {
  const indicatorContext = useContext(IndicatorContext);
  const navigation = useNavigation();
  // const [loadingLocal, setLoading] = useState(false);

  const { t } = useTranslation();

  const userData = useSelector((state: RootState) => state.userReducer);
  const RaicData = useSelector((state: RootState) => state.raicReducer);
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const dispatch = useDispatch();
  const user = userData?.data?.user?.id;

  const defaultOkrResult = {
    user,
    okr: '',
    key_result: '',
    target: '',
    current_done: 0,
    unit: '',
    plan_url: '',
    result_url: '',
    deadline: '',
  };
  const initialValues = {
    user, //  login => user
    department: '', //   phòng ban
    okr_parent: '', // okr cấp trên
    object_name: '', // mục tiêu okr
    is_done: true, // check false
    confident: 0, // mức độ tự tin
    okr_relate: [''], // liên kết chéo
    challenge: false, // thách thức
    user_responsible: [], //lên
    user_accountable: [],
    user_inform: [],
    user_consult: [],
    percent_changed: 0,
    raic_user: [],
    okr_result: [defaultOkrResult],
  };

  const route = useRoute();
  const { id, refetch }: any = route.params;
  // const id = dataOkr.id;
  const [dataGetOkr, loadingDataOkr, errorOkr, refetchGetOKR] =
    useGetOkrDetail(id);
  indicatorContext.setLoading(loadingDataOkr);

  const [dataCreateOKR, loadingCreateOKR, errorCreateOKR, refetchCreateOKR] =
    useGetCreateOKR();

  const [dataOKRs, loadingOKR, errorOKR, refetchOKR] = useGetOKR();

  const [defaultOKR, setDefaultOKR] = useState<any>(initialValues);

  const ConvertApi = (params: any) => {
    let user_responsible: any = []; //lên
    let user_accountable: any = [];
    let user_inform: any = [];
    let user_consult: any = [];
    (params?.raic_user || []).map((e: any) => {
      switch (e.raic_type) {
        case RAIC_TYPE.Responsible:
          user_responsible.push(e.user);
          break;
        case RAIC_TYPE.Accountable:
          user_accountable.push(e.user);
          break;
        case RAIC_TYPE.Inform:
          user_inform.push(e.user);
          break;
        case RAIC_TYPE.Consult:
          user_consult.push(e.user);
          break;
        default:
          break;
      }
    });
    (params?.okr_result || []).map((e: any, index: any) => {
      if (e.plan_url === null) {
        params.okr_result[index].plan_url = '';
      }
      if (e.result_url == null) {
        params.okr_result[index].result_url = '';
      }
    });
    params['okr_relate'] = [''];
    params['raic_user'] = [];
    params['user_responsible'] = user_responsible;
    params['user_accountable'] = user_accountable;
    params['user_inform'] = user_inform;
    params['user_consult'] = user_consult;
    setDefaultOKR(params);
  };

  useEffect(() => {
    if (!isEmpty(dataGetOkr)) {
      ConvertApi(dataGetOkr);
    } else {
      setDefaultOKR(initialValues);
    }
  }, [dataGetOkr]);

  useEffect(() => {
    dispatch(raicAction.getOkrRaic());
  }, []);

  const dataOkrOwner = useSelector((state: any) => {
    return state.okrReducer.data;
  });

  const validateCreateOKR = Yup.object().shape({
    object_name: Yup.string().required(t('common:required')),
    confident: Yup.string().required(t('common:required')),
    user: Yup.string().required(t('common:required')),
    department: Yup.string().required(t('common:required')),
    okr_result: Yup.array().of(
      Yup.object().shape({
        user: Yup.string().required(t('common:required')),
        key_result: Yup.string().required(t('common:required')),
        target: Yup.string().required(t('common:required')),
        current_done: Yup.string().required(t('common:required')),
        unit: Yup.string().required(t('common:required')),
        deadline: Yup.string().required(t('common:required')),
        plan_url: Yup.string().matches(regMatch, t('common:requiredLink')),
        result_url: Yup.string().matches(regMatch, t('common:requiredLink')),
      }),
    ),
  });

  const [onObject, setOnObject] = useState(false);
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const showDatePicker = (index: any) => {
    setDatePickerVisibility(index.toString());
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date: any, setFieldValue: any) => {
    const selected = moment(date).toISOString();
    setFieldValue(`okr_result[${isDatePickerVisible}].deadline`, selected);
    hideDatePicker();
  };
  const department = (dataCreateOKR?.Department || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.department_name,
    };
  });

  const okrRelate = (dataOKRs || []).map((elm) => {
    return {
      value: elm?.id,
      label: elm?.owner_okr,
    };
  });

  const okrPeriod = (dataOkrOwner?.results || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.owner_okr,
    };
  });
  const okrUnit = (dataCreateOKR?.Unit || []).map((elm) => {
    return {
      value: elm?.id,
      label: elm?.unit_name,
    };
  });
  const RAIC = (RaicData?.data.results || []).map((elm: any) => {
    return {
      value: elm?.id,
      label: elm?.full_name,
    };
  });

  const handleChoseOKR = () => setOnObject(true);

  const handleDataRAIC = (values: any) => {
    if (values.user_responsible !== undefined) {
      values.user_responsible.map((e: any) => {
        {
          values.raic_user = [
            ...values.raic_user,
            {
              raic_type: RAIC_TYPE.Responsible,
              user: e,
              okr: null,
              todo: null,
            },
          ];
        }
      });
    }
    if (!isEmpty(values.user_accountable)) {
      values.user_accountable.map((e: any) => {
        {
          values.raic_user = [
            ...values.raic_user,
            {
              raic_type: RAIC_TYPE.Accountable,
              user: e,
              okr: null,
              todo: null,
            },
          ];
        }
      });
    }
    if (values.user_inform !== undefined)
      values.user_inform.map((e: any) => {
        {
          values.raic_user = [
            ...values.raic_user,
            {
              raic_type: RAIC_TYPE.Inform,
              user: e,
              okr: null,
              todo: null,
            },
          ];
        }
      });
    if (values.user_consult !== undefined)
      values.user_consult.map((e: any) => {
        {
          values.raic_user = [
            ...values.raic_user,
            {
              raic_type: RAIC_TYPE.Consult,
              user: e,
              okr: null,
              todo: null,
            },
          ];
        }
      });
    // if (values.okr_relate == '' || values.okr_relate == undefined)
    // delete values.okr_relate;

    delete values.user_responsible;
    delete values.user_accountable;
    delete values.user_inform;
    delete values.user_consult;
  };

  const onSubmit = (values: any) => {
    handleDataRAIC(values);
    indicatorContext.setLoading(true);
    dispatch(
      okrActions.updateOkrRequest(values, {
        onSuccess: (success: any) => {
          indicatorContext.setLoading(false);
          Alert.alert(t('common:updateSuccess'), '', [
            {
              text: t('common:ok'),
              onPress: () => {
                refetch();
                navigation.goBack();
              },
            },
          ]);
        },
        onFailed: (error: any) => {
          indicatorContext.setLoading(false);
          Alert.alert(t('common:updateFailed'), error.errorMessage);
        },
      }),
    );
  };
  const setDataOkrOwner = (value: any) => {
    dispatch(okrActions.getListOkrFilter({ department: value }));
  };

  return (
    <AppContainer title={t('OKR:updateOkr')} isBack>
      <ScrollView>
        <Formik
          initialValues={defaultOKR}
          validationSchema={validateCreateOKR}
          enableReinitialize
          onSubmit={onSubmit}
        >
          {({
            handleChange,
            values,
            handleSubmit,
            setFieldValue,
            setValues,
            errors,
          }) => {
            // return null;
            return (
              <View>
                <View
                  style={{
                    //phan1
                    backgroundColor: theme.color.backgroundColor,
                    paddingHorizontal: theme.spacing.p16,
                  }}
                >
                  <AppDropDown
                    value={values.department}
                    isSearch
                    data={department}
                    onChoose={(value) => {
                      setFieldValue('department', value.value),
                        setDataOkrOwner(value);
                    }}
                    placeHolder={`${t('common:select')} ${t(
                      'createOkr:Okr:department',
                    )}`}
                    // customContainer={{
                    //   marginTop: theme.spacing.p16,
                    //   marginBottom: theme.spacing.p12,
                    // }}
                  />
                  <ErrorMessages name="department" />
                  <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ flex: 1 }}>
                      <AppDropDown
                        value={values.okr_parent}
                        isSearch
                        // title={`(R) ${t('createOkr:Okr:okrParent')}`}
                        placeHolder={t('createOkr:Okr:okrParent')}
                        data={okrPeriod}
                        onChoose={(value) => {
                          handleChoseOKR();
                          setFieldValue('okr_parent', value.value);
                        }}
                        // notify={
                        //   <Icon
                        //     type="material"
                        //     name="info"
                        //     color={theme.color.blueChathams}
                        //     onPress={() => Alert.alert('', smart.R)}
                        //   />
                        // }
                      />
                    </View>
                  </View>
                  <AppInputField
                    // title={t('createOkr:Okr:objectName')}
                    placeholder={`${t('createOkr:Okr:objectName')}`}
                    name="object_name"
                    multiline={true}
                    customInputStyle={{
                      height: 120,
                      justifyContent: 'flex-start',
                    }}
                  />
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      alignItems: 'flex-end',
                    }}
                  >
                    <AppCheckboxField
                      name="challenge"
                      title={`(S) ${t('createOkr:Okr:okrChallenge')}`}
                      onPress={(checked) => setFieldValue('challenge', checked)}
                    />
                    <Icon
                      type="material"
                      name="info"
                      color={theme.color.blueChathams}
                      onPress={() => Alert.alert('', smart.S)}
                    />
                  </View>
                  <AppDropDownMultiple
                    value={values.user_responsible}
                    isSearch
                    placeholder={t('createOkr:Okr:userResponsible')}
                    // title={t('createOkr:Okr:userResponsible')}
                    data={RAIC}
                    onChoose={(value) =>
                      setFieldValue('user_responsible', value)
                    }
                  />
                  <AppDropDownMultiple
                    value={values.user_accountable}
                    isSearch
                    placeholder={t('createOkr:Okr:userAccountable')}
                    data={RAIC}
                    onChoose={(value) =>
                      setFieldValue('user_accountable', value)
                    }
                  />
                  <AppDropDownMultiple
                    value={values.user_inform}
                    isSearch
                    placeholder={t('createOkr:Okr:userInform')}
                    data={RAIC}
                    onChoose={(value) => setFieldValue('user_inform', value)}
                  />
                  <AppDropDownMultiple
                    value={values.user_consult}
                    isSearch
                    placeholder={t('createOkr:Okr:userConsult')}
                    data={RAIC}
                    onChoose={(value) => setFieldValue('user_consult', value)}
                  />
                </View>

                <View
                  style={{
                    //phan2
                    paddingHorizontal: theme.spacing.p16,
                    paddingTop: theme.spacing.p24,
                    backgroundColor: theme.color.lightGray,
                  }}
                >
                  <FlatList
                    data={values.okr_result}
                    renderItem={({ item, index }) => {
                      return (
                        <>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}
                          >
                            <AppText
                              style={{
                                fontSize: theme.fontSize.f14,
                                fontWeight: 'bold',
                              }}
                            >
                              {t('createOkr:OkrKR:keyResult')}
                            </AppText>
                            {index >= 1 && (
                              <Icon
                                type="font-awesome"
                                name="minus-circle"
                                color={theme.color.torchRed}
                                onPress={() => {
                                  const newValue = [...values.okr_result];
                                  newValue.splice(index, 1);
                                  setFieldValue('okr_result', newValue);
                                }}
                              />
                            )}
                          </View>
                          {/* 
                          <SuggestionList
                            values={values}
                            setValues={setValues}
                            valueOKRParent={values?.okr_parent}
                            dataOKRParent={dataOkrOwner?.data.results || []}
                            index={index}
                          /> */}

                          <View style={{ flex: 1 }}>
                            <AppInputField
                              placeholder={`${t('createOkr:Okr:import')} ${t(
                                'createOkr:OkrKR:keyResult',
                              )}`}
                              name={`okr_result[${index}].key_result`}
                              //    title={`(A) ${t('createOkr:OkrKR:keyResult')}`}
                            />
                          </View>

                          {/* <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}
                            > */}
                          <View style={{ flex: 1 }}>
                            <AppInputField
                              // title={`(M) ${t('createOkr:OkrKR:target')}`}
                              placeholder={`${t('createOkr:Okr:import')} ${t(
                                'createOkr:OkrKR:target',
                              )}`}
                              name={`okr_result[${index}].target`}
                              keyboardType="numeric"
                            />
                          </View>
                          <View style={{ flex: 1 }}>
                            <AppDropDown
                              // title={t('createOkr:OkrKR:unit')}
                              placeHolder={t('createOkr:OkrKR:unit')}
                              value={values.okr_result[index]?.unit}
                              isSearch
                              onChoose={(value) =>
                                setFieldValue(
                                  `okr_result[${index}].unit`,
                                  value.value,
                                )
                              }
                              data={okrUnit}
                            />
                            <ErrorMessages name={`okr_result[${index}].unit`} />
                          </View>

                          <AppInputField
                            placeholder={`${t('createOkr:Okr:import')} ${t(
                              'createOkr:OkrKR:planUrl',
                            )}`}
                            name={`okr_result[${index}].plan_url`}
                          />
                          <AppInputField
                            // title={t('createOkr:OkrKR:resultUrl')}
                            placeholder={`${t('createOkr:Okr:import')} ${t(
                              'createOkr:OkrKR:resultUrl',
                            )}`}
                            name={`okr_result[${index}].result_url`}
                          />
                          <View
                            style={{
                              flexDirection: 'row',
                              alignItems: 'flex-end',
                            }}
                          >
                            <View
                              style={{
                                flex: 1,
                                marginVertical: theme.spacing.p4,
                              }}
                            >
                              {/* <AppText>(T) Thời gian hoàn thành</AppText> */}
                              {/* <View
                                style={{
                                  flexDirection: 'row',
                                  justifyContent: 'space-between',
                                }}
                              >
                                <AppText style={Styles.txtTitle}>
                                  {`(T) ${t('createOkr:OkrKR:deadline')}`}
                                </AppText>
                                <Icon
                                  type="material"
                                  name="info"
                                  color={theme.color.blueChathams}
                                  onPress={() => Alert.alert('', smart.T)}
                                />
                              </View> */}
                              <View>
                                <TouchableOpacity
                                  style={{
                                    borderWidth: 1,
                                    borderColor: theme.color.holderPlace,
                                    height: measure.inputHeight,
                                    justifyContent: 'center',
                                    borderRadius: 4,
                                    backgroundColor:
                                      theme.color.backgroundColor,
                                  }}
                                  onPress={() => showDatePicker(index)}
                                >
                                  <AppText
                                    style={{
                                      color: values.okr_result[index].deadline
                                        ? theme.color.black
                                        : theme.color.grayAbbey,
                                      fontSize: values.okr_result[index]
                                        .deadline
                                        ? theme.fontSize.f16
                                        : theme.fontSize.f14,
                                      paddingHorizontal: theme.spacing.p12,
                                    }}
                                  >
                                    {values.okr_result[index].deadline
                                      ? moment(
                                          values.okr_result[index].deadline,
                                        ).format(DDMMYYYY)
                                      : t('createOkr:OkrKR:deadline')}
                                  </AppText>
                                </TouchableOpacity>
                                <ErrorMessages
                                  name={`okr_result[${index}].deadline`}
                                />
                              </View>
                            </View>
                          </View>
                        </>
                      );
                    }}
                    keyExtractor={(_, index) => `${index}`}
                  />

                  <TouchableOpacity
                    onPress={() =>
                      setFieldValue('okr_result', [
                        ...values.okr_result,
                        defaultOkrResult,
                      ])
                    }
                    style={Styles.buttonAdd}
                  >
                    <Icon
                      name="add"
                      type="ionicon"
                      color={theme.color.violet}
                    />
                    <AppText style={Styles.titleAdd}>{`${t(
                      'createOkr:Okr:import',
                    )} ${t('createOkr:OkrKR:keyResult')}`}</AppText>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    //phan3
                    backgroundColor: theme.color.backgroundColor,
                    paddingHorizontal: theme.spacing.p16,
                    paddingTop: theme.spacing.p16,
                  }}
                >
                  {(values.okr_relate || []).map((elm: any, index: any) => {
                    return (
                      <View
                        key={index}
                        style={{
                          flexDirection: 'row',
                          alignItems: 'flex-start',
                        }}
                      >
                        <View style={{ flex: 1, backgroundColor: 'red' }}>
                          {/* <AppDropDown
                            value={values?.okr_relate?.[index]}
                            isSearch
                            placeHolder={t('createOkr:Okr:okrRelate')}
                            data={okrRelate}
                            onChoose={(value) =>
                              setFieldValue(
                                `okr_relate.[${index}]`,
                                value.value,
                              )
                            }
                            notify={
                              index >= 1 ? (
                                <Icon
                                  type="font-awesome"
                                  name="minus-circle"
                                  color={theme.color.torchRed}
                                  onPress={() => {
                                    const newValue = [...values.okr_relate];
                                    setFieldValue(
                                      'okr_relate',
                                      newValue.filter(
                                        (_, idx) => idx !== index,
                                      ),
                                    );
                                  }}
                                />
                              ) : (
                                <View />
                              )
                            }
                          /> */}
                        </View>
                      </View>
                    );
                  })}

                  <TouchableOpacity
                    onPress={() => {
                      setFieldValue(`okr_relate`, [...values.okr_relate, '']);
                    }}
                    style={Styles.buttonAdd}
                  >
                    <Icon
                      name="add"
                      type="ionicon"
                      color={theme.color.violet}
                    />
                    <AppText style={Styles.titleAdd}>{`${t(
                      'createOkr:Okr:import',
                    )} ${t('createOkr:Okr:okrRelate')}`}</AppText>
                  </TouchableOpacity>

                  <AppButton
                    containerStyle={{ marginTop: theme.spacing.p8 }}
                    onPress={() => handleSubmit()}
                    title={t('common:create')}
                  />
                  <DateTimePickerModal
                    locale="vi"
                    isVisible={Boolean(isDatePickerVisible)}
                    mode="date"
                    onConfirm={(date) => handleConfirm(date, setFieldValue)}
                    onCancel={hideDatePicker}
                    minimumDate={new Date()}
                  />
                </View>
              </View>
            );
          }}
        </Formik>
      </ScrollView>
    </AppContainer>
  );
};

export default UpdateOkr;
