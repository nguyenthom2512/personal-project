import { useNavigation } from '@react-navigation/native';
import moment from 'moment';
import React, {
  useCallback,
  useContext,
  useEffect,
  useRef,
  useState,
} from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  DeviceEventEmitter,
  LayoutChangeEvent,
  Platform,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { useDispatch, useSelector } from 'react-redux';
import Diamond from '../../Assets/Svg/diamond.svg';
import Letter from '../../Assets/Svg/letter.svg';
import Noted from '../../Assets/Svg/noted.svg';
import Star from '../../Assets/Svg/star.svg';
import { AppImage, AppItems, AppText } from '../../Components';
import { setTypeCalendar } from '../../Redux/Actions/calendarActions';
import {
  getCompletedWork,
  getTodoListRequest,
} from '../../Redux/Actions/todoListActions';
import { RootState } from '../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import { CalendarType, EventCalendar } from '../AgendaCalendar';
import styles from './styles';
import AppHeader from '../../Components/AppHeader';
import Calendar from '../../Components/react-native-big-calendar/index';
import serviceUrls from '../../Services/serviceUrls';
import useGetTodoDashboard from '../../Hooks/getTodoDashboard';
import { noDateTodoRequest } from '../../Redux/Actions/noDateTodoActions';
import { device } from '../../Helpers';
import { getTodoDashboardRequest } from '../../Redux/Actions/todoDashboardActions';
import { FlatList } from 'react-native-gesture-handler';
import { TodoItem, TodoItemComponent } from '../../Components/TodoItems';
import DraggableView from '../../Components/DraggableView';
import {
  dateFormat,
  EventName,
  minuteInHour,
  pixelHourCalendar,
} from '../../Helpers/constant';
import { apiPatch } from '../../Services/serviceHandle';
import { IndicatorContext } from '../../Context';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { userActions } from '../../Redux/Actions';
import useGetNotification from '../../Hooks/getNotification';

interface Props {}
let page = 1;
const page_size = 20;
let is_done = false;
const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';

const HEIGHT_TEMPLATE = 896;

export default () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { setLoading } = useContext(IndicatorContext);

  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();

  const { t } = useTranslation();
  const todoListReducer = useSelector(
    (state: RootState) => state.todoListReducer,
  );
  const noDateTodoReducer = useSelector(
    (state: RootState) => state.noDateTodoReducer,
  );
  const dashboardReducer = useSelector(
    (state: RootState) => state.todoDashboardReducer.data,
  );

  const now: any = moment().format(dateFormat.year_month_day);
  const [todoItem, setTodoItem] = useState<{
    item: TodoItem;
    locationY: number;
  } | null>(null);
  const userData = useSelector((state: RootState) => state.userReducer);
  const contentOffsetY = useRef(0);
  const [dailyPlanLayout, setDailyPlanLayout] = useState({
    left: 0,
    top: 0,
    width: 50,
    height: 50,
  });
  const [contentLayout, setContentLayout] = useState({
    left: 0,
    top: 0,
    width: 50,
    height: 50,
  });
  const minY = contentLayout.top + 70;
  const maxRangeY = contentLayout.top + dailyPlanLayout.height;
  const draggableViewWidth = dailyPlanLayout.width - theme.spacing.p16;
  const onGetDailyPlanLayout = useCallback((event: LayoutChangeEvent) => {
    const { x, y, height, width } = event.nativeEvent.layout;
    setDailyPlanLayout({
      left: x,
      top: y,
      width: width,
      height: height,
    });
  }, []);

  const onGetContentLayout = useCallback((event: LayoutChangeEvent) => {
    const { x, y, height, width } = event.nativeEvent.layout;
    setContentLayout({
      left: x,
      top: y,
      width: width,
      height: height,
    });
  }, []);
  const onDrop = (value: number, itemEvent: TodoItem) => {
    setTodoItem(null);
    const start =
      ((value + contentOffsetY.current) * minuteInHour) / pixelHourCalendar;
    onUpdateTodo(
      { startTime: start, endTime: start + minuteInHour },
      itemEvent.id,
    );
  };

  const hourMustMinus: number =
    Platform.OS === 'ios'
      ? (device.height * 2) / HEIGHT_TEMPLATE - 0.7
      : (device.height * 1.5) / HEIGHT_TEMPLATE;

  useEffect(() => {
    const start_time = moment().format(dateFormat.year_month_day);
    const end_time = moment().add(1, 'days').format(dateFormat.year_month_day);
    dispatch(getTodoListRequest({ start_time, end_time }));
    dispatch(noDateTodoRequest({ page, page_size }));
    dispatch(getCompletedWork({ page, page_size, is_done }));
    dispatch(getTodoDashboardRequest());
  }, []);

  const data = () => {
    const dataMap: any = todoListReducer?.data?.[now];
    return dataMap?.map((el: any) => ({
      id: el.id,
      title: el.todo_name,
      start: moment(el.start_time).toDate(),
      end: moment(el.end_time).toDate(),
      type: el?.priority?.id,
      color: el.priority.color,
    }));
  };

  const renderEvent = (data: EventCalendar, touchableOpacityProps: any) => {
    const { title, end, type } = data;
    touchableOpacityProps.style[2].backgroundColor = data.color;
    touchableOpacityProps.style[2].flexDirection = 'row';
    touchableOpacityProps.style[2].justifyContent = 'space-between';

    return (
      <TouchableOpacity {...touchableOpacityProps}>
        <AppText style={[{ fontSize: theme.fontSize.f14 }]}>{title}</AppText>
        {moment(end).isBefore(moment()) && (
          <TouchableOpacity
            onPress={() => {
              Alert.alert(
                t('common:notice'),
                t('common:overdueWork', {
                  time: moment(end).fromNow(),
                }),
              );
            }}
          >
            <AntDesign name="warning" size={20} color={theme.color.orange} />
          </TouchableOpacity>
        )}
      </TouchableOpacity>
    );
  };

  const onUpdateTodo = async (
    data: { startTime: number; endTime: number },
    todoId?: number,
  ) => {
    const { startTime, endTime } = data;

    setLoading(true);
    try {
      const res = await apiPatch(`${serviceUrls.url.todo}${todoId}`, {
        start_time: moment()
          .startOf('dates')
          .add(startTime, 'minutes')
          .format(dateFormat.dateTimeServer),
        end_time: moment()
          .startOf('dates')
          .add(endTime, 'minutes')
          .format(dateFormat.dateTimeServer),
        date: moment().startOf('dates').format(dateFormat.year_month_day),
      });
      if (res && res.error) {
        let errorMessage = res.detail?.message?.toString() || res.errorMessage;
        if (errorMessage === 'cannot start two todo in the same time') {
          errorMessage = t('todoDetail:haveTodoThisTime');
        }
        Alert.alert(t('common:notice'), errorMessage);
      }

      const start_time = moment().format(dateFormat.year_month_day);
      const end_time = moment()
        .add(1, 'days')
        .format(dateFormat.year_month_day);

      dispatch(noDateTodoRequest({ page: 1, page_size }));
      dispatch(getTodoListRequest({ start_time, end_time }));
    } catch (error: any) {
      Alert.alert(
        t('common:notice'),
        error?.toString() || t('createMissions:createTodoError'),
      );
    } finally {
      setLoading(false);
    }
  };

  const handleNavigateCalendar = (isTodayCalendar: boolean) => {
    if (isTodayCalendar) {
      dispatch(setTypeCalendar(CalendarType.TODAY));
    } else {
      dispatch(setTypeCalendar(CalendarType.DAY));
    }
    navigation.navigate('Calendar', { isTodayCalendar: isTodayCalendar });
  };

  const [dataDashboard, loadingDashboard, errorDashboard, refetchDashboard] =
    useGetTodoDashboard();
  useEffect(() => {
    //   !loadingDashboard && refetchDashboard();
    dispatch(userActions.getUserRequest(userData.data.user.id));
    refetchDashboard();
    DeviceEventEmitter.addListener(EventName.onReceivedNotification, () => {
      setTimeout(() => {
        dispatch(getTodoDashboardRequest());
      }, 2000);
    });
  }, []);

  // useEffect(()=> {

  // },[])

  const renderItems = () => (
    <>
      <AppItems
        icons={<Noted />}
        number={dashboardReducer?.todo_done}
        containerStyles={{ backgroundColor: theme.color.jadeGreen }}
        onNavigation={() => navigation.navigate('CompletedWork')}
      />
      <AppItems
        icons={<Letter />}
        number={dashboardReducer?.todo_doing}
        containerStyles={{ backgroundColor: theme.color.violet }}
        onNavigation={() =>
          navigation.navigate('Calendar', { isPriority: true, isDone: false })
        }
      />
      <AppItems
        icons={<Star />}
        number={dashboardReducer?.total_rate}
        containerStyles={{ backgroundColor: theme.color.yellow }}
        onNavigation={() => navigation.navigate('Profile', { isMyStar: true })}
      />
      <AppItems
        icons={<Diamond />}
        number={dashboardReducer?.diamond}
        containerStyles={{ backgroundColor: theme.color.lightOrange }}
        onNavigation={() => navigation.navigate('ItemStore')}
      />
    </>
  );

  const renderHeader = (text: string, isTodayCalendar: boolean) => (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: theme.spacing.p12,
      }}
    >
      <AppText style={Styles.textTodo}>{text}</AppText>
      <TouchableOpacity onPress={() => handleNavigateCalendar(isTodayCalendar)}>
        <Feather
          name="more-horizontal"
          size={24}
          color={`${theme.color.blackTitle}50`}
        />
      </TouchableOpacity>
    </View>
  );

  return (
    <>
      <AppHeader
        title={t('homepage:todoListApp')}
        iconRight="bell"
        typeIconRight="feather"
        onRightPress={() => {
          navigation.navigate('Notification');
        }}
        isShowBadge
        value={
          dashboardReducer?.unread_notification > 9
            ? '9+'
            : dashboardReducer?.unread_notification
        }
      />
      <View style={Styles.containerItems}>
        <View style={Styles.infoContainer}>
          <View style={{ flexDirection: 'column' }}>
            <AppText style={Styles.name}>
              {t('homepage:hi', { user: userData?.data?.user?.full_name })}
            </AppText>
            <AppText style={Styles.dateToday}>
              {t('homepage:todayIs') + moment().format('DD/MM/YYYY')}
            </AppText>
          </View>
          <AppImage
            source={{
              uri: userData?.data?.user?.img_url
                ? `${serviceUrls.url.IMAGE}${userData?.data?.user?.img_url}`
                : DEFAULT_AVATAR,
            }}
            imageStyle={Styles.imageStyles}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}
        >
          {renderItems()}
        </View>
      </View>
      <View onLayout={onGetContentLayout} style={Styles.contentContainer}>
        <View style={Styles.paddingDailyPlan}>
          <View onLayout={onGetDailyPlanLayout} style={Styles.containerWork}>
            {renderHeader(t('homepage:workToday'), true)}
            <Calendar
              onScroll={(event: {
                nativeEvent: { contentOffset: { y: number } };
              }) => {
                contentOffsetY.current = event.nativeEvent.contentOffset.y;
              }}
              date={new Date()}
              renderHeader={() => <View />}
              mode="day"
              weekEndsOn={0}
              events={data() || []}
              scrollOffsetMinutes={
                (moment().get('hours') - hourMustMinus) * pixelHourCalendar
              }
              height={600}
              onPressEvent={(e) =>
                navigation.navigate('TodoDetails', { id: e.id })
              }
              renderEvent={renderEvent}
              swipeEnabled={false}
              theme={{
                typography: {
                  xs: {
                    fontSize: theme.fontSize.f14,
                  },
                },
                palette: {
                  nowIndicator: theme.color.blackBox,
                  gray: {
                    '100': theme.color.grayShaft,
                    '200': theme.color.whiteGray,
                    '300': theme.color.gray,
                  },
                },
              }}
            />
          </View>
        </View>

        <View style={Styles.paddingTodoList}>
          <View style={Styles.containerWork}>
            {renderHeader(t('homepage:todoList'), false)}
            <FlatList
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{ marginTop: 2 }}
              style={{ flexGrow: 1, opacity: !!todoItem ? 0.5 : 1 }}
              data={noDateTodoReducer?.data?.results || []}
              renderItem={({ item }) => {
                return (
                  <TodoItemComponent
                    data={item}
                    onLongPress={(location) =>
                      setTodoItem({ item: item, locationY: location })
                    }
                  />
                );
              }}
              keyExtractor={(item) => `${item.id}`}
            />
          </View>
        </View>
      </View>

      {!!todoItem && (
        <View style={[Styles.draggableView, { top: todoItem.locationY - 40 }]}>
          <DraggableView
            children={
              <TodoItemComponent data={todoItem.item} onItemPress={() => {}} />
            }
            width={draggableViewWidth}
            maxX={0}
            minX={0}
            minY={minY}
            maxRangeY={maxRangeY}
            maxY={todoItem.locationY}
            space={0}
            onDrop={(value) => onDrop(value, todoItem.item)}
            onReset={() => setTodoItem(null)}
          />
        </View>
      )}
    </>
  );
};
