import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    containerItems: {
      paddingHorizontal: theme.spacing.p12,
      backgroundColor: theme.color.white,
      paddingVertical: theme.spacing.p16,
    },
    header: {
      backgroundColor: theme.color.blackHeader,
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingHorizontal: theme.spacing.p16,
      padding: theme.spacing.p12,
    },
    text: {
      color: theme.color.white,
      fontSize: theme.fontSize.f18,
      fontWeight: 'bold',
    },
    textTodo: {
      color: theme.color.shark,
      fontSize: theme.fontSize.f18,
      fontWeight: 'bold',
      marginBottom: theme.spacing.p12,
    },
    containerTodo: {
      backgroundColor: theme.color.dartBackground,
      paddingVertical: theme.spacing.p16,
      borderRadius: 4,
    },
    name: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
      marginBottom: 6,
    },
    dateToday: {
      marginBottom: theme.spacing.p16,
      fontSize: theme.fontSize.f14,
      color: theme.color.shark,
      opacity: 0.7,
    },
    topBox: {
      backgroundColor: theme.color.blackBox,
      paddingVertical: theme.spacing.p16,
      borderRadius: 4,
    },
    containerWork: {
      backgroundColor: theme.color.white,
      paddingHorizontal: theme.spacing.p8,
      paddingVertical: theme.spacing.p16,
      borderRadius: 4,
      flex: 1,
      zIndex: 1,
    },
    imageStyles: {
      width: 40,
      height: 40,
      borderRadius: theme.spacing.p20,
    },
    infoContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    contentContainer: { backgroundColor: theme.color.mercury, flex: 1 },
    paddingDailyPlan: {
      paddingHorizontal: theme.spacing.p16,
      marginTop: 16,
      flex: 1,
    },
    paddingTodoList: {
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
      flex: 1,
      flexGrow: 1,
    },
    draggableView: {
      position: 'absolute',
      width: '100%',
      alignItems: 'center',
    },
  });
  return styles;
};
