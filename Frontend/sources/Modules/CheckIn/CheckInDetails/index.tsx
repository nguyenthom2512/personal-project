import { useNavigation, useRoute } from '@react-navigation/native';
import { ErrorMessage, Formik } from 'formik';
import { isEmpty } from 'lodash';
import moment from 'moment';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  FlatList,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import { Tab, TabView } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import * as yup from 'yup';
import { AppHeader, AppText } from '../../../Components';
import AppButton from '../../../Components/AppButton';
import CheckInComponent from '../../../Components/CheckInComponent';
import CheckInCollapse from '../../../Components/CheckInComponent/CheckInCollapse';
import CheckInDetailComponent from '../../../Components/CheckInComponent/CheckInDetailComponent';
import { IndicatorContext } from '../../../Context';
import { DDMMYYYY } from '../../../Helpers/dateTime';
import { OkrResult } from '../../../interfaces/okr.interface';
import {
  getCheckInDetails,
  postCreateCheckIn,
} from '../../../Redux/Actions/checkInActions';
import { RootState } from '../../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './styles';

interface Props {}

export default (props: Props) => {
  const route = useRoute<any>();
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { id, onRefresh, isDone } = route.params;
  const { setLoading } = useContext(IndicatorContext);

  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const navigation = useNavigation();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCheckInDetails(id));
  }, []);

  const checkInDetailsReducer = useSelector(
    (state: RootState) => state.checkInReducer,
  );
  const checkInDetails = checkInDetailsReducer?.dataDetails;

  const [index, setIndex] = useState(0);
  const CheckInDetailSchema = yup.object().shape({
    confident: yup.string().required(t('checkIn:required')),
    user: yup.string().required(t('checkIn:required')),
    checkin_date: yup.date().when('is_done', {
      is: false,
      then: yup
        .date()
        .typeError('Thời gian sai định dạng ')
        .required(t('checkIn:required')),
    }),
    // checkin_date: yup.string().nullable().required(t('checkIn:required')),
    okr_result: yup.array().of(
      yup.object().shape({
        result: yup.string().required(t('checkIn:required')),
        confident: yup.string().nullable().required(t('checkIn:required')),
        overdue_note: yup.string().required(t('checkIn:required')),
        process_note: yup.string().required(t('checkIn:required')),
        solution_note: yup.string().required(t('checkIn:required')),
        total_done: yup
          .number()
          .typeError(t('createOkr:targeTypeError'))
          .required(t('checkIn:required')),
        target: yup.number().required(t('checkIn:required')),
        trouble_note: yup.string().required(t('checkIn:required')),
        processed: yup.string().required(t('checkIn:required')),
      }),
    ),
  });

  const checkDisable = (el: any) => {
    const index = checkInDetails?.ok_checkin[0]?.checkin_result?.findIndex(
      (value: any) => value?.result == el.id && value.is_done,
    );
    return index;
  };

  const fillData = (name: string, index: number, el: OkrResult) => {
    if (checkDisable(el) >= 0) {
      return checkInDetails.ok_checkin[0].checkin_result[checkDisable(el)]?.[
        name
      ].toString();
    }
    return '';
  };

  const processInitialValue = () => {
    const newData = (checkInDetails?.okr_result || []).map((elm: OkrResult) => {
      return {
        ...elm,
        overdue_note: fillData(`overdue_note`, index, elm),
        process_note: fillData(`process_note`, index, elm),
        solution_note: fillData(`solution_note`, index, elm),
        trouble_note: fillData(`trouble_note`, index, elm),
        total_done: fillData(`total_done`, index, elm),
        result: elm.id,
        target: elm.target.toString(),
        confident: fillData(`confident`, index, elm),
        is_done: !!fillData(`is_done`, index, elm),
      };
    });

    const oldData = {
      ...checkInDetails,
      confident: checkInDetails.confident
        ? checkInDetails.confident.toString()
        : '',
      okr_result: newData,
      checkin_date: '',
      is_done: false,
      user,

      old_checkin: checkInDetails?.ok_checkin || [],
    };

    return oldData;
  };

  const onSubmit = (values: any) => {
    const dataCheckin = (values?.okr_result).map((el: any) => {
      return {
        confident: el?.confident,
        is_done: el?.is_done,
        overdue_note: el?.overdue_note,
        process_note: el?.process_note,
        result: el?.result,
        solution_note: el?.solution_note,
        total_done: el?.total_done,
        trouble_note: el?.trouble_note,
      };
    });

    /**
     * today => hom nay
     * createtime => ngày tạo ra bản checkin
     * checkindate => ngày checkin tiếp theo
     *
     * today === createtime => checkin dung han\\ đã checkin
     * checkindate > today > createtime => chua checkin
     * today > checkindate => checkin muon
     *
     */

    const params = {
      user: values.user,
      okr: id,
      checkin_result: dataCheckin,
      checkin_date: values.checkin_date,
      is_done: values.is_done,
      confident: values.confident,
      checkin_status: 1,
    };
    if (values.is_done) {
      delete params.checkin_date;
    }
    if (values && values.ok_checkin && values.ok_checkin.length > 0) {
      if (
        moment(values.created_time)
          .startOf('day')
          .isAfter(moment(values.ok_checkin[0].checkin_date).startOf('day'))
      ) {
        params.checkin_status = 0;
      }
    }
    setLoading(true);

    dispatch(
      postCreateCheckIn(params, {
        onError: (er) => {
          setLoading(false);
          Alert.alert(t('common:notice'), er.toString());
        },
        onSuccess: () => {
          setLoading(false);
          Alert.alert(t('common:notice'), t('checkIn:checkinSuccess'));
        },
      }),
    );
  };

  useEffect(() => {}, []);

  const checkinTime = () => {
    if (checkInDetails.is_done) return t('checkIn:done');
    if (isEmpty(checkInDetails.ok_checkin)) return t('checkIn:checkInRequired');
    const dataLastCheckin = checkInDetails.ok_checkin[0];
    return moment(dataLastCheckin?.checkin_date).format(DDMMYYYY);
  };

  const percent = (value: any) => {
    if (value <= 0.25) {
      return theme.color.cinnabar;
    }
    if (value <= 0.75) {
      return theme.color.fireBush;
    }
    return theme.color.greenBox;
  };

  return (
    <Formik
      initialValues={processInitialValue()}
      onSubmit={onSubmit}
      validationSchema={CheckInDetailSchema}
      //   validateOnBlur={true}
      enableReinitialize
      validateOnChange={true}
    >
      {(formikProps) => {
        return (
          <>
            <AppHeader
              title={t('checkIn:checkInOkrs')}
              iconLeft="arrow-back"
              typeIconLeft="ionicons"
              onLeftPress={() => {
                onRefresh();
                navigation.goBack();
              }}
            />
            <View style={Styles.header}>
              <AppText style={Styles.objectName} numberOfLines={2}>
                {checkInDetails?.object_name}
              </AppText>
              <View style={Styles.checkInDay}>
                <AppText style={Styles.text}>{t('checkIn:checkInDay')}</AppText>
                <AppText style={{ flex: 1 }}>{checkinTime()}</AppText>
              </View>
              <View style={Styles.checkInDay}>
                <AppText style={Styles.text}>
                  {t('checkIn:implementationProgress')}
                </AppText>
                <View style={{ flex: 1 }}>
                  <TouchableOpacity
                    style={[
                      {
                        backgroundColor: percent(
                          checkInDetails?.percent_completed,
                        ),
                      },
                      Styles.progressButton,
                    ]}
                  >
                    <AppText style={Styles.textProgress}>{`${(
                      checkInDetails?.percent_completed * 100
                    ).toFixed(2)}%`}</AppText>
                  </TouchableOpacity>
                  </View>
                {/* <View
                  style={{
                    backgroundColor: percent(checkInDetails?.percent_completed),
                    marginLeft: theme.spacing.p20,
                    alignItems: 'center',
                    borderRadius: 4,
                    paddingHorizontal: theme.spacing.p12,
                    paddingVertical: theme.spacing.p8,
                  }}
                >
                  <AppText
                    style={{
                      color: theme.color.white,
                      fontSize: theme.fontSize.f14,
                    }}
                  >
                    {`${(checkInDetails?.percent_completed * 100).toFixed(2)}%`}
                  </AppText>
                </View> */}
              </View>
            </View>
            <View style={{ flex: 1, marginTop: 4 }}>
              <Tab value={index} onChange={setIndex}>
                <Tab.Item
                  style={{ backgroundColor: theme.color.white }}
                  titleStyle={Styles.titleStyle}
                  title={t('checkIn:checkInOKR')}
                  activeOpacity={1}
                />
                <Tab.Item
                  activeOpacity={1}
                  style={{ backgroundColor: theme.color.white }}
                  titleStyle={Styles.titleStyle}
                  title={t('checkIn:historyCheckIn')}
                />
              </Tab>
              <TabView value={index} onChange={setIndex}>
                <TabView.Item
                  style={Styles.checkInContainer}
                  onMoveShouldSetResponder={(e) => {
                    e.stopPropagation();
                    return false;
                  }}
                >
                  <>
                    <View style={{ flex: 1 }}>
                      <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={Styles.collapseContainer}
                      >
                        <CheckInCollapse
                          okCheckIn={checkInDetails?.ok_checkin}
                          data={checkInDetails?.okr_result}
                          formik={formikProps}
                        />
                      </ScrollView>
                    </View>
                    {!isDone && (
                      <View style={{ paddingBottom: theme.spacing.p12 }}>
                        <CheckInComponent
                          onComplete={formikProps.handleSubmit}
                          formik={formikProps}
                        />
                      </View>
                    )}
                  </>
                </TabView.Item>
                <TabView.Item
                  style={Styles.tabHistory}
                  onMoveShouldSetResponder={(e) => {
                    e.stopPropagation();
                    return false;
                  }}
                >
                  <ScrollView showsVerticalScrollIndicator={false}>
                    <CheckInDetailComponent
                      dateCheckIn={formikProps?.values?.old_checkin}
                      // dateCheckIn={checkInDetails}
                      formik={formikProps}
                      dataResult={checkInDetails?.okr_result}
                    />
                  </ScrollView>
                </TabView.Item>
              </TabView>
            </View>
          </>
        );
      }}
    </Formik>
  );
};
