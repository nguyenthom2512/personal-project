import { StyleSheet } from 'react-native';
import { isIOS, ScreenHeight } from 'react-native-elements/dist/helpers';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    header: {
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p8,
      backgroundColor: theme.color.white,
    },
    objectName: {
      fontSize: theme.fontSize.f20,
    },
    text: {
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.mineShaft,
      flex: 1
    },
    checkInDay: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: theme.spacing.p8,
    },
    titleStyle: {
      textTransform: 'none',
      color: theme.color.violet,
      fontSize: theme.fontSize.f16,
      fontWeight: '500',
    },
    tabHistory: {
      width: '100%',
      marginTop: theme.spacing.p16,
      paddingHorizontal: theme.spacing.p16,
      borderRadius: 4,
      flex: 1
    },
    checkInContainer: {
      flex: 1,
    },
    collapseContainer: {
      paddingHorizontal: theme.spacing.p16,
      paddingTop: theme.spacing.p16,
      borderRadius: 4,
      flex: 1,
    },
    progressButton:{
      alignSelf:'flex-start',
      padding: theme.spacing.p8,
      borderRadius: theme.spacing.p4,
    },
    textProgress:{
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color:theme.color.white
    }
  });
  return styles;
};
