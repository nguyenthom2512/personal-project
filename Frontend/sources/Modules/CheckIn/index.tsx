import { useNavigation } from '@react-navigation/core';
import React, { useEffect, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ActivityIndicator, FlatList, Text, View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  AppDropDown,
  AppHeader,
  AppText,
  CheckInItems,
} from '../../Components';
import { DataItem } from '../../Components/AppDropDown';
import AppDropDownMultiple from '../../Components/AppDropDownMultiple';
import ModalCallRoom from '../../Components/CheckInComponent/ModalCallRoom';
import ModalApp from '../../Components/ModalApp';
import { QUARTER } from '../../Helpers/dateTime';
import useGetDepartment from '../../Hooks/getDepartment';
import { getCheckIn } from '../../Redux/Actions/checkInActions';
import ActionTypes from '../../Redux/ActionTypes';
import { RootState } from '../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import { Modalize } from 'react-native-modalize';

interface Props {}
let page = 1;

export default () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { theme } = useTheme();

  const refRoom = useRef<Modalize>(null);
  const [initData, setInitData] = useState('');

  const page_size = 10;
  let department: string | number = '';
  let created_time__range: string | number = '';
  const checkInReducer = useSelector(
    (state: RootState) => state.checkInReducer,
  );

  useEffect(() => {
    dispatch(getCheckIn({ page, page_size, department, created_time__range }));
    onRefresh();
  }, []);
  const onRefresh = () => {
    page = 1;
    dispatch(getCheckIn({ page, page_size, department, created_time__range }));
  };
  const onLoad = () => {
    if (checkInReducer.loading || page >= checkInReducer.noPage) {
      return;
    }
    page = page + 1;
    dispatch(getCheckIn({ page, page_size, department, created_time__range }));
  };
  const [dataDepartment] = useGetDepartment();
  const data = dataDepartment?.map((el) => ({
    label: el.department_name,
    value: el.id,
  }));
  const onChooseDepartment = (item: DataItem) => {
    page = 1;
    department = item.value > '0' ? item.value : '';
    dispatch(
      getCheckIn({
        page,
        page_size,
        department,
        created_time__range,
      }),
    );
  };
  const onChooseQuarter = (item: DataItem) => {
    page = 1;
    created_time__range = item.value;
    dispatch(getCheckIn({ page, page_size, department, created_time__range }));
  };
  return (
    <View style={{ flex: 1 }}>
      <AppHeader title={t('checkIn:checkIn')} isMenu />
      <View style={Styles.container}>
        <AppDropDown
          data={data}
          placeHolder={t('checkIn:chooseDepartment')}
          customContainer={Styles.dropdown}
          customContent={{ backgroundColor: theme.color.white }}
          onChoose={onChooseDepartment}
        />
        <AppDropDown
          placeHolder={t('checkIn:chooseQuarter')}
          data={QUARTER()}
          customContent={{ backgroundColor: theme.color.white }}
          onChoose={onChooseQuarter}
        />
        <AppText style={Styles.checkIn}>{t('checkIn:checkIn')}</AppText>
      </View>
      <View style={{ flex: 1, paddingHorizontal: theme.spacing.p16 }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 50 }}
          data={checkInReducer?.data?.results}
          extraData={checkInReducer?.data?.results.length}
          renderItem={({ item, index }) => {
            return (
              <CheckInItems
                data={item}
                toDetails={(isDone?: any) =>
                  navigation.navigate('CheckInDetails', {
                    id: item.id,
                    onRefresh,
                    isDone,
                  })
                }
                onModal={() => {
                  setInitData(item);
                  refRoom.current?.open();
                }}
              />
            );
          }}
          keyExtractor={(item, index) => item.id.toString()}
          onEndReached={onLoad}
          onEndReachedThreshold={0.5}
          onRefresh={onRefresh}
          refreshing={
            page === 1 && checkInReducer.type === ActionTypes.GET_CHECK_IN
          }
          ListFooterComponent={
            page < checkInReducer.noPage ? <ActivityIndicator /> : null
          }
        />
      </View>
      <ModalCallRoom
        ref={refRoom}
        onCloseModal={() => refRoom.current?.close()}
        initData={initData}
      />
    </View>
  );
};
