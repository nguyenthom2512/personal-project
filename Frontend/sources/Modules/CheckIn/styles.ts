import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    dropdown: {
      marginTop: theme.spacing.p16,
    },
    container: {
      paddingHorizontal: theme.spacing.p16,
    },
    checkIn: {
      fontSize: theme.spacing.p20,
      fontWeight: '400',
      marginTop: theme.spacing.p24,
      marginBottom: theme.spacing.p16
    },
  });
  return styles;
};
