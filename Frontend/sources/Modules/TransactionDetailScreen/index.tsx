import { useNavigation } from '@react-navigation/core';
import { useRoute } from '@react-navigation/native';
import moment from 'moment';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text } from 'react-native';
import { AppButton, AppContainer } from '../../Components';
import { FeedbackComment } from '../../interfaces/feedback.interface';
import { useTheme, useThemeAwareObject } from '../../Theme';
import RenderHtml from 'react-native-render-html';
import styles from './styles';
import { device } from '../../Helpers';

export default () => {
  const { theme } = useTheme();
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const navigation = useNavigation();
  const route = useRoute();

  const dataTransaction = route.params as FeedbackComment;

  const tl = (objName?: string, Obj?: Object) =>
    t(`transactionScreen:${objName}`, Obj);

  //   const goBack = () => {
  //     navigation.goBack();
  //   };

  console.log('dataTransaction', dataTransaction);
  const source = {
    html: dataTransaction.content ?? '',
  };
  return (
    <AppContainer isBack title={tl('title')}>
      <View style={{ paddingHorizontal: 16 }}>
        <View
          style={{
            backgroundColor: 'white',
            padding: 16,
            marginTop: 16,
            borderRadius: 4,
          }}
        >
          <Text style={{ fontSize: 16, fontWeight: '600', color: '#292D3A' }}>
            {tl('detailTransaction')}
          </Text>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 16,
              borderBottomWidth: 1,
              borderColor: '#E9E9E9',
            }}
          >
            <Text
              style={{ flex: 1, color: '#292D3A', opacity: 0.7, fontSize: 14 }}
            >
              {tl('id')}
            </Text>
            <Text
              style={{
                flex: 2,
                textAlign: 'right',
                color: '#292D3A',
                fontSize: 14,
              }}
            >
              {dataTransaction?.id}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 16,
              borderBottomWidth: 1,
              borderColor: '#E9E9E9',
            }}
          >
            <Text
              style={{ flex: 1, color: '#292D3A', opacity: 0.7, fontSize: 14 }}
            >
              {tl('userReceiver')}
            </Text>
            <Text
              style={{
                flex: 2,
                textAlign: 'right',
                color: '#292D3A',
                fontSize: 14,
              }}
            >
              {dataTransaction.user_received_data.full_name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              paddingVertical: 16,
              borderBottomWidth: 1,
              borderColor: '#E9E9E9',
            }}
          >
            <Text style={{ color: '#292D3A', opacity: 0.7, fontSize: 14 }}>
              {tl('content')}
            </Text>
            <RenderHtml source={source} contentWidth={device.width} />
            <Text
              style={{
                flex: 2,
                textAlign: 'right',
                color: '#292D3A',
                fontSize: 14,
              }}
            >
              {dataTransaction.criteria_title}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 16,
              borderBottomWidth: 1,
              borderColor: '#E9E9E9',
            }}
          >
            <Text
              style={{ flex: 1, color: '#292D3A', opacity: 0.7, fontSize: 14 }}
            >
              {tl('time')}
            </Text>
            <Text
              style={{
                flex: 2,
                textAlign: 'right',
                color: '#292D3A',
                fontSize: 14,
              }}
            >
              {moment(dataTransaction.created_time).format(
                'DD/MM/YYYY - hh:mm',
              )}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingVertical: 16,
            }}
          >
            <Text
              style={{ flex: 1, color: '#292D3A', opacity: 0.7, fontSize: 14 }}
            >
              {tl('amount')}
            </Text>
            <Text
              style={{
                flex: 2,
                textAlign: 'right',
                color: '#292D3A',
                fontSize: 14,
              }}
            >
              {tl('numberStar', { star: dataTransaction.quantity })}
            </Text>
          </View>
          {/* <AppButton onPress={goBack} title="Huỷ bỏ" cancel /> */}
        </View>
      </View>
    </AppContainer>
  );
};
