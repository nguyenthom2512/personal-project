import React, { useContext, useEffect, useRef, useState } from 'react';
import { Alert, SafeAreaView, StyleSheet, View } from 'react-native';
import {
  ModalChoosePrioritized,
  ModalClock,
  ModalMore,
} from '../../Components';
import ModalCreate from '../../Components/ModalCreate';
import ModalChooseDays from '../../Components/ModalCreate/ModalChooseDays';
import ModalOkrs from '../../Components/ModalCreate/ModalOkrs';
import { ModalComment } from '../../Components';
import { ChooseImage } from '../../Components';
//import { color } from '../../Helpers';
import { Theme, useTheme, useThemeAwareObject } from '../../Theme';

import ModalChooseTag from '../../Components/ModalCreate/ModalChooseTag';
import ModalChooseTask from '../../Components/ModalCreate/ModalChooseTask';
import { Formik, FormikProps } from 'formik';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../Redux/Reducers';
import styles from './styles';
import todoServices from '../../Services/todoServices';
import { useTranslation } from 'react-i18next';
import { IndicatorContext } from '../../Context';
import * as yup from 'yup';
import ModalDuplycateMission from '../../Components/ModalCreate/ModalDuplycateMission';
import moment from 'moment';
import { getTodoListRequest } from '../../Redux/Actions/todoListActions';
import { Modalize } from 'react-native-modalize';
import { noDateTodoRequest } from '../../Redux/Actions/noDateTodoActions';
import { resetTodoComment } from '../../Redux/Actions/todoCommentActions';
import { useNavigation } from '@react-navigation/native';
import { getTodoDashboardRequest } from '../../Redux/Actions/todoDashboardActions';

interface Props {
  handleClose?: () => void;
  navigation?: any;
  createTodoSuccess?: () => void;
  closeModalCreate?: () => void;
}

export default (props: Props) => {
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { theme } = useTheme();
  const userData = useSelector((state: RootState) => state.userReducer);
  const user = userData?.data?.user?.id;

  const { setLoading } = useContext(IndicatorContext);
  const [isShowModal, setIsShowModal] = useState('close');
  const validateSchema = yup.object().shape({
    todo_name: yup.string().required(t('common:required')),
    // start_time: yup.string().required(t('common:required')),
    // end_time: yup.string().required(t('common:required')),
    // confident: yup.string().required(t('common:required')),
    // is_done: yup.string().required(t('common:required')),
    // user: yup.string().required(t('common:required')),
    // result: yup.string().required(t('common:required')), // nhiem vu ca nhan
    priority: yup.string().required(t('common:required')),
  });

  const dispatch = useDispatch();

  const navigation = useNavigation();

  const toggleModal = () => {
    setIsShowModal('close');
  };

  const RefOkr = useRef<Modalize>(null);
  const RefDay = useRef<Modalize>(null);
  const RefTag = useRef<Modalize>(null);
  const RefTask = useRef<Modalize>(null);
  const RefClock = useRef<Modalize>(null);
  const RefComment = useRef<Modalize>(null);
  const RefImage = useRef<Modalize>(null);
  const RefMore = useRef<Modalize>(null);
  const RefPriority = useRef<Modalize>(null);
  const RefDuplicate = useRef<Modalize>(null);

  const OpenOkr = () => {
    RefOkr.current?.open();
  };
  const Opendays = () => {
    RefDay.current?.open();
  };
  const OpenImage = () => {
    RefImage.current?.open();
  };

  const onOpen = (key: any) => {
    switch (key) {
      case 'tag':
        RefTag.current?.open();
        break;
      case 'priority':
        RefPriority.current?.open();
        break;
      case 'more':
        RefMore.current?.open();
        break;
      case 'comment':
        RefComment.current?.open();
        break;
      case 'clock':
        RefClock.current?.open();
        break;
      case 'duplicate':
        RefDuplicate.current?.open();
        break;
      case 'task':
        RefTask.current?.open();
        break;
      default:
        break;
    }
  };
  const onClose = () => {
    RefOkr.current?.close();
    RefDay.current?.close();
    RefTag.current?.close();
    RefImage.current?.close();
    RefPriority.current?.close();
    RefMore.current?.close();
    RefComment.current?.close();
    RefClock.current?.close();
    RefDuplicate.current?.close();
    RefTask.current?.close();
  };

  const renderModal = (formik: FormikProps<any>) => (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: 'transparent',
        justifyContent: 'flex-end',
      }}
    >
      <ModalCreate
        formik={formik}
        onChosseOkr={() => {
          OpenOkr();
        }}
        onChooseDays={() => {
          Opendays();
        }}
        pressAction={(key) => {
          onOpen(key);
        }}
        onChooseImage={() => OpenImage()}
        handleClose={props.handleClose}
      />
      <ModalChooseDays
        ref={RefDay}
        formik={formik}
        onCloseModal={() => onClose()}
      />
      <ModalOkrs
        ref={RefOkr}
        formik={formik}
        onCloseModal={() => onClose()}
        navigation={props.navigation}
        closeModal={props.handleClose}
      />

      <ModalChooseTag
        formik={formik}
        ref={RefTag}
        onCloseModal={() => onClose()}
        user={user}
      />

      <ChooseImage
        ref={RefImage}
        formik={formik}
        onCloseModal={() => onClose()}
      />

      <ModalChoosePrioritized
        formik={formik}
        ref={RefPriority}
        onCloseModal={() => onClose()}
        isGetFullInfo
      />
      <ModalComment
        ref={RefComment}
        onCloseModal={() => onClose()}
        formik={formik}
      />
      <ModalClock
        formik={formik}
        ref={RefClock}
        onCloseModal={() => onClose()}
      />

      <ModalChooseTask
        formik={formik}
        ref={RefTask}
        onCloseModal={() => onClose()}
      />
      <ModalMore
        formik={formik}
        ref={RefMore}
        onCloseModal={() => onClose()}
        chooseAction={(key) => onOpen(key)}
        isCreate
      />
      <ModalDuplycateMission
        formik={formik}
        ref={RefDuplicate}
        onCloseModal={() => onClose()}
      />
    </SafeAreaView>
  );

  return (
    <Formik
      initialValues={{
        raic_user: [],
        todo_name: '',
        start_time: '',
        end_time: '',
        description: '',
        confident: 0,
        is_done: false,
        user,
        result: '',
        resultName: '', // remove when submit
        // todo_parent: '',
        priority: '',
        tag: [],
        check_list: [''],
        okrId: '', // remove when submit
        date: '', // remove when submit
        user_responsible: [], // remove when submit
        user_accountable: [], // remove when submit
        user_inform: [], // remove when submit
        user_consult: [], // remove when submit
        priority_color: '', // remove when submit
        img_url: '',
        todo_comment: [],
      }}
      validateOnBlur
      validationSchema={validateSchema}
      onSubmit={(values, { resetForm }) => {
        setLoading(true);
        // @ts-ignore
        // values.priority = values.priority.id;

        if (values.date.includes('Invalid date')) {
          values.date = '';
        }
        todoServices.createTodo(
          values,
          (err) => {
            setLoading(false);
            Alert.alert(
              t('common:notice'),
              err || t('createMissions:timeError'),
            );
          },
          () => {
            setLoading(false);
            Alert.alert(
              t('common:notice'),
              t('createMissions:createTodoSuccess'),
              [
                {
                  text: 'OK',
                  onPress: () => {
                    const start_time = moment().format('YYYY-MM-DD');
                    const end_time = moment()
                      .add(1, 'days')
                      .format('YYYY-MM-DD');

                    dispatch(getTodoListRequest({ start_time, end_time }));

                    dispatch(noDateTodoRequest({ page: 1, page_size: 20 }));

                    dispatch(getTodoDashboardRequest());

                    resetForm();
                    props.handleClose && props.handleClose();
                    dispatch(resetTodoComment());
                    props.navigation.navigate(t('tabbar:home'), {
                      screen: 'HomePage',
                    });
                    // navigation.goBack();
                  },
                },
              ],
            );
            // resetForm();
          },
        );
      }}
    >
      {(propsFormik) => {
        return <View style={Styles.container}>{renderModal(propsFormik)}</View>;
      }}
    </Formik>
  );
};
