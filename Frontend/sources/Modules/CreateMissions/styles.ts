import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      backgroundColor: 'transparent',
      flex: 1,
      // justifyContent: 'flex-end'
    },
  });
  return styles;
};
