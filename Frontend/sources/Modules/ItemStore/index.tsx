import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, FlatList, TouchableOpacity, View } from 'react-native';
import {
  AppButton,
  AppContainer,
  AppImage,
  AppInputField,
  AppText,
} from '../../Components';
import DiamondIcon from '../../Assets/Svg/diamondIcon.svg';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import MiniDiamond from '../../Assets/Svg/miniDiamond.svg';
import useGetPresent from '../../Hooks/getPresent';
import { PresentItem } from '../../interfaces/present.interface';
import serviceUrls from '../../Services/serviceUrls';
import { useNavigation } from '@react-navigation/native';
import Modal from 'react-native-modal';
import useTransferDiamond from '../../Hooks/useTransferDiamond';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../Redux/Reducers';
import transferDiamondService from '../../Services/transferDiamondService';
import { IndicatorContext } from '../../Context';
import ModalApp from '../../Components/ModalApp';
import { Icon } from 'react-native-elements';
import { Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { getUserRequest } from '../../Redux/Actions/userActions';
import { getTodoDashboardRequest } from '../../Redux/Actions/todoDashboardActions';
import presentService from '../../Services/presentService';
import { useEffect } from 'react';
interface Props {
  number: number;
}
const DEFAULT_AVATAR = 'https://i.imgur.com/WxNkK7J.png';
export default (props: Props) => {
  const { t } = useTranslation();
  const { number } = props;
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const navigation = useNavigation();

  const [dataPresent, loading, error, refetch] = useGetPresent();
  const { setLoading } = useContext(IndicatorContext);
  const [presentId, setPresentId] = useState(0);
  console.log('presentId', presentId);

  const userReducer = useSelector(
    (state: RootState) => state.userReducer?.data,
  );

  const [modalGive, setModalGive] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const toggleModal = (id: number) => {
    setPresentId(id);
    setModalGive(!modalGive);
  };

  const dashboardReducer = useSelector(
    (state: RootState) => state.todoDashboardReducer,
  );

  const dispatch = useDispatch();
  const onSubmit = (values: any, { resetForm }: FormikProps<any>) => {
    setLoading(true);
    onCloseModal();
    transferDiamondService
      .postTransferDiamond(values)
      .then((res) => {
        setLoading(false);
        Alert.alert(t('common:notice'), t('common:changeDiamondSuccess'), [
          {
            text: 'OK',
            onPress: () => onCloseModal(),
          },
        ]);
        const userData = userReducer.user;
        dispatch(getUserRequest(userData.id));
        resetForm();
        dispatch(getTodoDashboardRequest());
      })
      .catch((err) => {
        setLoading(false);
        Alert.alert(t('common:notice'), err);
      });
  };

  const validationSchema = Yup.object().shape({
    rate_quantity: Yup.number()
      .typeError(t('common:errorNumber'))
      .required(t('common:required'))
      .min(1, t('common:errorMinDiamond'))
      .max(userReducer?.user?.total_rate, t('common:errorMaxDiamond')),
  });

  const onCloseModal = () => {
    setModalGive(false);
    setShowModal(false);
  };
  const renderItem = ({ item }: { item: PresentItem }) => {
    return (
      <TouchableOpacity
        onPress={() => navigation.navigate('GiftDetails', item)}
        style={Styles.itemContainer}
      >
        <AppImage
          source={{
            uri: item?.img_url
              ? `${serviceUrls.url.IMAGE}${item?.img_url}`
              : DEFAULT_AVATAR,
          }}
          imageStyle={Styles.imageStyles}
        />
        <View style={Styles.infoItem}>
          <AppText style={Styles.textStyles} numberOfLines={1}>
            {item.present_name}
          </AppText>
          <View style={Styles.diamondWrapper}>
            <View style={Styles.diamondContent}>
              <AppText style={Styles.point}>{item.price}</AppText>
              <MiniDiamond />
            </View>
          </View>
        </View>
        <AppButton
          containerStyle={{ flex: 1, justifyContent: 'center' }}
          style={{ height: 'auto' }}
          cancel
          title={t('store:change')}
          onPress={() => {
            if (dashboardReducer?.data?.diamond < (item?.price || 0)) {
              Alert.alert(t('common:notice'), t('common:errorGift'));
              return;
            }
            toggleModal(item?.id || 0);
          }}
          textStyle={Styles.textStyles}
        />
      </TouchableOpacity>
    );
  };
  return (
    <AppContainer title={t('store:itemStore')} isBack>
      <View style={Styles.container}>
        <View style={Styles.diamondStyles}>
          <View style={Styles.diamondText}>
            <AppText style={Styles.numberDiamond}>
              {dashboardReducer?.data?.diamond || 0}
            </AppText>
            <DiamondIcon />
          </View>
          <AppButton
            containerStyle={{ width: '40%', flex: 1 }}
            onPress={() => setShowModal(!showModal)}
            title={t('store:changeDiamond')}
          ></AppButton>
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            data={dataPresent}
            renderItem={renderItem}
            keyExtractor={(_, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            onRefresh={refetch}
            refreshing={loading}
          />
        </View>
        <Modal onBackdropPress={onCloseModal} isVisible={modalGive}>
          <View style={Styles.centeredView}>
            <AppText style={Styles.textModal}>{t('store:askChange')}</AppText>
            <View style={Styles.buttonContainer}>
              <AppButton
                containerStyle={{ flex: 1, marginRight: 12 }}
                title={t('todoDetail:cancel')}
                onPress={onCloseModal}
              />
              <AppButton
                containerStyle={{ flex: 1 }}
                cancel
                title={t('todoDetail:yes')}
                onPress={() => {
                  presentService
                    .presentChange({ present: presentId, quantity: 1 })
                    .then((res) => {
                      setLoading(false);
                      Alert.alert(
                        t('common:notice'),
                        t('common:giftChangeSuccess'),
                        [
                          {
                            text: 'OK',
                            // onPress: () => onCloseModal(),
                          },
                        ],
                      );
                      dispatch(getTodoDashboardRequest());
                    });
                  onCloseModal();
                }}
              />
            </View>
          </View>
        </Modal>
        <Formik
          initialValues={{
            rate_quantity: '',
          }}
          onSubmit={onSubmit}
          validationSchema={validationSchema}
        >
          {({ handleSubmit, values, setFieldValue, errors, resetForm }) => {
            console.log('errors', errors);

            return (
              <ModalApp
                visible={showModal}
                iconLeft={
                  <Icon
                    name="arrow-left"
                    type="feather"
                    color={theme.color.gray}
                    tvParallaxProperties
                  />
                }
                onLeftPress={() => {
                  resetForm();
                  setShowModal(false);
                }}
                headerText={t('common:changeDiamond')}
                headerStyles={Styles.headerStyles}
              >
                <View
                  style={{
                    marginHorizontal: theme.spacing.p20,
                    paddingVertical: 16,
                  }}
                >
                  <AppInputField
                    name={'rate_quantity'}
                    placeholder={t('common:numberDiamond')}
                    keyboardType="numeric"
                  />
                  <View>
                    <View style={Styles.myDiamond}>
                      <AppText>
                        {t('common:myStar')}{' '}
                        {dashboardReducer?.data?.total_rate}
                      </AppText>
                    </View>
                  </View>
                  <AppButton
                    onPress={handleSubmit}
                    title={t('common:change')}
                  />
                </View>
              </ModalApp>
            );
          }}
        </Formik>
      </View>
    </AppContainer>
  );
};
