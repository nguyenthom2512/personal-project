import { useNavigation, useRoute } from '@react-navigation/core';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ActivityIndicator,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import { AppButton, AppContainer, AppText } from '../../Components';
import TextDetail from '../../Components/TextDetail';
import useGetOkrDetail from '../../Hooks/getOkrDetail';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

const OkrDetail = (props: any) => {
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const navigation = useNavigation();
  const route = useRoute();
  const { item, refresh }: any = route?.params;

  const [dataGetOkr, loadingDataOkr, errorOkr, refetchGetOKR] = useGetOkrDetail(
    item.id,
  );
  const [isModalVisible, setModalVisible] = useState<any>({});
  const closeModal = () => {
    setModalVisible({});
  };
  const toggleModal = (item: any) => {
    setModalVisible(item);
  };
  const CONFIDENT = ['Kém', 'Bình thường', 'Rất tốt'];
  const createColor = (curentPercent: any) => {
    if (curentPercent === 2) {
      return theme.color.greenBox;
    } else if (curentPercent === 1) {
      return theme.color.fireBush;
    } else {
      return theme.color.cinnabar;
    }
  };

  return (
    <>
      <AppContainer
        isBack
        title={t('OKR:OKRs_Company')}
        iconRight="feedback"
        typeIconRight="materialIcons"
        onRightPress={() => {
          navigation.navigate('FeedBack', dataGetOkr);
        }}
      >
        {loadingDataOkr ? (
          <ActivityIndicator size="large" color={theme.color.gray} />
        ) : (
          <ScrollView>
            <View style={Styles.container}>
              <View style={Styles.objname}>
                <AppText
                  style={Styles.txtName}
                  numberOfLines={1}
                  ellipsizeMode="tail"
                >
                  {dataGetOkr?.object_name}
                </AppText>
              </View>
              <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                  dataGetOkr?.okr_result?.length > 0 &&
                    navigation.navigate('KrScreen', {
                      param: dataGetOkr,
                      refetchGetOKR,
                      type: 'keyResult',
                      refresh: refresh,
                    });
                }}
              >
                <View style={{ marginHorizontal: 16 }}>
                  <TextDetail
                    textKey={t('OKR:keyResult')}
                    value={
                      <AppButton
                        containerStyle={{ marginTop: theme.spacing.p8 }}
                        title={t('OKR:result', {
                          result: dataGetOkr?.okr_result?.length,
                        })}
                        onPress={() =>
                          navigation.navigate('KrScreen', {
                            param: dataGetOkr,
                            refetchGetOKR,
                            refresh: refresh,
                          })
                        }
                        cancel
                        disabled
                        // style={{ width: '40%' }}
                      />
                    }
                  />
                  <TextDetail
                    textKey={t('OKR:percentCompleted')}
                    value={`${dataGetOkr?.percent_completed * 100}%`}
                    color
                    style={{
                      backgroundColor: createColor(dataGetOkr?.confident),
                    }}
                  />
                  <TextDetail
                    textKey={t('OKR:change')}
                    value={`${dataGetOkr?.percent_changed * 100}%`}
                  />
                  <TextDetail
                    textKey={t('OKR:staff')}
                    value={
                      <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}
                      >
                        {dataGetOkr?.user_data?.img_url ? (
                          <Image
                            style={Styles.iconName}
                            source={{
                              uri: `${serviceUrls.url.IMAGE}${dataGetOkr?.user_data?.img_url}`,
                            }}
                          />
                        ) : (
                          <Image
                            style={Styles.iconName}
                            source={require('../../Assets/anhDaiDien.jpg')}
                          />
                        )}
                        <AppText>{dataGetOkr?.user_data?.full_name}</AppText>
                      </View>
                    }
                  />
                  <TextDetail
                    textKey={t('OKR:confident')}
                    value={
                      CONFIDENT[dataGetOkr?.confident] ||
                      `${t('OKR:notCheckIn')}`
                    }
                    style={{ color: createColor(dataGetOkr?.confident) }}
                  />
                </View>
              </TouchableOpacity>
            </View>
          </ScrollView>
        )}
      </AppContainer>
      {/* {
                isModalVisible && (
                    <ModalKr isModalVisible={isModalVisible} closeModal={closeModal} />
                )
            } */}
    </>
  );
};
export default OkrDetail;
