import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    flex1: { flex: 1 },
    flex3: { flex: 3 },
    container: {
      backgroundColor: theme.color.white,
      marginHorizontal: theme.spacing.p16,
      borderRadius: 8,
      marginVertical: theme.spacing.p8,
    },
    objname: {
      flexDirection: 'row',
      paddingVertical: theme.spacing.p12,
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p16,
      borderBottomWidth: 1,
      borderColor: theme.color.mercury,
    },
    txtName: {
      flex: 1,
      fontSize: theme.fontSize.f14,
      color: theme.color.shark,
      fontWeight: '500',
    },
    iconName: {
      width: 24,
      height: 24,
      borderRadius: 12,
      marginRight: theme.spacing.p8,
      backgroundColor: theme.color.gray,
    },
    textKey: {
      flex: 1,
      fontSize: theme.fontSize.f14,
      fontWeight: '400',
      color: theme.color.blackTitle,
      opacity: 0.5,
    },
  });
  return styles;
};
