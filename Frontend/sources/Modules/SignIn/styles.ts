import { Platform, StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      flex: 1,
    },
    containerStyles: {
      paddingHorizontal: theme.spacing.p20,
    },
    header: {
      fontSize: theme.fontSize.f24,
      fontWeight: '700',
      marginVertical: theme.spacing.p20,
      paddingTop: Platform.OS == 'android' ? theme.spacing.p20 : theme.spacing.p40
    },
    card: {
      flex: 1,
      backgroundColor: theme.color.white,
    },
    fPText: {
      marginVertical: theme.spacing.p12,
      color: theme.color.violet,
    },
    line: {
      flex: 1,
      borderBottomWidth: 1,
      borderBottomColor: theme.color.mercury,
    },
    txt: {
      textAlign: 'center',
      fontSize: 16,
      color: theme.color.shark,
      opacity: 0.5,
      marginVertical: theme.spacing.p20
    },

    wrapIcon: {
      backgroundColor: '#EEEEEE',
      height: 40,
      width: 40,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    wrapIconSignUp: {
      justifyContent: 'space-between',
    },
    iconSignUp: {
      flexDirection: 'row',
      padding: theme.spacing.p12,
      borderWidth: 1,
      borderRadius: 4,
      borderColor: theme.color.mercury,
      marginBottom: 8,
    },
    btnLoginWith: {
      flex: 1,
      textAlign: 'center',
      fontSize: theme.fontSize.f16,
    },
    createAccount:{
      textAlign: 'center',
      fontSize: 16,
      color: theme.color.shark,
      opacity: 0.5,
      marginTop: Platform.OS == 'android' ? theme.spacing.p48 : theme.spacing.p52
    },
  });
  return styles;
};
