import messaging from '@react-native-firebase/messaging';
import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Alert,
  SafeAreaView,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import { useDispatch } from 'react-redux';
import * as Yup from 'yup';
import Apple from '../../Assets/Svg/apple.svg';
import FacebookIcon from '../../Assets/Svg/facebook.svg';
import GoogleIcon from '../../Assets/Svg/google.svg';
import LockIcon from '../../Assets/Svg/lock.svg';
import MailIcon from '../../Assets/Svg/mail.svg';
import { AppButton, AppText } from '../../Components';
import AppInputFieldIcon from '../../Components/AppInputFieldIcon';
import { IndicatorContext } from '../../Context';
import { LOGIN_SOCIAL_TYPE } from '../../Helpers/constant';
import { requestUserPermission } from '../../Helpers/requestPermission';
import socialSignin from '../../Hooks/socialSignin';
import { userActions } from '../../Redux/Actions';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';

interface Props {}

export default () => {
  const Styles = useThemeAwareObject(styles);
  const { t } = useTranslation();
  const { theme } = useTheme();
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const { facebook, google } = socialSignin();
  /**
   * @description context
   */
  const indicatorContext = useContext(IndicatorContext);

  const [deviceToken, setDeviceToken] = useState<String>('');
  /**
   * @description validation
   */
  const validateLoginSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('login:invalidEmail'))
      .required(t('common:required')),
    password: Yup.string().required(t('common:required')),
  });

  const onLogin = (values: { email: string; password: string }) => {
    indicatorContext.setLoading(true);
    dispatch(
      userActions.loginRequest(
        {
          ...values,
          device_id: deviceToken,
        },
        {
          onSuccess: () => {
            indicatorContext.setLoading(false);
          },
          onError: (err: any) => {
            indicatorContext.setLoading(false);
            if (err === 'Unable to login with provided credentials.') {
              Alert.alert(t('common:notice'), t('login:wrongPassword'));
            } else if (err == 'NETWORK_ERROR') {
              Alert.alert(t('common:notice'), t('login:networkError'));
            } else {
              Alert.alert(t('common:notice'), t('login:wrongPassword'));
            }
          },
        },
      ),
    );
  };

  useEffect(() => {
    requestUserPermission().then((res) => {
      messaging()
        .getToken()
        .then((token) => {
          setDeviceToken(token);
        })
        .catch((err) => {
          __DEV__ && console.log('err', err);
        });
    });
  }, []);

  const handleError = (error: any) => {
    indicatorContext.setLoading(false);
    if (error === 'Unable to login with provided credentials.') {
      Alert.alert(t('common:notice'), t('login:wrongPassword'));
    } else if (error == 'NETWORK_ERROR') {
      Alert.alert(t('common:notice'), t('login:networkError'));
    } else {
      Alert.alert(t('common:notice'), t('login:loginFailed'));
    }
  };

  const onLoginFacebook = () => {
    indicatorContext.setLoading(true);
    facebook().then(
      (res) => {
        dispatch(
          userActions.loginSocialRequest(
            {
              access_token: res.accessToken,
              login_type: LOGIN_SOCIAL_TYPE.FACEBOOK,
              device_id: deviceToken,
            },
            {
              onSuccess: () => indicatorContext.setLoading(false),
              onError: handleError,
            },
          ),
        );
      },
      (rej) => {
        indicatorContext.setLoading(false);
      },
    );
  };

  const onLoginGoogle = () => {
    indicatorContext.setLoading(true);
    google().then(
      (res) => {
        dispatch(
          userActions.loginSocialRequest(
            {
              access_token: res.accessToken,
              login_type: LOGIN_SOCIAL_TYPE.GOOGLE,
              device_id: deviceToken,
            },
            {
              onSuccess: () => indicatorContext.setLoading(false),
              onError: handleError,
            },
          ),
        );
      },
      (rej) => {
        indicatorContext.setLoading(false);
      },
    );
  };

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      validationSchema={validateLoginSchema}
      initialValues={{ email: '', password: '' }}
      onSubmit={onLogin}
    >
      {({ handleSubmit }) => (
        <SafeAreaView style={Styles.card}>
          <ScrollView style={Styles.containerStyles}>
            <AppText style={Styles.header}>{t('login:loginAccount')}</AppText>
            <View>
              <AppInputFieldIcon
                name="email"
                placeholder={t('login:emailPlaceholder')}
                memo
                icon={<MailIcon />}
              />
              <AppInputFieldIcon
                containerStyle={{ marginTop: 12 }}
                secureTextEntry
                name="password"
                placeholder={t('login:passwordPlaceholder')}
                memo
                icon={<LockIcon />}
              />
              <AppText
                style={Styles.fPText}
                onPress={() => {
                  navigation.navigate('ResetPassword');
                }}
              >
                {t('login:forgotPassword')}?
              </AppText>
              <AppButton title={t('login:login')} onPress={handleSubmit} />
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={Styles.line} />
                <AppText style={[Styles.txt]}>Hoặc</AppText>
                <View style={Styles.line} />
              </View>
              <View style={Styles.wrapIconSignUp}>
                <TouchableOpacity
                  style={Styles.iconSignUp}
                  onPress={onLoginGoogle}
                >
                  <GoogleIcon />
                  <AppText style={Styles.btnLoginWith}>
                    {t('login:logInWithGG')}
                  </AppText>
                  <GoogleIcon style={{ opacity: 0 }} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={Styles.iconSignUp}
                  onPress={onLoginFacebook}
                >
                  <FacebookIcon />
                  <AppText style={Styles.btnLoginWith}>
                    {t('login:logInWithFb')}
                  </AppText>
                  <FacebookIcon style={{ opacity: 0 }} />
                </TouchableOpacity>
              </View>
              <AppText style={[Styles.createAccount]}>
                {t('login:isAccount')}
              </AppText>
            </View>
            <View>
              <AppButton
                title={t('login:createAccount')}
                onPress={() => {
                  navigation.navigate('SignUp');
                }}
                cancel
                containerStyle={{ marginTop: theme.spacing.p12 }}
              />
            </View>
          </ScrollView>
        </SafeAreaView>
      )}
    </Formik>
  );
};
