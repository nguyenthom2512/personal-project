import { useNavigation } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  ActivityIndicator,
  Alert,
  FlatList,
  TouchableOpacity,
  View,
} from 'react-native';
import IonIcon from 'react-native-vector-icons/Ionicons';
import { AppText } from '../../../Components';
import { device } from '../../../Helpers';
import dataOkr from '../../../Services/dataOkr';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from '../styles';
import { Iitem } from '../treeokr.interface';

const ItemOKR = ({
  data,
  dataSearch = { department: '', created_time__range: '' },
  refresh = () => {},
}: {
  data: Iitem;
  dataSearch: { department: string; created_time__range: string };
  refresh: () => void;
}) => {
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const { t } = useTranslation();
  const navigation = useNavigation();
  const [dataOkrChild, setDataOkrChild] = useState(data);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    setDataOkrChild(data);
  }, [data]);

  const getOKRData = (item: Iitem) => {
    dataOkr.getTreeOKR(
      item.id.toString(),
      dataSearch.department,
      dataSearch.created_time__range,
      {
        onStart: () => setLoading(true),
        onError: (e) => Alert.alert(t('common:notice'), e),
        onSuccess: (data) =>
          setDataOkrChild({
            ...dataOkrChild,
            children: data,
            isOpen: !dataOkrChild.isOpen,
          }),
        onFinish: () => setLoading(false),
      },
    );
  };

  const onClickOKR = (item: Iitem) => {
    navigation.navigate('OkrDetail', { item: item, refresh });
  };

  const RenderItem = ({ item }: { item: Iitem }) => {
    return (
      <>
        <View style={Styles.itemContainer}>
          <View
            style={{
              ...Styles.line,
              backgroundColor: item.color,
            }}
          />
          <View style={Styles.itemWrap}>
            <TouchableOpacity
              style={Styles.flex1}
              onPress={() => onClickOKR(item)}
            >
              <AppText
                key={item.id}
                numberOfLines={2}
                style={item.isMatch ? {} : { color: theme.color.redMonza }}
              >
                {item.object_name}
              </AppText>
            </TouchableOpacity>
            <TouchableOpacity
              style={Styles.iconDropdown}
              onPress={() => getOKRData(item)}
            >
              {loading ? (
                <ActivityIndicator size="small" color={theme.color.gray} />
              ) : (
                <IonIcon
                  name={item.isOpen ? 'chevron-up' : 'chevron-down'}
                  size={24}
                  color={theme.color.gray}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
        {dataOkrChild.isOpen && (
          <FlatList
            keyExtractor={(item) => item.id.toString()}
            data={dataOkrChild.children}
            renderItem={({ item }) => (
              <View style={{ paddingLeft: device.width * 0.03 }}>
                <ItemOKR
                  data={item}
                  dataSearch={dataSearch}
                  refresh={refresh}
                />
              </View>
            )}
          />
        )}
      </>
    );
  };
  return <RenderItem item={dataOkrChild} />;
};

export default ItemOKR;
