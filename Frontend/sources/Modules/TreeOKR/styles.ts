import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      paddingHorizontal: theme.spacing.p16,
      paddingTop: theme.spacing.p8,
      flex: 1,
    },
    styleDropDown: {
      borderRadius: theme.spacing.p4,
      backgroundColor: theme.color.white,
      height: 48,
    },
    itemContainer: {
      backgroundColor: theme.color.white,
      borderRadius: theme.spacing.p4,
      marginBottom: theme.spacing.p4,
    },
    itemWrap: {
      height: 48,
      flex: 1,
      // backgroundColor: theme.color.white,
      flexDirection: 'row',
      alignItems: 'center',
      paddingHorizontal: theme.spacing.p12,
      // shadowColor: '#000',
      // shadowOffset: {
      //   width: 0,
      //   height: 2,
      // },
      // shadowOpacity: 0.25,
      // shadowRadius: 3.84,
      // elevation: 5,
    },
    line: {
      height: 4,
      width: '100%',
      borderTopLeftRadius: theme.spacing.p4,
      borderTopRightRadius: theme.spacing.p4,
    },
    iconDropdown: {
      height: '100%',
      paddingHorizontal: theme.spacing.p8,
      alignItems: 'center',
      justifyContent: 'center',
    },
    flex1: {
      flex: 1,
    },
    scrollViewWrap: {
      flex: 1,
      paddingTop: theme.spacing.p16,
    },
    noticeTxt: {
      color: theme.color.violet,
      fontSize: theme.fontSize.f16,
    },
    titleTxt: {
      fontSize: theme.fontSize.f20,
      marginVertical: theme.spacing.p12,
    },
  });
