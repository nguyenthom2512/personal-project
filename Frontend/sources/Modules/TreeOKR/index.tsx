import { useNavigation } from '@react-navigation/core';
import { Formik } from 'formik';
import { isEmpty } from 'lodash';
import React, { useState } from 'react';
import { useSSR, useTranslation } from 'react-i18next';
import { Alert, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { number, string } from 'yup';
import { AppContainer, AppDropDown, AppText } from '../../Components';
import { QUARTER } from '../../Helpers/dateTime';
import useGetDepartment from '../../Hooks/getDepartment';
import dataOkr from '../../Services/dataOkr';
import { useThemeAwareObject } from '../../Theme';
import License from '../License';
import UpgradeAccount from '../UpgradeAccount';
import ItemOKR from './Components/ItemOKR';
import styles from './styles';
import { Iitem } from './treeokr.interface';

const TreeOKR = () => {
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const navigation = useNavigation();

  const [dataDepartment, loadingDepartment, error] = useGetDepartment();
  const department = (dataDepartment || []).map((elm) => {
    return {
      value: elm.id,
      label: elm.department_name,
    };
  });

  const [dataTree, setDataTree] = useState<Iitem[]>([]);
  const [loading, setLoading] = useState<boolean | null>(false);

  const onSubmit = (values: {
    department: string;
    created_time__range: string;
  }) => {
    dataOkr.getTreeOKR('', values.department, values.created_time__range, {
      onStart: () => setLoading(true),
      onError: (e) => Alert.alert(t('common:notice'), e),
      onSuccess: (data) => setDataTree(data),
      onFinish: () => setLoading(false),
    });
  };

  console.log('dataDepartment', dataDepartment);

  if (!loadingDepartment && error) {
    if (
      error?.toLowerCase() ===
      'You do not have permission to perform this action.'.toLowerCase()
    ) {
      return (
        <UpgradeAccount
          onPressUpgrade={() => {
            navigation.navigate('License');
          }}
        />
      );
    }
    console.log('error', error);
  }

  return (
    <Formik
      initialValues={{
        department: '',
        created_time__range: '',
      }}
      onSubmit={onSubmit}
    >
      {({ values, setFieldValue, handleSubmit }) => {
        return (
          <AppContainer
            title={t('treeOKR:okrs')}
            isMenu
            iconRight="plus"
            onRightPress={() => {
              navigation.navigate('CreateOKR', {
                id: '',
                refetchScreen: handleSubmit,
              });
            }}
          >
            <View style={Styles.container}>
              <AppText style={Styles.titleTxt}>{t('treeOKR:title')}</AppText>
              <AppDropDown
                value={values.department}
                isSearch
                data={department}
                placeHolder={t('treeOKR:selectDepartment')}
                onChoose={(value) => {
                  setFieldValue('department', value.value);
                  handleSubmit();
                }}
                customContent={Styles.styleDropDown}
              />
              <AppDropDown
                value={values.created_time__range}
                isSearch
                placeHolder={t('treeOKR:selectQuarter')}
                data={QUARTER()}
                onChoose={(value) => {
                  setFieldValue('created_time__range', value.value);
                  handleSubmit();
                }}
                customContent={Styles.styleDropDown}
              />
              <View style={Styles.scrollViewWrap}>
                {isEmpty(dataTree) ? (
                  values.department ? (
                    <View style={Styles.flex1}>
                      <AppText style={Styles.noticeTxt}>
                        {t('treeOKR:noResult')}
                      </AppText>
                    </View>
                  ) : (
                    <View style={Styles.flex1}>
                      <AppText style={Styles.noticeTxt}>
                        {t('treeOKR:pleaseSelectDepartment')}
                      </AppText>
                    </View>
                  )
                ) : (
                  <FlatList
                    refreshing={loading}
                    onRefresh={() => handleSubmit()}
                    keyExtractor={(item) => item.id.toString()}
                    data={dataTree}
                    renderItem={({ item }) => (
                      <ItemOKR
                        dataSearch={values}
                        data={item}
                        refresh={handleSubmit}
                      />
                    )}
                  />
                )}
              </View>
            </View>
          </AppContainer>
        );
      }}
    </Formik>
  );
};

export default TreeOKR;
