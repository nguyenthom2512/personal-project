import { StyleSheet } from 'react-native';
import { device } from '../../Helpers';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      paddingHorizontal: theme.spacing.p16,
      backgroundColor: theme.color.white,
      flex: 1,
    },
    imageStyles: {
      height: device.height / 3,
      width: '100%',
    },
    presentName: {
      fontSize: theme.fontSize.f18,
      fontWeight: '600',
      marginVertical: theme.spacing.p24,
    },
    priceWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    price: {
      fontSize: theme.fontSize.f24,
      fontWeight: '400',
      marginRight: theme.spacing.p8,
    },
    description: {
      fontSize: theme.fontSize.f16,
      color: theme.color.gray,
      marginTop: theme.spacing.p16,
      marginBottom: theme.spacing.p24,
    },
    material: {
      fontSize: theme.fontSize.f16,
      fontWeight: '600',
    },
  });
  return styles;
};
