import { useNavigation, useRoute } from '@react-navigation/native';
import React, { useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert, ScrollView, View } from 'react-native';
import { AppButton, AppContainer, AppImage, AppText } from '../../Components';
import useGetPresent from '../../Hooks/getPresent';
import serviceUrls from '../../Services/serviceUrls';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import BigDiamond from '../../Assets/Svg/bigDiamond.svg';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../Redux/Reducers';
import presentService from '../../Services/presentService';
import { IndicatorContext } from '../../Context';
import { getTodoDashboardRequest } from '../../Redux/Actions/todoDashboardActions';

interface Props {}

const DEFAULT_BANNER =
  'https://phongvu.vn/cong-nghe/wp-content/uploads/2021/06/laptop-chay-nhanh-hon-phong-vu-4.jpg';

export default (props: Props) => {
  const { t } = useTranslation();
  const Styles = useThemeAwareObject(styles);
  const { theme } = useTheme();
  const route = useRoute<any>();
  const item = route.params;
  const { setLoading } = useContext(IndicatorContext);

  const dashboardReducer = useSelector(
    (state: RootState) => state.todoDashboardReducer,
  );
  const dispatch = useDispatch();
  const navigation = useNavigation();
 

  return (
    <AppContainer isBack>
      <AppImage
        imageStyle={Styles.imageStyles}
        source={{
          uri: item?.img_url
            ? `${serviceUrls.url.IMAGE}${item?.img_url}`
            : DEFAULT_BANNER,
        }}
      />
      <ScrollView style={Styles.container}>
        <AppText style={Styles.presentName}>{item.present_name}</AppText>
        <View style={Styles.priceWrapper}>
          <AppText style={Styles.price}>{item.price}</AppText>
          <BigDiamond />
        </View>
        <AppText style={Styles.description}>
          {item.subscription || 'abc'}
        </AppText>
        <AppText style={Styles.material}>{t('store:material')}</AppText>
        <AppText style={Styles.description}>{item.material}</AppText>
        <AppButton
          containerStyle={{ paddingBottom: theme.spacing.p16 }}
          title={t('store:change')}
          onPress={() => {
            if (dashboardReducer?.data?.diamond < item.price) {
              Alert.alert(t('common:notice'), t('common:errorGift'));
              return;
            }
            presentService
              .presentChange({ present: item.id, quantity: 1 })
              .then((res) => {
                setLoading(false);
                Alert.alert(t('common:notice'), t('common:giftChangeSuccess'));
              });
            dispatch(getTodoDashboardRequest());
            navigation.goBack();
          }}
        ></AppButton>
      </ScrollView>
    </AppContainer>
  );
};
