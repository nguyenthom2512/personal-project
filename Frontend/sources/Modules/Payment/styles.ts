import { StyleSheet } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignContent: 'center',
    },
  });
