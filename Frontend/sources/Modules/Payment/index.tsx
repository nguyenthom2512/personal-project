import { useNavigation, useRoute } from '@react-navigation/core';
import React from 'react';
import { ActivityIndicator, Alert, View } from 'react-native';
import WebView, { WebViewNavigation } from 'react-native-webview';
import { AppContainer } from '../../Components';
import { useThemeAwareObject } from '../../Theme';
import styles from './styles';
import includes from 'lodash/includes';
import { useTranslation } from 'react-i18next';
import useGetPayment from '../../Hooks/getPayment';
import { Package } from '../../interfaces/package.interface';

const SUCCESS_CODE = '00';

const Payment = () => {
  let showAlert = false;
  const navigation = useNavigation();
  const { t } = useTranslation();
  const route = useRoute<any>();
  const { packageDetail }: { packageDetail: Package } = route.params;
  const [data, loading, error, refetch] = useGetPayment({
    package_id: packageDetail.id,
  });
  console.log('data, loading, error', data, loading, error);

  const Styles = useThemeAwareObject(styles);

  console.log({ packageDetail });

  const onNavigationStateChange = (event: WebViewNavigation) => {
    const { url, title } = event;
    if (includes(url, 'vnp_ResponseCode')) {
      if (includes(url, `vnp_ResponseCode=${SUCCESS_CODE}`)) {
        !showAlert &&
          Alert.alert(t('common:notice'), t('license:processTransaction'), [
            {
              onPress: () => navigation.goBack(),
              text: t('common:yes'),
            },
          ]);
        showAlert = true;
        // this.updateMoney();
      } else {
        !showAlert &&
          Alert.alert(t('common:notice'), t('license:processTransaction'), [
            {
              onPress: () => navigation.goBack(),
              text: t('common:no'),
            },
          ]);
        showAlert = true;
      }
      //   navigation.goBack();
      return;
    }
  };

  return (
    <AppContainer isBack>
      <View style={Styles.container}>
        {Boolean(loading) ? (
          <ActivityIndicator />
        ) : (
          <WebView
            scalesPageToFit
            source={{ uri: data.url }}
            startInLoadingState
            onNavigationStateChange={onNavigationStateChange}
          />
        )}
      </View>
    </AppContainer>
  );
};

export default Payment;
