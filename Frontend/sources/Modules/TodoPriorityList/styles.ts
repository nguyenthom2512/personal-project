import { StyleSheet, Dimensions } from 'react-native';
import { Theme } from '../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    headerContainer: {
      backgroundColor: theme.color.blackHeader,
      // backgroundColor:theme.color.redAlert,
      flexDirection: 'row',
      // justifyContent: 'space-between',
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p12,
      alignItems: 'center',
    },
    title: {
      color: theme.color.white,
      fontSize: theme.fontSize.f18,
      fontWeight: '600',
      flex: 1,
      textAlign: 'center',
    },
    todoList: {
      paddingVertical: theme.spacing.p16,
      marginLeft: 16,
      width: Dimensions.get('window').width / 2,
    },
    container: {
      width: '100%',
      height: '100%',
    },
    iconZoom: {
      backgroundColor: theme.color.black,
      borderRadius: 24,
      position: 'absolute',
      padding: theme.spacing.p8,
      bottom: 60,
      right: 16,
    },
    inputContainerStyle: {
      paddingTop: 0,
      marginTop: 0,
      borderBottomWidth: 1,
      borderBottomColor: 'white',
      flex: 1,
    },
    errorStyles: {
      marginBottom: 0,
      marginTop: 0,
      height: 0,
    },
    inputWrapper: {
      flex: 1,
      alignItems: 'center',
      marginHorizontal: theme.spacing.p20
    },
  });
  return styles;
};
