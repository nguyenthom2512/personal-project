import React, { useEffect } from 'react';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { View } from 'react-native';
import { Dimensions } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { useDispatch, useSelector } from 'react-redux';
import { AppText, TodoItems } from '../../../Components';
import { getPriorityRequest } from '../../../Redux/Actions/todoListActions';
import { RootState } from '../../../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../../../Theme';
import styles from './styles';

interface Props {
  data: any[];
}

export default (props: Props) => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const { data } = props;
  const priorityReducer = useSelector(
    (state: RootState) => state.priorityReducer,
  );
  const [slider1ActiveSlide, setSlider1ActiveSlide] = useState(0);

  const renderPagition = () => {
    return (
      <Pagination
        dotsLength={priorityReducer?.data?.length}
        activeDotIndex={slider1ActiveSlide}
        containerStyle={{
          bottom: 20,
        }}
        dotStyle={{
          width: 10,
          height: 10,
          borderRadius: 5,
          marginHorizontal: 8,
          backgroundColor: 'rgba(255, 255, 255, 0.92)',
        }}
        inactiveDotOpacity={0.4}
        inactiveDotScale={0.6}
      />
    );
  };

  return (
    <>
      <Carousel
        ref={(c) => c}
        data={data}
        loop={false}
        style={{ flex: 1 }}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }: any) => {
          const newData = item?.priority_todo?.map((elm: any) => {
            return {
              ...elm,
              ...{
                priority: {
                  color: item.color,
                  created_time: item.created_time,
                  id: item.id,
                  priority_level: item.priority_level,
                  priority_name: item.priority_level,
                  user: item.user,
                },
              },
            };
          });
          return (
            <View style={Styles.container}>
              {
                <TodoItems
                  imageStyle={{ height: 184 }}
                  headerTodoStyle={Styles.header}
                  showMenu
                  data={newData}
                  title={item?.priority_name}
                  onPress={() => {}}
                  containerTodo={{borderRadius: 12}}
                  textStyles={{color:theme.color.black}}
                  
                />
              }
            </View>
          );
        }}
        itemWidth={Dimensions.get('screen').width}
        sliderWidth={Dimensions.get('screen').width}
        inactiveSlideOpacity={4}
        inactiveSlideScale={1}
        activeSlideAlignment={'start'}
        onSnapToItem={setSlider1ActiveSlide}
      />
      {renderPagition()}
    </>
  );
};
