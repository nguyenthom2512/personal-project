import { StyleSheet } from 'react-native';
import { Theme } from '../../../Theme';

export default (theme: Theme) => {
  const styles = StyleSheet.create({
    container: {
      paddingHorizontal: theme.spacing.p16,
      paddingVertical: theme.spacing.p16,
      flex: 1,
    },
    header: {
      backgroundColor: theme.color.mercury,
    },
  });
  return styles;
};
