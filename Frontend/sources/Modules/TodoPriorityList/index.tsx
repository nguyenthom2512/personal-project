import { RouteProp, useNavigation, useRoute } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  SafeAreaView,
  TouchableOpacity,
  View,
  ImageBackground,
  ScrollView,
} from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { AppInputField, AppText, TodoItems } from '../../Components';
import { useTheme, useThemeAwareObject } from '../../Theme';
import styles from './styles';
import Feather from 'react-native-vector-icons/Feather';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  getCompletedWork,
  getInCompleteWork,
  getPriorityRequest,
} from '../../Redux/Actions/todoListActions';
import priorityReducer from '../../Redux/Reducers/priorityReducer';
import { RootState } from '../../Redux/Reducers';
import { useState } from 'react';
import PriorityDetails from './PriorityDetails';
import { Input } from 'react-native-elements/dist/input/Input';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Icon } from 'react-native-elements';

interface Props {
  route: { params: any };
  navigation: any;
}

export default (props: Props) => {
  const { t } = useTranslation();
  const { theme } = useTheme();
  const Styles = useThemeAwareObject(styles);
  const navigation = useNavigation();

  const routeParams = props.route?.params || {};

  const image = {
    uri: 'https://images.unsplash.com/photo-1583121136709-60d71d61894f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8ZWRpdGluZyUyMGJhY2tncm91bmRzfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&w=1000&q=80',
  };
  const dispatch = useDispatch();
  const priorityReducer = useSelector(
    (state: RootState) => state.priorityReducer,
  );

  const [details, setDetails] = useState(false);
  const handle = () => setDetails(!details);
  const [isClick, setIsClick] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const onSearchClick = () => {
    setIsClick(!isClick);
  };

  const onSearchText = (value: string) => {
    setSearchTerm(value);
  };

  // const onSearchResults = () => {
  //   const params: any = {
  //     todo_name: searchTerm
  //   }
  //   dispatch(getPriorityRequest(params))
  // };

  const inCompleteWork = useSelector(
    (state: RootState) => state.incompleteWorkReducer,
  );

  const renderHeader = () => (
    <View style={Styles.headerContainer}>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Icon
          name="arrow-left"
          type={'feather'}
          color={theme.color.white}
          tvParallaxProperties
        />
      </TouchableOpacity>
      {isClick ? (
        <>
          <Input
            onChangeText={onSearchText}
            containerStyle={Styles.inputWrapper}
            placeholderTextColor={theme.color.white}
            placeholder="Search"
            errorStyle={Styles.errorStyles}
            inputContainerStyle={Styles.inputContainerStyle}
            inputStyle={{ color: theme.color.white }}
          />
          <TouchableOpacity onPress={onSearchClick}>
            <AntDesign name="close" size={24} color={theme.color.white} />
          </TouchableOpacity>
        </>
      ) : (
        <>
          <AppText style={Styles.title}>{t('createMissions:todoList')}</AppText>
          <TouchableOpacity onPress={onSearchClick}>
            <Fontisto name="search" size={24} color={theme.color.white} />
          </TouchableOpacity>
        </>
      )}
    </View>
  );
  useEffect(() => {
    const params: any = {
      todo_name: searchTerm,
      is_done: false,
    };
    dispatch(getPriorityRequest(params));
  }, [searchTerm]);

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: theme.color.blackHeader }}>
      {renderHeader()}
      <ImageBackground source={image} style={Styles.container}>
        {details ? (
          <PriorityDetails
            data={priorityReducer?.data?.sort(
              (a: any, b: any) => a.priority_level - b.priority_level,
            )}
          />
        ) : (
          <ScrollView contentContainerStyle={{ paddingRight: 16 }} horizontal>
            {priorityReducer?.data
              ?.sort((a: any, b: any) => a.priority_level - b.priority_level)
              ?.map((el: any) => {
                const newData = el?.priority_todo?.map((elm: any) => {
                  return {
                    ...elm,
                    ...{
                      priority: {
                        color: el.color,
                        created_time: el.created_time,
                        id: el.id,
                        priority_level: el.priority_level,
                        priority_name: el.priority_level,
                        user: el.user,
                      },
                    },
                  };
                });
                return (
                  <View style={Styles.todoList}>
                    <TodoItems
                      showMenu
                      showPriority
                      imageStyle={{ height: 90 }}
                      data={newData}
                      onPress={() => {}}
                      headerTodoStyle={{
                        backgroundColor: theme.color.mercury,
                      }}
                      onItemPress={(id) =>
                        navigation.navigate('TodoDetails', {
                          id,
                          isDone: routeParams?.isDone,
                        })
                      }
                      title={el?.priority_name || ''}
                      containerTodo={{ borderRadius: 12 }}
                      textStyles={{ color: theme.color.black }}
                    />
                  </View>
                );
              })}
          </ScrollView>
        )}
        <TouchableOpacity style={Styles.iconZoom} onPress={handle}>
          <Feather
            name={!details ? 'zoom-in' : 'zoom-out'}
            size={28}
            color={theme.color.white}
          />
        </TouchableOpacity>
      </ImageBackground>
    </SafeAreaView>
  );
};
