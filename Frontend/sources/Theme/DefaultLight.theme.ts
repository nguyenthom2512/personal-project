import { ColorTheme, SpacingTheme, Theme } from './Theme.interface';
import { PixelRatio, Dimensions } from 'react-native';
import { FontSizeTheme } from '.';

const width = Dimensions.get('window').width;
const scale = width / 375;

const responsivePadding = (size: number) => {
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

const resizeFontsize = (size: number) => {
  const newSize = size * scale;
  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

const DEFAULT_LIGHT_COLOR_THEME: ColorTheme = {
  violet: '#A362EA',
  primary: '#FCB71E',
  backgroundColor: '#ffffff',
  white: '#ffffff',
  lightningYellow: '#FCB71E',
  blueChathams: '#134F8B',
  blueCerulean: '#008FE0',
  grayShaft: '#333333',
  gray: '#828282',
  lightGray: '#f5f5f5',
  grayAbbey: '#58595B',
  grayBoder: '#7C7C7C',
  graySilver: '#C6C6C6',
  grayIcon: '#212121',
  greenLimeade: '#5AA800',
  redMonza: '#E20011',
  redValencia: '#DD4545',
  redAlert: '#D80000',
  lightGrayBorder: '#EFEEF4',
  torchRed: '#F4001E',
  black: '#000000',
  blackTitle: '#26272E',
  orange: 'orange',
  mercury: '#E5E5E5',
  mineShaft: '#26272E80',
  shark: '#26272E',
  mediumPurple: '#A362EA',
  athensGray: '#E9ECEF',
  Alto: '#DBDBDB',
  Iron: '#CACFD2',
  mystic: '#E0E7EE',
  holderPlace: '#CACFD2',
  dartBackground: '#343640',
  dartBlue: '#23242A',
  wildSand: '#F4F4F4',
  blackHeader: '#07172F',
  pink: '#FFC3C3',
  salomie: '#FFD487',
  magicMint: '#ABFFC3',
  blizzardBlue: '#A1F1FF',
  grayBox: '#343640',
  whiteSmoke: '#F4F4F4',
  lavenderBlue: '#C5C3FF',
  yellow: '#F4A921',
  lightOrange: '#F45B34',
  blackBox: '#23242A',
  jadeGreen: '#43C8DE',
  blackBoder: '#404047',
  fireBush: '#E8983B',
  drawerContainer: '#18202E',
  greenBox: '#24AC63',
  cinnabar: '#DE3939',
  blackRussian: '#26272E70',
  alto: '#DEDEDE',
  whiteGray: '#EBEBEB',
  charade: '#292D3A',
};

const DEFAULT_LIGHT_SPACING_THEME: SpacingTheme = {
  p2: responsivePadding(2),
  p4: responsivePadding(4),
  p6: responsivePadding(6),
  p8: responsivePadding(8),
  p10: responsivePadding(10),
  p12: responsivePadding(12),
  p15: responsivePadding(15),
  p16: responsivePadding(16),
  p18: responsivePadding(18),
  p20: responsivePadding(20),
  p24: responsivePadding(24),
  p28: responsivePadding(28),
  p32: responsivePadding(32),
  p36: responsivePadding(36),
  p40: responsivePadding(40),
  p44: responsivePadding(44),
  p48: responsivePadding(48),
  p52: responsivePadding(52),
};

const DEFAULT_LIGHT_FONTSIZE_THEME: FontSizeTheme = {
  f4: resizeFontsize(4),
  f8: resizeFontsize(8),
  f11: resizeFontsize(11),
  f12: resizeFontsize(12),
  f14: resizeFontsize(14),
  f15: resizeFontsize(15),
  f16: resizeFontsize(16),
  f17: resizeFontsize(17),
  f18: resizeFontsize(18),
  f19: resizeFontsize(19),
  f20: resizeFontsize(20),
  f22: resizeFontsize(22),
  f24: resizeFontsize(24),
  f25: resizeFontsize(25),
  f28: resizeFontsize(28),
  f32: resizeFontsize(32),
  f34: resizeFontsize(34),
  f36: resizeFontsize(36),
  f40: resizeFontsize(40),
};

export const DEFAULT_LIGHT_THEME_ID = 'default-light';
export const DEFAULT_LIGHT_THEME: Theme = {
  id: DEFAULT_LIGHT_THEME_ID,
  color: DEFAULT_LIGHT_COLOR_THEME,
  spacing: DEFAULT_LIGHT_SPACING_THEME,
  fontSize: DEFAULT_LIGHT_FONTSIZE_THEME,
};
