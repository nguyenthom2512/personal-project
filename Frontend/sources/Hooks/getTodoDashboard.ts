import { useEffect, useState } from 'react';
import todoServices from '../Services/todoServices';
import TodoTag from '../Services/TodoTag';

const useGetTodoDashboard = () => {
  const [data, setData] = useState<any>([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    setLoading(true);

    try {
      const response = await todoServices.getTodoDashboard();
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await todoServices.getTodoDashboard();
        setData(response?.response || []);
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    setTimeout(() => {
      fetchData();
    }, 100);
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetTodoDashboard;
