import { useEffect, useState } from 'react';
import { NotificationItem } from '../interfaces/notification.interface';
import notification from '../Services/notification';

let totalPage = 1;
type TparamGetNotification = {
  page_size: number;
  page: number;
  criteria__isnull?: boolean;
};

let params: TparamGetNotification = {
  page: 1,
  page_size: 10,
  // criteria__isnull: true,
};

const useGetNotification = (param?: any) => {
  const [data, setData] = useState<Array<NotificationItem>>([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [loadMore, setLoadMore] = useState(false);

  const refetch = async () => {
    params = { ...params, page: 1 };
    totalPage = 1;
    try {
      const serverRes = await notification.getNotification(param || params);
      setData(serverRes?.response?.results || []);
      totalPage = Math.ceil(serverRes?.response?.count / params.page_size);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      params = { ...params, page: 1 };
      totalPage = 1;
      try {
        setLoading(true);
        const serverRes = await notification.getNotification(param || params);

        totalPage = Math.ceil(serverRes?.response?.count / params.page_size);
        setData(serverRes?.response?.results || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  const nextPage = async () => {
    if (totalPage === params.page || loadMore) return;
    params = { ...params, page: (params.page += 1) };
    setLoadMore(true);
    try {
      const serverRes = await notification.getNotification(params);
      totalPage = Math.ceil(serverRes?.response?.count / params.page_size);
      setData((prev) => [...prev, ...serverRes?.response?.results]);
    } catch (error: any) {
      setError(error);
    } finally {
      setLoadMore(false);
    }
  };

  return [data, loading, error, refetch, nextPage, loadMore] as const;
};

export default useGetNotification;
