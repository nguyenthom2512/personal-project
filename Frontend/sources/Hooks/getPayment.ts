import { useEffect, useState } from 'react';
import { Payment } from '../interfaces/payment.interface';
import paymentServices from '../Services/paymentServices';

const getPayment = (body?: any) => {
  const [data, setData] = useState<Payment>({ url: '' });
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const refetch = async () => {
    try {
      const response = await paymentServices.postPayment(body);
      setData(response?.response || {});
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await paymentServices.postPayment(body);

        setData(response?.response || {});
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default getPayment;
