import { useEffect, useState } from 'react';
import TodoTag from '../Services/TodoTag';

const useGetTodoTag = (filters?: any) => {
  const [data, setData] = useState([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    setLoading(true);

    try {
      const response = await TodoTag.getTodoTag();
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await TodoTag.getTodoTag();
        setData(response?.response || []);
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    setTimeout(() => {
      fetchData();
    }, 100);
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetTodoTag;
