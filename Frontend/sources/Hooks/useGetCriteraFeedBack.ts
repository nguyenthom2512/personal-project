import { useEffect, useState } from 'react';
import { CriteriaFeedback } from '../interfaces/feedback.interface';
import feedbackService from '../Services/feedbackService';

const useGetCriteriaFeedBack = () => {
  const [data, setData] = useState<Array<CriteriaFeedback>>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const refetch = async () => {
    try {
      const response = await feedbackService.getCriteriaFeedBack();
      setData(response?.response || []);
    } catch (error) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await feedbackService.getCriteriaFeedBack();
        setData(response?.response || []);
        setLoading(false);
      } catch (error) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetCriteriaFeedBack;
