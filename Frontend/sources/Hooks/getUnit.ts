import { useEffect, useState } from 'react';
import { Unit } from '../interfaces/selection.interface';
import selectionService from '../Services/selectionService';

const useGetUnit = (filters?: any) => {
  const [data, setData] = useState<Array<Unit>>([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await selectionService.getUnit();

      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await selectionService.getUnit();

        setData(response?.response || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetUnit;
