import { useEffect, useState } from 'react';
import getOkrRelate from '../Services/getOkrRelate';

const useGetOkrRelate = (id: any) => {
  const [data, setData] = useState<any>({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    if (!id) return;
    try {
      const response = await getOkrRelate.getDataOKR(id);
      setData(response?.response || {});
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await getOkrRelate.getDataOKR(id);
        console.log('responseRelate',response.response);
        
        setData(response.response || {});
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };
    if (id) {
      fetchData();
    }
    // return () => { };
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetOkrRelate;
