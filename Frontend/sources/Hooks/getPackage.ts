import { useEffect, useState } from 'react';
import { Package } from '../interfaces/package.interface';
import { Department } from '../interfaces/selection.interface';
import packageServices from '../Services/packageServices';
import selectionService from '../Services/selectionService';

const useGetPackage = (filters?: any) => {
  const [data, setData] = useState<Array<Package>>([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await packageServices.getPackages({});
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await packageServices.getPackages({});

        setData(response?.response || []);
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetPackage;
