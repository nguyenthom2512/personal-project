import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import { AccessToken, LoginManager } from 'react-native-fbsdk-next';

const socialSignin = () => {
  const google = async () =>
    new Promise<any>(async (resolve, reject) => {
      try {
        await GoogleSignin.hasPlayServices();
        GoogleSignin.signIn()
          .then((data) => {
            console.log('dataaa', data);

            GoogleSignin.getTokens()
              .then((res) => {
                resolve(res);
              })
              .catch((err) => {
                console.log('catch getTokens', err);
                reject(err);
              });
          })
          .catch((er) => {
            console.log('catch signIn', er);
            reject(er);
          });
      } catch (error: any) {
        reject(error);
        if (error.code === statusCodes.SIGN_IN_CANCELLED) {
          // user cancelled the login flow
        } else if (error.code === statusCodes.IN_PROGRESS) {
          // operation (e.g. sign in) is in progress already
        } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          // play services not available or outdated
        } else {
          // some other error happened
        }
      }
    });

  const facebook = () =>
    new Promise<any>((resolve, reject) => {
      LoginManager.logInWithPermissions(['public_profile']).then(
        function (result) {
          if (result.isCancelled) {
            reject('user cancel');
          } else {
            AccessToken.getCurrentAccessToken().then((data) => {
              resolve(data);
            });
          }
        },
        function (error) {
          __DEV__ && console.log('Login fail with error: ' + error);
        },
      );
    });

  return {
    google,
    facebook,
  };
};

export default socialSignin;
