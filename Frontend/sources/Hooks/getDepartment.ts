import { useEffect, useState } from 'react';
import { Department } from '../interfaces/selection.interface';
import selectionService from '../Services/selectionService';

const useGetDepartment = (filters?: any) => {
  const [data, setData] = useState<Array<Department>>([]);
  const [error, setError] = useState<string>('');
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await selectionService.getDepartment();
      if (response.error) {
        setError(response.errorMessage ?? '');
      } else {
        setData(response?.response || []);
      }
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await selectionService.getDepartment();
        if (response.error) {
          setError(response.errorMessage ?? '');
        } else {
          setData(response?.response || []);
        }
        setLoading(false);
      } catch (error: any) {
        console.log('errreas', error);

        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetDepartment;
