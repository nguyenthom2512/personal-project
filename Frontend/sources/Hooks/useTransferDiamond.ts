import { useEffect, useState } from 'react';
import { Priority } from '../interfaces/priority.interface';
import { OKR } from '../interfaces/selection.interface';
import priorityService from '../Services/priorityService';
import transferDiamondService from '../Services/transferDiamondService';

const useTransferDiamond = () => {
  
  const [data, setData] = useState([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async (params: any) => {
    setLoading(true);
    try {
      const response = await transferDiamondService.postTransferDiamond(params);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  // useEffect(() => {
  //   const fetchData = async () => {
  //     try { 
  //       setLoading(true);

  //       const response = await transferDiamondService.postTransferDiamond();
        
  //       setData(response?.response || []);
  //     } catch (error: any) {
  //       setError(error);
  //     } finally {
  //       setLoading(false);
  //     }
  //   };
  //   setTimeout(() => {
  //     fetchData();
  //   }, 100);
  //   return () => {};
  // }, []);

  return [data, loading, error, refetch] as const;
};

export default useTransferDiamond;
