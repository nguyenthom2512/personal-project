import { useEffect, useState } from 'react';
import { OKRDetail } from '../interfaces/okr.interface';
import { Department, OKR } from '../interfaces/selection.interface';
import dataOkr from '../Services/dataOkr';

const useGetOkrResult = ({ id }: { id: any }) => {
  const [data, setData] = useState<any>({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const refetch = async (idOKR?: string | number | undefined) => {
    try {
      const response = await dataOkr.getOkrResult(idOKR ? idOKR : id);

      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dataOkr.getOkrResult(id);

        setData(response?.response || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    if (id) {
      fetchData();
    }
    // return () => { };
  }, [id]);

  return [data, loading, error, refetch] as const;
};

export default useGetOkrResult;
