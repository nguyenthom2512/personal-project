import { useEffect, useState } from 'react';
import { PresentItem } from '../interfaces/present.interface';
import presentService from '../Services/presentService';

// let totalPage = 1;
// type TparamGetPresent = {
//   page_size: number;
//   page: number;
// };

// let params: TparamGetPresent = {
//   page: 1,
//   page_size: 10,
// };

const useGetPresent = (param?: any) => {
  const [data, setData] = useState<Array<PresentItem>>([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  //   const [loadMore, setLoadMore] = useState(false);

  const refetch = async () => {
    // params = { ...params, page: 1 };
    // totalPage = 1;
    try {
      const serverRes = await presentService.getPresent(param);

      //   setData(serverRes?.response?.results || []);
      setData(serverRes?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      //   params = { ...params, page: 1 };
      //   totalPage = 1;
      try {
        setLoading(true);
        const serverRes = await presentService.getPresent(param);
        // totalPage = Math.ceil(serverRes?.response?.count / params.page_size);
        setData(serverRes?.response || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  //   const nextPage = async () => {
  //     if (totalPage === params.page || loadMore) return;
  //     params = { ...params, page: (params.page += 1) };
  //     setLoadMore(true);
  //     try {
  //       const serverRes = await presentService.getPresent(params);
  //       totalPage = Math.ceil(serverRes?.response?.count / params.page_size);
  //       setData((prev) => [...prev, ...serverRes?.response?.results]);
  //     } catch (error: any) {
  //       setError(error);
  //     } finally {
  //       setLoadMore(false);
  //     }
  //   };

  return [data, loading, error, refetch] as const;
};

export default useGetPresent;
