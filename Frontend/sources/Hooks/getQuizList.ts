import { useEffect, useState } from 'react';
import { IListQuiz, Question } from '../interfaces/quiz.interface';
import getQuizService from '../Services/getQuizService';

const useGetListQuiz = (filters?: any) => {
  const [data, setData] = useState<Question[]>([]);
  //   const [data, setData] = useState<IListQuiz>();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await getQuizService.getQuiz();
      // setData(response?.response?.questions || []);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await getQuizService.getQuiz();
        // setData(response?.response?.questions || []);
        setData(response?.response);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetListQuiz;
