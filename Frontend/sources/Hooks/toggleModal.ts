import { isEmpty } from 'lodash';
import { useCallback, useState } from 'react';

const useToggleDialog = () => {
  const [visible, setVisible] = useState<boolean>(false);
  const [dataConfirm, setDataConfirm] = useState<any>({});

  const open = (data?: any) => {
    setVisible(true);
    setDataConfirm(data);
  };

  const close = () => {
    setVisible(false);
    setDataConfirm(null);
  };

  const toggle = () => setVisible(!visible);

  const isVisible = !isEmpty(dataConfirm) || visible;

  return [isVisible, dataConfirm, toggle, open, close] as const;
};

export default useToggleDialog;
