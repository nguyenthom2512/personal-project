import { useEffect, useState } from 'react';
import { OKRList, UserItem } from '../interfaces/okr.interface';
import { OKR } from '../interfaces/selection.interface';
import DataOkr from '../Services/dataOkr';
import userService from '../Services/userService';

interface IParams {
  okr?: number | string;
  department?: number | string;
}

const useGetStarUser = () => {
  const [data, setData] = useState<Partial<UserItem[]>>([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const getUser = async (params: IParams) => {
    const { okr, department } = params;
    let response;
    if (okr) {
      response = await DataOkr.getOkrUser({ okr });
      return response;
    }
    if (department) {
      response = await userService.getUser({ department });
      return response;
    }
  };

  const refetch = async (params: IParams) => {
    try {
      const response = await getUser(params);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {}, []);

  return [data, loading, error, refetch] as const;
};

export default useGetStarUser;
