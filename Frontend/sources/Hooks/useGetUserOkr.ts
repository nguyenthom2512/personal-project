import { useEffect, useState } from 'react';
import { Department } from '../interfaces/selection.interface';
import selectionService from '../Services/selectionService';

const useGetUserOkr = (body: object) => {
  const [data, setData] = useState<Array<Department>>([]);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await selectionService.getUser(body);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await selectionService.getUser(body);

        setData(response?.response || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetUserOkr;
