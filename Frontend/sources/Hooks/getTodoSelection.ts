import { useEffect, useState } from 'react';
import { TodoSelection } from '../interfaces/selection.interface';
import { TodoListSelection } from '../interfaces/todo.interface';
import todoServices from '../Services/todoServices';

const useGetTodoSelection = (params?: any) => {
  const [data, setData] = useState<TodoListSelection>({ count: 0, next: '', results: [] });
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      setLoading(true);
      const response = await todoServices.getTodoSelection(params);

      if (params.page === 1) {
        setData(response?.response || []);
      } else {
        setData({
          ...response?.response,
          results: [...data?.results, response?.response?.results]
        })
      }
      setLoading(false);
    } catch (error: any) {
      setError(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await todoServices.getTodoSelection(params);

        setData(response?.response || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => { };
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetTodoSelection;
