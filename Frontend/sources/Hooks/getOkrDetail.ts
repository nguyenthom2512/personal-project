import { useEffect, useState } from 'react';
import { OKRDetail } from '../interfaces/okr.interface';
import { Department, OKR } from '../interfaces/selection.interface';
import dataOkr from '../Services/dataOkr';

const useGetOkrDetail = (id: any) => {
  const [data, setData] = useState<any>({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    if (!id) return;
    try {
      const response = await dataOkr.getOkrResult(id);
      setData(response?.response || {});
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dataOkr.getOkrResult(id);

        setData(response?.response || {});
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };
    if (id) {
      fetchData();
    }
    // return () => { };
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetOkrDetail;
