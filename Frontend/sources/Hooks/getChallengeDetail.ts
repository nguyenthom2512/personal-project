import { useEffect, useState } from 'react';
import { Challenge } from '../interfaces/challenge.interface';
import getChallenge from '../Services/getChallenge';

const useGetChallengeDetail = (id: any) => {
  const [data, setData] = useState<Challenge>();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await getChallenge.getChallenge(id);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await getChallenge.getChallenge(id);

        setData(response?.response || []);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
  }, [id]);

  return [data, loading, error, refetch] as const;
};

export default useGetChallengeDetail;
