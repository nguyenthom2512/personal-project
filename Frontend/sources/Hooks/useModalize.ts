import { useRef } from 'react';

const useModalize = () => {
  const ref = useRef<any>(null);
  const open = () => {
    ref.current && ref.current.open();
  };
  const close = () => {
    ref.current && ref.current.close();
  };
  return { ref, open, close };
};

export default useModalize;
