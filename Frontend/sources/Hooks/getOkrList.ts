import { useEffect, useState } from 'react';
import { OKRList } from '../interfaces/okr.interface';
import { OKR } from '../interfaces/selection.interface';
import DataOkr from '../Services/dataOkr';

const useGetOKRList = ({ params }: { params: any }) => {
  const [data, setData] = useState<OKRList>();

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);

  const refetch = async () => {
    try {
      const response = await DataOkr.getOKRList(params);

      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await DataOkr.getOKRList(params);

        setData(response?.response || []);

        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetOKRList;
