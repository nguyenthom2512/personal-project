import { useEffect, useState } from 'react';
import commentService from '../Services/commentService';

const useGetComment = (filters?: any) => {
  const [data, setData] = useState<any>([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    setLoading(true);
    try {
      const response = await commentService.getCommentTodo(filters);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);

        const response = await commentService.getCommentTodo(filters);

        setData(response?.response || []);
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    if (filters) {
      fetchData();
    }

    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetComment;
