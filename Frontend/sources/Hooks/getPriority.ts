import { useEffect, useState } from 'react';
import { Priority } from '../interfaces/priority.interface';
import { OKR } from '../interfaces/selection.interface';
import priorityService from '../Services/priorityService';

const useGetPriority = (filters?: any) => {
  
  const [data, setData] = useState<Priority[]>([]);

  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    setLoading(true);
    try {
      const response = await priorityService.getTodoPriority();
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try { 
        setLoading(true);

        const response = await priorityService.getTodoPriority();
        
        setData(response?.response || []);
      } catch (error: any) {
        setError(error);
      } finally {
        setLoading(false);
      }
    };
    setTimeout(() => {
      fetchData();
    }, 100);
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetPriority;
