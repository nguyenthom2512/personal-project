import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Alert } from 'react-native';
import { Unit } from '../interfaces/selection.interface';
import roomServices, { IGetRoomDetailParams } from '../Services/roomServices';
import selectionService from '../Services/selectionService';

const useGetRoomDetail = (params: IGetRoomDetailParams) => {
  const [data, setData] = useState<any>({});
  const [error, setError] = useState<any>(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    if (params.room_id < 0) return;
    try {
      setLoading(true);
      const response = await roomServices.getRoomDetail(params);

      if (response.error) {
        setError(response.errorMessage || '');
      } else {
        setData(response?.response || {});
      }
    } catch (error: any) {
      setError(error);
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await roomServices.getRoomDetail(params);
        console.log({ response }, { params });

        if (response.error) {
          setError(response.errorMessage || '');
        } else {
          setData(response?.response || {});
        }
      } catch (err: any) {
        setError(err);
      } finally {
        setLoading(false);
      }
    };

    params.room_id > 0 && fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetRoomDetail;
