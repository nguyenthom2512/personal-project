import { useEffect, useState } from 'react';
import { Quiz } from '../interfaces/quiz.interface';
import getQuizService from '../Services/getQuizService';

const useGetDataDetailQuiz = (id: any) => {
  // const [data, setData] = useState<Quiz[]>([]);
  const [data, setData] = useState<Quiz>();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await getQuizService.getQuizDetail(id);
      // setData(response?.response?.questions || []);
      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await getQuizService.getQuizDetail(id);
        // setData(response?.response?.questions || []);
        setData(response?.response);
        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    id && fetchData();
  }, [id]);

  return [data, loading, error, refetch] as const;
};

export default useGetDataDetailQuiz;
