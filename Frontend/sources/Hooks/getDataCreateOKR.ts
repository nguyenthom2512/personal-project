import { useEffect, useState } from 'react';
import { DataCreateOkr } from '../interfaces/selection.interface';
import dataCreateOkrService from '../Services/dataCreateOkrService';

const useGetCreateOKR = (filters?: any) => {
  const [data, setData] = useState<DataCreateOkr>();
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const refetch = async () => {
    try {
      const response = await dataCreateOkrService.getDataCreateOKR();
      setData(response?.response || {});
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await dataCreateOkrService.getDataCreateOKR();

        setData(response?.response || {});

        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetCreateOKR;
