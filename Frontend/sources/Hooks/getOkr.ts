import { useEffect, useState } from 'react';
import { OKR } from '../interfaces/selection.interface';
import DataOkr from '../Services/dataOkr';

const useGetOKR = (filters?: any) => {
  const [data, setData] = useState<Array<OKR>>([]);

  const [error, setError] = useState('');
  const [loading, setLoading] = useState(true);

  const refetch = async (params: any) => {
    try {
      const response = await DataOkr.getDataOKR(params);

      setData(response?.response || []);
    } catch (error: any) {
      setError(error);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      try {
        setLoading(true);
        const response = await DataOkr.getDataOKR();

        setData(response?.response || []);

        setLoading(false);
      } catch (error: any) {
        setError(error);
        setLoading(false);
      }
    };

    fetchData();
    return () => {};
  }, []);

  return [data, loading, error, refetch] as const;
};

export default useGetOKR;
