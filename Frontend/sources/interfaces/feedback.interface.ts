export interface FeedbackComment {
  id?: number;
  rate?: null;
  criteria_title?: null;
  okr_name?: string;
  user_data?: UserData;
  created_time?: Date;
  content?: string;
  quantity?: number;
  user?: number;
  user_received?: number;
  okr?: number;
  criteria?: null;
  user_received_data: UserData;
}

export interface UserData {
  full_name?: string;
  img_url?: string;
}
export interface CriteriaFeedback {
  id: number;
  title_rate: string;
  created_time: Date;
  title: string;
  rate: number;
  user: number;
}
