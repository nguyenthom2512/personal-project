export interface ICallContext {
  roomId: number | string;
  setRoomId: (value: number | string) => void;
}
