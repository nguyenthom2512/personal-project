export interface Challenge {
  id: number;
  created_time: Date;
  public: boolean;
  content: string;
  user: number;
  okr: number;
  okr_name: string;
  challenged_company: number;
  challenged_user: number[];
  challenged_department: number[];
  challenge_comment: ChallengeComment[];
  user_data: UserData;
}

export interface ChallengeComment {
  id: number;
  user_data: UserDataChallengeComment;
  created_time: Date;
  title: string;
  user: number;
  todo: number;
  challenge: number;
}
export interface UserData {
  full_name: string;
  img_url?: string;
}
export interface UserDataChallengeComment {
  full_name: string;
  img_url?: string;
}
