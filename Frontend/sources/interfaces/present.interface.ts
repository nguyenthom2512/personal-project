export interface PresentItem {
  id?: number;
  created_time?: Date;
  present_name?: string;
  img_url?: string;
  price?: number;
  present_img?: string;
}
