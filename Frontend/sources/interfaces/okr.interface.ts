export interface OkrResult {
  id: number;
  user: number;
  key_result: string;
  target: number;
  current_done: number;
  unit_name: string;
  unit: number;
  plan_url: string;
  result_url: string;
  deadline: Date;
  processed: number;
  percent_changed: number;
  confident: any;
}

export interface UserData {
  id: number;
  full_name: string;
  img_url?: any;
}

export interface RaicUser {
  id: number;
  user_data: UserData;
  raic_type: number;
  user: number;
  okr: number;
  todo?: any;
}

export interface OKRDetail {
  id: number;
  okr_result: OkrResult[];
  raic_user: RaicUser[];
  created_time: Date;
  object_name: string;
  is_done: boolean;
  confident: number;
  challenge: boolean;
  percent_changed: number;
  user: number;
  department: number;
  okr_parent: number;
  okr_relate: any[];
}

export interface User {
  id: number;
  email: string;
  full_name: string;
  img_url?: any;
  date_of_birth: string;
  fb_link: string;
  phone_number: string;
}

export interface Result {
  id: number;
  created_time: Date;
  object_name: string;
  owner_okr: string;
  count_result: number;
  percent_changed: number;
  confident: number;
  percent_completed: number;
  count_child: number;
  okr_result: any[];
  user: User;
}

export interface OKRList {
  count: number;
  next?: any;
  previous?: any;
  results: Result[];
}

// OKR list

export interface OKRArrayObject {
  id?: number;
  created_time?: Date;
  object_name?: string;
  owner_okr?: string;
  count_result?: number;
  percent_changed?: number;
  confident?: number;
  percent_completed?: number;
  count_child?: number;
  okr_result?: OkrResult[];
  user?: User;
  last_checkin?: LastCheckin | null;
  is_match: boolean;
}

export interface LastCheckin {
  checkin_date?: Date;
  checkin_status?: number;
}

export interface UserItem {
  full_name: string;
  id: number;
  img_url?: string;
  name_with_department: string;
}
