export interface Package {
  id?: number;
  created_time?: Date;
  package_name?: string;
  duration?: number;
  price?: number;
  description?: string;
  is_recommend?: boolean;
}
