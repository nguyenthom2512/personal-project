export interface AttachmentQuestion {
  file_url: string;
  file_type: number;
}

export interface Answer {
  id: number;
  content: string;
}

export interface Question {
  id: number;
  attachment_question: AttachmentQuestion[];
  answers: Answer[];
  created_time: Date;
  content: string;
  question_type: number;
  duration: number;
  user: number;
}

export interface Quiz {
  id: number;
  questions: Question[];
  created_time: Date;
  title: string;
  description: string;
  duration: number;
  total_point: number;
  user_create: number;
}

/// list quiz
export interface AttachmentQuestion {
  file_url: string;
  file_type: number;
}

export interface Answer {
  id: number;
  content: string;
  weight: number;
  user_id: number;
}

export interface Question {
  id: number;
  attachment_question: AttachmentQuestion[];
  answers: Answer[];
  created_time: Date;
  content: string;
  question_type: number;
  duration: number;
  user: number;
}

export interface IListQuiz {
  id: number;
  questions: Question[];
  created_time: Date;
  title: string;
  description: string;
  duration: number;
  total_point: number;
  user_create: number;
}
