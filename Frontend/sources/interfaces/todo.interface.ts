export interface RaicUser {
  okr: string | number | undefined;
  raic_type: string | number | undefined;
  todo: string | number | undefined;
  user: string | number | undefined;
}

export interface TodoCreateBody {
  confident: string | number | undefined;
  date: string | number | undefined | Date | null;
  end_time: string | number | undefined | Date | null;
  description: string | number | undefined;
  is_done: boolean;
  okrId: string | number | undefined;
  priority: string | number | undefined;
  // raic_user?: RaicUser[];
  result: string | number | undefined;
  start_time: string | number | undefined | Date | null;
  check_list: any[];
  tag: any[];
  todo_name: string | number | undefined;
  // todo_parent: string | number | undefined;
  user: string | number | undefined;
  user_accountable: any[];
  user_consult: any[];
  user_inform: any[];
  user_responsible: any[];
  resultName: string | undefined
}

export interface Priority {
  id: number;
  created_time: Date;
  priority_level: number;
  priority_name: string;
  color: string;
  user: number;
}

export interface Tag {
  id: number;
  created_time: Date;
  tag_name: string;
  tag_code: string;
  user: number;
}

export interface TodoListSelection {
  count: number;
  next: string;
  previous?: any;
  results: Array<TodoDetail>;
}


// todoDetail
export interface TodoDetail {
  id?:            number;
  priority?:      Priority;
  tag?:           Tag[];
  raic_user?:     any[];
  result_name?:   string;
  okr_name?:      string;
  check_list?:    CheckList[];
  comment_count?: number;
  okr_id?:        number;
  department?:    string;
  created_time?:  Date;
  todo_name?:     string;
  description?:   string;
  start_time?:    null;
  end_time?:      null;
  confident?:     number;
  is_done?:       boolean;
  img_url?:       string;
  date?:          null;
  user?:          number;
  result?:        number;
  todo_comment?:  Array<any>
}

export interface CheckList {
  id:      number;
  content: string;
  is_done: boolean;
}

export interface Priority {
  id:             number;
  created_time:   Date;
  priority_level: number;
  priority_name:  string;
  color:          string;
  user:           number;
}

