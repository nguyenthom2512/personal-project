import { CalendarType } from '../Modules/AgendaCalendar';
import { TodoDetail } from './todo.interface';

export interface RootReducer {
  userReducer: any;
  okrReducer: IOkrReducer;
  todoListReducer: ITodolistReducer;
  calendarReducer: ICalendarReducer;
}

export interface ICalendarReducer {
  calendarType?: CalendarType;
  type: string;
  listEventByDay?: any[];
  listEvent?: any[];
  todoList?: any[];
  errorMessage: string;
}

export interface IOkrReducer {
  data: { results: Array<any> };
  dataParent: { results: Array<any> };
  type: string;
  loading: boolean;
}

export interface ITodolistReducer {
  data?: Array<any>;
  type: string;
  errorMessage: string;
  dataDetail: TodoDetail;
}

export interface IUserReducer {
  data?: {};
  type: string;
  loading: boolean;
  errorMessage: string;
}
