import { OkrResult } from './okr.interface';

export interface AppUser {
  id: number;
  full_name: string;
  img_url?: string;
}

export interface OKR {
  confident: number;
  count_result: number;
  id: number;
  object_name: string;
  okr_result: OkrResult[];
  owner_okr: string;
  percent_changed: number;
  percent_completed: number;
  user: AppUser;
}
export interface Unit {
  created_time: string;
  id: number;
  unit_name: string;
}

export interface DataCreateOkr {
  AppUser: AppUser[];
  Department: Department[];
  Unit: Unit[];
}

export interface Company {
  id: number;
  created_time: string;
  company_name: string;
}
export interface Department {
  company: Company;
  created_time: string;
  department_name: string;
  id: number;
}
export interface Unit {
  created_time: string;
  id: number;
  unit_name: string;
}
export interface TodoSelection {
  id: number;
  todo_name: string;
}
