export interface UserData {
  id: number;
  full_name: string;
  img_url?: any;
}

export interface RaicUser {
  id: number;
  user_data: UserData;
  raic_type: number;
  user: number;
  okr?: any;
  todo: number;
}

export interface PriorityTodo {
  id: number;
  raic_user: RaicUser[];
  created_time: Date;
  description?: any;
  todo_name: string;
  start_time: Date;
  end_time: Date;
  confident: number;
  is_done: boolean;
  user: number;
  result: number;
  todo_parent?: any;
  priority: number;
  tag: any[];
}

export interface Priority {
  id: number;
  priority_todo: PriorityTodo[];
  created_time: Date;
  priority_level: number;
  priority_name: string;
  color: string;
}

export interface PriorityBody {
  color: string;
  id: string | number;
  priority_level: string | number;
  priority_name: string;
  user: string;
}
