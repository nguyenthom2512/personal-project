export interface Comment {
  user: number,
  title: string,
  created_time?: string,
  user_data?: {
    full_name?: string,
    img_url?: string,
  },
  todo?: number,
  challenge?: number,
  id?: number | string | undefined,
}