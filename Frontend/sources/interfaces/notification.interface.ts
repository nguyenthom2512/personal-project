export interface NotificationItem {
  id: number;
  user_sent_data: UserSentData;
  created_time: Date;
  title: string;
  content: string;
  type_notification: number;
  is_read: boolean;
  user: number;
  user_sent: number;
  challenge: number;
  inspiration: null;
  todo: null;
  okr_id: number | null;
}

export interface UserSentData {
  full_name: string;
  img_url: null;
}
