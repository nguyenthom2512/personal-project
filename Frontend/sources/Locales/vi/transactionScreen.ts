export default {
  title: 'Chi tiết giao dịch',
  detailTransaction: 'Thông tin giao dịch',
  id: 'Mã ID',
  time: 'Thời gian',
  userReceiver: 'Người nhận',
  content: 'Nội dung',
  amount: 'Số lượng',
  numberStar: '{{star}} sao'
};
