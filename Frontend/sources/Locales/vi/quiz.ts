export default {
  quizDone:
    'Đã hoàn thành bài Quiz. Bạn đạt được {{point}} điểm. Điểm đã được gửi đến cho người tạo',
  title: 'Quiz',
};
