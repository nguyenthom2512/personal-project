export default {
  selectQuarter: 'Lựa chọn quý',
  selectDepartment: 'Lựa chọn Phòng ban',
  okrs: 'OKRs',
  title: 'Tổng quan OKRs',
  noResult: 'Không có OKR thoả mãn',
  pleaseSelectDepartment: 'Vui lòng chọn phòng ban',
};
