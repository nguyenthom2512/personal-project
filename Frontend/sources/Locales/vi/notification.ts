export default {
  notification: 'Thông báo',
  turnOffNotifications: 'Tắt thông báo',
  clearNotifications: 'Xoá thông báo',
  roomNotFound: 'Phòng họp không tồn tại, vui lòng kiểm tra lại',
  confirmJoinCall:
    'Phòng họp "{{roomName}}" sẽ bắt đầu lúc {{time}} ({{remainTime}}). Bạn có muốn tham gia phòng họp lúc này?',
  joinRoom: 'Tham gia phòng họp check-in',
};
