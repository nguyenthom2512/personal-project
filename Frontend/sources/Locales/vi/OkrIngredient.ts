export default {
  change: 'Thay đổi: ',
  confident: 'Mức độ tự tin: ',
  keyResult: 'Kết quả chính: ',
  results: 'Kết quả',
  seeMore: 'Có {{count}} OKR cấp dưới',
  target: 'Mục tiêu: ',
  unit: 'Đơn vị: ',
  current_done: 'Đạt được: ',
};
