export default {
  store: 'Cửa hàng',
  itemStore: 'Cửa hàng vật phẩm',
  buy:'Mua',
  change:'Đổi quà',
  askChange:'Bạn có muốn đổi món quà này không?',
  material:'Chất liệu',
  changeDiamond:'Đổi kim cương',
};
