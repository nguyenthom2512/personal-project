export default {
  title: 'Bảng giá',
  onlineAccount: ' tài khoản online',
  chatChannel: ' kênh chat',
  cloudStorage: ' lưu trữ đám mây',
  suggestion: 'Khuyên dùng',
  upgradeAccount: 'Nâng cấp tài khoản',
  month: ' /{{month}} tháng',
  getListFailed: "Lấy danh sách không thành công",
  processTransaction: "Giao dịch của bạn đang được xử lý"
};
