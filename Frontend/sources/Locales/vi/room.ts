export default {
  createRoom: 'Tạo phòng họp',
  failedRoom: 'Tạo phòng họp thất bại',
  successRoom: 'Tạo phòng họp thành công',
  nameRoom: 'Tên phòng họp',
  passRoom: 'Mật khẩu phòng họp',
  startTime: 'Thời gian bắt đầu'
}