export default {
  Okr: {
    department: 'Phòng Ban',
    okrParent: 'OKRs cấp trên',
    objectName: 'Mục tiêu của bạn',
    okrChallenge: 'OKR thách thức',
    okrRelate: 'OKR liên kết chéo',
    userResponsible: 'Người/Team cùng thực hiện OKRs',
    userAccountable: 'Người duyệt OKRs',
    userInform: 'Người thông báo',
    userConsult: 'Người tham vấn',
    import: 'Nhập',
    add: 'Thêm',
    createSuccess: 'Tạo mới thành công',
    createFalse: 'Tạo mới không thành công',
    listPerson: 'Danh sách người chịu trách nhiệm',
    createOkr: 'Tạo mới OKR',
  },
  OkrKR: {
    keyResult: 'Kết quả chính',
    target: 'Mục tiêu',
    unit: 'Đơn vị đo lường',
    planUrl: 'Link kế hoạch',
    resultUrl: 'Link kết quả',
    deadline: 'Thời gian hoàn thành',
    dob:'Ngày sinh'
  },
  targeTypeError: 'Mục tiêu phải là một số nguyên dương',
  resultParent: 'Kết quả cấp trên'
};
