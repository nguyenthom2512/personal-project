import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { ModuleType } from 'i18next';
import AsyncStorage from '@react-native-community/async-storage';
import * as RNLocalize from 'react-native-localize';

import vi from './vi';
import en from './en';
import asyncStorage from '../Helpers/asyncStorage';

const LANGUAGES = {
  vi,
  en,
};

const LANG_CODES = Object.keys(LANGUAGES);

const LANGUAGE_DETECTOR = {
  type: 'languageDetector' as ModuleType,
  async: true,
  detect: (callback: any) => {
    AsyncStorage.getItem(asyncStorage.userLanguage, (err, language) => {
      // if error fetching stored data or no language was stored
      // display errors when in DEV mode as console statements
      if (err || !language) {
        if (err) {
        } else {
        }
        // const findBestAvailableLanguage = RNLocalize.findBestAvailableLanguage(LANG_CODES);

        // callback(findBestAvailableLanguage?.languageTag || "vi");
        callback('vi');

        return;
      }
      callback(language);
    });
  },
  init: () => {},
  cacheUserLanguage: (language: any) => {
    AsyncStorage.setItem(asyncStorage.userLanguage, language);
  },
};

i18n
  // detect language
  .use(LANGUAGE_DETECTOR)
  // pass the i18n instance to react-i18next.
  .use(initReactI18next)
  // set options
  .init({
    resources: LANGUAGES,
    react: {
      useSuspense: false,
    },
    interpolation: {
      escapeValue: false,
    },
    defaultNS: 'common',
  });
