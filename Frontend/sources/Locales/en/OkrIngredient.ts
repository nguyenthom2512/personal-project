export default {
  change: 'Change: ',
  confident: 'Confidence level: ',
  keyResult: 'Key results: ',
  results: 'Results',
  seeMore: 'Have {{count}} lower level OKRs',
  target: 'Target: ',
  unit: 'Unit: ',
  current_done: 'Achieve: ',
};
