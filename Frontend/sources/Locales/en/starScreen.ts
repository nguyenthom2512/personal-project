export default {
  title: 'Your stars',
  sendStarfor: 'Give stars for {{user}}',
  receiverStarfrom: 'Receive from {{user}}',
  success: 'Success',
};
