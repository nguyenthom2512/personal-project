export default {
  title: 'Transaction details',
  detailTransaction: 'Transaction information',
  id: 'ID code',
  time: 'Time',
  userReceiver: 'Receiver',
  content: 'Content',
  amount: 'Số lượng',
  numberStar: '{{star}} stars'
};
