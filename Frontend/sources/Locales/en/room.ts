export default {
  createRoom: 'Create a meeting room',
  failedRoom: 'Meeting room creation failed',
  successRoom: 'Create a successful meeting room',
  nameRoom: 'Name of meeting room',
  passRoom: 'Meeting room password',
  startTime: 'Start time'
}