export default {
  OKRIngredientScreen: 'OKRIngredientScreen',
  department: 'Department',
  OKRs_Company: 'Company OKRs',
  change: 'Change: ',
  confident: 'Confidence level: ',
  keyResult: 'Key Results: ',
  results: 'Results:',
  seeMore: 'Have {{count}} lower level OKRs',
  target: 'Target',
  unit: 'Unit',
  current_done: 'Achieve: ',
  listKR: 'List of KRs',
  updateOkr: 'Update OKR',
  percentCompleted: 'Progress:',
  staff: 'Employee:',
  okrDetail: 'Detailed OKRs',
  plan: 'Plan:',
  day: 'Days',
  notCheckIn: "Haven't checked in yet",
  deadline:'Completion time:',
  feedback:'Feedback',
  criteria:'Criteria',
  content:'Content',
  chooseSelect:'Select criteria',
  success:'Feedback submitted successfully',
  feedbackSuccess: 'Feedback submitted successfully',
  cannotUpdateOfother: "Can't update someone else's KR",
  result: '{{result}} results',
};
