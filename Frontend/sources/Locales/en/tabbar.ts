export default {
  home: 'Home page',
  OKRS: 'OKRS',
  store: 'Store',
  account: 'Account',
  create: 'Create',
};
