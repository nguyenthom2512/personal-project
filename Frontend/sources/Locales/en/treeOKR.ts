export default {
  selectQuarter: 'Quarter selection',
  selectDepartment: 'Select Department',
  okrs: 'OKRs',
  title: 'OKRs Overview',
  noResult: 'There are no satisfying OKRs',
  pleaseSelectDepartment: 'Please select a department',
};
