export default {
  notification: 'Notice',
  turnOffNotifications: 'Turn off notifications',
  clearNotifications: 'Clear notifications',
  roomNotFound: 'The meeting room does not exist, please check again',
  confirmJoinCall:
    'The meeting room "{{roomName}}" will start when {{time}} ({{remainTime}}). You want to join the meeting room now',
  joinRoom: 'Join the check-in meeting room',
};
