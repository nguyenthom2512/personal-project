export default {
  quizDone:
    'Completed Quiz. You achieve {{point}} point. Points have been sent to the creator',
  title: 'Quiz',
};
