export default {
  title: 'Price list',
  onlineAccount: ' online account',
  chatChannel: ' chat channel',
  cloudStorage: ' cloud storage',
  suggestion: 'Recomend using',
  upgradeAccount: 'Upgrade account',
  month: ' /{{month}} months',
  getListFailed: "Get the list failed",
  processTransaction: "Your transaction is being processed"
};
