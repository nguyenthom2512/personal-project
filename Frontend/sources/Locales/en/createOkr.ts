export default {
  Okr: {
    department: 'Department',
    okrParent: 'Superior OKRs',
    objectName: 'Your target',
    okrChallenge: 'Challenging OKRs',
    okrRelate: 'Cross-link OKRs',
    userResponsible: 'People/Teams implementing OKRs',
    userAccountable: 'OKRs Reviewer',
    userInform: 'People notice',
    userConsult: 'Counselor',
    import: 'Input',
    add: 'Add',
    createSuccess: 'Successful new creation',
    createFalse: 'New creation failed',
    listPerson: 'List of responsible person',
    createOkr: 'Create new OKR',
  },
  OkrKR: {
    keyResult: 'Key results',
    target: 'Target',
    unit: 'Measure',
    planUrl: 'Plan link',
    resultUrl: 'Result link',
    deadline: 'Completion time',
    dob:'Date of birth'
  },
  targeTypeError: 'The target must be a positive integer',
  resultParent: 'Superior results'
};
