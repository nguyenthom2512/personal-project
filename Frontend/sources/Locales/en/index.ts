import OKR from './OKR';
import common from './common';
import tabbar from './tabbar';
import account from './account';
import store from './store';
import homepage from './homepage';
import OkrIngredient from './OkrIngredient';
import createOkr from './createOkr';
import login from './login';
import createMissions from './createMissions';
import checkIn from './checkIn';
import treeOKR from './treeOKR';
import todoDetail from './todoDetail';
import room from './room';
import notification from './notification';
import starScreen from './starScreen';
import calendar from './calendar';
import transactionScreen from './transactionScreen';
import quiz from './quiz';
import license from './license';

export default {
  common,
  tabbar,
  OKR,
  OkrIngredient,
  account,
  store,
  homepage,
  createOkr,
  login,
  createMissions,
  checkIn,
  treeOKR,
  todoDetail,
  room,
  notification,
  starScreen,
  calendar,
  transactionScreen,
  quiz,
  license,
};
