import { StyleSheet } from 'react-native';
import { Theme } from '../Theme';

export default (theme: Theme) =>
  StyleSheet.create({
    plusCenter: {
      width: 40,
      height: 40,
      backgroundColor: theme.color.mediumPurple,
      borderRadius: 20,
      alignItems: 'center',
      justifyContent: 'center',
    },
    iconWrapper: { alignItems: 'center', justifyContent: 'center' },
    tabName: {
      fontSize: theme.fontSize.f12,
      fontWeight: '500',
      marginTop: 5,
      color: theme.color.shark,
    },
    tabNameInactive: {
      fontSize: theme.fontSize.f12,
      fontWeight: '500',
      marginTop: 5,
      color: theme.color.mineShaft,
    },
  });
