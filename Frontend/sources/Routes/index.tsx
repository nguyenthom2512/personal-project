import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
  createDrawerNavigator,
  DrawerContentComponentProps,
} from '@react-navigation/drawer';
import { createStackNavigator } from '@react-navigation/stack';
import _ from 'lodash';
import React, { useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';
import { useSelector } from 'react-redux';
// // svg icon account
// import SvgAccount from '../Assets/Svg/account.svg';
// import SvgAccountInactive from '../Assets/Svg/accountInactive.svg';
// // svg icon Home
// import SvgHome from '../Assets/Svg/home.svg';
// import SvgHomeInactive from '../Assets/Svg/homeInactive.svg';
// // svg icon okr
// import SvgOKR from '../Assets/Svg/okr.svg';
// import SvgOKRInactive from '../Assets/Svg/okrInactive.svg';
// // svg icon store
// import SvgStore from '../Assets/Svg/store.svg';
// import SvgStoreInactive from '../Assets/Svg/storeInactive.svg';
import DrawerComponent from '../Components/DrawerComponent';
import { DEVICE_DIMENSION } from '../Helpers/constant';
import { CreateMissions, CreateOKR, SignIn, TodoDetails } from '../Modules';
import Account from '../Modules/Account';
import AgendaCalendar from '../Modules/AgendaCalendar';
import CustomDrawerContent from '../Modules/AgendaCalendar/DrawerContent';
import CFRS from '../Modules/CFRS';
import CheckIn from '../Modules/CheckIn';
import CheckInDetails from '../Modules/CheckIn/CheckInDetails';
import CompletedWork from '../Modules/CompletedWork';
import CreateKR from '../Modules/CreateKR';
import GiveStar from '../Modules/GiveStar';
import HomePage from '../Modules/HomePage';
import ItemStore from '../Modules/ItemStore';
import KrScreen from '../Modules/KrScreen';
// import Call from '../Modules/Call';
import Notification from '../Modules/Notification';
import OkrDetail from '../Modules/OkrDetail';
import Quiz from '../Modules/Quiz';
import ResetPassword from '../Modules/ResetPassword';
import SignUp from '../Modules/SignUp';
// import OKRIngredientScreen from '../Modules/OKRScreen/OKRingredientScreen/OKRingredientScreen';
// import OKRScreen from '../Modules/OKRScreen/OKRScreen';
import StarScreen from '../Modules/StarScreen';
import Store from '../Modules/Store';
import TodoListScreen from '../Modules/TodoList';
import TodoPriorityList from '../Modules/TodoPriorityList';
import TodoPriorityTeamList from '../Modules/TodoPriorityTeamList';
import TransactionDetailScreen from '../Modules/TransactionDetailScreen';
import TreeOKR from '../Modules/TreeOKR';
import UpdateOkr from '../Modules/UpdateOkr';
import UserInformation from '../Modules/UserInformation';
import UpdateUserInformation from '../Modules/UserInformation/UpdateUserInformation';
// import Call from '../Modules/Call';
import Challenge from '../Modules/Challenge';
import { RootState } from '../Redux/Reducers';
import { useTheme, useThemeAwareObject } from '../Theme';
import styles from './styles';
import SeenDetailCheckIn from '../Components/CheckInComponent/SeenDetailCheckIn';
import FeedBack from '../Modules/FeedBack';
import TabbarComponent from '../Components/TabbarComponent';
import GiftDetails from '../Modules/GiftDetails';
import License from '../Modules/License';
import Payment from '../Modules/Payment';
import ListGift from '../Modules/ListGift';

const Tabs = createBottomTabNavigator();

const AuthStack = createStackNavigator();
const HomeStack = createStackNavigator();
const SearchStack = createStackNavigator();
const StoreStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const OKRStack = createStackNavigator();
const CreateTodoStack = createStackNavigator();
const CheckInStack = createStackNavigator();
const LicenseStack = createStackNavigator();

const Drawer = createDrawerNavigator();

const RootStack = createStackNavigator();

const AuthStackScreen = () => (
  <AuthStack.Navigator initialRouteName="SignIn" headerMode="none">
    <AuthStack.Screen
      name="SignIn"
      component={SignIn}
      options={{ title: 'Sign In', animationEnabled: true }}
    />
    <AuthStack.Screen name="ResetPassword" component={ResetPassword} />
    <AuthStack.Screen name="SignUp" component={SignUp} />
    <AuthStack.Screen
      name="License"
      component={License}
      options={{
        animationEnabled: true,
      }}
    />
  </AuthStack.Navigator>
);

const HomeStackScreen = () => (
  <HomeStack.Navigator initialRouteName="HomePage" headerMode="none">
    <HomeStack.Screen name="HomePage" component={HomePage} />
    <HomeStack.Screen name="Calendar" component={DrawerRightScreen} />
    <HomeStack.Screen name="TodoDetails" component={TodoDetails} />
    <HomeStack.Screen name="Profile" component={ProfileStackScreen} />
    <HomeStack.Screen name="ItemStore" component={ItemStore} />
    <HomeStack.Screen name="GiftDetails" component={GiftDetails} />
    <HomeStack.Screen name="CompletedWork" component={CompletedWork} />
    <HomeStack.Screen name="Notification" component={Notification} />
    <HomeStack.Screen name="OkrDetail" component={OkrDetail} />
    <HomeStack.Screen name="Challenge" component={Challenge} />
    <HomeStack.Screen name="CheckInDetails" component={CheckInDetails} />
    <HomeStack.Screen name="Quiz" component={Quiz} />
  </HomeStack.Navigator>
);
const OKRStackScreen = () => (
  <OKRStack.Navigator initialRouteName="TreeOKR" headerMode="none">
    <OKRStack.Screen name="TreeOKR" component={TreeOKR} />
    {/* <OKRStack.Screen name="OKRScreen" component={OKRScreen} />
    <OKRStack.Screen
      name="OKRIngredientScreen"
      component={OKRIngredientScreen}
    /> */}
    <OKRStack.Screen name="CreateOKR" component={CreateOKR} />
    {/* <OKRStack.Screen name="UpdateOkr" component={UpdateOkr} /> */}
    <OKRStack.Screen name="OkrDetail" component={OkrDetail} />
    <OKRStack.Screen name="KrScreen" component={KrScreen} />
    <OKRStack.Screen name="CreateKR" component={CreateKR} />
    <OKRStack.Screen name="FeedBack" component={FeedBack} />
  </OKRStack.Navigator>
);
const StoreStackScreen = () => (
  <StoreStack.Navigator initialRouteName="Store" headerMode="none">
    <StoreStack.Screen name="Store" component={Store} />
  </StoreStack.Navigator>
);

const SearchStackScreen = () => (
  <SearchStack.Navigator initialRouteName="Search" headerMode="none">
    <SearchStack.Screen name="Search" component={SignIn} />
    {/* <SearchStack.Screen name="Search2" component={Template} /> */}
  </SearchStack.Navigator>
);

const TabIcon = (props: {
  iconActive: any;
  iconInActive: any;
  name: string;
  focused: Boolean;
}) => {
  const Styles = useThemeAwareObject(styles);

  return (
    <View style={Styles.iconWrapper}>
      {props.focused ? props.iconActive : props.iconInActive}
      <Text style={props.focused ? Styles.tabName : Styles.tabNameInactive}>
        {props.name}
      </Text>
    </View>
  );
};

const TabsScreen = () => {
  const { t } = useTranslation();
  //   const { theme } = useTheme();
  //   const Styles = useThemeAwareObject(styles);
  //   const { ref, open, close } = useModalize();

  //   const [nav, setNav] = useState(null);

  return (
    <>
      <Tabs.Navigator
        initialRouteName={t('tabbar:home')}
        tabBarOptions={{
          showLabel: false,
          style: { paddingBottom: 26, height: 85 },
        }}
        tabBar={(props) => <TabbarComponent {...props} />}
        // screenOptions={({ route }) => ({
        //   tabBarIcon: ({ focused, size }) => {
        //     switch (route.name) {
        //       case t('tabbar:home'):
        //         return (
        //           <TabIcon
        //             focused={focused}
        //             iconActive={<SvgHome />}
        //             iconInActive={<SvgHomeInactive />}
        //             name={route.name}
        //           />
        //         );
        //       case t('tabbar:OKR'):
        //         return (
        //           <TabIcon
        //             focused={focused}
        //             iconActive={<SvgOKR />}
        //             iconInActive={<SvgOKRInactive />}
        //             name={route.name}
        //           />
        //         );
        //       case t('tabbar:create'):
        //         return (
        //           <View style={Styles.plusCenter}>
        //             <Icon
        //               name="plus"
        //               type="feather"
        //               color={theme.color.white}
        //               tvParallaxProperties
        //             />
        //           </View>
        //         );
        //       case t('tabbar:store'):
        //         return (
        //           <TabIcon
        //             focused={focused}
        //             iconActive={<SvgStore />}
        //             iconInActive={<SvgStoreInactive />}
        //             name={route.name}
        //           />
        //         );
        //       case t('tabbar:account'):
        //         return (
        //           <TabIcon
        //             focused={focused}
        //             iconActive={<SvgAccount />}
        //             iconInActive={<SvgAccountInactive />}
        //             name={route.name}
        //           />
        //         );
        //     }
        //   },
        //   tabBarActiveTintColor: theme.color.mineShaft,
        //   tabBarInactiveTintColor: theme.color.shark,
        // })}
      >
        <Tabs.Screen name={t('tabbar:home')} component={HomeStackScreen} />
        <Tabs.Screen name={t('tabbar:OKR')} component={OKRStackScreen} />
        <Tabs.Screen
          name={t('tabbar:create')}
          component={CreateTodoScreen}
          options={{ tabBarAccessibilityLabel: 'true' }}
          listeners={({ navigation }) => ({
            tabPress: (e) => {
              e.preventDefault();
              //   setNav(navigation);
              //   open();
            },
          })}
        />
        <Tabs.Screen name={t('tabbar:store')} component={StoreStackScreen} />
        <Tabs.Screen
          name={t('tabbar:account')}
          component={ProfileStackScreen}
        />
      </Tabs.Navigator>

      {/* <Modalize ref={ref} adjustToContentHeight>
        <CreateMissions navigation={nav} handleClose={() => close()} />
      </Modalize> */}
    </>
  );
};

const CreateTodoScreen = () => (
  <CreateTodoStack.Navigator
    initialRouteName="CreateMissions"
    headerMode="none"
    mode="modal"
  >
    <CreateTodoStack.Screen name="CreateMissions" component={CreateMissions} />
    <CreateTodoStack.Screen name="CreateOKR" component={CreateOKR} />
  </CreateTodoStack.Navigator>
);

const ProfileStackScreen = (props: {
  route: { params: any };
  navigation: any;
}) => {
  const getInitialList = (params: any) => {
    if (params?.isMyStar) {
      return 'StarScreen';
    }
    return 'ProFile';
  };
  return (
    <ProfileStack.Navigator
      initialRouteName={getInitialList(props.route.params)}
      headerMode="none"
    >
      <ProfileStack.Screen name="Account" component={Account} />
      <ProfileStack.Screen name="StarScreen" component={StarScreen} />
      <ProfileStack.Screen name="ProFile" component={UserInformation} />
      <ProfileStack.Screen
        name="UpdateProFile"
        component={UpdateUserInformation}
      />
      <ProfileStack.Screen
        name="TransactionDetailScreen"
        component={TransactionDetailScreen}
      />
      <ProfileStack.Screen name="GiveStar" component={GiveStar} />
      <ProfileStack.Screen name="Notification" component={Notification} />
      <ProfileStack.Screen name="ListGift" component={ListGift} />
    </ProfileStack.Navigator>
  );
};

const DrawerScreen = (props: { route: { params: any }; navigation: any }) => (
  <Drawer.Navigator
    drawerStyle={{
      width: DEVICE_DIMENSION.Width / 2,
    }}
    drawerContent={(props) => {
      return <DrawerComponent />;
    }}
    initialRouteName="TabsScreen"
    screenOptions={{ swipeEnabled: false }}
  >
    <Drawer.Screen name="TabsScreen" component={TabsScreen} />
    <Drawer.Screen name="Profile" component={ProfileStackScreen} />
    <Drawer.Screen name="Calendar" component={DrawerRightScreen} />
    <Drawer.Screen name="CheckIn" component={CheckInStackScreen} />
    <Drawer.Screen name="CFRS" component={CFRS} />
    <Drawer.Screen name="License" component={LicenseStackScreen} />
    {/* <Drawer.Screen name="Call" component={Call} /> */}
  </Drawer.Navigator>
);

const LicenseStackScreen = () => (
  <LicenseStack.Navigator initialRouteName="License" headerMode="none">
    <LicenseStack.Screen name="License" component={License} />
    <LicenseStack.Screen name="Payment" component={Payment} />
  </LicenseStack.Navigator>
);

const CheckInStackScreen = () => (
  <CheckInStack.Navigator initialRouteName="CheckIn" headerMode="none">
    <CheckInStack.Screen name="CheckIn" component={CheckIn} />
    <CheckInStack.Screen name="CheckInDetails" component={CheckInDetails} />
    <CheckInStack.Screen name="KrScreen" component={KrScreen} />
    <CheckInStack.Screen
      name="SeenDetailCheckIn"
      component={SeenDetailCheckIn}
    />
  </CheckInStack.Navigator>
);

const DrawerRightScreen = (props: {
  route: { params: any };
  navigation: any;
}) => {
  const getInitialList = (params: any) => {
    if (params.isTodayCalendar) {
      return 'AgendaCalendar';
    }
    if (params.isPriority) {
      return 'TodoPriorityList';
    }
    return 'TodoList';
  };

  return (
    <Drawer.Navigator
      initialRouteName={getInitialList(props.route.params)}
      drawerPosition="right"
      drawerContent={(props: DrawerContentComponentProps) => (
        <CustomDrawerContent {...props} />
      )}
    >
      <Drawer.Screen name="TodoList" component={TodoListScreen} />
      <Drawer.Screen name="AgendaCalendar" component={AgendaCalendar} />
      <Drawer.Screen
        name="TodoPriorityList"
        component={() => <TodoPriorityList {...props} />}
      />
      <Drawer.Screen
        name="TodoPriorityTeamList"
        component={() => <TodoPriorityTeamList {...props} />}
      />
    </Drawer.Navigator>
  );
};

const RootStackScreen = () => {
  const userReducer = useSelector((state: RootState) => state.userReducer);
  const userToken = _.isEmpty(userReducer.data);

  return (
    <RootStack.Navigator headerMode="none">
      {userToken ? (
        <RootStack.Screen
          name="Auth"
          component={AuthStackScreen}
          options={{
            animationEnabled: true,
          }}
        />
      ) : (
        <RootStack.Screen
          name="App"
          component={DrawerScreen}
          options={{
            animationEnabled: true,
          }}
        />
      )}
    </RootStack.Navigator>
  );
};

export default RootStackScreen;
