import { apiGet } from './serviceHandle';
import serviceUrls from './serviceUrls';

class SelectionService {
  getCompany() {
    return apiGet(serviceUrls.url.company, {});
  }
  getDepartment() {
    return apiGet(serviceUrls.url.department, {});
  }
  getUnit() {
    return apiGet(serviceUrls.url.unit, {});
  }
  getUser(body : object) {
    return apiGet(serviceUrls.url.raic, {body});
  }
}

export default new SelectionService();
