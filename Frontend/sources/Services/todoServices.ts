import { isElement, isEmpty } from 'lodash';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import { RAIC_TYPE } from '../Helpers/constant';
import { RaicUser } from '../interfaces/todo.interface';
import { TodoCreateBody } from '../interfaces/todo.interface';
import { apiDelete, apiGet, apiPatch, apiPost, apiPut } from './serviceHandle';
import serviceUrls from './serviceUrls';
class todoServices {
  async createTodo(
    body: TodoCreateBody,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      //   const { t } = useTranslation();
      const {
        okrId = '', // remove when submit
        //    date = '', // remove when submit
        user_responsible = [], // remove when submit
        user_accountable = [], // remove when submit
        user_inform = [], // remove when submit
        user_consult = [], // remove when submit
        check_list = [],
        resultName,
        ...remainBody
      } = body;
      console.log('remainBody', remainBody);
      
      if (remainBody.date && remainBody.start_time && remainBody.end_time) {
        const dateConvert = moment(remainBody.date).format('YYYY-MM-DD');
        remainBody.start_time = moment(
          dateConvert + 'T' + remainBody.start_time,
        ).format('YYYY-MM-DDTHH:mm:ss');
        remainBody.end_time = moment(
          dateConvert + 'T' + remainBody.end_time,
        ).format('YYYY-MM-DDTHH:mm:ss');
      }
      !remainBody.start_time && delete remainBody.start_time;
      !remainBody.end_time && delete remainBody.end_time;

      if (isEmpty(remainBody.date)) {
        delete remainBody.date;
        delete remainBody.start_time;
        delete remainBody.end_time;
      }

      if (typeof remainBody.priority === 'object') {
        console.log('remainBody.priority', remainBody.priority);
        // @ts-ignore
        remainBody.priority = remainBody.priority?.id || '';
      }

      const raic_user: RaicUser[] = [];
      user_responsible.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Responsible,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      user_accountable.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Accountable,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      user_inform.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Inform,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      user_consult.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Consult,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      const res = await apiPost(serviceUrls.url.todo, {
        ...remainBody,
        raic_user: raic_user,
        check_list: check_list
          .filter((elm) => Boolean(elm))
          .map((el) => {
            return { content: el, is_done: false };
          }),
      });
      if (res.error) {
        let error = res.errorMessage;
        if (
          res?.detail?.message?.toString()?.toLowerCase() ===
          'cannot start two todo in the same time'.toLowerCase()
        ) {
          error = '';
          //   t('todoDetail:createMissionFailed');
          // 'Đã có công việc trong khung giờ này, vui lòng chọn khung giờ khác';
        }

        onError(error);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }

  async updateTodo(
    id: string | number | undefined,
    body: TodoCreateBody,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const {
        okrId = '', // remove when submit
        //   date = '', // remove when submit
        user_responsible = [], // remove when submit
        user_accountable = [], // remove when submit
        user_inform = [], // remove when submit
        user_consult = [], // remove when submit
        resultName,
        ...remainBody
      } = body;
      if (remainBody.date && remainBody.start_time && remainBody.end_time) {
        const dateConvert = moment(remainBody.date).format('YYYY-MM-DD');
        remainBody.start_time = moment(
          dateConvert + 'T' + remainBody.start_time,
        ).format('YYYY-MM-DDTHH:mm:ss');
        remainBody.end_time = moment(
          dateConvert + 'T' + remainBody.end_time,
        ).format('YYYY-MM-DDTHH:mm:ss');
      }
      {
        !remainBody.start_time && delete remainBody.start_time;
      }
      {
        !remainBody.end_time && delete remainBody.end_time;
      }
      if (isEmpty(remainBody.date)) {
        remainBody.date = null;
        remainBody.start_time = null;
        remainBody.end_time = null;
      }
      // {
      //   !remainBody.date && delete remainBody.date;
      // }
      const raic_user: RaicUser[] = [];
      user_responsible.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Responsible,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      user_accountable.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Accountable,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      user_inform.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Inform,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      user_consult.forEach((elm) => {
        raic_user.push({
          raic_type: RAIC_TYPE.Consult,
          todo: '',
          user: elm,
          okr: '',
        });
      });
      const res = await apiPut(`${serviceUrls.url.todo}${id}`, {
        ...remainBody,
        raic_user: raic_user,
      });
      if (res.error) {
        let error = res.errorMessage;
        if (
          res?.detail?.message?.toString()?.toLowerCase() ===
          'cannot start two todo in the same time'.toLowerCase()
        ) {
          error = '';
          //   t('todoDetail:createMissionFailed');
          // 'Đã có công việc trong khung giờ này, vui lòng chọn khung giờ khác';
        }
        onError(error);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }

  async updateTodoConfident(
    id: string | number,
    body: { confident: number },
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPatch(`${serviceUrls.url.todo}${id}`, body);
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }

  getTodoSelection(params: any) {
    return apiGet(serviceUrls.url.todo, params);
  }

  getTodoDashboard() {
    return apiGet(serviceUrls.url.dashboard, {});
  }
  async deleteTodoDetail(
    id: string | number,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiDelete(serviceUrls.url.todo + id, {});
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
}

export default new todoServices();
