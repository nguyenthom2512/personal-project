import { apiGet, apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class Quiz {
  getQuizDetail(id: String) {
    return apiGet(serviceUrls.url.quiz + id, {});
  }
  getQuiz() {
    return apiGet(serviceUrls.url.quiz, {});
  }
  postOkrChallenge(body: object) {
    return apiPost(serviceUrls.url.postOkrChallenge, body);
  }
  postOkrInspriration(body: object) {
    return apiPost(serviceUrls.url.postOkrInspriration, body);
  }
  getOkrInspriration(id: string) {
    return apiGet(serviceUrls.url.getOkrInsprirationById(id), {});
  }
}

export default new Quiz();
