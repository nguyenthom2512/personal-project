import { autoGenRandomColor } from '../Helpers/color';
import { OKRArrayObject } from '../interfaces/okr.interface';
import { Iitem } from '../Modules/TreeOKR/treeokr.interface';
import { apiGet } from './serviceHandle';
import serviceUrls from './serviceUrls';

class DataOkr {
  getDataOKR(params?: any) {
    return apiGet(serviceUrls.url.okrOwner, params);
  }
  getOkrResult(id: any) {
    return apiGet(serviceUrls.url.okr + id, {});
  }
  getOKRList(params: any) {
    return apiGet(serviceUrls.url.okr, params);
  }

  getOkrUser(params: {okr: number | string}) {
    return apiGet(serviceUrls.url.okrUser, params);
  }

  getTreeOKR = async (
    okr_parent: string,
    department: string,
    created_time__range: string,
    callback: {
      onStart: () => void;
      onSuccess: (data: Iitem[]) => void;
      onError: (e?: string) => void;
      onFinish: () => void;
    },
  ) => {
    if (!department) return;
    const params = {
      page: 1,
      size: 10000,
      okr_parent__isnull: !Boolean(okr_parent),
      okr_parent,
      department,
      created_time__range,
    };
    try {
      const color = autoGenRandomColor();
      const listOkr = await this.getOKRList(params);
      if (listOkr.error) {
        callback.onError(listOkr.errorMessage);
        // Alert.alert(t('common:notice'), listOkr.errorMessage);
        return;
      }
      const child = listOkr?.response?.results.map((elm: OKRArrayObject) => {
        return {
          id: elm.id,
          isOpen: false,
          object_name: elm.object_name,
          children: [],
          color,
          isMatch: elm.is_match,
        };
      });
      callback.onSuccess(child);
    } catch (error) {
      callback.onError('UNKNOWN');
    } finally {
      callback.onFinish();
    }
  };
}

export default new DataOkr();
