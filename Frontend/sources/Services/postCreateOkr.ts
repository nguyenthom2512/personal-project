import { apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class PostCreateOkr {
  postOkr(value: any) {
    apiPost(serviceUrls.url.okr, value);
  }
}

export default new PostCreateOkr();
