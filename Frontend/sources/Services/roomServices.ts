import { apiGet } from './serviceHandle';
import serviceUrls from './serviceUrls';

export interface IGetRoomDetailParams {
  room_id: number | string;
}

class roomServices {
  getRoomDetail(params: IGetRoomDetailParams) {
    return apiGet(serviceUrls.url.roomWithId(params.room_id), {});
  }
}

export default new roomServices();
