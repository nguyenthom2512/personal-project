import { apiGet } from './serviceHandle';
import serviceUrls from './serviceUrls';

class OkrRelate {
  getDataOKR(id?:any) {
    return apiGet(serviceUrls.url.okrRelate + id,{} );
  }
}

export default new OkrRelate();
