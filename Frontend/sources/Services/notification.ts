import { apiDelete, apiGet, apiPatch, apiPost, apiPut } from './serviceHandle';
import serviceUrls from './serviceUrls';

class Notification {
  getNotification(params: Object = {}) {
    return apiGet(serviceUrls.url.notification, params);
  }
  deleteNotification(id: String) {
    return apiDelete(serviceUrls.url.notification + id, {});
  }
  updateNotification(id: String | Number, body: object) {
    return apiPatch(serviceUrls.url.notification + id, body);
  }
  muteNotification(body: object) {
    return apiPatch(serviceUrls.url.muteNotification, body);
  }
  getDetailNotification(id: string) {
    return apiPatch(serviceUrls.url.notificationById(id), {});
  }
}

export default new Notification();
