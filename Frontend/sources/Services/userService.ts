import { apiGet, apiPatch } from './serviceHandle';
import serviceUrls from './serviceUrls';

class UserService {
  updateUser(body: any) {
    return apiPatch(serviceUrls.url.raic + body?.id, body);
  }
  getUser(body: any) {
    return apiGet(serviceUrls.url.raic, body);
  }
}

export default new UserService();
