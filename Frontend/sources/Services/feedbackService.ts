import { apiGet, apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';
class FeedbackService {
  getFeedback(params: Object = {}) {
    return apiGet(serviceUrls.url.feedback, params);
  }

  getCriteriaFeedBack(params: Object = {}) {
    return apiGet(serviceUrls.url.criteria, params);
  }

  async createFeedback(
    body: {},
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const response = await apiPost(serviceUrls.url.feedback, body);
      if (response.error) {
        let err = response.errorMessage;
        if (
          response.detail?.message?.toString() ===
          "Can't send feedback to user in different OKR"
        ) {
          err = 'Không thể tự đánh giá cho người dùng khác OKR';
        }
        if (
          response.detail?.message?.toString() ===
          'can not send feedback to yourself'
        ) {
          err = 'Không thể tự đánh giá cho OKR của bản thân';
        }
        if (
          response.detail?.message?.toString() ===
          'Your rate is not enough to send'
        ) {
          err = 'Bạn không có đủ sao để đánh giá';
        }
        onError(err);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
}
export default new FeedbackService();
