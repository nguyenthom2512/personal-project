import { apiGet, apiPost, apiPut } from './serviceHandle';
import serviceUrls from './serviceUrls';
class PresentService {
  getPresent(params: Object = {}) {
    return apiGet(serviceUrls.url.present, params);
  }
  presentChange(params?: any) {
    return apiPost(serviceUrls.url.present_change, params);
  }
  getListPresent() {
    return apiGet(serviceUrls.url.present_change, {});
  }
  putPresentChange(id: number,params: any){
    return apiPut(serviceUrls.url.present_change + id, params)
  }

}
export default new PresentService();
