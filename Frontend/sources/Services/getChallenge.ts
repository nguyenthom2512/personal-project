import { apiGet, apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class Challenge {
  getChallenge(id: any) {
    return apiGet(serviceUrls.url.challenge + id, {});
  }
}

export default new Challenge();
