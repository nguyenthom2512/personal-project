import { apiGet, apiPatch, apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class TransferDiamondService {
  postTransferDiamond(params: any) {
    return apiPost(serviceUrls.url.transferDiamond, params);
  }
}

export default new TransferDiamondService();
