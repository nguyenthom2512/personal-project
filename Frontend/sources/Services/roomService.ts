import { apiDelete, apiGet, apiPost, apiPut } from './serviceHandle';
import serviceUrls from './serviceUrls';

class roomServices {
  getRoom(params: any) {
    return apiGet(serviceUrls.url.roomDetail, { params });
  }

  async createRoom(
    body: any,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPost(serviceUrls.url.room, body);
      if (res.error) {
        let errorText = res.errorMessage;
        if (res?.detail?.message.includes('permission to create room')) {
          errorText = 'Bạn không có quyền tạo phòng họp';
        }

        onError(errorText);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
}

export default new roomServices();
