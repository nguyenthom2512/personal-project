import { apiGet } from './serviceHandle';
import serviceUrls from './serviceUrls';

class PackageServices {
  getPackages(params: any) {
    return apiGet(serviceUrls.url.package, { params });
  }
}

export default new PackageServices();
