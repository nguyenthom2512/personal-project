import { create } from 'apisauce';
import _ from 'lodash';
import { Alert } from 'react-native';
import serviceUrls from './serviceUrls';
export interface IReturnType {
  errorMessage?: string;
  detail?: any;
  error?: boolean;
  response?: any;
}

const api = create({
  baseURL: serviceUrls.url.HOST,
  timeout: 60000,
  headers: { 'content-type': 'application/json' },
});

/**
 * process return data
 * @param {*} response
 */
const returnData = (response: any) => {
  if (__DEV__) {
    console.log('returnData', response);
  }

  let errorMessage = '';
  if (serviceUrls.statusCode.success.includes(response.status)) {
    return {
      response: response.data,
      error: false,
    };
  }
  if (_.isNull(response.data)) {
    errorMessage = response.problem;
  } else if (
    [
      ...serviceUrls.statusCode.notFound,
      ...serviceUrls.statusCode.auth,
      ...serviceUrls.statusCode.permission,
    ].includes(response.status)
  ) {
    errorMessage = response.data.detail ? response.data.detail : response.data;
    // /**
    //  * CHECK PERMISSION
    //  */
    // if (
    //   errorMessage.toLowerCase() ===
    //   'You do not have permission to perform this action.'.toLowerCase()
    // ) {
    //   Alert.alert(
    //     'Thông báo',
    //     'Tài khoản của bạn chưa được cấp quyền để sử dụng tính năng này',
    //   );
    // }
  } else if (serviceUrls.statusCode.error.includes(response.status)) {
    errorMessage = response.problem;
  } else {
    errorMessage = response.data.problem;
  }

  return {
    errorMessage,
    detail: response?.data,
    error: true,
  };
};

/**
 * set token for authentication
 * @param {*} token
 */
const setToken = (token: string) => {
  if (token) {
    api.setHeader('Authorization', `Token ${token}`);
  } else {
    delete api.headers['Authorization'];
  }
};

/**
 *
 * @param {*url without host} url
 * @param {*param} params
 */
const apiGet = async (url: string, params: any) => {
  const dataResponse = await api.get(url, params);
  return returnData(dataResponse);
};

/**
 *
 * @param {*url without host} url
 * @param {*} body
 */
const apiPost = async (url: string, body: any) => {
  const dataResponse = await api.post(url, JSON.stringify(body));

  return returnData(dataResponse);
};

/**
 *
 * @param {*url without host} url
 * @param {*} body
 */
const apiPut = async (url: string, body: any) => {
  const dataResponse = await api.put(url, body);
  // logic handle dataResponse here
  return returnData(dataResponse);
};

/**
 *
 * @param {*url without host} url
 * @param {*} body
 */
const apiPatch = async (url: string, body: any) => {
  const response = await api.patch(url, body);
  return returnData(response);
};

/**
 *
 * @param {*url without host} url
 * @param {*} body
 */
const apiDelete = async (url: string, body: any) => {
  const response = await api.delete(url, body);
  // logic handle response here
  return returnData(response);
};

const uploadImage = async (body: FormData) => {
  const response = await api.post('/upload/', body, {
    headers: { 'content-type': 'multipart/form-data' },
    timeout: 240000,
  });
  return returnData(response);
};

export { apiGet, apiPost, setToken, apiPut, apiPatch, apiDelete, uploadImage };
