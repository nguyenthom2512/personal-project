import { apiGet, apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class PaymentServices {
  getPayment(params: any) {
    return apiGet(serviceUrls.url.payment, params);
  }
  postPayment(body: any) {
    return apiPost(serviceUrls.url.payment, body);
  }
}

export default new PaymentServices();
