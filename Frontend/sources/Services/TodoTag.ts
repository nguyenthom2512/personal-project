import { apiDelete, apiGet, apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

interface BodyCreateTag {
  tag_name: string;
  tag_code: string;
}

class TodoTag {
  getTodoTag() {
    return apiGet(serviceUrls.url.tag, {});
  }
  async createTodoTag(
    body: BodyCreateTag,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPost(serviceUrls.url.tag, body);
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
  async deleteTodoTag(
    id: string | number,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiDelete(serviceUrls.url.tag + id, {});
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
}

export default new TodoTag();
