import { apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class resetPassword {
  resetPassword(body: object) {
    return apiPost(serviceUrls.url.reserPassword, body );
  }
}

export default new resetPassword();
