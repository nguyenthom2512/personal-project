import serviceUrls from './serviceUrls';
import { apiGet, apiDelete, apiPost, apiPut } from './serviceHandle';

interface CommentInterface {
  user: number;
  title: string;
}

class TodoComment {
  getCommentTodo(id: number) {
    return apiGet(serviceUrls.url.commentTodo, { todo: id });
  }
  async createTodoComment(
    body: any,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPost(serviceUrls.url.commentTodo, body);
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
  async deleteTodoComment(
    id: any,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiDelete(serviceUrls.url.commentTodo + id, {});
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
  async updateComment(
    id: any,
    body: any,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPut(serviceUrls.url.commentTodo + id, body);
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
  postCommentTodo(body: object) {
    return apiPost(serviceUrls.url.commentTodo, body);
  }
  deleteComment(id: string) {
    return apiDelete(serviceUrls.url.commentTodo + id, {});
  }
  editComment(id: string | number, body: object) {
    return apiPut(serviceUrls.url.commentTodo + id, body);
  }
}

export default new TodoComment();
