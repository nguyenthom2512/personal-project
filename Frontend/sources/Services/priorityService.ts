import { PriorityBody } from '../interfaces/priority.interface';
import { apiDelete, apiGet, apiPost, apiPut } from './serviceHandle';
import serviceUrls from './serviceUrls';

interface BodyCreateTag {
  priority_name: string;
  color: string;
  user: number;
  priority_level: number;
}

class TodoPriority {
  getTodoPriority() {
    return apiGet(serviceUrls.url.priority, {});
  }
  async createTodoTag(
    body: BodyCreateTag,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPost(serviceUrls.url.priority, body);
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
  async deleteTodoPriority(
    id: string | number,
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiDelete(serviceUrls.url.priority + id, {});
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
  async updatePriority(
    body: PriorityBody[],
    onError: (errMess: string | undefined) => void,
    onSuccess: () => void,
  ) {
    try {
      const res = await apiPut(serviceUrls.url.updatePriorityList, body);
      if (res.error) {
        onError(res.errorMessage);
      } else {
        onSuccess();
      }
    } catch (error) {
      onError('UNKNOWN');
    }
  }
}

export default new TodoPriority();
