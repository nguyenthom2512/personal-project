import { apiGet } from './serviceHandle';
import serviceUrls from './serviceUrls';

class DataCreateOKRService {
  getDataCreateOKR() {
    return apiGet(serviceUrls.url.dataForCreateOkr, {});
  }
}

export default new DataCreateOKRService();
