import { apiPost } from './serviceHandle';
import serviceUrls from './serviceUrls';

class SignUp {
  postSignUp(value: any) {
    return apiPost(serviceUrls.url.signUp, value);
  }
}

export default new SignUp();
