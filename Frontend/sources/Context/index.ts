import React from 'react';
import { LoadingContext } from '../interfaces/loading.interface';
import { ICallContext } from '../interfaces/call.interface';

export const AuthContext = React.createContext(null);
export const IndicatorContext = React.createContext<LoadingContext>({
  loading: false,
  setLoading: (value) => {},
});
export const CallContext = React.createContext<ICallContext>({
  roomId: '',
  setRoomId: (value) => {},
});
