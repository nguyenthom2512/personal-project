export const INTERFACE_CONFIG = {
  LANG_DETECTION: false,
  lang: 'vi',
  APP_NAME: 'OKRs online',
  DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
  HIDE_INVITE_MORE_HEADER: true,
  MOBILE_APP_PROMO: false,
  SHOW_CHROME_EXTENSION_BANNER: false,
  TOOLBAR_BUTTONS: [
    // 'microphone',
    // 'camera',
    // 'fullscreen',
    // 'fodeviceselection',
    // 'hangup',
    // 'profile',
    // 'chat',
    // 'settings',
    // 'videoquality',
    // 'tileview',
    // 'download',
    // 'help',
    // 'mute-everyone',
    // 'security'
  ],
};

export const CONFIG_JITSI = {
  defaultLanguage: 'vi',
  prejoinPageEnabled: false,
};

export const DOMAIN_JITSI = 'https://meet.jit.si';
