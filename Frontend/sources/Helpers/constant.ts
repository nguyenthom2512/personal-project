import { Dimensions } from 'react-native';
import serviceUrls from '../Services/serviceUrls';

export const RAIC_TYPE = {
  Responsible: 0,
  Accountable: 1,
  Inform: 2,
  Consult: 3,
};

export const dateFormat = {
  year_month_day: 'YYYY-MM-DD',
  dateTimeFormat: 'YYYY-MM-DD HH:mm',
  dateTimeServer: 'YYYY-MM-DDTHH:mm:ss',
  DMYHm: 'DD/MM/YYYY - HH:mm',
  timeDate: 'HH:mm - DD/MM/YYYY',
};

export const DEVICE_DIMENSION = {
  Width: Dimensions.get('screen').width,
  Height: Dimensions.get('screen').height,
};

export const minuteInHour = 60;

export const headerCalendarHeight = 100;
export const pixelHourCalendar = 50;

export const imageAPILink = (url: string) => `${serviceUrls.url.IMAGE}${url}`;

export const WEB_CLIENT_ID =
  '975231315998-875ucvb0dm8r416gmhg647ak73sf2don.apps.googleusercontent.com';

export const LOGIN_SOCIAL_TYPE = {
  FACEBOOK: 1,
  GOOGLE: 2,
  APPLE: 3,
};

// emitter event name
export const EventName = {
  onReceivedNotification: "ON_RECEIVED_NOTIFICATION"
}
