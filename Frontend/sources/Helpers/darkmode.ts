import { Appearance } from 'react-native';
const isDarkMode = () => {
  return Appearance.getColorScheme() === 'dark';
};
export default isDarkMode;
