import moment from 'moment';

export const handleDataEvent = (data: any[]) => {
  return data.map((elm, _index) => {
    return {
      id: elm?.id,
      title: elm?.todo_name,
      start: elm?.start_time ? moment(elm?.start_time).toDate() : new Date(elm?.start_time),
      end: elm?.end_time ? moment(elm?.end_time).toDate() : new Date(elm?.end_time),
      type: elm?.priority?.priority_level ?? 0,
      color: elm.priority.color,
    };
  });
};

export const handleDataEventByDay = (data: any) => {
  return Object.keys(data).map((elm, _index) => {
    return {
      time: moment(elm).format('DD/MM'),
      listEvent: data[elm].map((item: any, index: number) => {
        return {
          title: item.todo_name,
          start: new Date(item?.start_time),
          end: new Date(item?.end_time),
          type: item.priority.priority_level,
        };
      }),
    };
  });
};
