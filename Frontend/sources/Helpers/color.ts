import { useTheme } from '../Theme';

// export default {
//   violet: '#A362EA',
//   primary: '#FCB71E',
//   backgroundColor: '#ffffff',
//   white: '#ffffff',
//   lightningYellow: '#FCB71E',
//   blueChathams: '#134F8B',
//   blueCerulean: '#008FE0',
//   grayShaft: '#333333',
//   gray: '#828282',
//   lightGray: '#f5f5f5',
//   grayAbbey: '#58595B',
//   grayBoder: '#7C7C7C',
//   graySilver: '#C6C6C6',
//   grayIcon: '#212121',
//   greenLimeade: '#5AA800',
//   redMonza: '#E20011',
//   redValencia: '#DD4545',
//   redAlert: '#D80000',
//   lightGrayBorder: '#EFEEF4',
//   torchRed: '#F4001E',
//   black: '#000000',
//   blackTitle: '#26272E',
//   orange: 'orange',
//   mercury: '#E5E5E5',
//   mineShaft: '#26272E80',
//   shark: '#26272E',
//   mediumPurple: '#A362EA',
//   holderPlace: '#CACFD2',
//   jadeGreen: '#43C8DE',
//   yellow: '#F4A921',
//   lightOrange: '#F45B34',
//   pink: '#FFC3C3',
//   blackBoder: '#404047',
//   blackHeader: '#07172F',
//   grayBox: '#343640',
//   magicMint: '#ABFFC3',
//   salomie: '#FFD487',
//   lavenderBlue: '#C5C3FF',
//   blizzardBlue: '#A1F1FF',
//   blackBox: '#23242A',
//   whiteSmoke: '#F4F4F4',
// };
export const getColorByPriority = (level: number) => {
  const { theme } = useTheme();
  switch (level) {
    case 0:
      return theme.color.pink;
    case 1:
      return theme.color.salomie;
    case 2:
      return theme.color.magicMint;
    case 3:
      return theme.color.blizzardBlue;
    default:
      return theme.color.primary;
  }
};

export const autoGenRandomColor = () =>
  `#${Math.floor(Math.random() * 16777215).toString(16)}`;
