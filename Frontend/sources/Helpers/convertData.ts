import {
  Asset,
  ImagePickerResponse,
  launchImageLibrary,
} from 'react-native-image-picker';
export const createFormImage = (imageData?: Asset) => {
  const body = new FormData();
  body.append('file', {
    uri: imageData?.uri,
    type: imageData?.type,
    name: imageData?.fileName,
  });

  return body;
};

export const createDataSelect = (
  arrayData: any = [],
  label: string,
  id = 'id',
) => {
  return arrayData.map((elm: any) => {
    return {
      label: elm[label],
      value: elm[id],
    };
  });
};
