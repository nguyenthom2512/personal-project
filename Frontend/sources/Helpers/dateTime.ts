import moment from 'moment';
import { useTranslation } from 'react-i18next';

export const DDMMYYYY = 'DD-MM-YYYY';
export const YYYYMMDD = 'YYYY-MM-DD';
export const HHmm = 'HH:mm';
export const LLL = 'lll';

const quarter = (currentQuarter: number) => {
  const current = moment().quarter(currentQuarter);
  return `${current.startOf('quarter').format(YYYYMMDD)},${current
    .endOf('quarter')
    .format(YYYYMMDD)}`;
};
export const QUARTER = () => {
  const { t } = useTranslation();
  const year = moment().format('YYYY');
  return [
    {
      label: t('common:quarter1', { year }),
      value: quarter(1),
      id: quarter(1),
    },
    {
      label: t('common:quarter2', { year }),
      value: quarter(2),
      id: quarter(2),
    },
    {
      label: t('common:quarter3', { year }),
      value: quarter(3),
      id: quarter(3),
    },
    {
      label: t('common:quarter4', { year }),
      value: quarter(4),
      id: quarter(4),
    },
  ];
};
