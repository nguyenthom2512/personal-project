// Imports: Dependencies
import AsyncStorage from '@react-native-community/async-storage';
import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import _ from 'lodash';
// Imports: Redux
import sagas from '../sources/Redux/Sagas';
import rootReducer from '../sources/Redux/Reducers';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as serviceHandle from '../sources/Services/serviceHandle';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import actionTypes from '../sources/Redux/ActionTypes';
// Middleware: Redux Persist Config
const persistConfig = {
  // Root
  key: 'root',
  timeout: 0,
  // Storage Method (React Native)
  storage: AsyncStorage,
  // Whitelist (Save Specific Reducers)
  whitelist: ['userReducer', 'languageReducer'],
  // Blacklist (Don't Save Specific Reducers)
  blacklist: [],
};
// Middleware: Redux Persist Persisted Reducer
const middleware = [];
const sagaMiddleware = createSagaMiddleware();
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const handleAuthTokenMiddleware =
  (store: any) => (next: any) => (action: any) => {
    if (action.type === actionTypes.LOGIN_SUCCESS) {
      const token = action.response.token;
      serviceHandle.setToken(token);
    }
    next(action);
  };
middleware.push(sagaMiddleware, handleAuthTokenMiddleware);
if (__DEV__) {
  middleware.push(createLogger());
}
const persistedReducer = persistReducer(persistConfig, rootReducer);

const enhancers = [applyMiddleware(...middleware)];

const config: any = { enhancers };

// Redux: Store

const store = createStore(persistedReducer, undefined, compose(...enhancers));
// Middleware: Redux Persist Persister
const persistor = persistStore(store, config, () => {
  const stateData = store.getState();
  if (
    !_.isEmpty(stateData.userReducer) &&
    !_.isEmpty(stateData.userReducer.data)
  ) {
    const token = stateData.userReducer.data.token;
    serviceHandle.setToken(token);
  }
});
sagaMiddleware.run(sagas);

// Exports
export { store, persistor };
